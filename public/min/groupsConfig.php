<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

session_start();

$ver = isset($_REQUEST['v'])?'/'.$_REQUEST['v']:'';

$assets = [

    // js

    'js.plain' => array(
        '//assets' . $ver . '/js/jquery.min.js',
        '//assets' . $ver . '/js/bootstrap.min.js',
        '//assets' . $ver . '/js/bootstrap-dialog.js',
        '//assets' . $ver . '/js/perfect-scrollbar.jquery.js',        
        //'//assets' . $ver . '/js/khazs-simple-slider.min.js',
        //'//assets' . $ver . '/js/khazs-simple-gallery.min.js',
        //'//assets' . $ver . '/js/spinners/spinners.min.js',
        '//assets' . $ver . '/js/psw-strength.js',        
        //'//assets' . $ver . '/js/lightview/lightview.js',
        '//assets' . $ver . '/js/dropdown-menu.min.js',
        //'//assets' . $ver . '/js/chosen.jquery.js',
        '//assets' . $ver . '/js/common.js',
        '//assets' . $ver . '/js/app.js'
    ),

    'js.default' => array(
        '//assets' . $ver . '/js/jquery.min.js',
        '//assets' . $ver . '/js/bootstrap.min.js',
        '//assets' . $ver . '/js/bootstrap-dialog.js',
        '//assets' . $ver . '/js/perfect-scrollbar.jquery.js',
        '//assets' . $ver . '/js/khazs-simple-slider.min.js',
        //'//assets' . $ver . '/js/khazs-simple-gallery.min.js',
        //'//assets' . $ver . '/js/spinners/spinners.min.js',
        //'//assets' . $ver . '/js/lightview/lightview.js',
        '//assets' . $ver . '/js/psw-strength.js',
        '//assets' . $ver . '/js/dropdown-menu.min.js',
        '//assets' . $ver . '/js/common.js',
        '//assets' . $ver . '/js/panel.js'     
    ),

    'js.full' => array(
        '//assets' . $ver . '/js/jquery.min.js',
        '//assets' . $ver . '/js/bootstrap.min.js',
        '//assets' . $ver . '/js/bootstrap-dialog.js',
        '//assets' . $ver . '/js/perfect-scrollbar.jquery.js',
        '//assets' . $ver . '/js/jquery.html5Loader.js',        
        '//assets' . $ver . '/js/jquery.html5Loader.custom.js',        
        '//assets' . $ver . '/js/modernizr.js',
        '//assets' . $ver . '/js/lightview/lightview.js',
        '//assets' . $ver . '/js/psw-strength.js',
        '//assets' . $ver . '/js/dropdown-menu.min.js',
        '//assets' . $ver . '/js/common.js',
        '//assets' . $ver . '/js/panel.js'     
    ),

    'js.promo' => array(
        '//assets' . $ver . '/js/jquery.min.js',
        '//assets' . $ver . '/js/bootstrap.min.js',
        '//assets' . $ver . '/js/bootstrap-dialog.js',
        '//assets' . $ver . '/js/perfect-scrollbar.jquery.js',
        '//assets' . $ver . '/js/jquery.html5Loader.js',        
        '//assets' . $ver . '/js/jquery.html5Loader.custom.js',        
        '//assets' . $ver . '/js/modernizr.js',
        '//assets' . $ver . '/js/lightview/lightview.js',
        '//assets' . $ver . '/js/psw-strength.js',
        '//assets' . $ver . '/js/dropdown-menu.min.js',
        '//assets' . $ver . '/js/common.js',
        '//assets' . $ver . '/js/panel.js',    
        '//assets' . $ver . '/js/promo.js'
    ),


    'js.mobile' => array(
        '//assets' . $ver . '/js/jquery.min.js',
        '//assets' . $ver . '/js/bootstrap.min.js',
    ),

    'js.panel' => array(
        '//assets' . $ver . '/js/jquery-2.1.1.js',
        '//assets' . $ver . '/js/bootstrap.min.js',
        '//assets' . $ver . '/js/perfect-scrollbar.jquery.js',        
        '//assets' . $ver . '/js/jquery-ui.min.js',
        '//assets' . $ver . '/js/bootstrap-dialog.js',
        '//assets' . $ver . '/js/moment.js',
        '//assets' . $ver . '/js/dropzone.js',
        //'//assets' . $ver . '/ckeditor/ckeditor.js',
        '//assets' . $ver . '/js/bootstrap-datetimepicker.js',
        //'//assets' . $ver . '/js/summernote.min.js',
        
        '//assets' . $ver . '/ckeditor/samples/js/sample.js',
        //'//assets' . $ver . '/js/enscroll.js',
        //'//assets' . $ver . '/js/chosen.jquery.js',
        '//assets' . $ver . '/js/common.js',
        '//assets' . $ver . '/js/panel.js'
    ),

    'js.guides' => [
        '//assets' . $ver . '/js/jquery.min.js',
        '//assets' . $ver . '/js/bootstrap.min.js',
        '//assets' . $ver . '/js/bootstrap-dialog.js',
        '//assets' . $ver . '/js/perfect-scrollbar.jquery.js',        
        '//assets' . $ver . '/js/psw-strength.js',
        '//assets' . $ver . '/js/player-portal.js',
        //'//assets' . $ver . '/js/jquery.uniform.min.js',
        '//assets' . $ver . '/js/jquery.ba-throttle-debounce.min.js',
        '//assets' . $ver . '/js/guides/dropdown-menu.min.js',
        '//assets' . $ver . '/js/jquery.swipebox.js',
        //'//assets' . $ver . '/js/guides/app.js',
        '//assets' . $ver . '/js/common.js',        
        '//assets' . $ver . '/js/panel.js'
    ],

    'js.story' => [
        '//assets' . $ver . '/js/jquery.min.js',
        //'//assets' . $ver . '/js/jquery.mobile.custom.min.js',
        '//assets' . $ver . '/js/bootstrap.min.js',        
        '//assets' . $ver . '/js/bootstrap-dialog.js',        
        '//assets' . $ver . '/js/jquery.html5Loader.js',        
        '//assets' . $ver . '/js/jquery.html5Loader.custom.js',        
        '//assets' . $ver . '/js/modernizr.js',
        '//assets' . $ver . '/js/common.js',        
        '//assets' . $ver . '/js/panel.js',
        //'//assets' . $ver . '/js/psw-strength.js'
    ],

    /*
    'js.history' => [
        '//assets' . $ver . '/js/jquery.min.js',
        '//assets' . $ver . '/js/bootstrap.min.js',        
        '//assets' . $ver . '/js/bootstrap-dialog.js',        
        '//assets' . $ver . '/js/perfect-scrollbar.jquery.js',        
        '//assets' . $ver . '/js/modernizr.custom.min.js',
        '//assets' . $ver . '/js/common.js',        
        '//assets' . $ver . '/js/panel.js',
        '//assets' . $ver . '/js/psw-strength.js',  
    ],*/

    'js.heroes' => [
        '//assets' . $ver . '/js/jquery.min.js',
        '//assets' . $ver . '/js/bootstrap.min.js',        
        '//assets' . $ver . '/js/bootstrap-dialog.js',
        '//assets' . $ver . '/js/perfect-scrollbar.jquery.js',        
        '//assets' . $ver . '/js/psw-strength.js',
        '//assets' . $ver . '/js/jquery.ba-throttle-debounce.min.js',
        //'//assets' . $ver . '/js/jquery.uniform.min.js',
        '//assets' . $ver . '/js/jquery.validate.min.js',
        '//assets' . $ver . '/js/jquery.swipebox.js',
        '//assets' . $ver . '/js/slick.min.js',
        '//assets' . $ver . '/js/jquery.panelSnap.js',
        '//assets' . $ver . '/js/jquery.fancybox.pack.js',
        '//assets' . $ver . '/js/infinite-odyssey.js',
        '//assets' . $ver . '/js/modernizr.js',
        '//assets' . $ver . '/js/guides/app.js',
        '//assets' . $ver . '/js/common.js',        
        '//assets' . $ver . '/js/panel.js'     
    ],

    'js.heroe' => [
        '//assets' . $ver . '/js/jquery.min.js',
        //'//assets' . $ver . '/js/jquery.ba-throttle-debounce.min.js',
        '//assets' . $ver . '/js/perfect-scrollbar.jquery.js',                
        //'//assets' . $ver . '/js/jquery.uniform.min.js',
        //'//assets' . $ver . '../js/player-portal5318.js?1423497759',
        '//assets' . $ver . '/js/player-portal.js',
        //'//assets' . $ver . '/js/jquery.validate.min.js',
        '//assets' . $ver . '/js/jquery.swipebox.js',
        '//assets' . $ver . '/js/heroe.js',
        //'//assets' . $ver . '/js/modernizr.js' 
    ],


    'js.inferno' => [
        '//assets' . $ver . '/js/jquery.min.js',
        '//assets' . $ver . '/js/bootstrap.min.js',        
        '//assets' . $ver . '/js/bootstrap-dialog.js',        
        '//assets' . $ver . '/js/perfect-scrollbar.jquery.js',                
        '//assets' . $ver . '/js/jquery.ba-throttle-debounce.min.js',
        //'//assets' . $ver . '/js/jquery.uniform.min.js',
        '//assets' . $ver . '/js/jquery.validate.min.js',
        '//assets' . $ver . '/js/jquery.swipebox.js',
        '//assets' . $ver . '/js/slick.min.js',
        '//assets' . $ver . '/js/jquery.panelSnap.js',
        '//assets' . $ver . '/js/jquery.fancybox.pack.js',
        '//assets' . $ver . '/js/infinite-odyssey-zones.js',
        '//assets' . $ver . '/js/modernizr.js',
        '//assets' . $ver . '/js/guides/app.js',
        '//assets' . $ver . '/js/common.js',        
        '//assets' . $ver . '/js/panel.js'           
    ],

    'js.items' => [
        '//assets' . $ver . '/js/jquery.min.js',
        '//assets' . $ver . '/js/bootstrap.min.js',        
        '//assets' . $ver . '/js/bootstrap-dialog.js', 
        '//assets' . $ver . '/js/perfect-scrollbar.jquery.js',        
        '//assets' . $ver . '/js/jquery.ba-throttle-debounce.min.js',
        '//assets' . $ver . '/js/player-portal.js',        
        //'//assets' . $ver . '/js/jquery.uniform.min.js',
        '//assets' . $ver . '/js/jquery.validate.min.js',
        '//assets' . $ver . '/js/jquery.swipebox.js',        
        '//assets' . $ver . '/js/modernizr.js',        
        '//assets' . $ver . '/js/app.js',
        '//assets' . $ver . '/js/common.js',        
        '//assets' . $ver . '/js/panel.js'             
    ],

    // css


    'css.plain' => array(
        '//assets' . $ver . '/css/bootstrap.min.css', 
        '//assets' . $ver . '/css/bootstrap-dialog.min.css',  
        '//assets' . $ver . '/css/ionicons.min.css', 
        '//assets' . $ver . '/css/font-awesome.css',         
        //'//assets' . $ver . '/fonts/vectors/foundation-icons.css'fa fa-plus, 
        '//assets' . $ver . '/css/app.css',
        '//assets' . $ver . '/css/common.css',
        '//assets' . $ver . '/css/chosen.css',
        '//assets' . $ver . '/css/buttons.css',        
        '//assets' . $ver . '/css/beaufort.css',
        '//assets' . $ver . '/css/spiegel.css',        
        //'//assets' . $ver . '/css/tab.minf39e.css',
        //'//assets' . $ver . '/css/lightview/lightview.css'
    ),


    'css.default' => array(
        '//assets' . $ver . '/css/bootstrap.css', 
        '//assets' . $ver . '/css/bootstrap-dialog.min.css',  
        '//assets' . $ver . '/css/perfect-scrollbar.css',  
        '//assets' . $ver . '/css/beaufort.css',
        '//assets' . $ver . '/css/spiegel.css',
        '//assets' . $ver . '/css/panel.css',
        '//assets' . $ver . '/css/font-awesome.css',         
        '//assets' . $ver . '/css/ionicons.min.css', 
        //'//assets' . $ver . '/fonts/vectors/foundation-icons.css'fa fa-plus, 
        '//assets' . $ver . '/css/app.css',
        '//assets' . $ver . '/css/common.css',
        '//assets' . $ver . '/css/buttons.css',
        //'//assets' . $ver . '/css/proximanova.css',
        //'//assets' . $ver . '/css/tab.minf39e.css',
        //'//assets' . $ver . '/css/lightview/lightview.css',
        //'//assets' . $ver . '/css/responsive.css',
    ),

    'css.mobile' => array(
        '//assets' . $ver . '/css/bootstrap.css', 
        '//assets' . $ver . '/css/mobile.css', 
        //'//assets' . $ver . '/css/responsive.css',
    ),

    'css.full' => array(
        '//assets' . $ver . '/css/bootstrap.css', 
        '//assets' . $ver . '/css/bootstrap-dialog.min.css',  
        '//assets' . $ver . '/css/beaufort.css',
        '//assets' . $ver . '/css/spiegel.css',
        '//assets' . $ver . '/css/panel.css',
        '//assets' . $ver . '/css/perfect-scrollbar.css',          
        //'//assets' . $ver . '/css/font-awesome.css',         
        '//assets' . $ver . '/css/promo.css',         
        '//assets' . $ver . '/css/ionicons.min.css', 
        '//assets' . $ver . '/css/font-awesome.css',                 
        //'//assets' . $ver . '/fonts/vectors/foundation-icons.css'fa fa-plus, 
        '//assets' . $ver . '/css/full.css',
        '//assets' . $ver . '/css/subnav.css', 
        '//assets' . $ver . '/css/common.css',
        '//assets' . $ver . '/css/buttons.css',
        //'//assets' . $ver . '/css/proximanova.css',
        //'//assets' . $ver . '/css/tab.minf39e.css',
        //'//assets' . $ver . '/css/lightview/lightview.css',
        //'//assets' . $ver . '/css/responsive.css',
    ),

    'css.promo' => array(
        '//assets' . $ver . '/css/bootstrap.css', 
        '//assets' . $ver . '/css/bootstrap-dialog.min.css',  
        '//assets' . $ver . '/css/beaufort.css',
        '//assets' . $ver . '/css/spiegel.css',
        '//assets' . $ver . '/css/panel.css',
        '//assets' . $ver . '/css/perfect-scrollbar.css',          
        //'//assets' . $ver . '/css/font-awesome.css',         
        '//assets' . $ver . '/css/promo.css',         
        '//assets' . $ver . '/css/ionicons.min.css', 
        '//assets' . $ver . '/css/font-awesome.css',                 
        //'//assets' . $ver . '/fonts/vectors/foundation-icons.css'fa fa-plus, 
        '//assets' . $ver . '/css/full.css',
        '//assets' . $ver . '/css/subnav.css', 
        '//assets' . $ver . '/css/common.css',
        '//assets' . $ver . '/css/buttons.css',
        //'//assets' . $ver . '/css/proximanova.css',
        //'//assets' . $ver . '/css/tab.minf39e.css',
        //'//assets' . $ver . '/css/lightview/lightview.css',
        //'//assets' . $ver . '/css/responsive.css',
    ),

    'css.story' => array(
        '//assets' . $ver . '/css/bootstrap.css',  
        '//assets' . $ver . '/css/bootstrap-dialog.min.css',
        //'//assets' . $ver . '/story/dist/production.min.css',          
        '//assets' . $ver . '/css/common.css',
        '//assets' . $ver . '/css/story.css',
        '//assets' . $ver . '/css/buttons.css',
        '//assets' . $ver . '/css/panel.css',        
        '//assets' . $ver . '/css/buttons.css',
        '//assets' . $ver . '/css/subnav.css', 
        '//assets' . $ver . '/css/spiegel.css',
        '//assets' . $ver . '/css/beaufort.css',
        '//assets' . $ver . '/css/ionicons.min.css', 
        '//assets' . $ver . '/css/font-awesome.css',                 
        //'//assets' . $ver . '/css/responsive.css',
    ),

    /*'css.history' => array(
        '//assets' . $ver . '/css/subnav.css', 
        '//assets' . $ver . '/css/history.css', 
        '//assets' . $ver . '/css/bootstrap.css',  
        '//assets' . $ver . '/css/perfect-scrollbar.css',          
        '//assets' . $ver . '/css/common.css',
        '//assets' . $ver . '/css/spiegel.css',
        '//assets' . $ver . '/css/beaufort.css',
        '//assets' . $ver . '/css/buttons.css',
        '//assets' . $ver . '/css/ionicons.min.css', 
        '//assets' . $ver . '/css/panel.css',        
        '//assets' . $ver . '/css/buttons.css',
        '//assets' . $ver . '/css/bootstrap-dialog.min.css',
        //'//assets' . $ver . '/css/responsive.css',
    ),*/

    'css.panel' => array(
        '//assets' . $ver . '/css/bootstrap.css', 
        '//assets' . $ver . '/css/bootstrap-dialog.min.css',          
        '//assets' . $ver . '/css/perfect-scrollbar.css',          
        '//assets' . $ver . '/css/panel.css',
        '//assets' . $ver . '/css/subnav.css',         
        '//assets' . $ver . '/css/preloader.css',
        '//assets' . $ver . '/css/bootstrap-datetimepicker.min.css',
        '//assets' . $ver . '/css/font-awesome.css', 
        '//assets' . $ver . '/css/ionicons.min.css',
        '//assets' . $ver . '/css/dropzone.css',
        '//assets' . $ver . '/css/beaufort.css',
        '//assets' . $ver . '/css/spiegel.css',        
        '//assets' . $ver . '/css/common.css',    
        '//assets' . $ver . '/css/buttons.css',        
        '//assets' . $ver . '/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css',
        
        //'//assets' . $ver . '/css/summernote.css',
        //'//assets' . $ver . '/css/responsive.css'
    ),

    'css.guides' => [
        '//assets' . $ver . '/css/bootstrap.css', 
        '//assets' . $ver . '/css/bootstrap-dialog.min.css',          
        '//assets' . $ver . '/css/perfect-scrollbar.css',          
        '//assets' . $ver . '/css/ionicons.min.css', 
        '//assets' . $ver . '/css/font-awesome.css',
        '//assets' . $ver . '/css/panel.css',         
        '//assets' . $ver . '/css/swipebox.css',
        '//assets' . $ver . '/css/guides/app.css',
        '//assets' . $ver . '/css/common.css',
        '//assets' . $ver . '/css/buttons.css',        
        '//assets' . $ver . '/css/guides/theme.css',        
        '//assets' . $ver . '/css/beaufort.css',
        '//assets' . $ver . '/css/spiegel.css',        
        //'//assets' . $ver . '/css/proximanova.css',
    ],    

    'css.heroes' => [
        '//assets' . $ver . '/css/bootstrap.custom.css',
        '//assets' . $ver . '/css/bootstrap-dialog.min.css',          
        '//assets' . $ver . '/css/ionicons.min.css',
        '//assets' . $ver . '/css/font-awesome.css',
        '//assets' . $ver . '/css/slick.css',
        '//assets' . $ver . '/css/jquery.fancybox.css',
        '//assets' . $ver . '/css/infinite-odyssey.css',
        '//assets' . $ver . '/css/swipebox.css',
        '//assets' . $ver . '/css/panel.css',
        '//assets' . $ver . '/css/buttons.css',
        '//assets' . $ver . '/css/common.css',
        '//assets' . $ver . '/css/beaufort.css',
        '//assets' . $ver . '/css/spiegel.css',        
        '//assets' . $ver . '/css/perfect-scrollbar.css',         
    ],  

    'css.heroe' => [
        '//assets' . $ver . '/css/screen.css',  
        '//assets' . $ver . '/css/beaufort.css',
        '//assets' . $ver . '/css/perfect-scrollbar.css',  
    ], 

    'css.inferno' => [
        '//assets' . $ver . '/css/bootstrap.custom.css',         
        '//assets' . $ver . '/css/bootstrap-dialog.min.css',
        '//assets' . $ver . '/css/ionicons.min.css', 
        '//assets' . $ver . '/css/font-awesome.css',         
        '//assets' . $ver . '/css/panel.css',
        '//assets' . $ver . '/css/buttons.css',        
        '//assets' . $ver . '/css/common.css',
        //'//assets' . $ver . '/css/screen.css',
        '//assets' . $ver . '/css/beaufort.css',
        '//assets' . $ver . '/css/spiegel.css',        
        '//assets' . $ver . '/css/slick.css',
        '//assets' . $ver . '/css/jquery.fancybox.css',
        '//assets' . $ver . '/css/infinite-odyssey.css',
        '//assets' . $ver . '/css/perfect-scrollbar.css',          
    ],    

    'css.items' => [
        '//assets' . $ver . '/css/bootstrap.custom.css',         
        '//assets' . $ver . '/css/bootstrap-dialog.min.css',
        '//assets' . $ver . '/css/ionicons.min.css', 
        '//assets' . $ver . '/css/font-awesome.css',  
        '//assets' . $ver . '/css/panel.css',       
        '//assets' . $ver . '/css/common.css',
        '//assets' . $ver . '/css/buttons.css',        
        '//assets' . $ver . '/css/items.custom.css',
        '//assets' . $ver . '/css/beaufort.css',
        '//assets' . $ver . '/css/spiegel.css',        
        '//assets' . $ver . '/css/perfect-scrollbar.css',
    ]                  
];

if(!empty($_GET['g']) AND substr($_GET['g'],0,2) == 'js'){
    $assets[$_GET['g']][]= '//assets' . $ver . '/js/ping.js' ;
}

return $assets;
