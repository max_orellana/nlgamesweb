
$(window).on('resize', function(){
  //return location.reload();  
  $('.ps-wrapper').perfectScrollbar('update');
});


$(document).on('click','#admin-menu-btn',function(){
  console.log("aaaaaaaa");
  if($("#admin-menu").is(":visible")){
    $(this).find("i").first().removeClass("fa-chevron-up").addClass("fa-chevron-down");
  } else {
    $(this).find("i").first().removeClass("fa-chevron-down").addClass("fa-chevron-up");
  }
  $("#admin-menu").slideToggle(300);
});

$(document).on('click','.btn-tag-included',function(){
  tags_exclude([$(this).text()],this);
});

$(document).on('click','.btn-tag-excluded',function(){
  tags_include([$(this).text()],this);
});

$(document).on('click','.btn-tags-add',function(){
  tags_add();
});

/* Tags */

function tags_add(){
  var tags_str = $.trim($( "#tags-add" ).val());

  if(tags_str == ""){
    alert("No string!");
    return false;
  }

  var tags = tags_str.split(",");

  for(var i in tags){
    tags[i] = $.trim(tags[i]);
  }
  $( "#tags-add" ).val( "" );
  tags_include(tags);
}

function tags(){
  var id = $( 'input[name="id"]' ).val();
  var type = $( 'input[name="type"]' ).val();
  $('.ps-body').addClass("loading");
  $.ajax({
    type:"GET",
    url: "/admin/tags/" + type + '/' + id,
    success : function(json){
      if(json){
        if(json.included){
          $(json.included).each(function(i,tag){
            $( ".tags-included" ).append( '<a href="#" class="label label-success label-badge btn-tag-included">' + tag + '</a>' );
          });
        }
        if(json.excluded){
          $(json.excluded).each(function(i,tag){
            $( ".tags-excluded" ).append( '<a href="#" class="label label-info label-badge btn-tag-excluded">' + tag + '</a>' );
          });
        }
      }
      $('.ps-body').removeClass("loading");
    }
  });
}

function tags_exclude(tags,btn){
  var id = $( 'input[name="id"]' ).val();
  var type = $( 'input[name="type"]' ).val();
  $('.ps-body').addClass("loading");
  $.ajax({
    type:"POST",
    url: "/admin/tags/relation/remove/" + type + '/' + id,
    data: {
      tags : tags
    },
    success : function(json){
      if(json){
        if(json.excluded){
          $(json.excluded).each(function(i,tag){
            $( ".tags-excluded" ).append( '<a href="#" class="label label-info label-badge btn-tag-excluded">' + tag + '</a>' );
          });
        }
        if($(btn).length){
          $(btn).remove();
        }
      }
      $('.ps-body').removeClass("loading");
    }
  });
}

function tags_include(tags,btn){
  var id = $( 'input[name="id"]' ).val();
  var type = $( 'input[name="type"]' ).val();
  $('.ps-body').addClass("loading");
  $.ajax({
    type:"POST",
    url: "/admin/tags/relation/add/" + type + '/' + id,
    data: {
      tags : tags
    },
    success : function(json){
      if(json){
        if(json.included){
          $(json.included).each(function(i,tag){
          $( ".tags-included" ).append( '<a href="#" class="label label-success label-badge btn-tag-included">' + tag + '</a>' );
          });
        }
        if($(btn).length){
          $(btn).remove();
        }
      }
      $('.ps-body').removeClass("loading");
    }
  });
}

function sendImage(file, editor, welEditable) {
    data = new FormData();
    data.append("file", file);
    $('.summernote-progress').removeClass('hide').hide().fadeIn();
    $.ajax({
        data: data,
        type: "POST",
        xhr: function() {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) myXhr.upload.addEventListener('progress',progressHandlingFunction, false);
            return myXhr;
        },        
        url: "/admin/files/upload",
        cache: false,
        contentType: false,
        processData: false,
        success: function(url) {
            $('.summernote-progress').fadeOut();
            editor.insertImage(welEditable, url);
        }
    });
}

function progressHandlingFunction(e){
    if(e.lengthComputable){
        var perc = Math.floor((e.loaded/e.total)*100);
        $('.progress-bar').attr({"aria-valuenow":perc}).width(perc+'%');
        // reset progress on complete
        if (e.loaded == e.total) {
            $('.progress-bar').attr('aria-valuenow','0.0');
        }
    }
}

function closeSearch(){
  $('#search-results')
  .slideUp()
  .html('');
}

$(function(){

  $('.ps-wrapper').perfectScrollbar();
  $("a,span,button").tooltip({container:'body'});

	$('#search').ajaxify({
		url:'/admin/search',
		onexit: function(){
      closeSearch();
		},
		focus: false,
		success:function(json){
			$('#search-results').html('');
			var str='';
			if(json.rows.length){
				var j=0;
				str+='<table class="table">';

				$(json.rows).each(function(i,row){
					if( ! j) {
						str+='<tr>';
						for(var i in row) str+='<th>' + i + '</th>';
						str+='</tr>';
					}
					str+='<tr>';
					for(var i in row) str+='<td><a href="/admin/' + json.slug + '/' + row.id + '">' + row[i] + '</a></td>';
					str+='</tr>';
					j++;
				});

				str+='</table>';

			} else {
				str+= '<strong>' + json.search + '</strong>: <em>' + json.no_match + '</em>';
			}

			$('#search-results')
				.html(str+'<a class="search-close-btn" href="javascript:closeSearch()"><i class="fa fa-times"></i></a>')
				.removeClass('hide')
				.hide()
				.slideDown();

		}
	});

	$('.ps-body').click(function(e){
		if( ! $(e.target).parent().hasClass('lang-button'))	$('.lang-selector').hide();
	});

	$('.lang-button').click(function(e){
		e.preventDefault();
		$('.lang-selector').removeClass('hide').hide().toggle();
		return false;
	});

	if($('.summernote').length){
	    $('.summernote').summernote({
	      	height: 450,   
	      	minHeight: null,
	      	maxHeight: null,
			styleWithSpan: false,
			toolbar: [
			    ['style', ['style']],
			    ['font', ['bold', 'italic', 'underline']],
			    ['color', ['color']],
			    ['para', ['ul', 'ol', 'paragraph']],
			    // ['height', ['height']],
			    ['table', ['table']],
			    ['insert', ['link', 'picture', 'hr']],
			    ['view', ['codeview']],
			    // ['help', ['help']]
		    ],
      	onImageUpload: function(files, editor, welEditable) {
        	sendImage(files[0], editor, welEditable);
      	},
        onpaste: function (e) {
          var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
          e.preventDefault();
          document.execCommand('insertText', false, bufferText);
        }
	    });
	}

	if($('.chosen').length){
	    $('.chosen').each(function(){
	      var width = "100%";
	      if($(this).data('width')){
	        width = $(this).data('width')+'px';
	      }
	      $(this).chosen({width:width}); 
	    });
	}
	
	if($('.datetimepicker').length){
	    $('.datetimepicker').datetimepicker();
	}

	if($('.dropzone').length){

		Dropzone.options.myAwesomeDropzone = false;
		Dropzone.autoDiscover = false;


	    $('.dropzone').each(function(){

	      if( ! $(this).hasClass('dz-clickable')){

	        var target = $(this).data('target');
	        var domain = $(this).data('domain');
	        var index = $(this).data('index');
	        var url = $(this).data('url')||"/api/upload";
	        var id = $(this).data('id');
	        var message = $(this).data('message')||Dropzone.prototype.defaultOptions.dictDefaultMessage;

	        var myDropzone = new Dropzone(this, { 
	          url: url,
	          dictDefaultMessage:message,
	          maxFilesize: 2,
	          addRemoveLinks:true,
	          dictCancelUpload:"Cancelar",
	          dictCancelUploadConfirmation:"¿Seguro desea cancelar?",
	          dictRemoveFile:"Eliminar",
	          parallelUploads: 1,
	          maxFiles: $(this).data('max')
	        });
	        
	        /* DZ upload handlers */

	        myDropzone.on("sending", function(file, xhr, formData) {
	          formData.append("id", id );
	          formData.append("target", target );
	          formData.append("domain", domain );
	        });

	        myDropzone.on("success", function(file, xhr) {
	          $(file.previewElement).data('id',xhr.id);
	          $(file.previewElement).data('domain',domain);          
	          if($('#'+target).find('input[name="id"]').val()==0){
	            $('#'+target).find('input[name="id"]').val(xhr.post_id);
	          }
	        });

	        myDropzone.on("removedfile", function(file, xhr) {
	          $.ajax({
	            type: 'post',
	            url : "/admin/files/remove",
	            data : {
	              id : $(file.previewElement).data('id'),
	              domain : $(file.previewElement).data('domain')
	            }
	          });
	        });

	        if($(this).data('hide')){
	          $(this).hide();
	        }

	        $(this).sortable({
	          update: function (event, ui) {

	            var sorted=[];
	            var id = "";
	            var domain = "";

	            $(this).find('.dz-preview').each(function(i,item){
	              if(id==""){
	                id = $(item).data('parent-id');
	              }
	              if(domain==""){
	                domain = $(item).data('domain');
	              }
	              sorted[i] = $(item).data('id');
	            });

	            $.ajax({
	              type:"POST",
	              url: "/admin/files/order",
	              data: {
	                id : id,
	                sorted : sorted,
	                domain : domain
	              },
	              success : function(json){
	                success('');
	              }
	            });
	          }
	        });

	        if(index){
	          setTimeout(function(){
	            $.ajax({
	              type: 'get',
	              url : index,
	              success : function(json){
	                $(json).each(function(i,item){
	                  var mockFile = { size: item.file_size, name: item.name };

	                  myDropzone.emit("addedfile", mockFile);
	                  myDropzone.emit("thumbnail", mockFile, '/upload/' + domain + '/300x200/' + item.name);
	                  
	                  $(mockFile.previewElement).data('id',item.id);
	                  $(mockFile.previewElement).data('domain',domain);
	                  $(mockFile.previewElement).find('.dz-progress').hide();
	                });
	              }
	            });
	          },100);
	        }
	      }
	    });
	}

  /* datetime picker */
  
  if($('.datetimepicker').length){
    $('.datetimepicker').each(function(){
      $(this).datetimepicker();
    });
  }

	/* toggleables */

	$('.toggle').click(function(e){e.preventDefault();
  $i = $(this).find('i');
  $.ajax({
  type: 'post',
  url: '/toggle',
  data : {
  'id'  : $i.data('id'),
  'model' : $i.data('model'),
  'field' : $i.data('field'),
  'value'  : $i.data('value'),
  'icons'  : $i.data('icons'),
  'rowstatus'  : $i.data('row-status')},
  success: function(json){
	if(json.rowstatus.length){
	$i.closest('tr').removeClass().addClass( json.rowstatus );}
	$i.data('value',json.value);
  $i.hide().removeClass().addClass('fa fa-md ' + (json.value ? 'fa-check-circle' : 'fa-exclamation-circle')).fadeIn('fast');}
  });
	});

  $('.networks div').hover(function(e){
  $(this).animate({'left':'0px'},100);
  $(this).find('img').animate({'left':'18px'},100);},function(e){
  $(this).animate({'left':'-27px'},100);
  $(this).find('img').animate({'left':'30px'},100);});

  $('.totop').click(function(e){
  $('.ps-body').animate({
  scrollTop: 0
  }, 300);
  });    

  $('.ps-body').click(function(e){
  if( ! $(e.target).parent().hasClass('lang-button')){
  $('.lang-selector').hide();}
  });

  $('a[href*=".jpg"], a[href*="jpeg"], a[href*=".png"], a[href*=".bmp"]').addClass("lightview").attr("data-lightview-group", "gallery").attr("data-lightview-group-options", "controls: 'thumbnails'");
  
  $('img#wpstats').remove();
  
  $('.active-canvas').click(function(){
  $(this).toggleClass('fi-indent-more').toggleClass('fi-indent-less').toggleClass('active-canvas-on');
  $('.all-content').toggleClass('on-canvas');
  });

  //**** Slide Panel Toggle ***//
  $(".slide-panel-btn").click( function(){
  $(".slide-panel-btn").toggleClass('active');
  $(".slide-panel").toggleClass('active');
  });
  
  //**** Slide Panel Toggle ***//
  $(".toggle-menu").click( function(){
  $("body").toggleClass('min-nav');
  });

  $("body").click(function () {
  $(".message.drop-list:visible").stop(true, true).fadeOut("fast");
  });
  
  //**** Notification Dropdown ***//
  $('.notification-list.dropdown > a').click(function(e) {
  $(".notification.drop-list").stop(true, true).fadeIn("fast");
  $(".message.drop-list:visible").stop(true, true).fadeOut("fast");
  $(".activity.drop-list:visible").stop(true, true).fadeOut("fast");
  e.stopPropagation();
  });

  //**** Profile Dropdown ***//
  $('.profile.dropdown > a').click(function(e) {
  $(".profile.drop-list").stop(true, true).fadeIn("fast");
  e.stopPropagation();
  });

  $('body').click(function(e){
    $('.drop-list').hide();
  });

	/*setTimeout(live,1000);
	setInterval(function(){
    if($('.notification-list').is(':hidden')) live();
  },10000);*/

});


/*
function live(){
  $messages = $('.message-list');
  $notification = $('.notification-list');
  $.ajax({
  type: 'get',
  url: '/live',
  cache: false,
  success: function(json){
  // audio notifications
  var message_count = $messages.find('.drop-list ul').children().length;
  var notification_count = $notification.find('.drop-list ul').children().length;
  var message_count2 = json.messages.data ? json.messages.data.length : 0;
  var notification_count2 = json.notification.data ? json.notification.data.length : 0;
  if( $messages.find('.drop-list ul').html() != undefined && message_count < message_count2){
  var audio = new Audio('/assets/009/audio/notification.ogg');
  audio.play();
  }
  if($notification.find('.drop-list ul').html() != undefined && notification_count < notification_count2){
  var audio2 = new Audio('/assets/009/audio/inbox.ogg');
  audio2.play();
  }
  
  // visual notifications 
  $messages.find('.drop-list').html('');
  $messages.find('span').remove();
  $messages.find('.drop-list').append('<span>' + json.messages.title + '</span>');
  $messages.find('.drop-list').html('<ul></ul>');
  $messages.find('.drop-list ul').after('<a href="/inbox">' + json.read_all + '</a>');

  if($(json.messages.data).length) {
  $('.inbox-badge').html('<span class="badge green">' + $(json.messages.data).length + '</span>');
  }
 
  $(json.messages.data).each(function(i,row){
  $messages.find('.drop-list ul').append('<li><a href="' + row.url + '" title=""><span><img src="' + row.avatar + '"></span><strong>' + row.sender + '</strong> &nbsp;' + row.title + '&nbsp;<h6>' + row.time + '</h6></a></li>');
  });

  $notification.find('.drop-list').html('');
  $notification.find('span').remove();
  $notification.find('.drop-list').append('<span>' + json.notification.title + '</span>');
  $notification.find('.drop-list').html('<ul></ul>');
  //$notification.find('.drop-list ul').after('<a href="/admin/tickets">' + json.read_all + '</a>');

  if($(json.notification.data).length) {
  //$notification.find('.ion-alert-circled').before('<span class="green">' + $(json.notification.data).length + '</span>');
  if($notification.is(':hidden')) $notification.hide().fadeIn(2000);
  }

  $(json.notification.data).each(function(i,row){
  $notification.find('.drop-list ul').append('<li><i class="ion-android-arrow-dropdown-circle"></i>&nbsp;<strong>' + row.title + '</strong><div class="hide text">' + row.caption + (row.url ? '&nbsp;<a href="' + row.url + '">' + json.click_here + '</a>' : '') + '</div><img class="profile-img" src="' + row.avatar + '" title="' + row.login + '"></li>');
  });

  $('.notification.drop-list ul li').click(function(e){
  $text = $(this).find('.text');
  if($text.length){
  if($text.is(':hidden')){
  $text.removeClass('hide').hide().slideDown('fast');
  } else {
  $text.slideUp('fast');
  }
  }
  e.stopPropagation();
  return false;
  });                
  }
  })
}
*/