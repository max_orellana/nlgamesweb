
$(function(){

  $('.chart').easyPieChart({
    lineWidth: 16,
    lineCap:"square",
    scaleColor:false,
    trackColor:"#333",
    barColor: "#8bd31c",
    easing: 'easeOutBounce',
    onStep: function(from, to, percent) {
      $(this.el).find('.percent').text(Math.round(percent));
    }
  });
 
  var chart = window.chart = $('.chart').data('easyPieChart');

  //$('[data-toggle="popover"]').popover({trigger:"hover"});
  //$("a,span,button").not('[data-toggle="popover"]').tooltip({container: 'body'});
});