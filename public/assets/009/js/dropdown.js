Dropdowns = new Class( {
	
	_dropdowns: new Array()
	
	,initialize: function() {
		$$( '.dropdown' ).each( function( dropdownElement ) {
			var dropdownClass = new Dropdown( dropdownElement );
			dropdownClass.addEvent( 'open', function() {
				this._closeDropdowns( dropdownClass );
			}.bind( this ) );
			this._dropdowns.push( dropdownClass );
		}.bind( this ) );
	}
	
	,_closeDropdowns: function( dropdown ) {
		this._dropdowns.each( function( dropen ) {
			if( dropen !== dropdown ) {
				// Es ist nicht das aktuelle Dropdown, also einfahren
				dropen.close();
			}
		} );
	}
	
	,get: function( search ) {
		search = document.id( search );
		var found = null;
		this._dropdowns.each( function( dropdown ) {
			if( dropdown._dropdown === search ) {
				found = dropdown
			}
		} );
		return found;
	}
} );

Dropdowns.instance = null;
Dropdowns.getInstance = function() {
	if( Dropdowns.instance === null ) {
		Dropdowns.instance = new Dropdowns();
	}
	return Dropdowns.instance;
};

Dropdown = new Class( {
	Implements: Events
	/**
	 * Höhe des Dropdowns
	 * 
	 * @var int
	 */
	,_height: null
	/**
	 * Dropdown Element
	 * 
	 * @var Element
	 */
	,_dropdown: null
	/**
	 * Animation zum auf und zuklappen
	 * 
	 * @var: Fx.Tween
	 */
	,_fx: null
	/**
	 * Falls Scroll aktiv ist, stehen hier die Infos drin
	 * 
	 * @var Object
	 */
	,_scroll: null
	
	/**
	 * Konstrukor
	 * 
	 * @param Element dropdown
	 * @param Element label
	 * @return Dropdown
	 */
	,initialize: function( dropdown, label ) {
		
		// Dropdown Element setzen
		this._dropdown = dropdown;
		if(this._dropdown.getElements( 'ul' ).length == 0 ){
			return false;
		}
		// Überprüft, ob das Dropdown eine Scrollbar bekommen soll
		this._checkScroll();
		
		// Höhe des Dropdowns: Anzahl Elemente * Deren Höhe
		this._height = this._dropdown.getElements( 'li' ).length * this._getLiHeight() + this._getBorderHeight();
		
		// IE7 Fix: UL mit Höhe 0 und overflow hidden funktioniert nicht, deswegen muss
		// das UL display none und block beim IE bekommen
		if( Browser.ie7 ) {
			if( !this._dropdown.hasClass( 'open' ) ) {
				this._dropdown.getElement( '.scrollContainer' ).addClass( 'hide' );
			}
		} else {
			// Animation
			this._fx = new Fx.Tween( this._dropdown.getElement( '.scrollContainer' ), { duration: 125 } );
		}
		
		// Fährt das Dropdown auf und zu
		this.initLabel( label || null );
		dropdown.getElement( 'div' ).addEvent( 'click', function() {
			if( this._dropdown.hasClass( 'open' ) ) {
				this.close();
			} else {
				this.open();
			}
		}.bind( this ) );
		
		// Anklicken eines Wertes
		dropdown.getElements( 'li[class!=inactive]' ).each( function( element ) {
			if( !element.hasClass( 'inactive' ) ) {
				element.addEvent( 'click', function() {
					// Kein input, kein gültiges Eingabefeld
					if( !element.getElement( 'input' ) ) {
						return;
					}
					
					// Dropdown Schließen
					this._dropdown.getElement( 'div' ).fireEvent( 'click' );
					
					// Abbrechen, wenn sich der ausgewählte Wert nicht geändert hat
					if( this._dropdown.getLast( 'input' ).get( 'value' ) === element.getElement( 'input' ).get( 'value' ) ) {
						return;
					}
					
					dropdown.getElements( 'li' ).each( function( element ) {
						element.removeClass( 'active' );
					} );
					element.addClass( 'active' );
					this._dropdown.getElement( 'div' ).set( 'html', element.get( 'html' ) );
					this._dropdown.getLast( 'input' ).set( 'value', element.getElement( 'input' ).get( 'value' ) );
					this._dropdown.getLast( 'input' ).fireEvent( 'change' );
					this.fireEvent( 'change', element.getElement( 'input' ).get( 'value' ) );
				}.bind( this ) );
			}
		}.bind( this ) );
	}
	
	,initLabel: function( label ) {
		if( label ) {
			label.addEvent( 'click', function() {
				this._dropdown.getElement( 'div' ).fireEvent( 'click' );
			}.bind( this ) );
		}
	}
	
	,close: function() {
		if( !this._dropdown.hasClass( 'open' ) ) {
			// Dropdown ist nicht offen
			return;
		}
		// HTML-Klasse entfernen
		this._dropdown.removeClass( 'open' );
                this._dropdown.removeClass( 'z-index' );

		// Animation ausführen
		if( Browser.ie7 ) {
			this._dropdown.getElement( '.scrollContainer' ).addClass( 'hide' );
		} else {
			this._fx.start( 'height', this._getHeightForAnimation(), 0 );
		}
		
		this.fireEvent( 'close' );
	}
	
	,open: function() {
		// HTML-Klasse hinzufügen
		this._dropdown.addClass( 'open' );
                this._dropdown.addClass( 'z-index' );
		
		// Animation ausführen
		if( Browser.ie7 ) {
			this._dropdown.getElement( '.scrollContainer' ).removeClass( 'hide' );
			this._dropdown.getElement( '.scrollContainer' ).setStyle( 'height', this._height );
		} else {
			if (this._height == 0) { //this may happen if the dropdown is initally hidden
				this._height = this._dropdown.getElements( 'li' ).length * this._getLiHeight() + this._getBorderHeight();
				}
			this._fx.start( 'height', this._getHeightForAnimation(), this._height );
		}
		
		this.fireEvent( 'open' );
	}
	
	,_getHeightForAnimation: function() {
		if( !Browser.ie7 ) {
			// Animation anhalten und aktuelle Höhe merken,
			// wird nur bei nicht IE7 Browsern benötigt, da bei IE7
			// die Animation nicht möglich ist
			this._fx.cancel();
			return this._dropdown.getElement( 'ul' ).getHeight();
		}
	}
	
	/**
	 * Bindet den Scroller in das Dropdown an, wenn angegeben
	 * 
	 * @return void
	 */
	,_checkScroll: function() {
		if( this._dropdown.getCssParam( 'scroll' ) ) {
			var scrollItems = this._dropdown.getCssParam( 'scroll' ).toInt();
			if( this._dropdown.getElements( 'li' ).length > scrollItems ) {
				this._dropdown.addClass( 'scroll' );
				var maxHeight = this._getLiHeight() * scrollItems + this._getBorderHeight();
				this._scroll = {
					scrollItems: scrollItems
					,maxHeight: maxHeight
					,elementCount: this._dropdown.getElements( 'li' ).length
				};
				// Scroll-Steps sind Elemenet ingesamt - sichtbare Elemente
				this._scroll.scrollSteps = ( this._scroll.elementCount - this._scroll.scrollItems );
				// Slider Elemente + Funktion erstellen
				this._createSlider();
			}
		}
	}
	
	,_getLiHeight: function() {
		var li = this._dropdown.getElement( 'li' );
		var height = 0;
		if (li != null) {
			height = li.measure( function() { return this.getHeight(); } );
			// adding margins to height
			height += li.getStyle( 'margin-top' ).toInt() - li.getStyle( 'margin-bottom' ).toInt();			
		}
		return height;
	}
	
	,_getBorderHeight: function() {
		var borderTop = this._dropdown.getElementById('scrollContainerTop');
		var borderBottom = this._dropdown.getElementById('scrollContainerBottom');
		var height = 0;
		
		if ( borderTop ) {
			height += borderTop.getStyle('height').toInt();
		}
		
		if ( borderBottom ) {
			height += borderBottom.getStyle('height').toInt();
		}
		return height;
	}
	
	,_createSlider: function() {
		// Dropdown darf nur bestimmte Höhe haben
		var scrollContainer = this._dropdown.getElement( '.scrollContainer' );
		
		scrollContainer.setStyle( 'max-height', this._scroll.maxHeight );
		
		// Scroll Bar erstellen
		this._scroll.slider = new Element( 'div', {
			'class': 'slider scrollSlider'
			,styles: {
				height: this._scroll.maxHeight
			}
		} ).inject( scrollContainer );
		this._scroll.sliderBar = new Element( 'div', {
			'class': 'bar'
		} ).inject( this._scroll.slider );
		
		var ulHeight = this._dropdown.getElement( 'ul' ).getHeight().toInt();
		this._scroll.maxScroll = this._scroll.maxHeight - ulHeight;
		
		var slider = new Slider( this._scroll.slider, this._scroll.sliderBar, {
			mode: 'vertical'
			,range: [ 0, this._scroll.scrollSteps ]
			,steps: this._scroll.scrollSteps
			,wheel: true
			,onChange: function( value ) {
				var marginTop = 0;
				// Element vertikal verschieben
				if(value === 0) {
					marginTop = value;
				} else {
					marginTop = value * this._getLiHeight() * -1 + this._getBorderHeight();
				}
				
				this._dropdown.getElement( 'ul' ).setStyle( 'margin-top', marginTop );
				this._scroll.currentStep = value;
			}.bind( this )
		} );
		
		
		this._dropdown.getElement( 'ul' ).addEvent( 'mousewheel', function( e ) {
			e.stop();
			this._scroll.currentStep -= e.wheel;
			if( this._scroll.currentStep < 0 ) {
				this._scroll.currentStep = 0;
			}
			if( this._scroll.scrollSteps < this._scroll.currentStep ) {
				this._scroll.currentStep = this._scroll.scrollSteps;
			}
			slider.set( this._scroll.currentStep );
		}.bind( this ) );
	}
} );

$(".dropdown").each(function(e){
	var t=new Dropdown(e);
	t.addEvent("open",function(){
		this._closeDropdowns(t)
	}.bind(this)),
	this._dropdowns.push(t)}.bind(this))},_closeDropdowns:function(e){this._dropdowns.each(function(t){t!==e&&t.close()})},get:function(e){e=document.id(e);var t=null;return this._dropdowns.each(function(n){n._dropdown===e&&(t=n)}),t}}),Dropdowns.instance=null,Dropdowns.getInstance=function(){return null===Dropdowns.instance&&(Dropdowns.instance=new Dropdowns),Dropdowns.instance},Dropdown=new Class({Implements:Events,_height:null,_dropdown:null,_fx:null,_scroll:null,initialize:function(e,t){return this._dropdown=e,0==this._dropdown.getElements("ul").length?!1:(this._checkScroll(),this._height=this._dropdown.getElements("li").length*this._getLiHeight()+this._getBorderHeight(),Browser.ie7?this._dropdown.hasClass("open")||this._dropdown.getElement(".scrollContainer").addClass("hide"):this._fx=new Fx.Tween(this._dropdown.getElement(".scrollContainer"),{duration:125}),this.initLabel(t||null),e.getElement("div").addEvent("click",function(){this._dropdown.hasClass("open")?this.close():this.open()}.bind(this)),void e.getElements("li[class!=inactive]").each(function(t){t.hasClass("inactive")||t.addEvent("click",function(){t.getElement("input")&&(this._dropdown.getElement("div").fireEvent("click"),this._dropdown.getLast("input").get("value")!==t.getElement("input").get("value")&&(e.getElements("li").each(function(e){e.removeClass("active")}),t.addClass("active"),this._dropdown.getElement("div").set("html",t.get("html")),this._dropdown.getLast("input").set("value",t.getElement("input").get("value")),this._dropdown.getLast("input").fireEvent("change"),this.fireEvent("change",t.getElement("input").get("value"))))}.bind(this))}.bind(this)))},initLabel:function(e){e&&e.addEvent("click",function(){this._dropdown.getElement("div").fireEvent("click")}.bind(this))},close:function(){this._dropdown.hasClass("open")&&(this._dropdown.removeClass("open"),this._dropdown.removeClass("z-index"),Browser.ie7?this._dropdown.getElement(".scrollContainer").addClass("hide"):this._fx.start("height",this._getHeightForAnimation(),0),this.fireEvent("close"))},open:function(){this._dropdown.addClass("open"),this._dropdown.addClass("z-index"),Browser.ie7?(this._dropdown.getElement(".scrollContainer").removeClass("hide"),this._dropdown.getElement(".scrollContainer").setStyle("height",this._height)):(0==this._height&&(this._height=this._dropdown.getElements("li").length*this._getLiHeight()+this._getBorderHeight()),this._fx.start("height",this._getHeightForAnimation(),this._height)),this.fireEvent("open")},_getHeightForAnimation:function(){return Browser.ie7?void 0:(this._fx.cancel(),this._dropdown.getElement("ul").getHeight())},_checkScroll:function(){if(this._dropdown.getCssParam("scroll")){var e=this._dropdown.getCssParam("scroll").toInt();if(this._dropdown.getElements("li").length>e){this._dropdown.addClass("scroll");var t=this._getLiHeight()*e+this._getBorderHeight();this._scroll={scrollItems:e,maxHeight:t,elementCount:this._dropdown.getElements("li").length},this._scroll.scrollSteps=this._scroll.elementCount-this._scroll.scrollItems,this._createSlider()}}},_getLiHeight:function(){var e=this._dropdown.getElement("li"),t=0;return null!=e&&(t=e.measure(function(){return this.getHeight()}),t+=e.getStyle("margin-top").toInt()-e.getStyle("margin-bottom").toInt()),t},_getBorderHeight:function(){var e=this._dropdown.getElementById("scrollContainerTop"),t=this._dropdown.getElementById("scrollContainerBottom"),n=0;return e&&(n+=e.getStyle("height").toInt()),t&&(n+=t.getStyle("height").toInt()),n},_createSlider:function(){var e=this._dropdown.getElement(".scrollContainer");e.setStyle("max-height",this._scroll.maxHeight),this._scroll.slider=new Element("div",{"class":"slider scrollSlider",styles:{height:this._scroll.maxHeight}}).inject(e),this._scroll.sliderBar=new Element("div",{"class":"bar"}).inject(this._scroll.slider);var t=this._dropdown.getElement("ul").getHeight().toInt();this._scroll.maxScroll=this._scroll.maxHeight-t;var n=new Slider(this._scroll.slider,this._scroll.sliderBar,{mode:"vertical",range:[0,this._scroll.scrollSteps],steps:this._scroll.scrollSteps,wheel:!0,onChange:function(e){var t=0;t=0===e?e:e*this._getLiHeight()*-1+this._getBorderHeight(),this._dropdown.getElement("ul").setStyle("margin-top",t),this._scroll.currentStep=e}.bind(this)});this._dropdown.getElement("ul").addEvent("mousewheel",function(e){e.stop(),this._scroll.currentStep-=e.wheel,this._scroll.currentStep<0&&(this._scroll.currentStep=0),this._scroll.scrollSteps<this._scroll.currentStep&&(this._scroll.currentStep=this._scroll.scrollSteps),n.set(this._scroll.currentStep)}.bind(this))}})