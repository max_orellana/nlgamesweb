$(document).ready(function(){

  $('body').click(function(e){
    if( ! $(e.target).parent().hasClass('lang-button')){
      $('.lang-selector').hide();
    }
  });

  $('.lang-button').click(function(e){
    e.preventDefault();
    $('.lang-selector').removeClass('hide').hide().toggle();
    return false;
  });
});