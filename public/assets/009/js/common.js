(function( $ ){
  var methods = {
    init : function(options) {
      $(this).keyup(function(e){
        if($(this).data('minlength') && $(this).data('minlength') > $(this).val().length ) {
          if(options.onexit) options.onexit.call(this);
          return false;
        }
        clearTimeout($.data(this, 'timer'));
        $.data(this, 'timer', setTimeout(function() {

          var data = {};
          var $this = $(e.target);
          data[$this.attr('name')] = $this.val();
          if($this.data('options')) data['options'] = $this.data('options');
          $.ajax({
            method: 'get',
            url: options.url,
            data: data,
            success:function(json){
              options.success.call(this,json);
            }
          });
        }, 1000));
        return false;
      });

      if(options.focus){
        $this.focus();
      }

    }
  };

  $.fn.ajaxify = function(methodOrOptions) {
    if ( methods[methodOrOptions] ) {
        return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
        // Default to "init"
        return methods.init.apply( this, arguments );
    } else {
        $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.tooltip' );
    }    
  };
})( jQuery );


$(document).ajaxError(function myErrorHandler(event, xhr, ajaxOptions, thrownError) {
  // notification("There was an ajax error!");
  console.log("There was an ajax error!");
  $('body').removeClass('loading');
});


$(document).on('click','.expanding-box .expanding-handle',function(e){
  $div = $(this).parent().find('div').first();
  if($div.is(':hidden')) $div.slideDown();
  else $div.slideUp();
  //return false;
});

$(function(){
  // internal section seek
  $('.goscroll').click(function(){
    $('.ps-body').scrollTop($($(this).attr('href')).offset().top + $('.ps-body').scrollTop() - 44).perfectScrollbar('update');
    return false;
  });
});

var icons = {
  'info' : "ios-help",
  'success' : "checkmark-round",
  'warning' : "android-warning",
  'danger' : "nuclear"
}


function ping(){

  var $messages = $('.message-list');
  var $notification = $('.notification-list');

  $.ajax({
    type: 'get',
    url: '/ping',
    cache: false,
    success: function(json){
      var $filter = [];
      var $noti = $notification.html();

      if(json.unread) {
          $('.inbox-unread').html(' (' + json.unread + ')');
      }
      if($(json.messages.data).length) {
          $(json.messages.data).each(function(i,row){
              if($noti.indexOf(row.title)==-1) {
                  notification(row.title, row.avatar, row.sender);
              }
          });
      }

      if($(json.notification.data).length) {
          $(json.notification.data).each(function(i,row){
              $noti = $notification.html();
              if($noti && $noti.indexOf(row.title)==-1) {
                  $filter.push(row);
              }
          });
      }

      if($notification.is(':hidden')) $notification.delay(1000).removeClass('hide').hide().fadeIn();

      if($filter.length){

          $notification.find('.drop-list').html('');
          $notification.find('span').remove();
          $notification.find('.drop-list').append('<span>' + json.notification.title + '</span>');
          $notification.find('.drop-list').html('<ul></ul>');

          $($filter).each(function(i,row){
              $notification.find('.drop-list ul').append('<li><i class="ion-android-arrow-dropdown-circle"></i>&nbsp;<strong>' + row.title + '</strong><div class="hide text">' + row.caption + (row.url ? '&nbsp;<a href="' + row.url + '">' + json.click_here + '</a>' : '') + '</div><img class="profile-img" src="' + row.avatar + '"></li>');
          });

          $('.notification.drop-list ul li').click(function(e){
              $text = $(this).find('.text');
              if($text.length){
                  if($text.is(':hidden')){
                      $text.removeClass('hide').hide().slideDown('fast');
                  } else {
                      $text.slideUp('fast');
                  }
              }
              e.stopPropagation();
              return false;
          });                
      }
    }
  });
}



// get permission to run notifications
var webnotigranted = false;

function askNoti(){
  if(Notification.permission && Notification.permission!='granted'){
    Notification.requestPermission().then(function(result) {
      return ('granted'==result);
    });
  }
}

function notification(theTitle, theIcon, theBody) {
  if(!webnotigranted) webnotigranted = askNoti();
  var options = {};
  if(theIcon == null) theIcon = '/assets/009/img/system.png';
  options.icon = theIcon;
  if(theBody) options.body = theBody;
  var n = new Notification(theTitle, options);
  var audio = new Audio('/assets/009/audio/notification.ogg');
  audio.play();  
  setTimeout(n.close.bind(n), 8000);
}

function form_lookup(json,form){
  if( json.code && json.message.length){
    if(json.position=='modal'){
      BootstrapDialog.closeAll();
      BootstrapDialog.show({
        type: json.type,
        cssClass: 'alert-dialog btn-dark ' + json.code,
        size: 'size-wide',        
        title: json.title,
        message: json.message,
        buttons: [{
          label: 'Entiendo',            
          action: function(dialogRef){
            if(json.continue){
              location.href = json.continue;
            }
            dialogRef.close();
          }
        }]        
      });
    } else {
      $(form).find('.alert')  
        .html('<i class="ion-' + icons[json.code] + '"></i>&nbsp; ' + json.message)
        .removeClass('alert-success alert-danger alert-warning')
        .removeClass("hide")
        .addClass('alert-' + json.code)
        .hide()
        .slideDown();
    }
  }
  return json;
}

var forms = {
  recoverpassword: function(json,form){
    if(json.code=='success') {
      $('.input-code.hide').delay(1000).removeClass('hide').hide().slideDown('slow',function(){
        $('.input-code.hide').focus();  
      });
    }
  }
};


function modal_register_open(e) {

  $('input[name="username"]').focus();

  setTimeout(function(){

    var options = {
        onKeyUp: function (e) {
          if(isEmpty($(e.target).val())){
            $('.password-strength-title, .password-verdict, .progress').hide();
            $(e.target).parent().removeClass('has-success').addClass('has-error');
          } else {
            $('.password-strength-title, .password-verdict, .progress').show();
            $(e.target).parent().removeClass('has-error').addClass('has-success');
          }

          $(e.target).pwstrength("outputErrorList");
        },
        verdicts: ["<?php print locale('weak');?>","<?php print locale('normal');?>","<?php print locale('medium');?>","<?php print locale('strong');?>","<?php print locale('optimum');?>"],
        errorMessages: {
          password_to_short: "<?php echo locale('password_too_short');?>",
          same_as_username: "<?php echo locale('password_same_as_username');?>"
        }        
    };

    $('#password').pwstrength(options);

    $('input[name="username"]').ajaxify({
      url:'/username-check',
      focus: false,
      success:function(json){
        $('#messages-username').html('<span class="text-' + json.status + '"><i class="' + json.ion + '"></i> ' + json.message + '</span>');
      }
    });

    $('input[name="character"]').ajaxify({
      url:'/character-check',
      focus: false,
      success:function(json){
        $('#messages-character').html('<span class="text-' + json.status + '"><i class="' + json.ion + '"></i> ' + json.message + '</span>');
      }
    });
    
    $('input[name="email"]').ajaxify({
      url:'/email-check',
      focus: false,
      success:function(json){
        $('#messages-email').html('<span class="text-' + json.status + '"><i class="' + json.ion + '"></i> ' + json.message + '</span>');
      }
    });

    /*
    $('.form-register').change(function(){
      if($('#messages-username span').first().hasClass("text-success") && 
        $('#messages-character span').first().hasClass("text-success") && 
        $('#messages-email span').first().hasClass("text-success")){
        $('.btn-register').removeClass("disabled");
      }
    });*/

    $('#password').after('<span class="password-strength-title">password strength: </span>');
    $('.password-strength-title, .password-verdict, .progress').hide();

  },100);
}


function jsonToQueryString(json,noquest) {
    return (noquest?'':'?') + 
        Object.keys(json).map(function(key) {
            return encodeURIComponent(key) + '=' +
                encodeURIComponent(json[key]);
        }).join('&');
}


function isEmpty(text) {
  return text.length<1;
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function modal_click(url,json,css,size){
  var link = document.createElement('a');
  link.setAttribute('data-toggle','modal');
  if(json) link.setAttribute('data-json',json);
  if(css) link.setAttribute('data-cssclass',css);
  if(size) link.setAttribute('data-size',size);
  link.href = '#'+url;
  document.body.appendChild(link);
  link.click();
  
  return false;
}

$(document).on('submit','.form-ajax',function(e){
  var form = this;
  if($(this).find('input[type="file"]').not('.note-image-input').length || $(this).data('ajax') === false) return true;
  if( $(this).attr("action") ){
    $('body').addClass("loading");
    $button = $(this).find('input[type="submit"]');
    $button.prop('disabled',true);
    $button.addClass('disabled');

    var arr = $(this).attr("action").split('/');
    if(arr[0]=='http:'||arr[0]=='https:') arr.splice(0,3);
    arr = $.grep(arr, function(n){ return (n); });
    var callback = arr.join('').replace(/[0-9]/g, '');

    $.ajax({
      type: 'post',
      url: $(this).attr('action'),
      data: $(this).serialize(),
      success:function(json){

        if(json.redirect){
          setTimeout(function(){
            location.href = json.redirect;  
          },1000);
        }

        if(json.modal){
          BootstrapDialog.closeAll();
          modal_click(json.modal);
        }

        form_lookup(json,form);

        if(typeof forms[callback] == 'function') {
          forms[callback].call(this,json,form,e);
        }

        $('body').removeClass("loading");
        $button.prop('disabled',false);
        $button.removeClass('disabled');
      }
    });

    return false;
  } 

  if(typeof forms[callback] == 'function') {
    forms[callback].call(this,null);
  }

  return false;
});

$(document).on("click",'*[data-toggle="modal"]',function(e){
  e.preventDefault();
  BootstrapDialog.closeAll();
  var onshow = $(this).data("show")||null;
  var onshown = $(this).data("shown")||null;
  var data = $(this).data("json")||null;
  var size = $(this).data("size")||'size-normal';
  var cssClass = $(this).data("cssclass")||'';
  $('body').addClass("loading");

  $.ajax({
      type:       'get',
      url:        '/modals/' + $(this).attr('href').replace('#',''),
      cache:      false, 
      data:       data,
  }).done(function(message) {
    BootstrapDialog.show({
      size: size,
      cssClass: cssClass,
      message: message.split("\n").join(""),
      closable: true,
      onshow: function(dialog) {
        var title = dialog.getModalContent().find('.modal-title').html();
        dialog.getModalContent().find('.modal-title').remove();
        dialog.setTitle(title);
        if( typeof eval(onshow) == "function" ){
          eval(onshow + '(dialog)');
        }
        $('.ps-wrapper').perfectScrollbar('update');
      },
      onshown: function(dialog) {
        $('body').removeClass("loading");
        $(".modal label, .modal input, .modal a").tooltip();

        if( typeof eval(onshown) == "function" ){
          eval(onshown + '(dialog)');
        }
      }
    });
  });  
});

$(document).on("click",'.custom-check',function(e){
  if($(this).is(':checked'))  $(this).next().addClass('checked');
  else $(this).next().removeClass('checked');
});
