var $heroes = undefined;
var $hero = undefined;
var $current = undefined;

$(document).on('click','#classes-prev-next .prev', function(){
  if( $current.closest('li').prev().length){
    var id= $current.parent().prev().find('a').attr('id');
    return window.parent.$('#'+id).click();
  } 
});

$(document).on('click','#classes-prev-next .next', function(){
  if( $current.parent().next().length){
    var id= $current.parent().next().find('a').attr('id');
    return window.parent.$('#'+id).click();
  }
});

$(function(){
  $heroes = window.parent.$('#heroes');
  $hero = $('#hero').data('hero');
  $current = $heroes.find('.slick-active #' + $hero);

  if($current.closest('li').prev().length) $('#classes-prev-next .prev').removeClass('skill-prev-next-disabled');
  if($current.closest('li').next().length) $('#classes-prev-next .next').removeClass('skill-prev-next-disabled');

  $('.ps-wrapper').perfectScrollbar();
  $('.stat-bar-contain').css({'opacity':1});
  $('.website-wrapper').delay(200).animate({'opacity':1});
    
  $('.detail-row .stat-bar-contain .stat-bar').hover(function(){
  	$(this).parent().find('.stat-value .title').hide();
  	$(this).parent().find('.stat-value .value').show();
  },function(){
  	$(this).parent().find('.stat-value .value').hide();
	$(this).parent().find('.stat-value .title').show();
  });
});