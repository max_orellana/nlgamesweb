$(function() {
    var isInit = true;

    // Sections
    var $sectionMain = $("#main");
	var $sectionZone = $("#zone");
    var $sectionFooter = $("footer");
    var $sectionPanel = $("section.panel");

    // Elements
    var $body = $("body");
    var $bodyHTML = $("body, html");
    var $stickyNav = $("#sticky-nav");
    var $stickyFooter = $("#sticky-footer");
    var $stickyFooterHomePosition = $("#sticky-footer-position");
    var $featuresContent = $("#features-content");
    var $featuresNav = $("#features-menu");
    var $featuresTab = $("#features-menu li");
    var $featureSubPanel = $(".feature-sub-panel");
    var $featureTextContainer = $("#feature-text-container");
    var $featureText = $(".feature-text");
    var $navLink = $(".nav-link a");
    var $navHoverArea = $("nav .hover-area");
    var $navZone = $(".main-nav a[href=#zone]");
    var $navSkills = $(".main-nav a[href=#skills]");
    var $toSkillSet = $(".to-skill-set");
    var $skills = $("#skills");
    var $skillBackToTop = $(".back-to-top");
    var $skillIcon = $(".class-skill-icon");
    var $skillMain = $("#skills-main");
    var $skillNav = $("#skills-nav li");
	var $classForEveryone = $("#classForEveryone");
	var $classInEveryone = $("#classForEveryone li");
    var $skillPrevNext = $(".skill-prev-next");
    var $skillSetContainer = $("section.skill-set-container");
    var $skillSetList = $("#skill-set-list");
    var $skillSub = $("#skills-sub .skills-classes");
    var $skillSubPanel = $("#skills-sub");
    var $skillTransparentBG = $("#skills-sub .class-skill-details .transparent-bg-overlay");
    var $subPanelClassSkillMenu = $("#sub-class-menu");
    var $transparentOverlay = $(".transparent-bg-overlay");
    var $win = $(window);
    var $zoneNextAtelia = $("#atelia-prev-next .next");
    var $zonePrevAtelia = $("#atelia-prev-next .prev");
    var $zoneNextGarden = $("#garden-prev-next .next");
    var $zonePrevGarden = $("#garden-prev-next .prev");
    var $zonePrevNext = $(".zone-prev-next .prev-next-link");
    var $zoneExpand = $(".expand-zone");
    var $zoneGalleryAtelia = $("#atelia-gallery");
    var $zoneGalleryGarden = $("#garden-gallery");
    var $zoneGalleryClose = $(".close-zone-gallery");
    var $zoneLinks = $(".zone-link");
    var $zoneMain = $("#zone-main");
    var $zoneMainBG = $(".zone-main-bg");
    var $zoneSub = $("#zone-sub");
    var $zoneSubPanel = $(".zone-sub-panel");
    var $zoneSubAtelia = $(".zone-atelia");
    var $zoneSubGarden = $(".zone-garden");
    var $zoneThumbs = $(".zones-nav");
    var $zoneThumbAtelia = $("#atelia-nav");
    var $zoneThumbGarden = $("#garden-nav");
    var $zoneTo = $(".zone-to");

    // Position of Sections
    /*var sectionPos = {
        main : $sectionMain.offset().top,
		zone : $sectionZone.offset().top
    };*/

    // Main Nav
    var NAV = {
        pagePositionNavCheck : function(){
            // Highlight the current page in the nav
            $sectionPanel.each(function(index) {
                var thisPanel = $(this).offset().top,
                    height = $(this).height(),
                    scrollTop = $win.scrollTop(),
                    currentPanelId = $(this).attr('id'),
                    overlapTop = 100,
                    navLink = $stickyNav.find('nav ul a[href=#' + currentPanelId + ']');

                if(scrollTop >= (thisPanel - overlapTop) && scrollTop < thisPanel + height - overlapTop) {
                    $stickyNav.find('li').removeClass('active');
// console.log(currentPanelId);                    
                    navLink.parent('li').addClass('active');
                }
            });
        },
        navClick : function() {
            var label = "";

            if ($(this).attr('href')) {
                label = formatAnchorToString($(this).attr('href'));
            }
// console.log(label);
            // GA
            BROWSER.trackGA("section", label);
        },
        onShowNav : function() {
            var scrollPos = $win.scrollTop();

            $stickyNav.css("visibility", "visible");

            if (scrollPos >= sectionPos.story) {
                $stickyNav.addClass("show-nav");
            } else {
                $stickyNav.removeClass("show-nav");
            }
        },
        onNavOrnamentHover : function(e) {
            var $navOrnament = $navHoverArea.parent();

            if (e.type === "mouseenter") {
                $navOrnament.addClass("hovered");
            } else {
                $navOrnament.removeClass("hovered");
            }
        },
    };

    // Features
    var FEATURES = {
        tabbedNavigation : function() {
            var $this = $(this);
            var tabIndex = $this.index();

            if (!$this.hasClass("active")) {
                // Text
                $featureText.fadeOut();
                $featureText.eq(tabIndex).fadeIn();

                // Panels
                $featureSubPanel.fadeOut();
                $featureSubPanel.eq(tabIndex).fadeIn();

                // GA
                if (!isInit) {
                    BROWSER.trackGA("features", $this.children().html());
                }
            }

            $featuresTab.removeClass("active");
            $this.addClass("active");
        }
    };

    // Skills
    var SKILLS = {
        currentClass : "",
        closeSubPanelMenu : function() {
            $subPanelClassSkillMenu.addClass('hide');
            $("#skills-sub .transparent-bg-overlay").fadeOut();
            $("#skills-prev-next").fadeIn();
        },
        goToSkill : function(idx) {
            $subPanelClassSkillMenu.addClass('hide'); // Hide SubPanel icon menu

            var slickTopContent = $skillSub[0].slick;
            //var slickListDetails = $skillSetList[0].slick;

            slickTopContent.slickGoTo(idx);
            //slickListDetails.slickGoTo(idx);

            SKILLS.subPanelMenu();
        },
        skillPrevNext : function() {
            var $this = $(this);
            var classes = $this.attr('class');
            var index = 0;
            var id = "";

            $subPanelClassSkillMenu.addClass('hide'); // Hide SubPanel icon menu

            if ($this.hasClass("prev")) {
                $skillSub.slick("slickPrev");
                $skillSetList.slick("slickPrev");

                index = $skillSub.slick("slickCurrentSlide");
                id = $(".slick-slide[data-slick-index=" + index + "]").attr("id");
                SKILLS.currentClass = id;

                // GA
                BROWSER.trackGA("skills", "prev-class-skills/index-" + id);
            } else if ($this.hasClass("next")) {
                $skillSub.slick("slickNext");
                $skillSetList.slick("slickNext");

                index = $skillSub.slick("slickCurrentSlide");
                id = $(".slick-slide[data-slick-index=" + index + "]").attr("id");
                SKILLS.currentClass = id;

                // GA
                BROWSER.trackGA("skills", "next-class-skills/index-" + id);
            }

            SKILLS.subPanelMenu();
        },
        showHideSkillSubPanel : function() {
            var $this = $(this);
            var index = parseInt($this.index());

            // TODO: Change Transition to Fade Here

            if ($this.hasClass("skills-link")) { // Show Main Skills Section, Hide Classes Sub Panels
                $skillMain.fadeIn();
                $skillSubPanel.animate({"opacity" : 0}, 100, function() {
                    $skillSubPanel.addClass("hide");

                    // Reset Slick Slides
                    SKILLS.goToSkill(0);
                });

                // Handle Class Skill Detail Section
                $skillSetContainer.removeClass("full-height");
                SKILLS.setSkillContainer(false);
            } else { // Hide Main Skills Section, Show Classes Sub Panels
                $skillMain.fadeOut();
                $skillSubPanel.removeClass("hide").animate({"opacity" : 1}, 400);

                // Handle Class Skill Detail Section
                $skillSetContainer.addClass("full-height");
                SKILLS.setSkillContainer(true);

                // Slick Slides
                SKILLS.goToSkill(index);
            }

            // TODO: Change Transition to Slide Here

            SKILLS.currentClass = $(".slick-slide[data-slick-index=" + index + "]").attr("id");

            // GA
            if ($this.parent().parent().attr("id") === "skills-nav") {
                BROWSER.trackGA("skills", $this.parent().parent().attr("id") + "/" + $this.attr("class"));
            }
        },
        showSkillSubMenu : function() {
          $subPanelClassSkillMenu.removeClass('hide');

          $("#skills-sub .transparent-bg-overlay").show();
          $("#skills-prev-next").hide();
        },
        setSkillContainer : function(isShow) {
            // Relocates this section to bottom of page to prevent '.panelSnap()' plugin issues
            if (isShow) {
                $skillSetContainer.insertAfter("#skills");
            } else {
                $skillSetContainer.insertAfter("footer");
            }
        },
        subPanelMenu : function() {
            var curIndex = $skillSub.slick("slickCurrentSlide");
			
            this.buildSubPanelMenu(curIndex);
            this.moveSubPanelIconMenu(curIndex);

            $subPanelClassSkillMenu.find("li").unbind("click").click(SKILLS.subPanelMenuLink);
        },
        subPanelMenuLink : function() {
            var $this = $(this);
            var index = parseInt($this.attr("data-class-index"));

            SKILLS.closeSubPanelMenu();

            SKILLS.goToSkill(index);

            SKILLS.currentClass = $(".slick-slide[data-slick-index=" + index + "]").attr("id");

            // GA
            BROWSER.trackGA("skills", "skills-sub-nav/" + $this.attr("class"));
        },
        buildSubPanelMenu : function(ignoreIndex) {
            $subPanelClassSkillMenu.empty(); // Cleans (Resets) the menu
            $subPanelClassSkillMenu.append("<ul>");
            
            var $list = $subPanelClassSkillMenu.find("ul");

            $skillNav.each(function(i) {
                var menuItem = $(this).clone();

                // Exclude Active Link
                if (i !== ignoreIndex) {
                    menuItem.attr("data-class-index", i);

                    $list.append(menuItem);
                }
            });
        },
        moveSubPanelIconMenu : function(currentIndex) {
            var id = $skillSub.find($("[data-slick-index=" + currentIndex + "]")).attr("id");

            $("#" + id).find(".icon-container").prepend($subPanelClassSkillMenu);
        },
        toSkillSet : function() {
            var pos = $skillSetContainer.offset().top;

            $bodyHTML.animate({scrollTop:pos}, 500);

            // GA
            BROWSER.trackGA("skills", SKILLS.currentClass + "/see-new-skill-list-link");
        },
        backToTop : function(e) {
            e.preventDefault();

            var pos = $skills.offset().top;

            $bodyHTML.animate({scrollTop:pos}, 500);

            // GA
            BROWSER.trackGA("skills", SKILLS.currentClass + "/back-to-top-skills-link");
        }
    };

    // Zones
    var ZONES = {
        mainMenuLinkID : ["link-atelia", "link-garden"],
        closeGallery : function() {
            var $this = $(this);
            var classes = $this.attr("class");

            BROWSER.repositionToSection("zone");

            if ($this.hasClass("close-atleia-gallery")) {
                $zoneGalleryAtelia.parent().removeClass("expand-mode");
            } else {
                $zoneGalleryGarden.parent().removeClass("expand-mode");
            }
        },
        mainZoneNav : function(e) {
            var $this = $(this);
            var id = $this.attr("id");
// console.log('hoverin');
            if (e.type === "mouseenter") {
                if (!$this.hasClass("active")) {
                    $zoneMainBG.fadeOut();
                    $zoneLinks.removeClass("active");

                    if (id === ZONES.mainMenuLinkID[0]) { // Atelia
                        $zoneMainBG.eq(0).fadeIn();
                    } else { // Garden of Spirits
                        $zoneMainBG.eq(1).fadeIn();
                    }
                }

                $this.addClass("active");
                $zoneMainBG.removeClass("desaturate");
            } else {
                $zoneMainBG.addClass("desaturate");
            }
        },
        zoneExpandGallery : function() {
            var $this = $(this);
            var classes = $this.attr("class");

            $(".zone-gallery").removeClass("expand-mode");

            if ($this.hasClass("expand-atelia")) {
                $zoneGalleryAtelia.parent().addClass("expand-mode");
            } else {
                $zoneGalleryGarden.parent().addClass("expand-mode");
            }

            if ($win.width() > 1920) {
                ZONES.zoneReinitGallery($this);
            }

            // GA
            BROWSER.trackGA("zones", $this.attr("class"));
        },
        zoneReinitGallery : function($this) {
            var index = 0;
            var slickByIndex = {};

            if ($this.hasClass("expand-atelia")) {
                index = $zoneGalleryAtelia.slick('slickCurrentSlide');
                slickByIndex = $zoneGalleryAtelia[0].slick;

                slickByIndex.unslick();
                $zoneGalleryAtelia.slick({
                    arrows : false,
                    draggable : false,
                    fade : true
                });

                $zoneGalleryAtelia[0].slick.slickGoTo(index);
            } else if ($this.hasClass('expand-garden')) {
                index = $zoneGalleryGarden.slick('slickCurrentSlide');
                slickByIndex = $zoneGalleryGarden[0].slick;

                slickByIndex.unslick();
                $zoneGalleryGarden.slick({
                    arrows : false,
                    draggable : false,
                    fade : true
                });

                $zoneGalleryGarden[0].slick.slickGoTo(index);
            }

// console.log('slicky new', index);

        },
        zonePrevNext : function() {
            var $this = $(this);
            var classes = $this.attr("class");
            var id = $this.parent().attr("id");
            var index = 0;

            if (id === "atelia-prev-next") {
                if ($this.hasClass("next")) {
                    $zoneGalleryAtelia.slick("slickNext");
                } else if ($this.hasClass("prev")) {
                    $zoneGalleryAtelia.slick("slickPrev");
                }
                index = $zoneGalleryAtelia.slick("slickCurrentSlide") + 1;
            } else if (id === "garden-prev-next") {
                if ($this.hasClass("next")) {
                    $zoneGalleryGarden.slick("slickNext");
                } else if ($this.hasClass("prev")) {
                    $zoneGalleryGarden.slick("slickPrev");
                }
                index = $zoneGalleryGarden.slick("slickCurrentSlide") + 1;
            }

            // GA
            BROWSER.trackGA("zones", (id + "-nav-" + index));
        },
        zoneSubPage : function() {
            var $this = $(this);
            var zone = $this.attr("id");

            $zoneSubPanel.css("visibility", "visible");

            if (zone === "link-atelia" || zone === "to-atelia") {
                $zoneMain.fadeOut();
                $zoneSub.show();
                $zoneSubAtelia.fadeIn();
                $zoneSubGarden.fadeOut();
            } else if (zone === "link-garden" || zone === "to-garden") {
                $zoneMain.fadeOut();
                $zoneSub.show();
                $zoneSubAtelia.fadeOut();
                $zoneSubGarden.fadeIn();
            } else {
                $zoneSubPanel.fadeOut({
                    complete : function() {
                        $zoneSub.hide();
                    }
                });
                $zoneMain.fadeIn();
            }

            // GA
            if ($(this).attr("href") !== "#zone") {
                BROWSER.trackGA("zones", zone);
            }
        },
        zoneThumbs : function() {
            var $this = $(this);
            var id = $this.parent().attr("id");
            var index = parseInt($this.index());
            var slickByIndex = {};

            if (id === "atelia-nav") {
                slickByIndex = $zoneGalleryAtelia[0].slick;
                slickByIndex.slickGoTo(index);
            } else if (id === "garden-nav") {
                slickByIndex = $zoneGalleryGarden[0].slick;
                slickByIndex.slickGoTo(index);
            }

            // GA
            BROWSER.trackGA("zones", (id + "-" + "thumbnail-" + (++index)));
        }
    };

    // Footer
    var STICKYFOOTER = {
        initPos : $stickyFooterHomePosition.offset().top,
        handleStickyness : function() {
            this.initPos = $stickyFooterHomePosition.offset().top;

            var scrollTop = $(window).scrollTop();
            var targetPos = this.initPos + 50; // 50 is the margintop;

            // Sticky Nav snaps to bottom of last section before footer
            if (BROWSER.scrollBottom > targetPos) {
                $stickyFooter.removeClass("sticky");
            } else {
                $stickyFooter.addClass("sticky");
            }
        }
    };

    // Browser Window
    var BROWSER = {
        scrollBottom : $win.scrollTop() + $win.height(),
        loaded : function() {
            // StickyFooter
            STICKYFOOTER.handleStickyness();
            
            // Features
            $featuresTab.eq(0).trigger("click");
            $featuresContent.fadeIn();

            // Zones
            $zoneLinks.eq(0).trigger("mouseenter").trigger("mouseleave");

            isInit = false;
        },
        repositionToSection : function(section) {
            var pos = $("section[id=" + section + "]").offset().top;

            $win.scrollTop(pos);
        },
        trackGA : function(pt, p) {
            var label = "";
            var path = (pt + "/") || "";
            var page = formatTrackingString(p) || "";

            label = path + page;

            //$.fn.ncsVirtualPageTracker({
            //    virtualPageview: label
            //});
        }
    };

    // INIT
    (function() {
        // Event Listeners - ALL - Resize
        $win.resize(function() {
            BROWSER.scrollBottom = $win.scrollTop() + $win.height();

        });
        // Event Listener - ALL - Scroll
        $win.scroll(function() {
            BROWSER.scrollBottom = $win.scrollTop() + $win.height();

            //NAV.pagePositionNavCheck();
            //NAV.onShowNav();
            STICKYFOOTER.handleStickyness();
        }).trigger("scroll");
        
        // Event Listener - ALL - Click
        $navLink.click(NAV.navClick);
        $featuresTab.click(FEATURES.tabbedNavigation);
        $navSkills.click(SKILLS.showHideSkillSubPanel);
        $skillBackToTop.click(SKILLS.backToTop);
        $skillIcon.click(SKILLS.showSkillSubMenu);
        $skillNav.click(SKILLS.showHideSkillSubPanel);
        $skillPrevNext.click(SKILLS.skillPrevNext);
        $skillTransparentBG.click(SKILLS.closeSubPanelMenu);
        $toSkillSet.click(SKILLS.toSkillSet);
        $navZone.one(function() {
            $(this).click(ZONES.zoneSubPage);
        });
        $zoneExpand.click(ZONES.zoneExpandGallery);
        $zoneLinks.click(ZONES.zoneSubPage);
        $zoneTo.click(ZONES.zoneSubPage);
        $zonePrevNext.click(ZONES.zonePrevNext);
        $zoneThumbs.find("a").click(ZONES.zoneThumbs);
        $zoneGalleryClose.click(ZONES.closeGallery);
        
        // Event Listener - ALL - Hover
        $navHoverArea.hover(NAV.onNavOrnamentHover);
        $zoneLinks.hover(ZONES.mainZoneNav);

        // Event Listeners - Window Ready
        $body[0].onload = BROWSER.loaded;

        // Init Plugins - Panel Snap
        // $body.panelSnap(); // TODO: Investigate wierd bug on smaller resolutions, possible cause by CSS min-height for html and body?

        // Init Plugins - Fancy Box Videos
        $(".fancybox").each(function() {
            var id = $(this).attr("id");
            $("#" + id).fancybox({ type : "iframe" });
        });

        // Init Plugins - Skills
        $skillSub.slick({
            arrows : false,
            draggable : false
        });
        $skillSetList.slick({
            arrows : false,
            draggable : false,
            slidesToShow: 1,
            adaptiveHeight : true
        });

        // Init Plugins - Zones
        $zoneGalleryAtelia.slick({
            arrows : false,
            draggable : false,
            fade : true
        });
        $zoneGalleryGarden.slick({
            arrows : false,
            draggable : false,
            fade : true
        });

        // Skills Init Functions
        SKILLS.setSkillContainer(false);

        // DIMEBAR
        //$().dimeBar();
        //initDimebar();
        $("#btn-portuguese").remove();

        // Set Cookie
        setCookie();
    })();

    // Cookie: Do Not Show
    $.Cookie = {
        get: function( name ) {
            var nameOfCookie = name + "=",
                x = 0;
            while( x <= document.cookie.length )    {
                var y = ( x + nameOfCookie.length );
                if( document.cookie.substring( x, y ) == nameOfCookie ) {
                    if( ( endOfCookie = document.cookie.indexOf( ";", y ) ) == -1 ) {
                        endOfCookie = document.cookie.length;
                    }
                    return unescape( document.cookie.substring( y, endOfCookie ) );
                }
                x = document.cookie.indexOf( " ", x ) + 1;
                if( x === 0 ) {
                    break;
                }
            }
            return "";
        },
        set: function( name, value, expiredays ) {
            var todayDate = new Date();

            expiredays = expiredays || null;
            if ( expiredays !== null ) {
                todayDate.setDate( todayDate.getDate() + expiredays );
            }
            document.cookie = name + "=" + escape( value ) + "; path=/; domain=" + document.domain + ";" + ( expiredays !== null ? ( "expires=" + todayDate.toGMTString() + ";" ) : "" );
        }
    };

    function setCookie() {
        $('#dont-show input[type=checkbox], #dont-show-from-main input[type=checkbox]').change(function() {
            if ($(this).attr("checked")) {
                $.Cookie.set("infinite_odyssey_microsite_show", "false", "30");
            } else {
                $.Cookie.set("infinite_odyssey_microsite_show", "false", "0");
            }
        });

        if(document.cookie.indexOf("infiniteOdysseyMicrositeVisited") < 0){
            document.cookie = "infiniteOdysseyMicrositeVisited%3dtrue%3bpath%3d/index.html";
        }
    }

    function formatTrackingString(s) {
        var trackingText = s.replace(/\s/g, "-").toLowerCase();

        return trackingText;
    }

    function formatAnchorToString(href) {
        var h = href.replace("#", "");

        return h;
    }
});