(window, document, jQuery), ! function() {
    var e = {},
        t = null,
        o = !0,
        n = !1;
    try {
        "undefined" != typeof AudioContext ? t = new AudioContext : "undefined" != typeof webkitAudioContext ? t = new webkitAudioContext : o = !1
    } catch (r) {
        o = !1
    }
    if (!o)
        if ("undefined" != typeof Audio) try {
            new Audio
        } catch (r) {
            n = !0
        } else n = !0;
    if (o) {
        var i = "undefined" == typeof t.createGain ? t.createGainNode() : t.createGain();
        i.gain.value = 1, i.connect(t.destination)
    }
    var a = function(e) {
        this._volume = 1, this._muted = !1, this.usingWebAudio = o, this.ctx = t, this.noAudio = n, this._howls = [], this._codecs = e, this.iOSAutoEnable = !0
    };
    a.prototype = {
        volume: function(e) {
            var t = this;
            if (e = parseFloat(e), e >= 0 && 1 >= e) {
                t._volume = e, o && (i.gain.value = e);
                for (var n in t._howls)
                    if (t._howls.hasOwnProperty(n) && t._howls[n]._webAudio === !1)
                        for (var r = 0; r < t._howls[n]._audioNode.length; r++) t._howls[n]._audioNode[r].volume = t._howls[n]._volume * t._volume;
                return t
            }
            return o ? i.gain.value : t._volume
        },
        mute: function() {
            return this._setMuted(!0), this
        },
        unmute: function() {
            return this._setMuted(!1), this
        },
        _setMuted: function(e) {
            var t = this;
            t._muted = e, o && (i.gain.value = e ? 0 : t._volume);
            for (var n in t._howls)
                if (t._howls.hasOwnProperty(n) && t._howls[n]._webAudio === !1)
                    for (var r = 0; r < t._howls[n]._audioNode.length; r++) t._howls[n]._audioNode[r].muted = e
        },
        codecs: function(e) {
            return this._codecs[e]
        },
        _enableiOSAudio: function() {
            var e = this;
            if (!t || !e._iOSEnabled && /iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                e._iOSEnabled = !1;
                var o = function() {
                    var n = t.createBuffer(1, 1, 22050),
                        r = t.createBufferSource();
                    r.buffer = n, r.connect(t.destination), "undefined" == typeof r.start ? r.noteOn(0) : r.start(0), setTimeout(function() {
                        (r.playbackState === r.PLAYING_STATE || r.playbackState === r.FINISHED_STATE) && (e._iOSEnabled = !0, e.iOSAutoEnable = !1, window.removeEventListener("touchstart", o, !1))
                    }, 0)
                };
                return window.addEventListener("touchstart", o, !1), e
            }
        }
    };
    var s = null,
        l = {};
    n || (s = new Audio, l = {
        mp3: !!s.canPlayType("audio/mpeg;").replace(/^no$/, ""),
        opus: !!s.canPlayType('audio/ogg; codecs="opus"').replace(/^no$/, ""),
        ogg: !!s.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""),
        wav: !!s.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""),
        aac: !!s.canPlayType("audio/aac;").replace(/^no$/, ""),
        m4a: !!(s.canPlayType("audio/x-m4a;") || s.canPlayType("audio/m4a;") || s.canPlayType("audio/aac;")).replace(/^no$/, ""),
        mp4: !!(s.canPlayType("audio/x-mp4;") || s.canPlayType("audio/mp4;") || s.canPlayType("audio/aac;")).replace(/^no$/, ""),
        weba: !!s.canPlayType('audio/webm; codecs="vorbis"').replace(/^no$/, "")
    });
    var c = new a(l),
        d = function(e) {
            var n = this;
            n._autoplay = e.autoplay || !1, n._buffer = e.buffer || !1, n._duration = e.duration || 0, n._format = e.format || null, n._loop = e.loop || !1, n._loaded = !1, n._sprite = e.sprite || {}, n._src = e.src || "", n._pos3d = e.pos3d || [0, 0, -.5], n._volume = void 0 !== e.volume ? e.volume : 1, n._urls = e.urls || [], n._rate = e.rate || 1, n._model = e.model || null, n._onload = [e.onload || function() {}], n._onloaderror = [e.onloaderror || function() {}], n._onend = [e.onend || function() {}], n._onpause = [e.onpause || function() {}], n._onplay = [e.onplay || function() {}], n._onendTimer = [], n._webAudio = o && !n._buffer, n._audioNode = [], n._webAudio && n._setupAudioNode(), "undefined" != typeof t && t && c.iOSAutoEnable && c._enableiOSAudio(), c._howls.push(n), n.load()
        };
    if (d.prototype = {
            load: function() {
                var e = this,
                    t = null;
                if (n) return void e.on("loaderror");
                for (var o = 0; o < e._urls.length; o++) {
                    var r, i;
                    if (e._format) r = e._format;
                    else {
                        if (i = e._urls[o], r = /^data:audio\/([^;,]+);/i.exec(i), r || (r = /\.([^.]+)$/.exec(i.split("?", 1)[0])), !r) return void e.on("loaderror");
                        r = r[1].toLowerCase()
                    }
                    if (l[r]) {
                        t = e._urls[o];
                        break
                    }
                }
                if (!t) return void e.on("loaderror");
                if (e._src = t, e._webAudio) u(e, t);
                else {
                    var s = new Audio;
                    s.addEventListener("error", function() {
                        s.error && 4 === s.error.code && (a.noAudio = !0), e.on("loaderror", {
                            type: s.error ? s.error.code : 0
                        })
                    }, !1), e._audioNode.push(s), s.src = t, s._pos = 0, s.preload = "auto", s.volume = c._muted ? 0 : e._volume * c.volume();
                    var d = function() {
                        e._duration = Math.ceil(10 * s.duration) / 10, 0 === Object.getOwnPropertyNames(e._sprite).length && (e._sprite = {
                            _default: [0, 1e3 * e._duration]
                        }), e._loaded || (e._loaded = !0, e.on("load")), e._autoplay && e.play(), s.removeEventListener("canplaythrough", d, !1)
                    };
                    s.addEventListener("canplaythrough", d, !1), s.load()
                }
                return e
            },
            urls: function(e) {
                var t = this;
                return e ? (t.stop(), t._urls = "string" == typeof e ? [e] : e, t._loaded = !1, t.load(), t) : t._urls
            },
            play: function(e, o) {
                var n = this;
                return "function" == typeof e && (o = e), e && "function" != typeof e || (e = "_default"), n._loaded ? n._sprite[e] ? (n._inactiveNode(function(r) {
                    r._sprite = e;
                    var i = r._pos > 0 ? r._pos : n._sprite[e][0] / 1e3,
                        a = 0;
                    n._webAudio ? (a = n._sprite[e][1] / 1e3 - r._pos, r._pos > 0 && (i = n._sprite[e][0] / 1e3 + i)) : a = n._sprite[e][1] / 1e3 - (i - n._sprite[e][0] / 1e3);
                    var s, l = !(!n._loop && !n._sprite[e][2]),
                        d = "string" == typeof o ? o : Math.round(Date.now() * Math.random()) + "";
                    if (function() {
                            var t = {
                                id: d,
                                sprite: e,
                                loop: l
                            };
                            s = setTimeout(function() {
                                !n._webAudio && l && n.stop(t.id).play(e, t.id), n._webAudio && !l && (n._nodeById(t.id).paused = !0, n._nodeById(t.id)._pos = 0, n._clearEndTimer(t.id)), n._webAudio || l || n.stop(t.id), n.on("end", d)
                            }, 1e3 * a), n._onendTimer.push({
                                timer: s,
                                id: t.id
                            })
                        }(), n._webAudio) {
                        var u = n._sprite[e][0] / 1e3,
                            p = n._sprite[e][1] / 1e3;
                        r.id = d, r.paused = !1, h(n, [l, u, p], d), n._playStart = t.currentTime, r.gain.value = n._volume, "undefined" == typeof r.bufferSource.start ? l ? r.bufferSource.noteGrainOn(0, i, 86400) : r.bufferSource.noteGrainOn(0, i, a) : l ? r.bufferSource.start(0, i, 86400) : r.bufferSource.start(0, i, a)
                    } else {
                        if (4 !== r.readyState && (r.readyState || !navigator.isCocoonJS)) return n._clearEndTimer(d),
                            function() {
                                var t = n,
                                    i = e,
                                    a = o,
                                    s = r,
                                    l = function() {
                                        t.play(i, a), s.removeEventListener("canplaythrough", l, !1)
                                    };
                                s.addEventListener("canplaythrough", l, !1)
                            }(), n;
                        r.readyState = 4, r.id = d, r.currentTime = i, r.muted = c._muted || r.muted, r.volume = n._volume * c.volume(), setTimeout(function() {
                            r.play()
                        }, 0)
                    }
                    return n.on("play"), "function" == typeof o && o(d), n
                }), n) : ("function" == typeof o && o(), n) : (n.on("load", function() {
                    n.play(e, o)
                }), n)
            },
            pause: function(e) {
                var t = this;
                if (!t._loaded) return t.on("play", function() {
                    t.pause(e)
                }), t;
                t._clearEndTimer(e);
                var o = e ? t._nodeById(e) : t._activeNode();
                if (o)
                    if (o._pos = t.pos(null, e), t._webAudio) {
                        if (!o.bufferSource || o.paused) return t;
                        o.paused = !0, "undefined" == typeof o.bufferSource.stop ? o.bufferSource.noteOff(0) : o.bufferSource.stop(0)
                    } else o.pause();
                return t.on("pause"), t
            },
            stop: function(e) {
                var t = this;
                if (!t._loaded) return t.on("play", function() {
                    t.stop(e)
                }), t;
                t._clearEndTimer(e);
                var o = e ? t._nodeById(e) : t._activeNode();
                if (o)
                    if (o._pos = 0, t._webAudio) {
                        if (!o.bufferSource || o.paused) return t;
                        o.paused = !0, "undefined" == typeof o.bufferSource.stop ? o.bufferSource.noteOff(0) : o.bufferSource.stop(0)
                    } else isNaN(o.duration) || (o.pause(), o.currentTime = 0);
                return t
            },
            mute: function(e) {
                var t = this;
                if (!t._loaded) return t.on("play", function() {
                    t.mute(e)
                }), t;
                var o = e ? t._nodeById(e) : t._activeNode();
                return o && (t._webAudio ? o.gain.value = 0 : o.muted = !0), t
            },
            unmute: function(e) {
                var t = this;
                if (!t._loaded) return t.on("play", function() {
                    t.unmute(e)
                }), t;
                var o = e ? t._nodeById(e) : t._activeNode();
                return o && (t._webAudio ? o.gain.value = t._volume : o.muted = !1), t
            },
            volume: function(e, t) {
                var o = this;
                if (e = parseFloat(e), e >= 0 && 1 >= e) {
                    if (o._volume = e, !o._loaded) return o.on("play", function() {
                        o.volume(e, t)
                    }), o;
                    var n = t ? o._nodeById(t) : o._activeNode();
                    return n && (o._webAudio ? n.gain.value = e : n.volume = e * c.volume()), o
                }
                return o._volume
            },
            loop: function(e) {
                var t = this;
                return "boolean" == typeof e ? (t._loop = e, t) : t._loop
            },
            sprite: function(e) {
                var t = this;
                return "object" == typeof e ? (t._sprite = e, t) : t._sprite
            },
            pos: function(e, o) {
                var n = this;
                if (!n._loaded) return n.on("load", function() {
                    n.pos(e)
                }), "number" == typeof e ? n : n._pos || 0;
                e = parseFloat(e);
                var r = o ? n._nodeById(o) : n._activeNode();
                if (r) return e >= 0 ? (n.pause(o), r._pos = e, n.play(r._sprite, o), n) : n._webAudio ? r._pos + (t.currentTime - n._playStart) : r.currentTime;
                if (e >= 0) return n;
                for (var i = 0; i < n._audioNode.length; i++)
                    if (n._audioNode[i].paused && 4 === n._audioNode[i].readyState) return n._webAudio ? n._audioNode[i]._pos : n._audioNode[i].currentTime
            },
            pos3d: function(e, t, o, n) {
                var r = this;
                if (t = "undefined" != typeof t && t ? t : 0, o = "undefined" != typeof o && o ? o : -.5, !r._loaded) return r.on("play", function() {
                    r.pos3d(e, t, o, n)
                }), r;
                if (!(e >= 0 || 0 > e)) return r._pos3d;
                if (r._webAudio) {
                    var i = n ? r._nodeById(n) : r._activeNode();
                    i && (r._pos3d = [e, t, o], i.panner.setPosition(e, t, o), i.panner.panningModel = r._model || "HRTF")
                }
                return r
            },
            fade: function(e, t, o, n, r) {
                var i = this,
                    a = Math.abs(e - t),
                    s = e > t ? "down" : "up",
                    l = a / .01,
                    c = o / l;
                if (!i._loaded) return i.on("load", function() {
                    i.fade(e, t, o, n, r)
                }), i;
                i.volume(e, r);
                for (var d = 1; l >= d; d++) ! function() {
                    var e = i._volume + ("up" === s ? .01 : -.01) * d,
                        o = Math.round(1e3 * e) / 1e3,
                        a = t;
                    setTimeout(function() {
                        i.volume(o, r), o === a && n && n()
                    }, c * d)
                }()
            },
            fadeIn: function(e, t, o) {
                return this.volume(0).play().fade(0, e, t, o)
            },
            fadeOut: function(e, t, o, n) {
                var r = this;
                return r.fade(r._volume, e, t, function() {
                    o && o(), r.pause(n), r.on("end")
                }, n)
            },
            _nodeById: function(e) {
                for (var t = this, o = t._audioNode[0], n = 0; n < t._audioNode.length; n++)
                    if (t._audioNode[n].id === e) {
                        o = t._audioNode[n];
                        break
                    }
                return o
            },
            _activeNode: function() {
                for (var e = this, t = null, o = 0; o < e._audioNode.length; o++)
                    if (!e._audioNode[o].paused) {
                        t = e._audioNode[o];
                        break
                    }
                return e._drainPool(), t
            },
            _inactiveNode: function(e) {
                for (var t = this, o = null, n = 0; n < t._audioNode.length; n++)
                    if (t._audioNode[n].paused && 4 === t._audioNode[n].readyState) {
                        e(t._audioNode[n]), o = !0;
                        break
                    }
                if (t._drainPool(), !o) {
                    var r;
                    if (t._webAudio) r = t._setupAudioNode(), e(r);
                    else {
                        t.load(), r = t._audioNode[t._audioNode.length - 1];
                        var i = navigator.isCocoonJS ? "canplaythrough" : "loadedmetadata",
                            a = function() {
                                r.removeEventListener(i, a, !1), e(r)
                            };
                        r.addEventListener(i, a, !1)
                    }
                }
            },
            _drainPool: function() {
                var e, t = this,
                    o = 0;
                for (e = 0; e < t._audioNode.length; e++) t._audioNode[e].paused && o++;
                for (e = t._audioNode.length - 1; e >= 0 && !(5 >= o); e--) t._audioNode[e].paused && (t._webAudio && t._audioNode[e].disconnect(0), o--, t._audioNode.splice(e, 1))
            },
            _clearEndTimer: function(e) {
                for (var t = this, o = 0, n = 0; n < t._onendTimer.length; n++)
                    if (t._onendTimer[n].id === e) {
                        o = n;
                        break
                    }
                var r = t._onendTimer[o];
                r && (clearTimeout(r.timer), t._onendTimer.splice(o, 1))
            },
            _setupAudioNode: function() {
                var e = this,
                    o = e._audioNode,
                    n = e._audioNode.length;
                return o[n] = "undefined" == typeof t.createGain ? t.createGainNode() : t.createGain(), o[n].gain.value = e._volume, o[n].paused = !0, o[n]._pos = 0, o[n].readyState = 4, o[n].connect(i), o[n].panner = t.createPanner(), o[n].panner.panningModel = e._model || "equalpower", o[n].panner.setPosition(e._pos3d[0], e._pos3d[1], e._pos3d[2]), o[n].panner.connect(o[n]), o[n]
            },
            on: function(e, t) {
                var o = this,
                    n = o["_on" + e];
                if ("function" == typeof t) n.push(t);
                else
                    for (var r = 0; r < n.length; r++) t ? n[r].call(o, t) : n[r].call(o);
                return o
            },
            off: function(e, t) {
                var o = this,
                    n = o["_on" + e],
                    r = t ? t.toString() : null;
                if (r) {
                    for (var i = 0; i < n.length; i++)
                        if (r === n[i].toString()) {
                            n.splice(i, 1);
                            break
                        }
                } else o["_on" + e] = [];
                return o
            },
            unload: function() {
                for (var t = this, o = t._audioNode, n = 0; n < t._audioNode.length; n++) o[n].paused || (t.stop(o[n].id), t.on("end", o[n].id)), t._webAudio ? o[n].disconnect(0) : o[n].src = "";
                for (n = 0; n < t._onendTimer.length; n++) clearTimeout(t._onendTimer[n].timer);
                var r = c._howls.indexOf(t);
                null !== r && r >= 0 && c._howls.splice(r, 1), delete e[t._src], t = null
            }
        }, o) var u = function(t, o) {
            if (o in e) return t._duration = e[o].duration, void f(t);
            if (/^data:[^;]+;base64,/.test(o)) {
                for (var n = atob(o.split(",")[1]), r = new Uint8Array(n.length), i = 0; i < n.length; ++i) r[i] = n.charCodeAt(i);
                p(r.buffer, t, o)
            } else {
                var a = new XMLHttpRequest;
                a.open("GET", o, !0), a.responseType = "arraybuffer", a.onload = function() {
                    p(a.response, t, o)
                }, a.onerror = function() {
                    t._webAudio && (t._buffer = !0, t._webAudio = !1, t._audioNode = [], delete t._gainNode, delete e[o], t.load())
                };
                try {
                    a.send()
                } catch (s) {
                    a.onerror()
                }
            }
        },
        p = function(o, n, r) {
            t.decodeAudioData(o, function(t) {
                t && (e[r] = t, f(n, t))
            }, function(e) {
                n.on("loaderror")
            })
        },
        f = function(e, t) {
            e._duration = t ? t.duration : e._duration, 0 === Object.getOwnPropertyNames(e._sprite).length && (e._sprite = {
                _default: [0, 1e3 * e._duration]
            }), e._loaded || (e._loaded = !0, e.on("load")), e._autoplay && e.play()
        },
        h = function(o, n, r) {
            var i = o._nodeById(r);
            i.bufferSource = t.createBufferSource(), i.bufferSource.buffer = e[o._src], i.bufferSource.connect(i.panner), i.bufferSource.loop = n[0], n[0] && (i.bufferSource.loopStart = n[1], i.bufferSource.loopEnd = n[1] + n[2]), i.bufferSource.playbackRate.value = o._rate
        };
    "function" == typeof define && define.amd && define(function() {
        return {
            Howler: c,
            Howl: d
        }
    }), "undefined" != typeof exports && (exports.Howler = c, exports.Howl = d), "undefined" != typeof window && (window.Howler = c, window.Howl = d)
}();


var audio = {
    tracks: [],
    changeAudio: function(e) {
        if (!isMobile) {
            e = e > state.actsLength - 1 ? state.actsLength - 1 : e;
            var t = state.currentAct >= state.actsLength - 1 ? state.actsLength - 1 : state.currentAct;
            void 0 !== audio.tracks[e] && 0 === audio.tracks[e]._volume && (void 0 !== audio.tracks[t] && audio.tracks[t].fade(1, 0, 2e3), audio.tracks[e].fade(0, 1, 2e3))
        }
    },
    toggleAudio: function(e) {
        isMobile || (Howler._muted || e ? ($(".js-audio-icon-play").show(), $(".js-audio-icon-mute").hide(), Howler.unmute()) : ($(".js-audio-icon-play").hide(), $(".js-audio-icon-mute").show(), Howler.mute()))
    }
};

var $doc = $(document),
    $win = $(window),
    $body = $("body"),
    isMobile = $win.width() < 768 ? !0 : !1;
$win.on("resize", function() {
    isMobile = $win.width() < 768 ? !0 : !1
});


$(function(){
    $(document.body).on("click touchend", ".js-toggle-audio", function() {
        audio.toggleAudio()
    });
    $('.c-options').delay(2000).removeClass('hide').hide().fadeIn(3000);
});

$(window).load(function() {
    isMobile || (audio.tracks = [new Howl({
        urls: [assetsPath + "/promo/audio/TerrestrialAlliance.ogg", assetsPath + "/promo/audio/TerrestrialAlliance.mp3"],
        loop: !0,
        volume: 0.4
    }).play()])
});