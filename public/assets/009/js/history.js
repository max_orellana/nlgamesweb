function drags(t, e, i) {
    t.on("mousedown touchstart", function(o) {
        t.addClass("draggable"), e.addClass("resizable");
        var n = o.pageX ? o.pageX : o.originalEvent.touches[0].pageX,
            s = t.outerWidth(),
            r = t.offset().left + s - n,
            a = i.offset().left,
            l = i.outerWidth();
        minLeft = a, maxLeft = a + l - s, t.parents().on("mousemove touchmove", function(t) {
            var i = t.pageX ? t.pageX : t.originalEvent.touches[0].pageX;
            leftValue = i + r - s, leftValue < minLeft ? leftValue = minLeft : leftValue > maxLeft && (leftValue = maxLeft), widthValue = 100 * (leftValue + s / 2 - a) / l + "%", $(".draggable").css("left", widthValue).on("mouseup touchend touchcancel", function() {
                $(this).removeClass("draggable"), e.removeClass("resizable")
            }), $(".resizable").css("width", widthValue)
        }).on("mouseup touchend touchcancel", function() {
            t.removeClass("draggable"), e.removeClass("resizable")
        }), o.preventDefault()
    }).on("mouseup touchend touchcancel", function(i) {
        t.removeClass("draggable"), e.removeClass("resizable")
    })
}

function getNow() {
    return Date.now() || function() {
        return (new Date).getTime()
    }
}

function debounce(t, e, i) {
    var o;
    return function() {
        var n = this,
            s = arguments,
            r = function() {
                o = null, i || t.apply(n, s)
            },
            a = i && !o;
        clearTimeout(o), o = setTimeout(r, e), a && t.apply(n, s)
    }
}

function randomNumber(t, e) {
    return Math.random() * (e - t) + t
}

function canvasFadeout(t) {
    t.save(), gradient = t.createLinearGradient(0, .8 * mc.height, 0, mc.height), gradient.addColorStop(0, "rgba(0, 0, 0, 0)"), gradient.addColorStop(1, "rgba(0, 0, 0, 1)"), t.fillStyle = gradient, t.fillRect(0, 0, mc.width, mc.height + 1), t.restore()
}
"document" in self && ("classList" in document.createElement("_") ? ! function() {
        var t = document.createElement("_");
        if (t.classList.add("c1", "c2"), !t.classList.contains("c2")) {
            var e = function(t) {
                var e = DOMTokenList.prototype[t];
                DOMTokenList.prototype[t] = function(t) {
                    var i, o = arguments.length;
                    for (i = 0; o > i; i++) t = arguments[i], e.call(this, t)
                }
            };
            e("add"), e("remove")
        }
        if (t.classList.toggle("c3", !1), t.classList.contains("c3")) {
            var i = DOMTokenList.prototype.toggle;
            DOMTokenList.prototype.toggle = function(t, e) {
                return 1 in arguments && !this.contains(t) == !e ? e : i.call(this, t)
            }
        }
        t = null
    }() : ! function(t) {
        "use strict";
        if ("Element" in t) {
            var e = "classList",
                i = "prototype",
                o = t.Element[i],
                n = Object,
                s = String[i].trim || function() {
                    return this.replace(/^\s+|\s+$/g, "")
                },
                r = Array[i].indexOf || function(t) {
                    for (var e = 0, i = this.length; i > e; e++)
                        if (e in this && this[e] === t) return e;
                    return -1
                },
                a = function(t, e) {
                    this.name = t, this.code = DOMException[t], this.message = e
                },
                l = function(t, e) {
                    if ("" === e) throw new a("SYNTAX_ERR", "An invalid or illegal string was specified");
                    if (/\s/.test(e)) throw new a("INVALID_CHARACTER_ERR", "String contains an invalid character");
                    return r.call(t, e)
                },
                c = function(t) {
                    for (var e = s.call(t.getAttribute("class") || ""), i = e ? e.split(/\s+/) : [], o = 0, n = i.length; n > o; o++) this.push(i[o]);
                    this._updateClassName = function() {
                        t.setAttribute("class", this.toString())
                    }
                },
                h = c[i] = [],
                d = function() {
                    return new c(this)
                };
            if (a[i] = Error[i], h.item = function(t) {
                    return this[t] || null
                }, h.contains = function(t) {
                    return t += "", -1 !== l(this, t)
                }, h.add = function() {
                    var t, e = arguments,
                        i = 0,
                        o = e.length,
                        n = !1;
                    do t = e[i] + "", -1 === l(this, t) && (this.push(t), n = !0); while (++i < o);
                    n && this._updateClassName()
                }, h.remove = function() {
                    var t, e, i = arguments,
                        o = 0,
                        n = i.length,
                        s = !1;
                    do
                        for (t = i[o] + "", e = l(this, t); - 1 !== e;) this.splice(e, 1), s = !0, e = l(this, t); while (++o < n);
                    s && this._updateClassName()
                }, h.toggle = function(t, e) {
                    t += "";
                    var i = this.contains(t),
                        o = i ? e !== !0 && "remove" : e !== !1 && "add";
                    return o && this[o](t), e === !0 || e === !1 ? e : !i
                }, h.toString = function() {
                    return this.join(" ")
                }, n.defineProperty) {
                var u = {
                    get: d,
                    enumerable: !0,
                    configurable: !0
                };
                try {
                    n.defineProperty(o, e, u)
                } catch (g) {
                    -2146823252 === g.number && (u.enumerable = !1, n.defineProperty(o, e, u))
                }
            } else n[i].__defineGetter__ && o.__defineGetter__(e, d)
        }
    }(self)), $(window).load(function() {
        $(".ba-slider").each(function() {
            var t = $(this),
                e = t.width() + "px";
            t.find(".resize img").css("width", e), drags(t.find(".handle"), t.find(".resize"), t)
        })
    }), $(window).resize(function() {
        $(".ba-slider").each(function() {
            var t = $(this),
                e = t.width() + "px";
            t.find(".resize img").css("width", e)
        })
    }),
    function() {
        for (var t, e = function() {}, i = ["assert", "clear", "count", "debug", "dir", "dirxml", "error", "exception", "group", "groupCollapsed", "groupEnd", "info", "log", "markTimeline", "profile", "profileEnd", "table", "time", "timeEnd", "timeStamp", "trace", "warn"], o = i.length, n = window.console = window.console || {}; o--;) t = i[o], n[t] || (n[t] = e)
    }(), throttle = function(t, e, i) {
        var o, n, s, r = null,
            a = 0;
        i || (i = {});
        var l = function() {
            a = i.leading === !1 ? 0 : getNow(), r = null, s = t.apply(o, n), r || (o = n = null)
        };
        return function() {
            var c = getNow();
            a || i.leading !== !1 || (a = c);
            var h = e - (c - a);
            return o = this, n = arguments, 0 >= h || h > e ? (r && (clearTimeout(r), r = null), a = c, s = t.apply(o, n), r || (o = n = null)) : r || i.trailing === !1 || (r = setTimeout(l, h)), s
        }
    };
var State = function(t) {
    this.state = "undefined" != typeof t ? t : !1
};
State.prototype.getState = function() {
    return this.state
}, State.prototype.setState = function(t, e) {
    this.state = "undefined" != typeof t ? t : !this.state, "function" == typeof e && e()
};
var mouse = {
    x: 0,
    y: 0,
    xPercent: 0,
    yPercent: 0,
    getPosition: function(t) {
        this.x = t.clientX, this.y = t.clientY, this.xPercent = this.x / window.innerWidth, this.yPercent = this.y / window.innerHeight
    }
};
$(document.body).on("mousemove", throttle(function(t) {
    mouse.getPosition(t)
}, 1e3 / 30));
var MistElement = function(t) {
    this.img = new Image, this.img.src = ASSETS + "/img/shadow-isles/" + this.images[Math.round(Math.random() * (this.images.length - 1))], this.x = Math.random() * mc.width, this.y = mc.height - Math.random() * mc.height * .4 - this.img.height / 2, this.dx = Math.max(.35 * Math.random(), .2), this.dy = .05 * Math.random(), this.alpha = 0, this.dAlpha = .006;
    var e = (.2 * Math.random() + .8) * t.scale;
    this.scale = {
        x: e,
        y: e
    }
};
MistElement.prototype.images = ["smoke-10.png", "smoke-11.png"], MistElement.prototype.update = function() {
    this.x += this.dx, this.y -= this.dy, this.scale.x = Math.min(this.scale.x += 1e-5, 1.3), this.alpha = Math.min(this.alpha += this.dAlpha, .6)
};
var Mist = function(t) {
    this.ctx = t.ctx, this.count = t.count, this.yOffset = t.yOffset || 0, this.scale = t.scale || 1, this._mist = [];
    for (var e = 0; e < this.count; e++) this._mist.push(new MistElement({
        x: mc.width / this.count * e,
        scale: this.scale
    }))
};
Mist.prototype.draw = function() {
    this.ctx.save(), this.ctx.globalCompositeOperation = "lighter";
    for (var t = 0; t < this._mist.length; t++) {
        var e = this._mist[t];
        this.ctx.globalAlpha = e.alpha, this.ctx.drawImage(e.img, e.x, e.y + this.yOffset, e.img.width * e.scale.x, e.img.height)
    }
    this.ctx.restore(), this.update()
}, Mist.prototype.update = function() {
    for (var t = 0; t < this._mist.length; t++) {
        var e = this._mist[t];
        e.update(), (e.x > mc.width || e.alpha < 0) && (this._mist.splice(t, 1), this._mist.push(new MistElement({
            scale: this.scale
        })))
    }
};
var FadingText = function(t, e) {
    this.story = stories.getStoryById(e), this.id = t, this.node = document.getElementById(t), this.letters = [], this.counter = null;
    var i = this;
    this.listener = throttle(function(t) {
        i.inView()
    }, 100, !1), this.init()
};
FadingText.prototype = new State, FadingText.prototype.init = function() {
    this.counter = 0;
    for (var t, e = this.node.innerHTML.split("<br>"), i = [], o = 0; o < e.length; o++) {
        t = e[o];
        for (var n = 0; n < t.length; n++) i.push('<span class="js-fading-text-char" style="opacity: 0">' + t[n] + "</span>");
        i.push("<br>")
    }
    this.node.innerHTML = i.join("");
    for (var s = this.node.getElementsByClassName("js-fading-text-char"), n = 0; n < s.length; n++) this.letters.push({
        letter: s[n],
        opacity: 0
    });
    $(window).on("scroll", this.listener), this.inView()
}, FadingText.prototype.inView = function() {
    var t = document.querySelector("body");
    (t.scrollTop > this.node.offsetTop - .75 * window.innerHeight || t.scrollTop + t.clientHeight >= t.scrollHeight) && (this.setState(!0), this.animate(), $(window).off("scroll", this.listener))
}, FadingText.prototype.animate = function() {
    for (var t = this, e = this.letters, i = null, o = 0; o < this.counter + 1; o++) i = e[o], i.letter.style.opacity = i.opacity, i.opacity = Math.min(i.opacity + .05, 1);
    this.counter = Math.min(this.counter + 1, e.length - 1), e[e.length - 1].letter.style.opacity < 1 && window.requestAnimationFrame(function() {
        t.animate()
    })
}, $(".js-footer-toggle").on("mousedown touchstart", function(t) {
    t.preventDefault(), $(".js-footer").toggleClass("is-active")
});
var $doc = $(document),
    $win = $(window),
    $body = $("body"),
    bodyNode = document.querySelector("body"),
    canvasNodes = document.querySelectorAll(".js-canvas-sizing"),
    mc = {
        width: null,
        height: null,
        ratio: null,
        setMC: function() {
            this.width = window.innerWidth, this.height = Math.min(window.innerHeight, .6 * this.width), this.ratio = this.width / this.height;
            for (var t = 0; t < canvasNodes.length; t++) canvasNodes[t].width = this.width, canvasNodes[t].height = this.height
        }
    };
mc.setMC();
var isMobile = mc.width < 768,
    debounceResize = debounce(function() {
        mc.setMC(), isMobile = mc.width < 768
    }, 10);
$win.on("resize", debounceResize);
var Scene = function(t, e) {
    this.story = e, this.node = t, this.id = this.node.id, this.graphic = {
        node: Modernizr.canvas ? document.getElementById(this.id + "-canvas") : document.getElementById(this.id + "-graphic")
    }, this.height = this.node.clientHeight, this.scrollTop = window.pageYOffset || document.documentElement.scrollTop, this.progressInvalid = !0, this.currentProgress = 0, this.progress = 0;
    var i = this;
    $win.on("load", function() {
        i.init()
    }), $win.on("scroll", throttle(function(t) {
        i.detectInView(), i.setProgress(), i.progressInvalid = !0
    }, 1e3 / 60, i))
};
Scene.prototype = new State, Scene.prototype.inViewOffset = 0, Scene.prototype.init = function() {
    this.height = this.node.clientHeight, this.scrollTop = window.pageYOffset || document.documentElement.scrollTop, this.detectInView(), this.setProgress()
}, Scene.prototype.setProgress = function() {
    this.progressInvalid && this.getState() && this.story.getState() && (this.progress = ((window.pageYOffset || document.documentElement.scrollTop) - this.node.offsetTop) / (bodyNode.scrollHeight - this.node.clientHeight), this.progressInvalid = !1), this.currentProgress = this.currentProgress + .1 * (this.progress - this.currentProgress)
}, Scene.prototype.getProgress = function() {
    return this.setProgress(), this.currentProgress
}, Scene.prototype.detectInView = function() {
    var t = (window.pageYOffset || document.documentElement.scrollTop) + this.inViewOffset >= this.node.offsetTop && (window.pageYOffset || document.documentElement.scrollTop) + this.inViewOffset < this.node.offsetTop + this.node.clientHeight,
        e = t ? "add" : "remove";
    this.node.classList[e]("is-current"), this.graphic.node.classList[e]("is-current"), this.setState(t)
};
var Story = function(t) {
    this._scenes = {}, this.id = t, this.node = document.getElementById(t), this.height = this.node.clientHeight, this.progress = 0, this.currentProgress = 0, this.init()
};
Story.prototype = new State, Story.prototype.init = function() {
    this.setProgress();
    var t = this;
    $win.on("scroll", throttle(function(e) {
        t.setProgress()
    }, 1e3 / 60, t))
}, Story.prototype.addScene = function(t) {
    return this._scenes[t] = new Scene(document.getElementById(t), this), this._scenes[t]
}, Story.prototype.setProgress = function() {
    this.progress = (window.pageYOffset || document.documentElement.scrollTop) / (bodyNode.scrollHeight - this.height), this.currentProgress = this.currentProgress + .1 * (this.progress - this.currentProgress)
}, Story.prototype.getProgress = function() {
    return this.setProgress(), this.currentProgress
}, Story.prototype.getSceneById = function(t) {
    return this._scenes[t]
};
var stories = {
    _stories: {},
    currentStory: null,
    nodeList: document.querySelectorAll(".js-story"),
    count: function() {
        var t, e = 0;
        for (t in this._stories) this._stories.hasOwnProperty(t) && e++;
        return e
    },
    addStory: function(t) {
        return null === this.currentStory && (this.currentStory = t), this._stories[t] = new Story(t), this.setCurrentStory(this.currentStory), this._stories[t]
    },
    getStoryById: function(t) {
        return this._stories[t]
    },
    setCurrentStory: function(t) {
        this.getStoryById(this.currentStory).setState(!1), this.getStoryById(t).setState(!0), this.currentStory = t
    }
};
$(".js-legal-toggle").on("click", function(t) {
    t.preventDefault(), $(this).toggleClass("is-active"), $(".js-legal").toggleClass("is-active")
});
var $nav = $(".js-nav"),
    $navToggleIcons = $(".js-nav-toggle-icon"),
    navInitialState = $nav.hasClass("is-active"),
    nav = new State(navInitialState),
    toggleNav = function() {
        $navToggleIcons.hide(), $navToggleIcons.filter('[data-nav-icon-state="' + nav.getState() + '"]').show();
        var t = nav.getState() ? "addClass" : "removeClass";
        $nav[t]("is-active")
    };
if ($(".js-nav-toggle").on("click", function() {
        nav.setState(void 0, toggleNav)
    }), document.querySelector(".js-novela")) {
    var Marker = function(t) {
        this.index = t.index, this.node = t.node, this.state = 0 === this.index ? !0 : !1, this.top = t.top
    };
    Marker.prototype.getState = function() {
        return this.state
    }, Marker.prototype.setState = function(t) {
        if (this.state !== t) {
            var e = t ? "add" : "remove";
            this.node.classList[e]("is-active")
        }
        this.state = t
    };
    for (var markers = {
            _markers: [],
            addMarker: function(t) {
                this._markers.push(new Marker(t))
            },
            getMarker: function(t) {
                return this._markers[t]
            },
            getCount: function() {
                return this._markers.length
            }
        }, novelaBlockNodes = document.querySelectorAll(".js-novela-block"), positionMarkerNodes = document.querySelectorAll(".js-position-marker"), novelaProgressPositionNode = document.querySelector(".js-novela-progress-position"), markerNodes = document.querySelectorAll(".js-marker"), i = 0; i < novelaBlockNodes.length; i++) {
        var block = novelaBlockNodes[i],
            percentTop = block.offsetTop / (bodyNode.scrollHeight - bodyNode.offsetHeight);
        markers.addMarker({
            index: i,
            node: markerNodes[i],
            state: 0 === i ? !0 : !1,
            top: percentTop
        }), positionMarkerNodes[i].style.top = 100 * percentTop + "%"
    }
    var debouncedProgressPosition = debounce(function() {
        var t = (window.innerWidth - novelaBlockNodes[0].offsetWidth) / 2 - 55;
        novelaProgressPositionNode.style.right = t + "px"
    }, 100);
    debouncedProgressPosition(), $win.on("resize", debouncedProgressPosition);
    var progressMeterNode = document.querySelector(".js-novela-progress-meter"),
        novelaGraphicNode = document.querySelector(".js-novela-graphic"),
        novelaGraphicStyle = {
            opacity: 0,
            display: "block"
        },
        novelaScroller = function() {

            var t = (window.pageYOffset || document.documentElement.scrollTop) / (bodyNode.scrollHeight - bodyNode.offsetHeight);
            progressMeterNode.style.height = 100 * t + "%";
            var c = 0;
            var ee = 0;

            for (var e, i, o = 0; o < markers.getCount(); o++) {
            	e = markers.getMarker(o), i = t >= e.top, e.setState(i);
				tt = parseFloat(t + 0.08);
            	if(tt >= e.top ) c = o, ee =e;
            }

            if($('.c-novela__graphic-img').eq(c).is(':hidden')){
            	$('.c-novela__graphic-img').hide();
				$('.c-novela__graphic-img').eq(c).removeClass("hide").show();
            }

            var d = Math.abs(ee.top - t);

            if(d < 0.1){
                var i = Math.max(0, Math.min(1, 1 - 10 * d));
                if(i > 0.4) novelaGraphicStyle.opacity = i, novelaGraphicStyle.display = novelaGraphicStyle.opacity ? "block" : "none", novelaGraphicNode.style.opacity = novelaGraphicStyle.opacity, novelaGraphicNode.style.display = novelaGraphicStyle.display
            }
        };
    $win.on("scroll", throttle(function(t) {
        novelaScroller()
    }, 1e3 / 60, self))
}
var Point = function(t) {
    this.node = t, this.id = this.node.id, this.topOffset = 0, this.bottomOffset = 0, this.message = this.node.dataset.message;
    var e = this;
    $win.on("load resize", function() {
        e.init()
    })
};
Point.prototype = new State, Point.prototype.init = function() {
    this.topOffset = this.node.offsetTop, this.bottomOffset = Math.min(this.node.offsetTop + window.innerHeight, document.body.scrollHeight)
};
var scrollLogging = {
    _points: [],
    scrollPosition: window.pageYOffset || document.documentElement.scrollTop,
    addPoint: function(t) {
        this._points.push(new Point(t))
    },
    getPoint: function(t) {
        return this._points[t]
    },
    testPoints: function() {
        var t = window.pageYOffset || document.documentElement.scrollTop;
        if ("function" == typeof window.ping) {
            var e, i = t > this.scrollPosition ? "down" : "up";
            if (document.body.scrollHeight - window.innerHeight === t) console.log("page-bottom-down"), window.ping("page-bottom-down", {
                "": ""
            });
            else if (0 === window.pageYOffset) console.log("page-top-up"), window.ping("page-top-up", {
                "": ""
            });
            else
                for (var o = 0; o < this._points.length; o++) {
                    var n = !1;
                    e = this.getPoint(o), e.getState() ? t + window.innerHeight <= e.bottomOffset && t + window.innerHeight >= e.topOffset && (n = e.message, e.setState(!1)) : (t + window.innerHeight > e.bottomOffset || t + window.innerHeight < e.topOffset) && e.setState(!0), n && (n += "-" + i, console.log(n), window.ping(n, {
                        "": ""
                    }))
                }
        }
        this.scrollPosition = t
    },
    init: function() {
        for (var t = document.querySelectorAll(".js-scroll-logging"), e = 0; e < t.length; e++) {
            var i = t[e];
            scrollLogging.addPoint(i)
        }
    }
};
scrollLogging.init(), $win.on("scroll", throttle(function(t) {
    nav.setState(!1, toggleNav), scrollLogging.testPoints()
}, 100, self)), $win.on("load", function() {
    if ($("#alert-cookie-policy").length > 0) {
        var t = $("#riotbar-alerts").height() + 40;
        $body.css("padding-top", t)
    }
    $(document.body).on("click", "#cookie-policy-agree", function(t) {
        t.preventDefault(), $body.css("padding-top", 40)
    }), scrollLogging.testPoints(), bodyNode.classList.add("is-loaded")
});