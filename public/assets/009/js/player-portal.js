var playerPortal = {
	account: {
		initialize: function() {

			var $sidebar = $("#sidebar");
			$sidebar.on("click", ".remove-subscription", function(event) {
				event.preventDefault();
				var threadid = $(this).data('threadid');

				//Remove the favorite.
				$(this).closest(".subscribed-thread").slideUp("fast", function() {
					$(this).remove();

					if ($sidebar.find(".my-subscriptions-list a").length == 0) {
						$sidebar.find(".my-subscriptions-list").html("<em>You are not subscribed to any threads.</em>");
					}
				});

				$.ajax({
					method: "POST",
					url: "/ajax/forums/unfavoriteThread/" + threadid,
					success: function(data) {
						threadStar.toggleClass("subscribed unsubscribed");
					},
					error: function(data) {
						//Display an alert of the error.
						alert(data.responseJSON.error);
					}
				});
			});

			var changePasswordForm = $("#change-password-form").validate({
				rules: {
					"oldPassword": {
						required: true
					},
					"newPassword": {
						required: true
					},
					"conPassword": {
						required: true,
						equalTo: "#newPassword"
					}
				},
				messages: {
					"conPassword": {
						equalTo: "Your passwords do not match."
					}
				},
				submitHandler: function(form) {

					var data = $(form).serialize();
					//AJAX POST.
					$.ajax({
						method: "POST",
						url: "/ajax/account/savePassword/",
						data: data,
						success: function() {
							alert('Your Password has been successfully changed.');
						},
						error: function(data) {
							//Display an alert of the error.
							alert(data.responseJSON.error);
						}
					});
				}
			});

			var accountDetailsForm = $(".account-details").validate({
				rules: {
					"nickname": {
						required: true
					},
					"email": {
						required: true,
						email: true
					},
					"password": {
						required: true
					}
				},
				submitHandler: function(form) {

					var data = $(form).serialize();
					//AJAX POST.
					$.ajax({
						method: "POST",
						url: "/ajax/account/updateAccountDetails/",
						data: data,
						success: function() {
							alert('Your Account has been updated.');
							window.location.reload();
						},
						error: function(data) {
							//Display an alert of the error.
							alert(data.responseJSON.error);
						}
					});
				}
			});

			var removeSavedCards = $('.remove-card').on('click', function()
			{
				if (!confirm('Are you sure you wish to remove this card?'))
				{
					return;
				}

				var cardParent = $(this).parent();
				$.ajax({
					method: "POST",
					url: "/ajax/account/removeCard/",
					data: {"profileId": cardParent.find('input[name="profileId"]').val()},
					success: function()
					{
						cardParent.fadeOut(400, function()
						{
							$(this).remove();
							if ($('.saved-credit-cards-wrapper').find('.credit-card-wrapper').length === 0)
							{
								$('.saved-credit-cards-wrapper').parent().fadeOut(400, function()
								{
									$(this).remove();
								});
							}
						});
					},
					error: function(data)
					{
						//Display an alert of the error.
						alert(data.responseJSON.error);
					}
				});
			});

			var referFriend = $( '#refer-friend-email' ).validate({
				errorPlacement: function(error, element) {
					error.hide().insertAfter(element).fadeIn("slow");
				},
				highlight: function(element, errorClass) {
					$(element).closest(".form-element").addClass("caused-error");
				},
				unhighlight: function(element, errorClass) {
					$(element).closest(".form-element").removeClass("caused-error");
				},
				rules: {
					"refer_email": {
						required: true,
						email: true
					}
				},
			});

		}
	},
	resetPassword: {
		initialize: function() {
			var resetPasswordForm = $("#new-password-form").validate({
				rules: {
					"password": {
						required: true
					},
					"confirm-password": {
						required: true,
						equalTo: "#password"
					}
				},
				messages: {
					"confirm-password": {
						equalTo: "Your passwords do not match."
					}
				},
				submitHandler: function(form) {

					var data = $(form).serialize();
					//AJAX POST.
					$.ajax({
						method: "POST",
						url: "/ajax/account/resetPassword/",
						data: data,
						success: function() {
							confirm('Your Password has been successfully changed.');
							document.location.href = '/login';
						},
						error: function(data) {
							//Display an alert of the error.
							alert(data.responseJSON.error);
						}
					});
				}
			});
		}
	},
	signup: {
		initialize: function() {
			var signupForm = $("#sign-up-form").validate({
				errorPlacement: function(error, element) {
					if (element.attr("name") === "signup-for-newsletter") {
						error.hide().insertAfter(element.closest(".checkbox-label")).fadeIn("slow");
					} else {
						error.hide().insertAfter(element).fadeIn("slow");
					}
				},
				highlight: function(element, errorClass) {
					$(element).closest(".form-element").addClass("caused-error");
				},
				unhighlight: function(element, errorClass) {
					$(element).closest(".form-element").removeClass("caused-error");
				},
				rules: {
					"fields_email": {
						required: true,
						email: true
					},
				},
				submitHandler: function(form) {

					var data = $(form).serialize();

					form.submit();

				}
			});
		}
	},
	download: {
		initialize: function() {
			var $systemRequirementsWrapper = $("#system-requirements-wrapper");
			var $systemRequirements = $("#system-requirements");

			// when clicking a download option, toggle the selected class
			$("#download-options").on("click", ".image-wrapper", function() {

				if (!$(this).hasClass("image-wrapper--linux")) {
					playerPortal.general.markSelectedItem($(this).closest(".three-column"), $("#download-options .three-column").not(this));
				}

			});

			// when clicking a download option's button, toggle the selected class
			$("#download-options").on("click", ".button", function() {
				playerPortal.general.markSelectedItem($(this).closest(".three-column"), $("#download-options .three-column").not($(this).closest(".three-column")));
				playerPortal.general.markSelectedItem($(this), $("#download-options .button").not($(this)), "downloading", "not-downloading");
			});

			// expand the system requirements container when clicking on the button
			$systemRequirementsWrapper.on("click", ".system-requirements-button", function(event) {
				event.preventDefault();
				$(this).toggleClass("expanded");
				$systemRequirements.slideToggle("fast");
			});

		}

	},
	heroes: {

		initialize: function() {

			$('.ability-wrapper').find('.ability').on('click', function(){

				$('.ability-wrapper .ability').removeClass('selected');
				$(this).addClass('selected');

				$(".ability-description").each(function() {

					if ($(this).is(":visible")) {
						previouslySelectedDiv = "#" + $(this).attr("id");
					}

				});

				selectedDiv = "#" + $(this).data("ability");

				if (!$(selectedDiv).is(":visible")) {

					$(previouslySelectedDiv).fadeOut(200,function() {
						$(selectedDiv).fadeIn(300);
					})

				}

			});

			$('.gearset-wrapper').find('.gearset-icon').on('click', function(){

				selectedDiv = "#" + $(this).data("gear");

				if (selectedDiv != "#undefined"){
					$('.gearset-wrapper .gearset-icon').removeClass('selected');
					$('.gearset-wrapper .gearset-icon[data-gear=' + $(this).data("gear") + ']').addClass('selected');
				}

				$(".header-right-container").each(function() {

					if ($(this).is(":visible")) {
						previouslySelectedDiv = "#" + $(this).attr("id");
					}

				});

				if (!$(selectedDiv).is(":visible") && selectedDiv !="#undefined") {

					$(previouslySelectedDiv).fadeOut(200,function() {
						$(selectedDiv).fadeIn(300);
					})
				}

			});

			$('.guides-wrapper').on('click', '.guides', function(){

				var previouslySelectedDiv;

				$('.guides-wrapper .guides').removeClass('selected');
				$(this).addClass('selected');

				$(".guides-layout").each(function() {

					if ($(this).is(":visible")) {
						previouslySelectedDiv = "#" + $(this).attr("id");
					}

				});

				var selectedDiv = "#" + $(this).data("guide");

				if (!$(selectedDiv).is(":visible")) {
					$(previouslySelectedDiv).fadeOut('fast', function() {
						$(selectedDiv).fadeIn('fast');
					})
				}

			});

			$('.item-box').find('.item').on('click', function(){

				$('.item-box .item').removeClass('selected');
				$(this).addClass('selected');

				$(".item-info-col").each(function() {

					if ($(this).is(":visible")) {
						previouslySelectedDiv = "#" + $(this).attr("id");
					}

				});

				selectedDiv = "#" + $(this).data("item");

				if (!$(selectedDiv).is(":visible")) {

					$(previouslySelectedDiv).fadeOut(function() {
						$(selectedDiv).fadeIn().css('display', 'inline-block');
					});

				}

			});

			$('.item-col').each(function() {
				$(this).find('.item-info-col').first().css({'display': 'inline-block'});
			});

			$abilities = $(".ability-wrapper");

			$(document).on("keypress", function(event) {

				switch (event.which) {

					case 113:
					case 81:
						$abilities.find("[data-key='q']").trigger("click");
						break;

					case 119:
					case 87:
						$abilities.find("[data-key='w']").trigger("click");
						break;

					case 101:
					case 69:
						$abilities.find("[data-key='e']").trigger("click");
						break;

					case 114:
					case 82:
						$abilities.find("[data-key='r']").trigger("click");
						break;

				}

			});

		}

		// initialize: function() {
		// 	$abilities = $("#abilities");
		// 	$gearsets = $("#gearsets");
		// 	$hero = $(".hero-model-image-wrapper");
		// 	$concept = $(".hero");

		// 	$abilities.on("click", ".skill", function() {
		// 		$abilities.find(".skill").removeClass("selected");
		// 		$abilities.find(".ability-description").removeClass("visible");

		// 		$(this).addClass("selected");
		// 		$abilities.find(".ability-description.ability-" + $(this).data("ability")).addClass("visible");

		// 	});

		// 	$gearsets.on("click", ".aesthetic", function() {
		// 		$gearsets.find(".aesthetic").removeClass("selected");
		// 		$hero.find(".gearset").removeClass("visible");

		// 		$(this).addClass("selected");
		// 		$hero.find(".set-" + $(this).data("gearset")).addClass("visible");


		// 		$concept.attr("data-gearset", $(this).data("gearset"));
		// 		console.log($concept.data("gearset"))

		// 	});

		// }
	},

	pets: {
		initialize: function() {
			$pets = $("#pets");
			$descriptions = $(".description-container");
			$selector = $("#selector");
			$familliar = $(".familliar");
			$ability = $(".ability-container")
			$form = $("#form-container")

			// pet switch
			$selector.on("click", ".familliar", function() {
				// console.log($(this).data("pet"))
				$selector.find(".familliar").removeClass("selected");
				$selector.find(".pet-content").removeClass("visible");

				$(this).addClass("selected");
				$(".pet-content").removeClass("visible");
				$("#" + $(this).data("pet")).addClass("visible");

				// resets when clicking a new pet
				$pets.find(".skin").removeClass("selected");
				$pets.find(".ability-description").removeClass("visible");
				$pets.find(".form").removeClass("visible");

				// default selection when changing pets
				$("#abilities-" + $(this).data("pet")).find(".ability-a").addClass("selected");
				$("#descriptions-" + $(this).data("pet")).find(".ability-a").addClass("visible");
				$("#form-" + $(this).data("pet")).find(".skin-1").addClass("selected");
				$("#" + $(this).data("pet")).find(".form.skin-1").addClass("visible");
			});

			// ability switch
			$pets.on("click", ".skill", function() {
				// console.log($(this).data("ability"))
				$pets.find(".skill").removeClass("selected");
				$descriptions.find(".ability-description").removeClass("visible");

				$(this).addClass("selected");
				$descriptions.find(".ability-description.ability-" + $(this).data("ability")).addClass("visible");

			});

			// model skin switch
			$pets.on("click", ".skin", function() {
				// console.log($(this).data("skin"))
				$pets.find(".skin").removeClass("selected");
				$pets.find(".form").removeClass("visible");

				$(this).addClass("selected");
				$pets.find(".form.skin-" + $(this).data("skin")).addClass("visible");
			});

			// check query string
			if (QueryString.pet) {
				$("#selector").find("li." + QueryString.pet).trigger("click");
			}

		}
	},

	home: {
		initialize: function() {

			$(".browse-articles").on("click", ".year-header", function() {
				$(this).closest("li").toggleClass("expanded").find(".archived-articles-list").slideToggle("fast");
			});

		}

	},

	items: {
		breakPoint: 0,

		initialize: function() {

			// grid calculations

			playerPortal.items.calculateLineBreak();

			$(".items__item").on("click", function(event) {
				var newElement,
					$afterElement,
					that = this;

				$(".items__item.items__item--expansion").slideUp({
					complete: function() {
						$(this).remove();
						$('.ps-wrapper').perfectScrollbar('update');
					}
				});

				if ($(this).hasClass("is-active")) {

					$(this).removeClass("is-active");

				} else {

					if ( $(that).hasClass("items__item--last-line-item") ) {
						$afterElement = $(that);
					} else {
						$afterElement = $(that).nextAll(".items__item--last-line-item").first();
					}

					$afterElement.after(function() {
						newElement = document.createElement("li");
						$(newElement).addClass("items__item items__item--expansion").html(
							$(that).find(".item-details").html()
						);
						return(newElement);
					});

					$(newElement).slideDown({
						complete: function() {
							$(this).css({
								display: "block"
							});
						}
					});

					$(".items__item.is-active").removeClass("is-active");
					$(that).addClass("is-active");

				}

			});

			$(window).resize(

				$.throttle(500, function() {
					playerPortal.items.calculateLineBreak()
				})

			);

			// item sort

			$filterBar = $(".filter-bar");
			$filterBarItems = $filterBar.find(".filter-bar__item");
			$filteredItemsContainer = $("ul.items");
			$filteredItems = $filteredItemsContainer.find("> .items__item");

			$filterBar.on("click", ".filter-bar__item", function() {
				var filter = $(this).data("filter");

				$filterBarItems.removeClass("is-active");
				$(this).addClass("is-active");

				if (filter == "*") {
					$filteredItems.removeClass("is-hidden").addClass("is-visible");
				} else {
					$filteredItemsContainer.find("> .items__item").not("." + filter).addClass("is-hidden").removeClass("is-visible");
					$filteredItemsContainer.find("> .items__item." + filter).removeClass("is-hidden").addClass("is-visible");
				}

				$(".item-group").each(function() {

					if ($(this).find(".items > .is-visible").length == 0) {

						$(this).find("h3").hide();

					} else {

						$(this).find("h3").show();

					}

				});

				playerPortal.items.calculateLineBreak(true);
			});

		},

		calculateLineBreak: function (forceRecalculate) {
			// console.log("it ran");

			$(".items").each(function () {
				var containerWidth = $(this).outerWidth(true),
					itemWidth = $(this).find(".items__item.is-visible").first().outerWidth(true),
					newBreakPoint;

				newBreakPoint = Math.floor(containerWidth / itemWidth);

				if ( (newBreakPoint != $(this).data("break-point")) || forceRecalculate == true) {
					var count = 0;

					$(this).find(".items__item--last-line-item").removeClass("items__item--last-line-item");
					$(this).find(".items__item.is-active").removeClass("is-active");
					$(this).find(".items__item--expansion").remove();

					$(this).find(".items__item.is-visible").each(function() {
						count++;

						if (count == newBreakPoint) {
							$(this).addClass("items__item--last-line-item");
							count = 0;
						}

					});

					$(this).find(".items__item.is-visible").last().addClass("items__item--last-line-item");
					$(this).data("break-point", newBreakPoint);
				}

			});

		}

	},
	forums: {
		general: {
			initialize: function() {

				var $sidebar = $("#sidebar");
				$sidebar.on("click", ".remove-subscription", function(event) {
					event.preventDefault();
					var threadid = $(this).data('threadid');

					//Remove the favorite.
					$(this).closest(".subscribed-thread").slideUp("fast", function() {
						$(this).remove();

						if ($sidebar.find(".my-subscriptions-list a").length == 0) {
							$sidebar.find(".my-subscriptions-list").html("<em>You are not subscribed to any threads.</em>");
						}
					});


					$.ajax({
						method: "POST",
						url: "/ajax/forums/unfavoriteThread/" + threadid,
						success: function(data) {
							threadStar.toggleClass("subscribed unsubscribed");
						},
						error: function(data) {
							//Display an alert of the error.
							alert(data.responseJSON.error);
						}
					});
				});

				// when the user clicks one of the nested replies button, show the nested replies
				$(".nested-replies").on("click", ".nested-replies-button", function() {
					var $button = $(this);
					$nestedWrapper = $(this).parent().find(".nested-thread-post-wrapper");
					playerPortal.forums.replies.toggleVisibility($button, $nestedWrapper);
				});

				$(".parent-posts").on("click", ".parent-posts-button", function() {
					var $button = $(this);
					$nestedWrapper = $button.closest(".parent-posts").find(".parent-thread-post-wrapper");
					playerPortal.forums.replies.toggleVisibility($button, $nestedWrapper);
				});

				// when the footer is visible, remove the "fixed" class on the post-reply box
				var $forumFooter = $("#forum-footer");

				if (playerPortal.forums.replies.isInsideViewport($(".footer-wrapper"), $forumFooter)) {
					$forumFooter.addClass("fixed");
				} else {
					$forumFooter.removeClass("fixed");
				}

				$(document).scroll(
					function() {
						if (playerPortal.forums.replies.isInsideViewport($(".footer-wrapper"), $forumFooter)) {
							$forumFooter.addClass("fixed");
						} else {
							$forumFooter.removeClass("fixed");
						}
					});
			}

		},
		replies: {
			initialize: function() {

				if (QueryString.postid) {
					var selector = ".main-thread-post[data-post-number='" + QueryString.postid + "']";
					$("html, body").scrollTop($(selector).offset().top);
				}

				// when a user clicks on any "reply" button, show the post reply box
				var $postReply = $("#post-reply");
				var $forumFooterContainer = $("#forum-footer-container");

				$("#forums, #forum-footer").on("click", ".button.reply, .reply-to-thread", function(event) {
					var $replyToText = $postReply.find(".replying-to");
					var $postReplyHeader = $postReply.find(".post-reply-header");

					event.preventDefault();

					if ($(this).hasClass("reply-to-thread")) {

						if ($postReply.is(":visible")) {
							$postReplyHeader.animate({opacity: 0}, function() {
								$replyToText.text($("#forums h1.thread-title").text());
								$postReplyHeader.animate({opacity: 1});
							});
						} else {
							$replyToText.text($("#forums h1.thread-title").text());
						}

					} else {
						var that = this;
						var postid = $(this).data('postid');

						//Adding an extra hidden form element.
						//But first destroy any existing ones.
						$('#post-parent-id').remove();
						var postidTag = document.createElement('input');
						postidTag.setAttribute('type', 'hidden');
						postidTag.setAttribute('name', 'parent_id');
						postidTag.setAttribute('value', postid);
						postidTag.setAttribute('id', 'post-parent-id');
						$("#post-reply-form").append(postidTag);


						if ($postReply.is(":visible")) {
							$postReplyHeader.animate({opacity: 0}, function() {
								$replyToText.text($(that).closest(".main-thread-post").find("> .post-content > .user-details").find(".username").text());
								$postReplyHeader.animate({opacity: 1});
							});
						} else {
							$replyToText.text($(that).closest(".main-thread-post").find("> .post-content > .user-details").find(".username").text());
						}

					}

					$forumFooterContainer.slideUp({duration: 200, queue: false}).fadeOut({duration: 200, queue: false});
					$postReply.slideDown({duration: 200, queue: false, always: function() {
							if ($("html").hasClass("touch")) {
								$("html, body").animate({
									scrollTop: ($("#forum-footer").offset().top - 50)
								}, 500);
							}
							$postReply.find("textarea").focus();
						}}).removeClass("no-padding");

				});

				// change the fixed footer text show the post number
				$(".main-thread-post").hover(
					function() {
						that = this;
						$forumFooterContainer.find(".thread-title").stop().fadeOut(function() {

							$forumFooterContainer.find(".post-information").stop().fadeIn(
								function() {
									if ($forumFooterContainer.find(".post-information").css("opacity") < 1) {
										$forumFooterContainer.find(".post-information").css({"opacity": 1});
									}
								}

							).html("Post #" + $(that).data("post-number"));

						});

					},
					function() {

						$forumFooterContainer.find(".post-information").stop().fadeOut(function() {
							$forumFooterContainer.find(".thread-title").stop().fadeIn({always: function() {
									$(this).css({"opacity": 1});
								}});
						});

					}
				);

				// when a user clicks on the "cancel" button in the post reply box, hide the box
				$postReply.on("click", ".cancel", function() {
					//Remove our post-parent-id hidden form element.
					$('#post-parent-id').remove();
					$forumFooterContainer.fadeIn({duration: 200, queue: false}).slideDown({duration: 200, queue: false});
					$postReply.slideUp({duration: 200, queue: false, start: function() {
							var docViewTop = $(window).scrollTop();
							var docViewBottom = docViewTop + $(window).height();
							var footerWrapperTop = $(".footer-wrapper").offset().top;
							if (docViewBottom > footerWrapperTop) {
								$("#forum-footer").removeClass("fixed");
							}
							postReplyForm.resetForm();
						}}).addClass("no-padding");
				});

				// validate that the textarea has text in it when the user submits the form
				var postReplyForm = $("#post-reply-form").validate({
					rules: {
						"text": {
							required: true
						}
					},
					submitHandler: function(form) {

						$postReply.find("input[type='submit']").attr("disabled", "disabled");
						$postReply.addClass("submitting-form");

						var data = $(form).serialize();
						var threadid = $(form).data('threadid');

						//AJAX POST.
						$.ajax({
							method: "POST",
							url: "/ajax/forums/createPost/" + threadid,
							data: data,
							success: function(data) {
								window.location.replace('/forums/thread/' + data.thread.thread_id + '/' + data.page + '?postid=' + data.post.post_id);
							},
							error: function(data) {
								//Display an alert of the error.
								alert(data.responseJSON.error);
								$postReply.removeClass("submitting-form");
								$postReply.find("input[type='submit']").removeAttr("disabled");
							}
						});
					}
				});

				// show the edit form when a user clicks on the "edit" button
				$(".post-footer").on("click", ".edit", function(event) {
					event.preventDefault();

					var $editButton = $(this);
					var postid = $($editButton).data('postid');
					var $postFooter;
					var $editFormWrapper;

					$postFooter = $editButton.closest(".post-footer");
					$editFormWrapper = $postFooter.find(".edit-form-wrapper");

					//Get the raw post data.
					$.ajax({
						method: "POST",
						url: "/ajax/forums/post/" + postid,
						success: function(data) {
							$postFooter.find("#edit-text").val(data.post.text);
							// after the post content has been successfully retrieved from the server, run the next few lines of code...
							$editButton.fadeOut("fast", function() {
								$postFooter.removeClass("loading").addClass("editing");
							});
							$editFormWrapper.slideDown("fast");
						},
						error: function(data) {
							//Display an alert of the error.
							alert(data.responseJSON.error);
							//End any further execution of the code?
							$postFooter.removeClass("editing loading");
						}
					});

					if ((!$postFooter.hasClass("loading")) && (!$postFooter.hasClass("editing"))) {
						$editButton.text("Loading");
						$postFooter.addClass("loading");
					}

					$editPostForm = $editFormWrapper.find(".edit-form").validate({
						rules: {
							"edit-text": {
								required: true
							}
						},
						submitHandler: function(form) {

							$editButton.attr("disabled", "disabled");
							$editFormWrapper.addClass("submitting-form");

							var data = $(form).serialize();

							//AJAX POST.
							$.ajax({
								method: "POST",
								url: "/ajax/forums/editPost/" + postid,
								data: data,
								success: function() {
									window.location.reload();
								},
								error: function(data) {
									//Display an alert of the error.
									alert(data.responseJSON.error);
									$editFormWrapper.removeClass("submitting-form");
									$editButton.removeAttr("disabled");
								}
							});
						}
					});

				});

				$(".post-footer").on("click", ".cancel", function(event) {
					$cancelButton = $(this);
					$postFooter = $cancelButton.closest(".post-footer");
					$editFormWrapper = $postFooter.find(".edit-form-wrapper");
					$editButton = $postFooter.find(".edit.button");

					$editButton.text("Edit");
					$editButton.fadeIn("fast");
					$postFooter.removeClass("editing loading");
					$editFormWrapper.slideUp("fast");
				});

				// Report a post.
				$(".post-footer").on("click", ".report", function(event) {
					event.preventDefault();
					var postid = $(this).data('postid');

					if (confirm('Are you sure you want to report this post?'))
					{
						$.ajax({
							method: "POST",
							url: "/ajax/forums/editPost/" + postid,
							data: "reported=1",
							success: function(data) {
								alert('Thanks! This post has been reported.');
							},
							error: function(data) {
								//Display an alert of the error.
								alert(data.responseJSON.error);
							}
						});

						$(this).remove();
					}
				});

				// Delete a post.
				$(".post-footer").on("click", ".delete-post", function(event) {
					event.preventDefault();
					var postid = $(this).data('postid');
					var $forumPost = $(this).closest(".main-thread-post");

					$.ajax({
						method: "POST",
						url: "/ajax/forums/editPost/" + postid,
						data: "deleted=1",
						success: function(data) {
							$forumPost.fadeOut({queue: false}).slideUp({queue: false});
						},
						error: function(data) {
							//Display an alert of the error.
							alert(data.responseJSON.error);
						}
					});
				});

			},
			isInsideViewport: function(elem, fixedElement) {
				// checks to see if an elemnt is visible in the viewport and if it's above the fixed element's coverage

				var docViewTop = $(window).scrollTop();
				var docViewBottom = docViewTop + $(window).height();
				var elemTop = $(elem).offset().top;
				var elemBottom = elemTop + $(elem).height();
				var fixedElementHeight = fixedElement.outerHeight(true);

				if (fixedElement.hasClass("fixed")) {
					return ((elemTop >= (docViewBottom - fixedElementHeight)));
				} else {
					return ((elemTop >= (docViewBottom)));
				}

			},
			toggleVisibility: function($button, $wrapper) {
				// toggles the "expanded" on the wrapper class and shows or hides it accordingly

				if ($wrapper.is(":visible")) {
					$wrapper.slideToggle("fast", function() {
						$button.toggleClass("expanded");
					});
				} else {
					$button.toggleClass("expanded");
					$wrapper.slideToggle("fast");
				}

			}

		},
		threadList: {
			initialize: function() {
				var $forums = $("#forums");
				var $sidebar = $("#sidebar");
				var $subscriptions = $("#my-subscriptions-list");
				var $newThreadButton = $("#new-thread-button");
				var $newThreadForm = $("#new-thread-form");
				var $newThreadFormWrapper = $("#new-thread-form-wrapper");

				//Jump to the latest post.
				$forums.on("click" , ".last-post-arrow" , function(event)
				{
					event.preventDefault();
					document.location.href = $(this).data('href');
				});

				// when the user clicks the star on the thread list, toggle the subscribed class on the star
				// need to add the functionality to remove this entry form the sidebar and the database entry
				$forums.on("click", ".subscribe-star", function(event) {
					event.preventDefault();
					var threadid = $(this).data('threadid');
					var targetURL = '';
					if ($(this).hasClass('subscribed unsubscribed'))
					{
						targetURL = "/ajax/forums/unfavoriteThread/" + threadid;
					}
					else
					{
						targetURL = "/ajax/forums/favoriteThread/" + threadid;
					}

					//Turn on/off that sweet star.
					$(this).toggleClass("subscribed unsubscribed");

					$.ajax({
						method: "POST",
						url: targetURL,
						success: function(data) {
							//Reload the favorites bar on the side.
							$subscriptions.empty();
							for (var threadid in data.threads) {
								var url = '/forums/thread/' + threadid;
								var anchor = document.createElement('a');
								var title = document.createElement('span');
								var footer = document.createElement('span');
								var replies = document.createElement('span');
								var removeSubs = document.createElement('span');

								removeSubs.setAttribute('class', 'remove-subscription');
								removeSubs.setAttribute('data-threadid', threadid);
								removeSubs.appendChild(document.createTextNode('Remove'));
								replies.setAttribute('class', 'thread-replies');
								replies.appendChild(document.createTextNode('Replies ' + data.threads[threadid].replies));

								footer.setAttribute('class', 'subscribed-thread-footer');
								footer.appendChild(replies);
								footer.appendChild(removeSubs);

								title.setAttribute('class', 'thread-title');
								title.appendChild(document.createTextNode(data.threads[threadid].title));

								anchor.setAttribute('class', 'subscribed-thread new-content');
								anchor.setAttribute('href', url);
								anchor.appendChild(title);
								anchor.appendChild(footer);

								$subscriptions.append(anchor);
							}
							if ($sidebar.find(".my-subscriptions-list a").length == 0) {
								$sidebar.find(".my-subscriptions-list").html("<em>You are not subscribed to any threads.</em>");
							}
						},
						error: function(data) {
							//Display an alert of the error.
							alert(data.responseJSON.error);
						}
					});
				});

				// when a moderator clicks the delete icon in a thread, prompt them, and then remove the thread
				$forums.on("click", ".delete-thread", function(event) {
					event.preventDefault();
					var $forumThread = $(this).closest(".forum-thread");
					var threadTitle = $forumThread.find(".thread-title").text();
					var confirmDelete = confirm("Do you really want to delete the following thread?\n" + threadTitle + "\nThis action cannot be undone.");

					if (confirmDelete) {
						var threadid = $(this).data('threadid');

						$.ajax({
							method: "POST",
							url: "/ajax/forums/editThread/" + threadid,
							data: 'deleted=1',
							success: function(data) {
								$forumThread.fadeOut({queue: false}).slideUp({queue: false});
							},
							error: function(data) {
								//Display an alert of the error.
								alert(data.responseJSON.error);
							}
						});
					}

				});

				// Locking a thread.
				$forums.on("click", ".lock-thread", function(event) {
					event.preventDefault();
					var $forumThread = $(this).closest(".forum-thread");
					var threadid = $(this).data('threadid');
					var locked;

					if ($forumThread.hasClass('locked'))
					{
						locked = '0';
					}
					else
					{
						locked = '1';
					}

					$.ajax({
						method: "POST",
						url: "/ajax/forums/editThread/" + threadid,
						data: 'locked=' + locked,
						success: function(data) {
							if ($forumThread.hasClass('locked'))
							{
								$forumThread.removeClass('locked');
							}
							else
							{
								$forumThread.addClass('locked');
							}
						},
						error: function(data) {
							//Display an alert of the error.
							alert(data.responseJSON.error);
						}
					});
				});

				// Sticky a thread.
				$forums.on("click", ".sticky-thread", function(event) {
					event.preventDefault();
					var $forumThread = $(this).closest(".forum-thread");
					var threadid = $(this).data('threadid');
					var sticky;

					if ($forumThread.hasClass('sticky'))
					{
						sticky = '0';
					}
					else
					{
						sticky = '1';
					}

					$.ajax({
						method: "POST",
						url: "/ajax/forums/editThread/" + threadid,
						data: 'sticky=' + sticky,
						success: function(data) {
							document.location.href = '/forums/topic/' + data.thread.topic_id;
						},
						error: function(data) {
							//Display an alert of the error.
							alert(data.responseJSON.error);
						}
					});
				});

				// when the user clicks the "new thread" button, show the form
				$newThreadButton.on("click", function(event) {
					event.preventDefault();
					$newThreadButton.fadeOut({duration: "fast", queue: false});
					$newThreadFormWrapper.slideDown({queue: false}).fadeIn();
				});

				$newThreadFormWrapper.on("click", ".cancel", function(event) {
					$newThreadButton.fadeIn({queue: false});
					$newThreadFormWrapper.slideUp({queue: false}).fadeOut();
					newThreadForm.resetForm();
					$newThreadFormWrapper.find(".caused-error").removeClass("caused-error");
				});

				// validate the "new thread" form
				var newThreadForm = $newThreadForm.validate({
					errorPlacement: function(error, element) {
						error.hide().insertAfter(element).fadeIn("slow");
					},
					highlight: function(element, errorClass) {
						$(element).closest(".form-element").addClass("caused-error");
					},
					unhighlight: function(element, errorClass) {
						$(element).closest(".form-element").removeClass("caused-error");
					},
					rules: {
						"title": {
							required: true
						},
						"text": {
							required: true
						}
					},
					submitHandler: function(form) {

						var data = $(form).serialize();
						var topicid = $($newThreadForm).data('topicid');

						$newThreadForm.find("input[type='submit']").attr("disabled", "disabled");
						$newThreadForm.addClass("submitting-form");

						//AJAX POST.
						$.ajax({
							method: "POST",
							url: "/ajax/forums/createThread/" + topicid,
							data: data,
							success: function(data) {
								document.location.href = '/forums/thread/' + data.thread_id;
							},
							error: function(data) {
								//Display an alert of the error.
								alert(data.responseJSON.error);
								$newThreadForm.removeClass("submitting-form");
								$newThreadForm.find("input[type='submit']").removeAttr("disabled");
							}
						});
					}
				});

				// when the user clicks the "remove" text on the "my subscriptions" sidebar, remove that entry from the sidebar
				// need to add the functionality to remove this entry form the sidebar and the database entry
				$sidebar.on("click", ".remove-subscription", function(event) {
					event.preventDefault();
					var threadid = $(this).data('threadid');
					var threadStar = $(".subscribe-star[data-threadid='" + threadid + "']");

					//Remove the favorite.
					$(this).closest(".subscribed-thread").slideUp("fast", function() {
						$(this).remove();

						if ($sidebar.find(".my-subscriptions-list a").length == 0) {
							$sidebar.find(".my-subscriptions-list").html("<em>You are not subscribed to any threads.</em>");
						}

					});

					$.ajax({
						method: "POST",
						url: "/ajax/forums/unfavoriteThread/" + threadid,
						success: function(data) {
							threadStar.toggleClass("subscribed unsubscribed");
						},
						error: function(data) {
							//Display an alert of the error.
							alert(data.responseJSON.error);
						}
					});
				});

			}

		}

	},

	landing: {

		initialize: function() {
			var $feature = $("#feature"),
				$carouselItems = $feature.find(".feature__item"),
				$carouselNavigation = $feature.find(".feature__navigation"),
				carouselTimer = setInterval(playerPortal.landing.incrementSlide, 8 * 1000);

			$carouselItems.first().addClass("is-visible");

			$carouselItems.each(function() {
				var slideClass = "";
				if ($(this).hasClass("is-visible")) {
					slideClass = "is-active";
				}
				$carouselNavigation.append('<a class="feature__navigation--item ' + slideClass + '" href="' + $(this).data("slide") + '"></a>');
			});

			$carouselNavigation.on("click", "a", function (event) {
				event.preventDefault();
				var activeClass = $(this).attr("href");
				$carouselItems.removeClass("is-visible");
				$carouselNavigation.find(".feature__navigation--item").removeClass("is-active");
				$(this).addClass("is-active");
				$feature.find(".feature__item--" + activeClass).addClass("is-visible");
				clearInterval(carouselTimer);
				carouselTimer = setInterval(playerPortal.landing.incrementSlide, 5 * 1000)
			});

		},

		incrementSlide: function() {
			var $feature = $("#feature"),
				$carouselNavigation = $feature.find(".feature__navigation");

			if ($carouselNavigation.find(".is-active").next().length == 0) {
				$carouselNavigation.find(".feature__navigation--item").first().trigger("click");
			} else {
				$carouselNavigation.find(".is-active").next().trigger("click");
			}

		}

	},

	maintenance: {
		initialize: function() {

			$videoWrapper = $(".video-container");

			if (Modernizr.video && !isMobile()) {
				theVideo = document.createElement("video");
				theVideo.setAttribute("loop", !0);

				if (Modernizr.video.h264) {
					theVideo.src = "/videos/maintenance-ray-v1.mp4";
					theVideo.type = "video/mp4";
				} else if (Modernizr.video.webm) {
					theVideo.src = "/videos/maintenance-ray-v1.webm";
					theVideo.type = "video/webm";
				} else if (Modernizr.video.h264 != '') {
					theVideo.src = "/videos/maintenance-ray-v1.mp4";
					theVideo.type = "video/mp4";
				} else if (Modernizr.video.webm != '') {
					theVideo.src = "/videos/maintenance-ray-v1.webm";
					theVideo.type = "video/webm";
				}

				theVideo.addEventListener("loadeddata", function() {
					$videoWrapper.addClass("video-loaded");
					this.play();
				});

				$videoWrapper.prepend(theVideo);
			}

		}
	},

	newPlayerGuide: {

		initialize: function() {
			$('.npg-tip--half').each( function() {
				$this = $(this);
				$sibling = $(this).next('.npg-tip--half');

				if( $this.innerHeight() > $sibling.innerHeight() ) {
					$sibling.css({
						height: $this.innerHeight() + 'px'
					})
				} else {
					$this.css({
						height: $sibling.innerHeight() + 'px'
					});
				}
			});

			$('.npg__ul--roadmap-guides').children('li').each( function() {
				$this = $(this);
				$sibling = $(this).siblings('li');

				if( $this.innerHeight() > $sibling.innerHeight() ) {
					$sibling.css({
						height: $this.innerHeight() + 'px'
					})
				} else {
					$this.css({
						height: $sibling.innerHeight() + 'px'
					});
				}
			});

			$sidebar = $('#npg--sidebar');

			$sidebar.stickySidebar({
				sidebarTopMargin: 0,
				footerThreshold: 190
			});

			$sidebar.on('click', 'h4', function() {
				$sidebarMenu = $(this).next('.npg-nav__ul');
				$sidebarMenu.css({ left: $(this).offset().left });
				$sidebarMenu.slideToggle('fast');
				$(this).siblings('.npg-nav__ul').not($sidebarMenu).hide();
				$(this).siblings('h4').not(this).removeClass("expanded");
				$(this).toggleClass("expanded");
			});

		},

		interactiveMap: {

			initialize: function() {
				$interactiveMap = $(".npg--arena");
				$map = $interactiveMap.find(".map");
				$mapWrapper = $map.find(".map__wrapper");
				$mapFilterWrapper = $map.find(".map-filter");
				$mapFilters = $mapFilterWrapper.find(".map-filter__item");
				$mapElementsWrapper = $map.find(".map__wrapper");
				$mapElements = $mapElementsWrapper.find(".map__element");
				$mapElementsIndicators = $mapElementsWrapper.find(".indicators");

				$indicatorsWrapper = $interactiveMap.find(".indicators");
				$indicators = $indicatorsWrapper.find(".indicators__item");

				$mapFilterWrapper.on("click", ".map-filter__item", function() {

					if ($(this).attr("data-filter")) {
						selectedFilter = $(this).data("filter");

						if( $(this).hasClass("is-active") ){
							selectedFilter = "all";
						} else {
							ga("send", "event", "Player Portal: Getting Started", "click", "Arena Filter: " + selectedFilter );
						}

						$mapElementsIndicators.find(".indicators__item").removeClass("is-visible");

						if (selectedFilter === "all") {
							$mapFilters.removeClass("is-active");
							$mapFilterWrapper.find(".map-filter__item--all").addClass("is-active");
							$mapElementsWrapper.find('[data-description]').removeClass("is-visible");
							$mapElementsIndicators.addClass("is-visible");
						} else {
							var selectedDescription = '[data-description='+selectedFilter+']'
							if( $mapWrapper.children( selectedDescription ).length < 1 )
								$mapWrapper.prepend( $( selectedDescription ) )

							$mapFilters.removeClass("is-active");
							$mapElementsIndicators.removeClass("is-visible");
							$mapElementsWrapper.find('[data-description]').removeClass("is-visible");
							$mapElementsWrapper.find(".indicators--" + selectedFilter).addClass("is-visible").children('.indicators__item:first-child').addClass('is-visible');
							$mapElementsWrapper.find( selectedDescription ).addClass("is-visible");

							$(this).addClass("is-active");
						}

					}

				});

				$mapWrapper.on("click", function(event) {
					$element = $(event.target);

					if( $element.hasClass("is-visible") ) {
						$element.removeClass("is-visible");
					} else {
						$mapElementsIndicators.find(".indicators__item").removeClass("is-visible");
						$element.addClass("is-visible")
						var elementFiltered = $element.parent('[data-pointer]').attr('data-pointer');
						ga("send", "event", "Player Portal: Getting Started", "click", 'Arena Map Point: ' + elementFiltered );

					}

				});

			}

		},

		commandWheel: {

			initialize: function() {

				$(".wheel__section").on("mouseenter mouseleave", function () {
					var $this = $(this);
					var hoveredSection = $this.data("section");
					var $parent = $this.closest(".wheel");

					if (event.type === "mouseover") {
						$parent.find(".wheel__image--" + hoveredSection).addClass("is-visible");
						$parent.find(".wheel__center").addClass("wheel__center--" + hoveredSection);
					} else if (event.type === "mouseout") {
						$parent.find(".wheel__image--" + hoveredSection).removeClass("is-visible");
						$parent.find(".wheel__center").removeClass("wheel__center--" + hoveredSection);
					}

				});

				$(".wheel__section").on("click", function(event) {
					var $this = $(this);
					var selectedSound = $this.data("sound");

					event.preventDefault();

					sound[selectedSound].play();

				});

			}

		}

	},

	orderSummary: {
		initialize: function() {

			ga("send", "event", "Player Portal: Purchase", "pageload", "Made Purchase: " + $(".summaryList").find(".listTitle").last().html());

			var $paypalButton = $("#paypal-confirm");

			$paypalButton.on("click", function() {
				$(this).closest(".form-element").addClass("is-loading");
				$(this).hide();
			});

		}
	},
	purchaseGems: {
		selectedPaymentMethod: "authorize",
		selectedGemPackage: "",
		totalPrice: "",
		paymentMethods: {
			otherPaymentMethodsText: "View Other Payment Methods",
			$otherPaymentMethodsWrapper: {},
			$otherPaymentMethodsList: {},
			setPaymentMethod: function(newPaymentMethod) {
				// sets the payment method when it gets changed

				if (playerPortal.purchaseGems.selectedPaymentMethod !== newPaymentMethod) {
					var $paymentMethodForms = $("#payment-method-forms");
					var $billingDetailsWrapper = $("#billing-details");
					var $selectedPaymentMethodForm = $billingDetailsWrapper.find(".payment-method");
					var oldPaymentMethod = playerPortal.purchaseGems.selectedPaymentMethod;

					$.uniform.restore($selectedPaymentMethodForm.find("input[type='checkbox'], input[type='radio'], select"));
					$paymentMethodForms.find("." + oldPaymentMethod).html($selectedPaymentMethodForm.html());
					$selectedPaymentMethodForm.removeClass(oldPaymentMethod).addClass(newPaymentMethod).html($paymentMethodForms.find("." + newPaymentMethod).html());
					$selectedPaymentMethodForm.find("input[type='checkbox'], input[type='radio'], select").uniform();
					$paymentMethodForms.find("." + newPaymentMethod).empty();
					playerPortal.purchaseGems.selectedPaymentMethod = newPaymentMethod;

					//De-select all gem packages
					$('input[name="gem-package"]').prop('checked', false);
					$('label.gem-package-' + oldPaymentMethod + '.selected').removeClass('selected').addClass('unselected');

					//Show new gem packages for new payment option
					$('.gem-package-' + oldPaymentMethod).hide();
					$('.gem-package-' + newPaymentMethod).show();

					if (newPaymentMethod === "authorize") {
						var $creditCardForm = $("#credit-card-form");
						var $recipientUsernameWrapper = $billingDetailsWrapper.find(".select-recipient");

						if (!$creditCardForm.is(":visible")) {
							$creditCardForm.show();
						}

						if ($recipientUsernameWrapper.is(":visible")) {
							$recipientUsernameWrapper.hide();
						}
					}

				}

			},
			restorePaymentMethod: function() {
				// restore the dropdown selected option if it gets opened & closed after already having been selected

				var previouslySelectedPaymentMethod;

				if (playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsList.is(":visible")) {
					var $otherPaymentMethodsSelection = playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsList.find(".selected");

					if ($otherPaymentMethodsSelection.length > 0) {
						var previouslySelectedPaymentMethod = $otherPaymentMethodsSelection.text();
						playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper.find(".other-payment-methods-button").addClass("selected");
						playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper.find(".other-payment-methods-button .text").text(previouslySelectedPaymentMethod);
					}

				}

			},
			toggleDropdown: function($otherPaymentMethodsWrapper) {
				// toggles the visibility & class name on the other payment methods dropdown menu

				playerPortal.purchaseGems.paymentMethods.restorePaymentMethod();
				$("#other-payment-methods-list").fadeToggle(75, function() {
					playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper.find(".other-payment-methods-button").toggleClass("dropdown-visible");
				});

			},
			hideDropdown: function($otherPaymentMethodsWrapper) {
				// hide the dropdown when the user clicks anywhere outside of the dropdown

				if ($("#other-payment-methods-list").is(":visible")) {
					playerPortal.purchaseGems.paymentMethods.toggleDropdown(playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper);
				}

			},
			initialize: function() {
				// registers the events for the prefrred payment methods

				var $paymentMethodsWrapper = $("#payment-methods");
				var $preferredPaymentMethodsWrapper = $("#preferred-payment-methods");
				var $billingDetailsWrapper = $("#billing-details");
				playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper = $paymentMethodsWrapper.find(".other-payment-methods-wrapper");
				playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsList = $("#other-payment-methods-list");

				// when the select option for "saved credit cards" gets changed, we show or hide the credit card form accordingly.
				// this also wraps the obfuscated credit card numbers in a span
				$billingDetailsWrapper.on("change", ".saved-credit-cards-select", function(event) {
					var $creditCardForm = $("#credit-card-form");
					var $savedCreditCardsSelectText = $billingDetailsWrapper.find(".saved-credit-cards div.selector span");
					var selectedCardType;

					if ($(this).val() == "new-card") {

						if (!$creditCardForm.is(":visible")) {
							$creditCardForm.slideDown("fast");
						}

						$(this).closest(".selector").attr("class", "selector");

					} else {

						if ($creditCardForm.is(":visible")) {
							$creditCardForm.slideUp("fast");
						}

						selectedCardType = $(this).find("option:selected").data("card-type");
						if (selectedCardType) {
							$(this).closest(".selector").attr("class", "selector " + selectedCardType);
						}

						$savedCreditCardsSelectText.html(
							$savedCreditCardsSelectText.text().replace("**** **** ****", '<em class="obfuscated">**** **** ****</em>')
							);

					}

				});

				// this sets the payment method on click or keypress (the "enter" key)
				$preferredPaymentMethodsWrapper.on("click keypress", ".payment-option", function(event) {
					if (!$(this).hasClass("inactive")) {
						if ((event.type == "keypress" && (event.keyCode == 13 || event.keyCode == 32)) || (event.type == "click")) {
							if (event.type == "keypress") {
								event.preventDefault();
							}
							var $otherPaymentMethodsText = playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper.find(".other-payment-methods-button .text");
							playerPortal.general.markSelectedItem($(this), [$paymentMethodsWrapper.find(".payment-option.selected").not(this), playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper.find(".selected").removeClass("selected"), playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper]);
							playerPortal.purchaseGems.paymentMethods.setPaymentMethod($(this).attr("data-payment-method"));
							if ($otherPaymentMethodsText.text() != playerPortal.purchaseGems.paymentMethods.otherPaymentMethodsText) {
								$otherPaymentMethodsText.text(playerPortal.purchaseGems.paymentMethods.otherPaymentMethodsText);
							}
						}
					}
				});

				// show or hide the dropdown when the user clicks on "other payment methods"
				// if the text had changed due to one of these menu options being selected, we change it back
				// this fires on click or keypress (the "enter" key)
				playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper.on("click keypress", ".other-payment-methods-button", function(event) {
					if ((event.type == "keypress" && (event.keyCode == 13 || event.keyCode == 32)) || (event.type == "click")) {

						if (event.type == "keypress") {
							event.preventDefault();
						}

						var $otherPaymentMethodsText = $(this).find(".text");
						event.stopPropagation();

						if ($otherPaymentMethodsText.text() != playerPortal.purchaseGems.paymentMethods.otherPaymentMethodsText) {
							$(this).removeClass("selected").addClass("unselected");
							$otherPaymentMethodsText.text(playerPortal.purchaseGems.paymentMethods.otherPaymentMethodsText);
						}

						playerPortal.purchaseGems.paymentMethods.toggleDropdown(playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper);
					}

				});

				// when the user clicks on an alternate payment method option, make that option the selected payment method
				playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper.on("click", "#other-payment-methods-list .payment-option", function(event) {
					playerPortal.general.markSelectedItem([$(this), playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper.find(".other-payment-methods-button"), playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper], $paymentMethodsWrapper.find(".payment-option.selected"));
					$paymentMethodsWrapper.find(".other-payment-methods-button .text").text($(this).text());
					playerPortal.purchaseGems.paymentMethods.setPaymentMethod($(this).attr("data-payment-method"));
				});

				// hide the dropdown when the user clicks anywhere outside of it
				$(document).on("click", function() {
					playerPortal.purchaseGems.paymentMethods.hideDropdown(playerPortal.purchaseGems.paymentMethods.$otherPaymentMethodsWrapper);
				});

				$billingDetailsWrapper.on("change", "#somebody-else, #myself", function() {
					// when the user clicks on "somebody else" or "myself" in the recipient field, show or hide the "recipient username" textbox accordingly

					if ($(this).is(":checked")) {
						var $recipientSelection = $billingDetailsWrapper.find(".recipient");
						var $recipientUsernameWrapper = $billingDetailsWrapper.find(".select-recipient");

						if ($(this).attr("id") == "somebody-else") {
							// the input option was "somebody-else" so we have to show the dropdown list

							if (($("body").innerWidth() + 15) > 650) {
								// if the width is greater than 650, we slide the input elements to the left and fade in the dropdown list

								if (!($recipientUsernameWrapper.is(":visible"))) {
									$recipientSelection.animate({paddingRight: 250}, 200, function() {
										$(this).css({paddingRight: 0});
										$recipientUsernameWrapper.fadeIn("fast").toggleClass("invisible").css({display: "inline-block"});
									});
								}

							} else {
								// if the width is less than or equal to 650, the elements are stacked so we just have to slide down the dropdown list

								if (!($recipientUsernameWrapper.is(":visible"))) {
									$recipientUsernameWrapper.slideDown("fast").toggleClass("invisible").css({display: "inline-block"});
								}

							}

						} else {

							if (($("body").innerWidth() + 15) > 650) {
								// if the width is greater than 650, we fade out the dropdown list and slide the input elements to the right

								if ($recipientUsernameWrapper.is(":visible")) {
									$recipientUsernameWrapper.toggleClass("invisible").fadeOut("fast", function() {
										$recipientSelection.css({paddingRight: 250}).animate({paddingRight: 0}, 200);
									});
								}

							} else {
								// if the width is less than or equal to 635, the elements are stacked so we just have to slide up the dropdown list

								if ($recipientUsernameWrapper.is(":visible")) {
									$recipientUsernameWrapper.toggleClass("invisible").slideUp("fast");
								}

							}

						}

					}

				});

				$('.payment-redirect').on('click', function(){
					var redirect = $(this).data('redirect');
					document.location.href = redirect;
				});
			}
		},
		gemPackages: {
			initialize: function() {
				// registers the events for the gem packages

				var $gemPackages = $("#gem-packages");

				$gemPackages.on("click", ".gem-package", function() {
					playerPortal.general.markSelectedItem($(this), $gemPackages.find(".gem-package").not(this));
				});

				$gemPackages.on("click", ".button", function() {
					$(this).closest(".gem-package").find(".image-wrapper").find("input").attr("checked", "checked");
				});

			}

		},
		checkoutForm: {
			initialize: function() {

				// initialize the validator on the form
				var purchaseGemsForm = $("#purchase-gems-form").validate({
					errorPlacement: function(error, element) {
						if (element.attr("name") === "gem-package") {
							error.hide().appendTo("#gem-packages").fadeIn("slow");
						} else {
							error.hide().insertAfter(element).fadeIn("slow");
						}
					},
					highlight: function(element, errorClass) {
						if ($(element).attr("name") === "gem-package") {
							$("#gem-packages").addClass("error");
							if ($(document).scrollTop() > $("#gem-packages").offset().top) {
								$("html, body").animate({
									scrollTop: ($("#gem-packages").offset().top - 50)
								}, 500);
							}
						} else {
							$(element).closest(".form-element").addClass("caused-error");
						}
					},
					unhighlight: function(element, errorClass) {
						if ($(element).attr("name") === "gem-package") {
							$("#gem-packages").removeClass("error");
						} else {
							$(element).closest(".form-element").removeClass("caused-error");
						}
					},
					invalidHandler: function(event, validator) {
						var errorNames = "";

						for (var error in validator.errorMap) {
							errorNames += error + ", ";
						}

						errorNames = errorNames.substring(0, errorNames.length - 2);

						ga("send", "event", "Player Portal: Payment: Form Error", "click", errorNames);
					},
					submitHandler: function(form) {
						$("#authorize-purchase-gems").hide();
						$("#paypal-purchase-gems").hide();

						form.submit();
					},
					groups: {
						fullName: "first-name last-name"
					},
					messages: {
						"gem-package": {
							required: "Please select a gem package."
						},
						"first-name": {
							required: "Please enter your first and last name."
						},
						"last-name": {
							required: "Please enter your first and last name."
						},
						"card-number": {
							required: "Please enter a valid credit card number.",
							creditcard: "Please enter a valid credit card number."
						}
					},
					rules: {
						"gem-package": {
							required: true
						},
						"first-name": {
							required: true
						},
						"last-name": {
							required: true
						},
						"zip-code": {
							required: true
						},
						"card-number": {
							required: true,
							creditcard: true
						},
						"cvv-number": {
							required: true
						}
					}
				});

				// format the credit card number as it's being entered into the textbox
				$("#billing-details").on("input", ".credit-card-number", function() {
					var creditCardNumber = $(this).val();
					creditCardNumber = creditCardNumber.replace(/[^\d]+/g, "");
					creditCardNumber = creditCardNumber.match(/.{1,4}/g);
					creditCardNumber = creditCardNumber ? creditCardNumber.join("  ") : "";
					$(this).val(creditCardNumber);
				});

			}

		}

	},
	general: {

		analytics: {

			initialize: function() {
				var $abilityWrappers = $(".ability-wrapper"),
					$bottomNavContainer = $(".bottom-nav-contain"),
					$fankit = $(".fankit"),
					$filterBar = $(".filter-bar__items"),
					$gearsetWrappers = $(".gearset-wrapper"),
					$heroesContainer = $("#hero"),
					$heroesGameplayBox = $heroesContainer.find(".gameplay-box"),
					$itemContainer = $(".item-box"),
					$itemGroup = $(".item-group"),
					$socialMediaLinks = $(".social-media-icons"),
					$videoContainer = $(".video-contain"),
					$guidesWrapper = $(".guides-wrapper"),
					heroName = $heroesContainer.data("hero");

				$abilityWrappers.on("click", "li", function() {
					ga("send", "event", "Player Portal: Heroes", "click", heroName + ": Ability: " + $(this).data("ability"));
				});

				$bottomNavContainer.on("click", "a", function() {
					var href = $(this).attr("href");
					href = href.replace("/", "");
					ga("send", "event", "Player Portal: Heroes", "click", heroName + " Bottom Nav: " + href);
				});

				$fankit.on("click", function() {
					ga("send", "event", "Player Portal: Fankit", "click", "Downloaded Fankit");
				});

				$filterBar.on("click", "li", function() {
					var classes = $(this).attr("class");
					classes = classes.replace("filter-bar__item ", "");
					classes = classes.replace("filter-bar__item--", "");
					ga("send", "event", "Player Portal: Items", "click", "Filtered: " + classes);
				});

				$gearsetWrappers.on("click", "li", function() {
					ga("send", "event", "Player Portal: Heroes", "click", heroName + ": Gearset: " + $(this).data("gear"));
				});

				$guidesWrapper.on("click", "li", function() {
					ga("send", "event", "Player Portal: Heroes", "click", heroName + ": Build Guide: " + $(this).data("guide"));
				});

				$heroesGameplayBox.on("click", ".button-flat", function() {
					ga("send", "event", "Player Portal: Heroes", "click", heroName + ": Lore");
				});

				$itemContainer.on("click", "li", function() {
					var classes = $(this).attr("class");
					classes = classes.replace("item ", "");
					ga("send", "event", "Player Portal: Heroes", "click", heroName + ": Item: " + classes);
				});

				$itemGroup.on("click", "li", function() {
					var classes = $(this).attr("class");
					classes = classes.replace("items__item ", "");
					classes = classes.replace("items__item--", "");
					ga("send", "event", "Player Portal: Items", "click", "Item: " + classes);
				});

				$socialMediaLinks.on("click", "a", function() {
					ga("send", "event", "Player Portal: Social Media", "click", $(this).attr("class"));
				});

				$videoContainer.on("click", function() {
					ga("send", "event", "Player Portal: Heroes", "click", heroName + ": Viewed Video");
				});

			}

		},

		accountOptions: {
			inviteFriends: {
				initialize: function() {
					var $inviteFriendsLink = $("#invite-friends-link");
					var $inviteFriendsPopover = $inviteFriendsLink.parent().find(".popover");

					$inviteFriendsLink.on("click", function(event) {
						if (!$("html").hasClass("touch")) {
							$inviteFriendsPopover.fadeToggle();
							event.preventDefault();
							event.stopPropagation();
						}
					});

					$(document).on("click", function(event) {
						if ($inviteFriendsPopover.is(":visible")) {
							if (!$(event.target).closest(".popover").length) {
								$inviteFriendsPopover.fadeToggle();
							}
						}
					});

				}

			},
			initialize: function() {
				var $welcomeText = $(".welcome-text");
				$welcomeText.find(".account-options").hide();

				$(document).on("click", function(event) {
					if ($welcomeText.find(".account-options").is(":visible") && !$(event.target).closest(".account-options").length && $welcomeText.hasClass("expanded")) {
						$welcomeText.find(".account-options").hide();
						$welcomeText.removeClass("expanded");
					}
				});

				$welcomeText.on("click", function(event) {
					event.stopPropagation();
					$(this).toggleClass("expanded");
					$welcomeText.find(".account-options").stop().toggle(0, function() {
						if ($(this).is(":visible")) {
							$(this).css({display: "inline-block"});
						}
					});
				});

			},
			uninitialize: function() {
				var $welcomeText = $(".welcome-text");

				$welcomeText.removeClass("expanded");
				$welcomeText.find(".account-options").css({display: "inline-block"});
				$welcomeText.off("click");

			}

		},
		footer: {
			initialize: function() {

				$(".footer-main-nav-mobile").on("change", function() {
					window.location.href = $(this).find(":selected").data("href");
				});

			}

		},
		glossary: {
			initialize: function() {
				$('.strife__anchor--definition').hover(
					function() {
						var term_title = $(this).attr('data-term');
						var definition = $(this).attr('data-definition');
						var id = $(this).attr('id');
						var term_id = id + '-glossary-definition';

						var pos_top = $(this).offset().top;
						var pos_left = $(this).offset().left;

						var width = $(this).width();

						if( $( '#'+term_id ).length < 1 ) {
							$('body').append('<div id="' + term_id + '" class="strife__tooltip strife__tooltip--definition"></div>');
						}

						var term = $( '#'+term_id );

						term.html('<figure class="strife__figure strife__figure--definition"><img src="/images/glossary/' + id + '.jpg" class="strife__image" width="80" height="80" /></figure><h4>' + term_title + '</h4><p>' + definition + '</p>');

						pos_top = pos_top - term.height() - 30;
						pos_left = ( pos_left - ( term.width() / 2 ) + ( width / 2 ) );

						term.css({
							height: term.height()
						}).offset({
							top: pos_top,
							left: pos_left
						}).addClass('is-visible');
					},
					function() {
						if( $('.is-visible').length > 0 )
							$('.is-visible').removeClass('is-visible');
					}
				);
			}

		},
		glowIndicator: {
			initialize: function($glowIndicator, $hoverElements, $parent) {
				// when hovering over $hoverElements item, move $glowIndicator above the element and resize its width to match the hovered element.
				// when not hovering, move $glowIndicator to its original location

				if (($("body").innerWidth() > 960) && $("html").hasClass("no-touch")) {

					var activeSection = false;
					var originalPosition;
					var originalWidth;

					for (i = 0; i < $hoverElements.length; i++) {
						var $element = $($hoverElements[i]);

						if ($element.hasClass("active")) {
							originalWidth = $element.width();
							originalPosition = $element.position().left + $parent.position().left;
							$glowIndicator.css({
								left: originalPosition,
								width: originalWidth
							});
							activeSection = true;
						}

					}

					if (activeSection == false) {
						originalPosition = $glowIndicator.position().left;
						originalWidth = $glowIndicator.width();
					}

					$hoverElements.hover(function(event) {

						if ((!$("#mobile-menu").is(":visible")) && ($("body").innerWidth() > 960)) {

							var $element = $(this);
							elementWidth = $element.width();

							if ($element.is("#logo")) {
								elementLeft = $element.position().left;
							} else {
								elementLeft = $element.position().left + $parent.position().left;
							}

							playerPortal.general.glowIndicator.animate($glowIndicator, elementLeft, elementWidth);

						}

					}, function(event) {
						playerPortal.general.glowIndicator.animate($glowIndicator, originalPosition, originalWidth);
					});

				}

			},
			animate: function($indicator, left, width) {
				// simply animates the indicator as specified

				$indicator.stop().animate({
					left: left,
					width: width
				});

			}

		},
		mobileMenu: {
			initialize: function() {

				$("#main-navigation li").on("touchstart", function() {
					$(this).addClass("active");
				});

				$("#main-navigation li").on("touchend", function() {
					$(this).removeClass("active");
				});

				// show & hide the nav when the user clicks on the hamburger icon
				$("#mobile-menu").on("click", function() {
					$("body").toggleClass("mobile-nav-visible");
				});

			}

		},
		resize: {
			initialize: function() {
				// handles all tasks that need to listen to the window resize event

				var previousWidth = $("body").innerWidth() + 15;
				var purchaseGemsPage = false;
				var sidebarExists = false;

				if ($("#purchase-gems").length > 0) {
					purchaseGemsPage = true;
				}

				if ($("#sidebar").length > 0) {
					sidebarExists = true;
				}

				$(window).resize(
					$.throttle(125, function() {

						currentWidth = $("body").innerWidth() + 15;

						if (previousWidth > 650 && currentWidth <= 650) {
							// these events fire when the viewport is being scaled down past 650px wide

							if (purchaseGemsPage) {
								$.uniform.restore(".saved-credit-cards-select");
								$(".saved-credit-cards-select").uniform();
							}

							if (sidebarExists) {
								playerPortal.general.sidebar.initialize();
							}

						}

						if (previousWidth > 400 && currentWidth <= 400) {

							playerPortal.general.accountOptions.initialize();

						}

						if (previousWidth <= 650 && currentWidth > 650) {
							// these events fire when the viewport is being scaled up past 650px wide

							if (purchaseGemsPage) {
								$.uniform.restore(".saved-credit-cards-select");
								$(".saved-credit-cards-select").uniform();
							}

							if (sidebarExists) {
								playerPortal.general.sidebar.uninitialize();
							}

						}

						if (previousWidth <= 400 && currentWidth > 400) {

							playerPortal.general.accountOptions.uninitialize();

						}

						previousWidth = currentWidth;

					})
					);
			}

		},
		sidebar: {
			initialize: function() {
				$sidebar = $("#sidebar");
				$sidebar.find(".sidebar-item-content").hide();
				$sidebar.removeClass("is-on-right");

				$sidebar.on("click", "h5", function() {
					$relatedContent = $(this).closest(".sidebar-item").find(".sidebar-item-content");
					$sidebar.find(".sidebar-item-content").not($relatedContent).hide();
					$sidebar.find("h5").not(this).removeClass("expanded");
					$(this).toggleClass("expanded");
					$relatedContent.slideToggle("fast");
				});

			},
			uninitialize: function() {
				var $sidebar = $("#sidebar");

				$sidebar.find(".sidebar-item-content").show();
				$sidebar.find("h5").removeClass("expanded");
				$sidebar.addClass("is-on-right");
				$sidebar.off("click", "h5");

			}

		},
		markSelectedItem: function($selectedItem, $otherItems, selectedClass, unselectedClass) {

			if (!selectedClass) {
				selectedClass = "selected";
			}

			if (!unselectedClass) {
				unselectedClass = "unselected";
			}

			// toggles the classname for the specified elements
			for (var i = 0; i < $otherItems.length; i++) {
				$($otherItems[i]).removeClass(selectedClass).addClass(unselectedClass);
			}

			for (var i = 0; i < $selectedItem.length; i++) {
				$($selectedItem[i]).removeClass(unselectedClass).addClass(selectedClass);
			}

		}

	},
	login: {
		initialize: function() {
			// registers the events for the login page

			var $body = $("body"),
				$loginPage = $("#site-login"),
				$loginForm = $("#login-form"),
				$userLoginFormStrife = $("#login-form--strife"),
				$userLoginFormPassport = $("#login-form--asiasoft-passport"),
				$createAccountForm = $("#create-account-form"),
				$createAccountWrapper = $("#create-account-form-wrapper"),
				$centerColumn = $loginPage.find(".center-column"),
				$forgotPasswordPopover = $("#forgot-password"),
				$externalLoginsList = $(".external-logins__list");

			$("#create-account-button").on("click", function(event) {
				event.preventDefault();
				$("#site-login").toggleClass("register-form-is-visible login-form-is-visible");
				$createAccountWrapper.find("input[name=username]").focus();
			});

			$("#create-account-cancel-button, .beta-key-alert .button").on("click", function() {
				$("#site-login").toggleClass("register-form-is-visible login-form-is-visible");
				$createAccountWrapper.find(".form-element").removeClass("caused-error");
				createAccountForm.resetForm();
			});

			// show the "forgot password" form when a user clicks the forgot password link

			$loginForm.on("click", ".forgot-password", function(event) {
				event.stopPropagation();
				event.preventDefault();
				$forgotPasswordPopover.fadeToggle({duration: 200, queue: false, done: function() {
					$(this).toggleClass("is-visible");
				}});
			});

			// don't hide the popover when the user clicks anywhere within the popover
			$loginForm.on("click", ".popover", function(event) {
				event.stopPropagation();
			});

			// hide the "forgot password" popover when a user clicks anywhere outside the "forgot password" popover
			$(document).on("click", function(event) {
				if ($forgotPasswordPopover.is(":visible")) {
					$loginForm.find(".forgot-password").trigger("click");
				}
			});

			$externalLoginsList.on("click", ".external-logins__item", function() {
				var selectedLoginMethod = $(this).data("method");

				$(".login-form__wrapper.is-visible").removeClass("is-visible");
				$("#login-form__wrapper--" + selectedLoginMethod).addClass("is-visible");
			});

			$userLoginFormStrife.validate({
				errorPlacement: function(error, element) {
					error.hide().insertAfter(element).fadeIn("slow");
				},
				highlight: function(element, errorClass) {
					$(element).closest(".form-element").addClass("caused-error");
				},
				unhighlight: function(element, errorClass) {
					$(element).closest(".form-element").removeClass("caused-error");
				},
				invalidHandler: function(event, validator) {
					var errorNames = "";

					for (var error in validator.errorMap) {
						errorNames += error + ", ";
					}

					errorNames = errorNames.substring(0, errorNames.length - 2);

					ga("send", "event", "Player Portal: Login: Form Error", "click", errorNames);
				},
				rules: {
					"password": {
						minlength: 4,
						required: true
					},
					"email": {
						required: true,
						email: true
					}
				},
				submitHandler: function(form) {
					$body.addClass("is-loading");
					$("#login-form-error-wrapper").slideUp("fast");
					//AJAX POST.
					$.ajax({
						method: "POST",
						url: "/ajax/login/login",
						data: $(form).serialize(),
						success: function(data) {
							//If a redirect was returned, use it. Else do the default.
							if (data.redirect)
							{
								document.location.href = "/" + data.redirect;
							}
							else
							{
								document.location.href = "/portal";
							}
						},
						error: function(data) {
							$body.removeClass("is-loading");
							//Display an alert of the error.
							$('#login-form-error-wrapper').fadeIn("fast");
							$('#login-form-error-message').html("<ul class=\"errors-list\"><li>" + data.responseJSON.error + "</li></ul>");
							ga("send", "event", "Player Portal: Login: Server Form Error", "click", data.responseJSON.error);
						}
					});
				}
			});

			// the validate plugin allows for easy form validation
			var createAccountForm = $("#create-account-form").validate({
				errorPlacement: function(error, element) {
					if ((element.attr("name") === "i-agree") || (element.attr("name") === "signup-for-newsletter")) {
						error.hide().insertAfter(element.closest(".checkbox-label")).fadeIn("slow");
					} else {
						error.hide().insertAfter(element).fadeIn("slow");
					}
				},
				invalidHandler: function(event, validator) {
					var errorNames = "";

					for (var error in validator.errorMap) {
						errorNames += error + ", ";
					}

					errorNames = errorNames.substring(0, errorNames.length - 2);

					ga("send", "event", "Player Portal: Create Account: Form Error", "click", errorNames);
				},
				highlight: function(element, errorClass) {
					$(element).closest(".form-element").addClass("caused-error");
				},
				unhighlight: function(element, errorClass) {
					$(element).closest(".form-element").removeClass("caused-error");
				},
				messages: {
					"confirm-password": {
						equalTo: "Your passwords do not match."
					},
					"i-agree": {
						required: "You must agree."
					},
					"signup-for-newsletter": {
						required: "You must sign up."
					}
				},
				rules: {
					"nickname": {
						rangelength: [3, 16],
						required: true
					},
					"firstName": {
						required: true
					},
					"lastName": {
						required: true
					},
					"password": {
						minlength: 4,
						required: true
					},
					"confirm-password": {
						required: true,
						equalTo: "#password"
					},
					"email": {
						required: true,
						email: true
					},
					"i-agree": {
						required: true
					},
					"signup-for-newsletter": {
						required: true
					}
				},
				submitHandler: function(form) {
					form.submit();
					/*
					var data = $(form).serialize();

					//AJAX POST.
					$.ajax({
						method: "POST",
						url: "/ajax/account/create",
						data: data,
						success: function(data) {
							//If a redirect was returned, use it. Else do the default.
							confirm('Your account was successfully created.');
							if (data.redirect)
							{
								document.location.href = "/" + data.redirect;
							}
							else
							{
								document.location.href = "/portal";
							}
						},
						error: function(data) {
							//Display an alert of the error.
							alert(data.responseJSON.error);
						}
					});*/
				}
			});

			var forgotPasswordForm = $("#forgot-password-form").validate({
				errorPlacement: function(error, element) {
					error.hide().insertAfter(element).fadeIn("slow");
				},
				highlight: function(element, errorClass) {
					$(element).closest(".form-element").addClass("caused-error");
				},
				unhighlight: function(element, errorClass) {
					$(element).closest(".form-element").removeClass("caused-error");
				},
				messages: {
					"conemail": {
						equalTo: "Your passwords do not match."
					}
				},
				rules: {
					"email": {
						required: true,
						email: true
					},
					"conemail": {
						required: true,
						email: true,
						equalTo: "#email"
					}
				},
				submitHandler: function(form) {

					var data = $(form).serialize();

					//AJAX POST.
					$.ajax({
						method: "POST",
						url: "/ajax/account/forgotPassword",
						data: data,
						success: function(data) {
							//If a redirect was returned, use it. Else do the default.
							if (data.redirect)
							{
								document.location.href = "/" + data.redirect;
							}
							else
							{
								document.location.href = "/portal";
							}
						},
						error: function(data) {
							//Display an alert of the error.
							alert(data.responseJSON.error);
						}
					});
				}
			});

		}

	},
	redeem: {
		initialize: function() {
			var createRedeemForm = $('#create-redeem-form').validate({
				errorPlacement: function(error, element) {
					error.hide().insertAfter(element).fadeIn("slow");
				},
				highlight: function(element, errorClass) {
					$(element).closest(".form-element").addClass("caused-error");
				},
				unhighlight: function(element, errorClass) {
					$(element).closest(".form-element").removeClass("caused-error");
				},
				rules: {
					"nickname": {
						rangelength: [3, 16],
						required: true
					},
					"firstName": {
						required: true
					},
					"lastName": {
						required: true
					},
					"password": {
						minlength: 4,
						required: true
					},
					"confirm-password": {
						required: true,
						equalTo: "#password"
					},
					"email": {
						required: true,
						email: true
					},
					"promoCode": {
						required: true,
						minlength: 25
					},
				},

			});
			var redeemForm = $('#redeem-form').validate({
				errorPlacement: function(error, element) {
					error.hide().insertAfter(element).fadeIn("slow");
				},
				highlight: function(element, errorClass) {
					$(element).closest(".form-element").addClass("caused-error");
				},
				unhighlight: function(element, errorClass) {
					$(element).closest(".form-element").removeClass("caused-error");
				},
				rules: {
					"promoCode": {
						required: true,
						minlength: 25
					},
				},

			});
		}
	},
	statsPage: {
		initialize: function() {

			$('#ladder-nav').find('.stat-button').on('click', function(){

				$('#ladder-nav li span').removeClass('active');
				$(this).addClass('active');

				$(".leaderboard-table").each(function() {
					if ($(this).is(":visible")) {
						previouslySelectedDiv = "#" + $(this).attr("id");
					}
				});

				selectedDiv = "#" + $(this).data("table");

				if (!$(selectedDiv).is(":visible")) {
					$(previouslySelectedDiv).fadeOut(function() {
						$(selectedDiv).fadeIn();
					})
				}

			});

		}
	},
	swipebox: {
		initialize: function() {
			$(".swipebox").swipebox({
				useCSS: true,
				hideBarsDelay: 0
			});
		}
	},
	thankyou: {

		initialize: function () {

			ga("send", "event", "Player Portal: Account", "pageload", "Successfully Created an Account");

			$(window).load(function() {
				window.setTimeout(function(){
					window.location.href = "/portal";
				}, 2000);
			});
		}

	}

};

$(function() {
	// the uniform plugin allows styling for unruly input elements
	//$("input[type=radio], input[type=checkbox], select").uniform();

	var $glowIndicator = $("#glow-indicator");
	var $hoverElements = $("#main-navigation .navigation-list > li, #logo");
	var $parent = $("#main-navigation");
	var viewport_width = $(window).width()

	if($glowIndicator.length){
		//playerPortal.general.analytics.initialize();
		playerPortal.general.glowIndicator.initialize($glowIndicator, $hoverElements, $parent);
		playerPortal.general.mobileMenu.initialize();
		playerPortal.general.footer.initialize();

		if( viewport_width >= 768 )
			playerPortal.general.glossary.initialize();

		playerPortal.general.resize.initialize();
	}

	if (($("body").innerWidth() + 15) <= 650) {
		playerPortal.general.sidebar.initialize();
	}

	if (($("body").innerWidth() + 15) <= 400) {
		playerPortal.general.accountOptions.initialize();
	}

	// if ($("#invite-friends-link").length > 0) {
	// 	playerPortal.general.accountOptions.inviteFriends.initialize();
	// }

	if ($("#purchase-gems").length > 0) {
		playerPortal.purchaseGems.paymentMethods.initialize();
		playerPortal.purchaseGems.gemPackages.initialize();
		playerPortal.purchaseGems.checkoutForm.initialize();
	}

	if ($("#forum-list").length > 0) {
		playerPortal.forums.general.initialize();
	}

	if ($("#forums").length > 0) {
		if ($(".individual-forum-thread").length > 0) {
			playerPortal.forums.general.initialize();
			playerPortal.forums.replies.initialize();
		}
		playerPortal.forums.threadList.initialize();
	}

	if ($("#download").length > 0) {
		playerPortal.download.initialize();
	}

	if ( ($("#heroes").length > 0) || ($("#hero").length > 0) ) {
		playerPortal.heroes.initialize();
	}

	if ($("#home").length > 0) {
		playerPortal.home.initialize();
	}

	if ($("#landing").length > 0) {
		playerPortal.landing.initialize();
	}

	if ($("#npg").length > 0) {
		playerPortal.newPlayerGuide.initialize();
	}

	if ($(".npg--arena").length > 0) {
		playerPortal.newPlayerGuide.interactiveMap.initialize();
	}

	if ($(".npg--command-wheel").length > 0) {
		playerPortal.newPlayerGuide.commandWheel.initialize();
	}

	if ($("#items").length > 0) {
		playerPortal.items.initialize();
	}

	if ($("#manage-account").length > 0) {
		playerPortal.account.initialize();
	}

	if ($("#refer-a-friend").length > 0) {
		playerPortal.account.initialize();
	}

	if ($("#maintenance").length > 0) {
		playerPortal.maintenance.initialize();
	}

	if (($("#media").length > 0) || ($("#heroes").length > 0) || ($("#landing").length > 0) || ($("#hero").length > 0) || ($("#cotd").length > 0)) {
		playerPortal.swipebox.initialize();
	}

	if ($("#order-summary").length > 0) {
		playerPortal.orderSummary.initialize();
	}

	if ($("#pets").length > 0) {
		playerPortal.pets.initialize();
	}

	if( $('#redeem-page').length> 0 ) {
		playerPortal.redeem.initialize();
	}

	if ($("#reset-password-page").length > 0) {
		playerPortal.resetPassword.initialize();
	}

	if ($("#sign-up").length > 0) {
		playerPortal.signup.initialize();
	}

	if ($("#site-login").length > 0) {
		playerPortal.login.initialize();
	}

	if ($("#stats-page").length > 0) {
		playerPortal.statsPage.initialize();
	}

	if ($(".create-thank-you").length > 0) {
		playerPortal.thankyou.initialize();
	}

});

var QueryString = function() {
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
			// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [query_string[pair[0]], pair[1]];
			query_string[pair[0]] = arr;
			// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	}
	return query_string;
}();

window.isMobile = function() {
	var check = false;
	(function(a) {if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
}

(function ($) {

	$.fn.stickySidebar = function (options) {

		var config = $.extend({
			headerSelector: '.header-wrapper',
			navSelector: '.account-info',
			contentSelector: '.body-wrapper',
			footerSelector: '.footer-wrapper',
			sidebarTopMargin: 20,
			footerThreshold: 60
		}, options);

		var fixSidebr = function () {

			var sidebarSelector = $(this);
			var viewportHeight = $(window).height();
			var viewportWidth = $(window).width();
			var documentHeight = $(document).height();
			var headerHeight = $(config.headerSelector).outerHeight();
			var navHeight = $(config.navSelector).outerHeight();
			var sidebarHeight = sidebarSelector.outerHeight();
			var contentHeight = $(config.contentSelector).outerHeight();
			var footerHeight = $(config.footerSelector).outerHeight();
			var scroll_top = $(window).scrollTop();
			var fixPosition = contentHeight - sidebarHeight;
			var breakingPoint1 = headerHeight + navHeight;
			var breakingPoint2 = documentHeight - (sidebarHeight + footerHeight + config.footerThreshold);

			// calculate
			if ((contentHeight > sidebarHeight) && (viewportHeight > sidebarHeight)) {

				if (scroll_top < breakingPoint1) {

					sidebarSelector.removeClass('sticky');

				} else if ((scroll_top >= breakingPoint1) && (scroll_top < breakingPoint2)) {

					sidebarSelector.addClass('sticky').css('top', config.sidebarTopMargin);

				} else {

					var negative = breakingPoint2 - scroll_top;
					sidebarSelector.addClass('sticky').css('top', negative);

				}

			}
		};

		return this.each(function () {
			$(window).on('scroll', $.proxy(fixSidebr, this));
			$(window).on('resize', $.proxy(fixSidebr, this))
			$.proxy(fixSidebr, this)();
		});

	};

}(jQuery));
