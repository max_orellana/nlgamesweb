function showRemaining() {
    currentDate = new Date, utc = 6e4 * currentDate.getTimezoneOffset(), timeLeft = timeDate - currentDate + utc, days = Math.floor(timeLeft / day) + 1, (1 >= days || 4 >= days && "ru" === po.locale) && $(".js-countdown-days-metric").html(po.day), $nodeDays.html(days)
}

function setSceneScale() {
    var e = $win.width(),
        t = $win.height(),
        o = e / sceneWidth,
        n = t / sceneHeight,
        r = n >= o ? n : o;
    $(".js-act-size").css({
        width: Math.floor(sceneWidth * r),
        height: sceneHeight * r - 40 - alertHeight
    }), $(".js-act-wrap-size").css({
        width: Math.ceil(sceneWidth * r * state.actsLength),
        height: sceneHeight * r - 40 - alertHeight
    }), $(".js-scrollbar").perfectScrollbar("update")
}

function changeAct(e) {
    var t = state.currentAct,
        o = e + 1;
    if (e !== t) {
        TweenMax.set(document.getElementsByClassName("js-scene-layer"), {
            autoAlpha: 0,
            display: "none"
        });
        var n = document.getElementsByClassName("js-scene-controls"),
            r = document.getElementsByClassName("js-scene-controls-" + o),
            i = 2 * Math.abs(e - t),
            a = ".3";
        if (state.overlayChanged && (i = 0, a = 0), TweenMax.to(n, a, {
                autoAlpha: 0
            }), TweenMax.set(n, {
                display: "none",
                delay: a
            }), e < state.actsLength) {
            var s = document.getElementsByClassName("js-acts-wrap"),
                l = "-" + 100 / state.actsLength * e + "%";
            TweenMax.to(s, i, {
                x: l
            })
        }
        if (TweenMax.set(r, {
                display: "block",
                delay: i
            }), TweenMax.to(r, a, {
                autoAlpha: 1,
                delay: i
            }), e < state.storyLength) {
            var c = document.getElementsByClassName("js-stories-wrap"),
                d = "-" + 100 / state.storyLength * e + "%";
            TweenMax.to(c, i, {
                x: d
            }), $(".js-story").removeClass("is-current"), $(".js-story-" + o).addClass("is-current")
        }
        $(".js-current-page").removeClass("is-current"), $('.js-current-page[data-current-page="' + state.overlayType + "-" + o + '"]').addClass("is-current")
    }
    audio.changeAudio(e), state.currentAct = e
}

function changeBrochurePage(e) {
    var t = state.currentBrochurePage;
    if (e !== t && e < state.brochureLength) {
        var o = document.getElementsByClassName("js-brochure-wrap"),
            n = "-" + 100 / state.brochureLength * e + "%",
            r = document.getElementsByClassName("js-brochure-controls"),
            i = document.getElementsByClassName("js-brochure-controls-" + (e + 1)),
            a = 2 * Math.abs(e - t),
            s = ".3";
        state.overlayChanged && (a = 0, s = 0), TweenMax.to(r, s, {
            autoAlpha: 0
        }), TweenMax.set(r, {
            display: "none",
            delay: s
        }), TweenMax.to(o, a, {
            x: n
        }), TweenMax.set(i, {
            display: "block",
            delay: a
        }), TweenMax.to(i, s, {
            autoAlpha: 1,
            delay: a
        })
    }
    state.currentBrochurePage = e
}

function updateOverlay(e) {
    var t = state.overlayType;
    if (t !== e) {
        var o = document.getElementsByClassName("js-world");
        if ("act" !== t) {
            var n = $('.js-overlay:not([data-overlay="' + e + '"])');
            TweenMax.to(n, ".25", {
                autoAlpha: 0
            }), TweenMax.set(n, {
                display: "none",
                delay: ".25"
            })
        }
        var r = $('.js-overlay[data-overlay="' + e + '"]');
        "act" !== e ? TweenMax.set(o, {
            display: "none",
            delay: ".25"
        }) : TweenMax.set(o, {
            display: "block",
            autoAlpha: 1
        }), TweenMax.set(r, {
            display: "block"
        }), TweenMax.to(r, ".25", {
            autoAlpha: 1
        }), $(".js-current-section").removeClass("is-current"), $('.js-current-section[data-current-section="' + e + '"]').addClass("is-current"), currentSublist = void 0, state.overlayType = e, state.overlayChanged = !0, setScrollPosition()
    } else state.overlayChanged = !1
}

function updatePage() {
    var e = window.location.hash.substring(1);
    "function" == typeof window.ping && window.ping("#" + e, {
        "": ""
    });
    var t = {
        "hireling-types": "brochure-1",
        "blackmarket-items": "brochure-1",
        aram: "brochure-1",
        "bilgewater-skins": "brochure-2",
        bundles: "brochure-2",
        "ward-skins": "brochure-2",
        merch: "brochure-2",
        rewards: "brochure-3",
        events: "brochure-3",
        papercraft: "brochure-3"
    };
    if (void 0 !== t[e]) {
        var o = $(".js-body-scroll-" + e).offset().top;
        $(".js-body-scroll-" + t[e]).scrollTop(o - 65), $(".js-scrollbar").perfectScrollbar("update"), e = t[e]
    }
    var n = e.split("-"),
        r = n[0],
        i = n[1];
    isMobile && "act" === r && (e = ""), updateOverlay("" === e ? "intro" : r), header.toggleState(!1), footer.toggleState(!1), i && ("brochure" !== r ? changeAct(i - 1) : changeBrochurePage(i - 1)), "function" == typeof player.stopVideo && player.stopVideo()
}

function setScrollPosition() {
    var e = isMobile ? "" : ($win.width() - $(".js-story-container").width()) / 2 + 25;
    $(".js-scroll-list-position").css("right", e)
}

function riotbarOffset(e) {
    var t = parseInt(e.css("top"), 10);
    e.css({
        top: alertHeight + t
    })
}! function(e) {
    e.fn.dragscrollable = function(t) {
        var o = e.extend({
                dragSelector: ">:first",
                acceptPropagatedEvent: !0,
                preventDefault: !0
            }, t || {}),
            n = {
                mouseDownHandler: function(t) {
                    return 1 != t.which || !t.data.acceptPropagatedEvent && t.target != this ? !1 : (t.data.lastCoord = {
                        left: t.clientX,
                        top: t.clientY
                    }, e.event.add(document, "mouseup", n.mouseUpHandler, t.data), e.event.add(document, "mousemove", n.mouseMoveHandler, t.data), t.data.preventDefault ? (t.preventDefault(), !1) : void 0)
                },
                mouseMoveHandler: function(e) {
                    var t = {
                        left: e.clientX - e.data.lastCoord.left,
                        top: e.clientY - e.data.lastCoord.top
                    };
                    return e.data.scrollable.scrollLeft(e.data.scrollable.scrollLeft() - t.left), e.data.scrollable.scrollTop(e.data.scrollable.scrollTop() - t.top), e.data.lastCoord = {
                        left: e.clientX,
                        top: e.clientY
                    }, e.data.preventDefault ? (e.preventDefault(), !1) : void 0
                },
                mouseUpHandler: function(t) {
                    return e.event.remove(document, "mousemove", n.mouseMoveHandler), e.event.remove(document, "mouseup", n.mouseUpHandler), t.data.preventDefault ? (t.preventDefault(), !1) : void 0
                }
            };
        this.each(function() {
            var t = {
                scrollable: e(this),
                acceptPropagatedEvent: o.acceptPropagatedEvent,
                preventDefault: o.preventDefault
            };
            e(this).find(o.dragSelector).bind("mousedown", t, n.mouseDownHandler)
        })
    }
}(jQuery),
function(e, t, o, n) {
    var r = o("html"),
        i = o(e),
        a = o(t),
        s = o.fancybox = function() {
            s.open.apply(this, arguments)
        },
        l = navigator.userAgent.match(/msie/i),
        c = null,
        d = t.createTouch !== n,
        u = function(e) {
            return e && e.hasOwnProperty && e instanceof o
        },
        p = function(e) {
            return e && "string" === o.type(e)
        },
        f = function(e) {
            return p(e) && 0 < e.indexOf("%")
        },
        h = function(e, t) {
            var o = parseInt(e, 10) || 0;
            return t && f(e) && (o *= s.getViewport()[t] / 100), Math.ceil(o)
        },
        g = function(e, t) {
            return h(e, t) + "px"
        };
    o.extend(s, {
        version: "2.1.5",
        defaults: {
            padding: 15,
            margin: 20,
            width: 800,
            height: 600,
            minWidth: 100,
            minHeight: 100,
            maxWidth: 9999,
            maxHeight: 9999,
            pixelRatio: 1,
            autoSize: !0,
            autoHeight: !1,
            autoWidth: !1,
            autoResize: !0,
            autoCenter: !d,
            fitToView: !0,
            aspectRatio: !1,
            topRatio: .5,
            leftRatio: .5,
            scrolling: "auto",
            wrapCSS: "",
            arrows: !0,
            closeBtn: !0,
            closeClick: !1,
            nextClick: !1,
            mouseWheel: !0,
            autoPlay: !1,
            playSpeed: 3e3,
            preload: 3,
            modal: !1,
            loop: !0,
            ajax: {
                dataType: "html",
                headers: {
                    "X-fancyBox": !0
                }
            },
            iframe: {
                scrolling: "auto",
                preload: !0
            },
            swf: {
                wmode: "transparent",
                allowfullscreen: "true",
                allowscriptaccess: "always"
            },
            keys: {
                next: {
                    13: "left",
                    34: "up",
                    39: "left",
                    40: "up"
                },
                prev: {
                    8: "right",
                    33: "down",
                    37: "right",
                    38: "down"
                },
                close: [27],
                play: [32],
                toggle: [70]
            },
            direction: {
                next: "left",
                prev: "right"
            },
            scrollOutside: !0,
            index: 0,
            type: null,
            href: null,
            content: null,
            title: null,
            tpl: {
                wrap: '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
                image: '<img class="fancybox-image" src="{href}" alt="" />',
                iframe: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen' + (l ? ' allowtransparency="true"' : "") + "></iframe>",
                error: '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
                closeBtn: '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>',
                next: '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
                prev: '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
            },
            openEffect: "fade",
            openSpeed: 250,
            openEasing: "swing",
            openOpacity: !0,
            openMethod: "zoomIn",
            closeEffect: "fade",
            closeSpeed: 250,
            closeEasing: "swing",
            closeOpacity: !0,
            closeMethod: "zoomOut",
            nextEffect: "elastic",
            nextSpeed: 250,
            nextEasing: "swing",
            nextMethod: "changeIn",
            prevEffect: "elastic",
            prevSpeed: 250,
            prevEasing: "swing",
            prevMethod: "changeOut",
            helpers: {
                overlay: !0,
                title: !0
            },
            onCancel: o.noop,
            beforeLoad: o.noop,
            afterLoad: o.noop,
            beforeShow: o.noop,
            afterShow: o.noop,
            beforeChange: o.noop,
            beforeClose: o.noop,
            afterClose: o.noop
        },
        group: {},
        opts: {},
        previous: null,
        coming: null,
        current: null,
        isActive: !1,
        isOpen: !1,
        isOpened: !1,
        wrap: null,
        skin: null,
        outer: null,
        inner: null,
        player: {
            timer: null,
            isActive: !1
        },
        ajaxLoad: null,
        imgPreload: null,
        transitions: {},
        helpers: {},
        open: function(e, t) {
            return e && (o.isPlainObject(t) || (t = {}), !1 !== s.close(!0)) ? (o.isArray(e) || (e = u(e) ? o(e).get() : [e]), o.each(e, function(r, i) {
                var a, l, c, d, f, h = {};
                "object" === o.type(i) && (i.nodeType && (i = o(i)), u(i) ? (h = {
                    href: i.data("fancybox-href") || i.attr("href"),
                    title: i.data("fancybox-title") || i.attr("title"),
                    isDom: !0,
                    element: i
                }, o.metadata && o.extend(!0, h, i.metadata())) : h = i), a = t.href || h.href || (p(i) ? i : null), l = t.title !== n ? t.title : h.title || "", d = (c = t.content || h.content) ? "html" : t.type || h.type, !d && h.isDom && (d = i.data("fancybox-type"), d || (d = (d = i.prop("class").match(/fancybox\.(\w+)/)) ? d[1] : null)), p(a) && (d || (s.isImage(a) ? d = "image" : s.isSWF(a) ? d = "swf" : "#" === a.charAt(0) ? d = "inline" : p(i) && (d = "html", c = i)), "ajax" === d && (f = a.split(/\s+/, 2), a = f.shift(), f = f.shift())), c || ("inline" === d ? a ? c = o(p(a) ? a.replace(/.*(?=#[^\s]+$)/, "") : a) : h.isDom && (c = i) : "html" === d ? c = a : !d && !a && h.isDom && (d = "inline", c = i)), o.extend(h, {
                    href: a,
                    type: d,
                    content: c,
                    title: l,
                    selector: f
                }), e[r] = h
            }), s.opts = o.extend(!0, {}, s.defaults, t), t.keys !== n && (s.opts.keys = t.keys ? o.extend({}, s.defaults.keys, t.keys) : !1), s.group = e, s._start(s.opts.index)) : void 0
        },
        cancel: function() {
            var e = s.coming;
            e && !1 !== s.trigger("onCancel") && (s.hideLoading(), s.ajaxLoad && s.ajaxLoad.abort(), s.ajaxLoad = null, s.imgPreload && (s.imgPreload.onload = s.imgPreload.onerror = null), e.wrap && e.wrap.stop(!0, !0).trigger("onReset").remove(), s.coming = null, s.current || s._afterZoomOut(e))
        },
        close: function(e) {
            s.cancel(), !1 !== s.trigger("beforeClose") && (s.unbindEvents(), s.isActive && (s.isOpen && !0 !== e ? (s.isOpen = s.isOpened = !1, s.isClosing = !0, o(".fancybox-item, .fancybox-nav").remove(), s.wrap.stop(!0, !0).removeClass("fancybox-opened"), s.transitions[s.current.closeMethod]()) : (o(".fancybox-wrap").stop(!0).trigger("onReset").remove(), s._afterZoomOut())))
        },
        play: function(e) {
            var t = function() {
                    clearTimeout(s.player.timer)
                },
                o = function() {
                    t(), s.current && s.player.isActive && (s.player.timer = setTimeout(s.next, s.current.playSpeed))
                },
                n = function() {
                    t(), a.unbind(".player"), s.player.isActive = !1, s.trigger("onPlayEnd")
                };
            !0 === e || !s.player.isActive && !1 !== e ? s.current && (s.current.loop || s.current.index < s.group.length - 1) && (s.player.isActive = !0, a.bind({
                "onCancel.player beforeClose.player": n,
                "onUpdate.player": o,
                "beforeLoad.player": t
            }), o(), s.trigger("onPlayStart")) : n()
        },
        next: function(e) {
            var t = s.current;
            t && (p(e) || (e = t.direction.next), s.jumpto(t.index + 1, e, "next"))
        },
        prev: function(e) {
            var t = s.current;
            t && (p(e) || (e = t.direction.prev), s.jumpto(t.index - 1, e, "prev"))
        },
        jumpto: function(e, t, o) {
            var r = s.current;
            r && (e = h(e), s.direction = t || r.direction[e >= r.index ? "next" : "prev"], s.router = o || "jumpto", r.loop && (0 > e && (e = r.group.length + e % r.group.length), e %= r.group.length), r.group[e] !== n && (s.cancel(), s._start(e)))
        },
        reposition: function(e, t) {
            var n, r = s.current,
                i = r ? r.wrap : null;
            i && (n = s._getPosition(t), e && "scroll" === e.type ? (delete n.position, i.stop(!0, !0).animate(n, 200)) : (i.css(n), r.pos = o.extend({}, r.dim, n)))
        },
        update: function(e) {
            var t = e && e.type,
                o = !t || "orientationchange" === t;
            o && (clearTimeout(c), c = null), s.isOpen && !c && (c = setTimeout(function() {
                var n = s.current;
                n && !s.isClosing && (s.wrap.removeClass("fancybox-tmp"), (o || "load" === t || "resize" === t && n.autoResize) && s._setDimension(), "scroll" === t && n.canShrink || s.reposition(e), s.trigger("onUpdate"), c = null)
            }, o && !d ? 0 : 300))
        },
        toggle: function(e) {
            s.isOpen && (s.current.fitToView = "boolean" === o.type(e) ? e : !s.current.fitToView, d && (s.wrap.removeAttr("style").addClass("fancybox-tmp"), s.trigger("onUpdate")), s.update())
        },
        hideLoading: function() {
            a.unbind(".loading"), o("#fancybox-loading").remove()
        },
        showLoading: function() {
            var e, t;
            s.hideLoading(), e = o('<div id="fancybox-loading"><div></div></div>').click(s.cancel).appendTo("body"), a.bind("keydown.loading", function(e) {
                27 === (e.which || e.keyCode) && (e.preventDefault(), s.cancel())
            }), s.defaults.fixed || (t = s.getViewport(), e.css({
                position: "absolute",
                top: .5 * t.h + t.y,
                left: .5 * t.w + t.x
            }))
        },
        getViewport: function() {
            var t = s.current && s.current.locked || !1,
                o = {
                    x: i.scrollLeft(),
                    y: i.scrollTop()
                };
            return t ? (o.w = t[0].clientWidth, o.h = t[0].clientHeight) : (o.w = d && e.innerWidth ? e.innerWidth : i.width(), o.h = d && e.innerHeight ? e.innerHeight : i.height()), o
        },
        unbindEvents: function() {
            s.wrap && u(s.wrap) && s.wrap.unbind(".fb"), a.unbind(".fb"), i.unbind(".fb")
        },
        bindEvents: function() {
            var e, t = s.current;
            t && (i.bind("orientationchange.fb" + (d ? "" : " resize.fb") + (t.autoCenter && !t.locked ? " scroll.fb" : ""), s.update), (e = t.keys) && a.bind("keydown.fb", function(r) {
                var i = r.which || r.keyCode,
                    a = r.target || r.srcElement;
                return 27 === i && s.coming ? !1 : void!(r.ctrlKey || r.altKey || r.shiftKey || r.metaKey || a && (a.type || o(a).is("[contenteditable]")) || !o.each(e, function(e, a) {
                    return 1 < t.group.length && a[i] !== n ? (s[e](a[i]), r.preventDefault(), !1) : -1 < o.inArray(i, a) ? (s[e](), r.preventDefault(), !1) : void 0
                }))
            }), o.fn.mousewheel && t.mouseWheel && s.wrap.bind("mousewheel.fb", function(e, n, r, i) {
                for (var a = o(e.target || null), l = !1; a.length && !l && !a.is(".fancybox-skin") && !a.is(".fancybox-wrap");) l = a[0] && !(a[0].style.overflow && "hidden" === a[0].style.overflow) && (a[0].clientWidth && a[0].scrollWidth > a[0].clientWidth || a[0].clientHeight && a[0].scrollHeight > a[0].clientHeight), a = o(a).parent();
                0 !== n && !l && 1 < s.group.length && !t.canShrink && (i > 0 || r > 0 ? s.prev(i > 0 ? "down" : "left") : (0 > i || 0 > r) && s.next(0 > i ? "up" : "right"), e.preventDefault())
            }))
        },
        trigger: function(e, t) {
            var n, r = t || s.coming || s.current;
            if (r) {
                if (o.isFunction(r[e]) && (n = r[e].apply(r, Array.prototype.slice.call(arguments, 1))), !1 === n) return !1;
                r.helpers && o.each(r.helpers, function(t, n) {
                    n && s.helpers[t] && o.isFunction(s.helpers[t][e]) && s.helpers[t][e](o.extend(!0, {}, s.helpers[t].defaults, n), r)
                }), a.trigger(e)
            }
        },
        isImage: function(e) {
            return p(e) && e.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i)
        },
        isSWF: function(e) {
            return p(e) && e.match(/\.(swf)((\?|#).*)?$/i)
        },
        _start: function(e) {
            var t, n, r = {};
            if (e = h(e), t = s.group[e] || null, !t) return !1;
            if (r = o.extend(!0, {}, s.opts, t), t = r.margin, n = r.padding, "number" === o.type(t) && (r.margin = [t, t, t, t]), "number" === o.type(n) && (r.padding = [n, n, n, n]), r.modal && o.extend(!0, r, {
                    closeBtn: !1,
                    closeClick: !1,
                    nextClick: !1,
                    arrows: !1,
                    mouseWheel: !1,
                    keys: null,
                    helpers: {
                        overlay: {
                            closeClick: !1
                        }
                    }
                }), r.autoSize && (r.autoWidth = r.autoHeight = !0), "auto" === r.width && (r.autoWidth = !0), "auto" === r.height && (r.autoHeight = !0), r.group = s.group, r.index = e, s.coming = r, !1 === s.trigger("beforeLoad")) s.coming = null;
            else {
                if (n = r.type, t = r.href, !n) return s.coming = null, s.current && s.router && "jumpto" !== s.router ? (s.current.index = e, s[s.router](s.direction)) : !1;
                if (s.isActive = !0, ("image" === n || "swf" === n) && (r.autoHeight = r.autoWidth = !1, r.scrolling = "visible"), "image" === n && (r.aspectRatio = !0), "iframe" === n && d && (r.scrolling = "scroll"), r.wrap = o(r.tpl.wrap).addClass("fancybox-" + (d ? "mobile" : "desktop") + " fancybox-type-" + n + " fancybox-tmp " + r.wrapCSS).appendTo(r.parent || "body"), o.extend(r, {
                        skin: o(".fancybox-skin", r.wrap),
                        outer: o(".fancybox-outer", r.wrap),
                        inner: o(".fancybox-inner", r.wrap)
                    }), o.each(["Top", "Right", "Bottom", "Left"], function(e, t) {
                        r.skin.css("padding" + t, g(r.padding[e]))
                    }), s.trigger("onReady"), "inline" === n || "html" === n) {
                    if (!r.content || !r.content.length) return s._error("content")
                } else if (!t) return s._error("href");
                "image" === n ? s._loadImage() : "ajax" === n ? s._loadAjax() : "iframe" === n ? s._loadIframe() : s._afterLoad()
            }
        },
        _error: function(e) {
            o.extend(s.coming, {
                type: "html",
                autoWidth: !0,
                autoHeight: !0,
                minWidth: 0,
                minHeight: 0,
                scrolling: "no",
                hasError: e,
                content: s.coming.tpl.error
            }), s._afterLoad()
        },
        _loadImage: function() {
            var e = s.imgPreload = new Image;
            e.onload = function() {
                this.onload = this.onerror = null, s.coming.width = this.width / s.opts.pixelRatio, s.coming.height = this.height / s.opts.pixelRatio, s._afterLoad()
            }, e.onerror = function() {
                this.onload = this.onerror = null, s._error("image")
            }, e.src = s.coming.href, !0 !== e.complete && s.showLoading()
        },
        _loadAjax: function() {
            var e = s.coming;
            s.showLoading(), s.ajaxLoad = o.ajax(o.extend({}, e.ajax, {
                url: e.href,
                error: function(e, t) {
                    s.coming && "abort" !== t ? s._error("ajax", e) : s.hideLoading()
                },
                success: function(t, o) {
                    "success" === o && (e.content = t, s._afterLoad())
                }
            }))
        },
        _loadIframe: function() {
            var e = s.coming,
                t = o(e.tpl.iframe.replace(/\{rnd\}/g, (new Date).getTime())).attr("scrolling", d ? "auto" : e.iframe.scrolling).attr("src", e.href);
            o(e.wrap).bind("onReset", function() {
                try {
                    o(this).find("iframe").hide().attr("src", "//about:blank").end().empty()
                } catch (e) {}
            }), e.iframe.preload && (s.showLoading(), t.one("load", function() {
                o(this).data("ready", 1), d || o(this).bind("load.fb", s.update), o(this).parents(".fancybox-wrap").width("100%").removeClass("fancybox-tmp").show(), s._afterLoad()
            })), e.content = t.appendTo(e.inner), e.iframe.preload || s._afterLoad()
        },
        _preloadImages: function() {
            var e, t, o = s.group,
                n = s.current,
                r = o.length,
                i = n.preload ? Math.min(n.preload, r - 1) : 0;
            for (t = 1; i >= t; t += 1) e = o[(n.index + t) % r], "image" === e.type && e.href && ((new Image).src = e.href)
        },
        _afterLoad: function() {
            var e, t, n, r, i, a = s.coming,
                l = s.current;
            if (s.hideLoading(), a && !1 !== s.isActive)
                if (!1 === s.trigger("afterLoad", a, l)) a.wrap.stop(!0).trigger("onReset").remove(), s.coming = null;
                else {
                    switch (l && (s.trigger("beforeChange", l), l.wrap.stop(!0).removeClass("fancybox-opened").find(".fancybox-item, .fancybox-nav").remove()), s.unbindEvents(), e = a.content, t = a.type, n = a.scrolling, o.extend(s, {
                        wrap: a.wrap,
                        skin: a.skin,
                        outer: a.outer,
                        inner: a.inner,
                        current: a,
                        previous: l
                    }), r = a.href, t) {
                        case "inline":
                        case "ajax":
                        case "html":
                            a.selector ? e = o("<div>").html(e).find(a.selector) : u(e) && (e.data("fancybox-placeholder") || e.data("fancybox-placeholder", o('<div class="fancybox-placeholder"></div>').insertAfter(e).hide()), e = e.show().detach(), a.wrap.bind("onReset", function() {
                                o(this).find(e).length && e.hide().replaceAll(e.data("fancybox-placeholder")).data("fancybox-placeholder", !1)
                            }));
                            break;
                        case "image":
                            e = a.tpl.image.replace("{href}", r);
                            break;
                        case "swf":
                            e = '<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="' + r + '"></param>', i = "", o.each(a.swf, function(t, o) {
                                e += '<param name="' + t + '" value="' + o + '"></param>', i += " " + t + '="' + o + '"'
                            }), e += '<embed src="' + r + '" type="application/x-shockwave-flash" width="100%" height="100%"' + i + "></embed></object>"
                    }(!u(e) || !e.parent().is(a.inner)) && a.inner.append(e), s.trigger("beforeShow"), a.inner.css("overflow", "yes" === n ? "scroll" : "no" === n ? "hidden" : n), s._setDimension(), s.reposition(), s.isOpen = !1, s.coming = null, s.bindEvents(), s.isOpened ? l.prevMethod && s.transitions[l.prevMethod]() : o(".fancybox-wrap").not(a.wrap).stop(!0).trigger("onReset").remove(), s.transitions[s.isOpened ? a.nextMethod : a.openMethod](), s._preloadImages()
                }
        },
        _setDimension: function() {
            var e, t, n, r, i, a, l, c, d, u = s.getViewport(),
                p = 0,
                v = !1,
                m = !1,
                v = s.wrap,
                b = s.skin,
                y = s.inner,
                w = s.current,
                m = w.width,
                x = w.height,
                _ = w.minWidth,
                T = w.minHeight,
                S = w.maxWidth,
                P = w.maxHeight,
                k = w.scrolling,
                $ = w.scrollOutside ? w.scrollbarWidth : 0,
                A = w.margin,
                M = h(A[1] + A[3]),
                L = h(A[0] + A[2]);
            if (v.add(b).add(y).width("auto").height("auto").removeClass("fancybox-tmp"), A = h(b.outerWidth(!0) - b.width()), e = h(b.outerHeight(!0) - b.height()), t = M + A, n = L + e, r = f(m) ? (u.w - t) * h(m) / 100 : m, i = f(x) ? (u.h - n) * h(x) / 100 : x, "iframe" === w.type) {
                if (d = w.content, w.autoHeight && 1 === d.data("ready")) try {
                    d[0].contentWindow.document.location && (y.width(r).height(9999), a = d.contents().find("body"), $ && a.css("overflow-x", "hidden"), i = a.outerHeight(!0))
                } catch (C) {}
            } else(w.autoWidth || w.autoHeight) && (y.addClass("fancybox-tmp"), w.autoWidth || y.width(r), w.autoHeight || y.height(i), w.autoWidth && (r = y.width()), w.autoHeight && (i = y.height()), y.removeClass("fancybox-tmp"));
            if (m = h(r), x = h(i), c = r / i, _ = h(f(_) ? h(_, "w") - t : _), S = h(f(S) ? h(S, "w") - t : S), T = h(f(T) ? h(T, "h") - n : T), P = h(f(P) ? h(P, "h") - n : P), a = S, l = P, w.fitToView && (S = Math.min(u.w - t, S), P = Math.min(u.h - n, P)), t = u.w - M, L = u.h - L, w.aspectRatio ? (m > S && (m = S, x = h(m / c)), x > P && (x = P, m = h(x * c)), _ > m && (m = _, x = h(m / c)), T > x && (x = T, m = h(x * c))) : (m = Math.max(_, Math.min(m, S)), w.autoHeight && "iframe" !== w.type && (y.width(m), x = y.height()), x = Math.max(T, Math.min(x, P))), w.fitToView)
                if (y.width(m).height(x), v.width(m + A), u = v.width(), M = v.height(), w.aspectRatio)
                    for (;
                        (u > t || M > L) && m > _ && x > T && !(19 < p++);) x = Math.max(T, Math.min(P, x - 10)), m = h(x * c), _ > m && (m = _, x = h(m / c)), m > S && (m = S, x = h(m / c)), y.width(m).height(x), v.width(m + A), u = v.width(), M = v.height();
                else m = Math.max(_, Math.min(m, m - (u - t))), x = Math.max(T, Math.min(x, x - (M - L)));
            $ && "auto" === k && i > x && t > m + A + $ && (m += $), y.width(m).height(x), v.width(m + A), u = v.width(), M = v.height(), v = (u > t || M > L) && m > _ && x > T, m = w.aspectRatio ? a > m && l > x && r > m && i > x : (a > m || l > x) && (r > m || i > x), o.extend(w, {
                dim: {
                    width: g(u),
                    height: g(M)
                },
                origWidth: r,
                origHeight: i,
                canShrink: v,
                canExpand: m,
                wPadding: A,
                hPadding: e,
                wrapSpace: M - b.outerHeight(!0),
                skinSpace: b.height() - x
            }), !d && w.autoHeight && x > T && P > x && !m && y.height("auto")
        },
        _getPosition: function(e) {
            var t = s.current,
                o = s.getViewport(),
                n = t.margin,
                r = s.wrap.width() + n[1] + n[3],
                i = s.wrap.height() + n[0] + n[2],
                n = {
                    position: "absolute",
                    top: n[0],
                    left: n[3]
                };
            return t.autoCenter && t.fixed && !e && i <= o.h && r <= o.w ? n.position = "fixed" : t.locked || (n.top += o.y, n.left += o.x), n.top = g(Math.max(n.top, n.top + (o.h - i) * t.topRatio)), n.left = g(Math.max(n.left, n.left + (o.w - r) * t.leftRatio)), n
        },
        _afterZoomIn: function() {
            var e = s.current;
            e && (s.isOpen = s.isOpened = !0, s.wrap.css("overflow", "visible").addClass("fancybox-opened"), s.update(), (e.closeClick || e.nextClick && 1 < s.group.length) && s.inner.css("cursor", "pointer").bind("click.fb", function(t) {
                !o(t.target).is("a") && !o(t.target).parent().is("a") && (t.preventDefault(), s[e.closeClick ? "close" : "next"]())
            }), e.closeBtn && o(e.tpl.closeBtn).appendTo(s.skin).bind("click.fb", function(e) {
                e.preventDefault(), s.close()
            }), e.arrows && 1 < s.group.length && ((e.loop || 0 < e.index) && o(e.tpl.prev).appendTo(s.outer).bind("click.fb", s.prev), (e.loop || e.index < s.group.length - 1) && o(e.tpl.next).appendTo(s.outer).bind("click.fb", s.next)), s.trigger("afterShow"), e.loop || e.index !== e.group.length - 1 ? s.opts.autoPlay && !s.player.isActive && (s.opts.autoPlay = !1, s.play()) : s.play(!1))
        },
        _afterZoomOut: function(e) {
            e = e || s.current, o(".fancybox-wrap").trigger("onReset").remove(), o.extend(s, {
                group: {},
                opts: {},
                router: !1,
                current: null,
                isActive: !1,
                isOpened: !1,
                isOpen: !1,
                isClosing: !1,
                wrap: null,
                skin: null,
                outer: null,
                inner: null
            }), s.trigger("afterClose", e)
        }
    }), s.transitions = {
        getOrigPosition: function() {
            var e = s.current,
                t = e.element,
                o = e.orig,
                n = {},
                r = 50,
                i = 50,
                a = e.hPadding,
                l = e.wPadding,
                c = s.getViewport();
            return !o && e.isDom && t.is(":visible") && (o = t.find("img:first"), o.length || (o = t)), u(o) ? (n = o.offset(), o.is("img") && (r = o.outerWidth(), i = o.outerHeight())) : (n.top = c.y + (c.h - i) * e.topRatio, n.left = c.x + (c.w - r) * e.leftRatio), ("fixed" === s.wrap.css("position") || e.locked) && (n.top -= c.y, n.left -= c.x), n = {
                top: g(n.top - a * e.topRatio),
                left: g(n.left - l * e.leftRatio),
                width: g(r + l),
                height: g(i + a)
            }
        },
        step: function(e, t) {
            var o, n, r = t.prop;
            n = s.current;
            var i = n.wrapSpace,
                a = n.skinSpace;
            ("width" === r || "height" === r) && (o = t.end === t.start ? 1 : (e - t.start) / (t.end - t.start), s.isClosing && (o = 1 - o), n = "width" === r ? n.wPadding : n.hPadding, n = e - n, s.skin[r](h("width" === r ? n : n - i * o)), s.inner[r](h("width" === r ? n : n - i * o - a * o)))
        },
        zoomIn: function() {
            var e = s.current,
                t = e.pos,
                n = e.openEffect,
                r = "elastic" === n,
                i = o.extend({
                    opacity: 1
                }, t);
            delete i.position, r ? (t = this.getOrigPosition(), e.openOpacity && (t.opacity = .1)) : "fade" === n && (t.opacity = .1), s.wrap.css(t).animate(i, {
                duration: "none" === n ? 0 : e.openSpeed,
                easing: e.openEasing,
                step: r ? this.step : null,
                complete: s._afterZoomIn
            })
        },
        zoomOut: function() {
            var e = s.current,
                t = e.closeEffect,
                o = "elastic" === t,
                n = {
                    opacity: .1
                };
            o && (n = this.getOrigPosition(), e.closeOpacity && (n.opacity = .1)), s.wrap.animate(n, {
                duration: "none" === t ? 0 : e.closeSpeed,
                easing: e.closeEasing,
                step: o ? this.step : null,
                complete: s._afterZoomOut
            })
        },
        changeIn: function() {
            var e, t = s.current,
                o = t.nextEffect,
                n = t.pos,
                r = {
                    opacity: 1
                },
                i = s.direction;
            n.opacity = .1, "elastic" === o && (e = "down" === i || "up" === i ? "top" : "left", "down" === i || "right" === i ? (n[e] = g(h(n[e]) - 200), r[e] = "+=200px") : (n[e] = g(h(n[e]) + 200), r[e] = "-=200px")), "none" === o ? s._afterZoomIn() : s.wrap.css(n).animate(r, {
                duration: t.nextSpeed,
                easing: t.nextEasing,
                complete: s._afterZoomIn
            })
        },
        changeOut: function() {
            var e = s.previous,
                t = e.prevEffect,
                n = {
                    opacity: .1
                },
                r = s.direction;
            "elastic" === t && (n["down" === r || "up" === r ? "top" : "left"] = ("up" === r || "left" === r ? "-" : "+") + "=200px"), e.wrap.animate(n, {
                duration: "none" === t ? 0 : e.prevSpeed,
                easing: e.prevEasing,
                complete: function() {
                    o(this).trigger("onReset").remove()
                }
            })
        }
    }, s.helpers.overlay = {
        defaults: {
            closeClick: !0,
            speedOut: 200,
            showEarly: !0,
            css: {},
            locked: !d,
            fixed: !0
        },
        overlay: null,
        fixed: !1,
        el: o("html"),
        create: function(e) {
            e = o.extend({}, this.defaults, e), this.overlay && this.close(), this.overlay = o('<div class="fancybox-overlay"></div>').appendTo(s.coming ? s.coming.parent : e.parent), this.fixed = !1, e.fixed && s.defaults.fixed && (this.overlay.addClass("fancybox-overlay-fixed"), this.fixed = !0)
        },
        open: function(e) {
            var t = this;
            e = o.extend({}, this.defaults, e), this.overlay ? this.overlay.unbind(".overlay").width("auto").height("auto") : this.create(e), this.fixed || (i.bind("resize.overlay", o.proxy(this.update, this)), this.update()), e.closeClick && this.overlay.bind("click.overlay", function(e) {
                return o(e.target).hasClass("fancybox-overlay") ? (s.isActive ? s.close() : t.close(), !1) : void 0
            }), this.overlay.css(e.css).show()
        },
        close: function() {
            var e, t;
            i.unbind("resize.overlay"), this.el.hasClass("fancybox-lock") && (o(".fancybox-margin").removeClass("fancybox-margin"), e = i.scrollTop(), t = i.scrollLeft(), this.el.removeClass("fancybox-lock"), i.scrollTop(e).scrollLeft(t)), o(".fancybox-overlay").remove().hide(), o.extend(this, {
                overlay: null,
                fixed: !1
            })
        },
        update: function() {
            var e, o = "100%";
            this.overlay.width(o).height("100%"), l ? (e = Math.max(t.documentElement.offsetWidth, t.body.offsetWidth), a.width() > e && (o = a.width())) : a.width() > i.width() && (o = a.width()), this.overlay.width(o).height(a.height())
        },
        onReady: function(e, t) {
            var n = this.overlay;
            o(".fancybox-overlay").stop(!0, !0), n || this.create(e), e.locked && this.fixed && t.fixed && (n || (this.margin = a.height() > i.height() ? o("html").css("margin-right").replace("px", "") : !1), t.locked = this.overlay.append(t.wrap), t.fixed = !1), !0 === e.showEarly && this.beforeShow.apply(this, arguments)
        },
        beforeShow: function(e, t) {
            var n, r;
            t.locked && (!1 !== this.margin && (o("*").filter(function() {
                return "fixed" === o(this).css("position") && !o(this).hasClass("fancybox-overlay") && !o(this).hasClass("fancybox-wrap")
            }).addClass("fancybox-margin"), this.el.addClass("fancybox-margin")), n = i.scrollTop(), r = i.scrollLeft(), this.el.addClass("fancybox-lock"), i.scrollTop(n).scrollLeft(r)), this.open(e)
        },
        onUpdate: function() {
            this.fixed || this.update()
        },
        afterClose: function(e) {
            this.overlay && !s.coming && this.overlay.fadeOut(e.speedOut, o.proxy(this.close, this))
        }
    }, s.helpers.title = {
        defaults: {
            type: "float",
            position: "bottom"
        },
        beforeShow: function(e) {
            var t = s.current,
                n = t.title,
                r = e.type;
            if (o.isFunction(n) && (n = n.call(t.element, t)), p(n) && "" !== o.trim(n)) {
                switch (t = o('<div class="fancybox-title fancybox-title-' + r + '-wrap">' + n + "</div>"), r) {
                    case "inside":
                        r = s.skin;
                        break;
                    case "outside":
                        r = s.wrap;
                        break;
                    case "over":
                        r = s.inner;
                        break;
                    default:
                        r = s.skin, t.appendTo("body"), l && t.width(t.width()), t.wrapInner('<span class="child"></span>'), s.current.margin[2] += Math.abs(h(t.css("margin-bottom")))
                }
                t["top" === e.position ? "prependTo" : "appendTo"](r)
            }
        }
    }, o.fn.fancybox = function(e) {
        var t, n = o(this),
            r = this.selector || "",
            i = function(i) {
                var a, l, c = o(this).blur(),
                    d = t;
                !(i.ctrlKey || i.altKey || i.shiftKey || i.metaKey || c.is(".fancybox-wrap") || (a = e.groupAttr || "data-fancybox-group", l = c.attr(a), l || (a = "rel", l = c.get(0)[a]), l && "" !== l && "nofollow" !== l && (c = r.length ? o(r) : n, c = c.filter("[" + a + '="' + l + '"]'), d = c.index(this)), e.index = d, !1 === s.open(c, e) || !i.preventDefault()))
            };
        return e = e || {}, t = e.index || 0, r && !1 !== e.live ? a.undelegate(r, "click.fb-start").delegate(r + ":not('.fancybox-item, .fancybox-nav')", "click.fb-start", i) : n.unbind("click.fb-start").bind("click.fb-start", i), this.filter("[data-fancybox-start=1]").trigger("click"), this
    }, a.ready(function() {
        var t, i;
        if (o.scrollbarWidth === n && (o.scrollbarWidth = function() {
                var e = o('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo("body"),
                    t = e.children(),
                    t = t.innerWidth() - t.height(99).innerWidth();
                return e.remove(), t
            }), o.support.fixedPosition === n) {
            t = o.support, i = o('<div style="position:fixed;top:20px;"></div>').appendTo("body");
            var a = 20 === i[0].offsetTop || 15 === i[0].offsetTop;
            i.remove(), t.fixedPosition = a
        }
        o.extend(s.defaults, {
            scrollbarWidth: o.scrollbarWidth(),
            fixed: o.support.fixedPosition,
            parent: o("body")
        }), t = o(e).width(), r.addClass("fancybox-lock-test"), i = o(e).width(), r.removeClass("fancybox-lock-test"), o("<style type='text/css'>.fancybox-margin{margin-right:" + (i - t) + "px;}</style>").appendTo("head")
    })
}(window, document, jQuery), ! function() {
    var e = {},
        t = null,
        o = !0,
        n = !1;
    try {
        "undefined" != typeof AudioContext ? t = new AudioContext : "undefined" != typeof webkitAudioContext ? t = new webkitAudioContext : o = !1
    } catch (r) {
        o = !1
    }
    if (!o)
        if ("undefined" != typeof Audio) try {
            new Audio
        } catch (r) {
            n = !0
        } else n = !0;
    if (o) {
        var i = "undefined" == typeof t.createGain ? t.createGainNode() : t.createGain();
        i.gain.value = 1, i.connect(t.destination)
    }
    var a = function(e) {
        this._volume = 1, this._muted = !1, this.usingWebAudio = o, this.ctx = t, this.noAudio = n, this._howls = [], this._codecs = e, this.iOSAutoEnable = !0
    };
    a.prototype = {
        volume: function(e) {
            var t = this;
            if (e = parseFloat(e), e >= 0 && 1 >= e) {
                t._volume = e, o && (i.gain.value = e);
                for (var n in t._howls)
                    if (t._howls.hasOwnProperty(n) && t._howls[n]._webAudio === !1)
                        for (var r = 0; r < t._howls[n]._audioNode.length; r++) t._howls[n]._audioNode[r].volume = t._howls[n]._volume * t._volume;
                return t
            }
            return o ? i.gain.value : t._volume
        },
        mute: function() {
            return this._setMuted(!0), this
        },
        unmute: function() {
            return this._setMuted(!1), this
        },
        _setMuted: function(e) {
            var t = this;
            t._muted = e, o && (i.gain.value = e ? 0 : t._volume);
            for (var n in t._howls)
                if (t._howls.hasOwnProperty(n) && t._howls[n]._webAudio === !1)
                    for (var r = 0; r < t._howls[n]._audioNode.length; r++) t._howls[n]._audioNode[r].muted = e
        },
        codecs: function(e) {
            return this._codecs[e]
        },
        _enableiOSAudio: function() {
            var e = this;
            if (!t || !e._iOSEnabled && /iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                e._iOSEnabled = !1;
                var o = function() {
                    var n = t.createBuffer(1, 1, 22050),
                        r = t.createBufferSource();
                    r.buffer = n, r.connect(t.destination), "undefined" == typeof r.start ? r.noteOn(0) : r.start(0), setTimeout(function() {
                        (r.playbackState === r.PLAYING_STATE || r.playbackState === r.FINISHED_STATE) && (e._iOSEnabled = !0, e.iOSAutoEnable = !1, window.removeEventListener("touchstart", o, !1))
                    }, 0)
                };
                return window.addEventListener("touchstart", o, !1), e
            }
        }
    };
    var s = null,
        l = {};
    n || (s = new Audio, l = {
        mp3: !!s.canPlayType("audio/mpeg;").replace(/^no$/, ""),
        opus: !!s.canPlayType('audio/ogg; codecs="opus"').replace(/^no$/, ""),
        ogg: !!s.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""),
        wav: !!s.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""),
        aac: !!s.canPlayType("audio/aac;").replace(/^no$/, ""),
        m4a: !!(s.canPlayType("audio/x-m4a;") || s.canPlayType("audio/m4a;") || s.canPlayType("audio/aac;")).replace(/^no$/, ""),
        mp4: !!(s.canPlayType("audio/x-mp4;") || s.canPlayType("audio/mp4;") || s.canPlayType("audio/aac;")).replace(/^no$/, ""),
        weba: !!s.canPlayType('audio/webm; codecs="vorbis"').replace(/^no$/, "")
    });
    var c = new a(l),
        d = function(e) {
            var n = this;
            n._autoplay = e.autoplay || !1, n._buffer = e.buffer || !1, n._duration = e.duration || 0, n._format = e.format || null, n._loop = e.loop || !1, n._loaded = !1, n._sprite = e.sprite || {}, n._src = e.src || "", n._pos3d = e.pos3d || [0, 0, -.5], n._volume = void 0 !== e.volume ? e.volume : 1, n._urls = e.urls || [], n._rate = e.rate || 1, n._model = e.model || null, n._onload = [e.onload || function() {}], n._onloaderror = [e.onloaderror || function() {}], n._onend = [e.onend || function() {}], n._onpause = [e.onpause || function() {}], n._onplay = [e.onplay || function() {}], n._onendTimer = [], n._webAudio = o && !n._buffer, n._audioNode = [], n._webAudio && n._setupAudioNode(), "undefined" != typeof t && t && c.iOSAutoEnable && c._enableiOSAudio(), c._howls.push(n), n.load()
        };
    if (d.prototype = {
            load: function() {
                var e = this,
                    t = null;
                if (n) return void e.on("loaderror");
                for (var o = 0; o < e._urls.length; o++) {
                    var r, i;
                    if (e._format) r = e._format;
                    else {
                        if (i = e._urls[o], r = /^data:audio\/([^;,]+);/i.exec(i), r || (r = /\.([^.]+)$/.exec(i.split("?", 1)[0])), !r) return void e.on("loaderror");
                        r = r[1].toLowerCase()
                    }
                    if (l[r]) {
                        t = e._urls[o];
                        break
                    }
                }
                if (!t) return void e.on("loaderror");
                if (e._src = t, e._webAudio) u(e, t);
                else {
                    var s = new Audio;
                    s.addEventListener("error", function() {
                        s.error && 4 === s.error.code && (a.noAudio = !0), e.on("loaderror", {
                            type: s.error ? s.error.code : 0
                        })
                    }, !1), e._audioNode.push(s), s.src = t, s._pos = 0, s.preload = "auto", s.volume = c._muted ? 0 : e._volume * c.volume();
                    var d = function() {
                        e._duration = Math.ceil(10 * s.duration) / 10, 0 === Object.getOwnPropertyNames(e._sprite).length && (e._sprite = {
                            _default: [0, 1e3 * e._duration]
                        }), e._loaded || (e._loaded = !0, e.on("load")), e._autoplay && e.play(), s.removeEventListener("canplaythrough", d, !1)
                    };
                    s.addEventListener("canplaythrough", d, !1), s.load()
                }
                return e
            },
            urls: function(e) {
                var t = this;
                return e ? (t.stop(), t._urls = "string" == typeof e ? [e] : e, t._loaded = !1, t.load(), t) : t._urls
            },
            play: function(e, o) {
                var n = this;
                return "function" == typeof e && (o = e), e && "function" != typeof e || (e = "_default"), n._loaded ? n._sprite[e] ? (n._inactiveNode(function(r) {
                    r._sprite = e;
                    var i = r._pos > 0 ? r._pos : n._sprite[e][0] / 1e3,
                        a = 0;
                    n._webAudio ? (a = n._sprite[e][1] / 1e3 - r._pos, r._pos > 0 && (i = n._sprite[e][0] / 1e3 + i)) : a = n._sprite[e][1] / 1e3 - (i - n._sprite[e][0] / 1e3);
                    var s, l = !(!n._loop && !n._sprite[e][2]),
                        d = "string" == typeof o ? o : Math.round(Date.now() * Math.random()) + "";
                    if (function() {
                            var t = {
                                id: d,
                                sprite: e,
                                loop: l
                            };
                            s = setTimeout(function() {
                                !n._webAudio && l && n.stop(t.id).play(e, t.id), n._webAudio && !l && (n._nodeById(t.id).paused = !0, n._nodeById(t.id)._pos = 0, n._clearEndTimer(t.id)), n._webAudio || l || n.stop(t.id), n.on("end", d)
                            }, 1e3 * a), n._onendTimer.push({
                                timer: s,
                                id: t.id
                            })
                        }(), n._webAudio) {
                        var u = n._sprite[e][0] / 1e3,
                            p = n._sprite[e][1] / 1e3;
                        r.id = d, r.paused = !1, h(n, [l, u, p], d), n._playStart = t.currentTime, r.gain.value = n._volume, "undefined" == typeof r.bufferSource.start ? l ? r.bufferSource.noteGrainOn(0, i, 86400) : r.bufferSource.noteGrainOn(0, i, a) : l ? r.bufferSource.start(0, i, 86400) : r.bufferSource.start(0, i, a)
                    } else {
                        if (4 !== r.readyState && (r.readyState || !navigator.isCocoonJS)) return n._clearEndTimer(d),
                            function() {
                                var t = n,
                                    i = e,
                                    a = o,
                                    s = r,
                                    l = function() {
                                        t.play(i, a), s.removeEventListener("canplaythrough", l, !1)
                                    };
                                s.addEventListener("canplaythrough", l, !1)
                            }(), n;
                        r.readyState = 4, r.id = d, r.currentTime = i, r.muted = c._muted || r.muted, r.volume = n._volume * c.volume(), setTimeout(function() {
                            r.play()
                        }, 0)
                    }
                    return n.on("play"), "function" == typeof o && o(d), n
                }), n) : ("function" == typeof o && o(), n) : (n.on("load", function() {
                    n.play(e, o)
                }), n)
            },
            pause: function(e) {
                var t = this;
                if (!t._loaded) return t.on("play", function() {
                    t.pause(e)
                }), t;
                t._clearEndTimer(e);
                var o = e ? t._nodeById(e) : t._activeNode();
                if (o)
                    if (o._pos = t.pos(null, e), t._webAudio) {
                        if (!o.bufferSource || o.paused) return t;
                        o.paused = !0, "undefined" == typeof o.bufferSource.stop ? o.bufferSource.noteOff(0) : o.bufferSource.stop(0)
                    } else o.pause();
                return t.on("pause"), t
            },
            stop: function(e) {
                var t = this;
                if (!t._loaded) return t.on("play", function() {
                    t.stop(e)
                }), t;
                t._clearEndTimer(e);
                var o = e ? t._nodeById(e) : t._activeNode();
                if (o)
                    if (o._pos = 0, t._webAudio) {
                        if (!o.bufferSource || o.paused) return t;
                        o.paused = !0, "undefined" == typeof o.bufferSource.stop ? o.bufferSource.noteOff(0) : o.bufferSource.stop(0)
                    } else isNaN(o.duration) || (o.pause(), o.currentTime = 0);
                return t
            },
            mute: function(e) {
                var t = this;
                if (!t._loaded) return t.on("play", function() {
                    t.mute(e)
                }), t;
                var o = e ? t._nodeById(e) : t._activeNode();
                return o && (t._webAudio ? o.gain.value = 0 : o.muted = !0), t
            },
            unmute: function(e) {
                var t = this;
                if (!t._loaded) return t.on("play", function() {
                    t.unmute(e)
                }), t;
                var o = e ? t._nodeById(e) : t._activeNode();
                return o && (t._webAudio ? o.gain.value = t._volume : o.muted = !1), t
            },
            volume: function(e, t) {
                var o = this;
                if (e = parseFloat(e), e >= 0 && 1 >= e) {
                    if (o._volume = e, !o._loaded) return o.on("play", function() {
                        o.volume(e, t)
                    }), o;
                    var n = t ? o._nodeById(t) : o._activeNode();
                    return n && (o._webAudio ? n.gain.value = e : n.volume = e * c.volume()), o
                }
                return o._volume
            },
            loop: function(e) {
                var t = this;
                return "boolean" == typeof e ? (t._loop = e, t) : t._loop
            },
            sprite: function(e) {
                var t = this;
                return "object" == typeof e ? (t._sprite = e, t) : t._sprite
            },
            pos: function(e, o) {
                var n = this;
                if (!n._loaded) return n.on("load", function() {
                    n.pos(e)
                }), "number" == typeof e ? n : n._pos || 0;
                e = parseFloat(e);
                var r = o ? n._nodeById(o) : n._activeNode();
                if (r) return e >= 0 ? (n.pause(o), r._pos = e, n.play(r._sprite, o), n) : n._webAudio ? r._pos + (t.currentTime - n._playStart) : r.currentTime;
                if (e >= 0) return n;
                for (var i = 0; i < n._audioNode.length; i++)
                    if (n._audioNode[i].paused && 4 === n._audioNode[i].readyState) return n._webAudio ? n._audioNode[i]._pos : n._audioNode[i].currentTime
            },
            pos3d: function(e, t, o, n) {
                var r = this;
                if (t = "undefined" != typeof t && t ? t : 0, o = "undefined" != typeof o && o ? o : -.5, !r._loaded) return r.on("play", function() {
                    r.pos3d(e, t, o, n)
                }), r;
                if (!(e >= 0 || 0 > e)) return r._pos3d;
                if (r._webAudio) {
                    var i = n ? r._nodeById(n) : r._activeNode();
                    i && (r._pos3d = [e, t, o], i.panner.setPosition(e, t, o), i.panner.panningModel = r._model || "HRTF")
                }
                return r
            },
            fade: function(e, t, o, n, r) {
                var i = this,
                    a = Math.abs(e - t),
                    s = e > t ? "down" : "up",
                    l = a / .01,
                    c = o / l;
                if (!i._loaded) return i.on("load", function() {
                    i.fade(e, t, o, n, r)
                }), i;
                i.volume(e, r);
                for (var d = 1; l >= d; d++) ! function() {
                    var e = i._volume + ("up" === s ? .01 : -.01) * d,
                        o = Math.round(1e3 * e) / 1e3,
                        a = t;
                    setTimeout(function() {
                        i.volume(o, r), o === a && n && n()
                    }, c * d)
                }()
            },
            fadeIn: function(e, t, o) {
                return this.volume(0).play().fade(0, e, t, o)
            },
            fadeOut: function(e, t, o, n) {
                var r = this;
                return r.fade(r._volume, e, t, function() {
                    o && o(), r.pause(n), r.on("end")
                }, n)
            },
            _nodeById: function(e) {
                for (var t = this, o = t._audioNode[0], n = 0; n < t._audioNode.length; n++)
                    if (t._audioNode[n].id === e) {
                        o = t._audioNode[n];
                        break
                    }
                return o
            },
            _activeNode: function() {
                for (var e = this, t = null, o = 0; o < e._audioNode.length; o++)
                    if (!e._audioNode[o].paused) {
                        t = e._audioNode[o];
                        break
                    }
                return e._drainPool(), t
            },
            _inactiveNode: function(e) {
                for (var t = this, o = null, n = 0; n < t._audioNode.length; n++)
                    if (t._audioNode[n].paused && 4 === t._audioNode[n].readyState) {
                        e(t._audioNode[n]), o = !0;
                        break
                    }
                if (t._drainPool(), !o) {
                    var r;
                    if (t._webAudio) r = t._setupAudioNode(), e(r);
                    else {
                        t.load(), r = t._audioNode[t._audioNode.length - 1];
                        var i = navigator.isCocoonJS ? "canplaythrough" : "loadedmetadata",
                            a = function() {
                                r.removeEventListener(i, a, !1), e(r)
                            };
                        r.addEventListener(i, a, !1)
                    }
                }
            },
            _drainPool: function() {
                var e, t = this,
                    o = 0;
                for (e = 0; e < t._audioNode.length; e++) t._audioNode[e].paused && o++;
                for (e = t._audioNode.length - 1; e >= 0 && !(5 >= o); e--) t._audioNode[e].paused && (t._webAudio && t._audioNode[e].disconnect(0), o--, t._audioNode.splice(e, 1))
            },
            _clearEndTimer: function(e) {
                for (var t = this, o = 0, n = 0; n < t._onendTimer.length; n++)
                    if (t._onendTimer[n].id === e) {
                        o = n;
                        break
                    }
                var r = t._onendTimer[o];
                r && (clearTimeout(r.timer), t._onendTimer.splice(o, 1))
            },
            _setupAudioNode: function() {
                var e = this,
                    o = e._audioNode,
                    n = e._audioNode.length;
                return o[n] = "undefined" == typeof t.createGain ? t.createGainNode() : t.createGain(), o[n].gain.value = e._volume, o[n].paused = !0, o[n]._pos = 0, o[n].readyState = 4, o[n].connect(i), o[n].panner = t.createPanner(), o[n].panner.panningModel = e._model || "equalpower", o[n].panner.setPosition(e._pos3d[0], e._pos3d[1], e._pos3d[2]), o[n].panner.connect(o[n]), o[n]
            },
            on: function(e, t) {
                var o = this,
                    n = o["_on" + e];
                if ("function" == typeof t) n.push(t);
                else
                    for (var r = 0; r < n.length; r++) t ? n[r].call(o, t) : n[r].call(o);
                return o
            },
            off: function(e, t) {
                var o = this,
                    n = o["_on" + e],
                    r = t ? t.toString() : null;
                if (r) {
                    for (var i = 0; i < n.length; i++)
                        if (r === n[i].toString()) {
                            n.splice(i, 1);
                            break
                        }
                } else o["_on" + e] = [];
                return o
            },
            unload: function() {
                for (var t = this, o = t._audioNode, n = 0; n < t._audioNode.length; n++) o[n].paused || (t.stop(o[n].id), t.on("end", o[n].id)), t._webAudio ? o[n].disconnect(0) : o[n].src = "";
                for (n = 0; n < t._onendTimer.length; n++) clearTimeout(t._onendTimer[n].timer);
                var r = c._howls.indexOf(t);
                null !== r && r >= 0 && c._howls.splice(r, 1), delete e[t._src], t = null
            }
        }, o) var u = function(t, o) {
            if (o in e) return t._duration = e[o].duration, void f(t);
            if (/^data:[^;]+;base64,/.test(o)) {
                for (var n = atob(o.split(",")[1]), r = new Uint8Array(n.length), i = 0; i < n.length; ++i) r[i] = n.charCodeAt(i);
                p(r.buffer, t, o)
            } else {
                var a = new XMLHttpRequest;
                a.open("GET", o, !0), a.responseType = "arraybuffer", a.onload = function() {
                    p(a.response, t, o)
                }, a.onerror = function() {
                    t._webAudio && (t._buffer = !0, t._webAudio = !1, t._audioNode = [], delete t._gainNode, delete e[o], t.load())
                };
                try {
                    a.send()
                } catch (s) {
                    a.onerror()
                }
            }
        },
        p = function(o, n, r) {
            t.decodeAudioData(o, function(t) {
                t && (e[r] = t, f(n, t))
            }, function(e) {
                n.on("loaderror")
            })
        },
        f = function(e, t) {
            e._duration = t ? t.duration : e._duration, 0 === Object.getOwnPropertyNames(e._sprite).length && (e._sprite = {
                _default: [0, 1e3 * e._duration]
            }), e._loaded || (e._loaded = !0, e.on("load")), e._autoplay && e.play()
        },
        h = function(o, n, r) {
            var i = o._nodeById(r);
            i.bufferSource = t.createBufferSource(), i.bufferSource.buffer = e[o._src], i.bufferSource.connect(i.panner), i.bufferSource.loop = n[0], n[0] && (i.bufferSource.loopStart = n[1], i.bufferSource.loopEnd = n[1] + n[2]), i.bufferSource.playbackRate.value = o._rate
        };
    "function" == typeof define && define.amd && define(function() {
        return {
            Howler: c,
            Howl: d
        }
    }), "undefined" != typeof exports && (exports.Howler = c, exports.Howl = d), "undefined" != typeof window && (window.Howler = c, window.Howl = d)
}(),
function e(t, o, n) {
    function r(a, s) {
        if (!o[a]) {
            if (!t[a]) {
                var l = "function" == typeof require && require;
                if (!s && l) return l(a, !0);
                if (i) return i(a, !0);
                var c = new Error("Cannot find module '" + a + "'");
                throw c.code = "MODULE_NOT_FOUND", c
            }
            var d = o[a] = {
                exports: {}
            };
            t[a][0].call(d.exports, function(e) {
                var o = t[a][1][e];
                return r(o ? o : e)
            }, d, d.exports, e, t, o, n)
        }
        return o[a].exports
    }
    for (var i = "function" == typeof require && require, a = 0; a < n.length; a++) r(n[a]);
    return r
}({
    1: [function(e, t, o) {
        "use strict";

        function n(e) {
            e.fn.perfectScrollbar = function(t) {
                return this.each(function() {
                    if ("object" == typeof t || "undefined" == typeof t) {
                        var o = t;
                        i.get(this) || r.initialize(this, o)
                    } else {
                        var n = t;
                        "update" === n ? r.update(this) : "destroy" === n && r.destroy(this)
                    }
                    return e(this)
                })
            }
        }
        var r = e("../main"),
            i = e("../plugin/instances");
        if ("function" == typeof define && define.amd) define(["jquery"], n);
        else {
            var a = window.jQuery ? window.jQuery : window.$;
            "undefined" != typeof a && n(a)
        }
        t.exports = n
    }, {
        "../main": 7,
        "../plugin/instances": 18
    }],
    2: [function(e, t, o) {
        "use strict";

        function n(e, t) {
            var o = e.className.split(" ");
            o.indexOf(t) < 0 && o.push(t), e.className = o.join(" ")
        }

        function r(e, t) {
            var o = e.className.split(" "),
                n = o.indexOf(t);
            n >= 0 && o.splice(n, 1), e.className = o.join(" ")
        }
        o.add = function(e, t) {
            e.classList ? e.classList.add(t) : n(e, t)
        }, o.remove = function(e, t) {
            e.classList ? e.classList.remove(t) : r(e, t)
        }, o.list = function(e) {
            return e.classList ? e.classList : e.className.split(" ")
        }
    }, {}],
    3: [function(e, t, o) {
        "use strict";

        function n(e, t) {
            return window.getComputedStyle(e)[t]
        }

        function r(e, t, o) {
            return "number" == typeof o && (o = o.toString() + "px"), e.style[t] = o, e
        }

        function i(e, t) {
            for (var o in t) {
                var n = t[o];
                "number" == typeof n && (n = n.toString() + "px"), e.style[o] = n
            }
            return e
        }
        o.e = function(e, t) {
            var o = document.createElement(e);
            return o.className = t, o
        }, o.appendTo = function(e, t) {
            return t.appendChild(e), e
        }, o.css = function(e, t, o) {
            return "object" == typeof t ? i(e, t) : "undefined" == typeof o ? n(e, t) : r(e, t, o)
        }, o.matches = function(e, t) {
            return "undefined" != typeof e.matches ? e.matches(t) : "undefined" != typeof e.matchesSelector ? e.matchesSelector(t) : "undefined" != typeof e.webkitMatchesSelector ? e.webkitMatchesSelector(t) : "undefined" != typeof e.mozMatchesSelector ? e.mozMatchesSelector(t) : "undefined" != typeof e.msMatchesSelector ? e.msMatchesSelector(t) : void 0
        }, o.remove = function(e) {
            "undefined" != typeof e.remove ? e.remove() : e.parentNode && e.parentNode.removeChild(e)
        }
    }, {}],
    4: [function(e, t, o) {
        "use strict";
        var n = function(e) {
            this.element = e, this.events = {}
        };
        n.prototype.bind = function(e, t) {
            "undefined" == typeof this.events[e] && (this.events[e] = []), this.events[e].push(t), this.element.addEventListener(e, t, !1)
        }, n.prototype.unbind = function(e, t) {
            var o = "undefined" != typeof t;
            this.events[e] = this.events[e].filter(function(n) {
                return o && n !== t ? !0 : (this.element.removeEventListener(e, n, !1), !1)
            }, this)
        }, n.prototype.unbindAll = function() {
            for (var e in this.events) this.unbind(e)
        };
        var r = function() {
            this.eventElements = []
        };
        r.prototype.eventElement = function(e) {
            var t = this.eventElements.filter(function(t) {
                return t.element === e
            })[0];
            return "undefined" == typeof t && (t = new n(e), this.eventElements.push(t)), t
        }, r.prototype.bind = function(e, t, o) {
            this.eventElement(e).bind(t, o)
        }, r.prototype.unbind = function(e, t, o) {
            this.eventElement(e).unbind(t, o)
        }, r.prototype.unbindAll = function() {
            for (var e = 0; e < this.eventElements.length; e++) this.eventElements[e].unbindAll()
        }, r.prototype.once = function(e, t, o) {
            var n = this.eventElement(e),
                r = function(e) {
                    n.unbind(t, r), o(e)
                };
            n.bind(t, r)
        }, t.exports = r
    }, {}],
    5: [function(e, t, o) {
        "use strict";
        t.exports = function() {
            function e() {
                return Math.floor(65536 * (1 + Math.random())).toString(16).substring(1)
            }
            return function() {
                return e() + e() + "-" + e() + "-" + e() + "-" + e() + "-" + e() + e() + e()
            }
        }()
    }, {}],
    6: [function(e, t, o) {
        "use strict";
        var n = e("./class"),
            r = e("./dom");
        o.toInt = function(e) {
            return "string" == typeof e ? parseInt(e, 10) : ~~e
        }, o.clone = function(e) {
            if (null === e) return null;
            if ("object" == typeof e) {
                var t = {};
                for (var o in e) t[o] = this.clone(e[o]);
                return t
            }
            return e
        }, o.extend = function(e, t) {
            var o = this.clone(e);
            for (var n in t) o[n] = this.clone(t[n]);
            return o
        }, o.isEditable = function(e) {
            return r.matches(e, "input,[contenteditable]") || r.matches(e, "select,[contenteditable]") || r.matches(e, "textarea,[contenteditable]") || r.matches(e, "button,[contenteditable]")
        }, o.removePsClasses = function(e) {
            for (var t = n.list(e), o = 0; o < t.length; o++) {
                var r = t[o];
                0 === r.indexOf("ps-") && n.remove(e, r)
            }
        }, o.outerWidth = function(e) {
            return this.toInt(r.css(e, "width")) + this.toInt(r.css(e, "paddingLeft")) + this.toInt(r.css(e, "paddingRight")) + this.toInt(r.css(e, "borderLeftWidth")) + this.toInt(r.css(e, "borderRightWidth"))
        }, o.startScrolling = function(e, t) {
            n.add(e, "ps-in-scrolling"), "undefined" != typeof t ? n.add(e, "ps-" + t) : (n.add(e, "ps-x"), n.add(e, "ps-y"))
        }, o.stopScrolling = function(e, t) {
            n.remove(e, "ps-in-scrolling"), "undefined" != typeof t ? n.remove(e, "ps-" + t) : (n.remove(e, "ps-x"), n.remove(e, "ps-y"))
        }, o.env = {
            isWebKit: "WebkitAppearance" in document.documentElement.style,
            supportsTouch: "ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch,
            supportsIePointer: null !== window.navigator.msMaxTouchPoints
        }
    }, {
        "./class": 2,
        "./dom": 3
    }],
    7: [function(e, t, o) {
        "use strict";
        var n = e("./plugin/destroy"),
            r = e("./plugin/initialize"),
            i = e("./plugin/update");
        t.exports = {
            initialize: r,
            update: i,
            destroy: n
        }
    }, {
        "./plugin/destroy": 9,
        "./plugin/initialize": 17,
        "./plugin/update": 20
    }],
    8: [function(e, t, o) {
        "use strict";
        t.exports = {
            wheelSpeed: 1,
            wheelPropagation: !1,
            swipePropagation: !0,
            minScrollbarLength: null,
            maxScrollbarLength: null,
            useBothWheelAxes: !1,
            useKeyboard: !0,
            suppressScrollX: !1,
            suppressScrollY: !1,
            scrollXMarginOffset: 0,
            scrollYMarginOffset: 0
        }
    }, {}],
    9: [function(e, t, o) {
        "use strict";
        var n = e("../lib/dom"),
            r = e("../lib/helper"),
            i = e("./instances");
        t.exports = function(e) {
            var t = i.get(e);
            t.event.unbindAll(), n.remove(t.scrollbarX), n.remove(t.scrollbarY), n.remove(t.scrollbarXRail), n.remove(t.scrollbarYRail), r.removePsClasses(e), i.remove(e)
        }
    }, {
        "../lib/dom": 3,
        "../lib/helper": 6,
        "./instances": 18
    }],
    10: [function(e, t, o) {
        "use strict";

        function n(e, t) {
            function o(e) {
                return e.getBoundingClientRect()
            }
            var n = window.Event.prototype.stopPropagation.bind;
            t.event.bind(t.scrollbarY, "click", n), t.event.bind(t.scrollbarYRail, "click", function(n) {
                var i = r.toInt(t.scrollbarYHeight / 2),
                    s = n.pageY - o(t.scrollbarYRail).top - i,
                    l = t.containerHeight - t.scrollbarYHeight,
                    c = s / l;
                0 > c ? c = 0 : c > 1 && (c = 1), e.scrollTop = (t.contentHeight - t.containerHeight) * c, a(e)
            }), t.event.bind(t.scrollbarX, "click", n), t.event.bind(t.scrollbarXRail, "click", function(n) {
                var i = r.toInt(t.scrollbarXWidth / 2),
                    s = n.pageX - o(t.scrollbarXRail).left - i;
                var l = t.containerWidth - t.scrollbarXWidth,
                    c = s / l;
                0 > c ? c = 0 : c > 1 && (c = 1), e.scrollLeft = (t.contentWidth - t.containerWidth) * c, a(e)
            })
        }
        var r = e("../../lib/helper"),
            i = e("../instances"),
            a = e("../update-geometry");
        t.exports = function(e) {
            var t = i.get(e);
            n(e, t)
        }
    }, {
        "../../lib/helper": 6,
        "../instances": 18,
        "../update-geometry": 19
    }],
    11: [function(e, t, o) {
        "use strict";

        function n(e, t) {
            function o(o) {
                var r = n + o,
                    i = t.containerWidth - t.scrollbarXWidth;
                t.scrollbarXLeft = 0 > r ? 0 : r > i ? i : r;
                var s = a.toInt(t.scrollbarXLeft * (t.contentWidth - t.containerWidth) / (t.containerWidth - t.scrollbarXWidth));
                e.scrollLeft = s
            }
            var n = null,
                r = null,
                s = function(t) {
                    o(t.pageX - r), l(e), t.stopPropagation(), t.preventDefault()
                },
                c = function() {
                    a.stopScrolling(e, "x"), t.event.unbind(t.ownerDocument, "mousemove", s)
                };
            t.event.bind(t.scrollbarX, "mousedown", function(o) {
                r = o.pageX, n = a.toInt(i.css(t.scrollbarX, "left")), a.startScrolling(e, "x"), t.event.bind(t.ownerDocument, "mousemove", s), t.event.once(t.ownerDocument, "mouseup", c), o.stopPropagation(), o.preventDefault()
            })
        }

        function r(e, t) {
            function o(o) {
                var r = n + o,
                    i = t.containerHeight - t.scrollbarYHeight;
                t.scrollbarYTop = 0 > r ? 0 : r > i ? i : r;
                var s = a.toInt(t.scrollbarYTop * (t.contentHeight - t.containerHeight) / (t.containerHeight - t.scrollbarYHeight));
                e.scrollTop = s
            }
            var n = null,
                r = null,
                s = function(t) {
                    o(t.pageY - r), l(e), t.stopPropagation(), t.preventDefault()
                },
                c = function() {
                    a.stopScrolling(e, "y"), t.event.unbind(t.ownerDocument, "mousemove", s)
                };
            t.event.bind(t.scrollbarY, "mousedown", function(o) {
                r = o.pageY, n = a.toInt(i.css(t.scrollbarY, "top")), a.startScrolling(e, "y"), t.event.bind(t.ownerDocument, "mousemove", s), t.event.once(t.ownerDocument, "mouseup", c), o.stopPropagation(), o.preventDefault()
            })
        }
        var i = e("../../lib/dom"),
            a = e("../../lib/helper"),
            s = e("../instances"),
            l = e("../update-geometry");
        t.exports = function(e) {
            var t = s.get(e);
            n(e, t), r(e, t)
        }
    }, {
        "../../lib/dom": 3,
        "../../lib/helper": 6,
        "../instances": 18,
        "../update-geometry": 19
    }],
    12: [function(e, t, o) {
        "use strict";

        function n(e, t) {
            function o(o, n) {
                var r = e.scrollTop;
                if (0 === o) {
                    if (!t.scrollbarYActive) return !1;
                    if (0 === r && n > 0 || r >= t.contentHeight - t.containerHeight && 0 > n) return !t.settings.wheelPropagation
                }
                var i = e.scrollLeft;
                if (0 === n) {
                    if (!t.scrollbarXActive) return !1;
                    if (0 === i && 0 > o || i >= t.contentWidth - t.containerWidth && o > 0) return !t.settings.wheelPropagation
                }
                return !0
            }
            var n = !1;
            t.event.bind(e, "mouseenter", function() {
                n = !0
            }), t.event.bind(e, "mouseleave", function() {
                n = !1
            });
            var i = !1;
            t.event.bind(t.ownerDocument, "keydown", function(s) {
                if ((!s.isDefaultPrevented || !s.isDefaultPrevented()) && n) {
                    var l = document.activeElement ? document.activeElement : t.ownerDocument.activeElement;
                    if (l) {
                        for (; l.shadowRoot;) l = l.shadowRoot.activeElement;
                        if (r.isEditable(l)) return
                    }
                    var c = 0,
                        d = 0;
                    switch (s.which) {
                        case 37:
                            c = -30;
                            break;
                        case 38:
                            d = 30;
                            break;
                        case 39:
                            c = 30;
                            break;
                        case 40:
                            d = -30;
                            break;
                        case 33:
                            d = 90;
                            break;
                        case 32:
                        case 34:
                            d = -90;
                            break;
                        case 35:
                            d = s.ctrlKey ? -t.contentHeight : -t.containerHeight;
                            break;
                        case 36:
                            d = s.ctrlKey ? e.scrollTop : t.containerHeight;
                            break;
                        default:
                            return
                    }
                    e.scrollTop = e.scrollTop - d, e.scrollLeft = e.scrollLeft + c, a(e), i = o(c, d), i && s.preventDefault()
                }
            })
        }
        var r = e("../../lib/helper"),
            i = e("../instances"),
            a = e("../update-geometry");
        t.exports = function(e) {
            var t = i.get(e);
            n(e, t)
        }
    }, {
        "../../lib/helper": 6,
        "../instances": 18,
        "../update-geometry": 19
    }],
    13: [function(e, t, o) {
        "use strict";

        function n(e, t) {
            function o(o, n) {
                var r = e.scrollTop;
                if (0 === o) {
                    if (!t.scrollbarYActive) return !1;
                    if (0 === r && n > 0 || r >= t.contentHeight - t.containerHeight && 0 > n) return !t.settings.wheelPropagation
                }
                var i = e.scrollLeft;
                if (0 === n) {
                    if (!t.scrollbarXActive) return !1;
                    if (0 === i && 0 > o || i >= t.contentWidth - t.containerWidth && o > 0) return !t.settings.wheelPropagation
                }
                return !0
            }

            function n(e) {
                var t = e.deltaX,
                    o = -1 * e.deltaY;
                return ("undefined" == typeof t || "undefined" == typeof o) && (t = -1 * e.wheelDeltaX / 6, o = e.wheelDeltaY / 6), e.deltaMode && 1 === e.deltaMode && (t *= 10, o *= 10), t !== t && o !== o && (t = 0, o = e.wheelDelta), [t, o]
            }

            function i(t, o) {
                var n = e.querySelector("textarea:hover");
                if (n) {
                    var r = n.scrollHeight - n.clientHeight;
                    if (r > 0 && !(0 === n.scrollTop && o > 0 || n.scrollTop === r && 0 > o)) return !0;
                    var i = n.scrollLeft - n.clientWidth;
                    if (i > 0 && !(0 === n.scrollLeft && 0 > t || n.scrollLeft === i && t > 0)) return !0
                }
                return !1
            }

            function s(s) {
                if (r.env.isWebKit || !e.querySelector("select:focus")) {
                    var c = n(s),
                        d = c[0],
                        u = c[1];
                    i(d, u) || (l = !1, t.settings.useBothWheelAxes ? t.scrollbarYActive && !t.scrollbarXActive ? (e.scrollTop = u ? e.scrollTop - u * t.settings.wheelSpeed : e.scrollTop + d * t.settings.wheelSpeed, l = !0) : t.scrollbarXActive && !t.scrollbarYActive && (e.scrollLeft = d ? e.scrollLeft + d * t.settings.wheelSpeed : e.scrollLeft - u * t.settings.wheelSpeed, l = !0) : (e.scrollTop = e.scrollTop - u * t.settings.wheelSpeed, e.scrollLeft = e.scrollLeft + d * t.settings.wheelSpeed), a(e), l = l || o(d, u), l && (s.stopPropagation(), s.preventDefault()))
                }
            }
            var l = !1;
            "undefined" != typeof window.onwheel ? t.event.bind(e, "wheel", s) : "undefined" != typeof window.onmousewheel && t.event.bind(e, "mousewheel", s)
        }
        var r = e("../../lib/helper"),
            i = e("../instances"),
            a = e("../update-geometry");
        t.exports = function(e) {
            var t = i.get(e);
            n(e, t)
        }
    }, {
        "../../lib/helper": 6,
        "../instances": 18,
        "../update-geometry": 19
    }],
    14: [function(e, t, o) {
        "use strict";

        function n(e, t) {
            t.event.bind(e, "scroll", function() {
                i(e)
            })
        }
        var r = e("../instances"),
            i = e("../update-geometry");
        t.exports = function(e) {
            var t = r.get(e);
            n(e, t)
        }
    }, {
        "../instances": 18,
        "../update-geometry": 19
    }],
    15: [function(e, t, o) {
        "use strict";

        function n(e, t) {
            function o() {
                var e = window.getSelection ? window.getSelection() : document.getSelection ? document.getSelection() : "";
                return 0 === e.toString().length ? null : e.getRangeAt(0).commonAncestorContainer
            }

            function n() {
                l || (l = setInterval(function() {
                    return i.get(e) ? (e.scrollTop = e.scrollTop + c.top, e.scrollLeft = e.scrollLeft + c.left, void a(e)) : void clearInterval(l)
                }, 50))
            }

            function s() {
                l && (clearInterval(l), l = null), r.stopScrolling(e)
            }
            var l = null,
                c = {
                    top: 0,
                    left: 0
                },
                d = !1;
            t.event.bind(t.ownerDocument, "selectionchange", function() {
                e.contains(o()) ? d = !0 : (d = !1, s())
            }), t.event.bind(window, "mouseup", function() {
                d && (d = !1, s())
            }), t.event.bind(window, "mousemove", function(t) {
                if (d) {
                    var o = {
                            x: t.pageX,
                            y: t.pageY
                        },
                        i = {
                            left: e.offsetLeft,
                            right: e.offsetLeft + e.offsetWidth,
                            top: e.offsetTop,
                            bottom: e.offsetTop + e.offsetHeight
                        };
                    o.x < i.left + 3 ? (c.left = -5, r.startScrolling(e, "x")) : o.x > i.right - 3 ? (c.left = 5, r.startScrolling(e, "x")) : c.left = 0, o.y < i.top + 3 ? (c.top = i.top + 3 - o.y < 5 ? -5 : -20, r.startScrolling(e, "y")) : o.y > i.bottom - 3 ? (c.top = o.y - i.bottom + 3 < 5 ? 5 : 20, r.startScrolling(e, "y")) : c.top = 0, 0 === c.top && 0 === c.left ? s() : n()
                }
            })
        }
        var r = e("../../lib/helper"),
            i = e("../instances"),
            a = e("../update-geometry");
        t.exports = function(e) {
            var t = i.get(e);
            n(e, t)
        }
    }, {
        "../../lib/helper": 6,
        "../instances": 18,
        "../update-geometry": 19
    }],
    16: [function(e, t, o) {
        "use strict";

        function n(e, t, o, n) {
            function a(o, n) {
                var r = e.scrollTop,
                    i = e.scrollLeft,
                    a = Math.abs(o),
                    s = Math.abs(n);
                if (s > a) {
                    if (0 > n && r === t.contentHeight - t.containerHeight || n > 0 && 0 === r) return !t.settings.swipePropagation
                } else if (a > s && (0 > o && i === t.contentWidth - t.containerWidth || o > 0 && 0 === i)) return !t.settings.swipePropagation;
                return !0
            }

            function s(t, o) {
                e.scrollTop = e.scrollTop - o, e.scrollLeft = e.scrollLeft - t, i(e)
            }

            function l() {
                y = !0
            }

            function c() {
                y = !1
            }

            function d(e) {
                return e.targetTouches ? e.targetTouches[0] : e
            }

            function u(e) {
                return e.targetTouches && 1 === e.targetTouches.length ? !0 : e.pointerType && "mouse" !== e.pointerType && e.pointerType !== e.MSPOINTER_TYPE_MOUSE ? !0 : !1
            }

            function p(e) {
                if (u(e)) {
                    w = !0;
                    var t = d(e);
                    g.pageX = t.pageX, g.pageY = t.pageY, v = (new Date).getTime(), null !== b && clearInterval(b), e.stopPropagation()
                }
            }

            function f(e) {
                if (!y && w && u(e)) {
                    var t = d(e),
                        o = {
                            pageX: t.pageX,
                            pageY: t.pageY
                        },
                        n = o.pageX - g.pageX,
                        r = o.pageY - g.pageY;
                    s(n, r), g = o;
                    var i = (new Date).getTime(),
                        l = i - v;
                    l > 0 && (m.x = n / l, m.y = r / l, v = i), a(n, r) && (e.stopPropagation(), e.preventDefault())
                }
            }

            function h() {
                !y && w && (w = !1, clearInterval(b), b = setInterval(function() {
                    return r.get(e) ? Math.abs(m.x) < .01 && Math.abs(m.y) < .01 ? void clearInterval(b) : (s(30 * m.x, 30 * m.y), m.x *= .8, void(m.y *= .8)) : void clearInterval(b)
                }, 10))
            }
            var g = {},
                v = 0,
                m = {},
                b = null,
                y = !1,
                w = !1;
            o && (t.event.bind(window, "touchstart", l), t.event.bind(window, "touchend", c), t.event.bind(e, "touchstart", p), t.event.bind(e, "touchmove", f), t.event.bind(e, "touchend", h)), n && (window.PointerEvent ? (t.event.bind(window, "pointerdown", l), t.event.bind(window, "pointerup", c), t.event.bind(e, "pointerdown", p), t.event.bind(e, "pointermove", f), t.event.bind(e, "pointerup", h)) : window.MSPointerEvent && (t.event.bind(window, "MSPointerDown", l), t.event.bind(window, "MSPointerUp", c), t.event.bind(e, "MSPointerDown", p), t.event.bind(e, "MSPointerMove", f), t.event.bind(e, "MSPointerUp", h)))
        }
        var r = e("../instances"),
            i = e("../update-geometry");
        t.exports = function(e, t, o) {
            var i = r.get(e);
            n(e, i, t, o)
        }
    }, {
        "../instances": 18,
        "../update-geometry": 19
    }],
    17: [function(e, t, o) {
        "use strict";
        var n = e("../lib/class"),
            r = e("../lib/helper"),
            i = e("./instances"),
            a = e("./update-geometry"),
            s = e("./handler/click-rail"),
            l = e("./handler/drag-scrollbar"),
            c = e("./handler/keyboard"),
            d = e("./handler/mouse-wheel"),
            u = e("./handler/native-scroll"),
            p = e("./handler/selection"),
            f = e("./handler/touch");
        t.exports = function(e, t) {
            t = "object" == typeof t ? t : {}, n.add(e, "ps-container");
            var o = i.add(e);
            o.settings = r.extend(o.settings, t), s(e), l(e), d(e), u(e), p(e), (r.env.supportsTouch || r.env.supportsIePointer) && f(e, r.env.supportsTouch, r.env.supportsIePointer), o.settings.useKeyboard && c(e), a(e)
        }
    }, {
        "../lib/class": 2,
        "../lib/helper": 6,
        "./handler/click-rail": 10,
        "./handler/drag-scrollbar": 11,
        "./handler/keyboard": 12,
        "./handler/mouse-wheel": 13,
        "./handler/native-scroll": 14,
        "./handler/selection": 15,
        "./handler/touch": 16,
        "./instances": 18,
        "./update-geometry": 19
    }],
    18: [function(e, t, o) {
        "use strict";

        function n(e) {
            var t = this;
            t.settings = u.clone(l), t.containerWidth = null, t.containerHeight = null, t.contentWidth = null, t.contentHeight = null, t.isRtl = "rtl" === s.css(e, "direction"), t.event = new c, t.ownerDocument = e.ownerDocument || document, t.scrollbarXRail = s.appendTo(s.e("div", "ps-scrollbar-x-rail"), e), t.scrollbarX = s.appendTo(s.e("div", "ps-scrollbar-x"), t.scrollbarXRail), t.scrollbarXActive = null, t.scrollbarXWidth = null, t.scrollbarXLeft = null, t.scrollbarXBottom = u.toInt(s.css(t.scrollbarXRail, "bottom")), t.isScrollbarXUsingBottom = t.scrollbarXBottom === t.scrollbarXBottom, t.scrollbarXTop = t.isScrollbarXUsingBottom ? null : u.toInt(s.css(t.scrollbarXRail, "top")), t.railBorderXWidth = u.toInt(s.css(t.scrollbarXRail, "borderLeftWidth")) + u.toInt(s.css(t.scrollbarXRail, "borderRightWidth")), t.railXMarginWidth = u.toInt(s.css(t.scrollbarXRail, "marginLeft")) + u.toInt(s.css(t.scrollbarXRail, "marginRight")), t.railXWidth = null, t.scrollbarYRail = s.appendTo(s.e("div", "ps-scrollbar-y-rail"), e), t.scrollbarY = s.appendTo(s.e("div", "ps-scrollbar-y"), t.scrollbarYRail), t.scrollbarYActive = null, t.scrollbarYHeight = null, t.scrollbarYTop = null, t.scrollbarYRight = u.toInt(s.css(t.scrollbarYRail, "right")), t.isScrollbarYUsingRight = t.scrollbarYRight === t.scrollbarYRight, t.scrollbarYLeft = t.isScrollbarYUsingRight ? null : u.toInt(s.css(t.scrollbarYRail, "left")), t.scrollbarYOuterWidth = t.isRtl ? u.outerWidth(t.scrollbarY) : null, t.railBorderYWidth = u.toInt(s.css(t.scrollbarYRail, "borderTopWidth")) + u.toInt(s.css(t.scrollbarYRail, "borderBottomWidth")), t.railYMarginHeight = u.toInt(s.css(t.scrollbarYRail, "marginTop")) + u.toInt(s.css(t.scrollbarYRail, "marginBottom")), t.railYHeight = null
        }

        function r(e) {
            return "undefined" == typeof e.dataset ? e.getAttribute("data-ps-id") : e.dataset.psId
        }

        function i(e, t) {
            "undefined" == typeof e.dataset ? e.setAttribute("data-ps-id", t) : e.dataset.psId = t
        }

        function a(e) {
            "undefined" == typeof e.dataset ? e.removeAttribute("data-ps-id") : delete e.dataset.psId
        }
        var s = e("../lib/dom"),
            l = e("./default-setting"),
            c = e("../lib/event-manager"),
            d = e("../lib/guid"),
            u = e("../lib/helper"),
            p = {};
        o.add = function(e) {
            var t = d();
            return i(e, t), p[t] = new n(e), p[t]
        }, o.remove = function(e) {
            delete p[r(e)], a(e)
        }, o.get = function(e) {
            return p[r(e)]
        }
    }, {
        "../lib/dom": 3,
        "../lib/event-manager": 4,
        "../lib/guid": 5,
        "../lib/helper": 6,
        "./default-setting": 8
    }],
    19: [function(e, t, o) {
        "use strict";

        function n(e, t) {
            return e.settings.minScrollbarLength && (t = Math.max(t, e.settings.minScrollbarLength)), e.settings.maxScrollbarLength && (t = Math.min(t, e.settings.maxScrollbarLength)), t
        }

        function r(e, t) {
            var o = {
                width: t.railXWidth
            };
            o.left = t.isRtl ? e.scrollLeft + t.containerWidth - t.contentWidth : e.scrollLeft, t.isScrollbarXUsingBottom ? o.bottom = t.scrollbarXBottom - e.scrollTop : o.top = t.scrollbarXTop + e.scrollTop, a.css(t.scrollbarXRail, o);
            var n = {
                top: e.scrollTop,
                height: t.railYHeight
            };
            t.isScrollbarYUsingRight ? n.right = t.isRtl ? t.contentWidth - e.scrollLeft - t.scrollbarYRight - t.scrollbarYOuterWidth : t.scrollbarYRight - e.scrollLeft : n.left = t.isRtl ? e.scrollLeft + 2 * t.containerWidth - t.contentWidth - t.scrollbarYLeft - t.scrollbarYOuterWidth : t.scrollbarYLeft + e.scrollLeft, a.css(t.scrollbarYRail, n), a.css(t.scrollbarX, {
                left: t.scrollbarXLeft,
                width: t.scrollbarXWidth - t.railBorderXWidth
            }), a.css(t.scrollbarY, {
                top: t.scrollbarYTop,
                height: t.scrollbarYHeight - t.railBorderYWidth
            })
        }
        var i = e("../lib/class"),
            a = e("../lib/dom"),
            s = e("../lib/helper"),
            l = e("./instances");
        t.exports = function(e) {
            var t = l.get(e);
            t.containerWidth = e.clientWidth, t.containerHeight = e.clientHeight, t.contentWidth = e.scrollWidth, t.contentHeight = e.scrollHeight, e.contains(t.scrollbarXRail) || a.appendTo(t.scrollbarXRail, e), e.contains(t.scrollbarYRail) || a.appendTo(t.scrollbarYRail, e), !t.settings.suppressScrollX && t.containerWidth + t.settings.scrollXMarginOffset < t.contentWidth ? (t.scrollbarXActive = !0, t.railXWidth = t.containerWidth - t.railXMarginWidth, t.scrollbarXWidth = n(t, s.toInt(t.railXWidth * t.containerWidth / t.contentWidth)), t.scrollbarXLeft = s.toInt(e.scrollLeft * (t.railXWidth - t.scrollbarXWidth) / (t.contentWidth - t.containerWidth))) : (t.scrollbarXActive = !1, t.scrollbarXWidth = 0, t.scrollbarXLeft = 0, e.scrollLeft = 0), !t.settings.suppressScrollY && t.containerHeight + t.settings.scrollYMarginOffset < t.contentHeight ? (t.scrollbarYActive = !0, t.railYHeight = t.containerHeight - t.railYMarginHeight, t.scrollbarYHeight = n(t, s.toInt(t.railYHeight * t.containerHeight / t.contentHeight)), t.scrollbarYTop = s.toInt(e.scrollTop * (t.railYHeight - t.scrollbarYHeight) / (t.contentHeight - t.containerHeight))) : (t.scrollbarYActive = !1, t.scrollbarYHeight = 0, t.scrollbarYTop = 0, e.scrollTop = 0), t.scrollbarXLeft >= t.railXWidth - t.scrollbarXWidth && (t.scrollbarXLeft = t.railXWidth - t.scrollbarXWidth), t.scrollbarYTop >= t.railYHeight - t.scrollbarYHeight && (t.scrollbarYTop = t.railYHeight - t.scrollbarYHeight), r(e, t), i[t.scrollbarXActive ? "add" : "remove"](e, "ps-active-x"), i[t.scrollbarYActive ? "add" : "remove"](e, "ps-active-y")
        }
    }, {
        "../lib/class": 2,
        "../lib/dom": 3,
        "../lib/helper": 6,
        "./instances": 18
    }],
    20: [function(e, t, o) {
        "use strict";
        var n = e("../lib/dom"),
            r = e("./instances"),
            i = e("./update-geometry");
        t.exports = function(e) {
            var t = r.get(e);
            n.css(t.scrollbarXRail, "display", "none"), n.css(t.scrollbarYRail, "display", "none"), i(e), n.css(t.scrollbarXRail, "display", "block"), n.css(t.scrollbarYRail, "display", "block")
        }
    }, {
        "../lib/dom": 3,
        "./instances": 18,
        "./update-geometry": 19
    }]
}, {}, [1]),
function() {
    for (var e, t = function() {}, o = ["assert", "clear", "count", "debug", "dir", "dirxml", "error", "exception", "group", "groupCollapsed", "groupEnd", "info", "log", "markTimeline", "profile", "profileEnd", "table", "time", "timeEnd", "timeStamp", "trace", "warn"], n = o.length, r = window.console = window.console || {}; n--;) e = o[n], r[e] || (r[e] = t)
}();
var $doc = $(document),
    $win = $(window),
    $body = $("body"),
    isMobile = $win.width() < 768 ? !0 : !1;
$win.on("resize", function() {
    isMobile = $win.width() < 768 ? !0 : !1
});
var state = {
        currentAct: 0,
        currentBrochurePage: 0,
        overlayType: !1,
        overlayChanged: !1,
        actsLength: $(".js-act").length,
        storyLength: $(".js-story").length,
        brochureLength: 3
    },
    $countdown = $(".js-countdown"),
    $nodeDays = $(".js-countdown-days", $countdown),
    timeDate = new Date("08/2/2015 4:00 PM"),
    second = 1e3,
    minute = 60 * second,
    hour = 60 * minute,
    day = 24 * hour,
    timer, currentDate, utc, timeLeft, days, hours, minutes, seconds;
var audio = {
    tracks: [],
    changeAudio: function(e) {
        if (!isMobile) {
            e = e > state.actsLength - 1 ? state.actsLength - 1 : e;
            var t = state.currentAct >= state.actsLength - 1 ? state.actsLength - 1 : state.currentAct;
            void 0 !== audio.tracks[e] && 0 === audio.tracks[e]._volume && (void 0 !== audio.tracks[t] && audio.tracks[t].fade(1, 0, 2e3), audio.tracks[e].fade(0, 1, 2e3))
        }
    },
    toggleAudio: function(e) {
        isMobile || (Howler._muted || e ? ($(".js-audio-icon-play").show(), $(".js-audio-icon-mute").hide(), Howler.unmute()) : ($(".js-audio-icon-play").hide(), $(".js-audio-icon-mute").show(), Howler.mute()))
    }
};
$win.load(function() {
    isMobile || (audio.tracks = [new Howl({
        urls: [assetsPath + "/audio/act-1/audio_1.ogg", assetsPath + "/audio/act-1/audio_1.mp3"],
        loop: !0,
        volume: 0.2
    }).play()/*, new Howl({
        urls: [assetsPath + "/audio/act-2/Bilgewater_web_audio_2_v2.ogg", assetsPath + "/audio/act-2/Bilgewater_web_audio_2_v2_192.mp3"],
        loop: !0,
        volume: 0
    }).play(), new Howl({
        urls: [assetsPath + "/audio/act-3/Bilgewater_web_audio_3_v2.ogg", assetsPath + "/audio/act-3/Bilgewater_web_audio_3_v2_192.mp3"],
        loop: !0,
        volume: 0
    }).play()*/])
});
var winWidth, winHeight, scaleX, scaleY, scale, sceneWidth = 2223,
    sceneHeight = 1250,
    poiID, $targetPOIPopup, popups = {
        currentPopup: void 0,
        openPopup: function(e) {
            if (!e.hasClass("is-disabled") && (poiID = e.data("target-poi"), targetPOIIDAttr = "#act-" + (state.currentAct + 1) + "-popup-" + poiID, $targetPOI = $(targetPOIIDAttr), $targetPOIPopup = $(".js-poi-popup", targetPOIIDAttr), $targetPOI.addClass("is-discovered"), this.currentPopup !== $targetPOIPopup && (this.closeCurrentPopup(), TweenMax.set($targetPOIPopup, {
                    display: "block"
                }), TweenMax.to($targetPOIPopup, ".3", {
                    autoAlpha: 1
                }), this.currentPopup = $targetPOIPopup), $("#act-" + (state.currentAct + 1) + "-marker-" + poiID).removeClass("is-disabled"), !isMobile)) {
                var t = $(targetPOIIDAttr + " button").css("left"),
                    o = $(targetPOIIDAttr + " button").css("top");

                $targetPOIPopup.css({
                    top: o,
                    left: t
                });
                var n = $targetPOIPopup.offset().left,
                    r = $targetPOIPopup.offset().top;
                30 > n && (n = parseInt(t, 10) + -1 * Math.round(n), $targetPOIPopup.css({
                    left: n + "px"
                })), 30 > r && (r = parseInt(o, 10) + -1 * Math.round(r), r = 30 > r ? 130 : r, $targetPOIPopup.css({
                    top: r + 40 + "px"
                }));
                var i = $("#world").offset().left,
                    a = $("#world").outerWidth(!0);
                a += i;
                var s = $targetPOIPopup.position().left + $targetPOIPopup.width(),
                    l = a - s;
                if (0 > l) {
                    var c = $targetPOIPopup.position().left + l;
                    $targetPOIPopup.css({
                        left: c + "px"
                    })
                }
                i = $("#world").offset().top;
                var d = $("#world").height();
                d += i;
                var u = $targetPOIPopup.position().top + $targetPOIPopup.height(),
                    p = d - u;
                if (30 > p) {
                    var f = $targetPOIPopup.position().top + p;
                    $targetPOIPopup.css({
                        top: f - 100 + "px"
                    })
                }
            }
        },
        closeCurrentPopup: function() {
            void 0 !== this.currentPopup && (TweenMax.to(this.currentPopup, ".2", {
                autoAlpha: 0
            }), TweenMax.set(this.currentPopup, {
                display: "none",
                delay: ".2"
            })), this.currentPopup = void 0
        }
    };
$(document.body).on("click touchend", ".js-open-poi", function() {
    var e = $(this);
    popups.openPopup(e)
}).on("click touchend", ".js-close-poi", function() {
    popups.closeCurrentPopup()
}), document.addEventListener("keyup", function(e) {
    27 == e.keyCode && popups.closeCurrentPopup()
}), $win.on("hashchange", function() {
    popups.closeCurrentPopup()
}), $poi = document.getElementsByClassName("js-poi");
var poi = {
    state: !0,
    toggleVisibility: function(e) {
        var t = e.data("poi-state");
        $(".js-toggle-poi").removeClass("is-active"), $('.js-toggle-poi[data-poi-state="' + t + '"]').addClass("is-active"), t ? (TweenMax.set($poi, {
            display: "block"
        }), TweenMax.to($poi, ".3", {
            autoAlpha: 1
        })) : (TweenMax.to($poi, ".3", {
            autoAlpha: 0
        }), TweenMax.set($poi, {
            display: "none",
            delay: ".3"
        })), this.state = t
    }
};
$(document.body).on("click touchend", ".js-toggle-poi", function() {
    var e = $(this);
    poi.toggleVisibility(e)
});
var $header = $(".js-header"),
    header = {
        state: !1,
        toggleState: function(e) {
            this.state = void 0 !== e ? e : !this.state, this.state ? $header.addClass("is-active") : $header.removeClass("is-active")
        }
    };
$(document.body).on("tap", ".js-toggle-header", function(e) {
    e.preventDefault(), header.toggleState()
}).on("tap", ".js-close-header", function() {
    header.toggleState(!1)
});
var currentSublist;
$(".js-toggle-sublist").on("tap", function() {
    var e = $(this),
        t = e.data("sublist");
    $(".js-sublist").slideUp(200), t !== currentSublist ? ($(this).find(".js-sublist").slideDown(200), currentSublist = t) : currentSublist = void 0
});
var $footer = $(".js-footer"),
    $toggleFooter = $(".js-toggle-footer"),
    $toggleFooterIndicator = $(".js-toggle-footer-indicator"),
    footer = {
        state: !1,
        toggleState: function(e) {
            this.state = void 0 !== e ? e : !this.state, this.state ? ($footer.addClass("is-active"), $toggleFooterIndicator.html("&minus;")) : ($footer.removeClass("is-active"), $toggleFooterIndicator.html("+"))
        }
    };
$(document.body).on("click touchend", ".js-toggle-footer", function(e) {
    e.preventDefault(), footer.toggleState()
});
var alertHeight = 0;
$win.load(function() {
    if (alertHeight = $("#riotbar-alerts").height(), TweenMax.set(document.getElementsByClassName("js-poi-popup"), {
            autoAlpha: 0,
            display: "none"
        }), TweenMax.set(document.getElementsByClassName("js-scene-controls"), {
            autoAlpha: 0,
            display: "none"
        }), TweenMax.set(document.getElementsByClassName("js-scene-controls-" + (state.currentAct + 1)), {
            autoAlpha: 1,
            display: "block"
        }), TweenMax.set(document.getElementsByClassName("js-brochure-controls"), {
            autoAlpha: 0,
            display: "none"
        }), TweenMax.set(document.getElementsByClassName("js-brochure-controls-" + (state.currentAct + 1)), {
            autoAlpha: 1,
            display: "block"
        }), $(".js-scrollbar").perfectScrollbar(), $("#alert-cookie-policy").length > 0) {
        $body.addClass("has-cookie-alert");
        var e = $(".js-alert-offset, #riotbar-bar, #riotbar-subbar");
        e.each(function() {
            riotbarOffset($(this))
        }), $(document.body).on("click", "#cookie-policy-agree", function(t) {
            t.preventDefault(), $body.removeClass("has-cookie-alert"), e.css({
                top: ""
            }), $win.trigger("resize")
        })
    }
    setSceneScale(), $win.on("resize", function() {
        setSceneScale()
    }), Modernizr.touch || $(".js-dragscrollable").dragscrollable({
        acceptPropagatedEvent: !0
    }), document.addEventListener("keyup", function(e) {
        27 == e.keyCode && (header.state ? header.toggleState(!1) : footer.state && footer.toggleState(!1))
    }), $(document.body).on("click touchend", ".js-toggle-audio", function() {
        audio.toggleAudio()
    }), setScrollPosition(), $win.resize(function() {
        setScrollPosition()
    });
    var t, o;
    $(".js-story").each(function() {
        var e = $(this),
            n = $(".js-story-scrolling-body", e),
            r = ($(".js-story-scrollbar", e), $(".js-story-scrollbar-marker", e)),
            i = 0,
            a = 0;
        $(".js-story-block", e).each(function() {
            i += $(this).height()
        }), $(".js-scroll-marker", e).each(function(n) {
            t = a / i * 100, $(this).css({
                top: t + "%"
            }).data("top", t), o = $(".js-story-block-" + (n + 1), e).height(), a += o
        });
        var s = n.height(),
            l = s / i * 50;
        r.height(l + "%");
        var c, d, u, p;
        n.on("scroll", function() {
            var t = $(".js-scroll-marker.is-active", e),
                o = !1;
            p = $(this).prop("offsetHeight"), c = n.scrollTop(), d = c > 2000 ? 2000 : c, u = 1 - d / 2000, u = .15 > u ? .15 : u, $(".js-story-fade-scroll", e).css({
                opacity: u
            });
            var i = c / (a - p / 2),
                s = 100 * i - i * l;
            $(".js-scroll-marker", e).each(function() {
                var e = $(this);
                return parseFloat(e.data("top")) <= s ? void(o = e) : !1
            }), n.scrollTop() + n.height() == n[0].scrollHeight && "function" == typeof window.ping && window.ping("#" + n.find(".c-story__body").attr("id") + "-bottom", {
                "": ""
            }), o && o.data("scroll-marker") != t.data("scroll-marker") && ("function" == typeof window.ping && window.ping(t.find("a").attr("href"), {
                "": ""
            }), t.removeClass("is-active"), o.addClass("is-active")), r.css({
                top: s + "%"
            })
        })
    }), TweenMax.set(document.getElementsByClassName("js-scene-layer"), {
        autoAlpha: 0,
        display: "none"
    });
    var n, r;
    $(".js-mask-layer").hover(function() {
        isMobile || (n = $(this).data("mask-layer"), r = $('.js-scene-layer[data-scene-layer="' + n + '"]', '.js-scene[data-scene-act="' + (state.currentAct + 1) + '"]'), TweenMax.set(r, {
            display: "block"
        }), TweenMax.to(r, ".4", {
            autoAlpha: 1
        }))
    }, function() {
        isMobile || (TweenMax.to(r, ".2", {
            autoAlpha: 0
        }), TweenMax.set(r, {
            display: "none",
            delay: ".2"
        }))
    }), $(".js-modal").fancybox({
        padding: 10,
        margin: 30,
        maxHeight: "80%",
        afterLoad: function() {
            this.title = '<h3 class="fancybox-gallery-title">' + this.title + '</h3><span class="fancybox-count">' + (this.index + 1) + " / " + this.group.length + "</span>"
        },
        helpers: {
            title: {
                type: "inside"
            }
        }
    }), updatePage(), $win.on("hashchange", function() {
        updatePage()
    })
});