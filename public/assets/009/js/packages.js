
$(function(){

    $('.whatsthis').click(function(){
        $('.ps-body').animate({
            scrollTop: ($('#hero').offset().top - 230)
        }, 300, function(){
            if(!$('#hero').parent().hasClass('expanded'))
                $('#hero').click();    
        });
        
    });

    $(".cofre").hover(function(e) {
        $(this).find('.price').fadeIn(50);
    },function(e) {
        $(this).find('.price').fadeOut(50);
    });

    $(".toggle a").hover(function(e) {
        $img = $(this).find('img');
        $img.attr('src',$img.attr('src').replace('boton.','boton-on.'));
    }, function(e) {
        $img = $(this).find('img');
        if($img.attr('id') == 'off'){
            $img.attr('src',$img.attr('src').replace('-on',''));
        }
    });

    $(".toggle a").click(function(e) {
        $img = $(this).find('img');
        if($img.attr('src').indexOf('1b') > -1) return false;
        $stack = $(this).parent().parent().find('.pack-info li');
        $stack.css({'opacity':0});

        if($img.attr('id') == 'off'){

            $img.attr('id','on');
            $img.parent().parent().parent().find('.pack-info').show();
            $stack.each(function(i,item) {
                r = 50 * i;

                $(item).delay(r).animate({
                    'opacity':1,
                    'top':"-20",
                });
            });

        } else {
            $img.attr('id','off');
            $img.parent().parent().parent().find('.pack-info').fadeOut(200,function(){
                $img.parent().parent().parent().find('.pack-info li').css({'opacity':0,'top':0});    
            });
        }

        return false;

    });

    $("h5").click(function(e) {
        $(this).closest(".faq-item").toggleClass("expanded").siblings().removeClass("expanded");
        $('ps-body').perfectScrollbar('update');
    }), $(document).find(".faq-item:first").addClass("expanded");

    $('.ps-container').scroll(function() {
        var e, t, n;

        return n = $(this).scrollTop(), $(".dscroll").length ? (t = 0, e = 0, $(".dscroll:visible:not(.dscroll-in)").each(function() {
            var r, i, o;
            return i = $(this), o = i.offset().top, n + $('.ps-container').height() > o + 100 ? (i.addClass("dscroll-in"), o === t ? (e++, r = .2 * e, i.css({
                "-webkit-animation-delay": r + "s",
                "animation-delay": r + "s"
            })) : e = 0, t = o) : void 0
        })) : void 0
    });

});
