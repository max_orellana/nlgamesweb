var forms = {
}

$(document).ready(function(){
  $('.btn-hover').hover(
    function(){
      if( ! $(this).data('out')) $(this).data('out',$(this).attr('src'));
      $(this).attr('src',$(this).data('over'));
    },
    function(){
      $(this).attr('src',$(this).data('out'));
    }
  );

  $('a[href*=".jpg"], a[href*="jpeg"], a[href*=".png"], a[href*=".bmp"]').addClass("lightview").attr("data-lightview-group", "gallery").attr("data-lightview-group-options", "controls: 'thumbnails'");
  
  $('img#wpstats').remove();
  
  $('.active-canvas').click(function(){
      $(this).toggleClass('fi-indent-more').toggleClass('fi-indent-less').toggleClass('active-canvas-on');
      $('.all-content').toggleClass('on-canvas');
  });
});