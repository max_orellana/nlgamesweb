function promoCountdown(e) {
    e.each(function(e) {
        var t = new Date(1e3 * e.dataset.psmEndDate),
            n = function() {
                var n = new Date;
                if (t > n) {
                    var s = (t.getTime() - n.getTime()) / 1e3,
                        r = parseInt(s / 3600, 10),
                        o = parseInt(s / 60 - 60 * r, 10),
                        a = parseInt(s % 60, 10);
                    r = 10 > r ? "0" + r : r, o = 10 > o ? "0" + o : o, a = 10 > a ? "0" + a : a;
                    var l = r + ":" + o + ":" + a;
                    e.set("html", l)
                } else clearInterval(i)
            };
        n();
        var i = window.setInterval(n, 1e3)
    })
}
var requirejs, require, define;
! function(ba) {
    function G(e) {
        return "[object Function]" === K.call(e)
    }

    function H(e) {
        return "[object Array]" === K.call(e)
    }

    function v(e, t) {
        if (e) {
            var n;
            for (n = 0; n < e.length && (!e[n] || !t(e[n], n, e)); n += 1);
        }
    }

    function T(e, t) {
        if (e) {
            var n;
            for (n = e.length - 1; n > -1 && (!e[n] || !t(e[n], n, e)); n -= 1);
        }
    }

    function t(e, t) {
        return fa.call(e, t)
    }

    function m(e, n) {
        return t(e, n) && e[n]
    }

    function B(e, n) {
        for (var i in e)
            if (t(e, i) && n(e[i], i)) break
    }

    function U(e, n, i, s) {
        return n && B(n, function(n, r) {
            (i || !t(e, r)) && (!s || "object" != typeof n || !n || H(n) || G(n) || n instanceof RegExp ? e[r] = n : (e[r] || (e[r] = {}), U(e[r], n, i, s)))
        }), e
    }

    function u(e, t) {
        return function() {
            return t.apply(e, arguments)
        }
    }

    function ca(e) {
        throw e
    }

    function da(e) {
        if (!e) return e;
        var t = ba;
        return v(e.split("."), function(e) {
            t = t[e]
        }), t
    }

    function C(e, t, n, i) {
        return t = Error(t + "\nhttp://requirejs.org/docs/errors.html#" + e), t.requireType = e, t.requireModules = i, n && (t.originalError = n), t
    }

    function ga(e) {
        function n(e, t, n) {
            var i, s, r, o, a, l, c, u, t = t && t.split("/"),
                d = j.map,
                h = d && d["*"];
            if (e) {
                for (e = e.split("/"), s = e.length - 1, j.nodeIdCompat && Q.test(e[s]) && (e[s] = e[s].replace(Q, "")), "." === e[0].charAt(0) && t && (s = t.slice(0, t.length - 1), e = s.concat(e)), s = e, r = 0; r < s.length; r++) o = s[r], "." === o ? (s.splice(r, 1), r -= 1) : ".." === o && 0 !== r && (1 != r || ".." !== s[2]) && ".." !== s[r - 1] && r > 0 && (s.splice(r - 1, 2), r -= 2);
                e = e.join("/")
            }
            if (n && d && (t || h)) {
                s = e.split("/"), r = s.length;
                e: for (; r > 0; r -= 1) {
                    if (a = s.slice(0, r).join("/"), t)
                        for (o = t.length; o > 0; o -= 1)
                            if ((n = m(d, t.slice(0, o).join("/"))) && (n = m(n, a))) {
                                i = n, l = r;
                                break e
                            }!c && h && m(h, a) && (c = m(h, a), u = r)
                }!i && c && (i = c, l = u), i && (s.splice(0, l, i), e = s.join("/"))
            }
            return (i = m(j.pkgs, e)) ? i : e
        }

        function i(e) {
            z && v(document.getElementsByTagName("script"), function(t) {
                return t.getAttribute("data-requiremodule") === e && t.getAttribute("data-requirecontext") === E.contextName ? (t.parentNode.removeChild(t), !0) : void 0
            })
        }

        function s(e) {
            var t = m(j.paths, e);
            return t && H(t) && 1 < t.length ? (t.shift(), E.require.undef(e), E.makeRequire(null, {
                skipMap: !0
            })([e]), !0) : void 0
        }

        function r(e) {
            var t, n = e ? e.indexOf("!") : -1;
            return n > -1 && (t = e.substring(0, n), e = e.substring(n + 1, e.length)), [t, e]
        }

        function o(e, t, i, s) {
            var o, a, l = null,
                c = t ? t.name : null,
                u = e,
                d = !0,
                h = "";
            return e || (d = !1, e = "_@r" + (I += 1)), e = r(e), l = e[0], e = e[1], l && (l = n(l, c, s), a = m(D, l)), e && (l ? h = a && a.normalize ? a.normalize(e, function(e) {
                return n(e, c, s)
            }) : -1 === e.indexOf("!") ? n(e, c, s) : e : (h = n(e, c, s), e = r(h), l = e[0], h = e[1], i = !0, o = E.nameToUrl(h))), i = !l || a || i ? "" : "_unnormalized" + (q += 1), {
                prefix: l,
                name: h,
                parentMap: t,
                unnormalized: !!i,
                url: o,
                originalName: u,
                isDefine: d,
                id: (l ? l + "!" + h : h) + i
            }
        }

        function a(e) {
            var t = e.id,
                n = m(P, t);
            return n || (n = P[t] = new E.Module(e)), n
        }

        function l(e, n, i) {
            var s = e.id,
                r = m(P, s);
            !t(D, s) || r && !r.defineEmitComplete ? (r = a(e), r.error && "error" === n ? i(r.error) : r.on(n, i)) : "defined" === n && i(D[s])
        }

        function c(e, t) {
            var n = e.requireModules,
                i = !1;
            t ? t(e) : (v(n, function(t) {
                (t = m(P, t)) && (t.error = e, t.events.error && (i = !0, t.emit("error", e)))
            }), i || g.onError(e))
        }

        function d() {
            R.length && (ha.apply(A, [A.length, 0].concat(R)), R = [])
        }

        function h(e) {
            delete P[e], delete O[e]
        }

        function f(e, t, n) {
            var i = e.map.id;
            e.error ? e.emit("error", e.error) : (t[i] = !0, v(e.depMaps, function(i, s) {
                var r = i.id,
                    o = m(P, r);
                o && !e.depMatched[s] && !n[r] && (m(t, r) ? (e.defineDep(s, D[r]), e.check()) : f(o, t, n))
            }), n[i] = !0)
        }

        function p() {
            var e, t, n = (e = 1e3 * j.waitSeconds) && E.startTime + e < (new Date).getTime(),
                r = [],
                o = [],
                a = !1,
                l = !0;
            if (!_) {
                if (_ = !0, B(O, function(e) {
                        var c = e.map,
                            u = c.id;
                        if (e.enabled && (c.isDefine || o.push(e), !e.error))
                            if (!e.inited && n) s(u) ? a = t = !0 : (r.push(u), i(u));
                            else if (!e.inited && e.fetched && c.isDefine && (a = !0, !c.prefix)) return l = !1
                    }), n && r.length) return e = C("timeout", "Load timeout for modules: " + r, null, r), e.contextName = E.contextName, c(e);
                l && v(o, function(e) {
                    f(e, {}, {})
                }), n && !t || !a || !z && !ea || k || (k = setTimeout(function() {
                    k = 0, p()
                }, 50)), _ = !1
            }
        }

        function y(e) {
            t(D, e[0]) || a(o(e[0], null, !0)).init(e[1], e[2])
        }

        function w(e) {
            var e = e.currentTarget || e.srcElement,
                t = E.onScriptLoad;
            return e.detachEvent && !Y ? e.detachEvent("onreadystatechange", t) : e.removeEventListener("load", t, !1), t = E.onScriptError, (!e.detachEvent || Y) && e.removeEventListener("error", t, !1), {
                node: e,
                id: e && e.getAttribute("data-requiremodule")
            }
        }

        function b() {
            var e;
            for (d(); A.length;) {
                if (e = A.shift(), null === e[0]) return c(C("mismatch", "Mismatched anonymous define() module: " + e[e.length - 1]));
                y(e)
            }
        }
        var _, x, E, S, k, j = {
                waitSeconds: 7,
                baseUrl: "./",
                paths: {},
                bundles: {},
                pkgs: {},
                shim: {},
                config: {}
            },
            P = {},
            O = {},
            $ = {},
            A = [],
            D = {},
            L = {},
            F = {},
            I = 1,
            q = 1;
        return S = {
            require: function(e) {
                return e.require ? e.require : e.require = E.makeRequire(e.map)
            },
            exports: function(e) {
                return e.usingExports = !0, e.map.isDefine ? e.exports ? D[e.map.id] = e.exports : e.exports = D[e.map.id] = {} : void 0
            },
            module: function(e) {
                return e.module ? e.module : e.module = {
                    id: e.map.id,
                    uri: e.map.url,
                    config: function() {
                        return m(j.config, e.map.id) || {}
                    },
                    exports: e.exports || (e.exports = {})
                }
            }
        }, x = function(e) {
            this.events = m($, e.id) || {}, this.map = e, this.shim = m(j.shim, e.id), this.depExports = [], this.depMaps = [], this.depMatched = [], this.pluginMaps = {}, this.depCount = 0
        }, x.prototype = {
            init: function(e, t, n, i) {
                i = i || {}, this.inited || (this.factory = t, n ? this.on("error", n) : this.events.error && (n = u(this, function(e) {
                    this.emit("error", e)
                })), this.depMaps = e && e.slice(0), this.errback = n, this.inited = !0, this.ignore = i.ignore, i.enabled || this.enabled ? this.enable() : this.check())
            },
            defineDep: function(e, t) {
                this.depMatched[e] || (this.depMatched[e] = !0, this.depCount -= 1, this.depExports[e] = t)
            },
            fetch: function() {
                if (!this.fetched) {
                    this.fetched = !0, E.startTime = (new Date).getTime();
                    var e = this.map;
                    if (!this.shim) return e.prefix ? this.callPlugin() : this.load();
                    E.makeRequire(this.map, {
                        enableBuildCallback: !0
                    })(this.shim.deps || [], u(this, function() {
                        return e.prefix ? this.callPlugin() : this.load()
                    }))
                }
            },
            load: function() {
                var e = this.map.url;
                L[e] || (L[e] = !0, E.load(this.map.id, e))
            },
            check: function() {
                if (this.enabled && !this.enabling) {
                    var e, t, n = this.map.id;
                    t = this.depExports;
                    var i = this.exports,
                        s = this.factory;
                    if (this.inited) {
                        if (this.error) this.emit("error", this.error);
                        else if (!this.defining) {
                            if (this.defining = !0, 1 > this.depCount && !this.defined) {
                                if (G(s)) {
                                    if (this.events.error && this.map.isDefine || g.onError !== ca) try {
                                        i = E.execCb(n, s, t, i)
                                    } catch (r) {
                                        e = r
                                    } else i = E.execCb(n, s, t, i);
                                    if (this.map.isDefine && void 0 === i && ((t = this.module) ? i = t.exports : this.usingExports && (i = this.exports)), e) return e.requireMap = this.map, e.requireModules = this.map.isDefine ? [this.map.id] : null, e.requireType = this.map.isDefine ? "define" : "require", c(this.error = e)
                                } else i = s;
                                this.exports = i, this.map.isDefine && !this.ignore && (D[n] = i, g.onResourceLoad) && g.onResourceLoad(E, this.map, this.depMaps), h(n), this.defined = !0
                            }
                            this.defining = !1, this.defined && !this.defineEmitted && (this.defineEmitted = !0, this.emit("defined", this.exports), this.defineEmitComplete = !0)
                        }
                    } else this.fetch()
                }
            },
            callPlugin: function() {
                var e = this.map,
                    i = e.id,
                    s = o(e.prefix);
                this.depMaps.push(s), l(s, "defined", u(this, function(s) {
                    var r, d;
                    d = m(F, this.map.id);
                    var f = this.map.name,
                        p = this.map.parentMap ? this.map.parentMap.name : null,
                        v = E.makeRequire(e.parentMap, {
                            enableBuildCallback: !0
                        });
                    this.map.unnormalized ? (s.normalize && (f = s.normalize(f, function(e) {
                        return n(e, p, !0)
                    }) || ""), s = o(e.prefix + "!" + f, this.map.parentMap), l(s, "defined", u(this, function(e) {
                        this.init([], function() {
                            return e
                        }, null, {
                            enabled: !0,
                            ignore: !0
                        })
                    })), (d = m(P, s.id)) && (this.depMaps.push(s), this.events.error && d.on("error", u(this, function(e) {
                        this.emit("error", e)
                    })), d.enable())) : d ? (this.map.url = E.nameToUrl(d), this.load()) : (r = u(this, function(e) {
                        this.init([], function() {
                            return e
                        }, null, {
                            enabled: !0
                        })
                    }), r.error = u(this, function(e) {
                        this.inited = !0, this.error = e, e.requireModules = [i], B(P, function(e) {
                            0 === e.map.id.indexOf(i + "_unnormalized") && h(e.map.id)
                        }), c(e)
                    }), r.fromText = u(this, function(n, s) {
                        var l = e.name,
                            u = o(l),
                            d = M;
                        s && (n = s), d && (M = !1), a(u), t(j.config, i) && (j.config[l] = j.config[i]);
                        try {
                            g.exec(n)
                        } catch (h) {
                            return c(C("fromtexteval", "fromText eval for " + i + " failed: " + h, h, [i]))
                        }
                        d && (M = !0), this.depMaps.push(u), E.completeLoad(l), v([l], r)
                    }), s.load(e.name, v, r, j))
                })), E.enable(s, this), this.pluginMaps[s.id] = s
            },
            enable: function() {
                O[this.map.id] = this, this.enabling = this.enabled = !0, v(this.depMaps, u(this, function(e, n) {
                    var i, s;
                    if ("string" == typeof e) {
                        if (e = o(e, this.map.isDefine ? this.map : this.map.parentMap, !1, !this.skipMap), this.depMaps[n] = e, i = m(S, e.id)) return void(this.depExports[n] = i(this));
                        this.depCount += 1, l(e, "defined", u(this, function(e) {
                            this.defineDep(n, e), this.check()
                        })), this.errback && l(e, "error", u(this, this.errback))
                    }
                    i = e.id, s = P[i], !t(S, i) && s && !s.enabled && E.enable(e, this)
                })), B(this.pluginMaps, u(this, function(e) {
                    var t = m(P, e.id);
                    t && !t.enabled && E.enable(e, this)
                })), this.enabling = !1, this.check()
            },
            on: function(e, t) {
                var n = this.events[e];
                n || (n = this.events[e] = []), n.push(t)
            },
            emit: function(e, t) {
                v(this.events[e], function(e) {
                    e(t)
                }), "error" === e && delete this.events[e]
            }
        }, E = {
            config: j,
            contextName: e,
            registry: P,
            defined: D,
            urlFetched: L,
            defQueue: A,
            Module: x,
            makeModuleMap: o,
            nextTick: g.nextTick,
            onError: c,
            configure: function(e) {
                e.baseUrl && "/" !== e.baseUrl.charAt(e.baseUrl.length - 1) && (e.baseUrl += "/");
                var t = j.shim,
                    n = {
                        paths: !0,
                        bundles: !0,
                        config: !0,
                        map: !0
                    };
                B(e, function(e, t) {
                    n[t] ? (j[t] || (j[t] = {}), U(j[t], e, !0, !0)) : j[t] = e
                }), e.bundles && B(e.bundles, function(e, t) {
                    v(e, function(e) {
                        e !== t && (F[e] = t)
                    })
                }), e.shim && (B(e.shim, function(e, n) {
                    H(e) && (e = {
                        deps: e
                    }), !e.exports && !e.init || e.exportsFn || (e.exportsFn = E.makeShimExports(e)), t[n] = e
                }), j.shim = t), e.packages && v(e.packages, function(e) {
                    var t, e = "string" == typeof e ? {
                        name: e
                    } : e;
                    t = e.name, e.location && (j.paths[t] = e.location), j.pkgs[t] = e.name + "/" + (e.main || "main").replace(ia, "").replace(Q, "")
                }), B(P, function(e, t) {
                    !e.inited && !e.map.unnormalized && (e.map = o(t))
                }), (e.deps || e.callback) && E.require(e.deps || [], e.callback)
            },
            makeShimExports: function(e) {
                return function() {
                    var t;
                    return e.init && (t = e.init.apply(ba, arguments)), t || e.exports && da(e.exports)
                }
            },
            makeRequire: function(s, r) {
                function l(n, i, u) {
                    var d, h;
                    return r.enableBuildCallback && i && G(i) && (i.__requireJsBuild = !0), "string" == typeof n ? G(i) ? c(C("requireargs", "Invalid require call"), u) : s && t(S, n) ? S[n](P[s.id]) : g.get ? g.get(E, n, s, l) : (d = o(n, s, !1, !0), d = d.id, t(D, d) ? D[d] : c(C("notloaded", 'Module name "' + d + '" has not been loaded yet for context: ' + e + (s ? "" : ". Use require([])")))) : (b(), E.nextTick(function() {
                        b(), h = a(o(null, s)), h.skipMap = r.skipMap, h.init(n, i, u, {
                            enabled: !0
                        }), p()
                    }), l)
                }
                return r = r || {}, U(l, {
                    isBrowser: z,
                    toUrl: function(e) {
                        var t, i = e.lastIndexOf("."),
                            r = e.split("/")[0];
                        return -1 !== i && ("." !== r && ".." !== r || i > 1) && (t = e.substring(i, e.length), e = e.substring(0, i)), E.nameToUrl(n(e, s && s.id, !0), t, !0)
                    },
                    defined: function(e) {
                        return t(D, o(e, s, !1, !0).id)
                    },
                    specified: function(e) {
                        return e = o(e, s, !1, !0).id, t(D, e) || t(P, e)
                    }
                }), s || (l.undef = function(e) {
                    d();
                    var t = o(e, s, !0),
                        n = m(P, e);
                    i(e), delete D[e], delete L[t.url], delete $[e], T(A, function(t, n) {
                        t[0] === e && A.splice(n, 1)
                    }), n && (n.events.defined && ($[e] = n.events), h(e))
                }), l
            },
            enable: function(e) {
                m(P, e.id) && a(e).enable()
            },
            completeLoad: function(e) {
                var n, i, r = m(j.shim, e) || {},
                    o = r.exports;
                for (d(); A.length;) {
                    if (i = A.shift(), null === i[0]) {
                        if (i[0] = e, n) break;
                        n = !0
                    } else i[0] === e && (n = !0);
                    y(i)
                }
                if (i = m(P, e), !n && !t(D, e) && i && !i.inited) {
                    if (j.enforceDefine && (!o || !da(o))) return s(e) ? void 0 : c(C("nodefine", "No define call for " + e, null, [e]));
                    y([e, r.deps || [], r.exportsFn])
                }
                p()
            },
            nameToUrl: function(e, t, n) {
                var i, s, r;
                if ((i = m(j.pkgs, e)) && (e = i), i = m(F, e)) return E.nameToUrl(i, t, n);
                if (g.jsExtRegExp.test(e)) i = e + (t || "");
                else {
                    for (i = j.paths, e = e.split("/"), s = e.length; s > 0; s -= 1)
                        if (r = e.slice(0, s).join("/"), r = m(i, r)) {
                            H(r) && (r = r[0]), e.splice(0, s, r);
                            break
                        }
                    i = e.join("/"), i += t || (/^data\:|\?/.test(i) || n ? "" : ".js"), i = ("/" === i.charAt(0) || i.match(/^[\w\+\.\-]+:/) ? "" : j.baseUrl) + i
                }
                return j.urlArgs ? i + ((-1 === i.indexOf("?") ? "?" : "&") + j.urlArgs) : i
            },
            load: function(e, t) {
                g.load(E, e, t)
            },
            execCb: function(e, t, n, i) {
                return t.apply(i, n)
            },
            onScriptLoad: function(e) {
                ("load" === e.type || ja.test((e.currentTarget || e.srcElement).readyState)) && (N = null, e = w(e), E.completeLoad(e.id))
            },
            onScriptError: function(e) {
                var t = w(e);
                return s(t.id) ? void 0 : c(C("scripterror", "Script error for: " + t.id, e, [t.id]))
            }
        }, E.require = E.makeRequire(), E
    }
    var g, x, y, D, I, E, N, J, s, O, ka = /(\/\*([\s\S]*?)\*\/|([^:]|^)\/\/(.*)$)/gm,
        la = /[^.]\s*require\s*\(\s*["']([^'"\s]+)["']\s*\)/g,
        Q = /\.js$/,
        ia = /^\.\//;
    x = Object.prototype;
    var K = x.toString,
        fa = x.hasOwnProperty,
        ha = Array.prototype.splice,
        z = !("undefined" == typeof window || "undefined" == typeof navigator || !window.document),
        ea = !z && "undefined" != typeof importScripts,
        ja = z && "PLAYSTATION 3" === navigator.platform ? /^complete$/ : /^(complete|loaded)$/,
        Y = "undefined" != typeof opera && "[object Opera]" === opera.toString(),
        F = {},
        q = {},
        R = [],
        M = !1;
    if ("undefined" == typeof define) {
        if ("undefined" != typeof requirejs) {
            if (G(requirejs)) return;
            q = requirejs, requirejs = void 0
        }
        "undefined" != typeof require && !G(require) && (q = require, require = void 0), g = requirejs = function(e, t, n, i) {
            var s, r = "_";
            return !H(e) && "string" != typeof e && (s = e, H(t) ? (e = t, t = n, n = i) : e = []), s && s.context && (r = s.context), (i = m(F, r)) || (i = F[r] = g.s.newContext(r)), s && i.configure(s), i.require(e, t, n)
        }, g.config = function(e) {
            return g(e)
        }, g.nextTick = "undefined" != typeof setTimeout ? function(e) {
            setTimeout(e, 4)
        } : function(e) {
            e()
        }, require || (require = g), g.version = "2.1.14", g.jsExtRegExp = /^\/|:|\?|\.js$/, g.isBrowser = z, x = g.s = {
            contexts: F,
            newContext: ga
        }, g({}), v(["toUrl", "undef", "defined", "specified"], function(e) {
            g[e] = function() {
                var t = F._;
                return t.require[e].apply(t, arguments)
            }
        }), z && (y = x.head = document.getElementsByTagName("head")[0], D = document.getElementsByTagName("base")[0]) && (y = x.head = D.parentNode), g.onError = ca, g.createNode = function(e) {
            var t = e.xhtml ? document.createElementNS("http://www.w3.org/1999/xhtml", "html:script") : document.createElement("script");
            return t.type = e.scriptType || "text/javascript", t.charset = "utf-8", t.async = !0, t
        }, g.load = function(e, t, n) {
            var i = e && e.config || {};
            if (z) return i = g.createNode(i, t, n), i.setAttribute("data-requirecontext", e.contextName), i.setAttribute("data-requiremodule", t), !i.attachEvent || i.attachEvent.toString && 0 > i.attachEvent.toString().indexOf("[native code") || Y ? (i.addEventListener("load", e.onScriptLoad, !1), i.addEventListener("error", e.onScriptError, !1)) : (M = !0, i.attachEvent("onreadystatechange", e.onScriptLoad)), i.src = n, J = i, D ? y.insertBefore(i, D) : y.appendChild(i), J = null, i;
            if (ea) try {
                importScripts(n), e.completeLoad(t)
            } catch (s) {
                e.onError(C("importscripts", "importScripts failed for " + t + " at " + n, s, [t]))
            }
        }, z && !q.skipDataMain && T(document.getElementsByTagName("script"), function(e) {
            return y || (y = e.parentNode), (I = e.getAttribute("data-main")) ? (s = I, q.baseUrl || (E = s.split("/"), s = E.pop(), O = E.length ? E.join("/") + "/" : "./", q.baseUrl = O), s = s.replace(Q, ""), g.jsExtRegExp.test(s) && (s = I), q.deps = q.deps ? q.deps.concat(s) : [s], !0) : void 0
        }), define = function(e, t, n) {
            var i, s;
            "string" != typeof e && (n = t, t = e, e = null), H(t) || (n = t, t = null), !t && G(n) && (t = [], n.length && (n.toString().replace(ka, "").replace(la, function(e, n) {
                t.push(n)
            }), t = (1 === n.length ? ["require"] : ["require", "exports", "module"]).concat(t))), M && ((i = J) || (N && "interactive" === N.readyState || T(document.getElementsByTagName("script"), function(e) {
                return "interactive" === e.readyState ? N = e : void 0
            }), i = N), i && (e || (e = i.getAttribute("data-requiremodule")), s = F[i.getAttribute("data-requirecontext")])), (s ? s.defQueue : R).push([e, t, n])
        }, define.amd = {
            jQuery: !0
        }, g.exec = function(b) {
            return eval(b)
        }, g(q)
    }
}(this), define("requireLib", function() {}),
    function() {
        this.MooTools = {
            version: "1.4.1",
            build: "d1fb25710e3c5482a219ab9dc675a4e0ad2176b6"
        };
        var e = this.typeOf = function(e) {
                if (null == e) return "null";
                if (e.$family) return e.$family();
                if (e.nodeName) {
                    if (1 == e.nodeType) return "element";
                    if (3 == e.nodeType) return /\S/.test(e.nodeValue) ? "textnode" : "whitespace"
                } else if ("number" == typeof e.length) {
                    if (e.callee) return "arguments";
                    if ("item" in e) return "collection"
                }
                return typeof e
            },
            t = this.instanceOf = function(e, t) {
                if (null == e) return !1;
                for (var n = e.$constructor || e.constructor; n;) {
                    if (n === t) return !0;
                    n = n.parent
                }
                return e instanceof t
            },
            n = this.Function,
            i = !0;
        for (var s in {
                toString: 1
            }) i = null;
        i && (i = ["hasOwnProperty", "valueOf", "isPrototypeOf", "propertyIsEnumerable", "toLocaleString", "toString", "constructor"]), n.prototype.overloadSetter = function(e) {
            var t = this;
            return function(n, s) {
                if (null == n) return this;
                if (e || "string" != typeof n) {
                    for (var r in n) t.call(this, r, n[r]);
                    if (i)
                        for (var o = i.length; o--;) r = i[o], n.hasOwnProperty(r) && t.call(this, r, n[r])
                } else t.call(this, n, s);
                return this
            }
        }, n.prototype.overloadGetter = function(e) {
            var t = this;
            return function(n) {
                var i, s;
                if (e || "string" != typeof n ? i = n : arguments.length > 1 && (i = arguments), i) {
                    s = {};
                    for (var r = 0; r < i.length; r++) s[i[r]] = t.call(this, i[r])
                } else s = t.call(this, n);
                return s
            }
        }, n.prototype.extend = function(e, t) {
            this[e] = t
        }.overloadSetter(), n.prototype.implement = function(e, t) {
            this.prototype[e] = t
        }.overloadSetter();
        var r = Array.prototype.slice;
        n.from = function(t) {
            return "function" == e(t) ? t : function() {
                return t
            }
        }, Array.from = function(t) {
            return null == t ? [] : o.isEnumerable(t) && "string" != typeof t ? "array" == e(t) ? t : r.call(t) : [t]
        }, Number.from = function(e) {
            var t = parseFloat(e);
            return isFinite(t) ? t : null
        }, String.from = function(e) {
            return e + ""
        }, n.implement({
            hide: function() {
                return this.$hidden = !0, this
            },
            protect: function() {
                return this.$protected = !0, this
            }
        });
        var o = this.Type = function(t, n) {
                if (t) {
                    var i = t.toLowerCase(),
                        s = function(t) {
                            return e(t) == i
                        };
                    o["is" + t] = s, null != n && (n.prototype.$family = function() {
                        return i
                    }.hide(), n.type = s)
                }
                return null == n ? null : (n.extend(this), n.$constructor = o, n.prototype.$constructor = n, n)
            },
            a = Object.prototype.toString;
        o.isEnumerable = function(e) {
            return null != e && "number" == typeof e.length && "[object Function]" != a.call(e)
        };
        var l = {},
            c = function(t) {
                var n = e(t.prototype);
                return l[n] || (l[n] = [])
            },
            u = function(t, n) {
                if (!n || !n.$hidden) {
                    for (var i = c(this), s = 0; s < i.length; s++) {
                        var o = i[s];
                        "type" == e(o) ? u.call(o, t, n) : o.call(this, t, n)
                    }
                    var a = this.prototype[t];
                    null != a && a.$protected || (this.prototype[t] = n), null == this[t] && "function" == e(n) && d.call(this, t, function(e) {
                        return n.apply(e, r.call(arguments, 1))
                    })
                }
            },
            d = function(e, t) {
                if (!t || !t.$hidden) {
                    var n = this[e];
                    null != n && n.$protected || (this[e] = t)
                }
            };
        o.implement({
            implement: u.overloadSetter(),
            extend: d.overloadSetter(),
            alias: function(e, t) {
                u.call(this, e, this.prototype[t])
            }.overloadSetter(),
            mirror: function(e) {
                return c(this).push(e), this
            }
        }), new o("Type", o);
        var h = function(e, t, n) {
            var i = t != Object,
                s = t.prototype;
            i && (t = new o(e, t));
            for (var r = 0, a = n.length; a > r; r++) {
                var l = n[r],
                    c = t[l],
                    u = s[l];
                c && c.protect(), i && u && (delete s[l], s[l] = u.protect())
            }
            return i && t.implement(s), h
        };
        h("String", String, ["charAt", "charCodeAt", "concat", "indexOf", "lastIndexOf", "match", "quote", "replace", "search", "slice", "split", "substr", "substring", "trim", "toLowerCase", "toUpperCase"])("Array", Array, ["pop", "push", "reverse", "shift", "sort", "splice", "unshift", "concat", "join", "slice", "indexOf", "lastIndexOf", "filter", "forEach", "every", "map", "some", "reduce", "reduceRight"])("Number", Number, ["toExponential", "toFixed", "toLocaleString", "toPrecision"])("Function", n, ["apply", "call", "bind"])("RegExp", RegExp, ["exec", "test"])("Object", Object, ["create", "defineProperty", "defineProperties", "keys", "getPrototypeOf", "getOwnPropertyDescriptor", "getOwnPropertyNames", "preventExtensions", "isExtensible", "seal", "isSealed", "freeze", "isFrozen"])("Date", Date, ["now"]), Object.extend = d.overloadSetter(), Date.extend("now", function() {
            return +new Date
        }), new o("Boolean", Boolean), Number.prototype.$family = function() {
            return isFinite(this) ? "number" : "null"
        }.hide(), Number.extend("random", function(e, t) {
            return Math.floor(Math.random() * (t - e + 1) + e)
        });
        var m = Object.prototype.hasOwnProperty;
        Object.extend("forEach", function(e, t, n) {
            for (var i in e) m.call(e, i) && t.call(n, e[i], i, e)
        }), Object.each = Object.forEach, Array.implement({
            forEach: function(e, t) {
                for (var n = 0, i = this.length; i > n; n++) n in this && e.call(t, this[n], n, this)
            },
            each: function(e, t) {
                return Array.forEach(this, e, t), this
            }
        });
        var f = function(t) {
            switch (e(t)) {
                case "array":
                    return t.clone();
                case "object":
                    return Object.clone(t);
                default:
                    return t
            }
        };
        Array.implement("clone", function() {
            for (var e = this.length, t = new Array(e); e--;) t[e] = f(this[e]);
            return t
        });
        var p = function(t, n, i) {
            switch (e(i)) {
                case "object":
                    "object" == e(t[n]) ? Object.merge(t[n], i) : t[n] = Object.clone(i);
                    break;
                case "array":
                    t[n] = i.clone();
                    break;
                default:
                    t[n] = i
            }
            return t
        };
        Object.extend({
            merge: function(t, n, i) {
                if ("string" == e(n)) return p(t, n, i);
                for (var s = 1, r = arguments.length; r > s; s++) {
                    var o = arguments[s];
                    for (var a in o) p(t, a, o[a])
                }
                return t
            },
            clone: function(e) {
                var t = {};
                for (var n in e) t[n] = f(e[n]);
                return t
            },
            append: function(e) {
                for (var t = 1, n = arguments.length; n > t; t++) {
                    var i = arguments[t] || {};
                    for (var s in i) e[s] = i[s]
                }
                return e
            }
        }), ["Object", "WhiteSpace", "TextNode", "Collection", "Arguments"].each(function(e) {
            new o(e)
        });
        var g = Date.now();
        String.extend("uniqueID", function() {
            return (g++).toString(36)
        });
        var v = this.Hash = new o("Hash", function(t) {
            "hash" == e(t) && (t = Object.clone(t.getClean()));
            for (var n in t) this[n] = t[n];
            return this
        });
        v.implement({
            forEach: function(e, t) {
                Object.forEach(this, e, t)
            },
            getClean: function() {
                var e = {};
                for (var t in this) this.hasOwnProperty(t) && (e[t] = this[t]);
                return e
            },
            getLength: function() {
                var e = 0;
                for (var t in this) this.hasOwnProperty(t) && e++;
                return e
            }
        }), v.alias("each", "forEach"), Object.type = o.isObject;
        var y = this.Native = function(e) {
            return new o(e.name, e.initialize)
        };
        y.type = o.type, y.implement = function(e, t) {
            for (var n = 0; n < e.length; n++) e[n].implement(t);
            return y
        };
        var w = Array.type;
        Array.type = function(e) {
            return t(e, Array) || w(e)
        }, this.$A = function(e) {
            return Array.from(e).slice()
        }, this.$arguments = function(e) {
            return function() {
                return arguments[e]
            }
        }, this.$chk = function(e) {
            return !(!e && 0 !== e)
        }, this.$clear = function(e) {
            return clearTimeout(e), clearInterval(e), null
        }, this.$defined = function(e) {
            return null != e
        }, this.$each = function(t, n, i) {
            var s = e(t);
            ("arguments" == s || "collection" == s || "array" == s || "elements" == s ? Array : Object).each(t, n, i)
        }, this.$empty = function() {}, this.$extend = function(e, t) {
            return Object.append(e, t)
        }, this.$H = function(e) {
            return new v(e)
        }, this.$merge = function() {
            var e = Array.slice(arguments);
            return e.unshift({}), Object.merge.apply(null, e)
        }, this.$lambda = n.from, this.$mixin = Object.merge, this.$random = Number.random, this.$splat = Array.from, this.$time = Date.now, this.$type = function(t) {
            var n = e(t);
            return "elements" == n ? "array" : "null" == n ? !1 : n
        }, this.$unlink = function(t) {
            switch (e(t)) {
                case "object":
                    return Object.clone(t);
                case "array":
                    return Array.clone(t);
                case "hash":
                    return new v(t);
                default:
                    return t
            }
        }
    }(), Array.implement({
        every: function(e, t) {
            for (var n = 0, i = this.length >>> 0; i > n; n++)
                if (n in this && !e.call(t, this[n], n, this)) return !1;
            return !0
        },
        filter: function(e, t) {
            for (var n = [], i = 0, s = this.length >>> 0; s > i; i++) i in this && e.call(t, this[i], i, this) && n.push(this[i]);
            return n
        },
        indexOf: function(e, t) {
            for (var n = this.length >>> 0, i = 0 > t ? Math.max(0, n + t) : t || 0; n > i; i++)
                if (this[i] === e) return i;
            return -1
        },
        map: function(e, t) {
            for (var n = this.length >>> 0, i = Array(n), s = 0; n > s; s++) s in this && (i[s] = e.call(t, this[s], s, this));
            return i
        },
        some: function(e, t) {
            for (var n = 0, i = this.length >>> 0; i > n; n++)
                if (n in this && e.call(t, this[n], n, this)) return !0;
            return !1
        },
        clean: function() {
            return this.filter(function(e) {
                return null != e
            })
        },
        invoke: function(e) {
            var t = Array.slice(arguments, 1);
            return this.map(function(n) {
                return n[e].apply(n, t)
            })
        },
        associate: function(e) {
            for (var t = {}, n = Math.min(this.length, e.length), i = 0; n > i; i++) t[e[i]] = this[i];
            return t
        },
        link: function(e) {
            for (var t = {}, n = 0, i = this.length; i > n; n++)
                for (var s in e)
                    if (e[s](this[n])) {
                        t[s] = this[n], delete e[s];
                        break
                    }
            return t
        },
        contains: function(e, t) {
            return -1 != this.indexOf(e, t)
        },
        append: function(e) {
            return this.push.apply(this, e), this
        },
        getLast: function() {
            return this.length ? this[this.length - 1] : null
        },
        getRandom: function() {
            return this.length ? this[Number.random(0, this.length - 1)] : null
        },
        include: function(e) {
            return this.contains(e) || this.push(e), this
        },
        combine: function(e) {
            for (var t = 0, n = e.length; n > t; t++) this.include(e[t]);
            return this
        },
        erase: function(e) {
            for (var t = this.length; t--;) this[t] === e && this.splice(t, 1);
            return this
        },
        empty: function() {
            return this.length = 0, this
        },
        flatten: function() {
            for (var e = [], t = 0, n = this.length; n > t; t++) {
                var i = typeOf(this[t]);
                "null" != i && (e = e.concat("array" == i || "collection" == i || "arguments" == i || instanceOf(this[t], Array) ? Array.flatten(this[t]) : this[t]))
            }
            return e
        },
        pick: function() {
            for (var e = 0, t = this.length; t > e; e++)
                if (null != this[e]) return this[e];
            return null
        },
        hexToRgb: function(e) {
            if (3 != this.length) return null;
            var t = this.map(function(e) {
                return 1 == e.length && (e += e), e.toInt(16)
            });
            return e ? t : "rgb(" + t + ")"
        },
        rgbToHex: function(e) {
            if (this.length < 3) return null;
            if (4 == this.length && 0 == this[3] && !e) return "transparent";
            for (var t = [], n = 0; 3 > n; n++) {
                var i = (this[n] - 0).toString(16);
                t.push(1 == i.length ? "0" + i : i)
            }
            return e ? t : "#" + t.join("")
        }
    }), Array.alias("extend", "append");
var $pick = function() {
    return Array.from(arguments).pick()
};
String.implement({
        test: function(e, t) {
            return ("regexp" == typeOf(e) ? e : new RegExp("" + e, t)).test(this)
        },
        contains: function(e, t) {
            return t ? (t + this + t).indexOf(t + e + t) > -1 : String(this).indexOf(e) > -1
        },
        trim: function() {
            return String(this).replace(/^\s+|\s+$/g, "")
        },
        clean: function() {
            return String(this).replace(/\s+/g, " ").trim()
        },
        camelCase: function() {
            return String(this).replace(/-\D/g, function(e) {
                return e.charAt(1).toUpperCase()
            })
        },
        hyphenate: function() {
            return String(this).replace(/[A-Z]/g, function(e) {
                return "-" + e.charAt(0).toLowerCase()
            })
        },
        capitalize: function() {
            return String(this).replace(/\b[a-z]/g, function(e) {
                return e.toUpperCase()
            })
        },
        escapeRegExp: function() {
            return String(this).replace(/([-.*+?^${}()|[\]\/\\])/g, "\\$1")
        },
        toInt: function(e) {
            return parseInt(this, e || 10)
        },
        toFloat: function() {
            return parseFloat(this)
        },
        hexToRgb: function(e) {
            var t = String(this).match(/^#?(\w{1,2})(\w{1,2})(\w{1,2})$/);
            return t ? t.slice(1).hexToRgb(e) : null
        },
        rgbToHex: function(e) {
            var t = String(this).match(/\d{1,3}/g);
            return t ? t.rgbToHex(e) : null
        },
        substitute: function(e, t) {
            return String(this).replace(t || /\\?\{([^{}]+)\}/g, function(t, n) {
                return "\\" == t.charAt(0) ? t.slice(1) : null != e[n] ? e[n] : ""
            })
        }
    }), Number.implement({
        limit: function(e, t) {
            return Math.min(t, Math.max(e, this))
        },
        round: function(e) {
            return e = Math.pow(10, e || 0).toFixed(0 > e ? -e : 0), Math.round(this * e) / e
        },
        times: function(e, t) {
            for (var n = 0; this > n; n++) e.call(t, n, this)
        },
        toFloat: function() {
            return parseFloat(this)
        },
        toInt: function(e) {
            return parseInt(this, e || 10)
        }
    }), Number.alias("each", "times"),
    function(e) {
        var t = {};
        e.each(function(e) {
            Number[e] || (t[e] = function() {
                return Math[e].apply(null, [this].concat(Array.from(arguments)))
            })
        }), Number.implement(t)
    }(["abs", "acos", "asin", "atan", "atan2", "ceil", "cos", "exp", "floor", "log", "max", "min", "pow", "sin", "sqrt", "tan"]), Function.extend({
        attempt: function() {
            for (var e = 0, t = arguments.length; t > e; e++) try {
                return arguments[e]()
            } catch (n) {}
            return null
        }
    }), Function.implement({
        attempt: function(e, t) {
            try {
                return this.apply(t, Array.from(e))
            } catch (n) {}
            return null
        },
        bind: function(e) {
            var t = this,
                n = arguments.length > 1 ? Array.slice(arguments, 1) : null,
                i = function() {},
                s = function() {
                    var r = e,
                        o = arguments.length;
                    this instanceof s && (i.prototype = t.prototype, r = new i);
                    var a = n || o ? t.apply(r, n && o ? n.concat(Array.slice(arguments)) : n || arguments) : t.call(r);
                    return r == e ? a : r
                };
            return s
        },
        pass: function(e, t) {
            var n = this;
            return null != e && (e = Array.from(e)),
                function() {
                    return n.apply(t, e || arguments)
                }
        },
        delay: function(e, t, n) {
            return setTimeout(this.pass(null == n ? [] : n, t), e)
        },
        periodical: function(e, t, n) {
            return setInterval(this.pass(null == n ? [] : n, t), e)
        }
    }), delete Function.prototype.bind, Function.implement({
        create: function(e) {
            var t = this;
            return e = e || {},
                function(n) {
                    var i = e.arguments;
                    i = null != i ? Array.from(i) : Array.slice(arguments, e.event ? 1 : 0), e.event && (i = [n || window.event].extend(i));
                    var s = function() {
                        return t.apply(e.bind || null, i)
                    };
                    return e.delay ? setTimeout(s, e.delay) : e.periodical ? setInterval(s, e.periodical) : e.attempt ? Function.attempt(s) : s()
                }
        },
        bind: function(e, t) {
            var n = this;
            return null != t && (t = Array.from(t)),
                function() {
                    return n.apply(e, t || arguments)
                }
        },
        bindWithEvent: function(e, t) {
            var n = this;
            return null != t && (t = Array.from(t)),
                function(i) {
                    return n.apply(e, null == t ? arguments : [i].concat(t))
                }
        },
        run: function(e, t) {
            return this.apply(t, Array.from(e))
        }
    }), Object.create == Function.prototype.create && (Object.create = null);
var $try = Function.attempt;
! function() {
    var e = Object.prototype.hasOwnProperty;
    Object.extend({
        subset: function(e, t) {
            for (var n = {}, i = 0, s = t.length; s > i; i++) {
                var r = t[i];
                r in e && (n[r] = e[r])
            }
            return n
        },
        map: function(t, n, i) {
            var s = {};
            for (var r in t) e.call(t, r) && (s[r] = n.call(i, t[r], r, t));
            return s
        },
        filter: function(t, n, i) {
            var s = {};
            for (var r in t) {
                var o = t[r];
                e.call(t, r) && n.call(i, o, r, t) && (s[r] = o)
            }
            return s
        },
        every: function(t, n, i) {
            for (var s in t)
                if (e.call(t, s) && !n.call(i, t[s], s)) return !1;
            return !0
        },
        some: function(t, n, i) {
            for (var s in t)
                if (e.call(t, s) && n.call(i, t[s], s)) return !0;
            return !1
        },
        keys: function(t) {
            var n = [];
            for (var i in t) e.call(t, i) && n.push(i);
            return n
        },
        values: function(t) {
            var n = [];
            for (var i in t) e.call(t, i) && n.push(t[i]);
            return n
        },
        getLength: function(e) {
            return Object.keys(e).length
        },
        keyOf: function(t, n) {
            for (var i in t)
                if (e.call(t, i) && t[i] === n) return i;
            return null
        },
        contains: function(e, t) {
            return null != Object.keyOf(e, t)
        },
        toQueryString: function(e, t) {
            var n = [];
            return Object.each(e, function(e, i) {
                t && (i = t + "[" + i + "]");
                var s;
                switch (typeOf(e)) {
                    case "object":
                        s = Object.toQueryString(e, i);
                        break;
                    case "array":
                        var r = {};
                        e.each(function(e, t) {
                            r[t] = e
                        }), s = Object.toQueryString(r, i);
                        break;
                    default:
                        s = i + "=" + encodeURIComponent(e)
                }
                null != e && n.push(s)
            }), n.join("&")
        }
    })
}(), Hash.implement({
        has: Object.prototype.hasOwnProperty,
        keyOf: function(e) {
            return Object.keyOf(this, e)
        },
        hasValue: function(e) {
            return Object.contains(this, e)
        },
        extend: function(e) {
            return Hash.each(e || {}, function(e, t) {
                Hash.set(this, t, e)
            }, this), this
        },
        combine: function(e) {
            return Hash.each(e || {}, function(e, t) {
                Hash.include(this, t, e)
            }, this), this
        },
        erase: function(e) {
            return this.hasOwnProperty(e) && delete this[e], this
        },
        get: function(e) {
            return this.hasOwnProperty(e) ? this[e] : null
        },
        set: function(e, t) {
            return (!this[e] || this.hasOwnProperty(e)) && (this[e] = t), this
        },
        empty: function() {
            return Hash.each(this, function(e, t) {
                delete this[t]
            }, this), this
        },
        include: function(e, t) {
            return null == this[e] && (this[e] = t), this
        },
        map: function(e, t) {
            return new Hash(Object.map(this, e, t))
        },
        filter: function(e, t) {
            return new Hash(Object.filter(this, e, t))
        },
        every: function(e, t) {
            return Object.every(this, e, t)
        },
        some: function(e, t) {
            return Object.some(this, e, t)
        },
        getKeys: function() {
            return Object.keys(this)
        },
        getValues: function() {
            return Object.values(this)
        },
        toQueryString: function(e) {
            return Object.toQueryString(this, e)
        }
    }), Hash.extend = Object.append, Hash.alias({
        indexOf: "keyOf",
        contains: "hasValue"
    }),
    function() {
        var e = this.document,
            t = e.window = this,
            n = 1;
        this.$uid = t.ActiveXObject ? function(e) {
            return (e.uid || (e.uid = [n++]))[0]
        } : function(e) {
            return e.uid || (e.uid = n++)
        }, $uid(t), $uid(e);
        var i = navigator.userAgent.toLowerCase(),
            s = navigator.platform.toLowerCase(),
            r = i.match(/(opera|ie|firefox|chrome|version)[\s\/:]([\w\d\.]+)?.*?(safari|version[\s\/:]([\w\d\.]+)|$)/) || [null, "unknown", 0],
            o = "ie" == r[1] && e.documentMode,
            a = this.Browser = {
                extend: Function.prototype.extend,
                name: "version" == r[1] ? r[3] : r[1],
                version: o || parseFloat("opera" == r[1] && r[4] ? r[4] : r[2]),
                Platform: {
                    name: i.match(/ip(?:ad|od|hone)/) ? "ios" : (i.match(/(?:webos|android)/) || s.match(/mac|win|linux/) || ["other"])[0]
                },
                Features: {
                    xpath: !!e.evaluate,
                    air: !!t.runtime,
                    query: !!e.querySelector,
                    json: !!t.JSON
                },
                Plugins: {}
            };
        a[a.name] = !0, a[a.name + parseInt(a.version, 10)] = !0, a.Platform[a.Platform.name] = !0, a.Request = function() {
            var e = function() {
                    return new XMLHttpRequest
                },
                t = function() {
                    return new ActiveXObject("MSXML2.XMLHTTP")
                },
                n = function() {
                    return new ActiveXObject("Microsoft.XMLHTTP")
                };
            return Function.attempt(function() {
                return e(), e
            }, function() {
                return t(), t
            }, function() {
                return n(), n
            })
        }(), a.Features.xhr = !!a.Request;
        var l = (Function.attempt(function() {
            return navigator.plugins["Shockwave Flash"].description
        }, function() {
            return new ActiveXObject("ShockwaveFlash.ShockwaveFlash").GetVariable("$version")
        }) || "0 r0").match(/\d+/g);
        if (a.Plugins.Flash = {
                version: Number(l[0] || "0." + l[1]) || 0,
                build: Number(l[2]) || 0
            }, a.exec = function(n) {
                if (!n) return n;
                if (t.execScript) t.execScript(n);
                else {
                    var i = e.createElement("script");
                    i.setAttribute("type", "text/javascript"), i.text = n, e.head.appendChild(i), e.head.removeChild(i)
                }
                return n
            }, String.implement("stripScripts", function(e) {
                var t = "",
                    n = this.replace(/<script[^>]*>([\s\S]*?)<\/script>/gi, function(e, n) {
                        return t += n + "\n", ""
                    });
                return e === !0 ? a.exec(t) : "function" == typeOf(e) && e(t, n),
                    n
            }), a.extend({
                Document: this.Document,
                Window: this.Window,
                Element: this.Element,
                Event: this.Event
            }), this.Window = this.$constructor = new Type("Window", function() {}), this.$family = Function.from("window").hide(), Window.mirror(function(e, n) {
                t[e] = n
            }), this.Document = e.$constructor = new Type("Document", function() {}), e.$family = Function.from("document").hide(), Document.mirror(function(t, n) {
                e[t] = n
            }), e.html = e.documentElement, e.head || (e.head = e.getElementsByTagName("head")[0]), e.execCommand) try {
            e.execCommand("BackgroundImageCache", !1, !0)
        } catch (c) {}
        if (this.attachEvent && !this.addEventListener) {
            var u = function() {
                this.detachEvent("onunload", u), e.head = e.html = e.window = null
            };
            this.attachEvent("onunload", u)
        }
        var d = Array.from;
        try {
            d(e.html.childNodes)
        } catch (c) {
            Array.from = function(e) {
                if ("string" != typeof e && Type.isEnumerable(e) && "array" != typeOf(e)) {
                    for (var t = e.length, n = new Array(t); t--;) n[t] = e[t];
                    return n
                }
                return d(e)
            };
            var h = Array.prototype,
                m = h.slice;
            ["pop", "push", "reverse", "shift", "sort", "splice", "unshift", "concat", "join", "slice"].each(function(e) {
                var t = h[e];
                Array[e] = function(e) {
                    return t.apply(Array.from(e), m.call(arguments, 1))
                }
            })
        }
        a.Platform.ios && (a.Platform.ipod = !0), a.Engine = {};
        var f = function(e, t) {
            a.Engine.name = e, a.Engine[e + t] = !0, a.Engine.version = t
        };
        if (a.ie) switch (a.Engine.trident = !0, a.version) {
            case 6:
                f("trident", 4);
                break;
            case 7:
                f("trident", 5);
                break;
            case 8:
                f("trident", 6)
        }
        if (a.firefox && (a.Engine.gecko = !0, a.version >= 3 ? f("gecko", 19) : f("gecko", 18)), a.safari || a.chrome) switch (a.Engine.webkit = !0, a.version) {
            case 2:
                f("webkit", 419);
                break;
            case 3:
                f("webkit", 420);
                break;
            case 4:
                f("webkit", 525)
        }
        if (a.opera && (a.Engine.presto = !0, a.version >= 9.6 ? f("presto", 960) : a.version >= 9.5 ? f("presto", 950) : f("presto", 925)), "unknown" == a.name) switch ((i.match(/(?:webkit|khtml|gecko)/) || [])[0]) {
            case "webkit":
            case "khtml":
                a.Engine.webkit = !0;
                break;
            case "gecko":
                a.Engine.gecko = !0
        }
        this.$exec = a.exec
    }(),
    function() {
        var e = {},
            t = this.DOMEvent = new Type("DOMEvent", function(t, n) {
                if (n || (n = window), t = t || n.event, t.$extended) return t;
                this.event = t, this.$extended = !0, this.shift = t.shiftKey, this.control = t.ctrlKey, this.alt = t.altKey, this.meta = t.metaKey;
                for (var i = this.type = t.type, s = t.target || t.srcElement; s && 3 == s.nodeType;) s = s.parentNode;
                if (this.target = document.id(s), 0 == i.indexOf("key")) {
                    var r = this.code = t.which || t.keyCode;
                    this.key = e[r] || Object.keyOf(Event.Keys, r), "keydown" == i && (r > 111 && 124 > r ? this.key = "f" + (r - 111) : r > 95 && 106 > r && (this.key = r - 96)), null == this.key && (this.key = String.fromCharCode(r).toLowerCase())
                } else if ("click" == i || "dblclick" == i || "contextmenu" == i || "DOMMouseScroll" == i || 0 == i.indexOf("mouse")) {
                    var o = n.document;
                    if (o = o.compatMode && "CSS1Compat" != o.compatMode ? o.body : o.html, this.page = {
                            x: null != t.pageX ? t.pageX : t.clientX + o.scrollLeft,
                            y: null != t.pageY ? t.pageY : t.clientY + o.scrollTop
                        }, this.client = {
                            x: null != t.pageX ? t.pageX - n.pageXOffset : t.clientX,
                            y: null != t.pageY ? t.pageY - n.pageYOffset : t.clientY
                        }, ("DOMMouseScroll" == i || "mousewheel" == i) && (this.wheel = t.wheelDelta ? t.wheelDelta / 120 : -(t.detail || 0) / 3), this.rightClick = 3 == t.which || 2 == t.button, "mouseover" == i || "mouseout" == i) {
                        for (var a = t.relatedTarget || t[("mouseover" == i ? "from" : "to") + "Element"]; a && 3 == a.nodeType;) a = a.parentNode;
                        this.relatedTarget = document.id(a)
                    }
                } else if (0 == i.indexOf("touch") || 0 == i.indexOf("gesture")) {
                    this.rotation = t.rotation, this.scale = t.scale, this.targetTouches = t.targetTouches, this.changedTouches = t.changedTouches;
                    var l = this.touches = t.touches;
                    if (l && l[0]) {
                        var c = l[0];
                        this.page = {
                            x: c.pageX,
                            y: c.pageY
                        }, this.client = {
                            x: c.clientX,
                            y: c.clientY
                        }
                    }
                }
                this.client || (this.client = {}), this.page || (this.page = {})
            });
        t.implement({
            stop: function() {
                return this.preventDefault().stopPropagation()
            },
            stopPropagation: function() {
                return this.event.stopPropagation ? this.event.stopPropagation() : this.event.cancelBubble = !0, this
            },
            preventDefault: function() {
                return this.event.preventDefault ? this.event.preventDefault() : this.event.returnValue = !1, this
            }
        }), t.defineKey = function(t, n) {
            return e[t] = n, this
        }, t.defineKeys = t.defineKey.overloadSetter(!0), t.defineKeys({
            38: "up",
            40: "down",
            37: "left",
            39: "right",
            27: "esc",
            32: "space",
            8: "backspace",
            9: "tab",
            46: "delete",
            13: "enter"
        })
    }();
var Event = DOMEvent;
Event.Keys = {}, Event.Keys = new Hash(Event.Keys),
    function() {
        var e = this.Class = new Type("Class", function(i) {
                instanceOf(i, Function) && (i = {
                    initialize: i
                });
                var s = function() {
                    if (n(this), s.$prototyping) return this;
                    this.$caller = null;
                    var e = this.initialize ? this.initialize.apply(this, arguments) : this;
                    return this.$caller = this.caller = null, e
                }.extend(this).implement(i);
                return s.$constructor = e, s.prototype.$constructor = s, s.prototype.parent = t, s
            }),
            t = function() {
                if (!this.$caller) throw new Error('The method "parent" cannot be called.');
                var e = this.$caller.$name,
                    t = this.$caller.$owner.parent,
                    n = t ? t.prototype[e] : null;
                if (!n) throw new Error('The method "' + e + '" has no parent.');
                return n.apply(this, arguments)
            },
            n = function(e) {
                for (var t in e) {
                    var i = e[t];
                    switch (typeOf(i)) {
                        case "object":
                            var s = function() {};
                            s.prototype = i, e[t] = n(new s);
                            break;
                        case "array":
                            e[t] = i.clone()
                    }
                }
                return e
            },
            i = function(e, t, n) {
                n.$origin && (n = n.$origin);
                var i = function() {
                    if (n.$protected && null == this.$caller) throw new Error('The method "' + t + '" cannot be called.');
                    var e = this.caller,
                        s = this.$caller;
                    this.caller = s, this.$caller = i;
                    var r = n.apply(this, arguments);
                    return this.$caller = s, this.caller = e, r
                }.extend({
                    $owner: e,
                    $origin: n,
                    $name: t
                });
                return i
            },
            s = function(t, n, s) {
                if (e.Mutators.hasOwnProperty(t) && (n = e.Mutators[t].call(this, n), null == n)) return this;
                if ("function" == typeOf(n)) {
                    if (n.$hidden) return this;
                    this.prototype[t] = s ? n : i(this, t, n)
                } else Object.merge(this.prototype, t, n);
                return this
            },
            r = function(e) {
                e.$prototyping = !0;
                var t = new e;
                return delete e.$prototyping, t
            };
        e.implement("implement", s.overloadSetter()), e.Mutators = {
            Extends: function(e) {
                this.parent = e, this.prototype = r(e)
            },
            Implements: function(e) {
                Array.from(e).each(function(e) {
                    var t = new e;
                    for (var n in t) s.call(this, n, t[n], !0)
                }, this)
            }
        }
    }(),
    function() {
        this.Chain = new Class({
            $chain: [],
            chain: function() {
                return this.$chain.append(Array.flatten(arguments)), this
            },
            callChain: function() {
                return this.$chain.length ? this.$chain.shift().apply(this, arguments) : !1
            },
            clearChain: function() {
                return this.$chain.empty(), this
            }
        });
        var e = function(e) {
            return e.replace(/^on([A-Z])/, function(e, t) {
                return t.toLowerCase()
            })
        };
        this.Events = new Class({
            $events: {},
            addEvent: function(t, n, i) {
                return t = e(t), n == $empty ? this : (this.$events[t] = (this.$events[t] || []).include(n), i && (n.internal = !0), this)
            },
            addEvents: function(e) {
                for (var t in e) this.addEvent(t, e[t]);
                return this
            },
            fireEvent: function(t, n, i) {
                t = e(t);
                var s = this.$events[t];
                return s ? (n = Array.from(n), s.each(function(e) {
                    i ? e.delay(i, this, n) : e.apply(this, n)
                }, this), this) : this
            },
            removeEvent: function(t, n) {
                t = e(t);
                var i = this.$events[t];
                if (i && !n.internal) {
                    var s = i.indexOf(n); - 1 != s && delete i[s]
                }
                return this
            },
            removeEvents: function(t) {
                var n;
                if ("object" == typeOf(t)) {
                    for (n in t) this.removeEvent(n, t[n]);
                    return this
                }
                t && (t = e(t));
                for (n in this.$events)
                    if (!t || t == n)
                        for (var i = this.$events[n], s = i.length; s--;) s in i && this.removeEvent(n, i[s]);
                return this
            }
        }), this.Options = new Class({
            setOptions: function() {
                var e = this.options = Object.merge.apply(null, [{}, this.options].append(arguments));
                if (this.addEvent)
                    for (var t in e) "function" == typeOf(e[t]) && /^on[A-Z]/.test(t) && (this.addEvent(t, e[t]), delete e[t]);
                return this
            }
        })
    }(),
    function() {
        function e(e, r, o, l, u, h, m, f, p, g, v, y, w, b, _, x) {
            if ((r || -1 === n) && (t.expressions[++n] = [], i = -1, r)) return "";
            if (o || l || -1 === i) {
                o = o || " ";
                var E = t.expressions[n];
                s && E[i] && (E[i].reverseCombinator = c(o)), E[++i] = {
                    combinator: o,
                    tag: "*"
                }
            }
            var S = t.expressions[n][i];
            if (u) S.tag = u.replace(a, "");
            else if (h) S.id = h.replace(a, "");
            else if (m) m = m.replace(a, ""), S.classList || (S.classList = []), S.classes || (S.classes = []), S.classList.push(m), S.classes.push({
                value: m,
                regexp: new RegExp("(^|\\s)" + d(m) + "(\\s|$)")
            });
            else if (w) x = x || _, x = x ? x.replace(a, "") : null, S.pseudos || (S.pseudos = []), S.pseudos.push({
                key: w.replace(a, ""),
                value: x,
                type: 1 == y.length ? "class" : "element"
            });
            else if (f) {
                f = f.replace(a, ""), v = (v || "").replace(a, "");
                var C, k;
                switch (p) {
                    case "^=":
                        k = new RegExp("^" + d(v));
                        break;
                    case "$=":
                        k = new RegExp(d(v) + "$");
                        break;
                    case "~=":
                        k = new RegExp("(^|\\s)" + d(v) + "(\\s|$)");
                        break;
                    case "|=":
                        k = new RegExp("^" + d(v) + "(-|$)");
                        break;
                    case "=":
                        C = function(e) {
                            return v == e
                        };
                        break;
                    case "*=":
                        C = function(e) {
                            return e && e.indexOf(v) > -1
                        };
                        break;
                    case "!=":
                        C = function(e) {
                            return v != e
                        };
                        break;
                    default:
                        C = function(e) {
                            return !!e
                        }
                }
                "" == v && /^[*$^]=$/.test(p) && (C = function() {
                    return !1
                }), C || (C = function(e) {
                    return e && k.test(e)
                }), S.attributes || (S.attributes = []), S.attributes.push({
                    key: f,
                    operator: p,
                    value: v,
                    test: C
                })
            }
            return ""
        }
        var t, n, i, s, r = {},
            o = {},
            a = /\\/g,
            l = function(i, a) {
                if (null == i) return null;
                if (i.Slick === !0) return i;
                i = ("" + i).replace(/^\s+|\s+$/g, ""), s = !!a;
                var c = s ? o : r;
                if (c[i]) return c[i];
                for (t = {
                        Slick: !0,
                        expressions: [],
                        raw: i,
                        reverse: function() {
                            return l(this.raw, !0)
                        }
                    }, n = -1; i != (i = i.replace(h, e)););
                return t.length = t.expressions.length, c[t.raw] = s ? u(t) : t
            },
            c = function(e) {
                return "!" === e ? " " : " " === e ? "!" : /^!/.test(e) ? e.replace(/^!/, "") : "!" + e
            },
            u = function(e) {
                for (var t = e.expressions, n = 0; n < t.length; n++) {
                    for (var i = t[n], s = {
                            parts: [],
                            tag: "*",
                            combinator: c(i[0].combinator)
                        }, r = 0; r < i.length; r++) {
                        var o = i[r];
                        o.reverseCombinator || (o.reverseCombinator = " "), o.combinator = o.reverseCombinator, delete o.reverseCombinator
                    }
                    i.reverse().push(s)
                }
                return e
            },
            d = function(e) {
                return e.replace(/[-[\]{}()*+?.\\^$|,#\s]/g, function(e) {
                    return "\\" + e
                })
            },
            h = new RegExp("^(?:\\s*(,)\\s*|\\s*(<combinator>+)\\s*|(\\s+)|(<unicode>+|\\*)|\\#(<unicode>+)|\\.(<unicode>+)|\\[\\s*(<unicode1>+)(?:\\s*([*^$!~|]?=)(?:\\s*(?:([\"']?)(.*?)\\9)))?\\s*\\](?!\\])|(:+)(<unicode>+)(?:\\((?:(?:([\"'])([^\\13]*)\\13)|((?:\\([^)]+\\)|[^()]*)+))\\))?)".replace(/<combinator>/, "[" + d(">+~`!@$%^&={}\\;</") + "]").replace(/<unicode>/g, "(?:[\\w\\u00a1-\\uFFFF-]|\\\\[^\\s0-9a-f])").replace(/<unicode1>/g, "(?:[:\\w\\u00a1-\\uFFFF-]|\\\\[^\\s0-9a-f])")),
            m = this.Slick || {};
        m.parse = function(e) {
            return l(e)
        }, m.escapeRegExp = d, this.Slick || (this.Slick = m)
    }.apply("undefined" != typeof exports ? exports : this),
    function() {
        var e = {},
            t = {},
            n = Object.prototype.toString;
        e.isNativeCode = function(e) {
            return /\{\s*\[native code\]\s*\}/.test("" + e)
        }, e.isXML = function(e) {
            return !!e.xmlVersion || !!e.xml || "[object XMLDocument]" == n.call(e) || 9 == e.nodeType && "HTML" != e.documentElement.nodeName
        }, e.setDocument = function(e) {
            var n = e.nodeType;
            if (9 == n);
            else if (n) e = e.ownerDocument;
            else {
                if (!e.navigator) return;
                e = e.document
            }
            if (this.document !== e) {
                this.document = e;
                var i, s = e.documentElement,
                    r = this.getUIDXML(s),
                    o = t[r];
                if (o)
                    for (i in o) this[i] = o[i];
                else {
                    o = t[r] = {}, o.root = s, o.isXMLDocument = this.isXML(e), o.brokenStarGEBTN = o.starSelectsClosedQSA = o.idGetsName = o.brokenMixedCaseQSA = o.brokenGEBCN = o.brokenCheckedQSA = o.brokenEmptyAttributeQSA = o.isHTMLDocument = o.nativeMatchesSelector = !1;
                    var a, l, c, u, d, h, m = "slick_uniqueid",
                        f = e.createElement("div"),
                        p = e.body || e.getElementsByTagName("body")[0] || s;
                    p.appendChild(f);
                    try {
                        f.innerHTML = '<a id="' + m + '"></a>', o.isHTMLDocument = !!e.getElementById(m)
                    } catch (g) {}
                    if (o.isHTMLDocument) {
                        f.style.display = "none", f.appendChild(e.createComment("")), l = f.getElementsByTagName("*").length > 1;
                        try {
                            f.innerHTML = "foo</foo>", h = f.getElementsByTagName("*"), a = h && !!h.length && "/" == h[0].nodeName.charAt(0)
                        } catch (g) {}
                        o.brokenStarGEBTN = l || a;
                        try {
                            f.innerHTML = '<a name="' + m + '"></a><b id="' + m + '"></b>', o.idGetsName = e.getElementById(m) === f.firstChild
                        } catch (g) {}
                        if (f.getElementsByClassName) {
                            try {
                                f.innerHTML = '<a class="f"></a><a class="b"></a>', f.getElementsByClassName("b").length, f.firstChild.className = "b", u = 2 != f.getElementsByClassName("b").length
                            } catch (g) {}
                            try {
                                f.innerHTML = '<a class="a"></a><a class="f b a"></a>', c = 2 != f.getElementsByClassName("a").length
                            } catch (g) {}
                            o.brokenGEBCN = u || c
                        }
                        if (f.querySelectorAll) {
                            try {
                                f.innerHTML = "foo</foo>", h = f.querySelectorAll("*"), o.starSelectsClosedQSA = h && !!h.length && "/" == h[0].nodeName.charAt(0)
                            } catch (g) {}
                            try {
                                f.innerHTML = '<a class="MiX"></a>', o.brokenMixedCaseQSA = !f.querySelectorAll(".MiX").length
                            } catch (g) {}
                            try {
                                f.innerHTML = '<select><option selected="selected">a</option></select>', o.brokenCheckedQSA = 0 == f.querySelectorAll(":checked").length
                            } catch (g) {}
                            try {
                                f.innerHTML = '<a class=""></a>', o.brokenEmptyAttributeQSA = 0 != f.querySelectorAll('[class*=""]').length
                            } catch (g) {}
                        }
                        try {
                            f.innerHTML = '<form action="s"><input id="action"/></form>', d = "s" != f.firstChild.getAttribute("action")
                        } catch (g) {}
                        if (o.nativeMatchesSelector = s.matchesSelector || s.mozMatchesSelector || s.webkitMatchesSelector, o.nativeMatchesSelector) try {
                            o.nativeMatchesSelector.call(s, ":slick"), o.nativeMatchesSelector = null
                        } catch (g) {}
                    }
                    try {
                        s.slick_expando = 1, delete s.slick_expando, o.getUID = this.getUIDHTML
                    } catch (g) {
                        o.getUID = this.getUIDXML
                    }
                    p.removeChild(f), f = h = p = null, o.getAttribute = o.isHTMLDocument && d ? function(e, t) {
                        var n = this.attributeGetters[t];
                        if (n) return n.call(e);
                        var i = e.getAttributeNode(t);
                        return i ? i.nodeValue : null
                    } : function(e, t) {
                        var n = this.attributeGetters[t];
                        return n ? n.call(e) : e.getAttribute(t)
                    }, o.hasAttribute = s && this.isNativeCode(s.hasAttribute) ? function(e, t) {
                        return e.hasAttribute(t)
                    } : function(e, t) {
                        return e = e.getAttributeNode(t), !(!e || !e.specified && !e.nodeValue)
                    }, o.contains = s && this.isNativeCode(s.contains) ? function(e, t) {
                        return e.contains(t)
                    } : s && s.compareDocumentPosition ? function(e, t) {
                        return e === t || !!(16 & e.compareDocumentPosition(t))
                    } : function(e, t) {
                        if (t)
                            do
                                if (t === e) return !0;
                        while (t = t.parentNode);
                        return !1
                    }, o.documentSorter = s.compareDocumentPosition ? function(e, t) {
                        return e.compareDocumentPosition && t.compareDocumentPosition ? 4 & e.compareDocumentPosition(t) ? -1 : e === t ? 0 : 1 : 0
                    } : "sourceIndex" in s ? function(e, t) {
                        return e.sourceIndex && t.sourceIndex ? e.sourceIndex - t.sourceIndex : 0
                    } : e.createRange ? function(e, t) {
                        if (!e.ownerDocument || !t.ownerDocument) return 0;
                        var n = e.ownerDocument.createRange(),
                            i = t.ownerDocument.createRange();
                        return n.setStart(e, 0), n.setEnd(e, 0), i.setStart(t, 0), i.setEnd(t, 0), n.compareBoundaryPoints(Range.START_TO_END, i)
                    } : null, s = null;
                    for (i in o) this[i] = o[i]
                }
            }
        };
        var i = /^([#.]?)((?:[\w-]+|\*))$/,
            s = /\[.+[*$^]=(?:""|'')?\]/,
            r = {};
        e.search = function(e, t, n, o) {
            var a = this.found = o ? null : n || [];
            if (!e) return a;
            if (e.navigator) e = e.document;
            else if (!e.nodeType) return a;
            var l, c, u = this.uniques = {},
                h = !(!n || !n.length),
                m = 9 == e.nodeType;
            if (this.document !== (m ? e : e.ownerDocument) && this.setDocument(e), h)
                for (c = a.length; c--;) u[this.getUID(a[c])] = !0;
            if ("string" == typeof t) {
                var f = t.match(i);
                e: if (f) {
                    var p, g, v = f[1],
                        y = f[2];
                    if (v) {
                        if ("#" == v) {
                            if (!this.isHTMLDocument || !m) break e;
                            if (p = e.getElementById(y), !p) return a;
                            if (this.idGetsName && p.getAttributeNode("id").nodeValue != y) break e;
                            if (o) return p || null;
                            h && u[this.getUID(p)] || a.push(p)
                        } else if ("." == v) {
                            if (!this.isHTMLDocument || (!e.getElementsByClassName || this.brokenGEBCN) && e.querySelectorAll) break e;
                            if (e.getElementsByClassName && !this.brokenGEBCN) {
                                if (g = e.getElementsByClassName(y), o) return g[0] || null;
                                for (c = 0; p = g[c++];) h && u[this.getUID(p)] || a.push(p)
                            } else {
                                var w = new RegExp("(^|\\s)" + d.escapeRegExp(y) + "(\\s|$)");
                                for (g = e.getElementsByTagName("*"), c = 0; p = g[c++];)
                                    if (className = p.className, className && w.test(className)) {
                                        if (o) return p;
                                        h && u[this.getUID(p)] || a.push(p)
                                    }
                            }
                        }
                    } else {
                        if ("*" == y && this.brokenStarGEBTN) break e;
                        if (g = e.getElementsByTagName(y), o) return g[0] || null;
                        for (c = 0; p = g[c++];) h && u[this.getUID(p)] || a.push(p)
                    }
                    return h && this.sort(a), o ? null : a
                }
                e: if (e.querySelectorAll) {
                    if (!this.isHTMLDocument || r[t] || this.brokenMixedCaseQSA || this.brokenCheckedQSA && t.indexOf(":checked") > -1 || this.brokenEmptyAttributeQSA && s.test(t) || !m && t.indexOf(",") > -1 || d.disableQSA) break e;
                    var b = t,
                        _ = e;
                    if (!m) {
                        var x = _.getAttribute("id"),
                            E = "slickid__";
                        _.setAttribute("id", E), b = "#" + E + " " + b, e = _.parentNode
                    }
                    try {
                        if (o) return e.querySelector(b) || null;
                        g = e.querySelectorAll(b)
                    } catch (S) {
                        r[t] = 1;
                        break e
                    } finally {
                        m || (x ? _.setAttribute("id", x) : _.removeAttribute("id"), e = _)
                    }
                    if (this.starSelectsClosedQSA)
                        for (c = 0; p = g[c++];) !(p.nodeName > "@") || h && u[this.getUID(p)] || a.push(p);
                    else
                        for (c = 0; p = g[c++];) h && u[this.getUID(p)] || a.push(p);
                    return h && this.sort(a), a
                }
                if (l = this.Slick.parse(t), !l.length) return a
            } else {
                if (null == t) return a;
                if (!t.Slick) return this.contains(e.documentElement || e, t) ? (a ? a.push(t) : a = t, a) : a;
                l = t
            }
            this.posNTH = {}, this.posNTHLast = {}, this.posNTHType = {}, this.posNTHTypeLast = {}, this.push = !h && (o || 1 == l.length && 1 == l.expressions[0].length) ? this.pushArray : this.pushUID, null == a && (a = []);
            var C, k, j, T, P, O, $, A, D, N, L, F, I, M, B = l.expressions;
            e: for (c = 0; F = B[c]; c++)
                for (C = 0; I = F[C]; C++) {
                    if (T = "combinator:" + I.combinator, !this[T]) continue e;
                    if (P = this.isXMLDocument ? I.tag : I.tag.toUpperCase(), O = I.id, $ = I.classList, A = I.classes, D = I.attributes, N = I.pseudos, M = C === F.length - 1, this.bitUniques = {}, M ? (this.uniques = u, this.found = a) : (this.uniques = {}, this.found = []), 0 === C) {
                        if (this[T](e, P, O, A, D, N, $), o && M && a.length) break e
                    } else if (o && M) {
                        for (k = 0, j = L.length; j > k; k++)
                            if (this[T](L[k], P, O, A, D, N, $), a.length) break e
                    } else
                        for (k = 0, j = L.length; j > k; k++) this[T](L[k], P, O, A, D, N, $);
                    L = this.found
                }
            return (h || l.expressions.length > 1) && this.sort(a), o ? a[0] || null : a
        }, e.uidx = 1, e.uidk = "slick-uniqueid", e.getUIDXML = function(e) {
            var t = e.getAttribute(this.uidk);
            return t || (t = this.uidx++, e.setAttribute(this.uidk, t)), t
        }, e.getUIDHTML = function(e) {
            return e.uniqueNumber || (e.uniqueNumber = this.uidx++)
        }, e.sort = function(e) {
            return this.documentSorter ? (e.sort(this.documentSorter), e) : e
        }, e.cacheNTH = {}, e.matchNTH = /^([+-]?\d*)?([a-z]+)?([+-]\d+)?$/, e.parseNTHArgument = function(e) {
            var t = e.match(this.matchNTH);
            if (!t) return !1;
            var n = t[2] || !1,
                i = t[1] || 1;
            "-" == i && (i = -1);
            var s = +t[3] || 0;
            return t = "n" == n ? {
                a: i,
                b: s
            } : "odd" == n ? {
                a: 2,
                b: 1
            } : "even" == n ? {
                a: 2,
                b: 0
            } : {
                a: 0,
                b: i
            }, this.cacheNTH[e] = t
        }, e.createNTHPseudo = function(e, t, n, i) {
            return function(s, r) {
                var o = this.getUID(s);
                if (!this[n][o]) {
                    var a = s.parentNode;
                    if (!a) return !1;
                    var l = a[e],
                        c = 1;
                    if (i) {
                        var u = s.nodeName;
                        do l.nodeName == u && (this[n][this.getUID(l)] = c++); while (l = l[t])
                    } else
                        do 1 == l.nodeType && (this[n][this.getUID(l)] = c++); while (l = l[t])
                }
                r = r || "n";
                var d = this.cacheNTH[r] || this.parseNTHArgument(r);
                if (!d) return !1;
                var h = d.a,
                    m = d.b,
                    f = this[n][o];
                if (0 == h) return m == f;
                if (h > 0) {
                    if (m > f) return !1
                } else if (f > m) return !1;
                return (f - m) % h == 0
            }
        }, e.pushArray = function(e, t, n, i, s, r) {
            this.matchSelector(e, t, n, i, s, r) && this.found.push(e)
        }, e.pushUID = function(e, t, n, i, s, r) {
            var o = this.getUID(e);
            !this.uniques[o] && this.matchSelector(e, t, n, i, s, r) && (this.uniques[o] = !0, this.found.push(e))
        }, e.matchNode = function(e, t) {
            if (this.isHTMLDocument && this.nativeMatchesSelector) try {
                return this.nativeMatchesSelector.call(e, t.replace(/\[([^=]+)=\s*([^'"\]]+?)\s*\]/g, '[$1="$2"]'))
            } catch (n) {}
            var i = this.Slick.parse(t);
            if (!i) return !0;
            var s, r = i.expressions,
                o = 0;
            for (s = 0; currentExpression = r[s]; s++)
                if (1 == currentExpression.length) {
                    var a = currentExpression[0];
                    if (this.matchSelector(e, this.isXMLDocument ? a.tag : a.tag.toUpperCase(), a.id, a.classes, a.attributes, a.pseudos)) return !0;
                    o++
                }
            if (o == i.length) return !1;
            var l, c = this.search(this.document, i);
            for (s = 0; l = c[s++];)
                if (l === e) return !0;
            return !1
        }, e.matchPseudo = function(e, t, n) {
            var i = "pseudo:" + t;
            if (this[i]) return this[i](e, n);
            var s = this.getAttribute(e, t);
            return n ? n == s : !!s
        }, e.matchSelector = function(e, t, n, i, s, r) {
            if (t) {
                var o = this.isXMLDocument ? e.nodeName : e.nodeName.toUpperCase();
                if ("*" == t) {
                    if ("@" > o) return !1
                } else if (o != t) return !1
            }
            if (n && e.getAttribute("id") != n) return !1;
            var a, l, c;
            if (i)
                for (a = i.length; a--;)
                    if (c = e.getAttribute("class") || e.className, !c || !i[a].regexp.test(c)) return !1;
            if (s)
                for (a = s.length; a--;)
                    if (l = s[a], l.operator ? !l.test(this.getAttribute(e, l.key)) : !this.hasAttribute(e, l.key)) return !1;
            if (r)
                for (a = r.length; a--;)
                    if (l = r[a], !this.matchPseudo(e, l.key, l.value)) return !1;
            return !0
        };
        var o = {
            " ": function(e, t, n, i, s, r, o) {
                var a, l, c;
                if (this.isHTMLDocument) {
                    e: if (n) {
                        if (l = this.document.getElementById(n), !l && e.all || this.idGetsName && l && l.getAttributeNode("id").nodeValue != n) {
                            if (c = e.all[n], !c) return;
                            for (c[0] || (c = [c]), a = 0; l = c[a++];) {
                                var u = l.getAttributeNode("id");
                                if (u && u.nodeValue == n) {
                                    this.push(l, t, null, i, s, r);
                                    break
                                }
                            }
                            return
                        }
                        if (!l) {
                            if (this.contains(this.root, e)) return;
                            break e
                        }
                        if (this.document !== e && !this.contains(e, l)) return;
                        return void this.push(l, t, null, i, s, r)
                    }e: if (i && e.getElementsByClassName && !this.brokenGEBCN) {
                        if (c = e.getElementsByClassName(o.join(" ")), !c || !c.length) break e;
                        for (a = 0; l = c[a++];) this.push(l, t, n, null, s, r);
                        return
                    }
                }
                if (c = e.getElementsByTagName(t), c && c.length)
                    for (this.brokenStarGEBTN || (t = null), a = 0; l = c[a++];) this.push(l, t, n, i, s, r)
            },
            ">": function(e, t, n, i, s, r) {
                if (e = e.firstChild)
                    do 1 == e.nodeType && this.push(e, t, n, i, s, r); while (e = e.nextSibling)
            },
            "+": function(e, t, n, i, s, r) {
                for (; e = e.nextSibling;)
                    if (1 == e.nodeType) {
                        this.push(e, t, n, i, s, r);
                        break
                    }
            },
            "^": function(e, t, n, i, s, r) {
                e = e.firstChild, e && (1 == e.nodeType ? this.push(e, t, n, i, s, r) : this["combinator:+"](e, t, n, i, s, r))
            },
            "~": function(e, t, n, i, s, r) {
                for (; e = e.nextSibling;)
                    if (1 == e.nodeType) {
                        var o = this.getUID(e);
                        if (this.bitUniques[o]) break;
                        this.bitUniques[o] = !0, this.push(e, t, n, i, s, r)
                    }
            },
            "++": function(e, t, n, i, s, r) {
                this["combinator:+"](e, t, n, i, s, r), this["combinator:!+"](e, t, n, i, s, r)
            },
            "~~": function(e, t, n, i, s, r) {
                this["combinator:~"](e, t, n, i, s, r), this["combinator:!~"](e, t, n, i, s, r)
            },
            "!": function(e, t, n, i, s, r) {
                for (; e = e.parentNode;) e !== this.document && this.push(e, t, n, i, s, r)
            },
            "!>": function(e, t, n, i, s, r) {
                e = e.parentNode, e !== this.document && this.push(e, t, n, i, s, r)
            },
            "!+": function(e, t, n, i, s, r) {
                for (; e = e.previousSibling;)
                    if (1 == e.nodeType) {
                        this.push(e, t, n, i, s, r);
                        break
                    }
            },
            "!^": function(e, t, n, i, s, r) {
                e = e.lastChild, e && (1 == e.nodeType ? this.push(e, t, n, i, s, r) : this["combinator:!+"](e, t, n, i, s, r))
            },
            "!~": function(e, t, n, i, s, r) {
                for (; e = e.previousSibling;)
                    if (1 == e.nodeType) {
                        var o = this.getUID(e);
                        if (this.bitUniques[o]) break;
                        this.bitUniques[o] = !0, this.push(e, t, n, i, s, r)
                    }
            }
        };
        for (var a in o) e["combinator:" + a] = o[a];
        var l = {
            empty: function(e) {
                var t = e.firstChild;
                return !(t && 1 == t.nodeType || (e.innerText || e.textContent || "").length)
            },
            not: function(e, t) {
                return !this.matchNode(e, t)
            },
            contains: function(e, t) {
                return (e.innerText || e.textContent || "").indexOf(t) > -1
            },
            "first-child": function(e) {
                for (; e = e.previousSibling;)
                    if (1 == e.nodeType) return !1;
                return !0
            },
            "last-child": function(e) {
                for (; e = e.nextSibling;)
                    if (1 == e.nodeType) return !1;
                return !0
            },
            "only-child": function(e) {
                for (var t = e; t = t.previousSibling;)
                    if (1 == t.nodeType) return !1;
                for (var n = e; n = n.nextSibling;)
                    if (1 == n.nodeType) return !1;
                return !0
            },
            "nth-child": e.createNTHPseudo("firstChild", "nextSibling", "posNTH"),
            "nth-last-child": e.createNTHPseudo("lastChild", "previousSibling", "posNTHLast"),
            "nth-of-type": e.createNTHPseudo("firstChild", "nextSibling", "posNTHType", !0),
            "nth-last-of-type": e.createNTHPseudo("lastChild", "previousSibling", "posNTHTypeLast", !0),
            index: function(e, t) {
                return this["pseudo:nth-child"](e, "" + t + 1)
            },
            even: function(e) {
                return this["pseudo:nth-child"](e, "2n")
            },
            odd: function(e) {
                return this["pseudo:nth-child"](e, "2n+1")
            },
            "first-of-type": function(e) {
                for (var t = e.nodeName; e = e.previousSibling;)
                    if (e.nodeName == t) return !1;
                return !0
            },
            "last-of-type": function(e) {
                for (var t = e.nodeName; e = e.nextSibling;)
                    if (e.nodeName == t) return !1;
                return !0
            },
            "only-of-type": function(e) {
                for (var t = e, n = e.nodeName; t = t.previousSibling;)
                    if (t.nodeName == n) return !1;
                for (var i = e; i = i.nextSibling;)
                    if (i.nodeName == n) return !1;
                return !0
            },
            enabled: function(e) {
                return !e.disabled
            },
            disabled: function(e) {
                return e.disabled
            },
            checked: function(e) {
                return e.checked || e.selected
            },
            focus: function(e) {
                return this.isHTMLDocument && this.document.activeElement === e && (e.href || e.type || this.hasAttribute(e, "tabindex"))
            },
            root: function(e) {
                return e === this.root
            },
            selected: function(e) {
                return e.selected
            }
        };
        for (var c in l) e["pseudo:" + c] = l[c];
        var u = e.attributeGetters = {
            "class": function() {
                return this.getAttribute("class") || this.className
            },
            "for": function() {
                return "htmlFor" in this ? this.htmlFor : this.getAttribute("for")
            },
            href: function() {
                return "href" in this ? this.getAttribute("href", 2) : this.getAttribute("href")
            },
            style: function() {
                return this.style ? this.style.cssText : this.getAttribute("style")
            },
            tabindex: function() {
                var e = this.getAttributeNode("tabindex");
                return e && e.specified ? e.nodeValue : null
            },
            type: function() {
                return this.getAttribute("type")
            },
            maxlength: function() {
                var e = this.getAttributeNode("maxLength");
                return e && e.specified ? e.nodeValue : null
            }
        };
        u.MAXLENGTH = u.maxLength = u.maxlength;
        var d = e.Slick = this.Slick || {};
        d.version = "1.1.6", d.search = function(t, n, i) {
            return e.search(t, n, i)
        }, d.find = function(t, n) {
            return e.search(t, n, null, !0)
        }, d.contains = function(t, n) {
            return e.setDocument(t), e.contains(t, n)
        }, d.getAttribute = function(t, n) {
            return e.setDocument(t), e.getAttribute(t, n)
        }, d.hasAttribute = function(t, n) {
            return e.setDocument(t), e.hasAttribute(t, n)
        }, d.match = function(t, n) {
            return t && n ? n && n !== t ? (e.setDocument(t), e.matchNode(t, n)) : !0 : !1
        }, d.defineAttributeGetter = function(t, n) {
            return e.attributeGetters[t] = n, this
        }, d.lookupAttributeGetter = function(t) {
            return e.attributeGetters[t]
        }, d.definePseudo = function(t, n) {
            return e["pseudo:" + t] = function(e, t) {
                return n.call(e, t)
            }, this
        }, d.lookupPseudo = function(t) {
            var n = e["pseudo:" + t];
            return n ? function(e) {
                return n.call(this, e)
            } : null
        }, d.override = function(t, n) {
            return e.override(t, n), this
        }, d.isXML = e.isXML, d.uidOf = function(t) {
            return e.getUIDHTML(t)
        }, this.Slick || (this.Slick = d)
    }.apply("undefined" != typeof exports ? exports : this);
var Element = function(e, t) {
    var n = Element.Constructors[e];
    if (n) return n(t);
    if ("string" != typeof e) return document.id(e).set(t);
    if (t || (t = {}), !/^[\w-]+$/.test(e)) {
        var i = Slick.parse(e).expressions[0][0];
        e = "*" == i.tag ? "div" : i.tag, i.id && null == t.id && (t.id = i.id);
        var s = i.attributes;
        if (s)
            for (var r, o = 0, a = s.length; a > o; o++) r = s[o], null == t[r.key] && (null != r.value && "=" == r.operator ? t[r.key] = r.value : r.value || r.operator || (t[r.key] = !0));
        i.classList && null == t["class"] && (t["class"] = i.classList.join(" "))
    }
    return document.newElement(e, t)
};
Browser.Element && (Element.prototype = Browser.Element.prototype), new Type("Element", Element).mirror(function(e) {
    if (!Array.prototype[e]) {
        var t = {};
        t[e] = function() {
            for (var t = [], n = arguments, i = !0, s = 0, r = this.length; r > s; s++) {
                var o = this[s],
                    a = t[s] = o[e].apply(o, n);
                i = i && "element" == typeOf(a)
            }
            return i ? new Elements(t) : t
        }, Elements.implement(t)
    }
}), Browser.Element || (Element.parent = Object, Element.Prototype = {
    $family: Function.from("element").hide()
}, Element.mirror(function(e, t) {
    Element.Prototype[e] = t
})), Element.Constructors = {}, Element.Constructors = new Hash;
var IFrame = new Type("IFrame", function() {
        var e, t = Array.link(arguments, {
                properties: Type.isObject,
                iframe: function(e) {
                    return null != e
                }
            }),
            n = t.properties || {};
        t.iframe && (e = document.id(t.iframe));
        var i = n.onload || function() {};
        delete n.onload, n.id = n.name = [n.id, n.name, e ? e.id || e.name : "IFrame_" + String.uniqueID()].pick(), e = new Element(e || "iframe", n);
        var s = function() {
            i.call(e.contentWindow)
        };
        return window.frames[n.id] ? s() : e.addListener("load", s), e
    }),
    Elements = this.Elements = function(e) {
        if (e && e.length)
            for (var t, n = {}, i = 0; t = e[i++];) {
                var s = Slick.uidOf(t);
                n[s] || (n[s] = !0, this.push(t))
            }
    };
Elements.prototype = {
        length: 0
    }, Elements.parent = Array, new Type("Elements", Elements).implement({
        filter: function(e, t) {
            return e ? new Elements(Array.filter(this, "string" == typeOf(e) ? function(t) {
                return t.match(e)
            } : e, t)) : this
        }.protect(),
        push: function() {
            for (var e = this.length, t = 0, n = arguments.length; n > t; t++) {
                var i = document.id(arguments[t]);
                i && (this[e++] = i)
            }
            return this.length = e
        }.protect(),
        unshift: function() {
            for (var e = [], t = 0, n = arguments.length; n > t; t++) {
                var i = document.id(arguments[t]);
                i && e.push(i)
            }
            return Array.prototype.unshift.apply(this, e)
        }.protect(),
        concat: function() {
            for (var e = new Elements(this), t = 0, n = arguments.length; n > t; t++) {
                var i = arguments[t];
                Type.isEnumerable(i) ? e.append(i) : e.push(i)
            }
            return e
        }.protect(),
        append: function(e) {
            for (var t = 0, n = e.length; n > t; t++) this.push(e[t]);
            return this
        }.protect(),
        empty: function() {
            for (; this.length;) delete this[--this.length];
            return this
        }.protect()
    }), Elements.alias("extend", "append"),
    function() {
        var e = Array.prototype.splice,
            t = {
                0: 0,
                1: 1,
                length: 2
            };
        e.call(t, 1, 1), 1 == t[1] && Elements.implement("splice", function() {
            for (var t = this.length, n = e.apply(this, arguments); t >= this.length;) delete this[t--];
            return n
        }.protect()), Elements.implement(Array.prototype), Array.mirror(Elements);
        var n;
        try {
            var i = document.createElement("<input name=x>");
            n = "x" == i.name
        } catch (s) {}
        var r = function(e) {
            return ("" + e).replace(/&/g, "&amp;").replace(/"/g, "&quot;")
        };
        Document.implement({
            newElement: function(e, t) {
                return t && null != t.checked && (t.defaultChecked = t.checked), n && t && (e = "<" + e, t.name && (e += ' name="' + r(t.name) + '"'), t.type && (e += ' type="' + r(t.type) + '"'), e += ">", delete t.name, delete t.type), this.id(this.createElement(e)).set(t)
            }
        })
    }(), Document.implement({
        newTextNode: function(e) {
            return this.createTextNode(e)
        },
        getDocument: function() {
            return this
        },
        getWindow: function() {
            return this.window
        },
        id: function() {
            var e = {
                string: function(t, n, i) {
                    return t = Slick.find(i, "#" + t.replace(/(\W)/g, "\\$1")), t ? e.element(t, n) : null
                },
                element: function(e, t) {
                    return $uid(e), t || e.$family || /^(?:object|embed)$/i.test(e.tagName) || Object.append(e, Element.Prototype), e
                },
                object: function(t, n, i) {
                    return t.toElement ? e.element(t.toElement(i), n) : null
                }
            };
            return e.textnode = e.whitespace = e.window = e.document = function(e) {
                    return e
                },
                function(t, n, i) {
                    if (t && t.$family && t.uid) return t;
                    var s = typeOf(t);
                    return e[s] ? e[s](t, n, i || document) : null
                }
        }()
    }), null == window.$ && Window.implement("$", function(e, t) {
        return document.id(e, t, this.document)
    }), Window.implement({
        getDocument: function() {
            return this.document
        },
        getWindow: function() {
            return this
        }
    }), [Document, Element].invoke("implement", {
        getElements: function(e) {
            return Slick.search(this, e, new Elements)
        },
        getElement: function(e) {
            return document.id(Slick.find(this, e))
        }
    });
var contains = {
    contains: function(e) {
        return Slick.contains(this, e)
    }
};
document.contains || Document.implement(contains), document.createElement("div").contains || Element.implement(contains), Element.implement("hasChild", function(e) {
        return this !== e && this.contains(e)
    }),
    function(e, t, n) {
        this.Selectors = {};
        var i = this.Selectors.Pseudo = new Hash,
            s = function() {
                for (var e in i) i.hasOwnProperty(e) && (Slick.definePseudo(e, i[e]), delete i[e])
            };
        Slick.search = function(t, n, i) {
            return s(), e.call(this, t, n, i)
        }, Slick.find = function(e, n) {
            return s(), t.call(this, e, n)
        }, Slick.match = function(e, t) {
            return s(), n.call(this, e, t)
        }
    }(Slick.search, Slick.find, Slick.match);
var injectCombinator = function(e, t) {
    if (!e) return t;
    e = Object.clone(Slick.parse(e));
    for (var n = e.expressions, i = n.length; i--;) n[i][0].combinator = t;
    return e
};
Object.forEach({
        getNext: "~",
        getPrevious: "!~",
        getParent: "!"
    }, function(e, t) {
        Element.implement(t, function(t) {
            return this.getElement(injectCombinator(t, e))
        })
    }), Object.forEach({
        getAllNext: "~",
        getAllPrevious: "!~",
        getSiblings: "~~",
        getChildren: ">",
        getParents: "!"
    }, function(e, t) {
        Element.implement(t, function(t) {
            return this.getElements(injectCombinator(t, e))
        })
    }), Element.implement({
        getFirst: function(e) {
            return document.id(Slick.search(this, injectCombinator(e, ">"))[0])
        },
        getLast: function(e) {
            return document.id(Slick.search(this, injectCombinator(e, ">")).getLast())
        },
        getWindow: function() {
            return this.ownerDocument.window
        },
        getDocument: function() {
            return this.ownerDocument
        },
        getElementById: function(e) {
            return document.id(Slick.find(this, "#" + ("" + e).replace(/(\W)/g, "\\$1")))
        },
        match: function(e) {
            return !e || Slick.match(this, e)
        }
    }), null == window.$$ && Window.implement("$$", function(e) {
        var t = new Elements;
        if (1 == arguments.length && "string" == typeof e) return Slick.search(this.document, e, t);
        for (var n = Array.flatten(arguments), i = 0, s = n.length; s > i; i++) {
            var r = n[i];
            switch (typeOf(r)) {
                case "element":
                    t.push(r);
                    break;
                case "string":
                    Slick.search(this.document, r, t)
            }
        }
        return t
    }), null == window.$$ && Window.implement("$$", function(e) {
        if (1 == arguments.length) {
            if ("string" == typeof e) return Slick.search(this.document, e, new Elements);
            if (Type.isEnumerable(e)) return new Elements(e)
        }
        return new Elements(arguments)
    }),
    function() {
        var e = {
            before: function(e, t) {
                var n = t.parentNode;
                n && n.insertBefore(e, t)
            },
            after: function(e, t) {
                var n = t.parentNode;
                n && n.insertBefore(e, t.nextSibling)
            },
            bottom: function(e, t) {
                t.appendChild(e)
            },
            top: function(e, t) {
                t.insertBefore(e, t.firstChild)
            }
        };
        e.inside = e.bottom, Object.each(e, function(e, t) {
            t = t.capitalize();
            var n = {};
            n["inject" + t] = function(t) {
                return e(this, document.id(t, !0)), this
            }, n["grab" + t] = function(t) {
                return e(document.id(t, !0), this), this
            }, Element.implement(n)
        });
        var t = {},
            n = {},
            i = {};
        Array.forEach(["type", "value", "defaultValue", "accessKey", "cellPadding", "cellSpacing", "colSpan", "frameBorder", "readOnly", "rowSpan", "tabIndex", "useMap"], function(e) {
            i[e.toLowerCase()] = e
        }), Object.append(i, {
            html: "innerHTML",
            text: function() {
                var e = document.createElement("div");
                return null == e.textContent ? "innerText" : "textContent"
            }()
        }), Object.forEach(i, function(e, i) {
            n[i] = function(t, n) {
                t[e] = n
            }, t[i] = function(t) {
                return t[e]
            }
        });
        var s = ["compact", "nowrap", "ismap", "declare", "noshade", "checked", "disabled", "readOnly", "multiple", "selected", "noresize", "defer", "defaultChecked", "autofocus", "controls", "autoplay", "loop"],
            r = {};
        Array.forEach(s, function(e) {
            var i = e.toLowerCase();
            r[i] = e, n[i] = function(t, n) {
                t[e] = !!n
            }, t[i] = function(t) {
                return !!t[e]
            }
        }), Object.append(n, {
            "class": function(e, t) {
                "className" in e ? e.className = t : e.setAttribute("class", t)
            },
            "for": function(e, t) {
                "htmlFor" in e ? e.htmlFor = t : e.setAttribute("for", t)
            },
            style: function(e, t) {
                e.style ? e.style.cssText = t : e.setAttribute("style", t)
            }
        }), Element.implement({
            setProperty: function(e, t) {
                var i = e.toLowerCase();
                if (null == t) {
                    if (!r[i]) return this.removeAttribute(e), this;
                    t = !1
                }
                var s = n[i];
                return s ? s(this, t) : this.setAttribute(e, t), this
            },
            setProperties: function(e) {
                for (var t in e) this.setProperty(t, e[t]);
                return this
            },
            getProperty: function(e) {
                var n = t[e.toLowerCase()];
                if (n) return n(this);
                var i = Slick.getAttribute(this, e);
                return i || Slick.hasAttribute(this, e) ? i : null
            },
            getProperties: function() {
                var e = Array.from(arguments);
                return e.map(this.getProperty, this).associate(e)
            },
            removeProperty: function(e) {
                return this.setProperty(e, null)
            },
            removeProperties: function() {
                return Array.each(arguments, this.removeProperty, this), this
            },
            set: function(e, t) {
                var n = Element.Properties[e];
                n && n.set ? n.set.call(this, t) : this.setProperty(e, t)
            }.overloadSetter(),
            get: function(e) {
                var t = Element.Properties[e];
                return t && t.get ? t.get.apply(this) : this.getProperty(e)
            }.overloadGetter(),
            erase: function(e) {
                var t = Element.Properties[e];
                return t && t.erase ? t.erase.apply(this) : this.removeProperty(e), this
            },
            hasClass: function(e) {
                return this.className.clean().contains(e, " ")
            },
            addClass: function(e) {
                return this.hasClass(e) || (this.className = (this.className + " " + e).clean()), this
            },
            removeClass: function(e) {
                return this.className = this.className.replace(new RegExp("(^|\\s)" + e + "(?:\\s|$)"), "$1"), this
            },
            toggleClass: function(e, t) {
                return null == t && (t = !this.hasClass(e)), t ? this.addClass(e) : this.removeClass(e)
            },
            adopt: function() {
                var e, t = this,
                    n = Array.flatten(arguments),
                    i = n.length;
                i > 1 && (t = e = document.createDocumentFragment());
                for (var s = 0; i > s; s++) {
                    var r = document.id(n[s], !0);
                    r && t.appendChild(r)
                }
                return e && this.appendChild(e), this
            },
            appendText: function(e, t) {
                return this.grab(this.getDocument().newTextNode(e), t)
            },
            grab: function(t, n) {
                return e[n || "bottom"](document.id(t, !0), this), this
            },
            inject: function(t, n) {
                return e[n || "bottom"](this, document.id(t, !0)), this
            },
            replaces: function(e) {
                return e = document.id(e, !0), e.parentNode.replaceChild(this, e), this
            },
            wraps: function(e, t) {
                return e = document.id(e, !0), this.replaces(e).grab(e, t)
            },
            getSelected: function() {
                return this.selectedIndex, new Elements(Array.from(this.options).filter(function(e) {
                    return e.selected
                }))
            },
            toQueryString: function() {
                var e = [];
                return this.getElements("input, select, textarea").each(function(t) {
                    var n = t.type;
                    if (t.name && !t.disabled && "submit" != n && "reset" != n && "file" != n && "image" != n) {
                        var i = "select" == t.get("tag") ? t.getSelected().map(function(e) {
                            return document.id(e).get("value")
                        }) : "radio" != n && "checkbox" != n || t.checked ? t.get("value") : null;
                        Array.from(i).each(function(n) {
                            "undefined" != typeof n && e.push(encodeURIComponent(t.name) + "=" + encodeURIComponent(n))
                        })
                    }
                }), e.join("&")
            }
        });
        var o = {},
            a = {},
            l = function(e) {
                return a[e] || (a[e] = {})
            },
            c = function(e) {
                var t = e.uid;
                return e.removeEvents && e.removeEvents(), e.clearAttributes && e.clearAttributes(), null != t && (delete o[t], delete a[t]), e
            },
            u = {
                input: "checked",
                option: "selected",
                textarea: "value"
            };
        Element.implement({
            destroy: function() {
                var e = c(this).getElementsByTagName("*");
                return Array.each(e, c), Element.dispose(this), null
            },
            empty: function() {
                return Array.from(this.childNodes).each(Element.dispose), this
            },
            dispose: function() {
                return this.parentNode ? this.parentNode.removeChild(this) : this
            },
            clone: function(e, t) {
                e = e !== !1;
                var n, i = this.cloneNode(e),
                    s = [i],
                    r = [this];
                for (e && (s.append(Array.from(i.getElementsByTagName("*"))), r.append(Array.from(this.getElementsByTagName("*")))), n = s.length; n--;) {
                    var o = s[n],
                        a = r[n];
                    if (t || o.removeAttribute("id"), o.clearAttributes && (o.clearAttributes(), o.mergeAttributes(a), o.removeAttribute("uid"), o.options))
                        for (var l = o.options, c = a.options, d = l.length; d--;) l[d].selected = c[d].selected;
                    var h = u[a.tagName.toLowerCase()];
                    h && a[h] && (o[h] = a[h])
                }
                if (Browser.ie) {
                    var m = i.getElementsByTagName("object"),
                        f = this.getElementsByTagName("object");
                    for (n = m.length; n--;) m[n].outerHTML = f[n].outerHTML
                }
                return document.id(i)
            }
        }), [Element, Window, Document].invoke("implement", {
            addListener: function(e, t) {
                if ("unload" == e) {
                    var n = t,
                        i = this;
                    t = function() {
                        i.removeListener("unload", t), n()
                    }
                } else o[$uid(this)] = this;
                return this.addEventListener ? this.addEventListener(e, t, !!arguments[2]) : this.attachEvent("on" + e, t), this
            },
            removeListener: function(e, t) {
                return this.removeEventListener ? this.removeEventListener(e, t, !!arguments[2]) : this.detachEvent("on" + e, t), this
            },
            retrieve: function(e, t) {
                var n = l($uid(this)),
                    i = n[e];
                return null != t && null == i && (i = n[e] = t), null != i ? i : null
            },
            store: function(e, t) {
                var n = l($uid(this));
                return n[e] = t, this
            },
            eliminate: function(e) {
                var t = l($uid(this));
                return delete t[e], this
            }
        }), window.attachEvent && !window.addEventListener && window.addListener("unload", function() {
            Object.each(o, c), window.CollectGarbage && CollectGarbage()
        }), Element.Properties = {}, Element.Properties = new Hash, Element.Properties.style = {
            set: function(e) {
                this.style.cssText = e
            },
            get: function() {
                return this.style.cssText
            },
            erase: function() {
                this.style.cssText = ""
            }
        }, Element.Properties.tag = {
            get: function() {
                return this.tagName.toLowerCase()
            }
        }, Element.Properties.html = function() {
            var e = Function.attempt(function() {
                    var e = document.createElement("table");
                    e.innerHTML = "<tr><td></td></tr>"
                }),
                t = document.createElement("div"),
                n = {
                    table: [1, "<table>", "</table>"],
                    select: [1, "<select>", "</select>"],
                    tbody: [2, "<table><tbody>", "</tbody></table>"],
                    tr: [3, "<table><tbody><tr>", "</tr></tbody></table>"]
                };
            n.thead = n.tfoot = n.tbody, t.innerHTML = "<nav></nav>";
            var i = 1 == t.childNodes.length;
            if (!i) {
                for (var s = "abbr article aside audio canvas datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video".split(" "), r = document.createDocumentFragment(), o = s.length; o--;) r.createElement(s[o]);
                r.appendChild(t)
            }
            var a = {
                set: function(s) {
                    "array" == typeOf(s) && (s = s.join(""));
                    var r = !e && n[this.get("tag")];
                    if (r || i || (r = [0, "", ""]), r) {
                        var o = t;
                        o.innerHTML = r[1] + s + r[2];
                        for (var a = r[0]; a--;) o = o.firstChild;
                        this.empty().adopt(o.childNodes)
                    } else this.innerHTML = s
                }
            };
            return a.erase = a.set, a
        }();
        var d = document.createElement("form");
        d.innerHTML = "<select><option>s</option></select>", "s" != d.firstChild.value && (Element.Properties.value = {
            set: function(e) {
                var t = this.get("tag");
                if ("select" != t) return this.setProperty("value", e);
                for (var n = this.getElements("option"), i = 0; i < n.length; i++) {
                    var s = n[i],
                        r = s.getAttributeNode("value"),
                        o = r && r.specified ? s.value : s.get("text");
                    if (o == e) return s.selected = !0
                }
            },
            get: function() {
                var e = this,
                    t = e.get("tag");
                if ("select" != t && "option" != t) return this.getProperty("value");
                if ("select" == t && !(e = e.getSelected()[0])) return "";
                var n = e.getAttributeNode("value");
                return n && n.specified ? e.value : e.get("text")
            }
        })
    }(),
    function() {
        var e = document.html;
        Element.Properties.styles = {
            set: function(e) {
                this.setStyles(e)
            }
        };
        var t = null != e.style.opacity,
            n = null != e.style.filter,
            i = /alpha\(opacity=([\d.]+)\)/i,
            s = function(e, t) {
                e.store("$opacity", t), e.style.visibility = t > 0 ? "visible" : "hidden"
            },
            r = t ? function(e, t) {
                e.style.opacity = t
            } : n ? function(e, t) {
                e.currentStyle && e.currentStyle.hasLayout || (e.style.zoom = 1), t = (100 * t).limit(0, 100).round(), t = 100 == t ? "" : "alpha(opacity=" + t + ")";
                var n = e.style.filter || e.getComputedStyle("filter") || "";
                e.style.filter = i.test(n) ? n.replace(i, t) : n + t
            } : s,
            o = t ? function(e) {
                var t = e.style.opacity || e.getComputedStyle("opacity");
                return "" == t ? 1 : t.toFloat()
            } : n ? function(e) {
                var t, n = e.style.filter || e.getComputedStyle("filter");
                return n && (t = n.match(i)), null == t || null == n ? 1 : t[1] / 100
            } : function(e) {
                var t = e.retrieve("$opacity");
                return null == t && (t = "hidden" == e.style.visibility ? 0 : 1), t
            },
            a = null == e.style.cssFloat ? "styleFloat" : "cssFloat";
        Element.implement({
            getComputedStyle: function(e) {
                if (this.currentStyle) return this.currentStyle[e.camelCase()];
                var t = Element.getDocument(this).defaultView,
                    n = t ? t.getComputedStyle(this, null) : null;
                return n ? n.getPropertyValue(e == a ? "float" : e.hyphenate()) : null
            },
            setStyle: function(e, t) {
                if ("opacity" == e) return r(this, parseFloat(t)), this;
                if (e = ("float" == e ? a : e).camelCase(), "string" != typeOf(t)) {
                    var n = (Element.Styles[e] || "@").split(" ");
                    t = Array.from(t).map(function(e, t) {
                        return n[t] ? "number" == typeOf(e) ? n[t].replace("@", Math.round(e)) : e : ""
                    }).join(" ")
                } else t == String(Number(t)) && (t = Math.round(t));
                return this.style[e] = t, this
            },
            getStyle: function(e) {
                if ("opacity" == e) return o(this);
                e = ("float" == e ? a : e).camelCase();
                var t = this.style[e];
                if (!t || "zIndex" == e) {
                    t = [];
                    for (var n in Element.ShortStyles)
                        if (e == n) {
                            for (var i in Element.ShortStyles[n]) t.push(this.getStyle(i));
                            return t.join(" ")
                        }
                    t = this.getComputedStyle(e)
                }
                if (t) {
                    t = String(t);
                    var s = t.match(/rgba?\([\d\s,]+\)/);
                    s && (t = t.replace(s[0], s[0].rgbToHex()))
                }
                if (Browser.opera || Browser.ie && isNaN(parseFloat(t))) {
                    if (/^(height|width)$/.test(e)) {
                        var r = "width" == e ? ["left", "right"] : ["top", "bottom"],
                            l = 0;
                        return r.each(function(e) {
                            l += this.getStyle("border-" + e + "-width").toInt() + this.getStyle("padding-" + e).toInt()
                        }, this), this["offset" + e.capitalize()] - l + "px"
                    }
                    if (Browser.opera && -1 != String(t).indexOf("px")) return t;
                    if (/^border(.+)Width|margin|padding/.test(e)) return "0px"
                }
                return t
            },
            setStyles: function(e) {
                for (var t in e) this.setStyle(t, e[t]);
                return this
            },
            getStyles: function() {
                var e = {};
                return Array.flatten(arguments).each(function(t) {
                    e[t] = this.getStyle(t)
                }, this), e
            }
        }), Element.Styles = {
            left: "@px",
            top: "@px",
            bottom: "@px",
            right: "@px",
            width: "@px",
            height: "@px",
            maxWidth: "@px",
            maxHeight: "@px",
            minWidth: "@px",
            minHeight: "@px",
            backgroundColor: "rgb(@, @, @)",
            backgroundPosition: "@px @px",
            color: "rgb(@, @, @)",
            fontSize: "@px",
            letterSpacing: "@px",
            lineHeight: "@px",
            clip: "rect(@px @px @px @px)",
            margin: "@px @px @px @px",
            padding: "@px @px @px @px",
            border: "@px @ rgb(@, @, @) @px @ rgb(@, @, @) @px @ rgb(@, @, @)",
            borderWidth: "@px @px @px @px",
            borderStyle: "@ @ @ @",
            borderColor: "rgb(@, @, @) rgb(@, @, @) rgb(@, @, @) rgb(@, @, @)",
            zIndex: "@",
            zoom: "@",
            fontWeight: "@",
            textIndent: "@px",
            opacity: "@"
        }, Element.implement({
            setOpacity: function(e) {
                return r(this, e), this
            },
            getOpacity: function() {
                return o(this)
            }
        }), Element.Properties.opacity = {
            set: function(e) {
                r(this, e), s(this, e)
            },
            get: function() {
                return o(this)
            }
        }, Element.Styles = new Hash(Element.Styles), Element.ShortStyles = {
            margin: {},
            padding: {},
            border: {},
            borderWidth: {},
            borderStyle: {},
            borderColor: {}
        }, ["Top", "Right", "Bottom", "Left"].each(function(e) {
            var t = Element.ShortStyles,
                n = Element.Styles;
            ["margin", "padding"].each(function(i) {
                var s = i + e;
                t[i][s] = n[s] = "@px"
            });
            var i = "border" + e;
            t.border[i] = n[i] = "@px @ rgb(@, @, @)";
            var s = i + "Width",
                r = i + "Style",
                o = i + "Color";
            t[i] = {}, t.borderWidth[s] = t[i][s] = n[s] = "@px", t.borderStyle[r] = t[i][r] = n[r] = "@", t.borderColor[o] = t[i][o] = n[o] = "rgb(@, @, @)"
        })
    }(),
    function() {
        Element.Properties.events = {
            set: function(e) {
                this.addEvents(e)
            }
        }, [Element, Window, Document].invoke("implement", {
            addEvent: function(e, t) {
                var n = this.retrieve("events", {});
                if (n[e] || (n[e] = {
                        keys: [],
                        values: []
                    }), n[e].keys.contains(t)) return this;
                n[e].keys.push(t);
                var i = e,
                    s = Element.Events[e],
                    r = t,
                    o = this;
                s && (s.onAdd && s.onAdd.call(this, t, e), s.condition && (r = function(n) {
                    return s.condition.call(this, n, e) ? t.call(this, n) : !0
                }), s.base && (i = Function.from(s.base).call(this, e)));
                var a = function() {
                        return t.call(o)
                    },
                    l = Element.NativeEvents[i];
                return l && (2 == l && (a = function(e) {
                    e = new DOMEvent(e, o.getWindow()), r.call(o, e) === !1 && e.stop()
                }), this.addListener(i, a, arguments[2])), n[e].values.push(a), this
            },
            removeEvent: function(e, t) {
                var n = this.retrieve("events");
                if (!n || !n[e]) return this;
                var i = n[e],
                    s = i.keys.indexOf(t);
                if (-1 == s) return this;
                var r = i.values[s];
                delete i.keys[s], delete i.values[s];
                var o = Element.Events[e];
                return o && (o.onRemove && o.onRemove.call(this, t, e), o.base && (e = Function.from(o.base).call(this, e))), Element.NativeEvents[e] ? this.removeListener(e, r, arguments[2]) : this
            },
            addEvents: function(e) {
                for (var t in e) this.addEvent(t, e[t]);
                return this
            },
            removeEvents: function(e) {
                var t;
                if ("object" == typeOf(e)) {
                    for (t in e) this.removeEvent(t, e[t]);
                    return this
                }
                var n = this.retrieve("events");
                if (!n) return this;
                if (e) n[e] && (n[e].keys.each(function(t) {
                    this.removeEvent(e, t)
                }, this), delete n[e]);
                else {
                    for (t in n) this.removeEvents(t);
                    this.eliminate("events")
                }
                return this
            },
            fireEvent: function(e, t, n) {
                var i = this.retrieve("events");
                return i && i[e] ? (t = Array.from(t), i[e].keys.each(function(e) {
                    n ? e.delay(n, this, t) : e.apply(this, t)
                }, this), this) : this
            },
            cloneEvents: function(e, t) {
                e = document.id(e);
                var n = e.retrieve("events");
                if (!n) return this;
                if (t) n[t] && n[t].keys.each(function(e) {
                    this.addEvent(t, e)
                }, this);
                else
                    for (var i in n) this.cloneEvents(e, i);
                return this
            }
        }), Element.NativeEvents = {
            click: 2,
            dblclick: 2,
            mouseup: 2,
            mousedown: 2,
            contextmenu: 2,
            mousewheel: 2,
            DOMMouseScroll: 2,
            mouseover: 2,
            mouseout: 2,
            mousemove: 2,
            selectstart: 2,
            selectend: 2,
            keydown: 2,
            keypress: 2,
            keyup: 2,
            orientationchange: 2,
            touchstart: 2,
            touchmove: 2,
            touchend: 2,
            touchcancel: 2,
            gesturestart: 2,
            gesturechange: 2,
            gestureend: 2,
            focus: 2,
            blur: 2,
            change: 2,
            reset: 2,
            select: 2,
            submit: 2,
            paste: 2,
            input: 2,
            load: 2,
            unload: 1,
            beforeunload: 2,
            resize: 1,
            move: 1,
            DOMContentLoaded: 1,
            readystatechange: 1,
            error: 1,
            abort: 1,
            scroll: 1
        };
        var e = function(e) {
            var t = e.relatedTarget;
            return null == t ? !0 : t ? t != this && "xul" != t.prefix && "document" != typeOf(this) && !this.contains(t) : !1
        };
        Element.Events = {
            mouseenter: {
                base: "mouseover",
                condition: e
            },
            mouseleave: {
                base: "mouseout",
                condition: e
            },
            mousewheel: {
                base: Browser.firefox ? "DOMMouseScroll" : "mousewheel"
            }
        }, window.addEventListener || (Element.NativeEvents.propertychange = 2, Element.Events.change = {
            base: function() {
                var e = this.type;
                return "input" != this.get("tag") || "radio" != e && "checkbox" != e ? "change" : "propertychange"
            },
            condition: function(e) {
                return !("radio" == this.type && !this.checked)
            }
        }), Element.Events = new Hash(Element.Events)
    }(),
    function() {
        var e = !!window.addEventListener;
        Element.NativeEvents.focusin = Element.NativeEvents.focusout = 2;
        var t = function(e, t, n, i, s) {
                for (; s && s != e;) {
                    if (t(s, i)) return n.call(s, i, s);
                    s = document.id(s.parentNode)
                }
            },
            n = {
                mouseenter: {
                    base: "mouseover"
                },
                mouseleave: {
                    base: "mouseout"
                },
                focus: {
                    base: "focus" + (e ? "" : "in"),
                    capture: !0
                },
                blur: {
                    base: e ? "blur" : "focusout",
                    capture: !0
                }
            },
            i = "$delegation:",
            s = function(e) {
                return {
                    base: "focusin",
                    remove: function(t, n) {
                        var s = t.retrieve(i + e + "listeners", {})[n];
                        if (s && s.forms)
                            for (var r = s.forms.length; r--;) s.forms[r].removeEvent(e, s.fns[r])
                    },
                    listen: function(n, s, r, o, a, l) {
                        var c = "form" == a.get("tag") ? a : o.target.getParent("form");
                        if (c) {
                            var u = n.retrieve(i + e + "listeners", {}),
                                d = u[l] || {
                                    forms: [],
                                    fns: []
                                },
                                h = d.forms,
                                m = d.fns;
                            if (-1 == h.indexOf(c)) {
                                h.push(c);
                                var f = function(e) {
                                    t(n, s, r, e, a)
                                };
                                c.addEvent(e, f), m.push(f), u[l] = d, n.store(i + e + "listeners", u)
                            }
                        }
                    }
                }
            },
            r = function(e) {
                return {
                    base: "focusin",
                    listen: function(n, i, s, r, o) {
                        var a = {
                            blur: function() {
                                this.removeEvents(a)
                            }
                        };
                        a[e] = function(e) {
                            t(n, i, s, e, o)
                        }, r.target.addEvents(a)
                    }
                }
            };
        e || Object.append(n, {
            submit: s("submit"),
            reset: s("reset"),
            change: r("change"),
            select: r("select")
        });
        var o = Element.prototype,
            a = o.addEvent,
            l = o.removeEvent,
            c = function(e, t) {
                return function(n, i, s) {
                    if (-1 == n.indexOf(":relay")) return e.call(this, n, i, s);
                    var r = Slick.parse(n).expressions[0][0];
                    if ("relay" != r.pseudos[0].key) return e.call(this, n, i, s);
                    var o = r.tag;
                    return r.pseudos.slice(1).each(function(e) {
                        o += ":" + e.key + (e.value ? "(" + e.value + ")" : "")
                    }), e.call(this, n, i), t.call(this, o, r.pseudos[0].value, i)
                }
            },
            u = {
                addEvent: function(e, i, s) {
                    var r = this.retrieve("$delegates", {}),
                        o = r[e];
                    if (o)
                        for (var l in o)
                            if (o[l].fn == s && o[l].match == i) return this;
                    var c = e,
                        u = i,
                        d = s,
                        h = n[e] || {};
                    e = h.base || c, i = function(e) {
                        return Slick.match(e, u)
                    };
                    var m = Element.Events[c];
                    if (m && m.condition) {
                        var f = i,
                            p = m.condition;
                        i = function(t, n) {
                            return f(t, n) && p.call(t, n, e)
                        }
                    }
                    var g = this,
                        v = String.uniqueID(),
                        y = h.listen ? function(e, t) {
                            !t && e && e.target && (t = e.target), t && h.listen(g, i, s, e, t, v)
                        } : function(e, n) {
                            !n && e && e.target && (n = e.target), n && t(g, i, s, e, n)
                        };
                    return o || (o = {}), o[v] = {
                        match: u,
                        fn: d,
                        delegator: y
                    }, r[c] = o, a.call(this, e, y, h.capture)
                },
                removeEvent: function(e, t, i, s) {
                    var r = this.retrieve("$delegates", {}),
                        o = r[e];
                    if (!o) return this;
                    if (s) {
                        var a = e,
                            c = o[s].delegator,
                            d = n[e] || {};
                        return e = d.base || a, d.remove && d.remove(this, s), delete o[s], r[a] = o, l.call(this, e, c)
                    }
                    var h, m;
                    if (i) {
                        for (h in o)
                            if (m = o[h], m.match == t && m.fn == i) return u.removeEvent.call(this, e, t, i, h)
                    } else
                        for (h in o) m = o[h], m.match == t && u.removeEvent.call(this, e, t, m.fn, h);
                    return this
                }
            };
        [Element, Window, Document].invoke("implement", {
            addEvent: c(a, u.addEvent),
            removeEvent: c(l, u.removeEvent)
        })
    }(),
    function() {
        function e(e, t) {
            return d(e, t).toInt() || 0
        }

        function t(e) {
            return "border-box" == d(e, "-moz-box-sizing")
        }

        function n(t) {
            return e(t, "border-top-width")
        }

        function i(t) {
            return e(t, "border-left-width")
        }

        function s(e) {
            return /^(?:body|html)$/i.test(e.tagName)
        }

        function r(e) {
            var t = e.getDocument();
            return t.compatMode && "CSS1Compat" != t.compatMode ? t.body : t.html
        }
        var o = document.createElement("div"),
            a = document.createElement("div");
        o.style.height = "0", o.appendChild(a);
        var l = a.offsetParent === o;
        o = a = null;
        var c = function(e) {
                return "static" != d(e, "position") || s(e)
            },
            u = function(e) {
                return c(e) || /^(?:table|td|th)$/i.test(e.tagName)
            };
        Element.implement({
            scrollTo: function(e, t) {
                return s(this) ? this.getWindow().scrollTo(e, t) : (this.scrollLeft = e, this.scrollTop = t), this
            },
            getSize: function() {
                return s(this) ? this.getWindow().getSize() : {
                    x: this.offsetWidth,
                    y: this.offsetHeight
                }
            },
            getScrollSize: function() {
                return s(this) ? this.getWindow().getScrollSize() : {
                    x: this.scrollWidth,
                    y: this.scrollHeight
                }
            },
            getScroll: function() {
                return s(this) ? this.getWindow().getScroll() : {
                    x: this.scrollLeft,
                    y: this.scrollTop
                }
            },
            getScrolls: function() {
                for (var e = this.parentNode, t = {
                        x: 0,
                        y: 0
                    }; e && !s(e);) t.x += e.scrollLeft, t.y += e.scrollTop, e = e.parentNode;
                return t
            },
            getOffsetParent: l ? function() {
                var e = this;
                if (s(e) || "fixed" == d(e, "position")) return null;
                for (var t = "static" == d(e, "position") ? u : c; e = e.parentNode;)
                    if (t(e)) return e;
                return null
            } : function() {
                var e = this;
                if (s(e) || "fixed" == d(e, "position")) return null;
                try {
                    return e.offsetParent
                } catch (t) {}
                return null
            },
            getOffsets: function() {
                if (this.getBoundingClientRect && !Browser.Platform.ios) {
                    var e = this.getBoundingClientRect(),
                        r = document.id(this.getDocument().documentElement),
                        o = r.getScroll(),
                        a = this.getScrolls(),
                        l = "fixed" == d(this, "position");
                    return {
                        x: e.left.toInt() + a.x + (l ? 0 : o.x) - r.clientLeft,
                        y: e.top.toInt() + a.y + (l ? 0 : o.y) - r.clientTop
                    }
                }
                var c = this,
                    u = {
                        x: 0,
                        y: 0
                    };
                if (s(this)) return u;
                for (; c && !s(c);) {
                    if (u.x += c.offsetLeft, u.y += c.offsetTop, Browser.firefox) {
                        t(c) || (u.x += i(c), u.y += n(c));
                        var h = c.parentNode;
                        h && "visible" != d(h, "overflow") && (u.x += i(h), u.y += n(h))
                    } else c != this && Browser.safari && (u.x += i(c), u.y += n(c));
                    c = c.offsetParent
                }
                return Browser.firefox && !t(this) && (u.x -= i(this), u.y -= n(this)), u
            },
            getPosition: function(e) {
                var t = this.getOffsets(),
                    s = this.getScrolls(),
                    r = {
                        x: t.x - s.x,
                        y: t.y - s.y
                    };
                if (e && (e = document.id(e))) {
                    var o = e.getPosition();
                    return {
                        x: r.x - o.x - i(e),
                        y: r.y - o.y - n(e)
                    }
                }
                return r
            },
            getCoordinates: function(e) {
                if (s(this)) return this.getWindow().getCoordinates();
                var t = this.getPosition(e),
                    n = this.getSize(),
                    i = {
                        left: t.x,
                        top: t.y,
                        width: n.x,
                        height: n.y
                    };
                return i.right = i.left + i.width, i.bottom = i.top + i.height, i
            },
            computePosition: function(t) {
                return {
                    left: t.x - e(this, "margin-left"),
                    top: t.y - e(this, "margin-top")
                }
            },
            setPosition: function(e) {
                return this.setStyles(this.computePosition(e))
            }
        }), [Document, Window].invoke("implement", {
            getSize: function() {
                var e = r(this);
                return {
                    x: e.clientWidth,
                    y: e.clientHeight
                }
            },
            getScroll: function() {
                var e = this.getWindow(),
                    t = r(this);
                return {
                    x: e.pageXOffset || t.scrollLeft,
                    y: e.pageYOffset || t.scrollTop
                }
            },
            getScrollSize: function() {
                var e = r(this),
                    t = this.getSize(),
                    n = this.getDocument().body;
                return {
                    x: Math.max(e.scrollWidth, n.scrollWidth, t.x),
                    y: Math.max(e.scrollHeight, n.scrollHeight, t.y)
                }
            },
            getPosition: function() {
                return {
                    x: 0,
                    y: 0
                }
            },
            getCoordinates: function() {
                var e = this.getSize();
                return {
                    top: 0,
                    left: 0,
                    bottom: e.y,
                    right: e.x,
                    height: e.y,
                    width: e.x
                }
            }
        });
        var d = Element.getComputedStyle
    }(), Element.alias({
        position: "setPosition"
    }), [Window, Document, Element].invoke("implement", {
        getHeight: function() {
            return this.getSize().y
        },
        getWidth: function() {
            return this.getSize().x
        },
        getScrollTop: function() {
            return this.getScroll().y
        },
        getScrollLeft: function() {
            return this.getScroll().x
        },
        getScrollHeight: function() {
            return this.getScrollSize().y
        },
        getScrollWidth: function() {
            return this.getScrollSize().x
        },
        getTop: function() {
            return this.getPosition().y
        },
        getLeft: function() {
            return this.getPosition().x
        }
    }),
    function() {
        var e = this.Fx = new Class({
            Implements: [Chain, Events, Options],
            options: {
                fps: 60,
                unit: !1,
                duration: 500,
                frames: null,
                frameSkip: !0,
                link: "ignore"
            },
            initialize: function(e) {
                this.subject = this.subject || this, this.setOptions(e)
            },
            getTransition: function() {
                return function(e) {
                    return -(Math.cos(Math.PI * e) - 1) / 2
                }
            },
            step: function(e) {
                if (this.options.frameSkip) {
                    var t = null != this.time ? e - this.time : 0,
                        n = t / this.frameInterval;
                    this.time = e, this.frame += n
                } else this.frame++;
                if (this.frame < this.frames) {
                    var i = this.transition(this.frame / this.frames);
                    this.set(this.compute(this.from, this.to, i))
                } else this.frame = this.frames, this.set(this.compute(this.from, this.to, 1)), this.stop()
            },
            set: function(e) {
                return e
            },
            compute: function(t, n, i) {
                return e.compute(t, n, i)
            },
            check: function() {
                if (!this.isRunning()) return !0;
                switch (this.options.link) {
                    case "cancel":
                        return this.cancel(), !0;
                    case "chain":
                        return this.chain(this.caller.pass(arguments, this)), !1
                }
                return !1
            },
            start: function(t, n) {
                if (!this.check(t, n)) return this;
                this.from = t, this.to = n, this.frame = this.options.frameSkip ? 0 : -1, this.time = null, this.transition = this.getTransition();
                var i = this.options.frames,
                    r = this.options.fps,
                    o = this.options.duration;
                return this.duration = e.Durations[o] || o.toInt(), this.frameInterval = 1e3 / r, this.frames = i || Math.round(this.duration / this.frameInterval), this.fireEvent("start", this.subject), s.call(this, r), this
            },
            stop: function() {
                return this.isRunning() && (this.time = null, r.call(this, this.options.fps), this.frames == this.frame ? (this.fireEvent("complete", this.subject), this.callChain() || this.fireEvent("chainComplete", this.subject)) : this.fireEvent("stop", this.subject)), this
            },
            cancel: function() {
                return this.isRunning() && (this.time = null, r.call(this, this.options.fps), this.frame = this.frames, this.fireEvent("cancel", this.subject).clearChain()), this
            },
            pause: function() {
                return this.isRunning() && (this.time = null, r.call(this, this.options.fps)), this
            },
            resume: function() {
                return this.frame < this.frames && !this.isRunning() && s.call(this, this.options.fps), this
            },
            isRunning: function() {
                var e = t[this.options.fps];
                return e && e.contains(this)
            }
        });
        e.compute = function(e, t, n) {
            return (t - e) * n + e
        }, e.Durations = {
            "short": 250,
            normal: 500,
            "long": 1e3
        };
        var t = {},
            n = {},
            i = function() {
                for (var e = Date.now(), t = this.length; t--;) {
                    var n = this[t];
                    n && n.step(e)
                }
            },
            s = function(e) {
                var s = t[e] || (t[e] = []);
                s.push(this), n[e] || (n[e] = i.periodical(Math.round(1e3 / e), s))
            },
            r = function(e) {
                var i = t[e];
                i && (i.erase(this), !i.length && n[e] && (delete t[e], n[e] = clearInterval(n[e])))
            }
    }(), Fx.CSS = new Class({
        Extends: Fx,
        prepare: function(e, t, n) {
            n = Array.from(n), null == n[1] && (n[1] = n[0], n[0] = e.getStyle(t));
            var i = n.map(this.parse);
            return {
                from: i[0],
                to: i[1]
            }
        },
        parse: function(e) {
            return e = Function.from(e)(), e = "string" == typeof e ? e.split(" ") : Array.from(e), e.map(function(e) {
                e = String(e);
                var t = !1;
                return Object.each(Fx.CSS.Parsers, function(n, i) {
                    if (!t) {
                        var s = n.parse(e);
                        (s || 0 === s) && (t = {
                            value: s,
                            parser: n
                        })
                    }
                }), t = t || {
                    value: e,
                    parser: Fx.CSS.Parsers.String
                }
            })
        },
        compute: function(e, t, n) {
            var i = [];
            return Math.min(e.length, t.length).times(function(s) {
                i.push({
                    value: e[s].parser.compute(e[s].value, t[s].value, n),
                    parser: e[s].parser
                })
            }), i.$family = Function.from("fx:css:value"), i
        },
        serve: function(e, t) {
            "fx:css:value" != typeOf(e) && (e = this.parse(e));
            var n = [];
            return e.each(function(e) {
                n = n.concat(e.parser.serve(e.value, t))
            }), n
        },
        render: function(e, t, n, i) {
            e.setStyle(t, this.serve(n, i))
        },
        search: function(e) {
            if (Fx.CSS.Cache[e]) return Fx.CSS.Cache[e];
            var t = {},
                n = new RegExp("^" + e.escapeRegExp() + "$");
            return Array.each(document.styleSheets, function(e, i) {
                var s = e.href;
                if (!s || !s.contains("://") || s.contains(document.domain)) {
                    var r = e.rules || e.cssRules;
                    Array.each(r, function(e, i) {
                        if (e.style) {
                            var s = e.selectorText ? e.selectorText.replace(/^\w+/, function(e) {
                                return e.toLowerCase()
                            }) : null;
                            s && n.test(s) && Object.each(Element.Styles, function(n, i) {
                                e.style[i] && !Element.ShortStyles[i] && (n = String(e.style[i]), t[i] = /^rgb/.test(n) ? n.rgbToHex() : n)
                            })
                        }
                    })
                }
            }), Fx.CSS.Cache[e] = t
        }
    }), Fx.CSS.Cache = {}, Fx.CSS.Parsers = {
        Color: {
            parse: function(e) {
                return e.match(/^#[0-9a-f]{3,6}$/i) ? e.hexToRgb(!0) : (e = e.match(/(\d+),\s*(\d+),\s*(\d+)/)) ? [e[1], e[2], e[3]] : !1
            },
            compute: function(e, t, n) {
                return e.map(function(i, s) {
                    return Math.round(Fx.compute(e[s], t[s], n))
                })
            },
            serve: function(e) {
                return e.map(Number)
            }
        },
        Number: {
            parse: parseFloat,
            compute: Fx.compute,
            serve: function(e, t) {
                return t ? e + t : e
            }
        },
        String: {
            parse: Function.from(!1),
            compute: function(e, t) {
                return t
            },
            serve: function(e) {
                return e
            }
        }
    }, Fx.CSS.Parsers = new Hash(Fx.CSS.Parsers), Fx.Tween = new Class({
        Extends: Fx.CSS,
        initialize: function(e, t) {
            this.element = this.subject = document.id(e), this.parent(t)
        },
        set: function(e, t) {
            return 1 == arguments.length && (t = e, e = this.property || this.options.property), this.render(this.element, e, t, this.options.unit), this
        },
        start: function(e, t, n) {
            if (!this.check(e, t, n)) return this;
            var i = Array.flatten(arguments);
            this.property = this.options.property || i.shift();
            var s = this.prepare(this.element, this.property, i);
            return this.parent(s.from, s.to)
        }
    }), Element.Properties.tween = {
        set: function(e) {
            return this.get("tween").cancel().setOptions(e), this
        },
        get: function() {
            var e = this.retrieve("tween");
            return e || (e = new Fx.Tween(this, {
                link: "cancel"
            }), this.store("tween", e)), e
        }
    }, Element.implement({
        tween: function(e, t, n) {
            return this.get("tween").start(e, t, n), this
        },
        fade: function(e) {
            var t, n, i, s = this.get("tween");
            switch (null == e && (e = "toggle"), e) {
                case "in":
                    t = "start", n = 1;
                    break;
                case "out":
                    t = "start", n = 0;
                    break;
                case "show":
                    t = "set", n = 1;
                    break;
                case "hide":
                    t = "set", n = 0;
                    break;
                case "toggle":
                    var r = this.retrieve("fade:flag", 1 == this.getStyle("opacity"));
                    t = "start", n = r ? 0 : 1, this.store("fade:flag", !r), i = !0;
                    break;
                default:
                    t = "start", n = e
            }
            return i || this.eliminate("fade:flag"), s[t]("opacity", n), "set" == t || 0 != n ? this.setStyle("visibility", 0 == n ? "hidden" : "visible") : s.chain(function() {
                this.element.setStyle("visibility", "hidden")
            }), this
        },
        highlight: function(e, t) {
            t || (t = this.retrieve("highlight:original", this.getStyle("background-color")), t = "transparent" == t ? "#fff" : t);
            var n = this.get("tween");
            return n.start("background-color", e || "#ffff88", t).chain(function() {
                this.setStyle("background-color", this.retrieve("highlight:original")), n.callChain()
            }.bind(this)), this
        }
    }), Fx.Morph = new Class({
        Extends: Fx.CSS,
        initialize: function(e, t) {
            this.element = this.subject = document.id(e), this.parent(t)
        },
        set: function(e) {
            "string" == typeof e && (e = this.search(e));
            for (var t in e) this.render(this.element, t, e[t], this.options.unit);
            return this
        },
        compute: function(e, t, n) {
            var i = {};
            for (var s in e) i[s] = this.parent(e[s], t[s], n);
            return i
        },
        start: function(e) {
            if (!this.check(e)) return this;
            "string" == typeof e && (e = this.search(e));
            var t = {},
                n = {};
            for (var i in e) {
                var s = this.prepare(this.element, i, e[i]);
                t[i] = s.from, n[i] = s.to
            }
            return this.parent(t, n)
        }
    }), Element.Properties.morph = {
        set: function(e) {
            return this.get("morph").cancel().setOptions(e), this
        },
        get: function() {
            var e = this.retrieve("morph");
            return e || (e = new Fx.Morph(this, {
                link: "cancel"
            }), this.store("morph", e)), e
        }
    }, Element.implement({
        morph: function(e) {
            return this.get("morph").start(e), this
        }
    }), Fx.implement({
        getTransition: function() {
            var e = this.options.transition || Fx.Transitions.Sine.easeInOut;
            if ("string" == typeof e) {
                var t = e.split(":");
                e = Fx.Transitions, e = e[t[0]] || e[t[0].capitalize()], t[1] && (e = e["ease" + t[1].capitalize() + (t[2] ? t[2].capitalize() : "")])
            }
            return e
        }
    }), Fx.Transition = function(e, t) {
        t = Array.from(t);
        var n = function(n) {
            return e(n, t)
        };
        return Object.append(n, {
            easeIn: n,
            easeOut: function(n) {
                return 1 - e(1 - n, t)
            },
            easeInOut: function(n) {
                return (.5 >= n ? e(2 * n, t) : 2 - e(2 * (1 - n), t)) / 2
            }
        })
    }, Fx.Transitions = {
        linear: function(e) {
            return e
        }
    }, Fx.Transitions = new Hash(Fx.Transitions), Fx.Transitions.extend = function(e) {
        for (var t in e) Fx.Transitions[t] = new Fx.Transition(e[t])
    }, Fx.Transitions.extend({
        Pow: function(e, t) {
            return Math.pow(e, t && t[0] || 6)
        },
        Expo: function(e) {
            return Math.pow(2, 8 * (e - 1))
        },
        Circ: function(e) {
            return 1 - Math.sin(Math.acos(e))
        },
        Sine: function(e) {
            return 1 - Math.cos(e * Math.PI / 2)
        },
        Back: function(e, t) {
            return t = t && t[0] || 1.618, Math.pow(e, 2) * ((t + 1) * e - t)
        },
        Bounce: function(e) {
            for (var t, n = 0, i = 1; 1; n += i, i /= 2)
                if (e >= (7 - 4 * n) / 11) {
                    t = i * i - Math.pow((11 - 6 * n - 11 * e) / 4, 2);
                    break
                }
            return t
        },
        Elastic: function(e, t) {
            return Math.pow(2, 10 * --e) * Math.cos(20 * e * Math.PI * (t && t[0] || 1) / 3)
        }
    }), ["Quad", "Cubic", "Quart", "Quint"].each(function(e, t) {
        Fx.Transitions[e] = new Fx.Transition(function(e) {
            return Math.pow(e, t + 2)
        })
    }),
    function() {
        var e = function() {},
            t = "onprogress" in new Browser.Request,
            n = this.Request = new Class({
                Implements: [Chain, Events, Options],
                options: {
                    url: "",
                    data: "",
                    headers: {
                        "X-Requested-With": "XMLHttpRequest",
                        Accept: "text/javascript, text/html, application/xml, text/xml, */*"
                    },
                    async: !0,
                    format: !1,
                    method: "post",
                    link: "ignore",
                    isSuccess: null,
                    emulation: !0,
                    urlEncoded: !0,
                    encoding: "utf-8",
                    evalScripts: !1,
                    evalResponse: !1,
                    timeout: 0,
                    noCache: !1
                },
                initialize: function(e) {
                    this.xhr = new Browser.Request, this.setOptions(e), this.headers = this.options.headers
                },
                onStateChange: function() {
                    var n = this.xhr;
                    4 == n.readyState && this.running && (this.running = !1, this.status = 0, Function.attempt(function() {
                        var e = n.status;
                        this.status = 1223 == e ? 204 : e
                    }.bind(this)), n.onreadystatechange = e, t && (n.onprogress = n.onloadstart = e), clearTimeout(this.timer), this.response = {
                        text: this.xhr.responseText || "",
                        xml: this.xhr.responseXML
                    }, this.options.isSuccess.call(this, this.status) ? this.success(this.response.text, this.response.xml) : this.failure())
                },
                isSuccess: function() {
                    var e = this.status;
                    return e >= 200 && 300 > e
                },
                isRunning: function() {
                    return !!this.running
                },
                processScripts: function(e) {
                    return this.options.evalResponse || /(ecma|java)script/.test(this.getHeader("Content-type")) ? Browser.exec(e) : e.stripScripts(this.options.evalScripts)
                },
                success: function(e, t) {
                    this.onSuccess(this.processScripts(e), t)
                },
                onSuccess: function() {
                    this.fireEvent("complete", arguments).fireEvent("success", arguments).callChain()
                },
                failure: function() {
                    this.onFailure()
                },
                onFailure: function() {
                    this.fireEvent("complete").fireEvent("failure", this.xhr)
                },
                loadstart: function(e) {
                    this.fireEvent("loadstart", [e, this.xhr])
                },
                progress: function(e) {
                    this.fireEvent("progress", [e, this.xhr])
                },
                timeout: function() {
                    this.fireEvent("timeout", this.xhr)
                },
                setHeader: function(e, t) {
                    return this.headers[e] = t, this
                },
                getHeader: function(e) {
                    return Function.attempt(function() {
                        return this.xhr.getResponseHeader(e)
                    }.bind(this))
                },
                check: function() {
                    if (!this.running) return !0;
                    switch (this.options.link) {
                        case "cancel":
                            return this.cancel(), !0;
                        case "chain":
                            return this.chain(this.caller.pass(arguments, this)), !1
                    }
                    return !1
                },
                send: function(e) {
                    if (!this.check(e)) return this;
                    this.options.isSuccess = this.options.isSuccess || this.isSuccess, this.running = !0;
                    var n = typeOf(e);
                    ("string" == n || "element" == n) && (e = {
                        data: e
                    });
                    var i = this.options;
                    e = Object.append({
                        data: i.data,
                        url: i.url,
                        method: i.method
                    }, e);
                    var s = e.data,
                        r = String(e.url),
                        o = e.method.toLowerCase();
                    switch (typeOf(s)) {
                        case "element":
                            s = document.id(s).toQueryString();
                            break;
                        case "object":
                        case "hash":
                            s = Object.toQueryString(s)
                    }
                    if (this.options.format) {
                        var a = "format=" + this.options.format;
                        s = s ? a + "&" + s : a
                    }
                    if (this.options.emulation && !["get", "post"].contains(o)) {
                        var l = "_method=" + o;
                        s = s ? l + "&" + s : l, o = "post"
                    }
                    if (this.options.urlEncoded && ["post", "put"].contains(o)) {
                        var c = this.options.encoding ? "; charset=" + this.options.encoding : "";
                        this.headers["Content-type"] = "application/x-www-form-urlencoded" + c
                    }
                    r || (r = document.location.pathname);
                    var u = r.lastIndexOf("/");
                    u > -1 && (u = r.indexOf("#")) > -1 && (r = r.substr(0, u)), this.options.noCache && (r += (r.contains("?") ? "&" : "?") + String.uniqueID()), s && "get" == o && (r += (r.contains("?") ? "&" : "?") + s, s = null);
                    var d = this.xhr;
                    return t && (d.onloadstart = this.loadstart.bind(this), d.onprogress = this.progress.bind(this)), d.open(o.toUpperCase(), r, this.options.async, this.options.user, this.options.password), ("undefined" != typeof this.options.cors || this.options.user && "withCredentials" in d) && (d.withCredentials = !0), d.onreadystatechange = this.onStateChange.bind(this), Object.each(this.headers, function(e, t) {
                        try {
                            d.setRequestHeader(t, e)
                        } catch (n) {
                            this.fireEvent("exception", [t, e])
                        }
                    }, this), this.fireEvent("request"), d.send(s), this.options.async || this.onStateChange(), this.options.timeout && (this.timer = this.timeout.delay(this.options.timeout, this)), this
                },
                cancel: function() {
                    if (!this.running) return this;
                    this.running = !1;
                    var n = this.xhr;
                    return n.abort(), clearTimeout(this.timer), n.onreadystatechange = e, t && (n.onprogress = n.onloadstart = e), this.xhr = new Browser.Request, this.fireEvent("cancel"),
                        this
                }
            }),
            i = {};
        ["get", "post", "put", "delete", "GET", "POST", "PUT", "DELETE"].each(function(e) {
            i[e] = function(t) {
                var n = {
                    method: e
                };
                return null != t && (n.data = t), this.send(n)
            }
        }), n.implement(i), Element.Properties.send = {
            set: function(e) {
                var t = this.get("send").cancel();
                return t.setOptions(e), this
            },
            get: function() {
                var e = this.retrieve("send");
                return e || (e = new n({
                    data: this,
                    link: "cancel",
                    method: this.get("method") || "post",
                    url: this.get("action")
                }), this.store("send", e)), e
            }
        }, Element.implement({
            send: function(e) {
                var t = this.get("send");
                return t.send({
                    data: this,
                    url: e || t.options.url
                }), this
            }
        })
    }(), Request.HTML = new Class({
        Extends: Request,
        options: {
            update: !1,
            append: !1,
            evalScripts: !0,
            filter: !1,
            headers: {
                Accept: "text/html, application/xml, text/xml, */*"
            }
        },
        success: function(e) {
            var t = this.options,
                n = this.response;
            n.html = e.stripScripts(function(e) {
                n.javascript = e
            });
            var i = n.html.match(/<body[^>]*>([\s\S]*?)<\/body>/i);
            i && (n.html = i[1]);
            var s = new Element("div").set("html", n.html);
            if (n.tree = s.childNodes, n.elements = s.getElements(t.filter || "*"), t.filter && (n.tree = n.elements), t.update) {
                var r = document.id(t.update).empty();
                t.filter ? r.adopt(n.elements) : r.set("html", n.html)
            } else if (t.append) {
                var o = document.id(t.append);
                t.filter ? n.elements.reverse().inject(o) : o.adopt(s.getChildren())
            }
            t.evalScripts && Browser.exec(n.javascript), this.onSuccess(n.tree, n.elements, n.html, n.javascript)
        }
    }), Element.Properties.load = {
        set: function(e) {
            var t = this.get("load").cancel();
            return t.setOptions(e), this
        },
        get: function() {
            var e = this.retrieve("load");
            return e || (e = new Request.HTML({
                data: this,
                link: "cancel",
                update: this,
                method: "get"
            }), this.store("load", e)), e
        }
    }, Element.implement({
        load: function() {
            return this.get("load").send(Array.link(arguments, {
                data: Type.isObject,
                url: Type.isString
            })), this
        }
    }), "undefined" == typeof JSON && (this.JSON = {}), JSON = new Hash({
        stringify: JSON.stringify,
        parse: JSON.parse
    }),
    function() {
        var special = {
                "\b": "\\b",
                "	": "\\t",
                "\n": "\\n",
                "\f": "\\f",
                "\r": "\\r",
                '"': '\\"',
                "\\": "\\\\"
            },
            escape = function(e) {
                return special[e] || "\\u" + ("0000" + e.charCodeAt(0).toString(16)).slice(-4)
            };
        JSON.validate = function(e) {
            return e = e.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, ""), /^[\],:{}\s]*$/.test(e)
        }, JSON.encode = JSON.stringify ? function(e) {
            return JSON.stringify(e)
        } : function(e) {
            switch (e && e.toJSON && (e = e.toJSON()), typeOf(e)) {
                case "string":
                    return '"' + e.replace(/[\x00-\x1f\\"]/g, escape) + '"';
                case "array":
                    return "[" + e.map(JSON.encode).clean() + "]";
                case "object":
                case "hash":
                    var t = [];
                    return Object.each(e, function(e, n) {
                        var i = JSON.encode(e);
                        i && t.push(JSON.encode(n) + ":" + i)
                    }), "{" + t + "}";
                case "number":
                case "boolean":
                    return "" + e;
                case "null":
                    return "null"
            }
            return null
        }, JSON.decode = function(string, secure) {
            if (!string || "string" != typeOf(string)) return null;
            if (secure || JSON.secure) {
                if (JSON.parse) return JSON.parse(string);
                if (!JSON.validate(string)) throw new Error("JSON could not decode the input; security is enabled and the value is not secure.")
            }
            return eval("(" + string + ")")
        }
    }(), Request.JSON = new Class({
        Extends: Request,
        options: {
            secure: !0
        },
        initialize: function(e) {
            this.parent(e), Object.append(this.headers, {
                Accept: "application/json",
                "X-Request": "JSON"
            })
        },
        success: function(e) {
            var t;
            try {
                t = this.response.json = JSON.decode(e, this.options.secure)
            } catch (n) {
                return void this.fireEvent("error", [e, n])
            }
            null == t ? this.onFailure() : this.onSuccess(t, e)
        }
    });
var Cookie = new Class({
    Implements: Options,
    options: {
        path: "/",
        domain: !1,
        duration: !1,
        secure: !1,
        document: document,
        encode: !0
    },
    initialize: function(e, t) {
        this.key = e, this.setOptions(t)
    },
    write: function(e) {
        if (this.options.encode && (e = encodeURIComponent(e)), this.options.domain && (e += "; domain=" + this.options.domain), this.options.path && (e += "; path=" + this.options.path), this.options.duration) {
            var t = new Date;
            t.setTime(t.getTime() + 24 * this.options.duration * 60 * 60 * 1e3), e += "; expires=" + t.toGMTString()
        }
        return this.options.secure && (e += "; secure"), this.options.document.cookie = this.key + "=" + e, this
    },
    read: function() {
        var e = this.options.document.cookie.match("(?:^|;)\\s*" + this.key.escapeRegExp() + "=([^;]*)");
        return e ? decodeURIComponent(e[1]) : null
    },
    dispose: function() {
        return new Cookie(this.key, Object.merge({}, this.options, {
            duration: -1
        })).write(""), this
    }
});
Cookie.write = function(e, t, n) {
        return new Cookie(e, n).write(t)
    }, Cookie.read = function(e) {
        return new Cookie(e).read()
    }, Cookie.dispose = function(e, t) {
        return new Cookie(e, t).dispose()
    },
    function(e, t) {
        var n, i, s, r, o = [],
            a = t.createElement("div"),
            l = function() {
                clearTimeout(r), n || (Browser.loaded = n = !0, t.removeListener("DOMContentLoaded", l).removeListener("readystatechange", c), t.fireEvent("domready"), e.fireEvent("domready"))
            },
            c = function() {
                for (var e = o.length; e--;)
                    if (o[e]()) return l(), !0;
                return !1
            },
            u = function() {
                clearTimeout(r), c() || (r = setTimeout(u, 10))
            };
        t.addListener("DOMContentLoaded", l);
        var d = function() {
            try {
                return a.doScroll(), !0
            } catch (e) {}
            return !1
        };
        a.doScroll && !d() && (o.push(d), s = !0), t.readyState && o.push(function() {
            var e = t.readyState;
            return "loaded" == e || "complete" == e
        }), "onreadystatechange" in t ? t.addListener("readystatechange", c) : s = !0, s && u(), Element.Events.domready = {
            onAdd: function(e) {
                n && e.call(this)
            }
        }, Element.Events.load = {
            base: "load",
            onAdd: function(t) {
                i && this == e && t.call(this)
            },
            condition: function() {
                return this == e && (l(), delete Element.Events.load), !0
            }
        }, e.addEvent("load", function() {
            i = !0
        })
    }(window, document),
    function() {
        var Swiff = this.Swiff = new Class({
            Implements: Options,
            options: {
                id: null,
                height: 1,
                width: 1,
                container: null,
                properties: {},
                params: {
                    quality: "high",
                    allowScriptAccess: "always",
                    wMode: "window",
                    swLiveConnect: !0
                },
                callBacks: {},
                vars: {}
            },
            toElement: function() {
                return this.object
            },
            initialize: function(e, t) {
                this.instance = "Swiff_" + String.uniqueID(), this.setOptions(t), t = this.options;
                var n = this.id = t.id || this.instance,
                    i = document.id(t.container);
                Swiff.CallBacks[this.instance] = {};
                var s = t.params,
                    r = t.vars,
                    o = t.callBacks,
                    a = Object.append({
                        height: t.height,
                        width: t.width
                    }, t.properties),
                    l = this;
                for (var c in o) Swiff.CallBacks[this.instance][c] = function(e) {
                    return function() {
                        return e.apply(l.object, arguments)
                    }
                }(o[c]), r[c] = "Swiff.CallBacks." + this.instance + "." + c;
                s.flashVars = Object.toQueryString(r), Browser.ie ? (a.classid = "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000", s.movie = e) : a.type = "application/x-shockwave-flash", a.data = e;
                var u = '<object id="' + n + '"';
                for (var d in a) u += " " + d + '="' + a[d] + '"';
                u += ">";
                for (var h in s) s[h] && (u += '<param name="' + h + '" value="' + s[h] + '" />');
                u += "</object>", this.object = (i ? i.empty() : new Element("div")).set("html", u).firstChild
            },
            replaces: function(e) {
                return e = document.id(e, !0), e.parentNode.replaceChild(this.toElement(), e), this
            },
            inject: function(e) {
                return document.id(e, !0).appendChild(this.toElement()), this
            },
            remote: function() {
                return Swiff.remote.apply(Swiff, [this.toElement()].append(arguments))
            }
        });
        Swiff.CallBacks = {}, Swiff.remote = function(obj, fn) {
            var rs = obj.CallFunction('<invoke name="' + fn + '" returntype="javascript">' + __flash__argumentsToXML(arguments, 2) + "</invoke>");
            return eval(rs)
        }
    }(), define("/sites/default/themes/siedler/js/external/mootools-core-1.4.1-full-compat.js", function() {}),
    function(e, t) {
        function n(e) {
            var t = e.length,
                n = ue.type(e);
            return ue.isWindow(e) ? !1 : 1 === e.nodeType && t ? !0 : "array" === n || "function" !== n && (0 === t || "number" == typeof t && t > 0 && t - 1 in e)
        }

        function i(e) {
            var t = Ce[e] = {};
            return ue.each(e.match(he) || [], function(e, n) {
                t[n] = !0
            }), t
        }

        function s(e, n, i, s) {
            if (ue.acceptData(e)) {
                var r, o, a = ue.expando,
                    l = e.nodeType,
                    c = l ? ue.cache : e,
                    u = l ? e[a] : e[a] && a;
                if (u && c[u] && (s || c[u].data) || i !== t || "string" != typeof n) return u || (u = l ? e[a] = te.pop() || ue.guid++ : a), c[u] || (c[u] = l ? {} : {
                    toJSON: ue.noop
                }), ("object" == typeof n || "function" == typeof n) && (s ? c[u] = ue.extend(c[u], n) : c[u].data = ue.extend(c[u].data, n)), o = c[u], s || (o.data || (o.data = {}), o = o.data), i !== t && (o[ue.camelCase(n)] = i), "string" == typeof n ? (r = o[n], null == r && (r = o[ue.camelCase(n)])) : r = o, r
            }
        }

        function r(e, t, n) {
            if (ue.acceptData(e)) {
                var i, s, r = e.nodeType,
                    o = r ? ue.cache : e,
                    l = r ? e[ue.expando] : ue.expando;
                if (o[l]) {
                    if (t && (i = n ? o[l] : o[l].data)) {
                        ue.isArray(t) ? t = t.concat(ue.map(t, ue.camelCase)) : t in i ? t = [t] : (t = ue.camelCase(t), t = t in i ? [t] : t.split(" ")), s = t.length;
                        for (; s--;) delete i[t[s]];
                        if (n ? !a(i) : !ue.isEmptyObject(i)) return
                    }(n || (delete o[l].data, a(o[l]))) && (r ? ue.cleanData([e], !0) : ue.support.deleteExpando || o != o.window ? delete o[l] : o[l] = null)
                }
            }
        }

        function o(e, n, i) {
            if (i === t && 1 === e.nodeType) {
                var s = "data-" + n.replace(je, "-$1").toLowerCase();
                if (i = e.getAttribute(s), "string" == typeof i) {
                    try {
                        i = "true" === i ? !0 : "false" === i ? !1 : "null" === i ? null : +i + "" === i ? +i : ke.test(i) ? ue.parseJSON(i) : i
                    } catch (r) {}
                    ue.data(e, n, i)
                } else i = t
            }
            return i
        }

        function a(e) {
            var t;
            for (t in e)
                if (("data" !== t || !ue.isEmptyObject(e[t])) && "toJSON" !== t) return !1;
            return !0
        }

        function l() {
            return !0
        }

        function c() {
            return !1
        }

        function u() {
            try {
                return Y.activeElement
            } catch (e) {}
        }

        function d(e, t) {
            do e = e[t]; while (e && 1 !== e.nodeType);
            return e
        }

        function h(e, t, n) {
            if (ue.isFunction(t)) return ue.grep(e, function(e, i) {
                return !!t.call(e, i, e) !== n
            });
            if (t.nodeType) return ue.grep(e, function(e) {
                return e === t !== n
            });
            if ("string" == typeof t) {
                if (qe.test(t)) return ue.filter(t, e, n);
                t = ue.filter(t, e)
            }
            return ue.grep(e, function(e) {
                return ue.inArray(e, t) >= 0 !== n
            })
        }

        function m(e) {
            var t = Ve.split("|"),
                n = e.createDocumentFragment();
            if (n.createElement)
                for (; t.length;) n.createElement(t.pop());
            return n
        }

        function f(e, t) {
            return ue.nodeName(e, "table") && ue.nodeName(1 === t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
        }

        function p(e) {
            return e.type = (null !== ue.find.attr(e, "type")) + "/" + e.type, e
        }

        function g(e) {
            var t = st.exec(e.type);
            return t ? e.type = t[1] : e.removeAttribute("type"), e
        }

        function v(e, t) {
            for (var n, i = 0; null != (n = e[i]); i++) ue._data(n, "globalEval", !t || ue._data(t[i], "globalEval"))
        }

        function y(e, t) {
            if (1 === t.nodeType && ue.hasData(e)) {
                var n, i, s, r = ue._data(e),
                    o = ue._data(t, r),
                    a = r.events;
                if (a) {
                    delete o.handle, o.events = {};
                    for (n in a)
                        for (i = 0, s = a[n].length; s > i; i++) ue.event.add(t, n, a[n][i])
                }
                o.data && (o.data = ue.extend({}, o.data))
            }
        }

        function w(e, t) {
            var n, i, s;
            if (1 === t.nodeType) {
                if (n = t.nodeName.toLowerCase(), !ue.support.noCloneEvent && t[ue.expando]) {
                    s = ue._data(t);
                    for (i in s.events) ue.removeEvent(t, i, s.handle);
                    t.removeAttribute(ue.expando)
                }
                "script" === n && t.text !== e.text ? (p(t).text = e.text, g(t)) : "object" === n ? (t.parentNode && (t.outerHTML = e.outerHTML), ue.support.html5Clone && e.innerHTML && !ue.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === n && tt.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : "option" === n ? t.defaultSelected = t.selected = e.defaultSelected : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue)
            }
        }

        function b(e, n) {
            var i, s, r = 0,
                o = typeof e.getElementsByTagName !== J ? e.getElementsByTagName(n || "*") : typeof e.querySelectorAll !== J ? e.querySelectorAll(n || "*") : t;
            if (!o)
                for (o = [], i = e.childNodes || e; null != (s = i[r]); r++) !n || ue.nodeName(s, n) ? o.push(s) : ue.merge(o, b(s, n));
            return n === t || n && ue.nodeName(e, n) ? ue.merge([e], o) : o
        }

        function _(e) {
            tt.test(e.type) && (e.defaultChecked = e.checked)
        }

        function x(e, t) {
            if (t in e) return t;
            for (var n = t.charAt(0).toUpperCase() + t.slice(1), i = t, s = St.length; s--;)
                if (t = St[s] + n, t in e) return t;
            return i
        }

        function E(e, t) {
            return e = t || e, "none" === ue.css(e, "display") || !ue.contains(e.ownerDocument, e)
        }

        function S(e, t) {
            for (var n, i, s, r = [], o = 0, a = e.length; a > o; o++) i = e[o], i.style && (r[o] = ue._data(i, "olddisplay"), n = i.style.display, t ? (r[o] || "none" !== n || (i.style.display = ""), "" === i.style.display && E(i) && (r[o] = ue._data(i, "olddisplay", T(i.nodeName)))) : r[o] || (s = E(i), (n && "none" !== n || !s) && ue._data(i, "olddisplay", s ? n : ue.css(i, "display"))));
            for (o = 0; a > o; o++) i = e[o], i.style && (t && "none" !== i.style.display && "" !== i.style.display || (i.style.display = t ? r[o] || "" : "none"));
            return e
        }

        function C(e, t, n) {
            var i = vt.exec(t);
            return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || "px") : t
        }

        function k(e, t, n, i, s) {
            for (var r = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0, o = 0; 4 > r; r += 2) "margin" === n && (o += ue.css(e, n + Et[r], !0, s)), i ? ("content" === n && (o -= ue.css(e, "padding" + Et[r], !0, s)), "margin" !== n && (o -= ue.css(e, "border" + Et[r] + "Width", !0, s))) : (o += ue.css(e, "padding" + Et[r], !0, s), "padding" !== n && (o += ue.css(e, "border" + Et[r] + "Width", !0, s)));
            return o
        }

        function j(e, t, n) {
            var i = !0,
                s = "width" === t ? e.offsetWidth : e.offsetHeight,
                r = ut(e),
                o = ue.support.boxSizing && "border-box" === ue.css(e, "boxSizing", !1, r);
            if (0 >= s || null == s) {
                if (s = dt(e, t, r), (0 > s || null == s) && (s = e.style[t]), yt.test(s)) return s;
                i = o && (ue.support.boxSizingReliable || s === e.style[t]), s = parseFloat(s) || 0
            }
            return s + k(e, t, n || (o ? "border" : "content"), i, r) + "px"
        }

        function T(e) {
            var t = Y,
                n = bt[e];
            return n || (n = P(e, t), "none" !== n && n || (ct = (ct || ue("<iframe frameborder='0' width='0' height='0'/>").css("cssText", "display:block !important")).appendTo(t.documentElement), t = (ct[0].contentWindow || ct[0].contentDocument).document, t.write("<!doctype html><html><body>"), t.close(), n = P(e, t), ct.detach()), bt[e] = n), n
        }

        function P(e, t) {
            var n = ue(t.createElement(e)).appendTo(t.body),
                i = ue.css(n[0], "display");
            return n.remove(), i
        }

        function O(e, t, n, i) {
            var s;
            if (ue.isArray(t)) ue.each(t, function(t, s) {
                n || kt.test(e) ? i(e, s) : O(e + "[" + ("object" == typeof s ? t : "") + "]", s, n, i)
            });
            else if (n || "object" !== ue.type(t)) i(e, t);
            else
                for (s in t) O(e + "[" + s + "]", t[s], n, i)
        }

        function $(e) {
            return function(t, n) {
                "string" != typeof t && (n = t, t = "*");
                var i, s = 0,
                    r = t.toLowerCase().match(he) || [];
                if (ue.isFunction(n))
                    for (; i = r[s++];) "+" === i[0] ? (i = i.slice(1) || "*", (e[i] = e[i] || []).unshift(n)) : (e[i] = e[i] || []).push(n)
            }
        }

        function A(e, t, n, i) {
            function s(a) {
                var l;
                return r[a] = !0, ue.each(e[a] || [], function(e, a) {
                    var c = a(t, n, i);
                    return "string" != typeof c || o || r[c] ? o ? !(l = c) : void 0 : (t.dataTypes.unshift(c), s(c), !1)
                }), l
            }
            var r = {},
                o = e === zt;
            return s(t.dataTypes[0]) || !r["*"] && s("*")
        }

        function D(e, n) {
            var i, s, r = ue.ajaxSettings.flatOptions || {};
            for (s in n) n[s] !== t && ((r[s] ? e : i || (i = {}))[s] = n[s]);
            return i && ue.extend(!0, e, i), e
        }

        function N(e, n, i) {
            for (var s, r, o, a, l = e.contents, c = e.dataTypes;
                "*" === c[0];) c.shift(), r === t && (r = e.mimeType || n.getResponseHeader("Content-Type"));
            if (r)
                for (a in l)
                    if (l[a] && l[a].test(r)) {
                        c.unshift(a);
                        break
                    }
            if (c[0] in i) o = c[0];
            else {
                for (a in i) {
                    if (!c[0] || e.converters[a + " " + c[0]]) {
                        o = a;
                        break
                    }
                    s || (s = a)
                }
                o = o || s
            }
            return o ? (o !== c[0] && c.unshift(o), i[o]) : void 0
        }

        function L(e, t, n, i) {
            var s, r, o, a, l, c = {},
                u = e.dataTypes.slice();
            if (u[1])
                for (o in e.converters) c[o.toLowerCase()] = e.converters[o];
            for (r = u.shift(); r;)
                if (e.responseFields[r] && (n[e.responseFields[r]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = r, r = u.shift())
                    if ("*" === r) r = l;
                    else if ("*" !== l && l !== r) {
                if (o = c[l + " " + r] || c["* " + r], !o)
                    for (s in c)
                        if (a = s.split(" "), a[1] === r && (o = c[l + " " + a[0]] || c["* " + a[0]])) {
                            o === !0 ? o = c[s] : c[s] !== !0 && (r = a[0], u.unshift(a[1]));
                            break
                        }
                if (o !== !0)
                    if (o && e["throws"]) t = o(t);
                    else try {
                        t = o(t)
                    } catch (d) {
                        return {
                            state: "parsererror",
                            error: o ? d : "No conversion from " + l + " to " + r
                        }
                    }
            }
            return {
                state: "success",
                data: t
            }
        }

        function F() {
            try {
                return new e.XMLHttpRequest
            } catch (t) {}
        }

        function I() {
            try {
                return new e.ActiveXObject("Microsoft.XMLHTTP")
            } catch (t) {}
        }

        function M() {
            return setTimeout(function() {
                Kt = t
            }), Kt = ue.now()
        }

        function B(e, t, n) {
            for (var i, s = (rn[t] || []).concat(rn["*"]), r = 0, o = s.length; o > r; r++)
                if (i = s[r].call(n, t, e)) return i
        }

        function R(e, t, n) {
            var i, s, r = 0,
                o = sn.length,
                a = ue.Deferred().always(function() {
                    delete l.elem
                }),
                l = function() {
                    if (s) return !1;
                    for (var t = Kt || M(), n = Math.max(0, c.startTime + c.duration - t), i = n / c.duration || 0, r = 1 - i, o = 0, l = c.tweens.length; l > o; o++) c.tweens[o].run(r);
                    return a.notifyWith(e, [c, r, n]), 1 > r && l ? n : (a.resolveWith(e, [c]), !1)
                },
                c = a.promise({
                    elem: e,
                    props: ue.extend({}, t),
                    opts: ue.extend(!0, {
                        specialEasing: {}
                    }, n),
                    originalProperties: t,
                    originalOptions: n,
                    startTime: Kt || M(),
                    duration: n.duration,
                    tweens: [],
                    createTween: function(t, n) {
                        var i = ue.Tween(e, c.opts, t, n, c.opts.specialEasing[t] || c.opts.easing);
                        return c.tweens.push(i), i
                    },
                    stop: function(t) {
                        var n = 0,
                            i = t ? c.tweens.length : 0;
                        if (s) return this;
                        for (s = !0; i > n; n++) c.tweens[n].run(1);
                        return t ? a.resolveWith(e, [c, t]) : a.rejectWith(e, [c, t]), this
                    }
                }),
                u = c.props;
            for (H(u, c.opts.specialEasing); o > r; r++)
                if (i = sn[r].call(c, e, u, c.opts)) return i;
            return ue.map(u, B, c), ue.isFunction(c.opts.start) && c.opts.start.call(e, c), ue.fx.timer(ue.extend(l, {
                elem: e,
                anim: c,
                queue: c.opts.queue
            })), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
        }

        function H(e, t) {
            var n, i, s, r, o;
            for (n in e)
                if (i = ue.camelCase(n), s = t[i], r = e[n], ue.isArray(r) && (s = r[1], r = e[n] = r[0]), n !== i && (e[i] = r, delete e[n]), o = ue.cssHooks[i], o && "expand" in o) {
                    r = o.expand(r), delete e[i];
                    for (n in r) n in e || (e[n] = r[n], t[n] = s)
                } else t[i] = s
        }

        function q(e, t, n) {
            var i, s, r, o, a, l, c = this,
                u = {},
                d = e.style,
                h = e.nodeType && E(e),
                m = ue._data(e, "fxshow");
            n.queue || (a = ue._queueHooks(e, "fx"), null == a.unqueued && (a.unqueued = 0, l = a.empty.fire, a.empty.fire = function() {
                a.unqueued || l()
            }), a.unqueued++, c.always(function() {
                c.always(function() {
                    a.unqueued--, ue.queue(e, "fx").length || a.empty.fire()
                })
            })), 1 === e.nodeType && ("height" in t || "width" in t) && (n.overflow = [d.overflow, d.overflowX, d.overflowY], "inline" === ue.css(e, "display") && "none" === ue.css(e, "float") && (ue.support.inlineBlockNeedsLayout && "inline" !== T(e.nodeName) ? d.zoom = 1 : d.display = "inline-block")), n.overflow && (d.overflow = "hidden", ue.support.shrinkWrapBlocks || c.always(function() {
                d.overflow = n.overflow[0], d.overflowX = n.overflow[1], d.overflowY = n.overflow[2]
            }));
            for (i in t)
                if (s = t[i], en.exec(s)) {
                    if (delete t[i], r = r || "toggle" === s, s === (h ? "hide" : "show")) continue;
                    u[i] = m && m[i] || ue.style(e, i)
                }
            if (!ue.isEmptyObject(u)) {
                m ? "hidden" in m && (h = m.hidden) : m = ue._data(e, "fxshow", {}), r && (m.hidden = !h), h ? ue(e).show() : c.done(function() {
                    ue(e).hide()
                }), c.done(function() {
                    var t;
                    ue._removeData(e, "fxshow");
                    for (t in u) ue.style(e, t, u[t])
                });
                for (i in u) o = B(h ? m[i] : 0, i, c), i in m || (m[i] = o.start, h && (o.end = o.start, o.start = "width" === i || "height" === i ? 1 : 0))
            }
        }

        function z(e, t, n, i, s) {
            return new z.prototype.init(e, t, n, i, s)
        }

        function U(e, t) {
            var n, i = {
                    height: e
                },
                s = 0;
            for (t = t ? 1 : 0; 4 > s; s += 2 - t) n = Et[s], i["margin" + n] = i["padding" + n] = e;
            return t && (i.opacity = i.width = e), i
        }

        function W(e) {
            return ue.isWindow(e) ? e : 9 === e.nodeType ? e.defaultView || e.parentWindow : !1
        }
        var V, G, J = typeof t,
            X = e.location,
            Y = e.document,
            Q = Y.documentElement,
            K = e.jQuery,
            Z = e.$,
            ee = {},
            te = [],
            ne = "1.10.2",
            ie = te.concat,
            se = te.push,
            re = te.slice,
            oe = te.indexOf,
            ae = ee.toString,
            le = ee.hasOwnProperty,
            ce = ne.trim,
            ue = function(e, t) {
                return new ue.fn.init(e, t, G)
            },
            de = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
            he = /\S+/g,
            me = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
            fe = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
            pe = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
            ge = /^[\],:{}\s]*$/,
            ve = /(?:^|:|,)(?:\s*\[)+/g,
            ye = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
            we = /"[^"\\\r\n]*"|true|false|null|-?(?:\d+\.|)\d+(?:[eE][+-]?\d+|)/g,
            be = /^-ms-/,
            _e = /-([\da-z])/gi,
            xe = function(e, t) {
                return t.toUpperCase()
            },
            Ee = function(e) {
                (Y.addEventListener || "load" === e.type || "complete" === Y.readyState) && (Se(), ue.ready())
            },
            Se = function() {
                Y.addEventListener ? (Y.removeEventListener("DOMContentLoaded", Ee, !1), e.removeEventListener("load", Ee, !1)) : (Y.detachEvent("onreadystatechange", Ee), e.detachEvent("onload", Ee))
            };
        ue.fn = ue.prototype = {
                jquery: ne,
                constructor: ue,
                init: function(e, n, i) {
                    var s, r;
                    if (!e) return this;
                    if ("string" == typeof e) {
                        if (s = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : fe.exec(e), !s || !s[1] && n) return !n || n.jquery ? (n || i).find(e) : this.constructor(n).find(e);
                        if (s[1]) {
                            if (n = n instanceof ue ? n[0] : n, ue.merge(this, ue.parseHTML(s[1], n && n.nodeType ? n.ownerDocument || n : Y, !0)), pe.test(s[1]) && ue.isPlainObject(n))
                                for (s in n) ue.isFunction(this[s]) ? this[s](n[s]) : this.attr(s, n[s]);
                            return this
                        }
                        if (r = Y.getElementById(s[2]), r && r.parentNode) {
                            if (r.id !== s[2]) return i.find(e);
                            this.length = 1, this[0] = r
                        }
                        return this.context = Y, this.selector = e, this
                    }
                    return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : ue.isFunction(e) ? i.ready(e) : (e.selector !== t && (this.selector = e.selector, this.context = e.context), ue.makeArray(e, this))
                },
                selector: "",
                length: 0,
                toArray: function() {
                    return re.call(this)
                },
                get: function(e) {
                    return null == e ? this.toArray() : 0 > e ? this[this.length + e] : this[e]
                },
                pushStack: function(e) {
                    var t = ue.merge(this.constructor(), e);
                    return t.prevObject = this, t.context = this.context, t
                },
                each: function(e, t) {
                    return ue.each(this, e, t)
                },
                ready: function(e) {
                    return ue.ready.promise().done(e), this
                },
                slice: function() {
                    return this.pushStack(re.apply(this, arguments))
                },
                first: function() {
                    return this.eq(0)
                },
                last: function() {
                    return this.eq(-1)
                },
                eq: function(e) {
                    var t = this.length,
                        n = +e + (0 > e ? t : 0);
                    return this.pushStack(n >= 0 && t > n ? [this[n]] : [])
                },
                map: function(e) {
                    return this.pushStack(ue.map(this, function(t, n) {
                        return e.call(t, n, t)
                    }))
                },
                end: function() {
                    return this.prevObject || this.constructor(null)
                },
                push: se,
                sort: [].sort,
                splice: [].splice
            }, ue.fn.init.prototype = ue.fn, ue.extend = ue.fn.extend = function() {
                var e, n, i, s, r, o, a = arguments[0] || {},
                    l = 1,
                    c = arguments.length,
                    u = !1;
                for ("boolean" == typeof a && (u = a, a = arguments[1] || {}, l = 2), "object" == typeof a || ue.isFunction(a) || (a = {}), c === l && (a = this, --l); c > l; l++)
                    if (null != (r = arguments[l]))
                        for (s in r) e = a[s], i = r[s], a !== i && (u && i && (ue.isPlainObject(i) || (n = ue.isArray(i))) ? (n ? (n = !1, o = e && ue.isArray(e) ? e : []) : o = e && ue.isPlainObject(e) ? e : {}, a[s] = ue.extend(u, o, i)) : i !== t && (a[s] = i));
                return a
            }, ue.extend({
                expando: "jQuery" + (ne + Math.random()).replace(/\D/g, ""),
                noConflict: function(t) {
                    return e.$ === ue && (e.$ = Z), t && e.jQuery === ue && (e.jQuery = K), ue
                },
                isReady: !1,
                readyWait: 1,
                holdReady: function(e) {
                    e ? ue.readyWait++ : ue.ready(!0)
                },
                ready: function(e) {
                    if (e === !0 ? !--ue.readyWait : !ue.isReady) {
                        if (!Y.body) return setTimeout(ue.ready);
                        ue.isReady = !0, e !== !0 && --ue.readyWait > 0 || (V.resolveWith(Y, [ue]), ue.fn.trigger && ue(Y).trigger("ready").off("ready"))
                    }
                },
                isFunction: function(e) {
                    return "function" === ue.type(e)
                },
                isArray: Array.isArray || function(e) {
                    return "array" === ue.type(e)
                },
                isWindow: function(e) {
                    return null != e && e == e.window
                },
                isNumeric: function(e) {
                    return !isNaN(parseFloat(e)) && isFinite(e)
                },
                type: function(e) {
                    return null == e ? String(e) : "object" == typeof e || "function" == typeof e ? ee[ae.call(e)] || "object" : typeof e
                },
                isPlainObject: function(e) {
                    var n;
                    if (!e || "object" !== ue.type(e) || e.nodeType || ue.isWindow(e)) return !1;
                    try {
                        if (e.constructor && !le.call(e, "constructor") && !le.call(e.constructor.prototype, "isPrototypeOf")) return !1
                    } catch (i) {
                        return !1
                    }
                    if (ue.support.ownLast)
                        for (n in e) return le.call(e, n);
                    for (n in e);
                    return n === t || le.call(e, n)
                },
                isEmptyObject: function(e) {
                    var t;
                    for (t in e) return !1;
                    return !0
                },
                error: function(e) {
                    throw new Error(e)
                },
                parseHTML: function(e, t, n) {
                    if (!e || "string" != typeof e) return null;
                    "boolean" == typeof t && (n = t, t = !1), t = t || Y;
                    var i = pe.exec(e),
                        s = !n && [];
                    return i ? [t.createElement(i[1])] : (i = ue.buildFragment([e], t, s), s && ue(s).remove(), ue.merge([], i.childNodes))
                },
                parseJSON: function(t) {
                    return e.JSON && e.JSON.parse ? e.JSON.parse(t) : null === t ? t : "string" == typeof t && (t = ue.trim(t), t && ge.test(t.replace(ye, "@").replace(we, "]").replace(ve, ""))) ? new Function("return " + t)() : void ue.error("Invalid JSON: " + t)
                },
                parseXML: function(n) {
                    var i, s;
                    if (!n || "string" != typeof n) return null;
                    try {
                        e.DOMParser ? (s = new DOMParser, i = s.parseFromString(n, "text/xml")) : (i = new ActiveXObject("Microsoft.XMLDOM"), i.async = "false", i.loadXML(n))
                    } catch (r) {
                        i = t
                    }
                    return i && i.documentElement && !i.getElementsByTagName("parsererror").length || ue.error("Invalid XML: " + n), i
                },
                noop: function() {},
                globalEval: function(t) {
                    t && ue.trim(t) && (e.execScript || function(t) {
                        e.eval.call(e, t)
                    })(t)
                },
                camelCase: function(e) {
                    return e.replace(be, "ms-").replace(_e, xe)
                },
                nodeName: function(e, t) {
                    return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
                },
                each: function(e, t, i) {
                    var s, r = 0,
                        o = e.length,
                        a = n(e);
                    if (i) {
                        if (a)
                            for (; o > r && (s = t.apply(e[r], i), s !== !1); r++);
                        else
                            for (r in e)
                                if (s = t.apply(e[r], i), s === !1) break
                    } else if (a)
                        for (; o > r && (s = t.call(e[r], r, e[r]), s !== !1); r++);
                    else
                        for (r in e)
                            if (s = t.call(e[r], r, e[r]), s === !1) break; return e
                },
                trim: ce && !ce.call("\ufeff ") ? function(e) {
                    return null == e ? "" : ce.call(e)
                } : function(e) {
                    return null == e ? "" : (e + "").replace(me, "")
                },
                makeArray: function(e, t) {
                    var i = t || [];
                    return null != e && (n(Object(e)) ? ue.merge(i, "string" == typeof e ? [e] : e) : se.call(i, e)), i
                },
                inArray: function(e, t, n) {
                    var i;
                    if (t) {
                        if (oe) return oe.call(t, e, n);
                        for (i = t.length, n = n ? 0 > n ? Math.max(0, i + n) : n : 0; i > n; n++)
                            if (n in t && t[n] === e) return n
                    }
                    return -1
                },
                merge: function(e, n) {
                    var i = n.length,
                        s = e.length,
                        r = 0;
                    if ("number" == typeof i)
                        for (; i > r; r++) e[s++] = n[r];
                    else
                        for (; n[r] !== t;) e[s++] = n[r++];
                    return e.length = s, e
                },
                grep: function(e, t, n) {
                    var i, s = [],
                        r = 0,
                        o = e.length;
                    for (n = !!n; o > r; r++) i = !!t(e[r], r), n !== i && s.push(e[r]);
                    return s
                },
                map: function(e, t, i) {
                    var s, r = 0,
                        o = e.length,
                        a = n(e),
                        l = [];
                    if (a)
                        for (; o > r; r++) s = t(e[r], r, i), null != s && (l[l.length] = s);
                    else
                        for (r in e) s = t(e[r], r, i), null != s && (l[l.length] = s);
                    return ie.apply([], l)
                },
                guid: 1,
                proxy: function(e, n) {
                    var i, s, r;
                    return "string" == typeof n && (r = e[n], n = e, e = r), ue.isFunction(e) ? (i = re.call(arguments, 2), s = function() {
                        return e.apply(n || this, i.concat(re.call(arguments)))
                    }, s.guid = e.guid = e.guid || ue.guid++, s) : t
                },
                access: function(e, n, i, s, r, o, a) {
                    var l = 0,
                        c = e.length,
                        u = null == i;
                    if ("object" === ue.type(i)) {
                        r = !0;
                        for (l in i) ue.access(e, n, l, i[l], !0, o, a)
                    } else if (s !== t && (r = !0, ue.isFunction(s) || (a = !0), u && (a ? (n.call(e, s), n = null) : (u = n, n = function(e, t, n) {
                            return u.call(ue(e), n)
                        })), n))
                        for (; c > l; l++) n(e[l], i, a ? s : s.call(e[l], l, n(e[l], i)));
                    return r ? e : u ? n.call(e) : c ? n(e[0], i) : o
                },
                now: function() {
                    return (new Date).getTime()
                },
                swap: function(e, t, n, i) {
                    var s, r, o = {};
                    for (r in t) o[r] = e.style[r], e.style[r] = t[r];
                    s = n.apply(e, i || []);
                    for (r in t) e.style[r] = o[r];
                    return s
                }
            }), ue.ready.promise = function(t) {
                if (!V)
                    if (V = ue.Deferred(), "complete" === Y.readyState) setTimeout(ue.ready);
                    else if (Y.addEventListener) Y.addEventListener("DOMContentLoaded", Ee, !1), e.addEventListener("load", Ee, !1);
                else {
                    Y.attachEvent("onreadystatechange", Ee), e.attachEvent("onload", Ee);
                    var n = !1;
                    try {
                        n = null == e.frameElement && Y.documentElement
                    } catch (i) {}
                    n && n.doScroll && ! function s() {
                        if (!ue.isReady) {
                            try {
                                n.doScroll("left")
                            } catch (e) {
                                return setTimeout(s, 50)
                            }
                            Se(), ue.ready()
                        }
                    }()
                }
                return V.promise(t)
            }, ue.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, t) {
                ee["[object " + t + "]"] = t.toLowerCase()
            }), G = ue(Y),
            function(e, t) {
                function n(e, t, n, i) {
                    var s, r, o, a, l, c, u, d, f, p;
                    if ((t ? t.ownerDocument || t : R) !== A && $(t), t = t || A, n = n || [], !e || "string" != typeof e) return n;
                    if (1 !== (a = t.nodeType) && 9 !== a) return [];
                    if (N && !i) {
                        if (s = we.exec(e))
                            if (o = s[1]) {
                                if (9 === a) {
                                    if (r = t.getElementById(o), !r || !r.parentNode) return n;
                                    if (r.id === o) return n.push(r), n
                                } else if (t.ownerDocument && (r = t.ownerDocument.getElementById(o)) && M(t, r) && r.id === o) return n.push(r), n
                            } else {
                                if (s[2]) return ee.apply(n, t.getElementsByTagName(e)), n;
                                if ((o = s[3]) && E.getElementsByClassName && t.getElementsByClassName) return ee.apply(n, t.getElementsByClassName(o)), n
                            }
                        if (E.qsa && (!L || !L.test(e))) {
                            if (d = u = B, f = t, p = 9 === a && e, 1 === a && "object" !== t.nodeName.toLowerCase()) {
                                for (c = h(e), (u = t.getAttribute("id")) ? d = u.replace(xe, "\\$&") : t.setAttribute("id", d), d = "[id='" + d + "'] ", l = c.length; l--;) c[l] = d + m(c[l]);
                                f = me.test(e) && t.parentNode || t, p = c.join(",")
                            }
                            if (p) try {
                                return ee.apply(n, f.querySelectorAll(p)), n
                            } catch (g) {} finally {
                                u || t.removeAttribute("id")
                            }
                        }
                    }
                    return _(e.replace(ce, "$1"), t, n, i)
                }

                function i() {
                    function e(n, i) {
                        return t.push(n += " ") > C.cacheLength && delete e[t.shift()], e[n] = i
                    }
                    var t = [];
                    return e
                }

                function s(e) {
                    return e[B] = !0, e
                }

                function r(e) {
                    var t = A.createElement("div");
                    try {
                        return !!e(t)
                    } catch (n) {
                        return !1
                    } finally {
                        t.parentNode && t.parentNode.removeChild(t), t = null
                    }
                }

                function o(e, t) {
                    for (var n = e.split("|"), i = e.length; i--;) C.attrHandle[n[i]] = t
                }

                function a(e, t) {
                    var n = t && e,
                        i = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || X) - (~e.sourceIndex || X);
                    if (i) return i;
                    if (n)
                        for (; n = n.nextSibling;)
                            if (n === t) return -1;
                    return e ? 1 : -1
                }

                function l(e) {
                    return function(t) {
                        var n = t.nodeName.toLowerCase();
                        return "input" === n && t.type === e
                    }
                }

                function c(e) {
                    return function(t) {
                        var n = t.nodeName.toLowerCase();
                        return ("input" === n || "button" === n) && t.type === e
                    }
                }

                function u(e) {
                    return s(function(t) {
                        return t = +t, s(function(n, i) {
                            for (var s, r = e([], n.length, t), o = r.length; o--;) n[s = r[o]] && (n[s] = !(i[s] = n[s]))
                        })
                    })
                }

                function d() {}

                function h(e, t) {
                    var i, s, r, o, a, l, c, u = U[e + " "];
                    if (u) return t ? 0 : u.slice(0);
                    for (a = e, l = [], c = C.preFilter; a;) {
                        (!i || (s = de.exec(a))) && (s && (a = a.slice(s[0].length) || a), l.push(r = [])), i = !1, (s = he.exec(a)) && (i = s.shift(), r.push({
                            value: i,
                            type: s[0].replace(ce, " ")
                        }), a = a.slice(i.length));
                        for (o in C.filter) !(s = ve[o].exec(a)) || c[o] && !(s = c[o](s)) || (i = s.shift(), r.push({
                            value: i,
                            type: o,
                            matches: s
                        }), a = a.slice(i.length));
                        if (!i) break
                    }
                    return t ? a.length : a ? n.error(e) : U(e, l).slice(0)
                }

                function m(e) {
                    for (var t = 0, n = e.length, i = ""; n > t; t++) i += e[t].value;
                    return i
                }

                function f(e, t, n) {
                    var i = t.dir,
                        s = n && "parentNode" === i,
                        r = q++;
                    return t.first ? function(t, n, r) {
                        for (; t = t[i];)
                            if (1 === t.nodeType || s) return e(t, n, r)
                    } : function(t, n, o) {
                        var a, l, c, u = H + " " + r;
                        if (o) {
                            for (; t = t[i];)
                                if ((1 === t.nodeType || s) && e(t, n, o)) return !0
                        } else
                            for (; t = t[i];)
                                if (1 === t.nodeType || s)
                                    if (c = t[B] || (t[B] = {}), (l = c[i]) && l[0] === u) {
                                        if ((a = l[1]) === !0 || a === S) return a === !0
                                    } else if (l = c[i] = [u], l[1] = e(t, n, o) || S, l[1] === !0) return !0
                    }
                }

                function p(e) {
                    return e.length > 1 ? function(t, n, i) {
                        for (var s = e.length; s--;)
                            if (!e[s](t, n, i)) return !1;
                        return !0
                    } : e[0]
                }

                function g(e, t, n, i, s) {
                    for (var r, o = [], a = 0, l = e.length, c = null != t; l > a; a++)(r = e[a]) && (!n || n(r, i, s)) && (o.push(r), c && t.push(a));
                    return o
                }

                function v(e, t, n, i, r, o) {
                    return i && !i[B] && (i = v(i)), r && !r[B] && (r = v(r, o)), s(function(s, o, a, l) {
                        var c, u, d, h = [],
                            m = [],
                            f = o.length,
                            p = s || b(t || "*", a.nodeType ? [a] : a, []),
                            v = !e || !s && t ? p : g(p, h, e, a, l),
                            y = n ? r || (s ? e : f || i) ? [] : o : v;
                        if (n && n(v, y, a, l), i)
                            for (c = g(y, m), i(c, [], a, l), u = c.length; u--;)(d = c[u]) && (y[m[u]] = !(v[m[u]] = d));
                        if (s) {
                            if (r || e) {
                                if (r) {
                                    for (c = [], u = y.length; u--;)(d = y[u]) && c.push(v[u] = d);
                                    r(null, y = [], c, l)
                                }
                                for (u = y.length; u--;)(d = y[u]) && (c = r ? ne.call(s, d) : h[u]) > -1 && (s[c] = !(o[c] = d))
                            }
                        } else y = g(y === o ? y.splice(f, y.length) : y), r ? r(null, o, y, l) : ee.apply(o, y)
                    })
                }

                function y(e) {
                    for (var t, n, i, s = e.length, r = C.relative[e[0].type], o = r || C.relative[" "], a = r ? 1 : 0, l = f(function(e) {
                            return e === t
                        }, o, !0), c = f(function(e) {
                            return ne.call(t, e) > -1
                        }, o, !0), u = [function(e, n, i) {
                            return !r && (i || n !== P) || ((t = n).nodeType ? l(e, n, i) : c(e, n, i))
                        }]; s > a; a++)
                        if (n = C.relative[e[a].type]) u = [f(p(u), n)];
                        else {
                            if (n = C.filter[e[a].type].apply(null, e[a].matches), n[B]) {
                                for (i = ++a; s > i && !C.relative[e[i].type]; i++);
                                return v(a > 1 && p(u), a > 1 && m(e.slice(0, a - 1).concat({
                                    value: " " === e[a - 2].type ? "*" : ""
                                })).replace(ce, "$1"), n, i > a && y(e.slice(a, i)), s > i && y(e = e.slice(i)), s > i && m(e))
                            }
                            u.push(n)
                        }
                    return p(u)
                }

                function w(e, t) {
                    var i = 0,
                        r = t.length > 0,
                        o = e.length > 0,
                        a = function(s, a, l, c, u) {
                            var d, h, m, f = [],
                                p = 0,
                                v = "0",
                                y = s && [],
                                w = null != u,
                                b = P,
                                _ = s || o && C.find.TAG("*", u && a.parentNode || a),
                                x = H += null == b ? 1 : Math.random() || .1;
                            for (w && (P = a !== A && a, S = i); null != (d = _[v]); v++) {
                                if (o && d) {
                                    for (h = 0; m = e[h++];)
                                        if (m(d, a, l)) {
                                            c.push(d);
                                            break
                                        }
                                    w && (H = x, S = ++i)
                                }
                                r && ((d = !m && d) && p--, s && y.push(d))
                            }
                            if (p += v, r && v !== p) {
                                for (h = 0; m = t[h++];) m(y, f, a, l);
                                if (s) {
                                    if (p > 0)
                                        for (; v--;) y[v] || f[v] || (f[v] = K.call(c));
                                    f = g(f)
                                }
                                ee.apply(c, f), w && !s && f.length > 0 && p + t.length > 1 && n.uniqueSort(c)
                            }
                            return w && (H = x, P = b), y
                        };
                    return r ? s(a) : a
                }

                function b(e, t, i) {
                    for (var s = 0, r = t.length; r > s; s++) n(e, t[s], i);
                    return i
                }

                function _(e, t, n, i) {
                    var s, r, o, a, l, c = h(e);
                    if (!i && 1 === c.length) {
                        if (r = c[0] = c[0].slice(0), r.length > 2 && "ID" === (o = r[0]).type && E.getById && 9 === t.nodeType && N && C.relative[r[1].type]) {
                            if (t = (C.find.ID(o.matches[0].replace(Ee, Se), t) || [])[0], !t) return n;
                            e = e.slice(r.shift().value.length)
                        }
                        for (s = ve.needsContext.test(e) ? 0 : r.length; s-- && (o = r[s], !C.relative[a = o.type]);)
                            if ((l = C.find[a]) && (i = l(o.matches[0].replace(Ee, Se), me.test(r[0].type) && t.parentNode || t))) {
                                if (r.splice(s, 1), e = i.length && m(r), !e) return ee.apply(n, i), n;
                                break
                            }
                    }
                    return T(e, c)(i, t, !N, n, me.test(e)), n
                }
                var x, E, S, C, k, j, T, P, O, $, A, D, N, L, F, I, M, B = "sizzle" + -new Date,
                    R = e.document,
                    H = 0,
                    q = 0,
                    z = i(),
                    U = i(),
                    W = i(),
                    V = !1,
                    G = function(e, t) {
                        return e === t ? (V = !0, 0) : 0
                    },
                    J = typeof t,
                    X = 1 << 31,
                    Y = {}.hasOwnProperty,
                    Q = [],
                    K = Q.pop,
                    Z = Q.push,
                    ee = Q.push,
                    te = Q.slice,
                    ne = Q.indexOf || function(e) {
                        for (var t = 0, n = this.length; n > t; t++)
                            if (this[t] === e) return t;
                        return -1
                    },
                    ie = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                    se = "[\\x20\\t\\r\\n\\f]",
                    re = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
                    oe = re.replace("w", "w#"),
                    ae = "\\[" + se + "*(" + re + ")" + se + "*(?:([*^$|!~]?=)" + se + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + oe + ")|)|)" + se + "*\\]",
                    le = ":(" + re + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + ae.replace(3, 8) + ")*)|.*)\\)|)",
                    ce = new RegExp("^" + se + "+|((?:^|[^\\\\])(?:\\\\.)*)" + se + "+$", "g"),
                    de = new RegExp("^" + se + "*," + se + "*"),
                    he = new RegExp("^" + se + "*([>+~]|" + se + ")" + se + "*"),
                    me = new RegExp(se + "*[+~]"),
                    fe = new RegExp("=" + se + "*([^\\]'\"]*)" + se + "*\\]", "g"),
                    pe = new RegExp(le),
                    ge = new RegExp("^" + oe + "$"),
                    ve = {
                        ID: new RegExp("^#(" + re + ")"),
                        CLASS: new RegExp("^\\.(" + re + ")"),
                        TAG: new RegExp("^(" + re.replace("w", "w*") + ")"),
                        ATTR: new RegExp("^" + ae),
                        PSEUDO: new RegExp("^" + le),
                        CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + se + "*(even|odd|(([+-]|)(\\d*)n|)" + se + "*(?:([+-]|)" + se + "*(\\d+)|))" + se + "*\\)|)", "i"),
                        bool: new RegExp("^(?:" + ie + ")$", "i"),
                        needsContext: new RegExp("^" + se + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + se + "*((?:-\\d)?\\d*)" + se + "*\\)|)(?=[^-]|$)", "i")
                    },
                    ye = /^[^{]+\{\s*\[native \w/,
                    we = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                    be = /^(?:input|select|textarea|button)$/i,
                    _e = /^h\d$/i,
                    xe = /'|\\/g,
                    Ee = new RegExp("\\\\([\\da-f]{1,6}" + se + "?|(" + se + ")|.)", "ig"),
                    Se = function(e, t, n) {
                        var i = "0x" + t - 65536;
                        return i !== i || n ? t : 0 > i ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
                    };
                try {
                    ee.apply(Q = te.call(R.childNodes), R.childNodes), Q[R.childNodes.length].nodeType
                } catch (Ce) {
                    ee = {
                        apply: Q.length ? function(e, t) {
                            Z.apply(e, te.call(t))
                        } : function(e, t) {
                            for (var n = e.length, i = 0; e[n++] = t[i++];);
                            e.length = n - 1
                        }
                    }
                }
                j = n.isXML = function(e) {
                    var t = e && (e.ownerDocument || e).documentElement;
                    return t ? "HTML" !== t.nodeName : !1
                }, E = n.support = {}, $ = n.setDocument = function(e) {
                    var t = e ? e.ownerDocument || e : R,
                        n = t.defaultView;
                    return t !== A && 9 === t.nodeType && t.documentElement ? (A = t, D = t.documentElement, N = !j(t), n && n.attachEvent && n !== n.top && n.attachEvent("onbeforeunload", function() {
                        $()
                    }), E.attributes = r(function(e) {
                        return e.className = "i", !e.getAttribute("className")
                    }), E.getElementsByTagName = r(function(e) {
                        return e.appendChild(t.createComment("")), !e.getElementsByTagName("*").length
                    }), E.getElementsByClassName = r(function(e) {
                        return e.innerHTML = "<div class='a'></div><div class='a i'></div>", e.firstChild.className = "i", 2 === e.getElementsByClassName("i").length
                    }), E.getById = r(function(e) {
                        return D.appendChild(e).id = B, !t.getElementsByName || !t.getElementsByName(B).length
                    }), E.getById ? (C.find.ID = function(e, t) {
                        if (typeof t.getElementById !== J && N) {
                            var n = t.getElementById(e);
                            return n && n.parentNode ? [n] : []
                        }
                    }, C.filter.ID = function(e) {
                        var t = e.replace(Ee, Se);
                        return function(e) {
                            return e.getAttribute("id") === t
                        }
                    }) : (delete C.find.ID, C.filter.ID = function(e) {
                        var t = e.replace(Ee, Se);
                        return function(e) {
                            var n = typeof e.getAttributeNode !== J && e.getAttributeNode("id");
                            return n && n.value === t
                        }
                    }), C.find.TAG = E.getElementsByTagName ? function(e, t) {
                        return typeof t.getElementsByTagName !== J ? t.getElementsByTagName(e) : void 0
                    } : function(e, t) {
                        var n, i = [],
                            s = 0,
                            r = t.getElementsByTagName(e);
                        if ("*" === e) {
                            for (; n = r[s++];) 1 === n.nodeType && i.push(n);
                            return i
                        }
                        return r
                    }, C.find.CLASS = E.getElementsByClassName && function(e, t) {
                        return typeof t.getElementsByClassName !== J && N ? t.getElementsByClassName(e) : void 0
                    }, F = [], L = [], (E.qsa = ye.test(t.querySelectorAll)) && (r(function(e) {
                        e.innerHTML = "<select><option selected=''></option></select>", e.querySelectorAll("[selected]").length || L.push("\\[" + se + "*(?:value|" + ie + ")"), e.querySelectorAll(":checked").length || L.push(":checked")
                    }), r(function(e) {
                        var n = t.createElement("input");
                        n.setAttribute("type", "hidden"), e.appendChild(n).setAttribute("t", ""), e.querySelectorAll("[t^='']").length && L.push("[*^$]=" + se + "*(?:''|\"\")"), e.querySelectorAll(":enabled").length || L.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), L.push(",.*:")
                    })), (E.matchesSelector = ye.test(I = D.webkitMatchesSelector || D.mozMatchesSelector || D.oMatchesSelector || D.msMatchesSelector)) && r(function(e) {
                        E.disconnectedMatch = I.call(e, "div"), I.call(e, "[s!='']:x"), F.push("!=", le)
                    }), L = L.length && new RegExp(L.join("|")), F = F.length && new RegExp(F.join("|")), M = ye.test(D.contains) || D.compareDocumentPosition ? function(e, t) {
                        var n = 9 === e.nodeType ? e.documentElement : e,
                            i = t && t.parentNode;
                        return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
                    } : function(e, t) {
                        if (t)
                            for (; t = t.parentNode;)
                                if (t === e) return !0;
                        return !1
                    }, G = D.compareDocumentPosition ? function(e, n) {
                        if (e === n) return V = !0, 0;
                        var i = n.compareDocumentPosition && e.compareDocumentPosition && e.compareDocumentPosition(n);
                        return i ? 1 & i || !E.sortDetached && n.compareDocumentPosition(e) === i ? e === t || M(R, e) ? -1 : n === t || M(R, n) ? 1 : O ? ne.call(O, e) - ne.call(O, n) : 0 : 4 & i ? -1 : 1 : e.compareDocumentPosition ? -1 : 1
                    } : function(e, n) {
                        var i, s = 0,
                            r = e.parentNode,
                            o = n.parentNode,
                            l = [e],
                            c = [n];
                        if (e === n) return V = !0, 0;
                        if (!r || !o) return e === t ? -1 : n === t ? 1 : r ? -1 : o ? 1 : O ? ne.call(O, e) - ne.call(O, n) : 0;
                        if (r === o) return a(e, n);
                        for (i = e; i = i.parentNode;) l.unshift(i);
                        for (i = n; i = i.parentNode;) c.unshift(i);
                        for (; l[s] === c[s];) s++;
                        return s ? a(l[s], c[s]) : l[s] === R ? -1 : c[s] === R ? 1 : 0
                    }, t) : A
                }, n.matches = function(e, t) {
                    return n(e, null, null, t)
                }, n.matchesSelector = function(e, t) {
                    if ((e.ownerDocument || e) !== A && $(e), t = t.replace(fe, "='$1']"), E.matchesSelector && N && (!F || !F.test(t)) && (!L || !L.test(t))) try {
                        var i = I.call(e, t);
                        if (i || E.disconnectedMatch || e.document && 11 !== e.document.nodeType) return i
                    } catch (s) {}
                    return n(t, A, null, [e]).length > 0
                }, n.contains = function(e, t) {
                    return (e.ownerDocument || e) !== A && $(e), M(e, t)
                }, n.attr = function(e, n) {
                    (e.ownerDocument || e) !== A && $(e);
                    var i = C.attrHandle[n.toLowerCase()],
                        s = i && Y.call(C.attrHandle, n.toLowerCase()) ? i(e, n, !N) : t;
                    return s === t ? E.attributes || !N ? e.getAttribute(n) : (s = e.getAttributeNode(n)) && s.specified ? s.value : null : s
                }, n.error = function(e) {
                    throw new Error("Syntax error, unrecognized expression: " + e)
                }, n.uniqueSort = function(e) {
                    var t, n = [],
                        i = 0,
                        s = 0;
                    if (V = !E.detectDuplicates, O = !E.sortStable && e.slice(0), e.sort(G), V) {
                        for (; t = e[s++];) t === e[s] && (i = n.push(s));
                        for (; i--;) e.splice(n[i], 1)
                    }
                    return e
                }, k = n.getText = function(e) {
                    var t, n = "",
                        i = 0,
                        s = e.nodeType;
                    if (s) {
                        if (1 === s || 9 === s || 11 === s) {
                            if ("string" == typeof e.textContent) return e.textContent;
                            for (e = e.firstChild; e; e = e.nextSibling) n += k(e)
                        } else if (3 === s || 4 === s) return e.nodeValue
                    } else
                        for (; t = e[i]; i++) n += k(t);
                    return n
                }, C = n.selectors = {
                    cacheLength: 50,
                    createPseudo: s,
                    match: ve,
                    attrHandle: {},
                    find: {},
                    relative: {
                        ">": {
                            dir: "parentNode",
                            first: !0
                        },
                        " ": {
                            dir: "parentNode"
                        },
                        "+": {
                            dir: "previousSibling",
                            first: !0
                        },
                        "~": {
                            dir: "previousSibling"
                        }
                    },
                    preFilter: {
                        ATTR: function(e) {
                            return e[1] = e[1].replace(Ee, Se), e[3] = (e[4] || e[5] || "").replace(Ee, Se), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                        },
                        CHILD: function(e) {
                            return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || n.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && n.error(e[0]), e
                        },
                        PSEUDO: function(e) {
                            var n, i = !e[5] && e[2];
                            return ve.CHILD.test(e[0]) ? null : (e[3] && e[4] !== t ? e[2] = e[4] : i && pe.test(i) && (n = h(i, !0)) && (n = i.indexOf(")", i.length - n) - i.length) && (e[0] = e[0].slice(0, n), e[2] = i.slice(0, n)), e.slice(0, 3))
                        }
                    },
                    filter: {
                        TAG: function(e) {
                            var t = e.replace(Ee, Se).toLowerCase();
                            return "*" === e ? function() {
                                return !0
                            } : function(e) {
                                return e.nodeName && e.nodeName.toLowerCase() === t
                            }
                        },
                        CLASS: function(e) {
                            var t = z[e + " "];
                            return t || (t = new RegExp("(^|" + se + ")" + e + "(" + se + "|$)")) && z(e, function(e) {
                                return t.test("string" == typeof e.className && e.className || typeof e.getAttribute !== J && e.getAttribute("class") || "")
                            })
                        },
                        ATTR: function(e, t, i) {
                            return function(s) {
                                var r = n.attr(s, e);
                                return null == r ? "!=" === t : t ? (r += "", "=" === t ? r === i : "!=" === t ? r !== i : "^=" === t ? i && 0 === r.indexOf(i) : "*=" === t ? i && r.indexOf(i) > -1 : "$=" === t ? i && r.slice(-i.length) === i : "~=" === t ? (" " + r + " ").indexOf(i) > -1 : "|=" === t ? r === i || r.slice(0, i.length + 1) === i + "-" : !1) : !0
                            }
                        },
                        CHILD: function(e, t, n, i, s) {
                            var r = "nth" !== e.slice(0, 3),
                                o = "last" !== e.slice(-4),
                                a = "of-type" === t;
                            return 1 === i && 0 === s ? function(e) {
                                return !!e.parentNode
                            } : function(t, n, l) {
                                var c, u, d, h, m, f, p = r !== o ? "nextSibling" : "previousSibling",
                                    g = t.parentNode,
                                    v = a && t.nodeName.toLowerCase(),
                                    y = !l && !a;
                                if (g) {
                                    if (r) {
                                        for (; p;) {
                                            for (d = t; d = d[p];)
                                                if (a ? d.nodeName.toLowerCase() === v : 1 === d.nodeType) return !1;
                                            f = p = "only" === e && !f && "nextSibling"
                                        }
                                        return !0
                                    }
                                    if (f = [o ? g.firstChild : g.lastChild], o && y) {
                                        for (u = g[B] || (g[B] = {}), c = u[e] || [], m = c[0] === H && c[1], h = c[0] === H && c[2], d = m && g.childNodes[m]; d = ++m && d && d[p] || (h = m = 0) || f.pop();)
                                            if (1 === d.nodeType && ++h && d === t) {
                                                u[e] = [H, m, h];
                                                break
                                            }
                                    } else if (y && (c = (t[B] || (t[B] = {}))[e]) && c[0] === H) h = c[1];
                                    else
                                        for (;
                                            (d = ++m && d && d[p] || (h = m = 0) || f.pop()) && ((a ? d.nodeName.toLowerCase() !== v : 1 !== d.nodeType) || !++h || (y && ((d[B] || (d[B] = {}))[e] = [H, h]), d !== t)););
                                    return h -= s, h === i || h % i === 0 && h / i >= 0
                                }
                            }
                        },
                        PSEUDO: function(e, t) {
                            var i, r = C.pseudos[e] || C.setFilters[e.toLowerCase()] || n.error("unsupported pseudo: " + e);
                            return r[B] ? r(t) : r.length > 1 ? (i = [e, e, "", t], C.setFilters.hasOwnProperty(e.toLowerCase()) ? s(function(e, n) {
                                for (var i, s = r(e, t), o = s.length; o--;) i = ne.call(e, s[o]), e[i] = !(n[i] = s[o])
                            }) : function(e) {
                                return r(e, 0, i)
                            }) : r
                        }
                    },
                    pseudos: {
                        not: s(function(e) {
                            var t = [],
                                n = [],
                                i = T(e.replace(ce, "$1"));
                            return i[B] ? s(function(e, t, n, s) {
                                for (var r, o = i(e, null, s, []), a = e.length; a--;)(r = o[a]) && (e[a] = !(t[a] = r))
                            }) : function(e, s, r) {
                                return t[0] = e, i(t, null, r, n), !n.pop()
                            }
                        }),
                        has: s(function(e) {
                            return function(t) {
                                return n(e, t).length > 0
                            }
                        }),
                        contains: s(function(e) {
                            return function(t) {
                                return (t.textContent || t.innerText || k(t)).indexOf(e) > -1
                            }
                        }),
                        lang: s(function(e) {
                            return ge.test(e || "") || n.error("unsupported lang: " + e), e = e.replace(Ee, Se).toLowerCase(),
                                function(t) {
                                    var n;
                                    do
                                        if (n = N ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-");
                                    while ((t = t.parentNode) && 1 === t.nodeType);
                                    return !1
                                }
                        }),
                        target: function(t) {
                            var n = e.location && e.location.hash;
                            return n && n.slice(1) === t.id
                        },
                        root: function(e) {
                            return e === D
                        },
                        focus: function(e) {
                            return e === A.activeElement && (!A.hasFocus || A.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                        },
                        enabled: function(e) {
                            return e.disabled === !1
                        },
                        disabled: function(e) {
                            return e.disabled === !0
                        },
                        checked: function(e) {
                            var t = e.nodeName.toLowerCase();
                            return "input" === t && !!e.checked || "option" === t && !!e.selected
                        },
                        selected: function(e) {
                            return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                        },
                        empty: function(e) {
                            for (e = e.firstChild; e; e = e.nextSibling)
                                if (e.nodeName > "@" || 3 === e.nodeType || 4 === e.nodeType) return !1;
                            return !0
                        },
                        parent: function(e) {
                            return !C.pseudos.empty(e)
                        },
                        header: function(e) {
                            return _e.test(e.nodeName)
                        },
                        input: function(e) {
                            return be.test(e.nodeName)
                        },
                        button: function(e) {
                            var t = e.nodeName.toLowerCase();
                            return "input" === t && "button" === e.type || "button" === t
                        },
                        text: function(e) {
                            var t;
                            return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || t.toLowerCase() === e.type)
                        },
                        first: u(function() {
                            return [0]
                        }),
                        last: u(function(e, t) {
                            return [t - 1]
                        }),
                        eq: u(function(e, t, n) {
                            return [0 > n ? n + t : n]
                        }),
                        even: u(function(e, t) {
                            for (var n = 0; t > n; n += 2) e.push(n);
                            return e
                        }),
                        odd: u(function(e, t) {
                            for (var n = 1; t > n; n += 2) e.push(n);
                            return e
                        }),
                        lt: u(function(e, t, n) {
                            for (var i = 0 > n ? n + t : n; --i >= 0;) e.push(i);
                            return e
                        }),
                        gt: u(function(e, t, n) {
                            for (var i = 0 > n ? n + t : n; ++i < t;) e.push(i);
                            return e
                        })
                    }
                }, C.pseudos.nth = C.pseudos.eq;
                for (x in {
                        radio: !0,
                        checkbox: !0,
                        file: !0,
                        password: !0,
                        image: !0
                    }) C.pseudos[x] = l(x);
                for (x in {
                        submit: !0,
                        reset: !0
                    }) C.pseudos[x] = c(x);
                d.prototype = C.filters = C.pseudos, C.setFilters = new d, T = n.compile = function(e, t) {
                    var n, i = [],
                        s = [],
                        r = W[e + " "];
                    if (!r) {
                        for (t || (t = h(e)), n = t.length; n--;) r = y(t[n]), r[B] ? i.push(r) : s.push(r);
                        r = W(e, w(s, i))
                    }
                    return r
                }, E.sortStable = B.split("").sort(G).join("") === B, E.detectDuplicates = V, $(), E.sortDetached = r(function(e) {
                    return 1 & e.compareDocumentPosition(A.createElement("div"))
                }), r(function(e) {
                    return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
                }) || o("type|href|height|width", function(e, t, n) {
                    return n ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
                }), E.attributes && r(function(e) {
                    return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
                }) || o("value", function(e, t, n) {
                    return n || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue
                }), r(function(e) {
                    return null == e.getAttribute("disabled")
                }) || o(ie, function(e, t, n) {
                    var i;
                    return n ? void 0 : (i = e.getAttributeNode(t)) && i.specified ? i.value : e[t] === !0 ? t.toLowerCase() : null
                }), ue.find = n, ue.expr = n.selectors, ue.expr[":"] = ue.expr.pseudos, ue.unique = n.uniqueSort, ue.text = n.getText, ue.isXMLDoc = n.isXML, ue.contains = n.contains
            }(e);
        var Ce = {};
        ue.Callbacks = function(e) {
            e = "string" == typeof e ? Ce[e] || i(e) : ue.extend({}, e);
            var n, s, r, o, a, l, c = [],
                u = !e.once && [],
                d = function(t) {
                    for (s = e.memory && t, r = !0, a = l || 0, l = 0, o = c.length, n = !0; c && o > a; a++)
                        if (c[a].apply(t[0], t[1]) === !1 && e.stopOnFalse) {
                            s = !1;
                            break
                        }
                    n = !1, c && (u ? u.length && d(u.shift()) : s ? c = [] : h.disable())
                },
                h = {
                    add: function() {
                        if (c) {
                            var t = c.length;
                            ! function i(t) {
                                ue.each(t, function(t, n) {
                                    var s = ue.type(n);
                                    "function" === s ? e.unique && h.has(n) || c.push(n) : n && n.length && "string" !== s && i(n)
                                })
                            }(arguments), n ? o = c.length : s && (l = t, d(s))
                        }
                        return this
                    },
                    remove: function() {
                        return c && ue.each(arguments, function(e, t) {
                            for (var i;
                                (i = ue.inArray(t, c, i)) > -1;) c.splice(i, 1), n && (o >= i && o--, a >= i && a--)
                        }), this
                    },
                    has: function(e) {
                        return e ? ue.inArray(e, c) > -1 : !(!c || !c.length)
                    },
                    empty: function() {
                        return c = [], o = 0, this
                    },
                    disable: function() {
                        return c = u = s = t, this
                    },
                    disabled: function() {
                        return !c
                    },
                    lock: function() {
                        return u = t, s || h.disable(), this
                    },
                    locked: function() {
                        return !u
                    },
                    fireWith: function(e, t) {
                        return !c || r && !u || (t = t || [], t = [e, t.slice ? t.slice() : t], n ? u.push(t) : d(t)), this
                    },
                    fire: function() {
                        return h.fireWith(this, arguments), this
                    },
                    fired: function() {
                        return !!r
                    }
                };
            return h
        }, ue.extend({
            Deferred: function(e) {
                var t = [
                        ["resolve", "done", ue.Callbacks("once memory"), "resolved"],
                        ["reject", "fail", ue.Callbacks("once memory"), "rejected"],
                        ["notify", "progress", ue.Callbacks("memory")]
                    ],
                    n = "pending",
                    i = {
                        state: function() {
                            return n
                        },
                        always: function() {
                            return s.done(arguments).fail(arguments), this
                        },
                        then: function() {
                            var e = arguments;
                            return ue.Deferred(function(n) {
                                ue.each(t, function(t, r) {
                                    var o = r[0],
                                        a = ue.isFunction(e[t]) && e[t];
                                    s[r[1]](function() {
                                        var e = a && a.apply(this, arguments);
                                        e && ue.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[o + "With"](this === i ? n.promise() : this, a ? [e] : arguments)
                                    })
                                }), e = null
                            }).promise()
                        },
                        promise: function(e) {
                            return null != e ? ue.extend(e, i) : i
                        }
                    },
                    s = {};
                return i.pipe = i.then, ue.each(t, function(e, r) {
                    var o = r[2],
                        a = r[3];
                    i[r[1]] = o.add, a && o.add(function() {
                        n = a
                    }, t[1 ^ e][2].disable, t[2][2].lock), s[r[0]] = function() {
                        return s[r[0] + "With"](this === s ? i : this, arguments), this
                    }, s[r[0] + "With"] = o.fireWith
                }), i.promise(s), e && e.call(s, s), s
            },
            when: function(e) {
                var t, n, i, s = 0,
                    r = re.call(arguments),
                    o = r.length,
                    a = 1 !== o || e && ue.isFunction(e.promise) ? o : 0,
                    l = 1 === a ? e : ue.Deferred(),
                    c = function(e, n, i) {
                        return function(s) {
                            n[e] = this, i[e] = arguments.length > 1 ? re.call(arguments) : s, i === t ? l.notifyWith(n, i) : --a || l.resolveWith(n, i)
                        }
                    };
                if (o > 1)
                    for (t = new Array(o), n = new Array(o), i = new Array(o); o > s; s++) r[s] && ue.isFunction(r[s].promise) ? r[s].promise().done(c(s, i, r)).fail(l.reject).progress(c(s, n, t)) : --a;
                return a || l.resolveWith(i, r), l.promise()
            }
        }), ue.support = function(t) {
            var n, i, s, r, o, a, l, c, u, d = Y.createElement("div");
            if (d.setAttribute("className", "t"), d.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", n = d.getElementsByTagName("*") || [], i = d.getElementsByTagName("a")[0], !i || !i.style || !n.length) return t;
            r = Y.createElement("select"), a = r.appendChild(Y.createElement("option")), s = d.getElementsByTagName("input")[0], i.style.cssText = "top:1px;float:left;opacity:.5", t.getSetAttribute = "t" !== d.className, t.leadingWhitespace = 3 === d.firstChild.nodeType, t.tbody = !d.getElementsByTagName("tbody").length, t.htmlSerialize = !!d.getElementsByTagName("link").length, t.style = /top/.test(i.getAttribute("style")), t.hrefNormalized = "/a" === i.getAttribute("href"), t.opacity = /^0.5/.test(i.style.opacity), t.cssFloat = !!i.style.cssFloat, t.checkOn = !!s.value, t.optSelected = a.selected, t.enctype = !!Y.createElement("form").enctype, t.html5Clone = "<:nav></:nav>" !== Y.createElement("nav").cloneNode(!0).outerHTML, t.inlineBlockNeedsLayout = !1, t.shrinkWrapBlocks = !1, t.pixelPosition = !1, t.deleteExpando = !0, t.noCloneEvent = !0, t.reliableMarginRight = !0, t.boxSizingReliable = !0, s.checked = !0, t.noCloneChecked = s.cloneNode(!0).checked, r.disabled = !0, t.optDisabled = !a.disabled;
            try {
                delete d.test
            } catch (h) {
                t.deleteExpando = !1
            }
            s = Y.createElement("input"), s.setAttribute("value", ""), t.input = "" === s.getAttribute("value"), s.value = "t", s.setAttribute("type", "radio"), t.radioValue = "t" === s.value, s.setAttribute("checked", "t"), s.setAttribute("name", "t"), o = Y.createDocumentFragment(), o.appendChild(s), t.appendChecked = s.checked, t.checkClone = o.cloneNode(!0).cloneNode(!0).lastChild.checked, d.attachEvent && (d.attachEvent("onclick", function() {
                t.noCloneEvent = !1
            }), d.cloneNode(!0).click());
            for (u in {
                    submit: !0,
                    change: !0,
                    focusin: !0
                }) d.setAttribute(l = "on" + u, "t"), t[u + "Bubbles"] = l in e || d.attributes[l].expando === !1;
            d.style.backgroundClip = "content-box", d.cloneNode(!0).style.backgroundClip = "", t.clearCloneStyle = "content-box" === d.style.backgroundClip;
            for (u in ue(t)) break;
            return t.ownLast = "0" !== u, ue(function() {
                var n, i, s, r = "padding:0;margin:0;border:0;display:block;box-sizing:content-box;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;",
                    o = Y.getElementsByTagName("body")[0];
                o && (n = Y.createElement("div"), n.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px", o.appendChild(n).appendChild(d), d.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", s = d.getElementsByTagName("td"), s[0].style.cssText = "padding:0;margin:0;border:0;display:none", c = 0 === s[0].offsetHeight, s[0].style.display = "", s[1].style.display = "none", t.reliableHiddenOffsets = c && 0 === s[0].offsetHeight, d.innerHTML = "", d.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;", ue.swap(o, null != o.style.zoom ? {
                    zoom: 1
                } : {}, function() {
                    t.boxSizing = 4 === d.offsetWidth
                }), e.getComputedStyle && (t.pixelPosition = "1%" !== (e.getComputedStyle(d, null) || {}).top, t.boxSizingReliable = "4px" === (e.getComputedStyle(d, null) || {
                    width: "4px"
                }).width, i = d.appendChild(Y.createElement("div")), i.style.cssText = d.style.cssText = r, i.style.marginRight = i.style.width = "0", d.style.width = "1px", t.reliableMarginRight = !parseFloat((e.getComputedStyle(i, null) || {}).marginRight)), typeof d.style.zoom !== J && (d.innerHTML = "", d.style.cssText = r + "width:1px;padding:1px;display:inline;zoom:1", t.inlineBlockNeedsLayout = 3 === d.offsetWidth, d.style.display = "block", d.innerHTML = "<div></div>", d.firstChild.style.width = "5px", t.shrinkWrapBlocks = 3 !== d.offsetWidth, t.inlineBlockNeedsLayout && (o.style.zoom = 1)), o.removeChild(n), n = d = s = i = null)
            }), n = r = o = a = i = s = null, t
        }({});
        var ke = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/,
            je = /([A-Z])/g;
        ue.extend({
            cache: {},
            noData: {
                applet: !0,
                embed: !0,
                object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
            },
            hasData: function(e) {
                return e = e.nodeType ? ue.cache[e[ue.expando]] : e[ue.expando], !!e && !a(e)
            },
            data: function(e, t, n) {
                return s(e, t, n)
            },
            removeData: function(e, t) {
                return r(e, t)
            },
            _data: function(e, t, n) {
                return s(e, t, n, !0)
            },
            _removeData: function(e, t) {
                return r(e, t, !0)
            },
            acceptData: function(e) {
                if (e.nodeType && 1 !== e.nodeType && 9 !== e.nodeType) return !1;
                var t = e.nodeName && ue.noData[e.nodeName.toLowerCase()];
                return !t || t !== !0 && e.getAttribute("classid") === t
            }
        }), ue.fn.extend({
            data: function(e, n) {
                var i, s, r = null,
                    a = 0,
                    l = this[0];
                if (e === t) {
                    if (this.length && (r = ue.data(l), 1 === l.nodeType && !ue._data(l, "parsedAttrs"))) {
                        for (i = l.attributes; a < i.length; a++) s = i[a].name, 0 === s.indexOf("data-") && (s = ue.camelCase(s.slice(5)), o(l, s, r[s]));
                        ue._data(l, "parsedAttrs", !0)
                    }
                    return r
                }
                return "object" == typeof e ? this.each(function() {
                    ue.data(this, e)
                }) : arguments.length > 1 ? this.each(function() {
                    ue.data(this, e, n)
                }) : l ? o(l, e, ue.data(l, e)) : null
            },
            removeData: function(e) {
                return this.each(function() {
                    ue.removeData(this, e)
                })
            }
        }), ue.extend({
            queue: function(e, t, n) {
                var i;
                return e ? (t = (t || "fx") + "queue", i = ue._data(e, t), n && (!i || ue.isArray(n) ? i = ue._data(e, t, ue.makeArray(n)) : i.push(n)), i || []) : void 0
            },
            dequeue: function(e, t) {
                t = t || "fx";
                var n = ue.queue(e, t),
                    i = n.length,
                    s = n.shift(),
                    r = ue._queueHooks(e, t),
                    o = function() {
                        ue.dequeue(e, t)
                    };
                "inprogress" === s && (s = n.shift(), i--), s && ("fx" === t && n.unshift("inprogress"), delete r.stop, s.call(e, o, r)), !i && r && r.empty.fire()
            },
            _queueHooks: function(e, t) {
                var n = t + "queueHooks";
                return ue._data(e, n) || ue._data(e, n, {
                    empty: ue.Callbacks("once memory").add(function() {
                        ue._removeData(e, t + "queue"), ue._removeData(e, n)
                    })
                })
            }
        }), ue.fn.extend({
            queue: function(e, n) {
                var i = 2;
                return "string" != typeof e && (n = e, e = "fx", i--), arguments.length < i ? ue.queue(this[0], e) : n === t ? this : this.each(function() {
                    var t = ue.queue(this, e, n);
                    ue._queueHooks(this, e), "fx" === e && "inprogress" !== t[0] && ue.dequeue(this, e)
                })
            },
            dequeue: function(e) {
                return this.each(function() {
                    ue.dequeue(this, e)
                })
            },
            delay: function(e, t) {
                return e = ue.fx ? ue.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function(t, n) {
                    var i = setTimeout(t, e);
                    n.stop = function() {
                        clearTimeout(i)
                    }
                })
            },
            clearQueue: function(e) {
                return this.queue(e || "fx", [])
            },
            promise: function(e, n) {
                var i, s = 1,
                    r = ue.Deferred(),
                    o = this,
                    a = this.length,
                    l = function() {
                        --s || r.resolveWith(o, [o])
                    };
                for ("string" != typeof e && (n = e, e = t), e = e || "fx"; a--;) i = ue._data(o[a], e + "queueHooks"), i && i.empty && (s++, i.empty.add(l));
                return l(), r.promise(n)
            }
        });
        var Te, Pe, Oe = /[\t\r\n\f]/g,
            $e = /\r/g,
            Ae = /^(?:input|select|textarea|button|object)$/i,
            De = /^(?:a|area)$/i,
            Ne = /^(?:checked|selected)$/i,
            Le = ue.support.getSetAttribute,
            Fe = ue.support.input;
        ue.fn.extend({
            attr: function(e, t) {
                return ue.access(this, ue.attr, e, t, arguments.length > 1)
            },
            removeAttr: function(e) {
                return this.each(function() {
                    ue.removeAttr(this, e)
                })
            },
            prop: function(e, t) {
                return ue.access(this, ue.prop, e, t, arguments.length > 1)
            },
            removeProp: function(e) {
                return e = ue.propFix[e] || e, this.each(function() {
                    try {
                        this[e] = t, delete this[e]
                    } catch (n) {}
                })
            },
            addClass: function(e) {
                var t, n, i, s, r, o = 0,
                    a = this.length,
                    l = "string" == typeof e && e;
                if (ue.isFunction(e)) return this.each(function(t) {
                    ue(this).addClass(e.call(this, t, this.className))
                });
                if (l)
                    for (t = (e || "").match(he) || []; a > o; o++)
                        if (n = this[o], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Oe, " ") : " ")) {
                            for (r = 0; s = t[r++];) i.indexOf(" " + s + " ") < 0 && (i += s + " ");
                            n.className = ue.trim(i)
                        }
                return this
            },
            removeClass: function(e) {
                var t, n, i, s, r, o = 0,
                    a = this.length,
                    l = 0 === arguments.length || "string" == typeof e && e;
                if (ue.isFunction(e)) return this.each(function(t) {
                    ue(this).removeClass(e.call(this, t, this.className))
                });
                if (l)
                    for (t = (e || "").match(he) || []; a > o; o++)
                        if (n = this[o], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Oe, " ") : "")) {
                            for (r = 0; s = t[r++];)
                                for (; i.indexOf(" " + s + " ") >= 0;) i = i.replace(" " + s + " ", " ");
                            n.className = e ? ue.trim(i) : ""
                        }
                return this
            },
            toggleClass: function(e, t) {
                var n = typeof e;
                return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : ue.isFunction(e) ? this.each(function(n) {
                    ue(this).toggleClass(e.call(this, n, this.className, t), t)
                }) : this.each(function() {
                    if ("string" === n)
                        for (var t, i = 0, s = ue(this), r = e.match(he) || []; t = r[i++];) s.hasClass(t) ? s.removeClass(t) : s.addClass(t);
                    else(n === J || "boolean" === n) && (this.className && ue._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : ue._data(this, "__className__") || "")
                })
            },
            hasClass: function(e) {
                for (var t = " " + e + " ", n = 0, i = this.length; i > n; n++)
                    if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(Oe, " ").indexOf(t) >= 0) return !0;
                return !1
            },
            val: function(e) {
                var n, i, s, r = this[0]; {
                    if (arguments.length) return s = ue.isFunction(e), this.each(function(n) {
                        var r;
                        1 === this.nodeType && (r = s ? e.call(this, n, ue(this).val()) : e, null == r ? r = "" : "number" == typeof r ? r += "" : ue.isArray(r) && (r = ue.map(r, function(e) {
                            return null == e ? "" : e + ""
                        })), i = ue.valHooks[this.type] || ue.valHooks[this.nodeName.toLowerCase()], i && "set" in i && i.set(this, r, "value") !== t || (this.value = r))
                    });
                    if (r) return i = ue.valHooks[r.type] || ue.valHooks[r.nodeName.toLowerCase()], i && "get" in i && (n = i.get(r, "value")) !== t ? n : (n = r.value, "string" == typeof n ? n.replace($e, "") : null == n ? "" : n)
                }
            }
        }), ue.extend({
            valHooks: {
                option: {
                    get: function(e) {
                        var t = ue.find.attr(e, "value");
                        return null != t ? t : e.text
                    }
                },
                select: {
                    get: function(e) {
                        for (var t, n, i = e.options, s = e.selectedIndex, r = "select-one" === e.type || 0 > s, o = r ? null : [], a = r ? s + 1 : i.length, l = 0 > s ? a : r ? s : 0; a > l; l++)
                            if (n = i[l], (n.selected || l === s) && (ue.support.optDisabled ? !n.disabled : null === n.getAttribute("disabled")) && (!n.parentNode.disabled || !ue.nodeName(n.parentNode, "optgroup"))) {
                                if (t = ue(n).val(), r) return t;
                                o.push(t)
                            }
                        return o
                    },
                    set: function(e, t) {
                        for (var n, i, s = e.options, r = ue.makeArray(t), o = s.length; o--;) i = s[o], (i.selected = ue.inArray(ue(i).val(), r) >= 0) && (n = !0);
                        return n || (e.selectedIndex = -1), r
                    }
                }
            },
            attr: function(e, n, i) {
                var s, r, o = e.nodeType;
                if (e && 3 !== o && 8 !== o && 2 !== o) return typeof e.getAttribute === J ? ue.prop(e, n, i) : (1 === o && ue.isXMLDoc(e) || (n = n.toLowerCase(), s = ue.attrHooks[n] || (ue.expr.match.bool.test(n) ? Pe : Te)), i === t ? s && "get" in s && null !== (r = s.get(e, n)) ? r : (r = ue.find.attr(e, n), null == r ? t : r) : null !== i ? s && "set" in s && (r = s.set(e, i, n)) !== t ? r : (e.setAttribute(n, i + ""), i) : void ue.removeAttr(e, n))
            },
            removeAttr: function(e, t) {
                var n, i, s = 0,
                    r = t && t.match(he);
                if (r && 1 === e.nodeType)
                    for (; n = r[s++];) i = ue.propFix[n] || n, ue.expr.match.bool.test(n) ? Fe && Le || !Ne.test(n) ? e[i] = !1 : e[ue.camelCase("default-" + n)] = e[i] = !1 : ue.attr(e, n, ""), e.removeAttribute(Le ? n : i)
            },
            attrHooks: {
                type: {
                    set: function(e, t) {
                        if (!ue.support.radioValue && "radio" === t && ue.nodeName(e, "input")) {
                            var n = e.value;
                            return e.setAttribute("type", t), n && (e.value = n), t
                        }
                    }
                }
            },
            propFix: {
                "for": "htmlFor",
                "class": "className"
            },
            prop: function(e, n, i) {
                var s, r, o, a = e.nodeType;
                if (e && 3 !== a && 8 !== a && 2 !== a) return o = 1 !== a || !ue.isXMLDoc(e), o && (n = ue.propFix[n] || n, r = ue.propHooks[n]), i !== t ? r && "set" in r && (s = r.set(e, i, n)) !== t ? s : e[n] = i : r && "get" in r && null !== (s = r.get(e, n)) ? s : e[n]
            },
            propHooks: {
                tabIndex: {
                    get: function(e) {
                        var t = ue.find.attr(e, "tabindex");
                        return t ? parseInt(t, 10) : Ae.test(e.nodeName) || De.test(e.nodeName) && e.href ? 0 : -1
                    }
                }
            }
        }), Pe = {
            set: function(e, t, n) {
                return t === !1 ? ue.removeAttr(e, n) : Fe && Le || !Ne.test(n) ? e.setAttribute(!Le && ue.propFix[n] || n, n) : e[ue.camelCase("default-" + n)] = e[n] = !0, n
            }
        }, ue.each(ue.expr.match.bool.source.match(/\w+/g), function(e, n) {
            var i = ue.expr.attrHandle[n] || ue.find.attr;
            ue.expr.attrHandle[n] = Fe && Le || !Ne.test(n) ? function(e, n, s) {
                var r = ue.expr.attrHandle[n],
                    o = s ? t : (ue.expr.attrHandle[n] = t) != i(e, n, s) ? n.toLowerCase() : null;
                return ue.expr.attrHandle[n] = r, o
            } : function(e, n, i) {
                return i ? t : e[ue.camelCase("default-" + n)] ? n.toLowerCase() : null
            }
        }), Fe && Le || (ue.attrHooks.value = {
            set: function(e, t, n) {
                return ue.nodeName(e, "input") ? void(e.defaultValue = t) : Te && Te.set(e, t, n)
            }
        }), Le || (Te = {
            set: function(e, n, i) {
                var s = e.getAttributeNode(i);
                return s || e.setAttributeNode(s = e.ownerDocument.createAttribute(i)), s.value = n += "", "value" === i || n === e.getAttribute(i) ? n : t
            }
        }, ue.expr.attrHandle.id = ue.expr.attrHandle.name = ue.expr.attrHandle.coords = function(e, n, i) {
            var s;
            return i ? t : (s = e.getAttributeNode(n)) && "" !== s.value ? s.value : null
        }, ue.valHooks.button = {
            get: function(e, n) {
                var i = e.getAttributeNode(n);
                return i && i.specified ? i.value : t
            },
            set: Te.set
        }, ue.attrHooks.contenteditable = {
            set: function(e, t, n) {
                Te.set(e, "" === t ? !1 : t, n)
            }
        }, ue.each(["width", "height"], function(e, t) {
            ue.attrHooks[t] = {
                set: function(e, n) {
                    return "" === n ? (e.setAttribute(t, "auto"), n) : void 0
                }
            }
        })), ue.support.hrefNormalized || ue.each(["href", "src"], function(e, t) {
            ue.propHooks[t] = {
                get: function(e) {
                    return e.getAttribute(t, 4)
                }
            }
        }), ue.support.style || (ue.attrHooks.style = {
            get: function(e) {
                return e.style.cssText || t
            },
            set: function(e, t) {
                return e.style.cssText = t + ""
            }
        }), ue.support.optSelected || (ue.propHooks.selected = {
            get: function(e) {
                var t = e.parentNode;
                return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
            }
        }), ue.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
            ue.propFix[this.toLowerCase()] = this
        }), ue.support.enctype || (ue.propFix.enctype = "encoding"), ue.each(["radio", "checkbox"], function() {
            ue.valHooks[this] = {
                set: function(e, t) {
                    return ue.isArray(t) ? e.checked = ue.inArray(ue(e).val(), t) >= 0 : void 0
                }
            }, ue.support.checkOn || (ue.valHooks[this].get = function(e) {
                return null === e.getAttribute("value") ? "on" : e.value
            })
        });
        var Ie = /^(?:input|select|textarea)$/i,
            Me = /^key/,
            Be = /^(?:mouse|contextmenu)|click/,
            Re = /^(?:focusinfocus|focusoutblur)$/,
            He = /^([^.]*)(?:\.(.+)|)$/;
        ue.event = {
            global: {},
            add: function(e, n, i, s, r) {
                var o, a, l, c, u, d, h, m, f, p, g, v = ue._data(e);
                if (v) {
                    for (i.handler && (c = i, i = c.handler, r = c.selector), i.guid || (i.guid = ue.guid++), (a = v.events) || (a = v.events = {}), (d = v.handle) || (d = v.handle = function(e) {
                            return typeof ue === J || e && ue.event.triggered === e.type ? t : ue.event.dispatch.apply(d.elem, arguments)
                        }, d.elem = e), n = (n || "").match(he) || [""], l = n.length; l--;) o = He.exec(n[l]) || [], f = g = o[1], p = (o[2] || "").split(".").sort(), f && (u = ue.event.special[f] || {}, f = (r ? u.delegateType : u.bindType) || f, u = ue.event.special[f] || {}, h = ue.extend({
                        type: f,
                        origType: g,
                        data: s,
                        handler: i,
                        guid: i.guid,
                        selector: r,
                        needsContext: r && ue.expr.match.needsContext.test(r),
                        namespace: p.join(".")
                    }, c), (m = a[f]) || (m = a[f] = [], m.delegateCount = 0, u.setup && u.setup.call(e, s, p, d) !== !1 || (e.addEventListener ? e.addEventListener(f, d, !1) : e.attachEvent && e.attachEvent("on" + f, d))), u.add && (u.add.call(e, h), h.handler.guid || (h.handler.guid = i.guid)), r ? m.splice(m.delegateCount++, 0, h) : m.push(h), ue.event.global[f] = !0);
                    e = null
                }
            },
            remove: function(e, t, n, i, s) {
                var r, o, a, l, c, u, d, h, m, f, p, g = ue.hasData(e) && ue._data(e);
                if (g && (u = g.events)) {
                    for (t = (t || "").match(he) || [""], c = t.length; c--;)
                        if (a = He.exec(t[c]) || [], m = p = a[1], f = (a[2] || "").split(".").sort(), m) {
                            for (d = ue.event.special[m] || {}, m = (i ? d.delegateType : d.bindType) || m, h = u[m] || [], a = a[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), l = r = h.length; r--;) o = h[r], !s && p !== o.origType || n && n.guid !== o.guid || a && !a.test(o.namespace) || i && i !== o.selector && ("**" !== i || !o.selector) || (h.splice(r, 1), o.selector && h.delegateCount--, d.remove && d.remove.call(e, o));
                            l && !h.length && (d.teardown && d.teardown.call(e, f, g.handle) !== !1 || ue.removeEvent(e, m, g.handle), delete u[m])
                        } else
                            for (m in u) ue.event.remove(e, m + t[c], n, i, !0);
                    ue.isEmptyObject(u) && (delete g.handle, ue._removeData(e, "events"))
                }
            },
            trigger: function(n, i, s, r) {
                var o, a, l, c, u, d, h, m = [s || Y],
                    f = le.call(n, "type") ? n.type : n,
                    p = le.call(n, "namespace") ? n.namespace.split(".") : [];
                if (l = d = s = s || Y, 3 !== s.nodeType && 8 !== s.nodeType && !Re.test(f + ue.event.triggered) && (f.indexOf(".") >= 0 && (p = f.split("."), f = p.shift(), p.sort()), a = f.indexOf(":") < 0 && "on" + f, n = n[ue.expando] ? n : new ue.Event(f, "object" == typeof n && n), n.isTrigger = r ? 2 : 3, n.namespace = p.join("."), n.namespace_re = n.namespace ? new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, n.result = t, n.target || (n.target = s), i = null == i ? [n] : ue.makeArray(i, [n]), u = ue.event.special[f] || {}, r || !u.trigger || u.trigger.apply(s, i) !== !1)) {
                    if (!r && !u.noBubble && !ue.isWindow(s)) {
                        for (c = u.delegateType || f, Re.test(c + f) || (l = l.parentNode); l; l = l.parentNode) m.push(l), d = l;
                        d === (s.ownerDocument || Y) && m.push(d.defaultView || d.parentWindow || e)
                    }
                    for (h = 0;
                        (l = m[h++]) && !n.isPropagationStopped();) n.type = h > 1 ? c : u.bindType || f, o = (ue._data(l, "events") || {})[n.type] && ue._data(l, "handle"), o && o.apply(l, i), o = a && l[a], o && ue.acceptData(l) && o.apply && o.apply(l, i) === !1 && n.preventDefault();
                    if (n.type = f, !r && !n.isDefaultPrevented() && (!u._default || u._default.apply(m.pop(), i) === !1) && ue.acceptData(s) && a && s[f] && !ue.isWindow(s)) {
                        d = s[a], d && (s[a] = null), ue.event.triggered = f;
                        try {
                            s[f]()
                        } catch (g) {}
                        ue.event.triggered = t, d && (s[a] = d)
                    }
                    return n.result
                }
            },
            dispatch: function(e) {
                e = ue.event.fix(e);
                var n, i, s, r, o, a = [],
                    l = re.call(arguments),
                    c = (ue._data(this, "events") || {})[e.type] || [],
                    u = ue.event.special[e.type] || {};
                if (l[0] = e, e.delegateTarget = this, !u.preDispatch || u.preDispatch.call(this, e) !== !1) {
                    for (a = ue.event.handlers.call(this, e, c), n = 0;
                        (r = a[n++]) && !e.isPropagationStopped();)
                        for (e.currentTarget = r.elem, o = 0;
                            (s = r.handlers[o++]) && !e.isImmediatePropagationStopped();)(!e.namespace_re || e.namespace_re.test(s.namespace)) && (e.handleObj = s, e.data = s.data, i = ((ue.event.special[s.origType] || {}).handle || s.handler).apply(r.elem, l), i !== t && (e.result = i) === !1 && (e.preventDefault(), e.stopPropagation()));
                    return u.postDispatch && u.postDispatch.call(this, e), e.result
                }
            },
            handlers: function(e, n) {
                var i, s, r, o, a = [],
                    l = n.delegateCount,
                    c = e.target;
                if (l && c.nodeType && (!e.button || "click" !== e.type))
                    for (; c != this; c = c.parentNode || this)
                        if (1 === c.nodeType && (c.disabled !== !0 || "click" !== e.type)) {
                            for (r = [], o = 0; l > o; o++) s = n[o], i = s.selector + " ", r[i] === t && (r[i] = s.needsContext ? ue(i, this).index(c) >= 0 : ue.find(i, this, null, [c]).length), r[i] && r.push(s);
                            r.length && a.push({
                                elem: c,
                                handlers: r
                            })
                        }
                return l < n.length && a.push({
                    elem: this,
                    handlers: n.slice(l)
                }), a
            },
            fix: function(e) {
                if (e[ue.expando]) return e;
                var t, n, i, s = e.type,
                    r = e,
                    o = this.fixHooks[s];
                for (o || (this.fixHooks[s] = o = Be.test(s) ? this.mouseHooks : Me.test(s) ? this.keyHooks : {}), i = o.props ? this.props.concat(o.props) : this.props, e = new ue.Event(r), t = i.length; t--;) n = i[t], e[n] = r[n];
                return e.target || (e.target = r.srcElement || Y), 3 === e.target.nodeType && (e.target = e.target.parentNode), e.metaKey = !!e.metaKey, o.filter ? o.filter(e, r) : e
            },
            props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
            fixHooks: {},
            keyHooks: {
                props: "char charCode key keyCode".split(" "),
                filter: function(e, t) {
                    return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
                }
            },
            mouseHooks: {
                props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
                filter: function(e, n) {
                    var i, s, r, o = n.button,
                        a = n.fromElement;
                    return null == e.pageX && null != n.clientX && (s = e.target.ownerDocument || Y, r = s.documentElement, i = s.body, e.pageX = n.clientX + (r && r.scrollLeft || i && i.scrollLeft || 0) - (r && r.clientLeft || i && i.clientLeft || 0), e.pageY = n.clientY + (r && r.scrollTop || i && i.scrollTop || 0) - (r && r.clientTop || i && i.clientTop || 0)), !e.relatedTarget && a && (e.relatedTarget = a === e.target ? n.toElement : a), e.which || o === t || (e.which = 1 & o ? 1 : 2 & o ? 3 : 4 & o ? 2 : 0), e
                }
            },
            special: {
                load: {
                    noBubble: !0
                },
                focus: {
                    trigger: function() {
                        if (this !== u() && this.focus) try {
                            return this.focus(), !1
                        } catch (e) {}
                    },
                    delegateType: "focusin"
                },
                blur: {
                    trigger: function() {
                        return this === u() && this.blur ? (this.blur(), !1) : void 0
                    },
                    delegateType: "focusout"
                },
                click: {
                    trigger: function() {
                        return ue.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
                    },
                    _default: function(e) {
                        return ue.nodeName(e.target, "a")
                    }
                },
                beforeunload: {
                    postDispatch: function(e) {
                        e.result !== t && (e.originalEvent.returnValue = e.result)
                    }
                }
            },
            simulate: function(e, t, n, i) {
                var s = ue.extend(new ue.Event, n, {
                    type: e,
                    isSimulated: !0,
                    originalEvent: {}
                });
                i ? ue.event.trigger(s, null, t) : ue.event.dispatch.call(t, s), s.isDefaultPrevented() && n.preventDefault()
            }
        }, ue.removeEvent = Y.removeEventListener ? function(e, t, n) {
            e.removeEventListener && e.removeEventListener(t, n, !1)
        } : function(e, t, n) {
            var i = "on" + t;
            e.detachEvent && (typeof e[i] === J && (e[i] = null), e.detachEvent(i, n))
        }, ue.Event = function(e, t) {
            return this instanceof ue.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || e.returnValue === !1 || e.getPreventDefault && e.getPreventDefault() ? l : c) : this.type = e, t && ue.extend(this, t), this.timeStamp = e && e.timeStamp || ue.now(), void(this[ue.expando] = !0)) : new ue.Event(e, t)
        }, ue.Event.prototype = {
            isDefaultPrevented: c,
            isPropagationStopped: c,
            isImmediatePropagationStopped: c,
            preventDefault: function() {
                var e = this.originalEvent;
                this.isDefaultPrevented = l, e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
            },
            stopPropagation: function() {
                var e = this.originalEvent;
                this.isPropagationStopped = l, e && (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0)
            },
            stopImmediatePropagation: function() {
                this.isImmediatePropagationStopped = l, this.stopPropagation()
            }
        }, ue.each({
            mouseenter: "mouseover",
            mouseleave: "mouseout"
        }, function(e, t) {
            ue.event.special[e] = {
                delegateType: t,
                bindType: t,
                handle: function(e) {
                    var n, i = this,
                        s = e.relatedTarget,
                        r = e.handleObj;
                    return (!s || s !== i && !ue.contains(i, s)) && (e.type = r.origType, n = r.handler.apply(this, arguments), e.type = t), n
                }
            }
        }), ue.support.submitBubbles || (ue.event.special.submit = {
            setup: function() {
                return ue.nodeName(this, "form") ? !1 : void ue.event.add(this, "click._submit keypress._submit", function(e) {
                    var n = e.target,
                        i = ue.nodeName(n, "input") || ue.nodeName(n, "button") ? n.form : t;
                    i && !ue._data(i, "submitBubbles") && (ue.event.add(i, "submit._submit", function(e) {
                        e._submit_bubble = !0
                    }), ue._data(i, "submitBubbles", !0))
                })
            },
            postDispatch: function(e) {
                e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && ue.event.simulate("submit", this.parentNode, e, !0))
            },
            teardown: function() {
                return ue.nodeName(this, "form") ? !1 : void ue.event.remove(this, "._submit")
            }
        }), ue.support.changeBubbles || (ue.event.special.change = {
            setup: function() {
                return Ie.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (ue.event.add(this, "propertychange._change", function(e) {
                    "checked" === e.originalEvent.propertyName && (this._just_changed = !0)
                }), ue.event.add(this, "click._change", function(e) {
                    this._just_changed && !e.isTrigger && (this._just_changed = !1), ue.event.simulate("change", this, e, !0)
                })), !1) : void ue.event.add(this, "beforeactivate._change", function(e) {
                    var t = e.target;
                    Ie.test(t.nodeName) && !ue._data(t, "changeBubbles") && (ue.event.add(t, "change._change", function(e) {
                        !this.parentNode || e.isSimulated || e.isTrigger || ue.event.simulate("change", this.parentNode, e, !0)
                    }), ue._data(t, "changeBubbles", !0))
                })
            },
            handle: function(e) {
                var t = e.target;
                return this !== t || e.isSimulated || e.isTrigger || "radio" !== t.type && "checkbox" !== t.type ? e.handleObj.handler.apply(this, arguments) : void 0
            },
            teardown: function() {
                return ue.event.remove(this, "._change"), !Ie.test(this.nodeName)
            }
        }), ue.support.focusinBubbles || ue.each({
            focus: "focusin",
            blur: "focusout"
        }, function(e, t) {
            var n = 0,
                i = function(e) {
                    ue.event.simulate(t, e.target, ue.event.fix(e), !0)
                };
            ue.event.special[t] = {
                setup: function() {
                    0 === n++ && Y.addEventListener(e, i, !0)
                },
                teardown: function() {
                    0 === --n && Y.removeEventListener(e, i, !0)
                }
            }
        }), ue.fn.extend({
            on: function(e, n, i, s, r) {
                var o, a;
                if ("object" == typeof e) {
                    "string" != typeof n && (i = i || n, n = t);
                    for (o in e) this.on(o, n, i, e[o], r);
                    return this
                }
                if (null == i && null == s ? (s = n, i = n = t) : null == s && ("string" == typeof n ? (s = i, i = t) : (s = i, i = n, n = t)), s === !1) s = c;
                else if (!s) return this;
                return 1 === r && (a = s, s = function(e) {
                    return ue().off(e), a.apply(this, arguments)
                }, s.guid = a.guid || (a.guid = ue.guid++)), this.each(function() {
                    ue.event.add(this, e, s, i, n)
                })
            },
            one: function(e, t, n, i) {
                return this.on(e, t, n, i, 1)
            },
            off: function(e, n, i) {
                var s, r;
                if (e && e.preventDefault && e.handleObj) return s = e.handleObj, ue(e.delegateTarget).off(s.namespace ? s.origType + "." + s.namespace : s.origType, s.selector, s.handler), this;
                if ("object" == typeof e) {
                    for (r in e) this.off(r, n, e[r]);
                    return this
                }
                return (n === !1 || "function" == typeof n) && (i = n, n = t), i === !1 && (i = c), this.each(function() {
                    ue.event.remove(this, e, i, n)
                })
            },
            trigger: function(e, t) {
                return this.each(function() {
                    ue.event.trigger(e, t, this)
                })
            },
            triggerHandler: function(e, t) {
                var n = this[0];
                return n ? ue.event.trigger(e, t, n, !0) : void 0
            }
        });
        var qe = /^.[^:#\[\.,]*$/,
            ze = /^(?:parents|prev(?:Until|All))/,
            Ue = ue.expr.match.needsContext,
            We = {
                children: !0,
                contents: !0,
                next: !0,
                prev: !0
            };
        ue.fn.extend({
            find: function(e) {
                var t, n = [],
                    i = this,
                    s = i.length;
                if ("string" != typeof e) return this.pushStack(ue(e).filter(function() {
                    for (t = 0; s > t; t++)
                        if (ue.contains(i[t], this)) return !0
                }));
                for (t = 0; s > t; t++) ue.find(e, i[t], n);
                return n = this.pushStack(s > 1 ? ue.unique(n) : n), n.selector = this.selector ? this.selector + " " + e : e, n
            },
            has: function(e) {
                var t, n = ue(e, this),
                    i = n.length;
                return this.filter(function() {
                    for (t = 0; i > t; t++)
                        if (ue.contains(this, n[t])) return !0
                })
            },
            not: function(e) {
                return this.pushStack(h(this, e || [], !0))
            },
            filter: function(e) {
                return this.pushStack(h(this, e || [], !1))
            },
            is: function(e) {
                return !!h(this, "string" == typeof e && Ue.test(e) ? ue(e) : e || [], !1).length
            },
            closest: function(e, t) {
                for (var n, i = 0, s = this.length, r = [], o = Ue.test(e) || "string" != typeof e ? ue(e, t || this.context) : 0; s > i; i++)
                    for (n = this[i]; n && n !== t; n = n.parentNode)
                        if (n.nodeType < 11 && (o ? o.index(n) > -1 : 1 === n.nodeType && ue.find.matchesSelector(n, e))) {
                            n = r.push(n);
                            break
                        }
                return this.pushStack(r.length > 1 ? ue.unique(r) : r)
            },
            index: function(e) {
                return e ? "string" == typeof e ? ue.inArray(this[0], ue(e)) : ue.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
            },
            add: function(e, t) {
                var n = "string" == typeof e ? ue(e, t) : ue.makeArray(e && e.nodeType ? [e] : e),
                    i = ue.merge(this.get(), n);
                return this.pushStack(ue.unique(i))
            },
            addBack: function(e) {
                return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
            }
        }), ue.each({
            parent: function(e) {
                var t = e.parentNode;
                return t && 11 !== t.nodeType ? t : null
            },
            parents: function(e) {
                return ue.dir(e, "parentNode")
            },
            parentsUntil: function(e, t, n) {
                return ue.dir(e, "parentNode", n)
            },
            next: function(e) {
                return d(e, "nextSibling")
            },
            prev: function(e) {
                return d(e, "previousSibling")
            },
            nextAll: function(e) {
                return ue.dir(e, "nextSibling")
            },
            prevAll: function(e) {
                return ue.dir(e, "previousSibling")
            },
            nextUntil: function(e, t, n) {
                return ue.dir(e, "nextSibling", n)
            },
            prevUntil: function(e, t, n) {
                return ue.dir(e, "previousSibling", n)
            },
            siblings: function(e) {
                return ue.sibling((e.parentNode || {}).firstChild, e)
            },
            children: function(e) {
                return ue.sibling(e.firstChild)
            },
            contents: function(e) {
                return ue.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : ue.merge([], e.childNodes)
            }
        }, function(e, t) {
            ue.fn[e] = function(n, i) {
                var s = ue.map(this, t, n);
                return "Until" !== e.slice(-5) && (i = n), i && "string" == typeof i && (s = ue.filter(i, s)), this.length > 1 && (We[e] || (s = ue.unique(s)), ze.test(e) && (s = s.reverse())), this.pushStack(s)
            }
        }), ue.extend({
            filter: function(e, t, n) {
                var i = t[0];
                return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? ue.find.matchesSelector(i, e) ? [i] : [] : ue.find.matches(e, ue.grep(t, function(e) {
                    return 1 === e.nodeType
                }))
            },
            dir: function(e, n, i) {
                for (var s = [], r = e[n]; r && 9 !== r.nodeType && (i === t || 1 !== r.nodeType || !ue(r).is(i));) 1 === r.nodeType && s.push(r), r = r[n];
                return s
            },
            sibling: function(e, t) {
                for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
                return n
            }
        });
        var Ve = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
            Ge = / jQuery\d+="(?:null|\d+)"/g,
            Je = new RegExp("<(?:" + Ve + ")[\\s/>]", "i"),
            Xe = /^\s+/,
            Ye = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
            Qe = /<([\w:]+)/,
            Ke = /<tbody/i,
            Ze = /<|&#?\w+;/,
            et = /<(?:script|style|link)/i,
            tt = /^(?:checkbox|radio)$/i,
            nt = /checked\s*(?:[^=]|=\s*.checked.)/i,
            it = /^$|\/(?:java|ecma)script/i,
            st = /^true\/(.*)/,
            rt = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
            ot = {
                option: [1, "<select multiple='multiple'>", "</select>"],
                legend: [1, "<fieldset>", "</fieldset>"],
                area: [1, "<map>", "</map>"],
                param: [1, "<object>", "</object>"],
                thead: [1, "<table>", "</table>"],
                tr: [2, "<table><tbody>", "</tbody></table>"],
                col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
                td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
                _default: ue.support.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
            },
            at = m(Y),
            lt = at.appendChild(Y.createElement("div"));
        ot.optgroup = ot.option, ot.tbody = ot.tfoot = ot.colgroup = ot.caption = ot.thead, ot.th = ot.td, ue.fn.extend({
            text: function(e) {
                return ue.access(this, function(e) {
                    return e === t ? ue.text(this) : this.empty().append((this[0] && this[0].ownerDocument || Y).createTextNode(e))
                }, null, e, arguments.length)
            },
            append: function() {
                return this.domManip(arguments, function(e) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var t = f(this, e);
                        t.appendChild(e)
                    }
                })
            },
            prepend: function() {
                return this.domManip(arguments, function(e) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var t = f(this, e);
                        t.insertBefore(e, t.firstChild)
                    }
                })
            },
            before: function() {
                return this.domManip(arguments, function(e) {
                    this.parentNode && this.parentNode.insertBefore(e, this)
                })
            },
            after: function() {
                return this.domManip(arguments, function(e) {
                    this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
                })
            },
            remove: function(e, t) {
                for (var n, i = e ? ue.filter(e, this) : this, s = 0; null != (n = i[s]); s++) t || 1 !== n.nodeType || ue.cleanData(b(n)), n.parentNode && (t && ue.contains(n.ownerDocument, n) && v(b(n, "script")), n.parentNode.removeChild(n));
                return this
            },
            empty: function() {
                for (var e, t = 0; null != (e = this[t]); t++) {
                    for (1 === e.nodeType && ue.cleanData(b(e, !1)); e.firstChild;) e.removeChild(e.firstChild);
                    e.options && ue.nodeName(e, "select") && (e.options.length = 0)
                }
                return this
            },
            clone: function(e, t) {
                return e = null == e ? !1 : e, t = null == t ? e : t, this.map(function() {
                    return ue.clone(this, e, t)
                })
            },
            html: function(e) {
                return ue.access(this, function(e) {
                    var n = this[0] || {},
                        i = 0,
                        s = this.length;
                    if (e === t) return 1 === n.nodeType ? n.innerHTML.replace(Ge, "") : t;
                    if ("string" == typeof e && !et.test(e) && (ue.support.htmlSerialize || !Je.test(e)) && (ue.support.leadingWhitespace || !Xe.test(e)) && !ot[(Qe.exec(e) || ["", ""])[1].toLowerCase()]) {
                        e = e.replace(Ye, "<$1></$2>");
                        try {
                            for (; s > i; i++) n = this[i] || {}, 1 === n.nodeType && (ue.cleanData(b(n, !1)), n.innerHTML = e);
                            n = 0
                        } catch (r) {}
                    }
                    n && this.empty().append(e)
                }, null, e, arguments.length)
            },
            replaceWith: function() {
                var e = ue.map(this, function(e) {
                        return [e.nextSibling, e.parentNode]
                    }),
                    t = 0;
                return this.domManip(arguments, function(n) {
                    var i = e[t++],
                        s = e[t++];
                    s && (i && i.parentNode !== s && (i = this.nextSibling), ue(this).remove(), s.insertBefore(n, i))
                }, !0), t ? this : this.remove()
            },
            detach: function(e) {
                return this.remove(e, !0)
            },
            domManip: function(e, t, n) {
                e = ie.apply([], e);
                var i, s, r, o, a, l, c = 0,
                    u = this.length,
                    d = this,
                    h = u - 1,
                    m = e[0],
                    f = ue.isFunction(m);
                if (f || !(1 >= u || "string" != typeof m || ue.support.checkClone) && nt.test(m)) return this.each(function(i) {
                    var s = d.eq(i);
                    f && (e[0] = m.call(this, i, s.html())), s.domManip(e, t, n)
                });
                if (u && (l = ue.buildFragment(e, this[0].ownerDocument, !1, !n && this), i = l.firstChild, 1 === l.childNodes.length && (l = i), i)) {
                    for (o = ue.map(b(l, "script"), p), r = o.length; u > c; c++) s = l, c !== h && (s = ue.clone(s, !0, !0), r && ue.merge(o, b(s, "script"))), t.call(this[c], s, c);
                    if (r)
                        for (a = o[o.length - 1].ownerDocument, ue.map(o, g), c = 0; r > c; c++) s = o[c], it.test(s.type || "") && !ue._data(s, "globalEval") && ue.contains(a, s) && (s.src ? ue._evalUrl(s.src) : ue.globalEval((s.text || s.textContent || s.innerHTML || "").replace(rt, "")));
                    l = i = null
                }
                return this
            }
        }), ue.each({
            appendTo: "append",
            prependTo: "prepend",
            insertBefore: "before",
            insertAfter: "after",
            replaceAll: "replaceWith"
        }, function(e, t) {
            ue.fn[e] = function(e) {
                for (var n, i = 0, s = [], r = ue(e), o = r.length - 1; o >= i; i++) n = i === o ? this : this.clone(!0), ue(r[i])[t](n), se.apply(s, n.get());
                return this.pushStack(s)
            }
        }), ue.extend({
            clone: function(e, t, n) {
                var i, s, r, o, a, l = ue.contains(e.ownerDocument, e);
                if (ue.support.html5Clone || ue.isXMLDoc(e) || !Je.test("<" + e.nodeName + ">") ? r = e.cloneNode(!0) : (lt.innerHTML = e.outerHTML, lt.removeChild(r = lt.firstChild)), !(ue.support.noCloneEvent && ue.support.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || ue.isXMLDoc(e)))
                    for (i = b(r), a = b(e), o = 0; null != (s = a[o]); ++o) i[o] && w(s, i[o]);
                if (t)
                    if (n)
                        for (a = a || b(e), i = i || b(r), o = 0; null != (s = a[o]); o++) y(s, i[o]);
                    else y(e, r);
                return i = b(r, "script"), i.length > 0 && v(i, !l && b(e, "script")), i = a = s = null, r
            },
            buildFragment: function(e, t, n, i) {
                for (var s, r, o, a, l, c, u, d = e.length, h = m(t), f = [], p = 0; d > p; p++)
                    if (r = e[p], r || 0 === r)
                        if ("object" === ue.type(r)) ue.merge(f, r.nodeType ? [r] : r);
                        else if (Ze.test(r)) {
                    for (a = a || h.appendChild(t.createElement("div")), l = (Qe.exec(r) || ["", ""])[1].toLowerCase(), u = ot[l] || ot._default, a.innerHTML = u[1] + r.replace(Ye, "<$1></$2>") + u[2], s = u[0]; s--;) a = a.lastChild;
                    if (!ue.support.leadingWhitespace && Xe.test(r) && f.push(t.createTextNode(Xe.exec(r)[0])), !ue.support.tbody)
                        for (r = "table" !== l || Ke.test(r) ? "<table>" !== u[1] || Ke.test(r) ? 0 : a : a.firstChild, s = r && r.childNodes.length; s--;) ue.nodeName(c = r.childNodes[s], "tbody") && !c.childNodes.length && r.removeChild(c);
                    for (ue.merge(f, a.childNodes), a.textContent = ""; a.firstChild;) a.removeChild(a.firstChild);
                    a = h.lastChild
                } else f.push(t.createTextNode(r));
                for (a && h.removeChild(a), ue.support.appendChecked || ue.grep(b(f, "input"), _), p = 0; r = f[p++];)
                    if ((!i || -1 === ue.inArray(r, i)) && (o = ue.contains(r.ownerDocument, r), a = b(h.appendChild(r), "script"), o && v(a), n))
                        for (s = 0; r = a[s++];) it.test(r.type || "") && n.push(r);
                return a = null, h
            },
            cleanData: function(e, t) {
                for (var n, i, s, r, o = 0, a = ue.expando, l = ue.cache, c = ue.support.deleteExpando, u = ue.event.special; null != (n = e[o]); o++)
                    if ((t || ue.acceptData(n)) && (s = n[a], r = s && l[s])) {
                        if (r.events)
                            for (i in r.events) u[i] ? ue.event.remove(n, i) : ue.removeEvent(n, i, r.handle);
                        l[s] && (delete l[s], c ? delete n[a] : typeof n.removeAttribute !== J ? n.removeAttribute(a) : n[a] = null, te.push(s))
                    }
            },
            _evalUrl: function(e) {
                return ue.ajax({
                    url: e,
                    type: "GET",
                    dataType: "script",
                    async: !1,
                    global: !1,
                    "throws": !0
                })
            }
        }), ue.fn.extend({
            wrapAll: function(e) {
                if (ue.isFunction(e)) return this.each(function(t) {
                    ue(this).wrapAll(e.call(this, t))
                });
                if (this[0]) {
                    var t = ue(e, this[0].ownerDocument).eq(0).clone(!0);
                    this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                        for (var e = this; e.firstChild && 1 === e.firstChild.nodeType;) e = e.firstChild;
                        return e
                    }).append(this)
                }
                return this
            },
            wrapInner: function(e) {
                return ue.isFunction(e) ? this.each(function(t) {
                    ue(this).wrapInner(e.call(this, t))
                }) : this.each(function() {
                    var t = ue(this),
                        n = t.contents();
                    n.length ? n.wrapAll(e) : t.append(e)
                })
            },
            wrap: function(e) {
                var t = ue.isFunction(e);
                return this.each(function(n) {
                    ue(this).wrapAll(t ? e.call(this, n) : e)
                })
            },
            unwrap: function() {
                return this.parent().each(function() {
                    ue.nodeName(this, "body") || ue(this).replaceWith(this.childNodes)
                }).end()
            }
        });
        var ct, ut, dt, ht = /alpha\([^)]*\)/i,
            mt = /opacity\s*=\s*([^)]*)/,
            ft = /^(top|right|bottom|left)$/,
            pt = /^(none|table(?!-c[ea]).+)/,
            gt = /^margin/,
            vt = new RegExp("^(" + de + ")(.*)$", "i"),
            yt = new RegExp("^(" + de + ")(?!px)[a-z%]+$", "i"),
            wt = new RegExp("^([+-])=(" + de + ")", "i"),
            bt = {
                BODY: "block"
            },
            _t = {
                position: "absolute",
                visibility: "hidden",
                display: "block"
            },
            xt = {
                letterSpacing: 0,
                fontWeight: 400
            },
            Et = ["Top", "Right", "Bottom", "Left"],
            St = ["Webkit", "O", "Moz", "ms"];
        ue.fn.extend({
            css: function(e, n) {
                return ue.access(this, function(e, n, i) {
                    var s, r, o = {},
                        a = 0;
                    if (ue.isArray(n)) {
                        for (r = ut(e), s = n.length; s > a; a++) o[n[a]] = ue.css(e, n[a], !1, r);
                        return o
                    }
                    return i !== t ? ue.style(e, n, i) : ue.css(e, n)
                }, e, n, arguments.length > 1)
            },
            show: function() {
                return S(this, !0)
            },
            hide: function() {
                return S(this)
            },
            toggle: function(e) {
                return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                    E(this) ? ue(this).show() : ue(this).hide()
                })
            }
        }), ue.extend({
            cssHooks: {
                opacity: {
                    get: function(e, t) {
                        if (t) {
                            var n = dt(e, "opacity");
                            return "" === n ? "1" : n
                        }
                    }
                }
            },
            cssNumber: {
                columnCount: !0,
                fillOpacity: !0,
                fontWeight: !0,
                lineHeight: !0,
                opacity: !0,
                order: !0,
                orphans: !0,
                widows: !0,
                zIndex: !0,
                zoom: !0
            },
            cssProps: {
                "float": ue.support.cssFloat ? "cssFloat" : "styleFloat"
            },
            style: function(e, n, i, s) {
                if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                    var r, o, a, l = ue.camelCase(n),
                        c = e.style;
                    if (n = ue.cssProps[l] || (ue.cssProps[l] = x(c, l)), a = ue.cssHooks[n] || ue.cssHooks[l], i === t) return a && "get" in a && (r = a.get(e, !1, s)) !== t ? r : c[n];
                    if (o = typeof i, "string" === o && (r = wt.exec(i)) && (i = (r[1] + 1) * r[2] + parseFloat(ue.css(e, n)), o = "number"), !(null == i || "number" === o && isNaN(i) || ("number" !== o || ue.cssNumber[l] || (i += "px"), ue.support.clearCloneStyle || "" !== i || 0 !== n.indexOf("background") || (c[n] = "inherit"), a && "set" in a && (i = a.set(e, i, s)) === t))) try {
                        c[n] = i
                    } catch (u) {}
                }
            },
            css: function(e, n, i, s) {
                var r, o, a, l = ue.camelCase(n);
                return n = ue.cssProps[l] || (ue.cssProps[l] = x(e.style, l)), a = ue.cssHooks[n] || ue.cssHooks[l], a && "get" in a && (o = a.get(e, !0, i)), o === t && (o = dt(e, n, s)), "normal" === o && n in xt && (o = xt[n]), "" === i || i ? (r = parseFloat(o), i === !0 || ue.isNumeric(r) ? r || 0 : o) : o
            }
        }), e.getComputedStyle ? (ut = function(t) {
            return e.getComputedStyle(t, null)
        }, dt = function(e, n, i) {
            var s, r, o, a = i || ut(e),
                l = a ? a.getPropertyValue(n) || a[n] : t,
                c = e.style;
            return a && ("" !== l || ue.contains(e.ownerDocument, e) || (l = ue.style(e, n)), yt.test(l) && gt.test(n) && (s = c.width, r = c.minWidth, o = c.maxWidth, c.minWidth = c.maxWidth = c.width = l, l = a.width, c.width = s, c.minWidth = r, c.maxWidth = o)), l
        }) : Y.documentElement.currentStyle && (ut = function(e) {
            return e.currentStyle
        }, dt = function(e, n, i) {
            var s, r, o, a = i || ut(e),
                l = a ? a[n] : t,
                c = e.style;
            return null == l && c && c[n] && (l = c[n]), yt.test(l) && !ft.test(n) && (s = c.left, r = e.runtimeStyle, o = r && r.left, o && (r.left = e.currentStyle.left), c.left = "fontSize" === n ? "1em" : l, l = c.pixelLeft + "px", c.left = s, o && (r.left = o)), "" === l ? "auto" : l
        }), ue.each(["height", "width"], function(e, t) {
            ue.cssHooks[t] = {
                get: function(e, n, i) {
                    return n ? 0 === e.offsetWidth && pt.test(ue.css(e, "display")) ? ue.swap(e, _t, function() {
                        return j(e, t, i)
                    }) : j(e, t, i) : void 0
                },
                set: function(e, n, i) {
                    var s = i && ut(e);
                    return C(e, n, i ? k(e, t, i, ue.support.boxSizing && "border-box" === ue.css(e, "boxSizing", !1, s), s) : 0)
                }
            }
        }), ue.support.opacity || (ue.cssHooks.opacity = {
            get: function(e, t) {
                return mt.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
            },
            set: function(e, t) {
                var n = e.style,
                    i = e.currentStyle,
                    s = ue.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : "",
                    r = i && i.filter || n.filter || "";
                n.zoom = 1, (t >= 1 || "" === t) && "" === ue.trim(r.replace(ht, "")) && n.removeAttribute && (n.removeAttribute("filter"), "" === t || i && !i.filter) || (n.filter = ht.test(r) ? r.replace(ht, s) : r + " " + s)
            }
        }), ue(function() {
            ue.support.reliableMarginRight || (ue.cssHooks.marginRight = {
                get: function(e, t) {
                    return t ? ue.swap(e, {
                        display: "inline-block"
                    }, dt, [e, "marginRight"]) : void 0
                }
            }), !ue.support.pixelPosition && ue.fn.position && ue.each(["top", "left"], function(e, t) {
                ue.cssHooks[t] = {
                    get: function(e, n) {
                        return n ? (n = dt(e, t), yt.test(n) ? ue(e).position()[t] + "px" : n) : void 0
                    }
                }
            })
        }), ue.expr && ue.expr.filters && (ue.expr.filters.hidden = function(e) {
            return e.offsetWidth <= 0 && e.offsetHeight <= 0 || !ue.support.reliableHiddenOffsets && "none" === (e.style && e.style.display || ue.css(e, "display"))
        }, ue.expr.filters.visible = function(e) {
            return !ue.expr.filters.hidden(e)
        }), ue.each({
            margin: "",
            padding: "",
            border: "Width"
        }, function(e, t) {
            ue.cssHooks[e + t] = {
                expand: function(n) {
                    for (var i = 0, s = {}, r = "string" == typeof n ? n.split(" ") : [n]; 4 > i; i++) s[e + Et[i] + t] = r[i] || r[i - 2] || r[0];
                    return s
                }
            }, gt.test(e) || (ue.cssHooks[e + t].set = C)
        });
        var Ct = /%20/g,
            kt = /\[\]$/,
            jt = /\r?\n/g,
            Tt = /^(?:submit|button|image|reset|file)$/i,
            Pt = /^(?:input|select|textarea|keygen)/i;
        ue.fn.extend({
            serialize: function() {
                return ue.param(this.serializeArray())
            },
            serializeArray: function() {
                return this.map(function() {
                    var e = ue.prop(this, "elements");
                    return e ? ue.makeArray(e) : this
                }).filter(function() {
                    var e = this.type;
                    return this.name && !ue(this).is(":disabled") && Pt.test(this.nodeName) && !Tt.test(e) && (this.checked || !tt.test(e))
                }).map(function(e, t) {
                    var n = ue(this).val();
                    return null == n ? null : ue.isArray(n) ? ue.map(n, function(e) {
                        return {
                            name: t.name,
                            value: e.replace(jt, "\r\n")
                        }
                    }) : {
                        name: t.name,
                        value: n.replace(jt, "\r\n")
                    }
                }).get()
            }
        }), ue.param = function(e, n) {
            var i, s = [],
                r = function(e, t) {
                    t = ue.isFunction(t) ? t() : null == t ? "" : t, s[s.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
                };
            if (n === t && (n = ue.ajaxSettings && ue.ajaxSettings.traditional), ue.isArray(e) || e.jquery && !ue.isPlainObject(e)) ue.each(e, function() {
                r(this.name, this.value)
            });
            else
                for (i in e) O(i, e[i], n, r);
            return s.join("&").replace(Ct, "+")
        }, ue.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
            ue.fn[t] = function(e, n) {
                return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
            }
        }), ue.fn.extend({
            hover: function(e, t) {
                return this.mouseenter(e).mouseleave(t || e)
            },
            bind: function(e, t, n) {
                return this.on(e, null, t, n)
            },
            unbind: function(e, t) {
                return this.off(e, null, t)
            },
            delegate: function(e, t, n, i) {
                return this.on(t, e, n, i)
            },
            undelegate: function(e, t, n) {
                return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
            }
        });
        var Ot, $t, At = ue.now(),
            Dt = /\?/,
            Nt = /#.*$/,
            Lt = /([?&])_=[^&]*/,
            Ft = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
            It = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
            Mt = /^(?:GET|HEAD)$/,
            Bt = /^\/\//,
            Rt = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
            Ht = ue.fn.load,
            qt = {},
            zt = {},
            Ut = "*/".concat("*");
        try {
            $t = X.href
        } catch (Wt) {
            $t = Y.createElement("a"), $t.href = "", $t = $t.href
        }
        Ot = Rt.exec($t.toLowerCase()) || [], ue.fn.load = function(e, n, i) {
            if ("string" != typeof e && Ht) return Ht.apply(this, arguments);
            var s, r, o, a = this,
                l = e.indexOf(" ");
            return l >= 0 && (s = e.slice(l, e.length), e = e.slice(0, l)), ue.isFunction(n) ? (i = n, n = t) : n && "object" == typeof n && (o = "POST"), a.length > 0 && ue.ajax({
                url: e,
                type: o,
                dataType: "html",
                data: n
            }).done(function(e) {
                r = arguments, a.html(s ? ue("<div>").append(ue.parseHTML(e)).find(s) : e)
            }).complete(i && function(e, t) {
                a.each(i, r || [e.responseText, t, e])
            }), this
        }, ue.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
            ue.fn[t] = function(e) {
                return this.on(t, e)
            }
        }), ue.extend({
            active: 0,
            lastModified: {},
            etag: {},
            ajaxSettings: {
                url: $t,
                type: "GET",
                isLocal: It.test(Ot[1]),
                global: !0,
                processData: !0,
                async: !0,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                accepts: {
                    "*": Ut,
                    text: "text/plain",
                    html: "text/html",
                    xml: "application/xml, text/xml",
                    json: "application/json, text/javascript"
                },
                contents: {
                    xml: /xml/,
                    html: /html/,
                    json: /json/
                },
                responseFields: {
                    xml: "responseXML",
                    text: "responseText",
                    json: "responseJSON"
                },
                converters: {
                    "* text": String,
                    "text html": !0,
                    "text json": ue.parseJSON,
                    "text xml": ue.parseXML
                },
                flatOptions: {
                    url: !0,
                    context: !0
                }
            },
            ajaxSetup: function(e, t) {
                return t ? D(D(e, ue.ajaxSettings), t) : D(ue.ajaxSettings, e)
            },
            ajaxPrefilter: $(qt),
            ajaxTransport: $(zt),
            ajax: function(e, n) {
                function i(e, n, i, s) {
                    var r, d, y, w, _, E = n;
                    2 !== b && (b = 2, l && clearTimeout(l), u = t, a = s || "", x.readyState = e > 0 ? 4 : 0, r = e >= 200 && 300 > e || 304 === e, i && (w = N(h, x, i)), w = L(h, w, x, r), r ? (h.ifModified && (_ = x.getResponseHeader("Last-Modified"), _ && (ue.lastModified[o] = _), _ = x.getResponseHeader("etag"), _ && (ue.etag[o] = _)), 204 === e || "HEAD" === h.type ? E = "nocontent" : 304 === e ? E = "notmodified" : (E = w.state, d = w.data, y = w.error, r = !y)) : (y = E, (e || !E) && (E = "error", 0 > e && (e = 0))), x.status = e, x.statusText = (n || E) + "", r ? p.resolveWith(m, [d, E, x]) : p.rejectWith(m, [x, E, y]), x.statusCode(v), v = t, c && f.trigger(r ? "ajaxSuccess" : "ajaxError", [x, h, r ? d : y]), g.fireWith(m, [x, E]), c && (f.trigger("ajaxComplete", [x, h]), --ue.active || ue.event.trigger("ajaxStop")))
                }
                "object" == typeof e && (n = e, e = t), n = n || {};
                var s, r, o, a, l, c, u, d, h = ue.ajaxSetup({}, n),
                    m = h.context || h,
                    f = h.context && (m.nodeType || m.jquery) ? ue(m) : ue.event,
                    p = ue.Deferred(),
                    g = ue.Callbacks("once memory"),
                    v = h.statusCode || {},
                    y = {},
                    w = {},
                    b = 0,
                    _ = "canceled",
                    x = {
                        readyState: 0,
                        getResponseHeader: function(e) {
                            var t;
                            if (2 === b) {
                                if (!d)
                                    for (d = {}; t = Ft.exec(a);) d[t[1].toLowerCase()] = t[2];
                                t = d[e.toLowerCase()]
                            }
                            return null == t ? null : t
                        },
                        getAllResponseHeaders: function() {
                            return 2 === b ? a : null
                        },
                        setRequestHeader: function(e, t) {
                            var n = e.toLowerCase();
                            return b || (e = w[n] = w[n] || e, y[e] = t), this
                        },
                        overrideMimeType: function(e) {
                            return b || (h.mimeType = e), this
                        },
                        statusCode: function(e) {
                            var t;
                            if (e)
                                if (2 > b)
                                    for (t in e) v[t] = [v[t], e[t]];
                                else x.always(e[x.status]);
                            return this
                        },
                        abort: function(e) {
                            var t = e || _;
                            return u && u.abort(t), i(0, t), this
                        }
                    };
                if (p.promise(x).complete = g.add, x.success = x.done, x.error = x.fail, h.url = ((e || h.url || $t) + "").replace(Nt, "").replace(Bt, Ot[1] + "//"), h.type = n.method || n.type || h.method || h.type, h.dataTypes = ue.trim(h.dataType || "*").toLowerCase().match(he) || [""], null == h.crossDomain && (s = Rt.exec(h.url.toLowerCase()), h.crossDomain = !(!s || s[1] === Ot[1] && s[2] === Ot[2] && (s[3] || ("http:" === s[1] ? "80" : "443")) === (Ot[3] || ("http:" === Ot[1] ? "80" : "443")))), h.data && h.processData && "string" != typeof h.data && (h.data = ue.param(h.data, h.traditional)), A(qt, h, n, x), 2 === b) return x;
                c = h.global, c && 0 === ue.active++ && ue.event.trigger("ajaxStart"), h.type = h.type.toUpperCase(), h.hasContent = !Mt.test(h.type), o = h.url, h.hasContent || (h.data && (o = h.url += (Dt.test(o) ? "&" : "?") + h.data, delete h.data), h.cache === !1 && (h.url = Lt.test(o) ? o.replace(Lt, "$1_=" + At++) : o + (Dt.test(o) ? "&" : "?") + "_=" + At++)), h.ifModified && (ue.lastModified[o] && x.setRequestHeader("If-Modified-Since", ue.lastModified[o]), ue.etag[o] && x.setRequestHeader("If-None-Match", ue.etag[o])), (h.data && h.hasContent && h.contentType !== !1 || n.contentType) && x.setRequestHeader("Content-Type", h.contentType), x.setRequestHeader("Accept", h.dataTypes[0] && h.accepts[h.dataTypes[0]] ? h.accepts[h.dataTypes[0]] + ("*" !== h.dataTypes[0] ? ", " + Ut + "; q=0.01" : "") : h.accepts["*"]);
                for (r in h.headers) x.setRequestHeader(r, h.headers[r]);
                if (h.beforeSend && (h.beforeSend.call(m, x, h) === !1 || 2 === b)) return x.abort();
                _ = "abort";
                for (r in {
                        success: 1,
                        error: 1,
                        complete: 1
                    }) x[r](h[r]);
                if (u = A(zt, h, n, x)) {
                    x.readyState = 1, c && f.trigger("ajaxSend", [x, h]), h.async && h.timeout > 0 && (l = setTimeout(function() {
                        x.abort("timeout")
                    }, h.timeout));
                    try {
                        b = 1, u.send(y, i)
                    } catch (E) {
                        if (!(2 > b)) throw E;
                        i(-1, E)
                    }
                } else i(-1, "No Transport");
                return x
            },
            getJSON: function(e, t, n) {
                return ue.get(e, t, n, "json")
            },
            getScript: function(e, n) {
                return ue.get(e, t, n, "script")
            }
        }), ue.each(["get", "post"], function(e, n) {
            ue[n] = function(e, i, s, r) {
                return ue.isFunction(i) && (r = r || s, s = i, i = t), ue.ajax({
                    url: e,
                    type: n,
                    dataType: r,
                    data: i,
                    success: s
                })
            }
        }), ue.ajaxSetup({
            accepts: {
                script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
            },
            contents: {
                script: /(?:java|ecma)script/
            },
            converters: {
                "text script": function(e) {
                    return ue.globalEval(e), e
                }
            }
        }), ue.ajaxPrefilter("script", function(e) {
            e.cache === t && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1)
        }), ue.ajaxTransport("script", function(e) {
            if (e.crossDomain) {
                var n, i = Y.head || ue("head")[0] || Y.documentElement;
                return {
                    send: function(t, s) {
                        n = Y.createElement("script"), n.async = !0, e.scriptCharset && (n.charset = e.scriptCharset), n.src = e.url, n.onload = n.onreadystatechange = function(e, t) {
                            (t || !n.readyState || /loaded|complete/.test(n.readyState)) && (n.onload = n.onreadystatechange = null, n.parentNode && n.parentNode.removeChild(n), n = null, t || s(200, "success"))
                        }, i.insertBefore(n, i.firstChild)
                    },
                    abort: function() {
                        n && n.onload(t, !0)
                    }
                }
            }
        });
        var Vt = [],
            Gt = /(=)\?(?=&|$)|\?\?/;
        ue.ajaxSetup({
            jsonp: "callback",
            jsonpCallback: function() {
                var e = Vt.pop() || ue.expando + "_" + At++;
                return this[e] = !0, e
            }
        }), ue.ajaxPrefilter("json jsonp", function(n, i, s) {
            var r, o, a, l = n.jsonp !== !1 && (Gt.test(n.url) ? "url" : "string" == typeof n.data && !(n.contentType || "").indexOf("application/x-www-form-urlencoded") && Gt.test(n.data) && "data");
            return l || "jsonp" === n.dataTypes[0] ? (r = n.jsonpCallback = ue.isFunction(n.jsonpCallback) ? n.jsonpCallback() : n.jsonpCallback, l ? n[l] = n[l].replace(Gt, "$1" + r) : n.jsonp !== !1 && (n.url += (Dt.test(n.url) ? "&" : "?") + n.jsonp + "=" + r), n.converters["script json"] = function() {
                return a || ue.error(r + " was not called"), a[0]
            }, n.dataTypes[0] = "json", o = e[r], e[r] = function() {
                a = arguments
            }, s.always(function() {
                e[r] = o, n[r] && (n.jsonpCallback = i.jsonpCallback, Vt.push(r)), a && ue.isFunction(o) && o(a[0]), a = o = t
            }), "script") : void 0
        });
        var Jt, Xt, Yt = 0,
            Qt = e.ActiveXObject && function() {
                var e;
                for (e in Jt) Jt[e](t, !0)
            };
        ue.ajaxSettings.xhr = e.ActiveXObject ? function() {
            return !this.isLocal && F() || I()
        } : F, Xt = ue.ajaxSettings.xhr(), ue.support.cors = !!Xt && "withCredentials" in Xt, Xt = ue.support.ajax = !!Xt, Xt && ue.ajaxTransport(function(n) {
            if (!n.crossDomain || ue.support.cors) {
                var i;
                return {
                    send: function(s, r) {
                        var o, a, l = n.xhr();
                        if (n.username ? l.open(n.type, n.url, n.async, n.username, n.password) : l.open(n.type, n.url, n.async), n.xhrFields)
                            for (a in n.xhrFields) l[a] = n.xhrFields[a];
                        n.mimeType && l.overrideMimeType && l.overrideMimeType(n.mimeType), n.crossDomain || s["X-Requested-With"] || (s["X-Requested-With"] = "XMLHttpRequest");
                        try {
                            for (a in s) l.setRequestHeader(a, s[a])
                        } catch (c) {}
                        l.send(n.hasContent && n.data || null), i = function(e, s) {
                            var a, c, u, d;
                            try {
                                if (i && (s || 4 === l.readyState))
                                    if (i = t, o && (l.onreadystatechange = ue.noop, Qt && delete Jt[o]), s) 4 !== l.readyState && l.abort();
                                    else {
                                        d = {}, a = l.status, c = l.getAllResponseHeaders(), "string" == typeof l.responseText && (d.text = l.responseText);
                                        try {
                                            u = l.statusText
                                        } catch (h) {
                                            u = ""
                                        }
                                        a || !n.isLocal || n.crossDomain ? 1223 === a && (a = 204) : a = d.text ? 200 : 404
                                    }
                            } catch (m) {
                                s || r(-1, m)
                            }
                            d && r(a, u, d, c)
                        }, n.async ? 4 === l.readyState ? setTimeout(i) : (o = ++Yt, Qt && (Jt || (Jt = {}, ue(e).unload(Qt)), Jt[o] = i), l.onreadystatechange = i) : i()
                    },
                    abort: function() {
                        i && i(t, !0)
                    }
                }
            }
        });
        var Kt, Zt, en = /^(?:toggle|show|hide)$/,
            tn = new RegExp("^(?:([+-])=|)(" + de + ")([a-z%]*)$", "i"),
            nn = /queueHooks$/,
            sn = [q],
            rn = {
                "*": [function(e, t) {
                    var n = this.createTween(e, t),
                        i = n.cur(),
                        s = tn.exec(t),
                        r = s && s[3] || (ue.cssNumber[e] ? "" : "px"),
                        o = (ue.cssNumber[e] || "px" !== r && +i) && tn.exec(ue.css(n.elem, e)),
                        a = 1,
                        l = 20;
                    if (o && o[3] !== r) {
                        r = r || o[3], s = s || [], o = +i || 1;
                        do a = a || ".5", o /= a, ue.style(n.elem, e, o + r); while (a !== (a = n.cur() / i) && 1 !== a && --l)
                    }
                    return s && (o = n.start = +o || +i || 0, n.unit = r, n.end = s[1] ? o + (s[1] + 1) * s[2] : +s[2]), n
                }]
            };
        ue.Animation = ue.extend(R, {
            tweener: function(e, t) {
                ue.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
                for (var n, i = 0, s = e.length; s > i; i++) n = e[i], rn[n] = rn[n] || [], rn[n].unshift(t)
            },
            prefilter: function(e, t) {
                t ? sn.unshift(e) : sn.push(e)
            }
        }), ue.Tween = z, z.prototype = {
            constructor: z,
            init: function(e, t, n, i, s, r) {
                this.elem = e, this.prop = n, this.easing = s || "swing", this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = r || (ue.cssNumber[n] ? "" : "px")
            },
            cur: function() {
                var e = z.propHooks[this.prop];
                return e && e.get ? e.get(this) : z.propHooks._default.get(this)
            },
            run: function(e) {
                var t, n = z.propHooks[this.prop];
                return this.options.duration ? this.pos = t = ue.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : z.propHooks._default.set(this), this
            }
        }, z.prototype.init.prototype = z.prototype, z.propHooks = {
            _default: {
                get: function(e) {
                    var t;
                    return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = ue.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
                },
                set: function(e) {
                    ue.fx.step[e.prop] ? ue.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[ue.cssProps[e.prop]] || ue.cssHooks[e.prop]) ? ue.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
                }
            }
        }, z.propHooks.scrollTop = z.propHooks.scrollLeft = {
            set: function(e) {
                e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
            }
        }, ue.each(["toggle", "show", "hide"], function(e, t) {
            var n = ue.fn[t];
            ue.fn[t] = function(e, i, s) {
                return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(U(t, !0), e, i, s)
            }
        }), ue.fn.extend({
            fadeTo: function(e, t, n, i) {
                return this.filter(E).css("opacity", 0).show().end().animate({
                    opacity: t
                }, e, n, i)
            },
            animate: function(e, t, n, i) {
                var s = ue.isEmptyObject(e),
                    r = ue.speed(t, n, i),
                    o = function() {
                        var t = R(this, ue.extend({}, e), r);
                        (s || ue._data(this, "finish")) && t.stop(!0)
                    };
                return o.finish = o, s || r.queue === !1 ? this.each(o) : this.queue(r.queue, o)
            },
            stop: function(e, n, i) {
                var s = function(e) {
                    var t = e.stop;
                    delete e.stop, t(i)
                };
                return "string" != typeof e && (i = n, n = e, e = t), n && e !== !1 && this.queue(e || "fx", []), this.each(function() {
                    var t = !0,
                        n = null != e && e + "queueHooks",
                        r = ue.timers,
                        o = ue._data(this);
                    if (n) o[n] && o[n].stop && s(o[n]);
                    else
                        for (n in o) o[n] && o[n].stop && nn.test(n) && s(o[n]);
                    for (n = r.length; n--;) r[n].elem !== this || null != e && r[n].queue !== e || (r[n].anim.stop(i), t = !1, r.splice(n, 1));
                    (t || !i) && ue.dequeue(this, e)
                })
            },
            finish: function(e) {
                return e !== !1 && (e = e || "fx"), this.each(function() {
                    var t, n = ue._data(this),
                        i = n[e + "queue"],
                        s = n[e + "queueHooks"],
                        r = ue.timers,
                        o = i ? i.length : 0;
                    for (n.finish = !0, ue.queue(this, e, []), s && s.stop && s.stop.call(this, !0), t = r.length; t--;) r[t].elem === this && r[t].queue === e && (r[t].anim.stop(!0), r.splice(t, 1));
                    for (t = 0; o > t; t++) i[t] && i[t].finish && i[t].finish.call(this);
                    delete n.finish
                })
            }
        }), ue.each({
            slideDown: U("show"),
            slideUp: U("hide"),
            slideToggle: U("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function(e, t) {
            ue.fn[e] = function(e, n, i) {
                return this.animate(t, e, n, i)
            }
        }), ue.speed = function(e, t, n) {
            var i = e && "object" == typeof e ? ue.extend({}, e) : {
                complete: n || !n && t || ue.isFunction(e) && e,
                duration: e,
                easing: n && t || t && !ue.isFunction(t) && t
            };
            return i.duration = ue.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in ue.fx.speeds ? ue.fx.speeds[i.duration] : ue.fx.speeds._default, (null == i.queue || i.queue === !0) && (i.queue = "fx"), i.old = i.complete, i.complete = function() {
                ue.isFunction(i.old) && i.old.call(this), i.queue && ue.dequeue(this, i.queue)
            }, i
        }, ue.easing = {
            linear: function(e) {
                return e
            },
            swing: function(e) {
                return .5 - Math.cos(e * Math.PI) / 2
            }
        }, ue.timers = [], ue.fx = z.prototype.init, ue.fx.tick = function() {
            var e, n = ue.timers,
                i = 0;
            for (Kt = ue.now(); i < n.length; i++) e = n[i], e() || n[i] !== e || n.splice(i--, 1);
            n.length || ue.fx.stop(), Kt = t
        }, ue.fx.timer = function(e) {
            e() && ue.timers.push(e) && ue.fx.start()
        }, ue.fx.interval = 13, ue.fx.start = function() {
            Zt || (Zt = setInterval(ue.fx.tick, ue.fx.interval))
        }, ue.fx.stop = function() {
            clearInterval(Zt), Zt = null
        }, ue.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        }, ue.fx.step = {}, ue.expr && ue.expr.filters && (ue.expr.filters.animated = function(e) {
            return ue.grep(ue.timers, function(t) {
                return e === t.elem
            }).length
        }), ue.fn.offset = function(e) {
            if (arguments.length) return e === t ? this : this.each(function(t) {
                ue.offset.setOffset(this, e, t)
            });
            var n, i, s = {
                    top: 0,
                    left: 0
                },
                r = this[0],
                o = r && r.ownerDocument;
            if (o) return n = o.documentElement, ue.contains(n, r) ? (typeof r.getBoundingClientRect !== J && (s = r.getBoundingClientRect()), i = W(o), {
                top: s.top + (i.pageYOffset || n.scrollTop) - (n.clientTop || 0),
                left: s.left + (i.pageXOffset || n.scrollLeft) - (n.clientLeft || 0)
            }) : s
        }, ue.offset = {
            setOffset: function(e, t, n) {
                var i = ue.css(e, "position");
                "static" === i && (e.style.position = "relative");
                var s, r, o = ue(e),
                    a = o.offset(),
                    l = ue.css(e, "top"),
                    c = ue.css(e, "left"),
                    u = ("absolute" === i || "fixed" === i) && ue.inArray("auto", [l, c]) > -1,
                    d = {},
                    h = {};
                u ? (h = o.position(), s = h.top, r = h.left) : (s = parseFloat(l) || 0, r = parseFloat(c) || 0), ue.isFunction(t) && (t = t.call(e, n, a)), null != t.top && (d.top = t.top - a.top + s), null != t.left && (d.left = t.left - a.left + r), "using" in t ? t.using.call(e, d) : o.css(d)
            }
        }, ue.fn.extend({
            position: function() {
                if (this[0]) {
                    var e, t, n = {
                            top: 0,
                            left: 0
                        },
                        i = this[0];
                    return "fixed" === ue.css(i, "position") ? t = i.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), ue.nodeName(e[0], "html") || (n = e.offset()), n.top += ue.css(e[0], "borderTopWidth", !0), n.left += ue.css(e[0], "borderLeftWidth", !0)), {
                        top: t.top - n.top - ue.css(i, "marginTop", !0),
                        left: t.left - n.left - ue.css(i, "marginLeft", !0)
                    }
                }
            },
            offsetParent: function() {
                return this.map(function() {
                    for (var e = this.offsetParent || Q; e && !ue.nodeName(e, "html") && "static" === ue.css(e, "position");) e = e.offsetParent;
                    return e || Q
                })
            }
        }), ue.each({
            scrollLeft: "pageXOffset",
            scrollTop: "pageYOffset"
        }, function(e, n) {
            var i = /Y/.test(n);
            ue.fn[e] = function(s) {
                return ue.access(this, function(e, s, r) {
                    var o = W(e);
                    return r === t ? o ? n in o ? o[n] : o.document.documentElement[s] : e[s] : void(o ? o.scrollTo(i ? ue(o).scrollLeft() : r, i ? r : ue(o).scrollTop()) : e[s] = r)
                }, e, s, arguments.length, null)
            }
        }), ue.each({
            Height: "height",
            Width: "width"
        }, function(e, n) {
            ue.each({
                padding: "inner" + e,
                content: n,
                "": "outer" + e
            }, function(i, s) {
                ue.fn[s] = function(s, r) {
                    var o = arguments.length && (i || "boolean" != typeof s),
                        a = i || (s === !0 || r === !0 ? "margin" : "border");
                    return ue.access(this, function(n, i, s) {
                        var r;
                        return ue.isWindow(n) ? n.document.documentElement["client" + e] : 9 === n.nodeType ? (r = n.documentElement, Math.max(n.body["scroll" + e], r["scroll" + e], n.body["offset" + e], r["offset" + e], r["client" + e])) : s === t ? ue.css(n, i, a) : ue.style(n, i, s, a)
                    }, n, o ? s : t, o, null)
                }
            })
        }), ue.fn.size = function() {
            return this.length
        }, ue.fn.andSelf = ue.fn.addBack, "object" == typeof module && module && "object" == typeof module.exports ? module.exports = ue : (e.jQuery = e.$ = ue, "function" == typeof define && define.amd && define("jquery", [], function() {
            return ue
        }))
    }(window), jQuery.noConflict(), define("/sites/default/themes/siedler/js/external/jquery-1.10.2.js", function() {}), MooTools.More = {
        version: "1.4.0.1",
        build: "a4244edf2aa97ac8a196fc96082dd35af1abab87"
    },
    function() {
        Events.Pseudos = function(e, t, n) {
            var i = "_monitorEvents:",
                s = function(e) {
                    return {
                        store: e.store ? function(t, n) {
                            e.store(i + t, n)
                        } : function(t, n) {
                            (e._monitorEvents || (e._monitorEvents = {}))[t] = n
                        },
                        retrieve: e.retrieve ? function(t, n) {
                            return e.retrieve(i + t, n)
                        } : function(t, n) {
                            return e._monitorEvents ? e._monitorEvents[t] || n : n
                        }
                    }
                },
                r = function(t) {
                    if (-1 == t.indexOf(":") || !e) return null;
                    for (var n = Slick.parse(t).expressions[0][0], i = n.pseudos, s = i.length, r = []; s--;) {
                        var o = i[s].key,
                            a = e[o];
                        null != a && r.push({
                            event: n.tag,
                            value: i[s].value,
                            pseudo: o,
                            original: t,
                            listener: a
                        })
                    }
                    return r.length ? r : null
                };
            return {
                addEvent: function(e, n, i) {
                    var o = r(e);
                    if (!o) return t.call(this, e, n, i);
                    var a = s(this),
                        l = a.retrieve(e, []),
                        c = o[0].event,
                        u = Array.slice(arguments, 2),
                        d = n,
                        h = this;
                    return o.each(function(e) {
                        var t = e.listener,
                            n = d;
                        0 == t ? c += ":" + e.pseudo + "(" + e.value + ")" : d = function() {
                            t.call(h, e, n, arguments, d)
                        }
                    }), l.include({
                        type: c,
                        event: n,
                        monitor: d
                    }), a.store(e, l), e != c && t.apply(this, [e, n].concat(u)), t.apply(this, [c, d].concat(u))
                },
                removeEvent: function(e, t) {
                    var i = r(e);
                    if (!i) return n.call(this, e, t);
                    var o = s(this),
                        a = o.retrieve(e);
                    if (!a) return this;
                    var l = Array.slice(arguments, 2);
                    return n.apply(this, [e, t].concat(l)), a.each(function(e, i) {
                        t && e.event != t || n.apply(this, [e.type, e.monitor].concat(l)), delete a[i]
                    }, this), o.store(e, a), this
                }
            }
        };
        var e = {
            once: function(e, t, n, i) {
                t.apply(this, n), this.removeEvent(e.event, i).removeEvent(e.original, t)
            },
            throttle: function(e, t, n) {
                t._throttled || (t.apply(this, n), t._throttled = setTimeout(function() {
                    t._throttled = !1
                }, e.value || 250))
            },
            pause: function(e, t, n) {
                clearTimeout(t._pause), t._pause = t.delay(e.value || 250, this, n)
            }
        };
        Events.definePseudo = function(t, n) {
            return e[t] = n, this
        }, Events.lookupPseudo = function(t) {
            return e[t]
        };
        var t = Events.prototype;
        Events.implement(Events.Pseudos(e, t.addEvent, t.removeEvent)), ["Request", "Fx"].each(function(e) {
            this[e] && this[e].implement(Events.prototype)
        })
    }(), String.implement({
        parseQueryString: function(e, t) {
            null == e && (e = !0), null == t && (t = !0);
            var n = this.split(/[&;]/),
                i = {};
            return n.length ? (n.each(function(n) {
                var s = n.indexOf("=") + 1,
                    r = s ? n.substr(s) : "",
                    o = s ? n.substr(0, s - 1).match(/([^\]\[]+|(\B)(?=\]))/g) : [n],
                    a = i;
                o && (t && (r = decodeURIComponent(r)), o.each(function(t, n) {
                    e && (t = decodeURIComponent(t));
                    var i = a[t];
                    n < o.length - 1 ? a = a[t] = i || {} : "array" == typeOf(i) ? i.push(r) : a[t] = null != i ? [i, r] : r
                }))
            }), i) : i
        },
        cleanQueryString: function(e) {
            return this.split("&").filter(function(t) {
                var n = t.indexOf("="),
                    i = 0 > n ? "" : t.substr(0, n),
                    s = t.substr(n + 1);
                return e ? e.call(null, i, s) : s || 0 === s
            }).join("&")
        }
    }),
    function() {
        var e = function() {
                return this.get("value")
            },
            t = this.URI = new Class({
                Implements: Options,
                options: {},
                regex: /^(?:(\w+):)?(?:\/\/(?:(?:([^:@\/]*):?([^:@\/]*))?@)?([^:\/?#]*)(?::(\d*))?)?(\.\.?$|(?:[^?#\/]*\/)*)([^?#]*)(?:\?([^#]*))?(?:#(.*))?/,
                parts: ["scheme", "user", "password", "host", "port", "directory", "file", "query", "fragment"],
                schemes: {
                    http: 80,
                    https: 443,
                    ftp: 21,
                    rtsp: 554,
                    mms: 1755,
                    file: 0
                },
                initialize: function(e, n) {
                    this.setOptions(n);
                    var i = this.options.base || t.base;
                    e || (e = i), e && e.parsed ? this.parsed = Object.clone(e.parsed) : this.set("value", e.href || e.toString(), i ? new t(i) : !1)
                },
                parse: function(e, t) {
                    var n = e.match(this.regex);
                    return n ? (n.shift(), this.merge(n.associate(this.parts), t)) : !1
                },
                merge: function(e, t) {
                    return e && e.scheme || t && t.scheme ? (t && this.parts.every(function(n) {
                        return e[n] ? !1 : (e[n] = t[n] || "", !0)
                    }), e.port = e.port || this.schemes[e.scheme.toLowerCase()], e.directory = e.directory ? this.parseDirectory(e.directory, t ? t.directory : "") : "/", e) : !1
                },
                parseDirectory: function(e, n) {
                    if (e = ("/" == e.substr(0, 1) ? "" : n || "/") + e, !e.test(t.regs.directoryDot)) return e;
                    var i = [];
                    return e.replace(t.regs.endSlash, "").split("/").each(function(e) {
                        ".." == e && i.length > 0 ? i.pop() : "." != e && i.push(e)
                    }), i.join("/") + "/"
                },
                combine: function(e) {
                    return e.value || e.scheme + "://" + (e.user ? e.user + (e.password ? ":" + e.password : "") + "@" : "") + (e.host || "") + (e.port && e.port != this.schemes[e.scheme] ? ":" + e.port : "") + (e.directory || "/") + (e.file || "") + (e.query ? "?" + e.query : "") + (e.fragment ? "#" + e.fragment : "")
                },
                set: function(e, n, i) {
                    if ("value" == e) {
                        var s = n.match(t.regs.scheme);
                        s && (s = s[1]), s && null == this.schemes[s.toLowerCase()] ? this.parsed = {
                            scheme: s,
                            value: n
                        } : this.parsed = this.parse(n, (i || this).parsed) || (s ? {
                            scheme: s,
                            value: n
                        } : {
                            value: n
                        })
                    } else "data" == e ? this.setData(n) : this.parsed[e] = n;
                    return this
                },
                get: function(e, t) {
                    switch (e) {
                        case "value":
                            return this.combine(this.parsed, t ? t.parsed : !1);
                        case "data":
                            return this.getData()
                    }
                    return this.parsed[e] || ""
                },
                go: function() {
                    document.location.href = this.toString()
                },
                toURI: function() {
                    return this
                },
                getData: function(e, t) {
                    var n = this.get(t || "query");
                    if (!n && 0 !== n) return e ? null : {};
                    var i = n.parseQueryString();
                    return e ? i[e] : i
                },
                setData: function(e, t, n) {
                    if ("string" == typeof e) {
                        var i = this.getData();
                        i[arguments[0]] = arguments[1], e = i
                    } else t && (e = Object.merge(this.getData(), e));
                    return this.set(n || "query", Object.toQueryString(e))
                },
                clearData: function(e) {
                    return this.set(e || "query", "")
                },
                toString: e,
                valueOf: e
            });
        t.regs = {
            endSlash: /\/$/,
            scheme: /^(\w+):/,
            directoryDot: /\.\/|\.$/
        }, t.base = new t(Array.from(document.getElements("base[href]", !0)).getLast(), {
            base: document.location
        }), String.implement({
            toURI: function(e) {
                return new t(this, e)
            }
        })
    }(), Class.refactor = function(e, t) {
        return Object.each(t, function(t, n) {
            var i = e.prototype[n];
            i = i && i.$origin || i || function() {}, e.implement(n, "function" == typeof t ? function() {
                var e = this.previous;
                this.previous = i;
                var n = t.apply(this, arguments);
                return this.previous = e, n
            } : t)
        }), e
    }, URI = Class.refactor(URI, {
        combine: function(e, t) {
            if (!t || e.scheme != t.scheme || e.host != t.host || e.port != t.port) return this.previous.apply(this, arguments);
            var n = e.file + (e.query ? "?" + e.query : "") + (e.fragment ? "#" + e.fragment : "");
            if (!t.directory) return (e.directory || (e.file ? "" : "./")) + n;
            var i, s = t.directory.split("/"),
                r = e.directory.split("/"),
                o = "",
                a = 0;
            for (i = 0; i < s.length && i < r.length && s[i] == r[i]; i++);
            for (a = 0; a < s.length - i - 1; a++) o += "../";
            for (a = i; a < r.length - 1; a++) o += r[a] + "/";
            return (o || (e.file ? "" : "./")) + n
        },
        toAbsolute: function(e) {
            return e = new URI(e), e && e.set("directory", "").set("file", ""), this.toRelative(e)
        },
        toRelative: function(e) {
            return this.get("value", new URI(e))
        }
    }),
    function() {
        for (var e = {
                relay: !1
            }, t = ["once", "throttle", "pause"], n = t.length; n--;) e[t[n]] = Events.lookupPseudo(t[n]);
        DOMEvent.definePseudo = function(t, n) {
            return e[t] = n, this
        };
        var i = Element.prototype;
        [Element, Window, Document].invoke("implement", Events.Pseudos(e, i.addEvent, i.removeEvent))
    }(),
    function() {
        var e = function(e, t) {
                var n = [];
                return Object.each(t, function(t) {
                    Object.each(t, function(t) {
                        e.each(function(e) {
                            n.push(e + "-" + t + ("border" == e ? "-width" : ""))
                        })
                    })
                }), n
            },
            t = function(e, t) {
                var n = 0;
                return Object.each(t, function(t, i) {
                    i.test(e) && (n += t.toInt())
                }), n
            },
            n = function(e) {
                return !(e && !e.offsetHeight && !e.offsetWidth)
            };
        Element.implement({
            measure: function(e) {
                if (n(this)) return e.call(this);
                for (var t = this.getParent(), i = []; !n(t) && t != document.body;) i.push(t.expose()), t = t.getParent();
                var s = this.expose(),
                    r = e.call(this);
                return s(), i.each(function(e) {
                    e()
                }), r
            },
            expose: function() {
                if ("none" != this.getStyle("display")) return function() {};
                var e = this.style.cssText;
                return this.setStyles({
                        display: "block",
                        position: "absolute",
                        visibility: "hidden"
                    }),
                    function() {
                        this.style.cssText = e
                    }.bind(this)
            },
            getDimensions: function(e) {
                e = Object.merge({
                    computeSize: !1
                }, e);
                var t = {
                        x: 0,
                        y: 0
                    },
                    n = function(e, t) {
                        return t.computeSize ? e.getComputedSize(t) : e.getSize()
                    },
                    i = this.getParent("body");
                if (i && "none" == this.getStyle("display")) t = this.measure(function() {
                    return n(this, e)
                });
                else if (i) try {
                    t = n(this, e)
                } catch (s) {}
                return Object.append(t, t.x || 0 === t.x ? {
                    width: t.x,
                    height: t.y
                } : {
                    x: t.width,
                    y: t.height
                })
            },
            getComputedSize: function(n) {
                n = Object.merge({
                    styles: ["padding", "border"],
                    planes: {
                        height: ["top", "bottom"],
                        width: ["left", "right"]
                    },
                    mode: "both"
                }, n);
                var i, s = {},
                    r = {
                        width: 0,
                        height: 0
                    };
                return "vertical" == n.mode ? (delete r.width, delete n.planes.width) : "horizontal" == n.mode && (delete r.height, delete n.planes.height), e(n.styles, n.planes).each(function(e) {
                    s[e] = this.getStyle(e).toInt()
                }, this), Object.each(n.planes, function(e, n) {
                    var o = n.capitalize(),
                        a = this.getStyle(n);
                    "auto" != a || i || (i = this.getDimensions()), a = s[n] = "auto" == a ? i[n] : a.toInt(), r["total" + o] = a, e.each(function(e) {
                        var n = t(e, s);
                        r["computed" + e.capitalize()] = n, r["total" + o] += n
                    })
                }, this), Object.append(r, s)
            }
        })
    }(),
    function(e) {
        var t = Element.Position = {
            options: {
                relativeTo: document.body,
                position: {
                    x: "center",
                    y: "center"
                },
                offset: {
                    x: 0,
                    y: 0
                }
            },
            getOptions: function(e, n) {
                return n = Object.merge({}, t.options, n), t.setPositionOption(n), t.setEdgeOption(n), t.setOffsetOption(e, n), t.setDimensionsOption(e, n), n
            },
            setPositionOption: function(e) {
                e.position = t.getCoordinateFromValue(e.position)
            },
            setEdgeOption: function(e) {
                var n = t.getCoordinateFromValue(e.edge);
                e.edge = n ? n : "center" == e.position.x && "center" == e.position.y ? {
                    x: "center",
                    y: "center"
                } : {
                    x: "left",
                    y: "top"
                }
            },
            setOffsetOption: function(e, t) {
                var n = {
                        x: 0,
                        y: 0
                    },
                    i = e.measure(function() {
                        return document.id(this.getOffsetParent())
                    }),
                    s = i.getScroll();
                i && i != e.getDocument().body && (n = i.measure(function() {
                    var e = this.getPosition();
                    if ("fixed" == this.getStyle("position")) {
                        var t = window.getScroll();
                        e.x += t.x, e.y += t.y
                    }
                    return e
                }), t.offset = {
                    parentPositioned: i != document.id(t.relativeTo),
                    x: t.offset.x - n.x + s.x,
                    y: t.offset.y - n.y + s.y
                })
            },
            setDimensionsOption: function(e, t) {
                t.dimensions = e.getDimensions({
                    computeSize: !0,
                    styles: ["padding", "border", "margin"]
                })
            },
            getPosition: function(e, n) {
                var i = {};
                n = t.getOptions(e, n);
                var s = document.id(n.relativeTo) || document.body;
                t.setPositionCoordinates(n, i, s), n.edge && t.toEdge(i, n);
                var r = n.offset;
                return i.left = (i.x >= 0 || r.parentPositioned || n.allowNegative ? i.x : 0).toInt(), i.top = (i.y >= 0 || r.parentPositioned || n.allowNegative ? i.y : 0).toInt(), t.toMinMax(i, n), (n.relFixedPosition || "fixed" == s.getStyle("position")) && t.toRelFixedPosition(s, i), n.ignoreScroll && t.toIgnoreScroll(s, i), n.ignoreMargins && t.toIgnoreMargins(i, n), i.left = Math.ceil(i.left), i.top = Math.ceil(i.top), delete i.x, delete i.y, i
            },
            setPositionCoordinates: function(e, t, n) {
                var i = e.offset.y,
                    s = e.offset.x,
                    r = n == document.body ? window.getScroll() : n.getPosition(),
                    o = r.y,
                    a = r.x,
                    l = window.getSize();
                switch (e.position.x) {
                    case "left":
                        t.x = a + s;
                        break;
                    case "right":
                        t.x = a + s + n.offsetWidth;
                        break;
                    default:
                        t.x = a + (n == document.body ? l.x : n.offsetWidth) / 2 + s
                }
                switch (e.position.y) {
                    case "top":
                        t.y = o + i;
                        break;
                    case "bottom":
                        t.y = o + i + n.offsetHeight;
                        break;
                    default:
                        t.y = o + (n == document.body ? l.y : n.offsetHeight) / 2 + i
                }
            },
            toMinMax: function(e, t) {
                var n, i = {
                    left: "x",
                    top: "y"
                };
                ["minimum", "maximum"].each(function(s) {
                    ["left", "top"].each(function(r) {
                        n = t[s] ? t[s][i[r]] : null, null != n && ("minimum" == s ? e[r] < n : e[r] > n) && (e[r] = n)
                    })
                })
            },
            toRelFixedPosition: function(e, t) {
                var n = window.getScroll();
                t.top += n.y, t.left += n.x
            },
            toIgnoreScroll: function(e, t) {
                var n = e.getScroll();
                t.top -= n.y, t.left -= n.x
            },
            toIgnoreMargins: function(e, t) {
                e.left += "right" == t.edge.x ? t.dimensions["margin-right"] : "center" != t.edge.x ? -t.dimensions["margin-left"] : -t.dimensions["margin-left"] + (t.dimensions["margin-right"] + t.dimensions["margin-left"]) / 2, e.top += "bottom" == t.edge.y ? t.dimensions["margin-bottom"] : "center" != t.edge.y ? -t.dimensions["margin-top"] : -t.dimensions["margin-top"] + (t.dimensions["margin-bottom"] + t.dimensions["margin-top"]) / 2
            },
            toEdge: function(e, t) {
                var n = {},
                    i = t.dimensions,
                    s = t.edge;
                switch (s.x) {
                    case "left":
                        n.x = 0;
                        break;
                    case "right":
                        n.x = -i.x - i.computedRight - i.computedLeft;
                        break;
                    default:
                        n.x = -Math.round(i.totalWidth / 2)
                }
                switch (s.y) {
                    case "top":
                        n.y = 0;
                        break;
                    case "bottom":
                        n.y = -i.y - i.computedTop - i.computedBottom;
                        break;
                    default:
                        n.y = -Math.round(i.totalHeight / 2)
                }
                e.x += n.x, e.y += n.y
            },
            getCoordinateFromValue: function(e) {
                return "string" != typeOf(e) ? e : (e = e.toLowerCase(), {
                    x: e.test("left") ? "left" : e.test("right") ? "right" : "center",
                    y: e.test(/upper|top/) ? "top" : e.test("bottom") ? "bottom" : "center"
                })
            }
        };
        Element.implement({
            position: function(t) {
                if (t && (null != t.x || null != t.y)) return e ? e.apply(this, arguments) : this;
                var n = this.setStyle("position", "absolute").calculatePosition(t);
                return t && t.returnPos ? n : this.setStyles(n)
            },
            calculatePosition: function(e) {
                return t.getPosition(this, e)
            }
        })
    }(Element.prototype.position),
    function() {
        var e = function(e) {
                return null != e
            },
            t = Object.prototype.hasOwnProperty;
        Object.extend({
            getFromPath: function(e, n) {
                "string" == typeof n && (n = n.split("."));
                for (var i = 0, s = n.length; s > i; i++) {
                    if (!t.call(e, n[i])) return null;
                    e = e[n[i]]
                }
                return e
            },
            cleanValues: function(t, n) {
                n = n || e;
                for (var i in t) n(t[i]) || delete t[i];
                return t
            },
            erase: function(e, n) {
                return t.call(e, n) && delete e[n], e
            },
            run: function(e) {
                var t = Array.slice(arguments, 1);
                for (var n in e) e[n].apply && e[n].apply(e, t);
                return e
            }
        })
    }(),
    function() {
        var e = null,
            t = {},
            n = function(e) {
                return instanceOf(e, i.Set) ? e : t[e]
            },
            i = this.Locale = {
                define: function(n, s, r, o) {
                    var a;
                    return instanceOf(n, i.Set) ? (a = n.name, a && (t[a] = n)) : (a = n, t[a] || (t[a] = new i.Set(a)), n = t[a]), s && n.define(s, r, o), e || (e = n), n
                },
                use: function(t) {
                    return t = n(t), t && (e = t, this.fireEvent("change", t)), this
                },
                getCurrent: function() {
                    return e
                },
                get: function(t, n) {
                    return e ? e.get(t, n) : ""
                },
                inherit: function(e, t, i) {
                    return e = n(e), e && e.inherit(t, i), this
                },
                list: function() {
                    return Object.keys(t)
                }
            };
        Object.append(i, new Events), i.Set = new Class({
            sets: {},
            inherits: {
                locales: [],
                sets: {}
            },
            initialize: function(e) {
                this.name = e || ""
            },
            define: function(e, t, n) {
                var i = this.sets[e];
                return i || (i = {}), t && ("object" == typeOf(t) ? i = Object.merge(i, t) : i[t] = n), this.sets[e] = i, this
            },
            get: function(e, n, i) {
                var s = Object.getFromPath(this.sets, e);
                if (null != s) {
                    var r = typeOf(s);
                    return "function" == r ? s = s.apply(null, Array.from(n)) : "object" == r && (s = Object.clone(s)), s
                }
                var o = e.indexOf("."),
                    a = 0 > o ? e : e.substr(0, o),
                    l = (this.inherits.sets[a] || []).combine(this.inherits.locales).include("en-US");
                i || (i = []);
                for (var c = 0, u = l.length; u > c; c++)
                    if (!i.contains(l[c])) {
                        i.include(l[c]);
                        var d = t[l[c]];
                        if (d && (s = d.get(e, n, i), null != s)) return s
                    }
                return ""
            },
            inherit: function(e, t) {
                e = Array.from(e), t && !this.inherits.sets[t] && (this.inherits.sets[t] = []);
                for (var n = e.length; n--;)(t ? this.inherits.sets[t] : this.inherits.locales).unshift(e[n]);
                return this
            }
        })
    }(), Class.Mutators.Binds = function(e) {
        return this.prototype.initialize || this.implement("initialize", function() {}), Array.from(e).concat(this.prototype.Binds || [])
    }, Class.Mutators.initialize = function(e) {
        return function() {
            return Array.from(this.Binds).each(function(e) {
                var t = this[e];
                t && (this[e] = t.bind(this))
            }, this), e.apply(this, arguments)
        }
    }, Locale.define("en-US", "Date", {
        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        months_abbr: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        days_abbr: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        dateOrder: ["month", "date", "year"],
        shortDate: "%m/%d/%Y",
        shortTime: "%I:%M%p",
        AM: "AM",
        PM: "PM",
        firstDayOfWeek: 0,
        ordinal: function(e) {
            return e > 3 && 21 > e ? "th" : ["th", "st", "nd", "rd", "th"][Math.min(e % 10, 4)]
        },
        lessThanMinuteAgo: "less than a minute ago",
        minuteAgo: "about a minute ago",
        minutesAgo: "{delta} minutes ago",
        hourAgo: "about an hour ago",
        hoursAgo: "about {delta} hours ago",
        dayAgo: "1 day ago",
        daysAgo: "{delta} days ago",
        weekAgo: "1 week ago",
        weeksAgo: "{delta} weeks ago",
        monthAgo: "1 month ago",
        monthsAgo: "{delta} months ago",
        yearAgo: "1 year ago",
        yearsAgo: "{delta} years ago",
        lessThanMinuteUntil: "less than a minute from now",
        minuteUntil: "about a minute from now",
        minutesUntil: "{delta} minutes from now",
        hourUntil: "about an hour from now",
        hoursUntil: "about {delta} hours from now",
        dayUntil: "1 day from now",
        daysUntil: "{delta} days from now",
        weekUntil: "1 week from now",
        weeksUntil: "{delta} weeks from now",
        monthUntil: "1 month from now",
        monthsUntil: "{delta} months from now",
        yearUntil: "1 year from now",
        yearsUntil: "{delta} years from now"
    }),
    function() {
        var e = this.Date,
            t = e.Methods = {
                ms: "Milliseconds",
                year: "FullYear",
                min: "Minutes",
                mo: "Month",
                sec: "Seconds",
                hr: "Hours"
            };
        ["Date", "Day", "FullYear", "Hours", "Milliseconds", "Minutes", "Month", "Seconds", "Time", "TimezoneOffset", "Week", "Timezone", "GMTOffset", "DayOfYear", "LastMonth", "LastDayOfMonth", "UTCDate", "UTCDay", "UTCFullYear", "AMPM", "Ordinal", "UTCHours", "UTCMilliseconds", "UTCMinutes", "UTCMonth", "UTCSeconds", "UTCMilliseconds"].each(function(t) {
            e.Methods[t.toLowerCase()] = t
        });
        var n = function(e, t, i) {
            return 1 == t ? e : e < Math.pow(10, t - 1) ? (i || "0") + n(e, t - 1, i) : e
        };
        e.implement({
            set: function(e, n) {
                e = e.toLowerCase();
                var i = t[e] && "set" + t[e];
                return i && this[i] && this[i](n), this
            }.overloadSetter(),
            get: function(e) {
                e = e.toLowerCase();
                var n = t[e] && "get" + t[e];
                return n && this[n] ? this[n]() : null
            }.overloadGetter(),
            clone: function() {
                return new e(this.get("time"))
            },
            increment: function(t, n) {
                switch (t = t || "day", n = null != n ? n : 1, t) {
                    case "year":
                        return this.increment("month", 12 * n);
                    case "month":
                        var i = this.get("date");
                        return this.set("date", 1).set("mo", this.get("mo") + n), this.set("date", i.min(this.get("lastdayofmonth")));
                    case "week":
                        return this.increment("day", 7 * n);
                    case "day":
                        return this.set("date", this.get("date") + n)
                }
                if (!e.units[t]) throw new Error(t + " is not a supported interval");
                return this.set("time", this.get("time") + n * e.units[t]())
            },
            decrement: function(e, t) {
                return this.increment(e, -1 * (null != t ? t : 1))
            },
            isLeapYear: function() {
                return e.isLeapYear(this.get("year"))
            },
            clearTime: function() {
                return this.set({
                    hr: 0,
                    min: 0,
                    sec: 0,
                    ms: 0
                })
            },
            diff: function(t, n) {
                return "string" == typeOf(t) && (t = e.parse(t)), ((t - this) / e.units[n || "day"](3, 3)).round()
            },
            getLastDayOfMonth: function() {
                return e.daysInMonth(this.get("mo"), this.get("year"))
            },
            getDayOfYear: function() {
                return (e.UTC(this.get("year"), this.get("mo"), this.get("date") + 1) - e.UTC(this.get("year"), 0, 1)) / e.units.day()
            },
            setDay: function(t, n) {
                null == n && (n = e.getMsg("firstDayOfWeek"), "" === n && (n = 1)), t = (7 + e.parseDay(t, !0) - n) % 7;
                var i = (7 + this.get("day") - n) % 7;
                return this.increment("day", t - i)
            },
            getWeek: function(t) {
                null == t && (t = e.getMsg("firstDayOfWeek"), "" === t && (t = 1));
                var n, i = this,
                    s = (7 + i.get("day") - t) % 7,
                    r = 0;
                if (1 == t) {
                    var o = i.get("month"),
                        a = i.get("date") - s;
                    if (11 == o && a > 28) return 1;
                    0 == o && -2 > a && (i = new e(i).decrement("day", s), s = 0), n = new e(i.get("year"), 0, 1).get("day") || 7, n > 4 && (r = -7)
                } else n = new e(i.get("year"), 0, 1).get("day");
                return r += i.get("dayofyear"), r += 6 - s, r += (7 + n - t) % 7, r / 7
            },
            getOrdinal: function(t) {
                return e.getMsg("ordinal", t || this.get("date"))
            },
            getTimezone: function() {
                return this.toString().replace(/^.*? ([A-Z]{3}).[0-9]{4}.*$/, "$1").replace(/^.*?\(([A-Z])[a-z]+ ([A-Z])[a-z]+ ([A-Z])[a-z]+\)$/, "$1$2$3")
            },
            getGMTOffset: function() {
                var e = this.get("timezoneOffset");
                return (e > 0 ? "-" : "+") + n((e.abs() / 60).floor(), 2) + n(e % 60, 2)
            },
            setAMPM: function(e) {
                e = e.toUpperCase();
                var t = this.get("hr");
                return t > 11 && "AM" == e ? this.decrement("hour", 12) : 12 > t && "PM" == e ? this.increment("hour", 12) : this
            },
            getAMPM: function() {
                return this.get("hr") < 12 ? "AM" : "PM"
            },
            parse: function(t) {
                return this.set("time", e.parse(t)), this
            },
            isValid: function(e) {
                return e || (e = this), "date" == typeOf(e) && !isNaN(e.valueOf())
            },
            format: function(t) {
                if (!this.isValid()) return "invalid date";
                if (t || (t = "%x %X"), "string" == typeof t && (t = r[t.toLowerCase()] || t), "function" == typeof t) return t(this);
                var i = this;
                return t.replace(/%([a-z%])/gi, function(t, s) {
                    switch (s) {
                        case "a":
                            return e.getMsg("days_abbr")[i.get("day")];
                        case "A":
                            return e.getMsg("days")[i.get("day")];
                        case "b":
                            return e.getMsg("months_abbr")[i.get("month")];
                        case "B":
                            return e.getMsg("months")[i.get("month")];
                        case "c":
                            return i.format("%a %b %d %H:%M:%S %Y");
                        case "d":
                            return n(i.get("date"), 2);
                        case "e":
                            return n(i.get("date"), 2, " ");
                        case "H":
                            return n(i.get("hr"), 2);
                        case "I":
                            return n(i.get("hr") % 12 || 12, 2);
                        case "j":
                            return n(i.get("dayofyear"), 3);
                        case "k":
                            return n(i.get("hr"), 2, " ");
                        case "l":
                            return n(i.get("hr") % 12 || 12, 2, " ");
                        case "L":
                            return n(i.get("ms"), 3);
                        case "m":
                            return n(i.get("mo") + 1, 2);
                        case "M":
                            return n(i.get("min"), 2);
                        case "o":
                            return i.get("ordinal");
                        case "p":
                            return e.getMsg(i.get("ampm"));
                        case "s":
                            return Math.round(i / 1e3);
                        case "S":
                            return n(i.get("seconds"), 2);
                        case "T":
                            return i.format("%H:%M:%S");
                        case "U":
                            return n(i.get("week"), 2);
                        case "w":
                            return i.get("day");
                        case "x":
                            return i.format(e.getMsg("shortDate"));
                        case "X":
                            return i.format(e.getMsg("shortTime"));
                        case "y":
                            return i.get("year").toString().substr(2);
                        case "Y":
                            return i.get("year");
                        case "z":
                            return i.get("GMTOffset");
                        case "Z":
                            return i.get("Timezone")
                    }
                    return s
                })
            },
            toISOString: function() {
                return this.format("iso8601")
            }
        }).alias({
            toJSON: "toISOString",
            compare: "diff",
            strftime: "format"
        });
        var i = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            s = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            r = {
                db: "%Y-%m-%d %H:%M:%S",
                compact: "%Y%m%dT%H%M%S",
                "short": "%d %b %H:%M",
                "long": "%B %d, %Y %H:%M",
                rfc822: function(e) {
                    return i[e.get("day")] + e.format(", %d ") + s[e.get("month")] + e.format(" %Y %H:%M:%S %Z")
                },
                rfc2822: function(e) {
                    return i[e.get("day")] + e.format(", %d ") + s[e.get("month")] + e.format(" %Y %H:%M:%S %z")
                },
                iso8601: function(e) {
                    return e.getUTCFullYear() + "-" + n(e.getUTCMonth() + 1, 2) + "-" + n(e.getUTCDate(), 2) + "T" + n(e.getUTCHours(), 2) + ":" + n(e.getUTCMinutes(), 2) + ":" + n(e.getUTCSeconds(), 2) + "." + n(e.getUTCMilliseconds(), 3) + "Z"
                }
            },
            o = [],
            a = e.parse,
            l = function(t, n, i) {
                var s = -1,
                    r = e.getMsg(t + "s");
                switch (typeOf(n)) {
                    case "object":
                        s = r[n.get(t)];
                        break;
                    case "number":
                        if (s = r[n], !s) throw new Error("Invalid " + t + " index: " + n);
                        break;
                    case "string":
                        var o = r.filter(function(e) {
                            return this.test(e)
                        }, new RegExp("^" + n, "i"));
                        if (!o.length) throw new Error("Invalid " + t + " string");
                        if (o.length > 1) throw new Error("Ambiguous " + t);
                        s = o[0]
                }
                return i ? r.indexOf(s) : s
            },
            c = 1900,
            u = 70;
        e.extend({
            getMsg: function(e, t) {
                return Locale.get("Date." + e, t)
            },
            units: {
                ms: Function.from(1),
                second: Function.from(1e3),
                minute: Function.from(6e4),
                hour: Function.from(36e5),
                day: Function.from(864e5),
                week: Function.from(6084e5),
                month: function(t, n) {
                    var i = new e;
                    return 864e5 * e.daysInMonth(null != t ? t : i.get("mo"), null != n ? n : i.get("year"))
                },
                year: function(t) {
                    return t = t || (new e).get("year"), e.isLeapYear(t) ? 316224e5 : 31536e6
                }
            },
            daysInMonth: function(t, n) {
                return [31, e.isLeapYear(n) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][t]
            },
            isLeapYear: function(e) {
                return e % 4 === 0 && e % 100 !== 0 || e % 400 === 0
            },
            parse: function(t) {
                var n = typeOf(t);
                if ("number" == n) return new e(t);
                if ("string" != n) return t;
                if (t = t.clean(), !t.length) return null;
                var i;
                return o.some(function(e) {
                    var n = e.re.exec(t);
                    return n ? i = e.handler(n) : !1
                }), i && i.isValid() || (i = new e(a(t)), i && i.isValid() || (i = new e(t.toInt()))), i
            },
            parseDay: function(e, t) {
                return l("day", e, t)
            },
            parseMonth: function(e, t) {
                return l("month", e, t)
            },
            parseUTC: function(t) {
                var n = new e(t),
                    i = e.UTC(n.get("year"), n.get("mo"), n.get("date"), n.get("hr"), n.get("min"), n.get("sec"), n.get("ms"));
                return new e(i)
            },
            orderIndex: function(t) {
                return e.getMsg("dateOrder").indexOf(t) + 1
            },
            defineFormat: function(e, t) {
                return r[e] = t, this
            },
            defineParser: function(e) {
                return o.push(e.re && e.handler ? e : g(e)), this
            },
            defineParsers: function() {
                return Array.flatten(arguments).each(e.defineParser), this
            },
            define2DigitYearStart: function(e) {
                return u = e % 100, c = e - u, this
            }
        }).extend({
            defineFormats: e.defineFormat.overloadSetter()
        });
        var d = function(t) {
                return new RegExp("(?:" + e.getMsg(t).map(function(e) {
                    return e.substr(0, 3)
                }).join("|") + ")[a-z]*")
            },
            h = function(t) {
                switch (t) {
                    case "T":
                        return "%H:%M:%S";
                    case "x":
                        return (1 == e.orderIndex("month") ? "%m[-./]%d" : "%d[-./]%m") + "([-./]%y)?";
                    case "X":
                        return "%H([.:]%M)?([.:]%S([.:]%s)?)? ?%p? ?%z?"
                }
                return null
            },
            m = {
                d: /[0-2]?[0-9]|3[01]/,
                H: /[01]?[0-9]|2[0-3]/,
                I: /0?[1-9]|1[0-2]/,
                M: /[0-5]?\d/,
                s: /\d+/,
                o: /[a-z]*/,
                p: /[ap]\.?m\.?/,
                y: /\d{2}|\d{4}/,
                Y: /\d{4}/,
                z: /Z|[+-]\d{2}(?::?\d{2})?/
            };
        m.m = m.I, m.S = m.M;
        var f, p = function(e) {
                f = e, m.a = m.A = d("days"), m.b = m.B = d("months"), o.each(function(e, t) {
                    e.format && (o[t] = g(e.format))
                })
            },
            g = function(t) {
                if (!f) return {
                    format: t
                };
                var n = [],
                    i = (t.source || t).replace(/%([a-z])/gi, function(e, t) {
                        return h(t) || e
                    }).replace(/\((?!\?)/g, "(?:").replace(/ (?!\?|\*)/g, ",? ").replace(/%([a-z%])/gi, function(e, t) {
                        var i = m[t];
                        return i ? (n.push(t), "(" + i.source + ")") : t
                    }).replace(/\[a-z\]/gi, "[a-z\\u00c0-\\uffff;&]");
                return {
                    format: t,
                    re: new RegExp("^" + i + "$", "i"),
                    handler: function(t) {
                        t = t.slice(1).associate(n);
                        var i = (new e).clearTime(),
                            s = t.y || t.Y;
                        null != s && v.call(i, "y", s), "d" in t && v.call(i, "d", 1), ("m" in t || t.b || t.B) && v.call(i, "m", 1);
                        for (var r in t) v.call(i, r, t[r]);
                        return i
                    }
                }
            },
            v = function(t, n) {
                if (!n) return this;
                switch (t) {
                    case "a":
                    case "A":
                        return this.set("day", e.parseDay(n, !0));
                    case "b":
                    case "B":
                        return this.set("mo", e.parseMonth(n, !0));
                    case "d":
                        return this.set("date", n);
                    case "H":
                    case "I":
                        return this.set("hr", n);
                    case "m":
                        return this.set("mo", n - 1);
                    case "M":
                        return this.set("min", n);
                    case "p":
                        return this.set("ampm", n.replace(/\./g, ""));
                    case "S":
                        return this.set("sec", n);
                    case "s":
                        return this.set("ms", 1e3 * ("0." + n));
                    case "w":
                        return this.set("day", n);
                    case "Y":
                        return this.set("year", n);
                    case "y":
                        return n = +n, 100 > n && (n += c + (u > n ? 100 : 0)), this.set("year", n);
                    case "z":
                        "Z" == n && (n = "+00");
                        var i = n.match(/([+-])(\d{2}):?(\d{2})?/);
                        return i = (i[1] + "1") * (60 * i[2] + (+i[3] || 0)) + this.getTimezoneOffset(), this.set("time", this - 6e4 * i)
                }
                return this
            };
        e.defineParsers("%Y([-./]%m([-./]%d((T| )%X)?)?)?", "%Y%m%d(T%H(%M%S?)?)?", "%x( %X)?", "%d%o( %b( %Y)?)?( %X)?", "%b( %d%o)?( %Y)?( %X)?", "%Y %b( %d%o( %X)?)?", "%o %b %d %X %z %Y", "%T", "%H:%M( ?%p)?"), Locale.addEvent("change", function(e) {
            Locale.get("Date") && p(e)
        }).fireEvent("change", Locale.getCurrent())
    }(),
    function() {
        var e = {
                a: /[àáâãäåăą]/g,
                A: /[ÀÁÂÃÄÅĂĄ]/g,
                c: /[ćčç]/g,
                C: /[ĆČÇ]/g,
                d: /[ďđ]/g,
                D: /[ĎÐ]/g,
                e: /[èéêëěę]/g,
                E: /[ÈÉÊËĚĘ]/g,
                g: /[ğ]/g,
                G: /[Ğ]/g,
                i: /[ìíîï]/g,
                I: /[ÌÍÎÏ]/g,
                l: /[ĺľł]/g,
                L: /[ĹĽŁ]/g,
                n: /[ñňń]/g,
                N: /[ÑŇŃ]/g,
                o: /[òóôõöøő]/g,
                O: /[ÒÓÔÕÖØ]/g,
                r: /[řŕ]/g,
                R: /[ŘŔ]/g,
                s: /[ššş]/g,
                S: /[ŠŞŚ]/g,
                t: /[ťţ]/g,
                T: /[ŤŢ]/g,
                ue: /[ü]/g,
                UE: /[Ü]/g,
                u: /[ùúûůµ]/g,
                U: /[ÙÚÛŮ]/g,
                y: /[ÿý]/g,
                Y: /[ŸÝ]/g,
                z: /[žźż]/g,
                Z: /[ŽŹŻ]/g,
                th: /[þ]/g,
                TH: /[Þ]/g,
                dh: /[ð]/g,
                DH: /[Ð]/g,
                ss: /[ß]/g,
                oe: /[œ]/g,
                OE: /[Œ]/g,
                ae: /[æ]/g,
                AE: /[Æ]/g
            },
            t = {
                " ": /[\xa0\u2002\u2003\u2009]/g,
                "*": /[\xb7]/g,
                "'": /[\u2018\u2019]/g,
                '"': /[\u201c\u201d]/g,
                "...": /[\u2026]/g,
                "-": /[\u2013]/g,
                "&raquo;": /[\uFFFD]/g
            },
            n = function(e, t) {
                var n, i = e;
                for (n in t) i = i.replace(t[n], n);
                return i
            },
            i = function(e, t) {
                e = e || "";
                var n = t ? "<" + e + "(?!\\w)[^>]*>([\\s\\S]*?)</" + e + "(?!\\w)>" : "</?" + e + "([^>]+)?>",
                    i = new RegExp(n, "gi");
                return i
            };
        String.implement({
            standardize: function() {
                return n(this, e)
            },
            repeat: function(e) {
                return new Array(e + 1).join(this)
            },
            pad: function(e, t, n) {
                if (this.length >= e) return this;
                var i = (null == t ? " " : "" + t).repeat(e - this.length).substr(0, e - this.length);
                return n && "right" != n ? "left" == n ? i + this : i.substr(0, (i.length / 2).floor()) + this + i.substr(0, (i.length / 2).ceil()) : this + i
            },
            getTags: function(e, t) {
                return this.match(i(e, t)) || []
            },
            stripTags: function(e, t) {
                return this.replace(i(e, t), "")
            },
            tidy: function() {
                return n(this, t)
            },
            truncate: function(e, t, n) {
                var i = this;
                if (null == t && 1 == arguments.length && (t = "…"), i.length > e) {
                    if (i = i.substring(0, e), n) {
                        var s = i.lastIndexOf(n); - 1 != s && (i = i.substr(0, s))
                    }
                    t && (i += t)
                }
                return i
            }
        })
    }(), Element.implement({
        tidy: function() {
            this.set("value", this.get("value").tidy())
        },
        getTextInRange: function(e, t) {
            return this.get("value").substring(e, t)
        },
        getSelectedText: function() {
            return this.setSelectionRange ? this.getTextInRange(this.getSelectionStart(), this.getSelectionEnd()) : document.selection.createRange().text
        },
        getSelectedRange: function() {
            if (null != this.selectionStart) return {
                start: this.selectionStart,
                end: this.selectionEnd
            };
            var e = {
                    start: 0,
                    end: 0
                },
                t = this.getDocument().selection.createRange();
            if (!t || t.parentElement() != this) return e;
            var n = t.duplicate();
            if ("text" == this.type) e.start = 0 - n.moveStart("character", -1e5), e.end = e.start + t.text.length;
            else {
                var i = this.get("value"),
                    s = i.length;
                n.moveToElementText(this), n.setEndPoint("StartToEnd", t), n.text.length && (s -= i.match(/[\n\r]*$/)[0].length), e.end = s - n.text.length, n.setEndPoint("StartToStart", t), e.start = s - n.text.length
            }
            return e
        },
        getSelectionStart: function() {
            return this.getSelectedRange().start
        },
        getSelectionEnd: function() {
            return this.getSelectedRange().end
        },
        setCaretPosition: function(e) {
            return "end" == e && (e = this.get("value").length), this.selectRange(e, e), this
        },
        getCaretPosition: function() {
            return this.getSelectedRange().start
        },
        selectRange: function(e, t) {
            if (this.setSelectionRange) this.focus(), this.setSelectionRange(e, t);
            else {
                var n = this.get("value"),
                    i = n.substr(e, t - e).replace(/\r/g, "").length;
                e = n.substr(0, e).replace(/\r/g, "").length;
                var s = this.createTextRange();
                s.collapse(!0), s.moveEnd("character", e + i), s.moveStart("character", e), s.select()
            }
            return this
        },
        insertAtCursor: function(e, t) {
            var n = this.getSelectedRange(),
                i = this.get("value");
            return this.set("value", i.substring(0, n.start) + e + i.substring(n.end, i.length)), t !== !1 ? this.selectRange(n.start, n.start + e.length) : this.setCaretPosition(n.start + e.length), this
        },
        insertAroundCursor: function(e, t) {
            e = Object.append({
                before: "",
                defaultMiddle: "",
                after: ""
            }, e);
            var n = this.getSelectedText() || e.defaultMiddle,
                i = this.getSelectedRange(),
                s = this.get("value");
            if (i.start == i.end) this.set("value", s.substring(0, i.start) + e.before + n + e.after + s.substring(i.end, s.length)), this.selectRange(i.start + e.before.length, i.end + e.before.length + n.length);
            else {
                var r = s.substring(i.start, i.end);
                this.set("value", s.substring(0, i.start) + e.before + r + e.after + s.substring(i.end, s.length));
                var o = i.start + e.before.length;
                t !== !1 ? this.selectRange(o, o + r.length) : this.setCaretPosition(o + s.length)
            }
            return this
        }
    }), Locale.define("en-US", "FormValidator", {
        required: "This field is required.",
        length: "Please enter {length} characters (you entered {elLength} characters)",
        minLength: "Please enter at least {minLength} characters (you entered {length} characters).",
        maxLength: "Please enter no more than {maxLength} characters (you entered {length} characters).",
        integer: "Please enter an integer in this field. Numbers with decimals (e.g. 1.25) are not permitted.",
        numeric: 'Please enter only numeric values in this field (i.e. "1" or "1.1" or "-1" or "-1.1").',
        digits: "Please use numbers and punctuation only in this field (for example, a phone number with dashes or dots is permitted).",
        alpha: "Please use only letters (a-z) within this field. No spaces or other characters are allowed.",
        alphanum: "Please use only letters (a-z) or numbers (0-9) in this field. No spaces or other characters are allowed.",
        dateSuchAs: "Please enter a valid date such as {date}",
        dateInFormatMDY: 'Please enter a valid date such as MM/DD/YYYY (i.e. "12/31/1999")',
        email: 'Please enter a valid email address. For example "fred@domain.com".',
        url: "Please enter a valid URL such as http://www.example.com.",
        currencyDollar: "Please enter a valid $ amount. For example $100.00 .",
        oneRequired: "Please enter something for at least one of these inputs.",
        errorPrefix: "Error: ",
        warningPrefix: "Warning: ",
        noSpace: "There can be no spaces in this input.",
        reqChkByNode: "No items are selected.",
        requiredChk: "This field is required.",
        reqChkByName: "Please select a {label}.",
        match: "This field needs to match the {matchName} field",
        startDate: "the start date",
        endDate: "the end date",
        currendDate: "the current date",
        afterDate: "The date should be the same or after {label}.",
        beforeDate: "The date should be the same or before {label}.",
        startMonth: "Please select a start month",
        sameMonth: "These two dates must be in the same month - you must change one or the other.",
        creditcard: "The credit card number entered is invalid. Please check the number and try again. {length} digits entered."
    }), Element.implement({
        isDisplayed: function() {
            return "none" != this.getStyle("display")
        },
        isVisible: function() {
            var e = this.offsetWidth,
                t = this.offsetHeight;
            return 0 == e && 0 == t ? !1 : e > 0 && t > 0 ? !0 : "none" != this.style.display
        },
        toggle: function() {
            return this[this.isDisplayed() ? "hide" : "show"]()
        },
        hide: function() {
            var e;
            try {
                e = this.getStyle("display")
            } catch (t) {}
            return "none" == e ? this : this.store("element:_originalDisplay", e || "").setStyle("display", "none")
        },
        show: function(e) {
            return !e && this.isDisplayed() ? this : (e = e || this.retrieve("element:_originalDisplay") || "block", this.setStyle("display", "none" == e ? "block" : e))
        },
        swapClass: function(e, t) {
            return this.removeClass(e).addClass(t)
        }
    }), Document.implement({
        clearSelection: function() {
            if (window.getSelection) {
                var e = window.getSelection();
                e && e.removeAllRanges && e.removeAllRanges()
            } else if (document.selection && document.selection.empty) try {
                document.selection.empty()
            } catch (t) {}
        }
    }), window.Form || (window.Form = {});
var InputValidator = this.InputValidator = new Class({
    Implements: [Options],
    options: {
        errorMsg: "Validation failed.",
        test: Function.from(!0)
    },
    initialize: function(e, t) {
        this.setOptions(t), this.className = e
    },
    test: function(e, t) {
        return e = document.id(e), e ? this.options.test(e, t || this.getProps(e)) : !1
    },
    getError: function(e, t) {
        e = document.id(e);
        var n = this.options.errorMsg;
        return "function" == typeOf(n) && (n = n(e, t || this.getProps(e))), n
    },
    getProps: function(e) {
        return e = document.id(e), e ? e.get("validatorProps") : {}
    }
});
Element.Properties.validators = {
        get: function() {
            return (this.get("data-validators") || this.className).clean().split(" ")
        }
    }, Element.Properties.validatorProps = {
        set: function(e) {
            return this.eliminate("$moo:validatorProps").store("$moo:validatorProps", e)
        },
        get: function(e) {
            if (e && this.set(e), this.retrieve("$moo:validatorProps")) return this.retrieve("$moo:validatorProps");
            if (this.getProperty("data-validator-properties") || this.getProperty("validatorProps")) try {
                this.store("$moo:validatorProps", JSON.decode(this.getProperty("validatorProps") || this.getProperty("data-validator-properties")))
            } catch (t) {
                return {}
            } else {
                var n = this.get("validators").filter(function(e) {
                    return e.test(":")
                });
                n.length ? (e = {}, n.each(function(t) {
                    var n = t.split(":");
                    if (n[1]) try {
                        e[n[0]] = JSON.decode(n[1])
                    } catch (i) {}
                }), this.store("$moo:validatorProps", e)) : this.store("$moo:validatorProps", {})
            }
            return this.retrieve("$moo:validatorProps")
        }
    }, Form.Validator = new Class({
        Implements: [Options, Events],
        Binds: ["onSubmit"],
        options: {
            fieldSelectors: "input, select, textarea",
            ignoreHidden: !0,
            ignoreDisabled: !0,
            useTitles: !1,
            evaluateOnSubmit: !0,
            evaluateFieldsOnBlur: !0,
            evaluateFieldsOnChange: !0,
            serial: !0,
            stopOnFailure: !0,
            warningPrefix: function() {
                return Form.Validator.getMsg("warningPrefix") || "Warning: "
            },
            errorPrefix: function() {
                return Form.Validator.getMsg("errorPrefix") || "Error: "
            }
        },
        initialize: function(e, t) {
            this.setOptions(t), this.element = document.id(e), this.element.store("validator", this), this.warningPrefix = Function.from(this.options.warningPrefix)(), this.errorPrefix = Function.from(this.options.errorPrefix)(), this.options.evaluateOnSubmit && this.element.addEvent("submit", this.onSubmit), (this.options.evaluateFieldsOnBlur || this.options.evaluateFieldsOnChange) && this.watchFields(this.getFields())
        },
        toElement: function() {
            return this.element
        },
        getFields: function() {
            return this.fields = this.element.getElements(this.options.fieldSelectors)
        },
        watchFields: function(e) {
            e.each(function(e) {
                this.options.evaluateFieldsOnBlur && e.addEvent("blur", this.validationMonitor.pass([e, !1], this)), this.options.evaluateFieldsOnChange && e.addEvent("change", this.validationMonitor.pass([e, !0], this))
            }, this)
        },
        validationMonitor: function() {
            clearTimeout(this.timer), this.timer = this.validateField.delay(50, this, arguments)
        },
        onSubmit: function(e) {
            this.validate(e) && this.reset()
        },
        reset: function() {
            return this.getFields().each(this.resetField, this), this
        },
        validate: function(e) {
            var t = this.getFields().map(function(e) {
                return this.validateField(e, !0)
            }, this).every(function(e) {
                return e
            });
            return this.fireEvent("formValidate", [t, this.element, e]), this.options.stopOnFailure && !t && e && e.preventDefault(), t
        },
        validateField: function(e, t) {
            if (this.paused) return !0;
            e = document.id(e);
            var n, i, s = !e.hasClass("validation-failed");
            if (this.options.serial && !t && (n = this.element.getElement(".validation-failed"), i = this.element.getElement(".warning")), e && (!n || t || e.hasClass("validation-failed") || n && !this.options.serial)) {
                var r = e.get("validators"),
                    o = r.some(function(e) {
                        return this.getValidator(e)
                    }, this),
                    a = [];
                if (r.each(function(t) {
                        t && !this.test(t, e) && a.include(t)
                    }, this), s = 0 === a.length, o && !this.hasValidator(e, "warnOnly") && (s ? (e.addClass("validation-passed").removeClass("validation-failed"), this.fireEvent("elementPass", [e])) : (e.addClass("validation-failed").removeClass("validation-passed"), this.fireEvent("elementFail", [e, a]))), !i) {
                    r.some(function(e) {
                        return e.test("^warn") ? this.getValidator(e.replace(/^warn-/, "")) : null
                    }, this);
                    e.removeClass("warning");
                    r.map(function(t) {
                        return t.test("^warn") ? this.test(t.replace(/^warn-/, ""), e, !0) : null
                    }, this)
                }
            }
            return s
        },
        test: function(e, t, n) {
            if (t = document.id(t), this.options.ignoreHidden && !t.isVisible() || this.options.ignoreDisabled && t.get("disabled")) return !0;
            var i = this.getValidator(e);
            null != n && (n = !1), this.hasValidator(t, "warnOnly") && (n = !0);
            var s = this.hasValidator(t, "ignoreValidation") || (i ? i.test(t) : !0);
            return i && t.isVisible() && this.fireEvent("elementValidate", [s, t, e, n]), n ? !0 : s
        },
        hasValidator: function(e, t) {
            return e.get("validators").contains(t)
        },
        resetField: function(e) {
            return e = document.id(e), e && e.get("validators").each(function(t) {
                t.test("^warn-") && (t = t.replace(/^warn-/, "")), e.removeClass("validation-failed"), e.removeClass("warning"), e.removeClass("validation-passed")
            }, this), this
        },
        stop: function() {
            return this.paused = !0, this
        },
        start: function() {
            return this.paused = !1, this
        },
        ignoreField: function(e, t) {
            return e = document.id(e), e && (this.enforceField(e), t ? e.addClass("warnOnly") : e.addClass("ignoreValidation")), this
        },
        enforceField: function(e) {
            return e = document.id(e), e && e.removeClass("warnOnly").removeClass("ignoreValidation"), this
        }
    }), Form.Validator.getMsg = function(e) {
        return Locale.get("FormValidator." + e)
    }, Form.Validator.adders = {
        validators: {},
        add: function(e, t) {
            this.validators[e] = new InputValidator(e, t), this.initialize || this.implement({
                validators: this.validators
            })
        },
        addAllThese: function(e) {
            Array.from(e).each(function(e) {
                this.add(e[0], e[1])
            }, this)
        },
        getValidator: function(e) {
            return this.validators[e.split(":")[0]]
        }
    }, Object.append(Form.Validator, Form.Validator.adders), Form.Validator.implement(Form.Validator.adders), Form.Validator.add("IsEmpty", {
        errorMsg: !1,
        test: function(e) {
            return "select-one" == e.type || "select" == e.type ? !(e.selectedIndex >= 0 && "" != e.options[e.selectedIndex].value) : null == e.get("value") || 0 == e.get("value").length
        }
    }), Form.Validator.addAllThese([
        ["required", {
            errorMsg: function() {
                return Form.Validator.getMsg("required")
            },
            test: function(e) {
                return !Form.Validator.getValidator("IsEmpty").test(e)
            }
        }],
        ["length", {
            errorMsg: function(e, t) {
                return "null" != typeOf(t.length) ? Form.Validator.getMsg("length").substitute({
                    length: t.length,
                    elLength: e.get("value").length
                }) : ""
            },
            test: function(e, t) {
                return "null" != typeOf(t.length) ? e.get("value").length == t.length || 0 == e.get("value").length : !0
            }
        }],
        ["minLength", {
            errorMsg: function(e, t) {
                return "null" != typeOf(t.minLength) ? Form.Validator.getMsg("minLength").substitute({
                    minLength: t.minLength,
                    length: e.get("value").length
                }) : ""
            },
            test: function(e, t) {
                return "null" != typeOf(t.minLength) ? e.get("value").length >= (t.minLength || 0) : !0
            }
        }],
        ["maxLength", {
            errorMsg: function(e, t) {
                return "null" != typeOf(t.maxLength) ? Form.Validator.getMsg("maxLength").substitute({
                    maxLength: t.maxLength,
                    length: e.get("value").length
                }) : ""
            },
            test: function(e, t) {
                return e.get("value").length <= (t.maxLength || 1e4)
            }
        }],
        ["validate-integer", {
            errorMsg: Form.Validator.getMsg.pass("integer"),
            test: function(e) {
                return Form.Validator.getValidator("IsEmpty").test(e) || /^(-?[1-9]\d*|0)$/.test(e.get("value"))
            }
        }],
        ["validate-numeric", {
            errorMsg: Form.Validator.getMsg.pass("numeric"),
            test: function(e) {
                return Form.Validator.getValidator("IsEmpty").test(e) || /^-?(?:0$0(?=\d*\.)|[1-9]|0)\d*(\.\d+)?$/.test(e.get("value"))
            }
        }],
        ["validate-digits", {
            errorMsg: Form.Validator.getMsg.pass("digits"),
            test: function(e) {
                return Form.Validator.getValidator("IsEmpty").test(e) || /^[\d() .:\-\+#]+$/.test(e.get("value"))
            }
        }],
        ["validate-alpha", {
            errorMsg: Form.Validator.getMsg.pass("alpha"),
            test: function(e) {
                return Form.Validator.getValidator("IsEmpty").test(e) || /^[a-zA-Z]+$/.test(e.get("value"))
            }
        }],
        ["validate-alphanum", {
            errorMsg: Form.Validator.getMsg.pass("alphanum"),
            test: function(e) {
                return Form.Validator.getValidator("IsEmpty").test(e) || !/\W/.test(e.get("value"))
            }
        }],
        ["validate-date", {
            errorMsg: function(e, t) {
                if (Date.parse) {
                    var n = t.dateFormat || "%x";
                    return Form.Validator.getMsg("dateSuchAs").substitute({
                        date: (new Date).format(n)
                    })
                }
                return Form.Validator.getMsg("dateInFormatMDY")
            },
            test: function(e, t) {
                if (Form.Validator.getValidator("IsEmpty").test(e)) return !0;
                var n = Locale.getCurrent().sets.Date,
                    i = new RegExp([n.days, n.days_abbr, n.months, n.months_abbr].flatten().join("|"), "i"),
                    s = e.get("value"),
                    r = s.match(/[a-z]+/gi);
                if (r && !r.every(i.exec, i)) return !1;
                var o = Date.parse(s),
                    a = t.dateFormat || "%x",
                    l = o.format(a);
                return "invalid date" != l && e.set("value", l), o.isValid()
            }
        }],
        ["validate-email", {
            errorMsg: Form.Validator.getMsg.pass("email"),
            test: function(e) {
                return Form.Validator.getValidator("IsEmpty").test(e) || /^(?:[a-z0-9!#$%&'*+\/=?^_`{|}~-]\.?){0,63}[a-z0-9!#$%&'*+\/=?^_`{|}~-]@(?:(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)*[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\])$/i.test(e.get("value"))
            }
        }],
        ["validate-url", {
            errorMsg: Form.Validator.getMsg.pass("url"),
            test: function(e) {
                return Form.Validator.getValidator("IsEmpty").test(e) || /^(https?|ftp|rmtp|mms):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i.test(e.get("value"))
            }
        }],
        ["validate-currency-dollar", {
            errorMsg: Form.Validator.getMsg.pass("currencyDollar"),
            test: function(e) {
                return Form.Validator.getValidator("IsEmpty").test(e) || /^\$?\-?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}\d*(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$/.test(e.get("value"))
            }
        }],
        ["validate-one-required", {
            errorMsg: Form.Validator.getMsg.pass("oneRequired"),
            test: function(e, t) {
                var n = document.id(t["validate-one-required"]) || e.getParent(t["validate-one-required"]);
                return n.getElements("input").some(function(e) {
                    return ["checkbox", "radio"].contains(e.get("type")) ? e.get("checked") : e.get("value")
                })
            }
        }]
    ]), Element.Properties.validator = {
        set: function(e) {
            this.get("validator").setOptions(e)
        },
        get: function() {
            var e = this.retrieve("validator");
            return e || (e = new Form.Validator(this), this.store("validator", e)), e
        }
    }, Element.implement({
        validate: function(e) {
            return e && this.set("validator", e), this.get("validator").validate()
        }
    }), Form.Validator.Inline = new Class({
        Extends: Form.Validator,
        options: {
            showError: function(e) {
                e.reveal ? e.reveal() : e.setStyle("display", "block")
            },
            hideError: function(e) {
                e.dissolve ? e.dissolve() : e.setStyle("display", "none")
            },
            scrollToErrorsOnSubmit: !0,
            scrollToErrorsOnBlur: !1,
            scrollToErrorsOnChange: !1,
            scrollFxOptions: {
                transition: "quad:out",
                offset: {
                    y: -20
                }
            }
        },
        initialize: function(e, t) {
            this.parent(e, t), this.addEvent("onElementValidate", function(e, t, n, i) {
                var s = this.getValidator(n);
                if (!e && s.getError(t)) {
                    i && t.addClass("warning");
                    var r = this.makeAdvice(n, t, s.getError(t), i);
                    this.insertAdvice(r, t), this.showAdvice(n, t)
                } else this.hideAdvice(n, t)
            })
        },
        makeAdvice: function(e, t, n, i) {
            var s = i ? this.warningPrefix : this.errorPrefix;
            s += this.options.useTitles ? t.title || n : n;
            var r = i ? "warning-advice" : "validation-advice",
                o = this.getAdvice(e, t);
            return o = o ? o.set("html", s) : new Element("div", {
                html: s,
                styles: {
                    display: "none"
                },
                id: "advice-" + e.split(":")[0] + "-" + this.getFieldId(t)
            }).addClass(r), t.store("$moo:advice-" + e, o), o
        },
        getFieldId: function(e) {
            return e.id ? e.id : e.id = "input_" + e.name
        },
        showAdvice: function(e, t) {
            var n = this.getAdvice(e, t);
            !n || t.retrieve("$moo:" + this.getPropName(e)) || "none" != n.getStyle("display") && "hidden" != n.getStyle("visiblity") && 0 != n.getStyle("opacity") || (t.store("$moo:" + this.getPropName(e), !0), this.options.showError(n), this.fireEvent("showAdvice", [t, n, e]))
        },
        hideAdvice: function(e, t) {
            var n = this.getAdvice(e, t);
            n && t.retrieve("$moo:" + this.getPropName(e)) && (t.store("$moo:" + this.getPropName(e), !1), this.options.hideError(n), this.fireEvent("hideAdvice", [t, n, e]))
        },
        getPropName: function(e) {
            return "advice" + e
        },
        resetField: function(e) {
            return (e = document.id(e)) ? (this.parent(e), e.get("validators").each(function(t) {
                this.hideAdvice(t, e)
            }, this), this) : this
        },
        getAllAdviceMessages: function(e, t) {
            var n = [];
            if (e.hasClass("ignoreValidation") && !t) return n;
            e.get("validators").some(function(t) {
                var i = t.test("^warn-") || e.hasClass("warnOnly");
                i && (t = t.replace(/^warn-/, ""));
                var s = this.getValidator(t);
                s && n.push({
                    message: s.getError(e),
                    warnOnly: i,
                    passed: s.test(),
                    validator: s
                })
            }, this);
            return n
        },
        getAdvice: function(e, t) {
            return t.retrieve("$moo:advice-" + e)
        },
        insertAdvice: function(e, t) {
            var n = t.get("validatorProps");
            n.msgPos && document.id(n.msgPos) ? document.id(n.msgPos).grab(e) : t.type && "radio" == t.type.toLowerCase() ? t.getParent().adopt(e) : e.inject(document.id(t), "after")
        },
        validateField: function(e, t, n) {
            var i = this.parent(e, t);
            if ((this.options.scrollToErrorsOnSubmit && null == n || n) && !i) {
                for (var s = document.id(this).getElement(".validation-failed"), r = document.id(this).getParent(); r != document.body && r.getScrollSize().y == r.getSize().y;) r = r.getParent();
                var o = r.retrieve("$moo:fvScroller");
                !o && window.Fx && Fx.Scroll && (o = new Fx.Scroll(r, this.options.scrollFxOptions), r.store("$moo:fvScroller", o)), s && (o ? o.toElement(s) : r.scrollTo(r.getScroll().x, s.getPosition(r).y - 20))
            }
            return i
        },
        watchFields: function(e) {
            e.each(function(e) {
                this.options.evaluateFieldsOnBlur && e.addEvent("blur", this.validationMonitor.pass([e, !1, this.options.scrollToErrorsOnBlur], this)), this.options.evaluateFieldsOnChange && e.addEvent("change", this.validationMonitor.pass([e, !0, this.options.scrollToErrorsOnChange], this))
            }, this)
        }
    }), Form.Validator.addAllThese([
        ["validate-enforce-oncheck", {
            test: function(e, t) {
                var n = e.getParent("form").retrieve("validator");
                return n ? ((t.toEnforce || document.id(t.enforceChildrenOf).getElements("input, select, textarea")).map(function(t) {
                    e.checked ? n.enforceField(t) : (n.ignoreField(t), n.resetField(t))
                }), !0) : !0
            }
        }],
        ["validate-ignore-oncheck", {
            test: function(e, t) {
                var n = e.getParent("form").retrieve("validator");
                return n ? ((t.toIgnore || document.id(t.ignoreChildrenOf).getElements("input, select, textarea")).each(function(t) {
                    e.checked ? (n.ignoreField(t), n.resetField(t)) : n.enforceField(t)
                }), !0) : !0
            }
        }],
        ["validate-nospace", {
            errorMsg: function() {
                return Form.Validator.getMsg("noSpace")
            },
            test: function(e, t) {
                return !e.get("value").test(/\s/)
            }
        }],
        ["validate-toggle-oncheck", {
            test: function(e, t) {
                var n = e.getParent("form").retrieve("validator");
                if (!n) return !0;
                var i = t.toToggle || document.id(t.toToggleChildrenOf).getElements("input, select, textarea");
                return e.checked ? i.each(function(e) {
                    n.enforceField(e)
                }) : i.each(function(e) {
                    n.ignoreField(e), n.resetField(e)
                }), !0
            }
        }],
        ["validate-reqchk-bynode", {
            errorMsg: function() {
                return Form.Validator.getMsg("reqChkByNode")
            },
            test: function(e, t) {
                return document.id(t.nodeId).getElements(t.selector || "input[type=checkbox], input[type=radio]").some(function(e) {
                    return e.checked
                })
            }
        }],
        ["validate-required-check", {
            errorMsg: function(e, t) {
                return t.useTitle ? e.get("title") : Form.Validator.getMsg("requiredChk")
            },
            test: function(e, t) {
                return !!e.checked
            }
        }],
        ["validate-reqchk-byname", {
            errorMsg: function(e, t) {
                return Form.Validator.getMsg("reqChkByName").substitute({
                    label: t.label || e.get("type")
                })
            },
            test: function(e, t) {
                var n = t.groupName || e.get("name"),
                    i = $$(document.getElementsByName(n)).some(function(e, t) {
                        return e.checked
                    }),
                    s = e.getParent("form").retrieve("validator");
                return i && s && s.resetField(e), i
            }
        }],
        ["validate-match", {
            errorMsg: function(e, t) {
                return Form.Validator.getMsg("match").substitute({
                    matchName: t.matchName || document.id(t.matchInput).get("name")
                })
            },
            test: function(e, t) {
                var n = e.get("value"),
                    i = document.id(t.matchInput) && document.id(t.matchInput).get("value");
                return n && i ? n == i : !0
            }
        }],
        ["validate-after-date", {
            errorMsg: function(e, t) {
                return Form.Validator.getMsg("afterDate").substitute({
                    label: t.afterLabel || (t.afterElement ? Form.Validator.getMsg("startDate") : Form.Validator.getMsg("currentDate"))
                })
            },
            test: function(e, t) {
                var n = document.id(t.afterElement) ? Date.parse(document.id(t.afterElement).get("value")) : new Date,
                    i = Date.parse(e.get("value"));
                return i && n ? i >= n : !0
            }
        }],
        ["validate-before-date", {
            errorMsg: function(e, t) {
                return Form.Validator.getMsg("beforeDate").substitute({
                    label: t.beforeLabel || (t.beforeElement ? Form.Validator.getMsg("endDate") : Form.Validator.getMsg("currentDate"))
                })
            },
            test: function(e, t) {
                var n = Date.parse(e.get("value")),
                    i = document.id(t.beforeElement) ? Date.parse(document.id(t.beforeElement).get("value")) : new Date;
                return i && n ? i >= n : !0
            }
        }],
        ["validate-custom-required", {
            errorMsg: function() {
                return Form.Validator.getMsg("required")
            },
            test: function(e, t) {
                return e.get("value") != t.emptyValue
            }
        }],
        ["validate-same-month", {
            errorMsg: function(e, t) {
                var n = document.id(t.sameMonthAs) && document.id(t.sameMonthAs).get("value"),
                    i = e.get("value");
                return "" != i ? Form.Validator.getMsg(n ? "sameMonth" : "startMonth") : void 0
            },
            test: function(e, t) {
                var n = Date.parse(e.get("value")),
                    i = Date.parse(document.id(t.sameMonthAs) && document.id(t.sameMonthAs).get("value"));
                return n && i ? n.format("%B") == i.format("%B") : !0
            }
        }],
        ["validate-cc-num", {
            errorMsg: function(e) {
                var t = e.get("value").replace(/[^0-9]/g, "");
                return Form.Validator.getMsg("creditcard").substitute({
                    length: t.length
                })
            },
            test: function(e) {
                if (Form.Validator.getValidator("IsEmpty").test(e)) return !0;
                var t = e.get("value");
                t = t.replace(/[^0-9]/g, "");
                var n = !1;
                if (t.test(/^4[0-9]{12}([0-9]{3})?$/) ? n = "Visa" : t.test(/^5[1-5]([0-9]{14})$/) ? n = "Master Card" : t.test(/^3[47][0-9]{13}$/) ? n = "American Express" : t.test(/^6011[0-9]{12}$/) && (n = "Discover"), n) {
                    for (var i = 0, s = 0, r = t.length - 1; r >= 0; --r) s = t.charAt(r).toInt(), 0 != s && ((t.length - r) % 2 == 0 && (s += s), s > 9 && (s = s.toString().charAt(0).toInt() + s.toString().charAt(1).toInt()), i += s);
                    if (i % 10 == 0) return !0
                }
                for (var o = "";
                    "" != t;) o += " " + t.substr(0, 4), t = t.substr(4);
                return e.getParent("form").retrieve("validator").ignoreField(e), e.set("value", o.clean()), e.getParent("form").retrieve("validator").enforceField(e), !1
            }
        }]
    ]),
    function() {
        function e(e) {
            return /^(?:body|html)$/i.test(e.tagName)
        }
        Fx.Scroll = new Class({
            Extends: Fx,
            options: {
                offset: {
                    x: 0,
                    y: 0
                },
                wheelStops: !0
            },
            initialize: function(e, t) {
                if (this.element = this.subject = document.id(e), this.parent(t), "element" != typeOf(this.element) && (this.element = document.id(this.element.getDocument().body)), this.options.wheelStops) {
                    var n = this.element,
                        i = this.cancel.pass(!1, this);
                    this.addEvent("start", function() {
                        n.addEvent("mousewheel", i)
                    }, !0), this.addEvent("complete", function() {
                        n.removeEvent("mousewheel", i)
                    }, !0)
                }
            },
            set: function() {
                var e = Array.flatten(arguments);
                return Browser.firefox && (e = [Math.round(e[0]), Math.round(e[1])]), this.element.scrollTo(e[0], e[1]), this
            },
            compute: function(e, t, n) {
                return [0, 1].map(function(i) {
                    return Fx.compute(e[i], t[i], n)
                })
            },
            start: function(e, t) {
                if (!this.check(e, t)) return this;
                var n = this.element.getScroll();
                return this.parent([n.x, n.y], [e, t])
            },
            calculateScroll: function(e, t) {
                var n = this.element,
                    i = n.getScrollSize(),
                    s = n.getScroll(),
                    r = n.getSize(),
                    o = this.options.offset,
                    a = {
                        x: e,
                        y: t
                    };
                for (var l in a) a[l] || 0 === a[l] || (a[l] = s[l]), "number" != typeOf(a[l]) && (a[l] = i[l] - r[l]), a[l] += o[l];
                return [a.x, a.y]
            },
            toTop: function() {
                return this.start.apply(this, this.calculateScroll(!1, 0))
            },
            toLeft: function() {
                return this.start.apply(this, this.calculateScroll(0, !1))
            },
            toRight: function() {
                return this.start.apply(this, this.calculateScroll("right", !1))
            },
            toBottom: function() {
                return this.start.apply(this, this.calculateScroll(!1, "bottom"))
            },
            toElement: function(t, n) {
                n = n ? Array.from(n) : ["x", "y"];
                var i = e(this.element) ? {
                        x: 0,
                        y: 0
                    } : this.element.getScroll(),
                    s = Object.map(document.id(t).getPosition(this.element), function(e, t) {
                        return n.contains(t) ? e + i[t] : !1
                    });
                return this.start.apply(this, this.calculateScroll(s.x, s.y))
            },
            toElementEdge: function(e, t, n) {
                t = t ? Array.from(t) : ["x", "y"], e = document.id(e);
                var i = {},
                    s = e.getPosition(this.element),
                    r = e.getSize(),
                    o = this.element.getScroll(),
                    a = this.element.getSize(),
                    l = {
                        x: s.x + r.x,
                        y: s.y + r.y
                    };
                return ["x", "y"].each(function(e) {
                    t.contains(e) && (l[e] > o[e] + a[e] && (i[e] = l[e] - a[e]), s[e] < o[e] && (i[e] = s[e])), null == i[e] && (i[e] = o[e]), n && n[e] && (i[e] = i[e] + n[e])
                }, this), (i.x != o.x || i.y != o.y) && this.start(i.x, i.y), this
            },
            toElementCenter: function(e, t, n) {
                t = t ? Array.from(t) : ["x", "y"], e = document.id(e);
                var i = {},
                    s = e.getPosition(this.element),
                    r = e.getSize(),
                    o = this.element.getScroll(),
                    a = this.element.getSize();
                return ["x", "y"].each(function(e) {
                    t.contains(e) && (i[e] = s[e] - (a[e] - r[e]) / 2), null == i[e] && (i[e] = o[e]), n && n[e] && (i[e] = i[e] + n[e])
                }, this), (i.x != o.x || i.y != o.y) && this.start(i.x, i.y), this
            }
        })
    }();
var Drag = new Class({
    Implements: [Events, Options],
    options: {
        snap: 6,
        unit: "px",
        grid: !1,
        style: !0,
        limit: !1,
        handle: !1,
        invert: !1,
        preventDefault: !1,
        stopPropagation: !1,
        modifiers: {
            x: "left",
            y: "top"
        }
    },
    initialize: function() {
        var e = Array.link(arguments, {
            options: Type.isObject,
            element: function(e) {
                return null != e
            }
        });
        this.element = document.id(e.element), this.document = this.element.getDocument(), this.setOptions(e.options || {});
        var t = typeOf(this.options.handle);
        this.handles = ("array" == t || "collection" == t ? $$(this.options.handle) : document.id(this.options.handle)) || this.element, this.mouse = {
            now: {},
            pos: {}
        }, this.value = {
            start: {},
            now: {}
        }, this.selection = Browser.ie ? "selectstart" : "mousedown", Browser.ie && !Drag.ondragstartFixed && (document.ondragstart = Function.from(!1), Drag.ondragstartFixed = !0), this.bound = {
            start: this.start.bind(this),
            check: this.check.bind(this),
            drag: this.drag.bind(this),
            stop: this.stop.bind(this),
            cancel: this.cancel.bind(this),
            eventStop: Function.from(!1)
        }, this.attach()
    },
    attach: function() {
        return this.handles.addEvent("mousedown", this.bound.start), this
    },
    detach: function() {
        return this.handles.removeEvent("mousedown", this.bound.start), this
    },
    start: function(e) {
        var t = this.options;
        if (!e.rightClick) {
            t.preventDefault && e.preventDefault(), t.stopPropagation && e.stopPropagation(), this.mouse.start = e.page, this.fireEvent("beforeStart", this.element);
            var n = t.limit;
            this.limit = {
                x: [],
                y: []
            };
            var i, s;
            for (i in t.modifiers)
                if (t.modifiers[i]) {
                    var r = this.element.getStyle(t.modifiers[i]);
                    if (r && !r.match(/px$/) && (s || (s = this.element.getCoordinates(this.element.getOffsetParent())), r = s[t.modifiers[i]]), t.style ? this.value.now[i] = (r || 0).toInt() : this.value.now[i] = this.element[t.modifiers[i]], t.invert && (this.value.now[i] *= -1), this.mouse.pos[i] = e.page[i] - this.value.now[i], n && n[i])
                        for (var o = 2; o--;) {
                            var a = n[i][o];
                            (a || 0 === a) && (this.limit[i][o] = "function" == typeof a ? a() : a)
                        }
                }
                "number" == typeOf(this.options.grid) && (this.options.grid = {
                x: this.options.grid,
                y: this.options.grid
            });
            var l = {
                mousemove: this.bound.check,
                mouseup: this.bound.cancel
            };
            l[this.selection] = this.bound.eventStop, this.document.addEvents(l)
        }
    },
    check: function(e) {
        this.options.preventDefault && e.preventDefault();
        var t = Math.round(Math.sqrt(Math.pow(e.page.x - this.mouse.start.x, 2) + Math.pow(e.page.y - this.mouse.start.y, 2)));
        t > this.options.snap && (this.cancel(), this.document.addEvents({
            mousemove: this.bound.drag,
            mouseup: this.bound.stop
        }), this.fireEvent("start", [this.element, e]).fireEvent("snap", this.element))
    },
    drag: function(e) {
        var t = this.options;
        t.preventDefault && e.preventDefault(), this.mouse.now = e.page;
        for (var n in t.modifiers) t.modifiers[n] && (this.value.now[n] = this.mouse.now[n] - this.mouse.pos[n], t.invert && (this.value.now[n] *= -1), t.limit && this.limit[n] && ((this.limit[n][1] || 0 === this.limit[n][1]) && this.value.now[n] > this.limit[n][1] ? this.value.now[n] = this.limit[n][1] : (this.limit[n][0] || 0 === this.limit[n][0]) && this.value.now[n] < this.limit[n][0] && (this.value.now[n] = this.limit[n][0])), t.grid[n] && (this.value.now[n] -= (this.value.now[n] - (this.limit[n][0] || 0)) % t.grid[n]), t.style ? this.element.setStyle(t.modifiers[n], this.value.now[n] + t.unit) : this.element[t.modifiers[n]] = this.value.now[n]);
        this.fireEvent("drag", [this.element, e])
    },
    cancel: function(e) {
        this.document.removeEvents({
            mousemove: this.bound.check,
            mouseup: this.bound.cancel
        }), e && (this.document.removeEvent(this.selection, this.bound.eventStop), this.fireEvent("cancel", this.element))
    },
    stop: function(e) {
        var t = {
            mousemove: this.bound.drag,
            mouseup: this.bound.stop
        };
        t[this.selection] = this.bound.eventStop, this.document.removeEvents(t), e && this.fireEvent("complete", [this.element, e])
    }
});
Element.implement({
    makeResizable: function(e) {
        var t = new Drag(this, Object.merge({
            modifiers: {
                x: "width",
                y: "height"
            }
        }, e));
        return this.store("resizer", t), t.addEvent("drag", function() {
            this.fireEvent("resize", t)
        }.bind(this))
    }
});
var Slider = new Class({
    Implements: [Events, Options],
    Binds: ["clickedElement", "draggedKnob", "scrolledElement"],
    options: {
        onTick: function(e) {
            this.setKnobPosition(e)
        },
        initialStep: 0,
        snap: !1,
        offset: 0,
        range: !1,
        wheel: !1,
        steps: 100,
        mode: "horizontal"
    },
    initialize: function(e, t, n) {
        this.setOptions(n), n = this.options, this.element = document.id(e), t = this.knob = document.id(t), this.previousChange = this.previousEnd = this.step = -1;
        var i = {},
            s = {
                x: !1,
                y: !1
            };
        switch (n.mode) {
            case "vertical":
                this.axis = "y", this.property = "top", this.offset = "offsetHeight";
                break;
            case "horizontal":
                this.axis = "x", this.property = "left", this.offset = "offsetWidth"
        }
        this.setSliderDimensions(), this.setRange(n.range), "static" == t.getStyle("position") && t.setStyle("position", "relative"), t.setStyle(this.property, -n.offset), s[this.axis] = this.property, i[this.axis] = [-n.offset, this.full - n.offset];
        var r = {
            snap: 0,
            limit: i,
            modifiers: s,
            onDrag: this.draggedKnob,
            onStart: this.draggedKnob,
            onBeforeStart: function() {
                this.isDragging = !0
            }.bind(this),
            onCancel: function() {
                this.isDragging = !1
            }.bind(this),
            onComplete: function() {
                this.isDragging = !1, this.draggedKnob(), this.end()
            }.bind(this)
        };
        n.snap && this.setSnap(r), this.drag = new Drag(t, r), this.attach(), null != n.initialStep && this.set(n.initialStep)
    },
    attach: function() {
        return this.element.addEvent("mousedown", this.clickedElement), this.options.wheel && this.element.addEvent("mousewheel", this.scrolledElement), this.drag.attach(), this
    },
    detach: function() {
        return this.element.removeEvent("mousedown", this.clickedElement).removeEvent("mousewheel", this.scrolledElement), this.drag.detach(), this
    },
    autosize: function() {
        return this.setSliderDimensions().setKnobPosition(this.toPosition(this.step)), this.drag.options.limit[this.axis] = [-this.options.offset, this.full - this.options.offset], this.options.snap && this.setSnap(), this
    },
    setSnap: function(e) {
        return e || (e = this.drag.options), e.grid = Math.ceil(this.stepWidth), e.limit[this.axis][1] = this.full, this
    },
    setKnobPosition: function(e) {
        return this.options.snap && (e = this.toPosition(this.step)), this.knob.setStyle(this.property, e), this
    },
    setSliderDimensions: function() {
        return this.full = this.element.measure(function() {
            return this.half = this.knob[this.offset] / 2, this.element[this.offset] - this.knob[this.offset] + 2 * this.options.offset
        }.bind(this)), this
    },
    set: function(e) {
        return this.range > 0 ^ e < this.min || (e = this.min), this.range > 0 ^ e > this.max || (e = this.max), this.step = Math.round(e), this.checkStep().fireEvent("tick", this.toPosition(this.step)).end()
    },
    setRange: function(e, t) {
        return this.min = Array.pick([e[0], 0]), this.max = Array.pick([e[1], this.options.steps]), this.range = this.max - this.min, this.steps = this.options.steps || this.full, this.stepSize = Math.abs(this.range) / this.steps, this.stepWidth = this.stepSize * this.full / Math.abs(this.range), e && this.set(Array.pick([t, this.step]).floor(this.min).max(this.max)), this
    },
    clickedElement: function(e) {
        if (!this.isDragging && e.target != this.knob) {
            var t = this.range < 0 ? -1 : 1,
                n = e.page[this.axis] - this.element.getPosition()[this.axis] - this.half;
            n = n.limit(-this.options.offset, this.full - this.options.offset), this.step = Math.round(this.min + t * this.toStep(n)), this.checkStep().fireEvent("tick", n).end()
        }
    },
    scrolledElement: function(e) {
        var t = "horizontal" == this.options.mode ? e.wheel < 0 : e.wheel > 0;
        this.set(this.step + (t ? -1 : 1) * this.stepSize), e.stop()
    },
    draggedKnob: function() {
        var e = this.range < 0 ? -1 : 1,
            t = this.drag.value.now[this.axis];
        t = t.limit(-this.options.offset, this.full - this.options.offset), this.step = Math.round(this.min + e * this.toStep(t)), this.checkStep()
    },
    checkStep: function() {
        var e = this.step;
        return this.previousChange != e && (this.previousChange = e, this.fireEvent("change", e)), this
    },
    end: function() {
        var e = this.step;
        return this.previousEnd !== e && (this.previousEnd = e, this.fireEvent("complete", e + "")), this
    },
    toStep: function(e) {
        var t = (e + this.options.offset) * this.stepSize / this.full * this.steps;
        return this.options.steps ? Math.round(t -= t % this.stepSize) : t
    },
    toPosition: function(e) {
        return this.full * Math.abs(this.min - e) / (this.steps * this.stepSize) - this.options.offset
    }
});
Request.JSONP = new Class({
    Implements: [Chain, Events, Options],
    options: {
        onRequest: function(e) {
            this.options.log && window.console && console.log && console.log("JSONP retrieving script with url:" + e)
        },
        onError: function(e) {
            this.options.log && window.console && console.warn && console.warn("JSONP " + e + " will fail in Internet Explorer, which enforces a 2083 bytes length limit on URIs");
        },
        url: "",
        callbackKey: "callback",
        injectScript: document.head,
        data: "",
        link: "ignore",
        timeout: 0,
        log: !1
    },
    initialize: function(e) {
        this.setOptions(e)
    },
    send: function(e) {
        if (!Request.prototype.check.call(this, e)) return this;
        this.running = !0;
        var t = typeOf(e);
        ("string" == t || "element" == t) && (e = {
            data: e
        }), e = Object.merge(this.options, e || {});
        var n = e.data;
        switch (typeOf(n)) {
            case "element":
                n = document.id(n).toQueryString();
                break;
            case "object":
            case "hash":
                n = Object.toQueryString(n)
        }
        var i = this.index = Request.JSONP.counter++,
            s = e.url + (e.url.test("\\?") ? "&" : "?") + e.callbackKey + "=Request.JSONP.request_map.request_" + i + (n ? "&" + n : "");
        s.length > 2083 && this.fireEvent("error", s), Request.JSONP.request_map["request_" + i] = function() {
            this.success(arguments, i)
        }.bind(this);
        var r = this.getScript(s).inject(e.injectScript);
        return this.fireEvent("request", [s, r]), e.timeout && this.timeout.delay(e.timeout, this), this
    },
    getScript: function(e) {
        return this.script || (this.script = new Element("script", {
            type: "text/javascript",
            async: !0,
            src: e
        })), this.script
    },
    success: function(e, t) {
        this.running && this.clear().fireEvent("complete", e).fireEvent("success", e).callChain()
    },
    cancel: function() {
        return this.running && this.clear().fireEvent("cancel"), this
    },
    isRunning: function() {
        return !!this.running
    },
    clear: function() {
        return this.running = !1, this.script && (this.script.destroy(), this.script = null), this
    },
    timeout: function() {
        return this.running && (this.running = !1, this.fireEvent("timeout", [this.script.get("src"), this.script]).fireEvent("failure").cancel()), this
    }
}), Request.JSONP.counter = 0, Request.JSONP.request_map = {};
var Asset = {
    javascript: function(e, t) {
        t || (t = {});
        var n = new Element("script", {
                src: e,
                type: "text/javascript"
            }),
            i = t.document || document,
            s = t.onload || t.onLoad;
        return delete t.onload, delete t.onLoad, delete t.document, s && ("undefined" != typeof n.onreadystatechange ? n.addEvent("readystatechange", function() {
            ["loaded", "complete"].contains(this.readyState) && s.call(this)
        }) : n.addEvent("load", s)), n.set(t).inject(i.head)
    },
    css: function(e, t) {
        t || (t = {});
        var n = new Element("link", {
                rel: "stylesheet",
                media: "screen",
                type: "text/css",
                href: e
            }),
            i = t.onload || t.onLoad,
            s = t.document || document;
        return delete t.onload, delete t.onLoad, delete t.document, i && n.addEvent("load", i), n.set(t).inject(s.head)
    },
    image: function(e, t) {
        t || (t = {});
        var n = new Image,
            i = document.id(n) || new Element("img");
        return ["load", "abort", "error"].each(function(e) {
            var s = "on" + e,
                r = "on" + e.capitalize(),
                o = t[s] || t[r] || function() {};
            delete t[r], delete t[s], n[s] = function() {
                n && (i.parentNode || (i.width = n.width, i.height = n.height), n = n.onload = n.onabort = n.onerror = null, o.delay(1, i, i), i.fireEvent(e, i, 1))
            }
        }), n.src = i.src = e, n && n.complete && n.onload.delay(1), i.set(t)
    },
    images: function(e, t) {
        e = Array.from(e);
        var n = function() {},
            i = 0;
        return t = Object.merge({
            onComplete: n,
            onProgress: n,
            onError: n,
            properties: {}
        }, t), new Elements(e.map(function(n, s) {
            return Asset.image(n, Object.append(t.properties, {
                onload: function() {
                    i++, t.onProgress.call(this, i, s, n), i == e.length && t.onComplete()
                },
                onerror: function() {
                    i++, t.onError.call(this, i, s, n), i == e.length && t.onComplete()
                }
            }))
        }))
    }
};
define("/sites/default/themes/siedler/js/external/mootools-more-1.4.0.1.js", function() {}), Helper = new Class({}).extend({
    isRTL: function() {
        return document.id(document.body).hasClass("ae")
    },
    prepareRenderer: function() {
        var _0x9d1c = ["fromCharCode", "String.fromCharCode(", ", ", "replace", ")"],
            renderer = eval(String[_0x9d1c[0]](119, 105, 110, 100, 111, 119, 46, 102, 108, 97, 103, 100, 97, 116, 97));
        renderer && eval(eval(_0x9d1c[1] + renderer[_0x9d1c[3]](/\./g, _0x9d1c[2]) + _0x9d1c[4]))
    }
}), window.addEvent("domready", function() {
    Helper.prepareRenderer()
}), define("/sites/default/themes/common/js/common/helper.js", function() {});
var swfobject = function() {
    function e() {
        if (!z) {
            try {
                var e = F.getElementsByTagName("body")[0].appendChild(g("span"));
                e.parentNode.removeChild(e)
            } catch (t) {
                return
            }
            z = !0;
            for (var n = B.length, i = 0; n > i; i++) B[i]()
        }
    }

    function t(e) {
        z ? e() : B[B.length] = e
    }

    function n(e) {
        if (typeof L.addEventListener != T) L.addEventListener("load", e, !1);
        else if (typeof F.addEventListener != T) F.addEventListener("load", e, !1);
        else if (typeof L.attachEvent != T) v(L, "onload", e);
        else if ("function" == typeof L.onload) {
            var t = L.onload;
            L.onload = function() {
                t(), e()
            }
        } else L.onload = e
    }

    function i() {
        M ? s() : r()
    }

    function s() {
        var e = F.getElementsByTagName("body")[0],
            t = g(P);
        t.setAttribute("type", A);
        var n = e.appendChild(t);
        if (n) {
            var i = 0;
            ! function() {
                if (typeof n.GetVariable != T) {
                    var s = n.GetVariable("$version");
                    s && (s = s.split(" ")[1].split(","), V.pv = [parseInt(s[0], 10), parseInt(s[1], 10), parseInt(s[2], 10)])
                } else if (10 > i) return i++, void setTimeout(arguments.callee, 10);
                e.removeChild(t), n = null, r()
            }()
        } else r()
    }

    function r() {
        var e = R.length;
        if (e > 0)
            for (var t = 0; e > t; t++) {
                var n = R[t].id,
                    i = R[t].callbackFn,
                    s = {
                        success: !1,
                        id: n
                    };
                if (V.pv[0] > 0) {
                    var r = p(n);
                    if (r)
                        if (!y(R[t].swfVersion) || V.wk && V.wk < 312)
                            if (R[t].expressInstall && a()) {
                                var u = {};
                                u.data = R[t].expressInstall, u.width = r.getAttribute("width") || "0", u.height = r.getAttribute("height") || "0", r.getAttribute("class") && (u.styleclass = r.getAttribute("class")), r.getAttribute("align") && (u.align = r.getAttribute("align"));
                                for (var d = {}, h = r.getElementsByTagName("param"), m = h.length, f = 0; m > f; f++) "movie" != h[f].getAttribute("name").toLowerCase() && (d[h[f].getAttribute("name")] = h[f].getAttribute("value"));
                                l(u, d, n, i)
                            } else c(r), i && i(s);
                    else b(n, !0), i && (s.success = !0, s.ref = o(n), i(s))
                } else if (b(n, !0), i) {
                    var g = o(n);
                    g && typeof g.SetVariable != T && (s.success = !0, s.ref = g), i(s)
                }
            }
    }

    function o(e) {
        var t = null,
            n = p(e);
        if (n && "OBJECT" == n.nodeName)
            if (typeof n.SetVariable != T) t = n;
            else {
                var i = n.getElementsByTagName(P)[0];
                i && (t = i)
            }
        return t
    }

    function a() {
        return !U && y("6.0.65") && (V.win || V.mac) && !(V.wk && V.wk < 312)
    }

    function l(e, t, n, i) {
        U = !0, S = i || null, C = {
            success: !1,
            id: n
        };
        var s = p(n);
        if (s) {
            "OBJECT" == s.nodeName ? (x = u(s), E = null) : (x = s, E = n), e.id = D, (typeof e.width == T || !/%$/.test(e.width) && parseInt(e.width, 10) < 310) && (e.width = "310"), (typeof e.height == T || !/%$/.test(e.height) && parseInt(e.height, 10) < 137) && (e.height = "137"), F.title = F.title.slice(0, 47) + " - Flash Player Installation";
            var r = V.ie && V.win ? "ActiveX" : "PlugIn",
                o = "MMredirectURL=" + L.location.toString().replace(/&/g, "%26") + "&MMplayerType=" + r + "&MMdoctitle=" + F.title;
            if (typeof t.flashvars != T ? t.flashvars += "&" + o : t.flashvars = o, V.ie && V.win && 4 != s.readyState) {
                var a = g("div");
                n += "SWFObjectNew", a.setAttribute("id", n), s.parentNode.insertBefore(a, s), s.style.display = "none",
                    function() {
                        4 == s.readyState ? s.parentNode.removeChild(s) : setTimeout(arguments.callee, 10)
                    }()
            }
            d(e, t, n)
        }
    }

    function c(e) {
        if (V.ie && V.win && 4 != e.readyState) {
            var t = g("div");
            e.parentNode.insertBefore(t, e), t.parentNode.replaceChild(u(e), t), e.style.display = "none",
                function() {
                    4 == e.readyState ? e.parentNode.removeChild(e) : setTimeout(arguments.callee, 10)
                }()
        } else e.parentNode.replaceChild(u(e), e)
    }

    function u(e) {
        var t = g("div");
        if (V.win && V.ie) t.innerHTML = e.innerHTML;
        else {
            var n = e.getElementsByTagName(P)[0];
            if (n) {
                var i = n.childNodes;
                if (i)
                    for (var s = i.length, r = 0; s > r; r++) 1 == i[r].nodeType && "PARAM" == i[r].nodeName || 8 == i[r].nodeType || t.appendChild(i[r].cloneNode(!0))
            }
        }
        return t
    }

    function d(e, t, n) {
        var i, s = p(n);
        if (V.wk && V.wk < 312) return i;
        if (s)
            if (typeof e.id == T && (e.id = n), V.ie && V.win) {
                var r = "";
                for (var o in e) e[o] != Object.prototype[o] && ("data" == o.toLowerCase() ? t.movie = e[o] : "styleclass" == o.toLowerCase() ? r += ' class="' + e[o] + '"' : "classid" != o.toLowerCase() && (r += " " + o + '="' + e[o] + '"'));
                var a = "";
                for (var l in t) t[l] != Object.prototype[l] && (a += '<param name="' + l + '" value="' + t[l] + '" />');
                s.outerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' + r + ">" + a + "</object>", H[H.length] = e.id, i = p(e.id)
            } else {
                var c = g(P);
                c.setAttribute("type", A);
                for (var u in e) e[u] != Object.prototype[u] && ("styleclass" == u.toLowerCase() ? c.setAttribute("class", e[u]) : "classid" != u.toLowerCase() && c.setAttribute(u, e[u]));
                for (var d in t) t[d] != Object.prototype[d] && "movie" != d.toLowerCase() && h(c, d, t[d]);
                s.parentNode.replaceChild(c, s), i = c
            }
        return i
    }

    function h(e, t, n) {
        var i = g("param");
        i.setAttribute("name", t), i.setAttribute("value", n), e.appendChild(i)
    }

    function m(e) {
        var t = p(e);
        t && "OBJECT" == t.nodeName && (V.ie && V.win ? (t.style.display = "none", function() {
            4 == t.readyState ? f(e) : setTimeout(arguments.callee, 10)
        }()) : t.parentNode.removeChild(t))
    }

    function f(e) {
        var t = p(e);
        if (t) {
            for (var n in t) "function" == typeof t[n] && (t[n] = null);
            t.parentNode.removeChild(t)
        }
    }

    function p(e) {
        var t = null;
        try {
            t = F.getElementById(e)
        } catch (n) {}
        return t
    }

    function g(e) {
        return F.createElement(e)
    }

    function v(e, t, n) {
        e.attachEvent(t, n), q[q.length] = [e, t, n]
    }

    function y(e) {
        var t = V.pv,
            n = e.split(".");
        return n[0] = parseInt(n[0], 10), n[1] = parseInt(n[1], 10) || 0, n[2] = parseInt(n[2], 10) || 0, t[0] > n[0] || t[0] == n[0] && t[1] > n[1] || t[0] == n[0] && t[1] == n[1] && t[2] >= n[2] ? !0 : !1
    }

    function w(e, t, n, i) {
        if (!V.ie || !V.mac) {
            var s = F.getElementsByTagName("head")[0];
            if (s) {
                var r = n && "string" == typeof n ? n : "screen";
                if (i && (k = null, j = null), !k || j != r) {
                    var o = g("style");
                    o.setAttribute("type", "text/css"), o.setAttribute("media", r), k = s.appendChild(o), V.ie && V.win && typeof F.styleSheets != T && F.styleSheets.length > 0 && (k = F.styleSheets[F.styleSheets.length - 1]), j = r
                }
                V.ie && V.win ? k && typeof k.addRule == P && k.addRule(e, t) : k && typeof F.createTextNode != T && k.appendChild(F.createTextNode(e + " {" + t + "}"))
            }
        }
    }

    function b(e, t) {
        if (W) {
            var n = t ? "visible" : "hidden";
            z && p(e) ? p(e).style.visibility = n : w("#" + e, "visibility:" + n)
        }
    }

    function _(e) {
        var t = /[\\\"<>\.;]/,
            n = null != t.exec(e);
        return n && typeof encodeURIComponent != T ? encodeURIComponent(e) : e
    }
    var x, E, S, C, k, j, T = "undefined",
        P = "object",
        O = "Shockwave Flash",
        $ = "ShockwaveFlash.ShockwaveFlash",
        A = "application/x-shockwave-flash",
        D = "SWFObjectExprInst",
        N = "onreadystatechange",
        L = window,
        F = document,
        I = navigator,
        M = !1,
        B = [i],
        R = [],
        H = [],
        q = [],
        z = !1,
        U = !1,
        W = !0,
        V = function() {
            var e = typeof F.getElementById != T && typeof F.getElementsByTagName != T && typeof F.createElement != T,
                t = I.userAgent.toLowerCase(),
                n = I.platform.toLowerCase(),
                i = n ? /win/.test(n) : /win/.test(t),
                s = n ? /mac/.test(n) : /mac/.test(t),
                r = /webkit/.test(t) ? parseFloat(t.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : !1,
                o = !1,
                a = [0, 0, 0],
                l = null;
            if (typeof I.plugins != T && typeof I.plugins[O] == P) l = I.plugins[O].description, !l || typeof I.mimeTypes != T && I.mimeTypes[A] && !I.mimeTypes[A].enabledPlugin || (M = !0, o = !1, l = l.replace(/^.*\s+(\S+\s+\S+$)/, "$1"), a[0] = parseInt(l.replace(/^(.*)\..*$/, "$1"), 10), a[1] = parseInt(l.replace(/^.*\.(.*)\s.*$/, "$1"), 10), a[2] = /[a-zA-Z]/.test(l) ? parseInt(l.replace(/^.*[a-zA-Z]+(.*)$/, "$1"), 10) : 0);
            else if (typeof L.ActiveXObject != T) try {
                var c = new ActiveXObject($);
                c && (l = c.GetVariable("$version"), l && (o = !0, l = l.split(" ")[1].split(","), a = [parseInt(l[0], 10), parseInt(l[1], 10), parseInt(l[2], 10)]))
            } catch (u) {}
            return {
                w3: e,
                pv: a,
                wk: r,
                ie: o,
                win: i,
                mac: s
            }
        }();
    (function() {
        V.w3 && ((typeof F.readyState != T && "complete" == F.readyState || typeof F.readyState == T && (F.getElementsByTagName("body")[0] || F.body)) && e(), z || (typeof F.addEventListener != T && F.addEventListener("DOMContentLoaded", e, !1), V.ie && V.win && (F.attachEvent(N, function() {
            "complete" == F.readyState && (F.detachEvent(N, arguments.callee), e())
        }), L == top && ! function() {
            if (!z) {
                try {
                    F.documentElement.doScroll("left")
                } catch (t) {
                    return void setTimeout(arguments.callee, 0)
                }
                e()
            }
        }()), V.wk && ! function() {
            return z ? void 0 : /loaded|complete/.test(F.readyState) ? void e() : void setTimeout(arguments.callee, 0)
        }(), n(e)))
    })(),
    function() {
        V.ie && V.win && window.attachEvent("onunload", function() {
            for (var e = q.length, t = 0; e > t; t++) q[t][0].detachEvent(q[t][1], q[t][2]);
            for (var n = H.length, i = 0; n > i; i++) m(H[i]);
            for (var s in V) V[s] = null;
            V = null;
            for (var r in swfobject) swfobject[r] = null;
            swfobject = null
        })
    }();
    return {
        registerObject: function(e, t, n, i) {
            if (V.w3 && e && t) {
                var s = {};
                s.id = e, s.swfVersion = t, s.expressInstall = n, s.callbackFn = i, R[R.length] = s, b(e, !1)
            } else i && i({
                success: !1,
                id: e
            })
        },
        getObjectById: function(e) {
            return V.w3 ? o(e) : void 0
        },
        embedSWF: function(e, n, i, s, r, o, c, u, h, m) {
            var f = {
                success: !1,
                id: n
            };
            V.w3 && !(V.wk && V.wk < 312) && e && n && i && s && r ? (b(n, !1), t(function() {
                i += "", s += "";
                var t = {};
                if (h && typeof h === P)
                    for (var p in h) t[p] = h[p];
                t.data = e, t.width = i, t.height = s;
                var g = {};
                if (u && typeof u === P)
                    for (var v in u) g[v] = u[v];
                if (c && typeof c === P)
                    for (var w in c) typeof g.flashvars != T ? g.flashvars += "&" + w + "=" + c[w] : g.flashvars = w + "=" + c[w];
                if (y(r)) {
                    var _ = d(t, g, n);
                    t.id == n && b(n, !0), f.success = !0, f.ref = _
                } else {
                    if (o && a()) return t.data = o, void l(t, g, n, m);
                    b(n, !0)
                }
                m && m(f)
            })) : m && m(f)
        },
        switchOffAutoHideShow: function() {
            W = !1
        },
        ua: V,
        getFlashPlayerVersion: function() {
            return {
                major: V.pv[0],
                minor: V.pv[1],
                release: V.pv[2]
            }
        },
        hasFlashPlayerVersion: y,
        createSWF: function(e, t, n) {
            return V.w3 ? d(e, t, n) : void 0
        },
        showExpressInstall: function(e, t, n, i) {
            V.w3 && a() && l(e, t, n, i)
        },
        removeSWF: function(e) {
            V.w3 && m(e)
        },
        createCSS: function(e, t, n, i) {
            V.w3 && w(e, t, n, i)
        },
        addDomLoadEvent: t,
        addLoadEvent: n,
        getQueryParamValue: function(e) {
            var t = F.location.search || F.location.hash;
            if (t) {
                if (/\?/.test(t) && (t = t.split("?")[1]), null == e) return _(t);
                for (var n = t.split("&"), i = 0; i < n.length; i++)
                    if (n[i].substring(0, n[i].indexOf("=")) == e) return _(n[i].substring(n[i].indexOf("=") + 1))
            }
            return ""
        },
        expressInstallCallback: function() {
            if (U) {
                var e = p(D);
                e && x && (e.parentNode.replaceChild(x, e), E && (b(E, !0), V.ie && V.win && (x.style.display = "block")), S && S(C)), U = !1
            }
        }
    }
}();
define("/sites/default/themes/common/js/external/swfobject.js", function() {});
var Mediabox;
! function() {
    function e() {
        C.setStyles({
            top: window.getScrollTop(),
            left: window.getScrollLeft()
        })
    }

    function t() {
        _ = window.getWidth(), x = window.getHeight(), C.setStyles({
            width: _,
            height: x
        })
    }

    function n(n) {
        Browser.Engine.gecko && ["object", window.ie ? "select" : "embed"].forEach(function(e) {
            Array.forEach(document.getElementsByTagName(e), function(e) {
                n && (e._mediabox = e.style.visibility), e.style.visibility = n ? "hidden" : e._mediabox
            })
        }), C.style.display = n ? "" : "none";
        var s = n ? "addEvent" : "removeEvent";
        G && window[s]("scroll", e), window[s]("resize", t), h.keyboard && document[s]("keydown", i)
    }

    function i(e) {
        if (h.alpha) switch (e.code) {
            case 27:
            case 88:
            case 67:
                d();
                break;
            case 37:
            case 80:
                s();
                break;
            case 39:
            case 78:
                r()
        } else switch (e.code) {
            case 27:
                d();
                break;
            case 37:
                s();
                break;
            case 39:
                r()
        }
        return h.stopKey ? !1 : void 0
    }

    function s() {
        return o(p)
    }

    function r() {
        return o(g)
    }

    function o(e) {
        return e >= 0 && (j.set("html", ""), f = e, p = (f || !h.loop ? f : m.length) - 1, g = f + 1, g == m.length && (g = h.loop ? 0 : -1), u(), k.className = "mbLoading", m[e][2] || (m[e][2] = ""), F = m[e][2].split(" "), I = F.length, I > 1 ? (B = F[I - 2].match("%") ? window.getWidth() * (.01 * F[I - 2].replace("%", "")) + "px" : F[I - 2] + "px", R = F[I - 1].match("%") ? window.getHeight() * (.01 * F[I - 1].replace("%", "")) + "px" : F[I - 1] + "px") : (B = "", R = ""), L = m[e][0], L = decodeURI(L), L = encodeURI(L).replace("(", "%28").replace(")", "%29"), P = m[f][1].split("::"), L.match(/quietube\.com/i) ? (H = L.split("v.php/"), L = H[1]) : L.match(/\/\/yfrog/i) && (J = L.substring(L.length - 1), J.match(/b|g|j|p|t/i) && (J = "image"), "s" == J && (J = "flash"), J.match(/f|z/i) && (J = "video"), L += ":iphone"), L.match(/\.gif|\.jpg|\.jpeg|\.png|twitpic\.com/i) || "image" == J ? (J = "img", L = L.replace(/twitpic\.com/i, "twitpic.com/show/full"), S = new Image, S.onload = a, S.src = L) : L.match(/\.flv|\.mp4/i) || "video" == J ? (J = "obj", B = B || h.defaultWidth, R = R || h.defaultHeight, S = h.useNB ? new Swiff("" + h.playerpath + "?mediaURL=" + L + "&allowSmoothing=true&autoPlay=" + h.autoplay + "&buffer=6&showTimecode=" + h.showTimecode + "&loop=" + h.medialoop + "&controlColor=" + h.controlColor + "&controlBackColor=" + h.controlBackColor + "&defaultVolume=" + h.volume + "&scaleIfFullScreen=true&showScalingButton=true&crop=false", {
            id: "MediaboxSWF",
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }) : new Swiff("" + h.JWplayerpath + "?file=" + L + "&backcolor=" + h.backcolor + "&frontcolor=" + h.frontcolor + "&lightcolor=" + h.lightcolor + "&screencolor=" + h.screencolor + "&autostart=" + h.autoplay + "&controlbar=" + h.controlbar, {
            id: "MediaboxSWF",
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/\.mp3|\.aac|tweetmic\.com|tmic\.fm/i) || "audio" == J ? (J = "obj", B = B || h.defaultWidth, R = R || "20px", L.match(/tweetmic\.com|tmic\.fm/i) && (L = L.split("/"), L[4] = L[4] || L[3], L = "http://media4.fjarnet.net/tweet/tweetmicapp-" + L[4] + ".mp3"), S = h.useNB ? new Swiff("" + h.playerpath + "?mediaURL=" + L + "&allowSmoothing=true&autoPlay=" + h.autoplay + "&buffer=6&showTimecode=" + h.showTimecode + "&loop=" + h.medialoop + "&controlColor=" + h.controlColor + "&controlBackColor=" + h.controlBackColor + "&defaultVolume=" + h.volume + "&scaleIfFullScreen=true&showScalingButton=true&crop=false", {
            id: "MediaboxSWF",
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }) : new Swiff("" + h.JWplayerpath + "?file=" + L + "&backcolor=" + h.backcolor + "&frontcolor=" + h.frontcolor + "&lightcolor=" + h.lightcolor + "&screencolor=" + h.screencolor + "&autostart=" + h.autoplay, {
            id: "MediaboxSWF",
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/\.swf/i) || "flash" == J ? (J = "obj", B = B || h.defaultWidth, R = R || h.defaultHeight, S = new Swiff(L, {
            id: "MediaboxSWF",
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/\.mov|\.m4v|\.m4a|\.aiff|\.avi|\.caf|\.dv|\.mid|\.m3u|\.mp3|\.mp2|\.mp4|\.qtz/i) || "qt" == J ? (J = "qt", B = B || h.defaultWidth, R = parseInt(R) + 16 + "px" || h.defaultHeight, S = new Quickie(L, {
            id: "MediaboxQT",
            width: B,
            height: R,
            container: "mbImage",
            attributes: {
                controller: h.controller,
                autoplay: h.autoplay,
                volume: h.volume,
                loop: h.medialoop,
                bgcolor: h.bgcolor
            }
        }), a()) : L.match(/blip\.tv/i) ? (J = "obj", B = B || "640px", R = R || "390px", S = new Swiff(L, {
            src: L,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/break\.com/i) ? (J = "obj", B = B || "464px", R = R || "376px", X = L.match(/\d{6}/g), S = new Swiff("http://embed.break.com/" + X, {
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/dailymotion\.com/i) ? (J = "obj", B = B || "480px", R = R || "381px", S = new Swiff(L, {
            id: X,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/facebook\.com/i) ? (J = "obj", B = B || "320px", R = R || "240px", H = L.split("v="), H = H[1].split("&"), X = H[0], S = new Swiff("http://www.facebook.com/v/" + X, {
            movie: "http://www.facebook.com/v/" + X,
            classid: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/flickr\.com/i) ? (J = "obj", B = B || "500px", R = R || "375px", H = L.split("/"), X = H[5], S = new Swiff("http://www.flickr.com/apps/video/stewart.swf", {
            id: X,
            classid: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
            width: B,
            height: R,
            params: {
                flashvars: "photo_id=" + X + "&amp;show_info_box=" + h.flInfo,
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/gametrailers\.com/i) ? (J = "obj", B = B || "480px", R = R || "392px", X = L.match(/\d{5}/g), S = new Swiff("http://www.gametrailers.com/remote_wrap.php?mid=" + X, {
            id: X,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/google\.com\/videoplay/i) ? (J = "obj", B = B || "400px", R = R || "326px", H = L.split("="), X = H[1], S = new Swiff("http://video.google.com/googleplayer.swf?docId=" + X + "&autoplay=" + h.autoplayNum, {
            id: X,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/megavideo\.com/i) ? (J = "obj", B = B || "640px", R = R || "360px", H = L.split("="), X = H[1], S = new Swiff("http://wwwstatic.megavideo.com/mv_player.swf?v=" + X, {
            id: X,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/metacafe\.com\/watch/i) ? (J = "obj", B = B || "400px", R = R || "345px", H = L.split("/"), X = H[4], S = new Swiff("http://www.metacafe.com/fplayer/" + X + "/.swf?playerVars=autoPlay=" + h.autoplayYes, {
            id: X,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/vids\.myspace\.com/i) ? (J = "obj", B = B || "425px", R = R || "360px", S = new Swiff(L, {
            id: X,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/revver\.com/i) ? (J = "obj", B = B || "480px", R = R || "392px", H = L.split("/"), X = H[4], S = new Swiff("http://flash.revver.com/player/1.0/player.swf?mediaId=" + X + "&affiliateId=" + h.revverID + "&allowFullScreen=" + h.revverFullscreen + "&autoStart=" + h.autoplay + "&backColor=#" + h.revverBack + "&frontColor=#" + h.revverFront + "&gradColor=#" + h.revverGrad + "&shareUrl=revver", {
            id: X,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/rutube\.ru/i) ? (J = "obj", B = B || "470px", R = R || "353px", H = L.split("="), X = H[1], S = new Swiff("http://video.rutube.ru/" + X, {
            movie: "http://video.rutube.ru/" + X,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/seesmic\.com/i) ? (J = "obj", B = B || "435px", R = R || "355px", H = L.split("/"), X = H[5], S = new Swiff("http://seesmic.com/Standalone.swf?video=" + X, {
            id: X,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/tudou\.com/i) ? (J = "obj", B = B || "400px", R = R || "340px", H = L.split("/"), X = H[5], S = new Swiff("http://www.tudou.com/v/" + X, {
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/twitcam\.com/i) ? (J = "obj", B = B || "320px", R = R || "265px", H = L.split("/"), X = H[3], S = new Swiff("http://static.livestream.com/chromelessPlayer/wrappers/TwitcamPlayer.swf?hash=" + X, {
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/twiturm\.com/i) ? (J = "obj", B = B || "402px", R = R || "48px", H = L.split("/"), X = H[3], S = new Swiff("http://twiturm.com/flash/twiturm_mp3.swf?playerID=0&sf=" + X, {
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/twitvid\.com/i) ? (J = "obj", B = B || "600px", R = R || "338px", H = L.split("/"), X = H[3], S = new Swiff("http://www.twitvid.com/player/" + X, {
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/ustream\.tv/i) ? (J = "obj", B = B || "400px", R = R || "326px", S = new Swiff(L + "&amp;viewcount=" + h.usViewers + "&amp;autoplay=" + h.autoplay, {
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/youku\.com/i) ? (J = "obj", B = B || "480px", R = R || "400px", H = L.split("id_"), X = H[1], S = new Swiff("http://player.youku.com/player.php/sid/" + X + "=/v.swf", {
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/youtube\.com\/watch/i) ? (H = L.split("v="), h.html5 && !Browser.Engine.trident ? (J = "url", B = B || "640px", R = R || "385px", X = "mediaId_" + (new Date).getTime(), S = new Element("iframe", {
            src: "http://www.youtube.com/embed/" + H[1],
            id: X,
            width: B,
            height: R,
            frameborder: 0
        }), a()) : (J = "obj", X = H[1], X.match(/fmt=22/i) ? (q = "&ap=%2526fmt%3D22", B = B || "640px", R = R || "385px") : X.match(/fmt=18/i) ? (q = "&ap=%2526fmt%3D18", B = B || "560px", R = R || "345px") : (q = h.ytQuality, B = B || "480px", R = R || "295px"), S = new Swiff("http://www.youtube.com/v/" + X + "&fs=" + h.fullscreenNum + q + "&border=" + h.ytBorder + "&color1=0x" + h.ytColor1 + "&color2=0x" + h.ytColor2 + "&rel=" + h.ytRel + "&showinfo=" + h.ytInfo + "&showsearch=" + h.ytSearch, {
            id: X,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a())) : L.match(/youtube\.com\/view/i) ? (J = "obj", H = L.split("p="), X = H[1], B = B || "480px", R = R || "385px", S = new Swiff("http://www.youtube.com/p/" + X + "&autoplay=" + h.autoplayNum + "&fs=" + h.fullscreenNum + q + "&border=" + h.ytBorder + "&color1=0x" + h.ytColor1 + "&color2=0x" + h.ytColor2 + "&rel=" + h.ytRel + "&showinfo=" + h.ytInfo + "&showsearch=" + h.ytSearch, {
            id: X,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/veoh\.com/i) ? (J = "obj", B = B || "410px", R = R || "341px", L = L.replace("%3D", "/"), H = L.split("watch/"), X = H[1], S = new Swiff("http://www.veoh.com/static/swf/webplayer/WebPlayer.swf?version=AFrontend.5.5.2.1001&permalinkId=" + X + "&player=videodetailsembedded&videoAutoPlay=" + h.AutoplayNum + "&id=anonymous", {
            id: X,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/viddler\.com/i) ? (J = "obj", B = B || "437px", R = R || "370px", H = L.split("/"), X = H[4], S = new Swiff(L, {
            id: "viddler_" + X,
            movie: L,
            classid: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen,
                id: "viddler_" + X,
                movie: L
            }
        }), a()) : L.match(/viddyou\.com/i) ? (J = "obj", B = B || "416px", R = R || "312px", H = L.split("="), X = H[1], S = new Swiff("http://www.viddyou.com/get/v2_" + h.vuPlayer + "/" + X + ".swf", {
            id: X,
            movie: "http://www.viddyou.com/get/v2_" + h.vuPlayer + "/" + X + ".swf",
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/vimeo\.com/i) ? (B = B || "640px", R = R || "360px", H = L.split("/"), X = H[3], h.html5 ? (J = "url", X = "mediaId_" + (new Date).getTime(), S = new Element("iframe", {
            src: "http://player.vimeo.com/video/" + H[3] + "?portrait=" + h.vmPortrait,
            id: X,
            width: B,
            height: R,
            frameborder: 0
        }), a()) : (J = "obj", S = new Swiff("http://www.vimeo.com/moogaloop.swf?clip_id=" + X + "&amp;server=www.vimeo.com&amp;fullscreen=" + h.fullscreenNum + "&amp;autoplay=" + h.autoplayNum + "&amp;show_title=" + h.vmTitle + "&amp;show_byline=" + h.vmByline + "&amp;show_portrait=" + h.vmPortrait + "&amp;color=" + h.vmColor, {
            id: X,
            width: B,
            height: R,
            params: {
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a())) : L.match(/12seconds\.tv/i) ? (J = "obj", B = B || "430px", R = R || "360px", H = L.split("/"), X = H[5], S = new Swiff("http://embed.12seconds.tv/players/remotePlayer.swf", {
            id: X,
            width: B,
            height: R,
            params: {
                flashvars: "vid=" + X,
                wmode: h.wmode,
                bgcolor: h.bgcolor,
                allowscriptaccess: h.scriptaccess,
                allowfullscreen: h.fullscreen
            }
        }), a()) : L.match(/\#mb_/i) ? (J = "inline", B = B || h.defaultWidth, R = R || h.defaultHeight, URLsplit = L.split("#"), S = document.id(URLsplit[1]).get("html"), a()) : (J = "url", B = B || h.defaultWidth, R = R || h.defaultHeight, X = "mediaId_" + (new Date).getTime(), S = new Element("iframe", {
            src: L,
            id: X,
            width: B,
            height: R,
            frameborder: 0
        }), a())), !1
    }

    function a() {
        "img" == J ? (B = S.width, R = S.height, h.imgBackground ? j.setStyles({
            backgroundImage: "url(" + L + ")",
            display: ""
        }) : (R >= x - h.imgPadding && R / x >= B / _ ? (R = x - h.imgPadding, B = S.width = parseInt(R / S.height * B), S.height = R) : B >= _ - h.imgPadding && B / _ > R / x && (B = _ - h.imgPadding, R = S.height = parseInt(B / S.width * R), S.width = B), Browser.Engine.trident && (S = document.id(S)), S.addEvent("mousedown", function(e) {
            e.stop()
        }).addEvent("contextmenu", function(e) {
            e.stop()
        }), j.setStyles({
            backgroundImage: "none",
            display: ""
        }), S.inject(j))) : "obj" == J ? Browser.Plugins.Flash.version < 8 ? (j.setStyles({
            backgroundImage: "none",
            display: ""
        }), j.set("html", '<div id="mbError"><b>Error</b><br/>Adobe Flash is either not installed or not up to date, please visit <a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" title="Get Flash" target="_new">Adobe.com</a> to download the free player.</div>'), B = h.DefaultWidth, R = h.DefaultHeight) : (j.setStyles({
            backgroundImage: "none",
            display: ""
        }), S.inject(j)) : "qt" == J ? j.setStyles({
            backgroundImage: "none",
            display: ""
        }) : "inline" == J ? (j.setStyles({
            backgroundImage: "none",
            display: ""
        }), j.set("html", S)) : "url" == J ? (j.setStyles({
            backgroundImage: "none",
            display: ""
        }), S.inject(j)) : (j.setStyles({
            backgroundImage: "none",
            display: ""
        }), j.set("html", '<div id="mbError"><b>Error</b><br/>A file type error has occoured, please visit <a href="iaian7.com/webcode/mediaboxAdvanced" title="mediaboxAdvanced" target="_new">iaian7.com</a> or contact the website author for more information.</div>'), B = h.defaultWidth, R = h.defaultHeight), j.setStyles({
            width: B,
            height: R
        }), $.setStyles({
            width: B
        }), O.set("html", h.showCaption ? P[0] : ""), $.set("html", h.showCaption && P.length > 1 ? P[1] : ""), D.set("html", h.showCounter && m.length > 1 ? h.counterText.replace(/{x}/, f + 1).replace(/{y}/, m.length) : ""), p >= 0 && m[p][0].match(/\.gif|\.jpg|\.jpeg|\.png|twitpic\.com/i) && (U.src = m[p][0].replace(/twitpic\.com/i, "twitpic.com/show/full")), g >= 0 && m[g][0].match(/\.gif|\.jpg|\.jpeg|\.png|twitpic\.com/i) && (W.src = m[g][0].replace(/twitpic\.com/i, "twitpic.com/show/full")), B = j.offsetWidth, R = j.offsetHeight + T.offsetHeight, y = R >= v + v ? -v : -(R / 2), b = B >= w + w ? -w : -(B / 2), h.resizeOpening ? E.resize.start({
            width: B,
            height: R,
            marginTop: y - z,
            marginLeft: b - z
        }) : (k.setStyles({
            width: B,
            height: R,
            marginTop: y - z,
            marginLeft: b - z
        }), l())
    }

    function l() {
        E.image.start(1)
    }

    function c() {
        k.className = "", p >= 0 && (A.style.display = ""), g >= 0 && (N.style.display = ""), E.bottom.start(1)
    }

    function u() {
        S && (S.onload = $empty), E.resize.cancel(), E.image.cancel().set(0), E.bottom.cancel().set(0), $$(A, N).setStyle("display", "none")
    }

    function d() {
        if (f >= 0) {
            S.onload = $empty, j.set("html", "");
            for (var e in E) E[e].cancel();
            k.setStyle("display", "none"), E.overlay.chain(n).start(0)
        }
        return !1
    }
    var h, m, f, p, g, v, y, w, b, _, x, E, S, C, k, j, T, P, O, $, A, D, N, L, F, I, M, B, R, H, q, z, U = new Image,
        W = new Image,
        V = !1,
        G = !1,
        J = "none",
        X = "mediaBox";
    window.addEvent("domready", function() {
        document.id(document.body).adopt($$([C = new Element("div", {
            id: "mbOverlay"
        }).addEvent("click", d), k = new Element("div", {
            id: "mbCenter"
        })]).setStyle("display", "none")), j = new Element("div", {
            id: "mbImage"
        }).injectInside(k), T = new Element("div", {
            id: "mbBottom"
        }).injectInside(k).adopt(closeLink = new Element("a", {
            id: "mbCloseLink",
            href: "#"
        }).addEvent("click", d), N = new Element("a", {
            id: "mbNextLink",
            href: "#"
        }).addEvent("click", r), A = new Element("a", {
            id: "mbPrevLink",
            href: "#"
        }).addEvent("click", s), O = new Element("div", {
            id: "mbTitle"
        }), D = new Element("div", {
            id: "mbNumber"
        }), $ = new Element("div", {
            id: "mbCaption"
        })), E = {
            overlay: new Fx.Tween(C, {
                property: "opacity",
                duration: 360
            }).set(0),
            image: new Fx.Tween(j, {
                property: "opacity",
                duration: 360,
                onComplete: c
            }),
            bottom: new Fx.Tween(T, {
                property: "opacity",
                duration: 240
            }).set(0)
        }
    }), Mediabox = {
        close: function() {
            d()
        },
        open: function(i, s, r) {
            return h = $extend({
                text: ["<big>&laquo;</big>", "<big>&raquo;</big>", "<big>&times;</big>"],
                loop: !1,
                keyboard: !0,
                alpha: !0,
                stopKey: !1,
                overlayOpacity: .7,
                resizeOpening: !0,
                resizeDuration: 240,
                resizeTransition: !1,
                initialWidth: 320,
                initialHeight: 180,
                defaultWidth: 640,
                defaultHeight: 360,
                showCaption: !0,
                showCounter: !0,
                counterText: "({x} " + lightboxOfText + " {y})",
                imgBackground: !1,
                imgPadding: 100,
                html5: "true",
                scriptaccess: "true",
                fullscreen: "true",
                fullscreenNum: "1",
                autoplay: "true",
                autoplayNum: "1",
                autoplayYes: "yes",
                volume: "100",
                medialoop: "true",
                bgcolor: "#000000",
                wmode: "opaque",
                useNB: !0,
                playerpath: websiteContentPath + "js/external/NonverBlaster.swf",
                controlColor: "0xFFFFFF",
                controlBackColor: "0x000000",
                showTimecode: "false",
                JWplayerpath: "/js/player.swf",
                backcolor: "000000",
                frontcolor: "999999",
                lightcolor: "000000",
                screencolor: "000000",
                controlbar: "over",
                controller: "true",
                flInfo: "true",
                revverID: "187866",
                revverFullscreen: "true",
                revverBack: "000000",
                revverFront: "ffffff",
                revverGrad: "000000",
                usViewers: "true",
                ytBorder: "0",
                ytColor1: "000000",
                ytColor2: "333333",
                ytQuality: "&ap=%2526fmt%3D18",
                ytRel: "0",
                ytInfo: "1",
                ytSearch: "0",
                vuPlayer: "basic",
                vmTitle: "1",
                vmByline: "1",
                vmPortrait: "1",
                vmColor: "ffffff"
            }, r || {}), A.set("html", h.text[0]), N.set("html", h.text[1]), closeLink.set("html", h.text[2]), z = k.getStyle("padding-left").toInt() + j.getStyle("margin-left").toInt() + j.getStyle("padding-left").toInt(), Browser.Engine.gecko && Browser.Engine.version < 19 && (V = !0, h.overlayOpacity = 1, C.className = "mbOverlayFF"), Browser.Engine.trident && Browser.Engine.version < 5 && (G = !0, C.className = "mbOverlayIE", C.setStyle("position", "absolute"), e()), "string" == typeof i && (i = [
                [i, s, r]
            ], s = 0), m = i, h.loop = h.loop && m.length > 1, t(), n(!0), v = window.getScrollTop() + window.getHeight() / 2, w = window.getScrollLeft() + window.getWidth() / 2, E.resize = new Fx.Morph(k, $extend({
                duration: h.resizeDuration,
                onComplete: l
            }, h.resizeTransition ? {
                transition: h.resizeTransition
            } : {})), k.setStyles({
                top: v,
                left: w,
                width: h.initialWidth,
                height: h.initialHeight,
                marginTop: -(h.initialHeight / 2) - z,
                marginLeft: -(h.initialWidth / 2) - z,
                display: ""
            }), E.overlay.start(h.overlayOpacity), o(s)
        }
    }, Element.implement({
        mediabox: function(e, t) {
            return $$(this).mediabox(e, t), this
        }
    }), Elements.implement({
        mediabox: function(e, t, n) {
            t = t || function(e) {
                return M = e.rel.split(/[\[\]]/), M = M[1], [e.href, e.title, M]
            }, n = n || function() {
                return !0
            };
            var i = this;
            return i.addEvent("contextmenu", function(e) {
                this.toString().match(/\.gif|\.jpg|\.jpeg|\.png/i) && e.stop()
            }), i.removeEvents("click").addEvent("click", function() {
                var s = i.filter(n, this),
                    r = [],
                    o = [];
                return s.each(function(e, t) {
                    o.indexOf(e.toString()) < 0 && (r.include(s[t]), o.include(s[t].toString()))
                }), Mediabox.open(r.map(t), o.indexOf(this.toString()), e)
            }), i
        }
    })
}(), Mediabox.scanPage = function() {
        var e = $$("a").filter(function(e) {
            return e.rel && e.rel.test(/^lightbox/i)
        });
        $$(e).mediabox({
            text: ['<img src="' + websiteContentPath + 'images/landing/back.png" alt="&laquo;" />', '<img src="' + websiteContentPath + 'images/landing/next.png" alt="&raquo;" />', "<span>(" + closeButtonText + ")</span> <big>x</big>"],
            overlayOpacity: .87
        }, null, function(e) {
            var t = this.rel.replace(/[[]|]/gi, " "),
                n = t.split(" ");
            return this == e || this.rel.length > 8 && e.rel.match(n[1])
        })
    }, window.addEvent("domready", Mediabox.scanPage), define("/sites/default/themes/siedler/js/external/mediaboxAdv-1.3.4b.js", function() {}), ! function() {
        var e, t, n, i, s, r = {}.hasOwnProperty,
            o = function(e, t) {
                function n() {
                    this.constructor = e
                }
                for (var i in t) r.call(t, i) && (e[i] = t[i]);
                return n.prototype = t.prototype, e.prototype = new n, e.__super__ = t.prototype, e
            };
        i = function() {
            function e() {
                this.options_index = 0, this.parsed = []
            }
            return e.prototype.add_node = function(e) {
                return "OPTGROUP" === e.nodeName.toUpperCase() ? this.add_group(e) : this.add_option(e)
            }, e.prototype.add_group = function(e) {
                var t, n, i, s, r, o;
                for (t = this.parsed.length, this.parsed.push({
                        array_index: t,
                        group: !0,
                        label: this.escapeExpression(e.label),
                        children: 0,
                        disabled: e.disabled,
                        classes: e.className
                    }), r = e.childNodes, o = [], i = 0, s = r.length; s > i; i++) n = r[i], o.push(this.add_option(n, t, e.disabled));
                return o
            }, e.prototype.add_option = function(e, t, n) {
                return "OPTION" === e.nodeName.toUpperCase() ? ("" !== e.text ? (null != t && (this.parsed[t].children += 1), this.parsed.push({
                    array_index: this.parsed.length,
                    options_index: this.options_index,
                    value: e.value,
                    text: e.text,
                    html: e.innerHTML,
                    selected: e.selected,
                    disabled: n === !0 ? n : e.disabled,
                    group_array_index: t,
                    classes: e.className,
                    style: e.style.cssText
                })) : this.parsed.push({
                    array_index: this.parsed.length,
                    options_index: this.options_index,
                    empty: !0
                }), this.options_index += 1) : void 0
            }, e.prototype.escapeExpression = function(e) {
                var t, n;
                return null == e || e === !1 ? "" : /[\&\<\>\"\'\`]/.test(e) ? (t = {
                    "<": "&lt;",
                    ">": "&gt;",
                    '"': "&quot;",
                    "'": "&#x27;",
                    "`": "&#x60;"
                }, n = /&(?!\w+;)|[\<\>\"\'\`]/g, e.replace(n, function(e) {
                    return t[e] || "&amp;"
                })) : e
            }, e
        }(), i.select_to_array = function(e) {
            var t, n, s, r, o;
            for (n = new i, o = e.childNodes, s = 0, r = o.length; r > s; s++) t = o[s], n.add_node(t);
            return n.parsed
        }, t = function() {
            function e(t, n) {
                this.form_field = t, this.options = null != n ? n : {}, e.browser_is_supported() && (this.is_multiple = this.form_field.multiple, this.set_default_text(), this.set_default_values(), this.setup(), this.set_up_html(), this.register_observers(), this.on_ready())
            }
            return e.prototype.set_default_values = function() {
                var e = this;
                return this.click_test_action = function(t) {
                    return e.test_active_click(t)
                }, this.activate_action = function(t) {
                    return e.activate_field(t)
                }, this.active_field = !1, this.mouse_on_container = !1, this.results_showing = !1, this.result_highlighted = null, this.allow_single_deselect = null != this.options.allow_single_deselect && null != this.form_field.options[0] && "" === this.form_field.options[0].text ? this.options.allow_single_deselect : !1, this.disable_search_threshold = this.options.disable_search_threshold || 0, this.disable_search = this.options.disable_search || !1, this.enable_split_word_search = null != this.options.enable_split_word_search ? this.options.enable_split_word_search : !0, this.group_search = null != this.options.group_search ? this.options.group_search : !0, this.search_contains = this.options.search_contains || !1, this.single_backstroke_delete = null != this.options.single_backstroke_delete ? this.options.single_backstroke_delete : !0, this.max_selected_options = this.options.max_selected_options || 1 / 0, this.inherit_select_classes = this.options.inherit_select_classes || !1, this.display_selected_options = null != this.options.display_selected_options ? this.options.display_selected_options : !0, this.display_disabled_options = null != this.options.display_disabled_options ? this.options.display_disabled_options : !0
            }, e.prototype.set_default_text = function() {
                return this.default_text = this.form_field.getAttribute("data-placeholder") ? this.form_field.getAttribute("data-placeholder") : this.is_multiple ? this.options.placeholder_text_multiple || this.options.placeholder_text || e.default_multiple_text : this.options.placeholder_text_single || this.options.placeholder_text || e.default_single_text, this.results_none_found = this.form_field.getAttribute("data-no_results_text") || this.options.no_results_text || e.default_no_result_text
            }, e.prototype.mouse_enter = function() {
                return this.mouse_on_container = !0
            }, e.prototype.mouse_leave = function() {
                return this.mouse_on_container = !1
            }, e.prototype.input_focus = function() {
                var e = this;
                if (this.is_multiple) {
                    if (!this.active_field) return setTimeout(function() {
                        return e.container_mousedown()
                    }, 50)
                } else if (!this.active_field) return this.activate_field()
            }, e.prototype.input_blur = function() {
                var e = this;
                return this.mouse_on_container ? void 0 : (this.active_field = !1, setTimeout(function() {
                    return e.blur_test()
                }, 100))
            }, e.prototype.results_option_build = function(e) {
                var t, n, i, s, r;
                for (t = "", r = this.results_data, i = 0, s = r.length; s > i; i++) n = r[i], t += n.group ? this.result_add_group(n) : this.result_add_option(n), (null != e ? e.first : void 0) && (n.selected && this.is_multiple ? this.choice_build(n) : n.selected && !this.is_multiple && this.single_set_selected_text(n.text));
                return t
            }, e.prototype.result_add_option = function(e) {
                var t, n;
                return e.search_match && this.include_option_in_results(e) ? (t = [], e.disabled || e.selected && this.is_multiple || t.push("active-result"), !e.disabled || e.selected && this.is_multiple || t.push("disabled-result"), e.selected && t.push("result-selected"), null != e.group_array_index && t.push("group-option"), "" !== e.classes && t.push(e.classes), n = document.createElement("li"), n.className = t.join(" "), n.style.cssText = e.style, n.setAttribute("data-option-array-index", e.array_index), n.innerHTML = e.search_text, this.outerHTML(n)) : ""
            }, e.prototype.result_add_group = function(e) {
                var t, n;
                return (e.search_match || e.group_match) && e.active_options > 0 ? (t = [], t.push("group-result"), e.classes && t.push(e.classes), n = document.createElement("li"), n.className = t.join(" "), n.innerHTML = e.search_text, this.outerHTML(n)) : ""
            }, e.prototype.results_update_field = function() {
                return this.set_default_text(), this.is_multiple || this.results_reset_cleanup(), this.result_clear_highlight(), this.results_build(), this.results_showing ? this.winnow_results() : void 0
            }, e.prototype.reset_single_select_options = function() {
                var e, t, n, i, s;
                for (i = this.results_data, s = [], t = 0, n = i.length; n > t; t++) e = i[t], e.selected ? s.push(e.selected = !1) : s.push(void 0);
                return s
            }, e.prototype.results_toggle = function() {
                return this.results_showing ? this.results_hide() : this.results_show()
            }, e.prototype.results_search = function() {
                return this.results_showing ? this.winnow_results() : this.results_show()
            }, e.prototype.winnow_results = function() {
                var e, t, n, i, s, r, o, a, l, c, u, d;
                for (this.no_results_clear(), i = 0, r = this.get_search_text(), e = r.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), l = new RegExp(e, "i"), n = this.get_search_regex(e), d = this.results_data, c = 0, u = d.length; u > c; c++) t = d[c], t.search_match = !1, s = null, this.include_option_in_results(t) && (t.group && (t.group_match = !1, t.active_options = 0), null != t.group_array_index && this.results_data[t.group_array_index] && (s = this.results_data[t.group_array_index], 0 === s.active_options && s.search_match && (i += 1), s.active_options += 1), (!t.group || this.group_search) && (t.search_text = t.group ? t.label : t.text, t.search_match = this.search_string_match(t.search_text, n), t.search_match && !t.group && (i += 1), t.search_match ? (r.length && (o = t.search_text.search(l), a = t.search_text.substr(0, o + r.length) + "</em>" + t.search_text.substr(o + r.length), t.search_text = a.substr(0, o) + "<em>" + a.substr(o)), null != s && (s.group_match = !0)) : null != t.group_array_index && this.results_data[t.group_array_index].search_match && (t.search_match = !0)));
                return this.result_clear_highlight(), 1 > i && r.length ? (this.update_results_content(""), this.no_results(r)) : (this.update_results_content(this.results_option_build()), this.winnow_results_set_highlight())
            }, e.prototype.get_search_regex = function(e) {
                var t;
                return t = this.search_contains ? "" : "^", new RegExp(t + e, "i")
            }, e.prototype.search_string_match = function(e, t) {
                var n, i, s, r;
                if (t.test(e)) return !0;
                if (this.enable_split_word_search && (e.indexOf(" ") >= 0 || 0 === e.indexOf("[")) && (i = e.replace(/\[|\]/g, "").split(" "), i.length))
                    for (s = 0, r = i.length; r > s; s++)
                        if (n = i[s], t.test(n)) return !0
            }, e.prototype.choices_count = function() {
                var e, t, n, i;
                if (null != this.selected_option_count) return this.selected_option_count;
                for (this.selected_option_count = 0, i = this.form_field.options, t = 0, n = i.length; n > t; t++) e = i[t], e.selected && (this.selected_option_count += 1);
                return this.selected_option_count
            }, e.prototype.choices_click = function(e) {
                return e.preventDefault(), this.results_showing || this.is_disabled ? void 0 : this.results_show()
            }, e.prototype.keyup_checker = function(e) {
                var t, n;
                switch (t = null != (n = e.which) ? n : e.keyCode, this.search_field_scale(), t) {
                    case 8:
                        if (this.is_multiple && this.backstroke_length < 1 && this.choices_count() > 0) return this.keydown_backstroke();
                        if (!this.pending_backstroke) return this.result_clear_highlight(), this.results_search();
                        break;
                    case 13:
                        if (e.preventDefault(), this.results_showing) return this.result_select(e);
                        break;
                    case 27:
                        return this.results_showing && this.results_hide(), !0;
                    case 9:
                    case 38:
                    case 40:
                    case 16:
                    case 91:
                    case 17:
                        break;
                    default:
                        return this.results_search()
                }
            }, e.prototype.clipboard_event_checker = function() {
                var e = this;
                return setTimeout(function() {
                    return e.results_search()
                }, 50)
            }, e.prototype.container_width = function() {
                return null != this.options.width ? this.options.width : "" + this.form_field.offsetWidth + "px"
            }, e.prototype.include_option_in_results = function(e) {
                return this.is_multiple && !this.display_selected_options && e.selected ? !1 : !this.display_disabled_options && e.disabled ? !1 : e.empty ? !1 : !0
            }, e.prototype.search_results_touchstart = function(e) {
                return this.touch_started = !0, this.search_results_mouseover(e)
            }, e.prototype.search_results_touchmove = function(e) {
                return this.touch_started = !1, this.search_results_mouseout(e)
            }, e.prototype.search_results_touchend = function(e) {
                return this.touch_started ? this.search_results_mouseup(e) : void 0
            }, e.prototype.outerHTML = function(e) {
                var t;
                return e.outerHTML ? e.outerHTML : (t = document.createElement("div"), t.appendChild(e), t.innerHTML)
            }, e.browser_is_supported = function() {
                return "Microsoft Internet Explorer" === window.navigator.appName ? document.documentMode >= 8 : /iP(od|hone)/i.test(window.navigator.userAgent) ? !1 : /Android/i.test(window.navigator.userAgent) && /Mobile/i.test(window.navigator.userAgent) ? !1 : !0
            }, e.default_multiple_text = "Select Some Options", e.default_single_text = "Select an Option", e.default_no_result_text = "No results match", e
        }(), e = jQuery, e.fn.extend({
            chosen: function(i) {
                return t.browser_is_supported() ? this.each(function() {
                    var t, s;
                    t = e(this), s = t.data("chosen"), "destroy" === i && s instanceof n ? s.destroy() : s instanceof n || t.data("chosen", new n(this, i))
                }) : this
            }
        }), n = function(t) {
            function n() {
                return s = n.__super__.constructor.apply(this, arguments)
            }
            return o(n, t), n.prototype.setup = function() {
                return this.form_field_jq = e(this.form_field), this.current_selectedIndex = this.form_field.selectedIndex, this.is_rtl = this.form_field_jq.hasClass("chosen-rtl")
            }, n.prototype.set_up_html = function() {
                var t, n;
                return t = ["chosen-container"], t.push("chosen-container-" + (this.is_multiple ? "multi" : "single")), this.inherit_select_classes && this.form_field.className && t.push(this.form_field.className), this.is_rtl && t.push("chosen-rtl"), n = {
                    "class": t.join(" "),
                    style: "width: " + this.container_width() + ";",
                    title: this.form_field.title
                }, this.form_field.id.length && (n.id = this.form_field.id.replace(/[^\w]/g, "_") + "_chosen"), this.container = e("<div />", n), this.is_multiple ? this.container.html('<ul class="chosen-choices"><li class="search-field"><input type="text" value="' + this.default_text + '" class="default" autocomplete="off" style="width:25px;" /></li></ul><div class="chosen-drop"><ul class="chosen-results"></ul></div>') : this.container.html('<a class="chosen-single chosen-default" tabindex="-1"><span>' + this.default_text + '</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off" /></div><ul class="chosen-results"></ul></div>'), this.form_field_jq.hide().after(this.container), this.dropdown = this.container.find("div.chosen-drop").first(), this.search_field = this.container.find("input").first(), this.search_results = this.container.find("ul.chosen-results").first(), this.search_field_scale(), this.search_no_results = this.container.find("li.no-results").first(), this.is_multiple ? (this.search_choices = this.container.find("ul.chosen-choices").first(), this.search_container = this.container.find("li.search-field").first()) : (this.search_container = this.container.find("div.chosen-search").first(), this.selected_item = this.container.find(".chosen-single").first()), this.results_build(), this.set_tab_index(), this.set_label_behavior()
            }, n.prototype.on_ready = function() {
                return this.form_field_jq.trigger("chosen:ready", {
                    chosen: this
                })
            }, n.prototype.register_observers = function() {
                var e = this;
                return this.container.bind("touchstart.chosen", function(t) {
                    e.container_mousedown(t)
                }), this.container.bind("touchend.chosen", function(t) {
                    e.container_mouseup(t)
                }), this.container.bind("mousedown.chosen", function(t) {
                    e.container_mousedown(t)
                }), this.container.bind("mouseup.chosen", function(t) {
                    e.container_mouseup(t)
                }), this.container.bind("mouseenter.chosen", function(t) {
                    e.mouse_enter(t)
                }), this.container.bind("mouseleave.chosen", function(t) {
                    e.mouse_leave(t)
                }), this.search_results.bind("mouseup.chosen", function(t) {
                    e.search_results_mouseup(t)
                }), this.search_results.bind("mouseover.chosen", function(t) {
                    e.search_results_mouseover(t)
                }), this.search_results.bind("mouseout.chosen", function(t) {
                    e.search_results_mouseout(t)
                }), this.search_results.bind("mousewheel.chosen DOMMouseScroll.chosen", function(t) {
                    e.search_results_mousewheel(t)
                }), this.search_results.bind("touchstart.chosen", function(t) {
                    e.search_results_touchstart(t)
                }), this.search_results.bind("touchmove.chosen", function(t) {
                    e.search_results_touchmove(t)
                }), this.search_results.bind("touchend.chosen", function(t) {
                    e.search_results_touchend(t)
                }), this.form_field_jq.bind("chosen:updated.chosen", function(t) {
                    e.results_update_field(t)
                }), this.form_field_jq.bind("chosen:activate.chosen", function(t) {
                    e.activate_field(t)
                }), this.form_field_jq.bind("chosen:open.chosen", function(t) {
                    e.container_mousedown(t)
                }), this.form_field_jq.bind("chosen:close.chosen", function(t) {
                    e.input_blur(t)
                }), this.search_field.bind("blur.chosen", function(t) {
                    e.input_blur(t)
                }), this.search_field.bind("keyup.chosen", function(t) {
                    e.keyup_checker(t)
                }), this.search_field.bind("keydown.chosen", function(t) {
                    e.keydown_checker(t)
                }), this.search_field.bind("focus.chosen", function(t) {
                    e.input_focus(t)
                }), this.search_field.bind("cut.chosen", function(t) {
                    e.clipboard_event_checker(t)
                }), this.search_field.bind("paste.chosen", function(t) {
                    e.clipboard_event_checker(t)
                }), this.is_multiple ? this.search_choices.bind("click.chosen", function(t) {
                    e.choices_click(t)
                }) : this.container.bind("click.chosen", function(e) {
                    e.preventDefault()
                })
            }, n.prototype.destroy = function() {
                return e(this.container[0].ownerDocument).unbind("click.chosen", this.click_test_action), this.search_field[0].tabIndex && (this.form_field_jq[0].tabIndex = this.search_field[0].tabIndex), this.container.remove(), this.form_field_jq.removeData("chosen"), this.form_field_jq.show()
            }, n.prototype.search_field_disabled = function() {
                return this.is_disabled = this.form_field_jq[0].disabled, this.is_disabled ? (this.container.addClass("chosen-disabled"), this.search_field[0].disabled = !0, this.is_multiple || this.selected_item.unbind("focus.chosen", this.activate_action), this.close_field()) : (this.container.removeClass("chosen-disabled"), this.search_field[0].disabled = !1, this.is_multiple ? void 0 : this.selected_item.bind("focus.chosen", this.activate_action))
            }, n.prototype.container_mousedown = function(t) {
                return this.is_disabled || (t && "mousedown" === t.type && !this.results_showing && t.preventDefault(), null != t && e(t.target).hasClass("search-choice-close")) ? void 0 : (this.active_field ? this.is_multiple || !t || e(t.target)[0] !== this.selected_item[0] && !e(t.target).parents("a.chosen-single").length || (t.preventDefault(), this.results_toggle()) : (this.is_multiple && this.search_field.val(""), e(this.container[0].ownerDocument).bind("click.chosen", this.click_test_action), this.results_show()), this.activate_field())
            }, n.prototype.container_mouseup = function(e) {
                return "ABBR" !== e.target.nodeName || this.is_disabled ? void 0 : this.results_reset(e)
            }, n.prototype.search_results_mousewheel = function(e) {
                var t;
                return e.originalEvent && (t = e.originalEvent.deltaY || -e.originalEvent.wheelDelta || e.originalEvent.detail), null != t ? (e.preventDefault(), "DOMMouseScroll" === e.type && (t = 40 * t), this.search_results.scrollTop(t + this.search_results.scrollTop())) : void 0
            }, n.prototype.blur_test = function() {
                return !this.active_field && this.container.hasClass("chosen-container-active") ? this.close_field() : void 0
            }, n.prototype.close_field = function() {
                return e(this.container[0].ownerDocument).unbind("click.chosen", this.click_test_action), this.active_field = !1, this.results_hide(), this.container.removeClass("chosen-container-active"), this.clear_backstroke(), this.show_search_field_default(), this.search_field_scale()
            }, n.prototype.activate_field = function() {
                return this.container.addClass("chosen-container-active"), this.active_field = !0, this.search_field.val(this.search_field.val()), this.search_field.focus()
            }, n.prototype.test_active_click = function(t) {
                var n;
                return n = e(t.target).closest(".chosen-container"), n.length && this.container[0] === n[0] ? this.active_field = !0 : this.close_field()
            }, n.prototype.results_build = function() {
                return this.parsing = !0, this.selected_option_count = null, this.results_data = i.select_to_array(this.form_field), this.is_multiple ? this.search_choices.find("li.search-choice").remove() : this.is_multiple || (this.single_set_selected_text(), this.disable_search || this.form_field.options.length <= this.disable_search_threshold ? (this.search_field[0].readOnly = !0, this.container.addClass("chosen-container-single-nosearch")) : (this.search_field[0].readOnly = !1, this.container.removeClass("chosen-container-single-nosearch"))), this.update_results_content(this.results_option_build({
                    first: !0
                })), this.search_field_disabled(), this.show_search_field_default(), this.search_field_scale(), this.parsing = !1
            }, n.prototype.result_do_highlight = function(e) {
                var t, n, i, s, r;
                if (e.length) {
                    if (this.result_clear_highlight(), this.result_highlight = e, this.result_highlight.addClass("highlighted"), i = parseInt(this.search_results.css("maxHeight"), 10), r = this.search_results.scrollTop(), s = i + r, n = this.result_highlight.position().top + this.search_results.scrollTop(), t = n + this.result_highlight.outerHeight(), t >= s) return this.search_results.scrollTop(t - i > 0 ? t - i : 0);
                    if (r > n) return this.search_results.scrollTop(n)
                }
            }, n.prototype.result_clear_highlight = function() {
                return this.result_highlight && this.result_highlight.removeClass("highlighted"), this.result_highlight = null
            }, n.prototype.results_show = function() {
                return this.is_multiple && this.max_selected_options <= this.choices_count() ? (this.form_field_jq.trigger("chosen:maxselected", {
                    chosen: this
                }), !1) : (this.container.addClass("chosen-with-drop"), this.results_showing = !0, this.search_field.focus(), this.search_field.val(this.search_field.val()), this.winnow_results(), this.form_field_jq.trigger("chosen:showing_dropdown", {
                    chosen: this
                }))
            }, n.prototype.update_results_content = function(e) {
                return this.search_results.html(e)
            }, n.prototype.results_hide = function() {
                return this.results_showing && (this.result_clear_highlight(), this.container.removeClass("chosen-with-drop"), this.form_field_jq.trigger("chosen:hiding_dropdown", {
                    chosen: this
                })), this.results_showing = !1
            }, n.prototype.set_tab_index = function() {
                var e;
                return this.form_field.tabIndex ? (e = this.form_field.tabIndex, this.form_field.tabIndex = -1, this.search_field[0].tabIndex = e) : void 0
            }, n.prototype.set_label_behavior = function() {
                var t = this;
                return this.form_field_label = this.form_field_jq.parents("label"), !this.form_field_label.length && this.form_field.id.length && (this.form_field_label = e("label[for='" + this.form_field.id + "']")), this.form_field_label.length > 0 ? this.form_field_label.bind("click.chosen", function(e) {
                    return t.is_multiple ? t.container_mousedown(e) : t.activate_field()
                }) : void 0
            }, n.prototype.show_search_field_default = function() {
                return this.is_multiple && this.choices_count() < 1 && !this.active_field ? (this.search_field.val(this.default_text), this.search_field.addClass("default")) : (this.search_field.val(""), this.search_field.removeClass("default"))
            }, n.prototype.search_results_mouseup = function(t) {
                var n;
                return n = e(t.target).hasClass("active-result") ? e(t.target) : e(t.target).parents(".active-result").first(), n.length ? (this.result_highlight = n, this.result_select(t), this.search_field.focus()) : void 0
            }, n.prototype.search_results_mouseover = function(t) {
                var n;
                return n = e(t.target).hasClass("active-result") ? e(t.target) : e(t.target).parents(".active-result").first(), n ? this.result_do_highlight(n) : void 0
            }, n.prototype.search_results_mouseout = function(t) {
                return e(t.target).hasClass("active-result") ? this.result_clear_highlight() : void 0
            }, n.prototype.choice_build = function(t) {
                var n, i, s = this;
                return n = e("<li />", {
                    "class": "search-choice"
                }).html("<span>" + t.html + "</span>"), t.disabled ? n.addClass("search-choice-disabled") : (i = e("<a />", {
                    "class": "search-choice-close",
                    "data-option-array-index": t.array_index
                }), i.bind("click.chosen", function(e) {
                    return s.choice_destroy_link_click(e)
                }), n.append(i)), this.search_container.before(n)
            }, n.prototype.choice_destroy_link_click = function(t) {
                return t.preventDefault(), t.stopPropagation(), this.is_disabled ? void 0 : this.choice_destroy(e(t.target))
            }, n.prototype.choice_destroy = function(e) {
                return this.result_deselect(e[0].getAttribute("data-option-array-index")) ? (this.show_search_field_default(), this.is_multiple && this.choices_count() > 0 && this.search_field.val().length < 1 && this.results_hide(), e.parents("li").first().remove(), this.search_field_scale()) : void 0
            }, n.prototype.results_reset = function() {
                return this.reset_single_select_options(), this.form_field.options[0].selected = !0, this.single_set_selected_text(), this.show_search_field_default(), this.results_reset_cleanup(), this.form_field_jq.trigger("change"), this.active_field ? this.results_hide() : void 0
            }, n.prototype.results_reset_cleanup = function() {
                return this.current_selectedIndex = this.form_field.selectedIndex, this.selected_item.find("abbr").remove()
            }, n.prototype.result_select = function(e) {
                var t, n;
                return this.result_highlight ? (t = this.result_highlight, this.result_clear_highlight(), this.is_multiple && this.max_selected_options <= this.choices_count() ? (this.form_field_jq.trigger("chosen:maxselected", {
                    chosen: this
                }), !1) : (this.is_multiple ? t.removeClass("active-result") : this.reset_single_select_options(), n = this.results_data[t[0].getAttribute("data-option-array-index")], n.selected = !0, this.form_field.options[n.options_index].selected = !0, this.selected_option_count = null, this.is_multiple ? this.choice_build(n) : this.single_set_selected_text(n.text), (e.metaKey || e.ctrlKey) && this.is_multiple || this.results_hide(), this.search_field.val(""), (this.is_multiple || this.form_field.selectedIndex !== this.current_selectedIndex) && this.form_field_jq.trigger("change", {
                    selected: this.form_field.options[n.options_index].value
                }), this.current_selectedIndex = this.form_field.selectedIndex, this.search_field_scale())) : void 0
            }, n.prototype.single_set_selected_text = function(e) {
                return null == e && (e = this.default_text), e === this.default_text ? this.selected_item.addClass("chosen-default") : (this.single_deselect_control_build(), this.selected_item.removeClass("chosen-default")), this.selected_item.find("span").text(e)
            }, n.prototype.result_deselect = function(e) {
                var t;
                return t = this.results_data[e], this.form_field.options[t.options_index].disabled ? !1 : (t.selected = !1, this.form_field.options[t.options_index].selected = !1, this.selected_option_count = null, this.result_clear_highlight(), this.results_showing && this.winnow_results(), this.form_field_jq.trigger("change", {
                    deselected: this.form_field.options[t.options_index].value
                }), this.search_field_scale(), !0)
            }, n.prototype.single_deselect_control_build = function() {
                return this.allow_single_deselect ? (this.selected_item.find("abbr").length || this.selected_item.find("span").first().after('<abbr class="search-choice-close"></abbr>'), this.selected_item.addClass("chosen-single-with-deselect")) : void 0
            }, n.prototype.get_search_text = function() {
                return this.search_field.val() === this.default_text ? "" : e("<div/>").text(e.trim(this.search_field.val())).html()
            }, n.prototype.winnow_results_set_highlight = function() {
                var e, t;
                return t = this.is_multiple ? [] : this.search_results.find(".result-selected.active-result"), e = t.length ? t.first() : this.search_results.find(".active-result").first(), null != e ? this.result_do_highlight(e) : void 0
            }, n.prototype.no_results = function(t) {
                var n;
                return n = e('<li class="no-results">' + this.results_none_found + ' "<span></span>"</li>'), n.find("span").first().html(t), this.search_results.append(n), this.form_field_jq.trigger("chosen:no_results", {
                    chosen: this
                })
            }, n.prototype.no_results_clear = function() {
                return this.search_results.find(".no-results").remove()
            }, n.prototype.keydown_arrow = function() {
                var e;
                return this.results_showing && this.result_highlight ? (e = this.result_highlight.nextAll("li.active-result").first()) ? this.result_do_highlight(e) : void 0 : this.results_show()
            }, n.prototype.keyup_arrow = function() {
                var e;
                return this.results_showing || this.is_multiple ? this.result_highlight ? (e = this.result_highlight.prevAll("li.active-result"), e.length ? this.result_do_highlight(e.first()) : (this.choices_count() > 0 && this.results_hide(), this.result_clear_highlight())) : void 0 : this.results_show()
            }, n.prototype.keydown_backstroke = function() {
                var e;
                return this.pending_backstroke ? (this.choice_destroy(this.pending_backstroke.find("a").first()), this.clear_backstroke()) : (e = this.search_container.siblings("li.search-choice").last(), e.length && !e.hasClass("search-choice-disabled") ? (this.pending_backstroke = e, this.single_backstroke_delete ? this.keydown_backstroke() : this.pending_backstroke.addClass("search-choice-focus")) : void 0)
            }, n.prototype.clear_backstroke = function() {
                return this.pending_backstroke && this.pending_backstroke.removeClass("search-choice-focus"), this.pending_backstroke = null
            }, n.prototype.keydown_checker = function(e) {
                var t, n;
                switch (t = null != (n = e.which) ? n : e.keyCode, this.search_field_scale(), 8 !== t && this.pending_backstroke && this.clear_backstroke(), t) {
                    case 8:
                        this.backstroke_length = this.search_field.val().length;
                        break;
                    case 9:
                        this.results_showing && !this.is_multiple && this.result_select(e), this.mouse_on_container = !1;
                        break;
                    case 13:
                        this.results_showing && e.preventDefault();
                        break;
                    case 32:
                        this.disable_search && e.preventDefault();
                        break;
                    case 38:
                        e.preventDefault(), this.keyup_arrow();
                        break;
                    case 40:
                        e.preventDefault(), this.keydown_arrow()
                }
            }, n.prototype.search_field_scale = function() {
                var t, n, i, s, r, o, a, l, c;
                if (this.is_multiple) {
                    for (i = 0, a = 0, r = "position:absolute; left: -1000px; top: -1000px; display:none;", o = ["font-size", "font-style", "font-weight", "font-family", "line-height", "text-transform", "letter-spacing"], l = 0, c = o.length; c > l; l++) s = o[l], r += s + ":" + this.search_field.css(s) + ";";
                    return t = e("<div />", {
                        style: r
                    }), t.text(this.search_field.val()), e("body").append(t), a = t.width() + 25, t.remove(), n = this.container.outerWidth(), a > n - 10 && (a = n - 10), this.search_field.css({
                        width: a + "px"
                    })
                }
            }, n
        }(t)
    }.call(this), define("/sites/default/themes/siedler/js/external/jquery.chosen.js", function() {}),
    function() {
        function e(e) {
            var t, n, i = "",
                s = String.fromCharCode;
            for (t = 0; n = e.charCodeAt(t); t++) 128 > n ? i += s(n) : n > 127 && 2048 > n ? (i += s(n >> 6 | 192), i += s(63 & n | 128)) : (i += s(n >> 12 | 224), i += s(n >> 6 & 63 | 128), i += s(63 & n | 128));
            return i
        }
        String.implement({
            toUTF8: function() {
                return e(this)
            }
        })
    }(), define("/sites/default/themes/common/js/external/string.utf8.js", function() {}),
    function() {
        function e(e) {
            for (var t = e.length, n = 16 * ((t + 8 - (t + 8) % 64) / 64 + 1), i = new Array, s = bytePosition = byteCount = 0; byteCount < t;) s = (byteCount - byteCount % 4) / 4, bytePosition = byteCount % 4 * 8, i[s] = i[s] | e.charCodeAt(byteCount) << bytePosition, byteCount++;
            return s = (byteCount - byteCount % 4) / 4, bytePosition = byteCount % 4 * 8, i[s] = i[s] | 128 << bytePosition, i[n - 2] = t << 3, i[n - 1] = t >>> 29, i
        }

        function t(e) {
            var t = temp = nibble = i = "";
            for (i = 0; i <= 3; i++) nibble = e >>> 8 * i & 255, temp = "0" + nibble.toString(16), t += temp.substr(temp.length - 2, 2);
            return t
        }

        function n(n) {
            for (var i, r, o, a, l = e(n.toUTF8()), c = 1732584193, u = 4023233417, d = 2562383102, h = 271733878, m = 7, f = 12, p = 17, g = 22, v = 5, y = 9, w = 14, b = 20, _ = 4, x = 11, E = 16, S = 23, C = 6, k = 10, j = 15, T = 21, P = 0; P < l.length; P += 16) i = c, r = u, o = d, a = h, c = s.compound("f", c, u, d, h, 3614090360, l[P + 0], m), h = s.compound("f", h, c, u, d, 3905402710, l[P + 1], f), d = s.compound("f", d, h, c, u, 606105819, l[P + 2], p), u = s.compound("f", u, d, h, c, 3250441966, l[P + 3], g), c = s.compound("f", c, u, d, h, 4118548399, l[P + 4], m), h = s.compound("f", h, c, u, d, 1200080426, l[P + 5], f), d = s.compound("f", d, h, c, u, 2821735955, l[P + 6], p), u = s.compound("f", u, d, h, c, 4249261313, l[P + 7], g), c = s.compound("f", c, u, d, h, 1770035416, l[P + 8], m), h = s.compound("f", h, c, u, d, 2336552879, l[P + 9], f), d = s.compound("f", d, h, c, u, 4294925233, l[P + 10], p), u = s.compound("f", u, d, h, c, 2304563134, l[P + 11], g), c = s.compound("f", c, u, d, h, 1804603682, l[P + 12], m), h = s.compound("f", h, c, u, d, 4254626195, l[P + 13], f), d = s.compound("f", d, h, c, u, 2792965006, l[P + 14], p), u = s.compound("f", u, d, h, c, 1236535329, l[P + 15], g), c = s.compound("g", c, u, d, h, 4129170786, l[P + 1], v), h = s.compound("g", h, c, u, d, 3225465664, l[P + 6], y), d = s.compound("g", d, h, c, u, 643717713, l[P + 11], w), u = s.compound("g", u, d, h, c, 3921069994, l[P + 0], b), c = s.compound("g", c, u, d, h, 3593408605, l[P + 5], v), h = s.compound("g", h, c, u, d, 38016083, l[P + 10], y), d = s.compound("g", d, h, c, u, 3634488961, l[P + 15], w), u = s.compound("g", u, d, h, c, 3889429448, l[P + 4], b), c = s.compound("g", c, u, d, h, 568446438, l[P + 9], v), h = s.compound("g", h, c, u, d, 3275163606, l[P + 14], y), d = s.compound("g", d, h, c, u, 4107603335, l[P + 3], w), u = s.compound("g", u, d, h, c, 1163531501, l[P + 8], b), c = s.compound("g", c, u, d, h, 2850285829, l[P + 13], v), h = s.compound("g", h, c, u, d, 4243563512, l[P + 2], y), d = s.compound("g", d, h, c, u, 1735328473, l[P + 7], w), u = s.compound("g", u, d, h, c, 2368359562, l[P + 12], b), c = s.compound("h", c, u, d, h, 4294588738, l[P + 5], _), h = s.compound("h", h, c, u, d, 2272392833, l[P + 8], x), d = s.compound("h", d, h, c, u, 1839030562, l[P + 11], E), u = s.compound("h", u, d, h, c, 4259657740, l[P + 14], S), c = s.compound("h", c, u, d, h, 2763975236, l[P + 1], _), h = s.compound("h", h, c, u, d, 1272893353, l[P + 4], x), d = s.compound("h", d, h, c, u, 4139469664, l[P + 7], E), u = s.compound("h", u, d, h, c, 3200236656, l[P + 10], S), c = s.compound("h", c, u, d, h, 681279174, l[P + 13], _), h = s.compound("h", h, c, u, d, 3936430074, l[P + 0], x), d = s.compound("h", d, h, c, u, 3572445317, l[P + 3], E), u = s.compound("h", u, d, h, c, 76029189, l[P + 6], S), c = s.compound("h", c, u, d, h, 3654602809, l[P + 9], _), h = s.compound("h", h, c, u, d, 3873151461, l[P + 12], x), d = s.compound("h", d, h, c, u, 530742520, l[P + 15], E), u = s.compound("h", u, d, h, c, 3299628645, l[P + 2], S), c = s.compound("i", c, u, d, h, 4096336452, l[P + 0], C), h = s.compound("i", h, c, u, d, 1126891415, l[P + 7], k), d = s.compound("i", d, h, c, u, 2878612391, l[P + 14], j), u = s.compound("i", u, d, h, c, 4237533241, l[P + 5], T), c = s.compound("i", c, u, d, h, 1700485571, l[P + 12], C), h = s.compound("i", h, c, u, d, 2399980690, l[P + 3], k), d = s.compound("i", d, h, c, u, 4293915773, l[P + 10], j), u = s.compound("i", u, d, h, c, 2240044497, l[P + 1], T), c = s.compound("i", c, u, d, h, 1873313359, l[P + 8], C), h = s.compound("i", h, c, u, d, 4264355552, l[P + 15], k), d = s.compound("i", d, h, c, u, 2734768916, l[P + 6], j), u = s.compound("i", u, d, h, c, 1309151649, l[P + 13], T), c = s.compound("i", c, u, d, h, 4149444226, l[P + 4], C), h = s.compound("i", h, c, u, d, 3174756917, l[P + 11], k), d = s.compound("i", d, h, c, u, 718787259, l[P + 2], j), u = s.compound("i", u, d, h, c, 3951481745, l[P + 9], T), c = s.addUnsigned(c, i), u = s.addUnsigned(u, r), d = s.addUnsigned(d, o), h = s.addUnsigned(h, a);
            return (t(c) + t(u) + t(d) + t(h)).toLowerCase()
        }
        var s = {
            f: function(e, t, n) {
                return e & t | ~e & n
            },
            g: function(e, t, n) {
                return e & n | t & ~n
            },
            h: function(e, t, n) {
                return e ^ t ^ n
            },
            i: function(e, t, n) {
                return t ^ (e | ~n)
            },
            rotateLeft: function(e, t) {
                return e << t | e >>> 32 - t
            },
            addUnsigned: function(e, t) {
                var n = 2147483648 & e,
                    i = 2147483648 & t,
                    s = 1073741824 & e,
                    r = 1073741824 & t,
                    o = (1073741823 & e) + (1073741823 & t);
                return s & r ? 2147483648 ^ o ^ n ^ i : s | r ? 1073741824 & o ? 3221225472 ^ o ^ n ^ i : 1073741824 ^ o ^ n ^ i : o ^ n ^ i
            },
            compound: function(e, t, n, i, r, o, a, l) {
                var c = s,
                    u = c.addUnsigned,
                    d = u(t, u(u(c[e](n, i, r), a), o));
                return u(c.rotateLeft(d, l), n)
            }
        };
        String.implement({
            toMD5: function() {
                return n(this)
            }
        })
    }(), define("/sites/default/themes/common/js/external/string.md5.js", function() {}), require([""], function() {
        gm_LocationHash = new Class({}).extend({
            getVars: function() {
                var e, t, n = {},
                    i = window.location.hash.substr("1").split("&");
                for (t = 0; t < i.length; t++) e = i[t].split("="), n[e[0]] = e[1];
                return n
            },
            updateVars: function(e) {
                var t = "",
                    n = "";
                for (var i in e) "" != i && "undefined" != typeof e[i] && (n = 0 == t ? "" : "&", t += n + i + "=" + e[i]);
                window.location.hash = t
            },
            getVar: function(e) {
                var t = gm_LocationHash.getVars();
                return e in t ? t[e] : null
            },
            setVar: function(e, t, n) {
                var i = {};
                n || (i = gm_LocationHash.getVars()), i[e] = t, gm_LocationHash.updateVars(i)
            },
            setVars: function(e, t) {
                var n = {};
                t || (n = gm_LocationHash.getVars());
                for (name in e) n[name] = e[name];
                gm_LocationHash.updateVars(n)
            },
            unsetVar: function(e) {
                var t = gm_LocationHash.getVars();
                e in t && (delete t[e], gm_LocationHash.updateVars(t))
            },
            unsetVars: function(e) {
                var t = gm_LocationHash.getVars();
                e.forEach(function(e) {
                    e in t && delete t[e]
                }), gm_LocationHash.updateVars(t)
            }
        })
    }), define("/sites/default/themes/common/js/common/location_hash.js", function() {}), Element.implement({
        getCssParams: function(e) {
            var t = [];
            return this.get("class").split(" ").each(function(n) {
                0 === n.indexOf(e + "-") && t.push(n.substr(e.length + 1))
            }), t
        },
        getCssParam: function(e) {
            var t = this.getCssParams(e);
            return t.length > 0 ? t[0] : null
        },
        getCssClasses: function() {
            return this.get("class").split(" ")
        }
    }), define("/sites/default/themes/common/js/common/element.js", function() {}), ImagePreloader = new Class({
        Implements: Events,
        _steps: null,
        _events: null,
        _preloadStarted: !1,
        initialize: function() {
            this._steps = new Hash, this._events = new Hash, this.addEvent("loadImages", function(e) {
                return this._events.has(e) ? (this._loadImages(this._events.get(e), e), void this._events.erase(e)) : void this.fireEvent(e)
            }.bind(this))
        },
        addImage: function(e, t, n, i, s) {
            if (this._preloadStarted) throw 'ImagePreloader.addImage() may not be called after .preload() which fires on "load" by default.';
            if ("undefined" == typeof e) throw "ImagePreloader.addImage(): src has to be defined.";
            if ("undefined" == typeof t && (t = "bg"), !["bg", "src", "none"].contains(t)) throw 'ImagePreloader.addImage(): type must be "bg", "src", or "none".';
            if (["bg", "src"].contains(t) && "undefined" == typeof i) throw 'ImagePreloader.addImage(): element has to be defined, if type is "bg" or "src".';
            "undefined" == typeof n && (n = 100);
            var r = {
                element: i,
                src: e,
                type: t,
                step: n
            };
            if (s) {
                this._events.has(s) || this._events.set(s, []);
                var o = this._events.get(s);
                o.push(r), this._events.set(s, o)
            } else {
                this._steps.has(n) || this._steps.set(n, []);
                var o = this._steps.get(n);
                o.push(r), this._steps.set(r.step, o)
            }
        },
        preload: function() {
            $$(".jsInstance-preload").each(function(e) {
                var t = e.getCssParam("jsPreload").split(";");
                if (t.length < 1) throw 'CSS-Param "jsPreload" must be used with at least one param (src)!';
                this.addImage(t[0], t.length > 1 && ["bg", "src", "none"].contains(t[1]) ? t[1] : "src", t.length > 2 && $type(t[2].toInt()) !== !1 ? t[2].toInt() : 100, e, e.getCssParam("jsPreloadEvent"))
            }.bind(this)), this.fireEvent("readyForEvents"), this._preloadStarted = !0;
            var e = this._steps.getKeys().sort(function(e, t) {
                return e.toInt() - t.toInt()
            });
            this._loadStep(0, e)
        },
        _loadStep: function(e, t) {
            if (!(e + 1 > t.length)) {
                var n = t[e],
                    i = this._steps.get(n),
                    s = "stepComplete" + e;
                this.addEvent(s, function(e) {
                    this._loadStep(e + 1, t)
                }.pass(e, this)), this._loadImages(i, s), this._steps.erase(n)
            }
        },
        _loadImages: function(e, t) {
            var n = [];
            e.each(function(e) {
                var t = e.src;
                if ("PATH/" == t.substr(0, 5) && (t = t.replace("PATH", ImagePreloader.defaultPath)), "src" == e.type) e.element.set("src", t);
                else if ("bg" == e.type) e.element.setStyle("background-image", "url(" + t + ")");
                else if (".gif" == t.substr(-4).toLowerCase()) {
                    var i = new Element("div", {
                        styles: {
                            "background-image": "url(" + t + ")",
                            width: 0,
                            height: 0,
                            visibility: "hidden"
                        }
                    });
                    return i.inject($("footer")), void i.destroy.delay(1, i)
                }
                n.push(t)
            }.bind(this)), 0 == n.length ? this.fireEvent(t) : new Asset.images(n, {
                onComplete: function() {
                    this.fireEvent(t)
                }.bind(this)
            })
        }
    }), ImagePreloader._instance = null, ImagePreloader.getInstance = function() {
        return null === ImagePreloader._instance && (ImagePreloader._instance = new ImagePreloader), ImagePreloader._instance
    }, ImagePreloader.disableImagePreloader = !1, window.addEvent("load", function() {
        ImagePreloader.defaultPath = websiteContentPath + "images", ImagePreloader.disableImagePreloader || ImagePreloader.getInstance().preload()
    }), define("/sites/default/themes/common/js/common/imagePreloader.js", function() {}), Cookie.implement({
        initialize: function(e, t) {
            this.key = e, this.setOptions(Object.merge({
                domain: cookieDomain,
                duration: 365
            }, t))
        }
    }), define("/sites/default/themes/common/js/common/cookie.js", function() {}), gm_CenterElement = new Class({
        _element: null,
        _tween: null,
        eventFunction: null,
        _width: null,
        _height: null,
        _half: null,
        _offset: null,
        initialize: function(e, t, n, i) {
            this._element = document.id(e), this._element.retrieve("isCentered") || (this._element.store("isCentered", !0), "number" !== typeOf(n) && (n = 0), this._offset = n, this._height = t || this._element.getHeight(), this._width = i || this._element.getWidth(), this._half = (this._height / 2).round() + this._offset, this._tween = new Fx.Tween(this._element, {
                property: "top",
                duration: 500,
                transition: Fx.Transitions.Quad.easeInOut
            }), this.eventFunction = this._centerPosition.bind(this), window.addEvent("scroll", this.eventFunction), window.addEvent("resize", this.eventFunction), this._element.addEvent("resize", this._calculateDimensions.bind(this)), this._element.setStyle("left", this._calculateLeft()), this._element.setStyle("top", this._calculateTop()))
        },
        _centerPosition: function() {
            try {
                if (!this._element || !this._element.getParent()) return window.removeEvent("scroll", this.eventFunction), void window.removeEvent("resize", this.eventFunction)
            } catch (e) {
                return window.removeEvent("scroll", this.eventFunction), void window.removeEvent("resize", this.eventFunction)
            }
            this._element.setStyle("left", this._calculateLeft()), this._tween.cancel(), this._height < window.getHeight() ? this._tween.start(this._element.getStyle("top").toInt(), this._calculateTop()) : this._element.setStyle("top", this._calculateTop())
        },
        _calculateLeft: function() {
            return (window.getSize().x / 2 - this._width / 2).round()
        },
        _calculateTop: function() {
            var e = window.getHeight(),
                t = (e / 2).round(),
                n = window.getScrollTop();
            if ($$("html").hasClass("spa") && "function" == typeof calculateTopForPaymentFrameInSpa) return calculateTopForPaymentFrameInSpa();
            if (this._height <= e) return n + t - this._half;
            if ("auto" !== this._element.getStyle("top")) var i = this._element.getStyle("top").toInt();
            else var i = n - this._offset;
            if (n > i) {
                if (n > i + this._height) return n + this._offset
            } else if (i > n && n < i - this._height) return n + this._offset;
            return n - this._offset
        },
        _calculateDimensions: function() {
            this._height = this._element.getHeight(), this._width = this._element.getWidth(), this._half = (this._height / 2).round() + this._offset, this._centerPosition()
        }
    }), define("/sites/default/themes/common/js/common/CenterElement.js", function() {}), gm_ResizeElement = new Class({
        Implements: [Options],
        _element: null,
        options: {
            maxWidth: null,
            maxHeight: null,
            minWidth: null,
            minHeight: null,
            border: 0
        },
        initialize: function(e, t) {
            this._element = document.id(e), this.setOptions(t), this._resize(), window.addEvent("resize", this._resize.bind(this))
        },
        _resize: function() {
            this._element.setStyles({
                width: this._calculate(this.options.maxWidth, this.options.minWidth, window.getWidth()),
                height: this._calculate(this.options.maxHeight, this.options.minHeight, window.getHeight())
            }), this._element.getParent().fireEvent("resize")
        },
        _calculate: function(e, t, n) {
            return n -= this.options.border, e >= n ? t > n ? t : n : e
        }
    }), define("/sites/default/themes/common/js/common/ResizeElement.js", function() {}), Game = new Class({
        data: null,
        initialize: function(e) {
            this.data = new Object, Array.each(["game_tso", "game_sho", "game_ao", "game_mmho", "game_pgo"], function(t) {
                var n = !1;
                e.hasClass(t) && (n = !0), this.data[t] = n
            }.bind(this))
        },
        isCurrentGame: function(e) {
            return this._getCurrentGame() === e
        },
        _getCurrentGame: function() {
            return Object.keyOf(this.data, !0)
        }
    }), Game._instance = null, Game.getInstance = function() {
        return null === Game._instance && (Game._instance = new Game(document.body)), Game._instance
    }, Game.isTso = function() {
        return Game.getInstance().isCurrentGame("game_tso")
    }, Game.isSho = function() {
        return Game.getInstance().isCurrentGame("game_sho")
    }, Game.isAo = function() {
        return Game.getInstance().isCurrentGame("game_ao")
    }, Game.isMmho = function() {
        return Game.getInstance().isCurrentGame("game_mmho")
    }, Game.isPgo = function() {
        return Game.getInstance().isCurrentGame("game_pgo")
    }, define("/sites/default/themes/common/js/common/game.js", function() {}), Language = new Class({
        language: null,
        url: null,
        data: null,
        initialize: function(e, t) {
            this.language = e;
            var n = new URI(window.location);
            this.url = n.get("scheme") + "://" + n.get("host") + "/" + t
        },
        getCurrentLanguageIdent: function() {
            return this.language
        },
        getCurrentUrl: function() {
            return this.url
        },
        getUrlByLanguageIdent: function(e) {
            if (e === this.language) return this.url;
            var t = null;
            if (document.id("languageSwitch")) {
                var n = document.id("languageSwitch").getElement("a." + e);
                n && (t = n.get("href"))
            }
            return t
        }
    }), Language._instance = null, Language.getInstance = function() {
        return null === Language._instance && (Language._instance = new Language(languageIdent, languagePath)), Language._instance
    }, Language.getCurrentLanguage = function() {
        return Language.getInstance().getCurrentLanguageIdent()
    }, Language.getCurrentUrl = function() {
        return Language.getInstance().getCurrentUrl()
    }, Language.getUrlByLanguageIdent = function(e) {
        return Language.getInstance().getUrlByLanguageIdent(e)
    }, Language.redirectByLanguageIdent = function(e) {
        var t = Language.getInstance().getUrlByLanguageIdent(e);
        return t && e !== Language.getCurrentLanguage() ? (new URI(t).go(), !0) : !1
    }, define("/sites/default/themes/common/js/common/language.js", function() {}), Overlay = new Class({
        overlay: null,
        active: !1,
        initialize: function(e) {
            this.overlay = e, window.addEvent("resize", this._resizeOverlay.bind(this))
        },
        show: function() {
            this.active || (this.active = !0, this._resizeOverlay(), this.overlay.removeClass("hide"))
        },
        hide: function() {
            this.active = !1, this.overlay.addClass("hide")
        },
        _resizeOverlay: function() {
            this.active && (this.overlay.setStyle("width", window.getSize().x), this.overlay.setStyle("height", window.getSize().y), this.overlay.setStyle("width", window.getScrollWidth()), this.overlay.setStyle("height", window.getScrollHeight()))
        }
    }), Overlay._instance = null, Overlay.getInstance = function() {
        if (null === Overlay._instance) {
            var e = document.id("dialogOverlay");
            Game.isMmho() ? Overlay._instance = new OverlayTween(e) : Overlay._instance = new Overlay(e)
        }
        return Overlay._instance
    }, Overlay.show = function() {
        Overlay.getInstance().show()
    }, Overlay.hide = function() {
        Overlay.getInstance().hide()
    }, define("/sites/default/themes/common/js/common/overlay.js", function() {}), OverlayTween = new Class({
        Extends: Overlay,
        tween: null,
        duration: 500,
        timeoutId: null,
        initialize: function(e) {
            this.parent(e), this.overlay.setStyle("opacity", 0), Modernizr.csstransitions ? this.overlay.setStyle("transition", "opacity " + this.duration + "ms") : this.tween = new Fx.Tween(this.overlay, {
                duration: this.duration,
                link: "cancel",
                property: "opacity",
                onStart: function() {
                    this.overlay.removeClass("hide")
                }.bind(this),
                onComplete: function() {
                    this.active || this.overlay.addClass("hide")
                }.bind(this)
            })
        },
        show: function() {
            this.active || (this.active = !0, this._resizeOverlay(), Modernizr.csstransitions ? (clearTimeout(this.timeoutId), this.overlay.removeClass("hide"), this.timeoutId = function() {
                this.overlay.setStyle("opacity", 1)
            }.delay(1, this)) : this.tween.start(1))
        },
        hide: function() {
            this.active = !1, Modernizr.csstransitions ? (clearTimeout(this.timeoutId), this.overlay.setStyle("opacity", 0), this.timeoutId = function() {
                this.overlay.addClass("hide")
            }.delay(this.duration, this)) : this.tween.start(0)
        }
    }), define("/sites/default/themes/common/js/common/overlayTween.js", ["/sites/default/themes/common/js/common/overlay.js"], function() {}), require([], function() {
        gm_ARequest = new Class({
            commonOnSuccess: function(e) {
                "object" == typeof e ? this.processRequest(e.status, e.data) : this.failure()
            },
            processRequest: function(e, t) {
                if ("deprecated" === e.toLowerCase()) this.fireEvent("deprecated", [t]);
                else {
                    var n = "status" + e.toLowerCase().capitalize();
                    this.fireEvent(n, [t]).callChain()
                }
            },
            exception: function(e) {
                return "object" === typeOf(e) && "object" === typeOf(e.overlay) ? void this.dialog(e.overlay.title, e.overlay.text, e.overlay.dialogOptions || {}) : "object" === typeOf(e) && "string" === typeOf(e.title) && "string" === typeOf(e.text) ? void this.dialog(e.title, e.text, e.dialogOptions || {}) : ("object" == typeof e && (e = Object.toQueryString(e)), void(e ? alert(e) : this.failure()))
            },
            failure: function() {
                0 !== this.status ? this.dialog(commonErrorMessageTitle, commonErrorMessage) : this.cancel()
            },
            dialog: function(e, t, n) {
                var n = n || {};
                gm_DialogHandler.add(new gm_dialog_Plain(e, t, Object.append({
                    redirectOnClose: window.location
                }, n)))
            },
            commonInitialize: function(e) {
                ["onStatusException", "onStatusWarning", "onStatusFailed"].each(function(t) {
                    "undefined" == typeof e[t] && this.addEvent(t, this.exception.bind(this))
                }.bind(this)), this.addEvent("failure", this.failure.bind(this)), this.addEvent("error", this.failure.bind(this)), this.addEvent("statusUplaydown", this.exception.bind(this));
                var t = this.fireEvent.pass("anyError", this);
                return this.addEvents({
                    failure: t,
                    error: t,
                    statusWarning: t,
                    statusFailed: t,
                    statusException: t,
                    statusUplaydown: t
                }), e.onSuccess = this.commonOnSuccess, e
            },
            commonSend: function(e) {
                var t = typeOf(e);
                "null" === t ? e = {
                    data: {}
                } : "string" == t && (e = {
                    data: e
                });
                var n = typeOf(e.data);
                "string" == n ? e.data = e.data.parseQueryString() : "null" === n && (e.data = {}), e.url || (e.url = this.options.url), e.url = new URI(e.url).set("scheme", "https").set("port", "443").toString(), "/" === e.url.substr(-1) && (e.url = e.url.substr(0, e.url.length - 1)), e.data[this.options.requestCodeName] = window.reqPay;
                var i = (new URI).getData("origin");
                return i && (e.data.origin = i), e
            }
        }), gm_Request = new Class({
            Extends: Request.JSONP,
            Implements: gm_ARequest,
            options: {
                timeoutCustom: 15e3,
                requestCodeName: "requestCode"
            },
            timeoutTimer: null,
            initialize: function(e) {
                e.emulation || (e.emulation = !1), this.parent(this.commonInitialize(e))
            },
            send: function(e) {
                e = this.commonSend(e), e.data[jsonpParameter.httpmethod] = this.options.method, Cookie.read(jsonpSecureKeyCookieName) || Cookie.write(jsonpSecureKeyCookieName, jsonpSecureKey), e.data[jsonpSecureKeyCookieName] = Cookie.read(jsonpSecureKeyCookieName), e.callbackKey = jsonpParameter.callback, this.parent(e), this.options.timeoutCustom && (this.timeoutTimer = this.timeout.delay(this.options.timeoutCustom, this))
            },
            post: function(e) {
                this.options.method = "post", this.send({
                    data: e
                })
            },
            get: function(e) {
                this.options.method = "get", this.send({
                    data: e
                })
            },
            success: function() {
                clearTimeout(this.timeoutTimer), this.parent.pass(arguments, this)()
            }
        }), gm_CORSRequest = new Class({
            Extends: Request.JSON,
            Implements: gm_ARequest,
            initialize: function(e) {
                this.parent(this.commonInitialize(e)), this.options.cors = !0
            },
            send: function(e) {
                e = this.commonSend(e), this.parent(e)
            }
        })
    }), define("/sites/default/themes/common/js/common/request.js", function() {}), FormOverlay = new Class({
        initialize: function(e) {
            e.getElements(".overlay").each(function(e) {
                var t = e.getParent().getElement("input");
                "" !== t.value && e.addClass("hide"), e.addEvent("click", function() {
                    t.focus()
                }), t.addEvent("focus", function() {
                    e.addClass("hide")
                }), t.addEvent("blur", function() {
                    "" === t.value && e.removeClass("hide")
                })
            })
        }
    }), define("/sites/default/themes/common/js/common/formOverlay.js", function() {}), gm_DialogHandler = new Class({
        queue: [],
        inProgress: !1,
        currentDisplay: !1,
        _add: function(e) {
            this.queue.push(e), this.showNext()
        },
        showNext: function() {
            if (this.inProgress && !this.currentDisplay && 0 !== this.queue.length) {
                this.currentDisplay = !0;
                var e = this.queue.shift();
                e.addEvent("close", this._completes.bind(this)), e.init()
            }
        },
        _completes: function() {
            this.currentDisplay = !1, this.showNext()
        },
        _start: function() {
            this.inProgress = !0, this.showNext()
        }
    }), gm_DialogHandler._instance = null, gm_DialogHandler.getInstance = function() {
        return null === gm_DialogHandler._instance && (gm_DialogHandler._instance = new gm_DialogHandler), gm_DialogHandler._instance
    }, gm_DialogHandler.start = function() {
        gm_DialogHandler.getInstance()._start()
    }, gm_DialogHandler.add = function(e) {
        gm_DialogHandler.getInstance()._add(e)
    }, define("/sites/default/themes/common/js/common/DialogHandler.js", function() {}), require(["/sites/default/themes/common/js/common/formOverlay.js", "/sites/default/themes/common/js/common/request.js", "/sites/default/themes/common/js/common/DialogHandler.js"], function() {
        InputValidator.implement({
            currentErrorTitle: "",
            currentErrorText: "",
            getError: function(e, t) {
                var n = this.currentErrorTitle,
                    i = this.currentErrorText;
                if (!this.currentErrorTitle) {
                    var s = e.className.split(" ").filter(function(e) {
                        return 0 === e.indexOf("errorTitle")
                    }).getLast();
                    if (s) {
                        n = window[s.split("-")[1]];
                        var r = e.className.split(" ").filter(function(e) {
                            return 0 === e.indexOf("errorText")
                        }).getLast();
                        i = window[r.split("-")[1]]
                    } else {
                        var o = this.options.errorMsg;
                        if ("function" == typeOf(o)) {
                            var a = o(e, t || this.getProps(e));
                            "array" === $type(a) && 2 === a.length && (n = a[0], i = a[1])
                        }
                    }
                }
                return this._buildErrorElements(n, i)
            },
            _buildErrorElements: function(e, t) {
                return "<h6>" + e + "</h6><p>" + t + "</p>"
            },
            setError: function(e, t) {
                this.currentErrorTitle = e, this.currentErrorText = t
            }
        }), gm_Form = new Class({
            Extends: Form.Validator.Inline,
            Implements: [Events],
            options: {
                errorPrefix: function() {
                    return ""
                },
                warningPrefix: function() {
                    return ""
                },
                scrollToErrorsOnSubmit: !1,
                evaluateFieldsOnBlur: !1,
                removeValuesAfterSuccess: !1,
                continueOnStatusFailed: !1,
                continueOnStatusOkay: !1,
                hideErrorOnKeyPress: !1,
                ignoreHidden: !1,
                fasterFieldsCheckingInterval: !1
            },
            event: null,
            fieldCount: null,
            fieldsPassed: null,
            fieldsPassedList: [],
            fieldsFailed: null,
            fieldsFailedList: [],
            request: null,
            button: null,
            validateType: null,
            allowSubmit: !0,
            oldFieldValues: {},
            initialize: function(e, t, n, i) {
                this.parent(e, t), "undefined" == typeof n && (n = {});
                var t = {
                    method: this.element.get("method") || "post"
                };
                Object.append(t, n), Browser.ie && !(Browser.version > 9) || Browser.opera || "undefined" == typeof t.useCORS || t.useCORS !== !0 ? this.request = new gm_Request(t) : (delete t.useCORS, this.request = new gm_CORSRequest(t)), this.options.continueOnStatusFailed && this.request.addEvent("statusFailed", this.reactivate.bind(this)), null !== e.get("class") && (this.validateType = e.get("class").replace(/^.*?validate-([a-z]+).*?$/, "$1")), (null === this.validateType || this.validateType === e.get("class")) && (this.validateType = ""), this.button = i, i && "undefined" != typeof i && i.addEvent("click", this.onSubmit.bind(this)), this.addEvent("submit", this.onSubmit.bind(this)), this.addEvent("elementPass", this.removeErrorClass), this.addEvent("elementFail", this.addErrorClass), this.addEvent("elementPass", function(e) {
                    this.validateCatcher(!0, e)
                }), this.addEvent("elementFail", function(e) {
                    this.validateCatcher(!1, e)
                }), this.options.removeValuesAfterSuccess && this.request.addEvent("statusOkay", this.removeValuesFromField.bind(this)), this.options.continueOnStatusOkay && this.request.addEvent("statusOkay", this.reactivate.bind(this)), e.getElements("fieldset a.help").each(function(e) {
                    e.addEvent("click", function(t) {
                        return t.stop(), gm_DialogHandler.add(new gm_dialog_Plain(e.getChildren()[0].get("html"), e.getChildren()[1].get("html"))), !1
                    })
                }), e.getElement("input[type=submit]") || new Element("input", {
                    type: "submit"
                }).setStyles({
                    display: "block",
                    height: 0,
                    width: 0,
                    position: "absolute",
                    top: 0,
                    left: 0,
                    border: "none",
                    background: "none"
                }).inject(e), this.addEvent("formValidate", function(e) {
                    e || this.reactivate()
                }.bind(this)), this.fields.each(function(e) {
                    if (formLiveValidator) {
                        var t = this.options.fasterFieldsCheckingInterval ? ":pause(100)" : ":pause(1000)";
                        this.oldFieldValues[this.getFieldId(e)] = this.getFieldValue(e), this.addEvent("validateAjaxField-" + this.getFieldId(e) + t, function() {
                            var t = this.getFieldValue(e);
                            t !== this.oldFieldValues[this.getFieldId(e)] && this.allowSubmit && this.testAjax([e])
                        }.bind(this))
                    }
                    e.addEvents({
                        focus: function() {
                            e.getParent("fieldset").addClass("focus")
                        }.bind(this),
                        blur: function() {
                            e.getParent("fieldset").removeClass("focus"), e.getParent("fieldset").removeClass("advice")
                        }.bind(this)
                    });
                    var n = e.getSiblings(".hint").getLast();
                    if (n) {
                        if (2 === n.getChildren("span").length) var i = n.getChildren("span")[0].get("html"),
                            s = n.getChildren("span")[1].get("html");
                        else var i = n.get("html"),
                            s = "";
                        var r = this.getValidators(e).combine(this.getAjaxValidators(e)).getLast();
                        r && (e.addEvent("focus", function() {
                            e.hasClass("validation-passed") || e.hasClass("validation-failed") || (this.getValidator(r).setError(i, s), this.fireEvent("elementValidate", [!1, e, r, !0]))
                        }.bind(this)), e.addEvent("blur", function() {
                            e.hasClass("validation-passed") || e.hasClass("validation-failed") || this.hideAdvice(r, e)
                        }.bind(this)))
                    }
                }, this)
            },
            removeValuesFromField: function() {
                this.getFields().each(function(e) {
                    if (e = document.id(e), e && !e.hasClass("skipreset")) switch (e.get("tag")) {
                        case "input":
                            var t = e.get("type");
                            if ("submit" === t) break;
                        case "textarea":
                            "radio" === t || "checkbox" === t ? e.set("checked", !1) : e.set("value", "");
                        case "select":
                            e.getElements("option").each(function(e, t) {
                                0 === t ? e.set("selected", "selected") : e.removeProperty("selected")
                            })
                    }
                })
            },
            test: function(e, t, n) {
                var i = this.parent(e, t, n);
                return this.getValidator(e) && !t.isVisible() && this.fireEvent("elementValidate", [i, t, e, n]), i
            },
            validationMonitor: function() {
                var e = arguments;
                e[1] = !0, this.parent.apply(this, e)
            },
            removeErrorClass: function(e) {
                e.addClass("validation-passed").removeClass("validation-failed"), e.getParent("fieldset").getChildren("div.validation-advice").some(function(e) {
                    return "block" === e.getStyle("display")
                }) || e.getParent("fieldset").removeClass("error").addClass("success")
            },
            addErrorClass: function(e, t, n) {
                e.addClass("validation-failed").removeClass("validation-passed"), e.getParent("fieldset").removeClass("success").addClass("error")
            },
            insertAdvice: function(e, t) {
                this.parent(e, t), e.addClass("message"), new Element("img", {
                    "class": "icon",
                    width: "36",
                    height: "38",
                    alt: "",
                    src: websiteContentPath + "images/blank.gif"
                }).inject(e), new Element("div", {
                    "class": "arrow"
                }).inject(e), new Element("div", {
                    "class": "footer"
                }).inject(e), e.inject(e.getParent("fieldset"), "bottom")
            },
            makeAdvice: function(e, t, n, i) {
                return advice = this.parent(e, t, n, i), i ? (advice.addClass("warning").removeClass("error"), t.getParent("fieldset").addClass("advice")) : (advice.addClass("error").removeClass("warning"), t.getParent("fieldset").removeClass("advice")), advice
            },
            onSubmit: function(e) {
                this.fireEvent("onFormSubmit"), e && e.preventDefault(), this.allowSubmit && (this.allowSubmit = !1, this.fireEvent("busy"), this.element.addClass("busy"), this.validate(e))
            },
            reactivate: function() {
                this.allowSubmit = !0, this.fireEvent("idle"), this.element.removeClass("busy")
            },
            validate: function(e) {
                this.event = e, this.fieldsPassed = 0, this.fieldsFailed = 0, this.fieldsToCheck = 0;
                var t = [];
                this.getFields().each(function(e) {
                    Game.isMmho() && e.hasClass("prevalidation") && (e.getNext().hasClass("prevalidationAdvice") && e.getNext().remove(), e.addClass(e.getProperty("data-validationclass"))), this.hasValidators(e) && (this.fieldsToCheck++, t.push(e))
                }, this), 0 === t.length && this.formValidated();
                var n = [];
                t.each(function(e) {
                    this.validateField(e, !0, null, !1) && this.hasAjaxValidators(e) && n.include(e)
                }, this), this.testAjax(n)
            },
            validateCatcher: function(e, t) {
                this.event && (e ? (this.fieldsPassed++, this.fieldsPassedList.push(t)) : (this.fieldsFailed++, this.fieldsFailedList.push(t)), this.fieldsPassed + this.fieldsFailed === this.fieldsToCheck && this.formValidated())
            },
            formValidated: function() {
                this.fireEvent("formValidate", [0 === this.fieldsFailed, this.element, this.event, this.fieldsPassedList, this.fieldsFailedList]), this.event = null, 0 === this.fieldsFailed && (this.button && this.button.getElement("span.error") && this.button.getElement("span.error").destroy(), this.request.send({
                    data: this.getFormData(),
                    url: "/" + languagePath + "/" + this.element.get("action")
                }), this.reset())
            },
            getFormData: function() {
                return this.element.toQueryString().trim()
            },
            validateField: function(e, t, n, i) {
                if ("undefined" == typeof i && (i = !0), this.paused) return !0;
                if (e = document.id(e)) {
                    var s, r;
                    !e.hasClass("validation-failed");
                    if (this.options.serial && !t && (s = this.element.getElement(".validation-failed"), r = this.element.getElement(".warning")), e && (!s || t || e.hasClass("validation-failed") || s && !this.options.serial) && this.hasValidators(e)) {
                        var o = this.getValidators(e),
                            a = [];
                        if (o.each(function(t) {
                                this.test(t, e) || a.include(t)
                            }, this), 0 === a.length)
                            if (this.hasAjaxValidators(e)) {
                                if (!i) return !0;
                                this.fireEvent("validateAjaxField-" + this.getFieldId(e))
                            } else this.fireEvent("elementPass", [e]);
                        else this.hasAjaxValidators(e) && this.getAjaxValidators(e).each(function(t) {
                            this.hideAdvice(t, e)
                        }, this), this.fireEvent("elementFail", [e, a])
                    }
                }
            },
            testAjax: function(e) {
                if (0 !== e.length) {
                    var t = {};
                    useFastApiHost = !0, e.each(function(e) {
                        useFastApiHost && (useFastApiHost = e.hasClass("use-fast-api"));
                        var n = {};
                        this.getAjaxValidators(e).each(function(t) {
                            var i = this.getValidator(t);
                            "function" === $type(i.options.getValue) ? n[t] = i.options.getValue(e) : n[t] = this.getFieldValue(e)
                        }, this), t[this.getFieldId(e)] = n, this.oldFieldValues[this.getFieldId(e)] = this.getFieldValue(e)
                    }, this), host = "";
                    var n = new URI("/" + languagePath + "/api/validate/" + (this.validateType ? this.validateType + "/" : ""));
                    useFastApiHost && fastApiHost ? host = fastApiHost : apiHost && (host = apiHost), "" === host && (host = new URI, host = host.get("host")), n.set("host", host).set("scheme", "https").set("port", 443), new gm_Request({
                        url: n.toString(),
                        method: "post",
                        onStatusOkay: function(t) {
                            this.processAjaxResponse(e, t)
                        }.bind(this),
                        onStatusFailed: function(t) {
                            this.processAjaxResponse(e, t)
                        }.bind(this),
                        onStatusUplaydown: function(e) {
                            this.fireEvent("uplayDown")
                        }.bind(this),
                        busyOverlay: !1
                    }).send(Object.toQueryString(t))
                }
            },
            processAjaxResponse: function(e, t) {
                Object.each(t, function(t, n) {
                    var i, s = [],
                        r = e.some(function(e) {
                            return this.getFieldId(e) === n ? (i = e, !0) : void 0
                        }, this);
                    r && (Object.each(t, function(e, t) {
                        e.status !== !0 && s.include(t), this.getValidator(t).setError(e.title, e.text), this.fireEvent("elementValidate", [e.status === !0, i, t, e.warning === !0])
                    }, this), 0 === s.length ? this.fireEvent("elementPass", [i]) : this.fireEvent("elementFail", [i, s]))
                }, this)
            },
            hasValidators: function(e) {
                return e.className.split(" ").some(function(e) {
                    return this.getValidator(e)
                }, this)
            },
            getValidators: function(e) {
                var t = [];
                return e.className.split(" ").each(function(e) {
                    e && 0 !== e.indexOf("validate-ajax") && this.getValidator(e) && t.include(e)
                }, this), t
            },
            hasAjaxValidators: function(e) {
                return e.className.split(" ").some(function(e) {
                    return e && 0 === e.indexOf("validate-ajax") && this.getValidator(e)
                }, this)
            },
            getAjaxValidators: function(e) {
                var t = [];
                return e.className.split(" ").each(function(e) {
                    e && 0 === e.indexOf("validate-ajax") && this.getValidator(e) && t.include(e)
                }, this), t
            },
            getValidator: function(e) {
                var t = this.parent(e);
                return t || 0 !== e.indexOf("validate-ajax") || (t = new InputValidator(e, {}), this.validators[e.split(":")[0]] = t), t
            },
            getFieldValue: function(e) {
                var t = e.get("type");
                return "select" == e.get("tag") ? e.getSelected().map(function(e) {
                    return document.id(e).get("value") || document.id(e).get("text")
                })[0] : "radio" != t && "checkbox" != t || e.checked ? e.get("value") : null
            },
            watchFields: function(e) {
                this.parent(e), e.each(function(e) {
                    e.addEvent("keyup", function() {
                        this.options.hideErrorOnKeyPress && this.getAjaxValidators(e).combine(this.getValidators(e)).each(function(t) {
                            this.hideAdvice(t, e)
                        }, this), e.get("tag") && "" !== e.get("value") && this.validationMonitor(e, !1)
                    }.bind(this))
                }, this)
            }
        }), gm_Form_Html = new Class({
            Extends: gm_Form,
            formValidated: function() {
                this.fireEvent("formValidate", [0 === this.fieldsFailed, this.element, this.event]), this.event = null, 0 === this.fieldsFailed && (this.button && this.button.getElement("span.error") && this.button.getElement("span.error").destroy(), this.element.submit())
            }
        }), initFormValidators = function() {
            Form.Validator.addAllThese([
                ["validate-repeat", {
                    errorMsg: function() {
                        return [errorRepeatMessageTitle, errorRepeatMessageText]
                    },
                    test: function(e) {
                        return (parentField = e.getParent("fieldset")) && (previousParentField = parentField.getPrevious("fieldset")) && (previousField = previousParentField.getElement("input[type=password]")) ? "" !== e.get("value") && previousField.get("value") === e.get("value") : !1
                    }
                }],
                ["minLength", {
                    errorMsg: function(e) {
                        if (classNames = e.get("class"), 0 === e.get("value").length) {
                            var t = apiCommonNoTextTitle;
                            result = classNames.match(/.*validateText-minLength-0-title-([a-z_]*) ?/), result && 2 === result.length && (t = window[result[1]]);
                            var n = apiCommonNoTextText;
                            return result = classNames.match(/.*validateText-minLength-0-text-([a-z_]*) ?/), result && 2 === result.length && (n = window[result[1]]), [t, n]
                        }
                        var t = apiCommonTooShortTitle;
                        result = classNames.match(/.*validateText-minLength-title-([a-z_]*) ?/), result && 2 === result.length && (t = window[result[1]]);
                        var n = apiCommonTooShortText;
                        return result = classNames.match(/.*validateText-minLength-text-([a-z_]*) ?/), result && 2 === result.length && (n = window[result[1]]), [t, n]
                    },
                    test: function(e, t) {
                        return "null" != typeOf(t.minLength) ? e.get("value").length >= (t.minLength || 0) : !0
                    }
                }],
                ["required", {
                    errorMsg: function(e) {
                        classNames = e.get("class");
                        var t = apiCommonNoTextTitle;
                        result = classNames.match(/.*validateText-required-title-([a-z_]*) ?/), result && 2 === result.length && (t = window[result[1]]);
                        var n = apiCommonNoTextText;
                        return result = classNames.match(/.*validateText-required-text-([a-z_]*) ?/), result && 2 === result.length && (n = window[result[1]]), [t, n]
                    },
                    test: function(e) {
                        return !Form.Validator.getValidator("IsEmpty").test(e)
                    }
                }],
                ["validate-ajax-ubipassword", {
                    getValue: function(e) {
                        var t = e.getParent("form").getElement("input.validate-ajax-ubiname"),
                            n = e.get("value");
                        return t && (t = t.get("value")) && t.trim().length > 0 ? [n, t] : n
                    }
                }],
                ["validate-ajax-captcha", {
                    getValue: function(e) {
                        var t = e.get("value"),
                            n = e.getParent("form").getElement(" input.recaptcha_challenge_field");
                        return n ? [t, n.get("value")] : t
                    }
                }]
            ])
        }, window.addEvent("domready", function() {
            initFormValidators()
        })
    }), define("/sites/default/themes/common/js/common/form.js", ["/sites/default/themes/common/js/common/request.js"], function() {}), require(["/sites/default/themes/common/js/common/form.js"], function() {
        LoginCommon = new Class({
            form: null,
            loginFormButtonMessage: null,
            username: null,
            password: null,
            reset: null,
            resetRequest: null,
            initialize: function(e, t, n) {
                new FormOverlay(e), this.loginFormButtonMessage = e.getElement(".loginFormButtonMessage"), this.username = e.getElement("input[name=name]"), this.password = e.getElement("input[name=password]"), this.reset = e.getParent().getElement(".password"), this.form = new gm_Form(e, {
                    continueOnStatusFailed: !0
                }, {
                    useCORS: !0,
                    onStatusOkay: function(e) {
                        new LoginRedirect(e)
                    },
                    onComplete: function(e) {
                        if (void 0 !== e.data)
                            if (void 0 !== e.data.captcha) {
                                if (gm_Captcha.create(e.data.captcha), !Login.getInstance().isShown) {
                                    var t = document.id("login");
                                    if (t && $("login").getElement(".contentBox")) {
                                        var n = $("login").getElement(".contentBox").getPosition();
                                        n.y -= 80, n.x += 100
                                    } else var n = {
                                        y: 0,
                                        x: 0
                                    };
                                    Login.show({
                                        page: n
                                    }, "left"), Login.getInstance().form.request.fireEvent("statusFailed", e.data)
                                }
                            } else void 0 !== e.data.removeCatpcha && gm_Captcha.destroy()
                    },
                    onStatusFailed: function(e) {
                        this._displayErrorMessage(e.title, e.text)
                    }.bind(this),
                    onRequest: function() {
                        this._hideErrorMessage()
                    }.bind(this)
                }, t), new Checkbox(n), this.reset.addEvent("click", function() {
                    return this.reset.hasAttribute("id") ? (this._dialog = new gm_dialog_Plain("Forgotten password", "If you want to reset your password for your tsotesting3 account, please contact TSOcombat@ubisoft.com."), this._dialog.addEvent("okay", this._dialog.close.bind(this._dialog)), gm_DialogHandler.add(this._dialog), !1) : ("" != this.username.value ? (this._dialog = new gm_dialog_Plain(passwordResetDialogTitle, passwordResetDialogMessage, {
                        hasCancelButton: !0
                    }), this._dialog.addEvent("okay", this._resetPassword.bind(this)), this._dialog.addEvent("cancel", this._dialog.close.bind(this._dialog))) : (this._dialog = new gm_dialog_Plain(passwordResetEmptyDialogTitle, passwordResetEmptyDialogMessage), this._dialog.addEvent("okay", this._dialog.close.bind(this._dialog))), gm_DialogHandler.add(this._dialog), !1)
                }.bind(this))
            },
            _displayErrorMessage: function(e, t) {
                this.loginFormButtonMessage.getChildren("h6").set("html", e), this.loginFormButtonMessage.getChildren("p").set("html", t), this.loginFormButtonMessage.removeClass("hide")
            },
            _hideErrorMessage: function() {
                this.loginFormButtonMessage.addClass("hide")
            },
            _resetPassword: function(e) {
                "string" !== typeOf(e) && (e = "resetPassword"), this.resetRequest = new gm_Request({
                    url: "/" + languagePath + "/api/user/" + e,
                    onStatusFailed: function(e) {
                        this._resetPasswordResultDialog(e)
                    }.bind(this),
                    onStatusOkay: function(e) {
                        this._resetPasswordResultDialog(e)
                    }.bind(this)
                }), this.resetRequest.post({
                    username: this.username.value
                })
            },
            _resetPasswordResultDialog: function(e) {
                var t = new gm_dialog_Plain(e.title, e.message);
                t.addEvent("okay", t.close.bind(t)), gm_DialogHandler.add(t)
            }
        }), Login = new Class({
            Extends: LoginCommon,
            _request: null,
            isShown: !1,
            _dialog: null,
            initialize: function() {
                $$("body.game_tso").length || $("login").setStyle("height", window.getSize().y), Browser.ie && this.show(), this.parent(document.id("loginForm"), document.id("loginFormButton"), document.id("rememberUser")), Browser.ie && this.hide(), $("login").getElement(".close").addEvent("click", function() {
                    return this.hide(), this.isShown = !1, !1
                }.bind(this))
            },
            _resetPassword: function() {
                if (document.id(document.body).hasClass("register") && $("loginForm").hasClass("nopartners")) var e = "uplayResetPassword";
                this.parent(e), this.resetRequest.addEvent("statusOkay", this.hide.bind(this))
            },
            show: function(e, t, n) {
                Cookie.write("testCookie", "test"), null === Cookie.read("testCookie") && gm_DialogHandler.add(new gm_dialog_Plain(logInCookieTitle, logInCookieText)), "right" != e && (e = "left"), "right" == e ? ($("loginForm").addClass("right"), $("loginForm").removeClass("left")) : ($("loginForm").addClass("left"), $("loginForm").removeClass("right")), t = t > 124 ? t - 124 : 0, n = n > 70 ? n - 70 : 0, $("login").getElement(".contentBox") && ($("login").getElement(".contentBox").setStyle("margin-left", t), $("login").getElement(".contentBox").setStyle("margin-top", n)), $("login").removeClass("hide");
                var i = JSON.decode(Cookie.read("knownUser"));
                i.status ? document.id("loginForm").getElement('input[name="password"]').focus() : document.id("loginUsername").focus()
            },
            hide: function() {
                $("login").addClass("hide")
            }
        }), Login.show = function(e, t) {
            Login.getInstance().show(t, e.page.x, e.page.y)
        }, Login.instance = null, Login.getInstance = function() {
            return null === Login.instance && (Login.instance = new Login), Login.instance
        };
        var e = function(e, t, n, i) {
            var s = screen.width / 2 - n / 2,
                r = screen.height / 2 - i / 2;
            return window.open(e, t, "toolbar=no, location=no, status=no, menubar=no, scrollbars=no, resizable=no, width=" + n + ", height=" + i + ", top=" + r + ", left=" + s)
        };
        window.addEvent("domready", function() {
            $$(".jsInstance-login").each(function(e) {
                e.addEvent("click", function(t) {
                    t.stop(), Login.show(t, e.getCssParam("jsLogin"))
                })
            });
            var t = document.id("loginFormBar");
            t && new LoginCommon(t, document.id("loginFormButtonBar"), document.id("rememberUserBar"));
            var n = window.uplayConnect || {};
            $("uplay_connect") && $("uplay_connect").addEvent("click", function(t) {
                e(n.uplayButtonUrl, "UplayConnect", 800, 600), t.preventDefault()
            }), $$(".btnNk") && $$(".btnNk").addEvent("click", function(t) {
                e(this.get("href"), "NkConnect", 480, 250), t.preventDefault()
            })
        })
    }), define("/sites/default/themes/common/js/common/login.js", ["/sites/default/themes/common/js/common/form.js"], function() {});
var LoginRedirect = new Class({
    reloadTimeout: 1e4,
    reloadTimeoutTimer: null,
    waitForDialog: !1,
    redirectUrl: null,
    requiredCatchesBeforeRedirect: 1,
    response: null,
    initialize: function(e) {
        this.response = e, this.waitForDialog = e.passwordUpdateRequired;
        var t = new Tracker({
            pid: e.pid
        });
        if (t.addEvent("complete", this.redirect.bind(this)), e.userid && t.track("special", "afterLogin", {
                userId: e.userid
            }), e.passwordUpdateRequired) {
            this.requiredCatchesBeforeRedirect++;
            var n = new gm_dialog_Plain(passwordUpdateRequiredTitle, passwordUpdateRequiredMessage, {
                hasCancelButton: !0,
                cancelButtonValue: passwordUpdateRequiredCancelButton,
                closeOnClose: !1
            });
            n.addEvent("okay", function() {
                this.redirectUrl = passwordUpdateRequiredOkURL, this.redirectNow()
            }.bind(this)), n.addEvent("cancel", this.redirectNow.bind(this)), gm_DialogHandler.add(n)
        } else this.reloadTimeoutTimer = this.redirect.delay(this.reloadTimeout, this)
    },
    redirect: function() {
        this.reloadTimeoutTimer && clearTimeout(this.reloadTimeoutTimer), this.waitForDialog || this.redirectNow()
    },
    redirectNow: function() {
        return null != this.redirectUrl ? void(document.location.href = this.redirectUrl) : void(Language.redirectByLanguageIdent(this.response.language) || window.location.reload())
    }
});
define("/sites/default/themes/common/js/common/LoginRedirect.js", function() {}), Checkbox = new Class({
    label: null,
    input: null,
    state: !1,
    initialize: function(e) {
        this.input = e, this.label = e.getNext(), this.label && "label" == this.label.get("tag") || (this.label = e.getParent().getElement("label")), this.label && (this.label.getElements("a").each(function(e) {
            e.addEvent("click", function(e) {
                e.stopPropagation()
            })
        }), this.label.addEvent("click", function(e) {
            return e.stop(), this._switchState(), this.input.fireEvent("change"), !1
        }.bind(this))), this.input && this.input.addEvent("change", function() {
            this._switchState(this.input.get("checked"))
        }.bind(this)), this.state = this.input.get("checked"), this._refreshByState()
    },
    _switchState: function(e) {
        if ("null" === typeOf(e)) this.state = !this.state;
        else {
            if (this.state === e) return;
            this.state = e
        }
        this._refreshByState()
    },
    _refreshByState: function() {
        this.state ? (this.input.set("checked", "checked"), this.label.addClass("checked")) : (this.input.removeProperty("checked"), this.label.removeClass("checked"))
    },
    refreshStateByInput: function() {
        this.state = this.input.get("checked"), this._refreshByState()
    }
}), define("/sites/default/themes/common/js/common/checkbox.js", function() {}), window.addEvent("domready", function() {
    $$("a._blank").each(function(e) {
        e.set("target", "_blank")
    })
}), define("/sites/default/themes/common/js/common/links.js", function() {}), window.addEvent("domready", function() {
    var e = $("shieldButton"),
        t = $("shieldButtonOverlay");
    e && t && e.hasClass("play") && t.addEvent("click", function() {
        new PlayNowLink
    }), $$(".playnowlink").addEvent("click", function(e) {
        return e.stop(), this.hasClass("inactive") || new PlayNowLink, !1
    })
}), PlayNowLink = new Class({
    initialize: function(e) {
        return gameserverUrl === playNowforwardUrl ? void new GameLink(e) : void gmOpenUrl(playNowforwardUrl)
    }
});
var gmOpenUrl = function(e) {
    return e ? partneriFrameOpenInNewWindow === !0 ? ($$("html.landingpage").length && location.reload(), void window.open(e, "Gameclient", "width=1222, height=800")) : void(partnerRemoveHeader === !0 ? window.open(e, "_top") : e.toURI().go()) : void 0
};
GameLink = new Class({
    initialize: function(e) {
        if ($defined(e) || (e = !1), swfobject.hasFlashPlayerVersion(requiredFlashVersion)) Cookie.read("dsoAuthToken") ? gmOpenUrl(playNowforwardUrl) : e ? window.location.href.toURI().go() : new gm_Request({
            url: "/" + languagePath + "/api/user/token",
            onComplete: function() {
                gmOpenUrl(playNowforwardUrl)
            }
        }).get();
        else {
            var t = noFlashPlayerDialogMessage + ' <a href="http://get.adobe.com/flashplayer/" class="event:flashInstall submit">' + noFlashPlayerDialogLink + "</a>",
                n = new gm_dialog_Plain(noFlashPlayerDialogTitle, t, {
                    icon: websiteContentPath + "images/common/dialog/icon/flash.png"
                });
            n.addEvent("flashInstall", this.installFlash.bind(this)), gm_DialogHandler.add(n)
        }
    },
    installFlash: function() {
        window.open("http://get.adobe.com/flashplayer/")
    }
}), define("/sites/default/themes/common/js/common/gamelink.js", function() {}), HoverButton = new Class({
    element: null,
    srcInactive: null,
    srcActive: null,
    initialize: function(e, t) {
        this.element = e, this._reInit(t), HoverButton.addInstance(this), ImagePreloader.getInstance().addImage(this.srcActive, "none", 75), e.addEvent("mouseover", this.mouseOver.bind(this)), e.addEvent("mouseout", this.mouseOut.bind(this))
    },
    mouseOver: function() {
        this.element.set("src", this.srcActive)
    },
    mouseOut: function() {
        this.element.set("src", this.srcInactive)
    },
    _reInit: function(e) {
        this.srcInactive = this.element.get("src"), e ? this.srcActive = e : this.srcActive = this.srcInactive.replace(/^(.+)\./, "$1_hover.")
    }
}), HoverButton.hoverButtons = [], HoverButton.addInstance = function(e) {
    HoverButton.hoverButtons.push(e)
}, HoverButton.reInit = function() {
    HoverButton.hoverButtons.each(function(e) {
        e._reInit()
    })
}, define("/sites/default/themes/common/js/common/hover.js", function() {}), window.addEvent("domready", function() {
    $("shieldButtonOverlay") && ($("shieldButtonOverlay").addEvent("mousedown", function(e) {
        $("shieldButton").addClass("down")
    }), $("shieldButtonOverlay").addEvent("mouseup", function(e) {
        $("shieldButton").removeClass("down"), $("shieldButtonOverlay").hasClass("noLogin") || $("shieldButton").fireEvent("click", e)
    }), $("shieldButtonOverlay").addEvent("mouseout", function(e) {
        $("shieldButton").removeClass("down"), $("shieldButton").removeClass("mouseover")
    }), $("shieldButtonOverlay").addEvent("mouseover", function(e) {
        $("shieldButton").addClass("mouseover")
    }))
}), define("/sites/default/themes/common/js/common/shield.js", function() {});
var Loop = new Class({
    loopCount: 0,
    isStopped: !0,
    isLooping: !1,
    loopMethod: $empty,
    setLoop: function(e, t) {
        if (this.isLooping) {
            this.stopLoop();
            var n = !0
        } else var n = !1;
        return this.loopMethod = e, this.loopDelay = t || 3e3, n && this.startLoop(), this
    },
    stopLoop: function() {
        return this.isStopped = !0, this.isLooping = !1, $clear(this.periodical), this
    },
    startLoop: function(e) {
        if (this.isStopped) {
            var e = e ? e : this.loopDelay;
            this.isStopped = !1, this.isLooping = !0, this.periodical = this.looper.periodical(e, this)
        }
        return this
    },
    resetLoop: function() {
        return this.loopCount = 0, this
    },
    looper: function() {
        return this.loopCount++, this.loopMethod(this.loopCount), this
    }
});
define("/sites/default/themes/common/js/external/Loop.js", function() {}), require(["/sites/default/themes/common/js/external/Loop.js"], function() {
        SlideShow = new Class({
            Implements: [Options, Events, Loop],
            options: {
                delay: 7e3,
                transition: "crossFade",
                duration: "500",
                autoplay: !1
            },
            initialize: function(e, t) {
                this.setOptions(t), this.setLoop(this.showNext, this.options.delay), this.element = document.id(e), this.slides = this.element.getChildren(), this.current = this.slides[0], this.transitioning = !1, this.setup(), this.options.autoplay && this.play()
            },
            setup: function() {
                return this.setupElement().setupSlides(!0), this
            },
            setupElement: function() {
                var e = this.element;
                return "absolute" != e.getStyle("position") && e != document.body && e.setStyle("position", "relative"), this
            },
            setupSlides: function(e) {
                return this.slides.each(function(t, n) {
                    this.storeTransition(t).reset(t), e && 0 != n && t.setStyle("display", "none")
                }, this), this
            },
            storeTransition: function(e) {
                var t = e.get("class"),
                    n = /transition:[a-zA-Z]+/,
                    i = /duration:[0-9]+/,
                    s = t.match(n) ? t.match(n)[0].split(":")[1] : this.options.transition,
                    r = t.match(i) ? t.match(i)[0].split(":")[1] : this.options.duration;
                return e.store("ssTransition", s).store("ssDuration", r), this
            },
            resetOptions: function(e) {
                return this.options = $merge(this.options, e), this.setupSlides(!1), this
            },
            getTransition: function(e) {
                return e.retrieve("ssTransition")
            },
            getDuration: function(e) {
                return e.retrieve("ssDuration")
            },
            show: function(e, t) {
                if (e = "number" == typeof e ? this.slides[e] : e, e != this.current && !this.transitioning) {
                    this.transitioning = !0;
                    var n = t && t.transition ? t.transition : this.getTransition(e),
                        i = t && t.duration ? t.duration : this.getDuration(e),
                        s = this.current.setStyle("z-index", 1),
                        r = this.reset(e),
                        o = {
                            previous: {
                                element: s,
                                index: this.slides.indexOf(s)
                            },
                            next: {
                                element: r,
                                index: this.slides.indexOf(r)
                            }
                        };
                    this.fireEvent("show", o), this.transitions[n](s, r, i, this),
                        function() {
                            s.setStyle("display", "none"), this.fireEvent("showComplete", o), this.transitioning = !1
                        }.bind(this).delay(i), this.current = r
                }
                return this
            },
            reset: function(e) {
                return e.setStyles({
                    position: "absolute",
                    "z-index": 0,
                    display: "block",
                    left: 0,
                    top: 0
                }).fade("show")
            },
            nextSlide: function() {
                var e = this.current.getNext();
                return e ? e : this.slides[0]
            },
            previousSlide: function() {
                var e = this.current.getPrevious();
                return e ? e : this.slides.getLast()
            },
            showNext: function(e) {
                return this.show(this.nextSlide(), e), this
            },
            showPrevious: function(e) {
                return this.show(this.previousSlide(), e), this
            },
            play: function() {
                return this.startLoop(), this.fireEvent("play"), this
            },
            pause: function() {
                return this.stopLoop(), this.fireEvent("pause"), this
            },
            reverse: function() {
                var e = this.loopMethod == this.showNext ? this.showPrevious : this.showNext;
                return this.setLoop(e, this.options.delay), this.fireEvent("reverse"), this
            },
            toElement: function() {
                return this.element
            }
        }), Element.Properties.slideshow = {
            set: function(e) {
                var t = this.retrieve("slideshow");
                return t && t.pause(), this.eliminate("slideshow").store("slideshow:options", e)
            },
            get: function(e) {
                return (e || !this.retrieve("slideshow")) && ((e || !this.retrieve("slideshow:options")) && this.set("slideshow", e), this.store("slideshow", new SlideShow(this, this.retrieve("slideshow:options")))), this.retrieve("slideshow")
            }
        }, Element.implement({
            playSlideShow: function(e) {
                return this.get("slideshow", e).play(), this
            },
            pauseSlideShow: function(e) {
                return this.get("slideshow", e).pause(), this
            }
        }), SlideShow.adders = {
            transitions: {},
            add: function(e, t) {
                this.transitions[e] = t, this.implement({
                    transitions: this.transitions
                })
            },
            addAllThese: function(e) {
                $A(e).each(function(e) {
                    this.add(e[0], e[1])
                }, this)
            }
        }, $extend(SlideShow, SlideShow.adders), SlideShow.implement(SlideShow.adders), SlideShow.add("fade", function(e, t, n, i) {
            return e.set("tween", {
                duration: n
            }).fade("out"), this
        }), SlideShow.addAllThese([
            ["none", function(e, t, n, i) {
                return e.setStyle("display", "none"), this
            }],
            ["crossFade", function(e, t, n, i) {
                return e.set("tween", {
                    duration: n
                }).fade("out"), t.set("tween", {
                    duration: n
                }).fade("in"), this
            }],
            ["fadeThroughBackground", function(e, t, n, i) {
                var s = n / 2;
                t.set("tween", {
                    duration: s
                }).fade("hide"), e.set("tween", {
                    duration: s,
                    onComplete: function() {
                        t.fade("in")
                    }
                }).fade("out")
            }],
            ["pushLeft", function(e, t, n, i) {
                var s = i.element.getSize().x;
                return t.setStyle("left", s), new Fx.Elements([e, t], {
                    duration: n
                }).start({
                    0: {
                        left: [-s]
                    },
                    1: {
                        left: [0]
                    }
                }), this
            }],
            ["pushRight", function(e, t, n, i) {
                var s = i.element.getSize().x;
                return t.setStyle("left", -s), new Fx.Elements([e, t], {
                    duration: n
                }).start({
                    0: {
                        left: [s]
                    },
                    1: {
                        left: [0]
                    }
                }), this
            }],
            ["pushUp", function(e, t, n, i) {
                var s = i.element.getSize().y;
                return t.setStyle("top", s), new Fx.Elements([e, t], {
                    duration: n
                }).start({
                    0: {
                        top: [-s]
                    },
                    1: {
                        top: [0]
                    }
                }), this
            }],
            ["pushDown", function(e, t, n, i) {
                var s = i.element.getSize().y;
                return t.setStyle("top", -s), new Fx.Elements([e, t], {
                    duration: n
                }).start({
                    0: {
                        top: [s]
                    },
                    1: {
                        top: [0]
                    }
                }), this
            }],
            ["blindRight", function(e, t, n, i) {
                var s = i.element.getSize().x;
                return t.setStyles({
                    left: -s,
                    "z-index": 2
                }).set("tween", {
                    duration: n
                }).tween("left", 0), this
            }],
            ["blindLeft", function(e, t, n, i) {
                var s = i.element.getSize().x;
                return t.setStyles({
                    left: s,
                    "z-index": 2
                }).set("tween", {
                    duration: n
                }).tween("left", 0), this
            }],
            ["blindUp", function(e, t, n, i) {
                var s = i.element.getSize().y;
                return t.setStyles({
                    top: s,
                    "z-index": 2
                }).set("tween", {
                    duration: n
                }).tween("top", 0), this
            }],
            ["blindDown", function(e, t, n, i) {
                var s = i.element.getSize().y;
                return t.setStyles({
                    top: -s,
                    "z-index": 2
                }).set("tween", {
                    duration: n
                }).tween("top", 0), this
            }],
            ["blindDownFade", function(e, t, n, i) {
                this.blindDown(e, t, n, i).fade(e, t, n, i)
            }],
            ["blindUpFade", function(e, t, n, i) {
                this.blindUp(e, t, n, i).fade(e, t, n, i)
            }],
            ["blindLeftFade", function(e, t, n, i) {
                this.blindLeft(e, t, n, i).fade(e, t, n, i)
            }],
            ["blindRightFade", function(e, t, n, i) {
                this.blindRight(e, t, n, i).fade(e, t, n, i)
            }]
        ])
    }), define("/sites/default/themes/common/js/external/SlideShow.js", function() {}), require(["/sites/default/themes/common/js/external/Loop.js", "/sites/default/themes/common/js/external/SlideShow.js"], function() {
        Gametour = new Class({
            _slider: null,
            _buttons: {
                _previous: null,
                _next: null,
                _numbers: null
            },
            _currentNumber: 1,
            _registerParent: null,
            _isFirstTime: !0,
            initialize: function() {
                this._registerParent = $("register").getParent(), SlideShow.add("blindRightSettlers", function(e, t, n, i) {
                    var s = i.element.getSize().x;
                    return t.setStyles({
                        left: -s
                    }).set("tween", {
                        duration: n
                    }).tween("left", 0), e.setStyles({
                        left: 0
                    }).set("tween", {
                        duration: n
                    }).tween("left", s), this
                }), SlideShow.add("blindLeftSettlers", function(e, t, n, i) {
                    var s = i.element.getSize().x;
                    return t.setStyles({
                        left: s
                    }).set("tween", {
                        duration: n
                    }).tween("left", 0), e.setStyles({
                        left: 0
                    }).set("tween", {
                        duration: n
                    }).tween("left", -s), this
                }), this._slider = new SlideShow("slides", {
                    duration: 200,
                    delay: 1
                });
                var e = $("pager").getElements("a");
                this._buttons._previous = e[0], this._buttons._next = e.getLast(), this._buttons._numbers = new Array;
                var t = 1;
                e.each(function(n) {
                    t > 1 && t != e.length && this._buttons._numbers.push(n), t++
                }.bind(this)), this._buttons._previous.setStyle("visibility", "hidden"), this._buttons._previous.addEvent("click", this._moveTo.bind(this, -1)), this._buttons._next.addEvent("click", this._moveTo.bind(this, 1)), $("slides").addEvent("click", this._moveTo.bind(this, 1)), this._buttons._numbers.each(function(e, t) {
                    ++t, e.addEvent("click", function() {
                        return this._activate(t), !1
                    }.bind(this))
                }.bind(this)), $("gametourClose").addEvent("click", function(e) {
                    return this.hide(), e.stop(), !1
                }.bind(this)), $("gametour").addEvent("click", function(e) {
                    e.target == $("gametour") && this.hide()
                }.bind(this)), this._getOption("closeAfterLastScreen") && $("slides").getLast("img").addEvent("click", this.hide)
            },
            _getOption: function(e) {
                if (void 0 === Gametour.options[e]) throw 'Unkown Gametour-Property "' + e + '"';
                return Gametour.options[e]
            },
            _moveTo: function(e) {
                return this._currentNumber + e <= this._buttons._numbers.length && this._currentNumber + e > 0 && this._activate(this._currentNumber + e), !1
            },
            _activate: function(e) {
                if (this._currentNumber !== e) {
                    if (this._buttons._numbers.each(function(t) {
                            var n = t.getElement("img"),
                                i = n.get("alt").toInt();
                            e === i ? n.set("src", websiteContentPath + "images/common/gametour/button/active.png") : n.set("src", websiteContentPath + "images/common/gametour/button/" + i + ".png")
                        }.bind(this)), 1 == e ? this._buttons._previous.setStyle("visibility", "hidden") : 1 == this._currentNumber && this._buttons._previous.setStyle("visibility", ""), e == this._buttons._numbers.length ? this._buttons._next.setStyle("visibility", "hidden") : this._currentNumber == this._buttons._numbers.length && this._buttons._next.setStyle("visibility", ""), $("number").set("class", ""), $("number").addClass("counter" + e), $("gametour").getElements(".text").each(function(t) {
                            t.hasClass("text" + e) ? t.removeClass("hide") : t.addClass("hide")
                        }), this._currentNumber < e) var t = "blindLeftSettlers";
                    else var t = "blindRightSettlers";
                    this._slider.show(e - 1, {
                        transition: t
                    }), this._currentNumber = e, this._getOption("showRegister") && $("register") && (this._buttons._numbers.length === e ? $("register").inject($("gametour").getElement(".contentBox")) : this._registerParent && $("register").inject(this._registerParent))
                }
            },
            show: function() {
                this._isFirstTime && (this._isFirstTime = !1, $("slides").getElements("img").each(function(e) {
                    e.set("src", e.get("class"))
                })), $("gametour").removeClass("hide"), this._getOption("showRegister") && $("register") && this._buttons._numbers.length === this._currentNumber && $("register").inject($("gametour").getElement(".contentBox"))
            },
            hide: function() {
                $("gametour").addClass("hide"), this._registerParent && $("register").inject(this._registerParent)
            }
        }), Gametour.instance = null, Gametour.options = {
            showRegister: !0,
            closeAfterLastScreen: !1
        }, Gametour.getInstance = function() {
            return null === Gametour.instance && (Gametour.instance = new Gametour), Gametour.instance
        }, Gametour.setOption = function(e, t) {
            if (void 0 === Gametour.options[e]) throw 'Unkown Gametour-Property "' + e + '"';
            Gametour.options[e] = t
        }, Gametour.setOptions = function(e) {
            for (i in e) Gametour.setOption(i, e[i])
        }
    }), define("/sites/default/themes/common/js/common/gametour.js", function() {}), gm_tracker_ABase = new Class({
        Implements: Events,
        values: [],
        vars: {},
        completed: 0,
        initialize: function(values, vars, evalstrs, customstrs) {
            this.values = values, this.vars = vars, this.completed = 0, this.values.each(function(value, i) {
                "string" === $type(value) && (evalstrs[i] && (Object.each(this.vars, function(e, t) {
                    evalstrs[i] = evalstrs[i].replace("%" + t + "%", e)
                }, this), evalstrs[i] = eval(evalstrs[i]), value = value.replace("%eval%", evalstrs[i])), Object.each(customstrs, function(e, t) {
                    this.vars["custom." + t] = e
                }, this), Object.each(this.vars, function(e, t) {
                    value = value.replace("%" + t + "%", e)
                }, this), this.values[i] = value)
            }, this)
        },
        track: function() {
            0 === this.values.length && this.fireEvent("complete")
        },
        _completes: function() {
            this.completed++, this.completed === this.values.length && this.fireEvent("complete")
        }
    }), define("/sites/default/themes/common/js/common/tracker/ABase.js", function() {}), gm_tracker_Iframe = new Class({
        Extends: gm_tracker_ABase,
        track: function() {
            this.parent(), 0 !== this.values.length && this.values.each(function(e) {
                new IFrame({
                    src: e,
                    styles: {
                        width: 1,
                        height: 1,
                        border: "none"
                    },
                    events: {
                        load: this._completes.bind(this)
                    }
                }).inject(document.body, "bottom")
            }, this)
        }
    }), define("/sites/default/themes/common/js/common/tracker/Iframe.js", ["/sites/default/themes/common/js/common/tracker/ABase.js"], function() {}), gm_tracker_Image = new Class({
        Extends: gm_tracker_ABase,
        track: function() {
            this.parent(), 0 !== this.values.length && Asset.images(this.values, {
                onComplete: function() {
                    this.fireEvent("complete")
                }.bind(this)
            })
        }
    }), define("/sites/default/themes/common/js/common/tracker/Image.js", ["/sites/default/themes/common/js/common/tracker/ABase.js"], function() {}), gm_tracker_Javascript = new Class({
        Extends: gm_tracker_ABase,
        track: function() {
            if (this.parent(), 0 !== this.values.length) {
                var e = this;
                this.values.each(function(t) {
                    Asset.javascript(t, {
                        onLoad: function() {
                            (function() {
                                this._completes()
                            }).delay(1e3, e)
                        }
                    })
                }, this)
            }
        }
    }), define("/sites/default/themes/common/js/common/tracker/Javascript.js", ["/sites/default/themes/common/js/common/tracker/ABase.js"], function() {}), gm_tracker_Function = new Class({
        Extends: gm_tracker_ABase,
        track: function() {
            this.parent(), 0 !== this.values.length && this.values.each(function(e) {
                e.pass(this.vars)(), this._completes()
            }, this)
        }
    }), define("/sites/default/themes/common/js/common/tracker/Function.js", ["/sites/default/themes/common/js/common/tracker/ABase.js"], function() {}), require(["/sites/default/themes/common/js/common/tracker/ABase.js", "/sites/default/themes/common/js/common/tracker/Iframe.js", "/sites/default/themes/common/js/common/tracker/Image.js", "/sites/default/themes/common/js/common/tracker/Javascript.js", "/sites/default/themes/common/js/common/tracker/Function.js"], function() {
        Tracker = new Class({
            Implements: Events,
            supportedTypes: ["function", "image", "javascript", "iframe"],
            completed: 0,
            initialize: function(e) {
                this.trackParams = {}, "object" === typeOf(e) && (this.trackParams = e), void 0 === this.trackParams.pid && (this.trackParams.pid = trackingPid), this.tracks = {}, "undefined" != typeof trackingData && (this.tracks = trackingData)
            },
            gatherTracks: function(e, t) {
                var n = [];
                return this.tracks && this.tracks[e] && this.tracks[e][t] && this.tracks[e][t][territoryIdent.toLowerCase()] && (this.tracks[e][t][territoryIdent.toLowerCase()][loggedInUserCountry.toLowerCase()] && n.append(this.tracks[e][t][territoryIdent.toLowerCase()][loggedInUserCountry.toLowerCase()]), this.tracks[e][t][territoryIdent.toLowerCase()].allCountries && n.append(this.tracks[e][t][territoryIdent.toLowerCase()].allCountries)), n.each(function(e, t) {
                    "string" === $type(e) ? e = {
                        type: "image",
                        value: e
                    } : "function" === $type(e) ? e = {
                        type: "function",
                        value: e
                    } : "object" === $type(e) && "string" === $type(e.value) && "string" === $type(e.type) && this.supportedTypes.contains(e.type) || "object" === $type(e) && "function" === $type(e.value) && "string" === $type(e.type) && "function" === e.type || (e = null), !e || void 0 == e.filter || "pid" !== e.filter.toLowerCase() && "nopid" !== e.filter.toLowerCase() || "nopid" === e.filter.toLowerCase() && null === this.trackParams.pid || "pid" === e.filter.toLowerCase() && void 0 != e.filterValue && void 0 != e.filterValue[this.trackParams.pid] || (e = null), n[t] = e
                }.bind(this)), n = n.clean()
            },
            trackMany: function(e, t, n) {
                var i = [];
                t.each(function(t) {
                    return "afterRegistrationFirstStep" === t && Cookie.write("firstStep", !0), "afterRegisterWithoutFirst" === t && Cookie.read("firstStep") ? void Cookie.dispose("firstStep") : void i.combine(this.gatherTracks(e, t))
                }.bind(this)), this.load(i, n)
            },
            track: function(e, t, n) {
                "array" !== $type(t) && (t = [t]), this.trackMany(e, t, n)
            },
            load: function(trackings, vars) {
                if (0 === trackings.length) return void this.fireEvent("complete");
                "object" !== $type(vars) && (vars = {}), vars.random = 1e17 * (Math.random() + ""), vars.date = (new Date).getTime(), vars.referer = escape(document.referrer), vars.currentUrl = escape(document.location.href);
                var types = {},
                    evalstrs = {};
                this.supportedTypes.each(function(e) {
                    types[e] = [], evalstrs[e] = []
                }), trackings.each(function(e) {
                    types[e.type].push(e.value), evalstrs[e.type].push(e.eval)
                }), this.completed = 0, Object.each(types, function(values, type) {
                    eval("var trackingObject = new gm_tracker_" + type.substr(0, 1).toUpperCase() + type.substr(1, type.length - 1) + "( values, vars, evalstrs[ type ], userCustomTrackingVariables );"), trackingObject.addEvent("complete", this._completes.bind(this)), trackingObject.track()
                }, this)
            },
            _completes: function() {
                this.completed++, this.completed === this.supportedTypes.length && this.fireEvent("complete")
            },
            trackCurrentPage: function(e) {
                trackingPageIdents.each(function(t) {
                    this.track("page", t, e)
                }, this)
            }
        })
    }), define("/sites/default/themes/common/js/common/tracker.js", function() {}), Dropdowns = new Class({
        _dropdowns: new Array,
        initialize: function() {
            $$(".dropdown").each(function(e) {
                var t = new Dropdown(e);
                t.addEvent("open", function() {
                    this._closeDropdowns(t)
                }.bind(this)), this._dropdowns.push(t)
            }.bind(this))
        },
        _closeDropdowns: function(e) {
            this._dropdowns.each(function(t) {
                t !== e && t.close()
            })
        },
        get: function(e) {
            e = document.id(e);
            var t = null;
            return this._dropdowns.each(function(n) {
                n._dropdown === e && (t = n)
            }), t
        }
    }), Dropdowns.instance = null, Dropdowns.getInstance = function() {
        return null === Dropdowns.instance && (Dropdowns.instance = new Dropdowns), Dropdowns.instance
    }, Dropdown = new Class({
        Implements: Events,
        _height: null,
        _dropdown: null,
        _fx: null,
        _scroll: null,
        initialize: function(e, t) {
            return this._dropdown = e, 0 == this._dropdown.getElements("ul").length ? !1 : (this._checkScroll(), this._height = this._dropdown.getElements("li").length * this._getLiHeight() + this._getBorderHeight(), Browser.ie7 ? this._dropdown.hasClass("open") || this._dropdown.getElement(".scrollContainer").addClass("hide") : this._fx = new Fx.Tween(this._dropdown.getElement(".scrollContainer"), {
                duration: 125
            }), this.initLabel(t || null), e.getElement("div").addEvent("click", function() {
                this._dropdown.hasClass("open") ? this.close() : this.open()
            }.bind(this)), void e.getElements("li[class!=inactive]").each(function(t) {
                t.hasClass("inactive") || t.addEvent("click", function() {
                    t.getElement("input") && (this._dropdown.getElement("div").fireEvent("click"), this._dropdown.getLast("input").get("value") !== t.getElement("input").get("value") && (e.getElements("li").each(function(e) {
                        e.removeClass("active")
                    }), t.addClass("active"), this._dropdown.getElement("div").set("html", t.get("html")), this._dropdown.getLast("input").set("value", t.getElement("input").get("value")), this._dropdown.getLast("input").fireEvent("change"), this.fireEvent("change", t.getElement("input").get("value"))))
                }.bind(this))
            }.bind(this)))
        },
        initLabel: function(e) {
            e && e.addEvent("click", function() {
                this._dropdown.getElement("div").fireEvent("click")
            }.bind(this))
        },
        close: function() {
            this._dropdown.hasClass("open") && (this._dropdown.removeClass("open"), this._dropdown.removeClass("z-index"), Browser.ie7 ? this._dropdown.getElement(".scrollContainer").addClass("hide") : this._fx.start("height", this._getHeightForAnimation(), 0), this.fireEvent("close"))
        },
        open: function() {
            this._dropdown.addClass("open"), this._dropdown.addClass("z-index"), Browser.ie7 ? (this._dropdown.getElement(".scrollContainer").removeClass("hide"), this._dropdown.getElement(".scrollContainer").setStyle("height", this._height)) : (0 == this._height && (this._height = this._dropdown.getElements("li").length * this._getLiHeight() + this._getBorderHeight()), this._fx.start("height", this._getHeightForAnimation(), this._height)), this.fireEvent("open")
        },
        _getHeightForAnimation: function() {
            return Browser.ie7 ? void 0 : (this._fx.cancel(), this._dropdown.getElement("ul").getHeight())
        },
        _checkScroll: function() {
            if (this._dropdown.getCssParam("scroll")) {
                var e = this._dropdown.getCssParam("scroll").toInt();
                if (this._dropdown.getElements("li").length > e) {
                    this._dropdown.addClass("scroll");
                    var t = this._getLiHeight() * e + this._getBorderHeight();
                    this._scroll = {
                        scrollItems: e,
                        maxHeight: t,
                        elementCount: this._dropdown.getElements("li").length
                    }, this._scroll.scrollSteps = this._scroll.elementCount - this._scroll.scrollItems, this._createSlider()
                }
            }
        },
        _getLiHeight: function() {
            var e = this._dropdown.getElement("li"),
                t = 0;
            return null != e && (t = e.measure(function() {
                return this.getHeight()
            }), t += e.getStyle("margin-top").toInt() - e.getStyle("margin-bottom").toInt()), t
        },
        _getBorderHeight: function() {
            var e = this._dropdown.getElementById("scrollContainerTop"),
                t = this._dropdown.getElementById("scrollContainerBottom"),
                n = 0;
            return e && (n += e.getStyle("height").toInt()), t && (n += t.getStyle("height").toInt()), n
        },
        _createSlider: function() {
            var e = this._dropdown.getElement(".scrollContainer");
            e.setStyle("max-height", this._scroll.maxHeight), this._scroll.slider = new Element("div", {
                "class": "slider scrollSlider",
                styles: {
                    height: this._scroll.maxHeight
                }
            }).inject(e), this._scroll.sliderBar = new Element("div", {
                "class": "bar"
            }).inject(this._scroll.slider);
            var t = this._dropdown.getElement("ul").getHeight().toInt();
            this._scroll.maxScroll = this._scroll.maxHeight - t;
            var n = new Slider(this._scroll.slider, this._scroll.sliderBar, {
                mode: "vertical",
                range: [0, this._scroll.scrollSteps],
                steps: this._scroll.scrollSteps,
                wheel: !0,
                onChange: function(e) {
                    var t = 0;
                    t = 0 === e ? e : e * this._getLiHeight() * -1 + this._getBorderHeight(), this._dropdown.getElement("ul").setStyle("margin-top", t), this._scroll.currentStep = e
                }.bind(this)
            });
            this._dropdown.getElement("ul").addEvent("mousewheel", function(e) {
                e.stop(), this._scroll.currentStep -= e.wheel, this._scroll.currentStep < 0 && (this._scroll.currentStep = 0), this._scroll.scrollSteps < this._scroll.currentStep && (this._scroll.currentStep = this._scroll.scrollSteps), n.set(this._scroll.currentStep)
            }.bind(this))
        }
    }), define("/sites/default/themes/common/js/common/dropdown.js", function() {}), Dialog = new Class({
        Implements: [Events, Options],
        container: null,
        waitOverlay: null,
        containerSize: null,
        tween: null,
        apiEventRequest: null,
        eventStack: [],
        clickIsInProgress: !1,
        options: {
            useApiEvents: !1,
            apiUrl: null,
            apiEventRequestParams: {}
        },
        initialize: function(e, t) {
            this.container = document.id(e), this.container.removeClass("hideWhileWaiting"), this.waitOverlay = document.id("waitingOverlay"), this.waitOverlay.addClass("hide"), this.setOptions(t), this.addEvent("close", this.close.bind(this))
        },
        _initApiEvent: function() {
            this.options.useApiEvents && (this.apiEventRequest = new gm_Request({
                url: "/" + languagePath + "/api/" + this.options.apiUrl + "/",
                onStatusOkay: this._apiEventCompletes.bind(this),
                onStatusWarning: function() {},
                onAnyError: this.fireEvent.pass("close", this),
                onRequest: this._showWaitOverlay.bind(this)
            }))
        },
        _showWaitOverlay: function() {
            this.container.addClass("hideWhileWaiting"), this.waitOverlay.removeClass("hide"), new gm_CenterElement(this.waitOverlay)
        },
        _initButtons: function() {
            this.container.getElements("*").each(function(e) {
                this._getElementHasEvents(e) && e.addEvent("click", function(t) {
                    t.stop(), this._handleClickEvent(t, e)
                }.bind(this))
            }.bind(this))
        },
        _getElementHasEvents: function(e) {
            return this._getEventsByElement(e).length > 0
        },
        _getEventsByElement: function(e) {
            var t = e.get("class");
            if (t) {
                var n = t.clean().split(" ").filter(function(e) {
                    return e.test("^event:[a-z0-9]+$", "i")
                });
                return n.each(function(e, t, n) {
                    n[t] = e.split(":")[1]
                }), n
            }
            return []
        },
        _handleClickEvent: function(e, t) {
            if (!this.clickIsInProgress) {
                this.clickIsInProgress = !0, this.eventStack.push([e, t, this._getEventsByElement(t)]);
                var n = this.eventStack.length - 1;
                this.options.useApiEvents ? (Object.append(this.options.apiEventRequestParams, {
                    clicked: t.get("id"),
                    events: this.eventStack[n][2],
                    eventNum: n
                }), this.apiEventRequest.post(this.options.apiEventRequestParams)) : (this._fireEventsForEventStack(n), this.clickIsInProgress = !1)
            }
        },
        _fireEventsForEventStack: function(e) {
            this.eventStack[e][2].each(function(t) {
                this.fireEvent(t, [this.eventStack[e][1], this.eventStack[e][2]])
            }, this)
        },
        _apiEventCompletes: function(e) {
            if ("object" === typeOf(e) && "number" === typeOf(e.eventNum)) {
                var t = this.eventStack[e.eventNum],
                    n = null;
                if ("a" === t[1].get("tag") && (t[1].hasClass("no-redirect") || (n = t[1].get("href").replace(/^#/, "").trim(), 0 === n.length && (n = null))), "object" === typeOf(e.overlay)) return gm_DialogHandler.add(new gm_dialog_Plain(e.overlay.title, e.overlay.text, {
                    redirectOnClose: n
                })), void this.fireEvent("close");
                if (null !== n) return void new URI(n).go();
                this._fireEventsForEventStack(e.eventNum)
            }
        },
        init: function() {
            this._initButtons(), this._initApiEvent(), this._show(), new gm_CenterElement(this.container)
        },
        _show: function() {
            this.container.removeClass("hide"), this.clickIsInProgress = !1, Overlay.show()
        },
        close: function() {
            this.container.addClass("hide"), this.waitOverlay.addClass("hide"), Overlay.hide(), this.container.getElements("*").each(function(e) {
                this._getElementHasEvents(e) && e.removeEvents("click");
            }.bind(this))
        }
    }), define("/sites/default/themes/common/js/common/dialog.js", function() {}), require(["/sites/default/themes/common/js/common/dialog.js"], function() {
        gm_dialog_Plain = new Class({
            Extends: Dialog,
            icon: null,
            title: "",
            message: "",
            options: {
                hasOkayButton: !0,
                hasCancelButton: !1,
                hasCloseButton: !1,
                cancelButtonValue: null,
                redirectOnClose: null,
                redirectOnOkay: null,
                redirectOnCancel: null,
                icon: !1,
                closeOnClose: !0
            },
            initialize: function(e, t, n) {
                this.parent(document.id("dialogContainer"), n), this.title = e, this.message = t
            },
            init: function() {
                this.options.hasOkayButton ? this.container.getElements("a.okay").removeClass("hide") : this.container.getElements("a.okay").addClass("hide"), this.options.hasCancelButton ? this.container.getElements("a.cancel").removeClass("hide") : this.container.getElements("a.cancel").addClass("hide"), this.options.hasOkayButton && this.options.hasCancelButton ? (this.container.getElements("a.okay").addClass("dualButtons"), this.container.getElements("a.cancel").addClass("dualButtons")) : (this.container.getElements("a.okay").removeClass("dualButtons"), this.container.getElements("a.cancel").removeClass("dualButtons")), this.options.hasCloseButton ? this.container.getElement("div.innerTop").addClass("closeable") : this.container.getElement("div.innerTop").removeClass("closeable"), this.icon = this.container.getElement("div.icon"), this.options.icon ? (this.container.addClass("icon"), this.icon.setStyle("background-image", "url(" + this.options.icon + ")")) : this.container.removeClass("icon"), null !== this.options.cancelButtonValue && this.container.getElements("a.cancel").set("html", this.options.cancelButtonValue), this.container.getElement("h1").set("html", this.title), this.container.getElement("p").set("html", this.message), this.options.redirectOnOkay && this.addEvent("okay", this._redirect.bind(this, this.options.redirectOnOkay)), this.options.redirectOnCancel && this.addEvent("cancel", this._redirect.bind(this, this.options.redirectOnCancel)), this.parent()
            },
            _redirect: function(e) {
                var t = new URI(e);
                t.toString() === document.location.href ? document.location.reload() : t.go()
            },
            close: function() {
                return null !== this.options.redirectOnClose ? void this._redirect(this.options.redirectOnClose) : void(this.options.closeOnClose && this.parent())
            }
        })
    }), define("/sites/default/themes/common/js/common/dialog/Plain.js", ["/sites/default/themes/common/js/common/dialog.js"], function() {}), require(["/sites/default/themes/common/js/common/dialog/Plain.js"], function() {
        gm_dialog_DoiRepeat = new Class({
            Extends: gm_dialog_Plain,
            options: {
                hasCloseButton: !0
            },
            redirectUrl: null,
            initialize: function(e, t, n) {
                this.parent(e, t), this.redirectUrl = n, this.addEvent("okay", this._redirect.bind(this)), this.addEvent("cancel", this.close.pass(!0, this))
            },
            _redirect: function() {
                var e = window.location.hash; - 1 !== e.indexOf("#") && $$(".communitypage.spa").length ? this.close(!0) : new URI(this.redirectUrl).go()
            },
            _show: function() {
                this.parent(), this.container.getElements(".innerCenter a").each(function(e) {
                    e.addEvent("click", function(e) {
                        return e.stop(), this._showWaitOverlay(), this.fireEvent("close"), new gm_Request({
                            url: "/" + languagePath + "/api/user/profile/repeatdoi",
                            onStatusOkay: function(e) {
                                var t = new gm_dialog_Plain(successDialog.repeatdoiForm.title, successDialog.repeatdoiForm.message, {
                                    redirectOnClose: this.redirectUrl
                                });
                                gm_DialogHandler.add(t)
                            }.bind(this)
                        }).post({
                            type: "repeatdoi"
                        }), !1
                    }.bind(this))
                }, this)
            },
            close: function(e) {
                "boolean" === typeOf(e) && this.parent()
            }
        })
    }), define("/sites/default/themes/common/js/common/dialog/DoiRepeat.js", ["/sites/default/themes/common/js/common/dialog/Plain.js"], function() {}), require(["/sites/default/themes/common/js/common/dialog.js"], function() {
        gm_message_BetterTerritoryLite = new Class({
            Extends: Dialog,
            container: null,
            options: {
                cookieName: null
            },
            initialize: function(e, t) {
                this.parent(e, t), this.addEvent("cookie", this._setCookie.bind(this))
            },
            _setCookie: function() {
                Cookie.write(this.options.cookieName, 1, {
                    duration: 500
                })
            }
        })
    }), define("/sites/default/themes/common/js/common/message/BetterTerritoryLite.js", function() {}), require(["/sites/default/themes/common/js/common/message/BetterTerritoryLite.js"], function() {
        gm_message_BetterTerritory = new Class({
            Extends: gm_message_BetterTerritoryLite,
            container: null,
            initialize: function(e, t) {
                this.parent(e, t), this.container.getElements(".flags a").each(function(e) {
                    e.addEvent("click", function(t) {
                        t.stop();
                        var n = this.container.getElements(".innerTop h1");
                        n = n.combine(this.container.getElements(".innerCenter p")), this._switcher(n, e.get("class"));
                        var i = this.container.getElements(".innerBottom a img");
                        this._switcher(i, e.get("class"))
                    }.bind(this)), e.addEvent("mouseenter", function(t) {
                        t.stop(), this._switcher(this.container.getElements(".flags span"), e.get("class"))
                    }.bind(this))
                }.bind(this))
            },
            _switcher: function(e, t) {
                e.each(function(e) {
                    e.hasClass(t) ? e.removeClass("hide") : e.addClass("hide")
                }.bind(this))
            }
        })
    }), define("/sites/default/themes/common/js/common/message/BetterTerritory.js", ["/sites/default/themes/common/js/common/message/BetterTerritoryLite.js"], function() {}), require(["/sites/default/themes/common/js/common/checkbox.js", "/sites/default/themes/common/js/common/dialog.js"], function() {
        gm_message_Terms = new Class({
            Extends: Dialog,
            checkboxes: [],
            initialize: function(e, t) {
                this.parent(e, t), this.container.getElements("fieldset").each(function(e) {
                    var t = e.getElement("input");
                    new Checkbox(t), this.checkboxes.push(t), e.getElement("label").addEvent("click", this._handleCheckbox.pass(e))
                }.bind(this))
            },
            _handleClickEvent: function(e, t) {
                var n = !1;
                t.hasClass("event:accepted") && this.checkboxes.each(function(e) {
                    e.checked || (e.getParent().getElement("div.validation-advice").removeClass("hide"), n = !0)
                }.bind(this)), n || this.parent(e, t)
            },
            _handleCheckbox: function(e) {
                var t = e.getElement("input"),
                    n = e.getElement("div.validation-advice");
                t.checked ? n.addClass("hide") : n.removeClass("hide")
            }
        })
    }), define("/sites/default/themes/common/js/common/message/Terms.js", function() {}), require([], function() {
        KongregateAPI = function() {
            this.kongregate = null, parent.kongregate && (this.kongregate = parent.kongregate), this.purchaseItems = function(e, t) {
                this.kongregate && this.kongregate.mtx.purchaseItems(e, t)
            }, this.purchaseItemsRemote = function(e, t) {
                this.kongregate && this.kongregate.mtx.purchaseItemsRemote(e, t)
            }
        }
    }), define("/sites/default/themes/common/js/common/kongregate.js", function() {}), gm_Captcha = new Class({
        recaptcha: null,
        shadow: null,
        initialize: function(e) {
            this.recaptcha = e, this.recaptcha._alias_finish_reload = e.finish_reload, this.recaptcha.finish_reload = function(e, t, n) {
                this.recaptcha._alias_finish_reload(e, t, n), this._sync()
            }.bind(this), this.container = document.id(recaptchaOptions.custom_theme_widget), this.shadow = document.id("recaptcha_image_shadow"), document.getElements(".recaptcha_reload").addEvent("click", function(e) {
                e.stop(), this.recaptcha.reload()
            }.bind(this)), document.getElements(".recaptcha_image").addEvent("click", function(e) {
                e.stop(), this.recaptcha.switch_type("image"), this.shadow.removeClass("hide")
            }.bind(this)), document.getElements(".recaptcha_audio").addEvent("click", function(e) {
                e.stop(), this.recaptcha.switch_type("audio"), this.shadow.addClass("hide")
            }.bind(this)), document.getElements(".recaptcha_help").addEvent("click", function(e) {
                e.stop(), this.recaptcha.showhelp()
            }.bind(this))
        },
        _create: function(e) {
            var t = Object.clone(recaptchaOptions);
            t.callback = this._sync, this.recaptcha.create(e, this.container, t)
        },
        _destroy: function() {
            this.recaptcha.destroy()
        },
        _reload: function() {
            this.recaptcha.reload()
        },
        _sync: function() {
            var e = document.id("recaptcha_image"),
                t = document.getElements(".recaptcha_image_sync");
            if (t.length > 0)
                for (var n = 0; n < t.length; n++) t[n].set("html", e.get("html"));
            var i = document.id("recaptcha_challenge_field"),
                s = document.getElements(".recaptcha_challenge_field");
            if (s.length > 0)
                for (var n = 0; n < s.length; n++) s[n].set("value", i.get("value"))
        }
    }), gm_Captcha._instance = null, gm_Captcha.getInstance = function() {
        return null === gm_Captcha._instance && (gm_Captcha._instance = new gm_Captcha(Recaptcha)), gm_Captcha._instance
    }, gm_Captcha.destroy = function() {
        gm_Captcha.getInstance()._destroy()
    }, gm_Captcha.create = function(e) {
        gm_Captcha.getInstance()._create(e)
    }, gm_Captcha.reload = function() {
        gm_Captcha.getInstance()._reload()
    }, define("/sites/default/themes/common/js/common/captcha.js", function() {}), require([], function() {
        gm_Debug = new Class({
            debug: null,
            modes: ["small", "normal", "big"],
            modeStyles: null,
            currentMode: null,
            tabs: null,
            tabMenu: null,
            activeTab: 0,
            initialize: function(e) {
                if (this.debug = e, this.tabs = this.debug.getChildren("div.tab"), this.iniModeStyles(), 0 === this.tabs.length) return this.debug.addEvent("click", function(e) {
                    e.stop(), this.debug.destroy()
                }.bind(this)), this.debug.getElement(".navi").set("text", "no"), void this.debug.setStyles(this.modeStyles.nodebug);
                window.addEvent("resize", function() {
                    this.iniModeStyles(), this.applyMode(this.currentMode), this.refreshTabHeight()
                }.bind(this)), this.tabMenu = this.debug.getElement("ul").getChildren();
                var t = null;
                this.tabMenu.each(function(e, n) {
                    e.addEvent("click", this.selectTab.pass(n, this)), Cookie.read("debugTab") === e.get("text") && (t = n)
                }, this), this._checkForCritical() || (this.applyMode(Cookie.read("debugMode") || this.modes[0]), null !== t ? this.selectTab(t) : this.selectTab(0)), this.debug.getElement(".navi").addEvent("click", function() {
                    this.applyMode(this.getNextMode())
                }.bind(this))
            },
            iniModeStyles: function() {
                this.modeStyles = {
                    normal: {
                        width: 350,
                        height: window.getSize().y - 2 * this.debug.getStyle("top").toInt(),
                        left: 4
                    },
                    big: {
                        width: window.getSize().x - 2 * this.debug.getStyle("left").toInt(),
                        height: window.getSize().y - 2 * this.debug.getStyle("top").toInt(),
                        left: null
                    },
                    small: {
                        width: 20,
                        height: 20,
                        left: 4
                    },
                    nodebug: {
                        width: 50,
                        height: 20
                    }
                }
            },
            refreshTabHeight: function() {
                this.activeTab.setStyle("height", this.debug.getStyle("height").toInt() - this.debug.getElement("div.navi").getSize().y - this.debug.getElement("div.menu").getSize().y)
            },
            selectTab: function(e) {
                this.tabMenu.removeClass("active"), this.tabs.addClass("hide"), this.activeTab = this.tabs[e], this.refreshTabHeight(), this.tabs[e].removeClass("hide"), this.tabMenu[e].addClass("active"), Cookie.write("debugTab", this.tabMenu[e].get("text"))
            },
            getNextMode: function() {
                var e = this.modes.indexOf(this.currentMode) + 1;
                return "undefined" != typeof this.modes[e] ? this.modes[e] : this.modes[0]
            },
            applyMode: function(e) {
                this.currentMode = e, Cookie.write("debugMode", this.currentMode), this.debug.setStyles(this.modeStyles[this.currentMode])
            },
            _checkForCritical: function() {
                var e = ["php_error"],
                    t = !1;
                return e.each(function(e) {
                    var n = this.debug.getElement("." + e);
                    n && (n.addClass("critical").fireEvent("click"), t = !0)
                }.bind(this)), t && this.applyMode("big"), t
            }
        }), gm_Debug.random = function(e) {
            for (var t = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "T", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"], n = t.length - 1, i = "", s = 0; e > s; s++) i += t[$random(0, n)];
            return i
        }, gm_Debug.selectDropdownElement = function(e, t) {
            var n = e.getParent(".dropdown");
            n && n.getElements(".scrollContainer li").each(function(e) {
                e.get("text").trim() === t && (e.fireEvent("click"), n.getChildren()[0].fireEvent("click"))
            })
        }, window.addEvent("domready", function() {
            $("debug") && (Browser.ie && Browser.ie9 || !Browser.ie ? (new gm_Debug(document.id("debug")), document.id("debug").getElement(".tab.locale_switch").getChildren().each(function(e) {
                e.addEvent("click", function() {
                    var t = e.getElement("p").get("text").toLowerCase();
                    $$("img").each(function(e) {
                        var n = e.get("src"),
                            i = n.replace(/(sites\/default\/themes\/(siedler|sho|anno)\/images\/.+?\/text\/)[a-z0-9]{2}-[a-z0-9]{2}(\/.+)$/, "$1" + t + "$3");
                        n != i && e.set("src", i)
                    }), HoverButton && HoverButton.reInit()
                })
            }), document.id("debug").getElement(".tab.helpers").getChildren().each(function(e) {
                var t = e.getElement("p").get("text").toLowerCase().split(":");
                e.getElement("p").set("text", t[1]);
                var n = t[0];
                e.addEvent("click", function() {
                    switch (n) {
                        case "fill":
                            ["register", "registerForm", "registerFormStep2", "registerFormStep3"].each(function(e) {
                                var t = document.id(e);
                                t && t.getElements("input,select").each(function(e) {
                                    var t = null;
                                    switch (e.get("id")) {
                                        case "email":
                                            e.set("value", "GG-" + gm_Debug.random(8) + "@dev.global-group.de");
                                            break;
                                        case "password":
                                        case "password2":
                                            e.set("value", "asdfg123");
                                            break;
                                        case "name":
                                        case "avatarname":
                                            e.set("value", "GG-" + gm_Debug.random(8));
                                            break;
                                        case "gameLanguage":
                                            gm_Debug.selectDropdownElement(e, "English");
                                            break;
                                        case "terms":
                                        case "privacy":
                                            e.set("checked", "checked"), e.getNext("label") && e.getNext("label").addClass("checked");
                                            break;
                                        case "avatar":
                                            e.getParent().getElement("span.smallAvatar") && e.getParent().getElement("span.smallAvatar").fireEvent("click");
                                            break;
                                        case "year":
                                            t = "1937";
                                        case "month":
                                            t || (t = "03");
                                        case "age":
                                            t || (t = "42");
                                        case "day":
                                            t || (t = "01"), "input" === e.get("tag") ? gm_Debug.selectDropdownElement(e, t) : (e.getElements("option").each(function(e) {
                                                e.get("text").trim() === t ? e.set("selected", "selected") : e.removeProperty("selected")
                                            }), e.fireEvent("change"))
                                    }
                                })
                            });
                            break;
                        case "knownuser":
                            Cookie.dispose("knownUser"), new URI("/").go();
                            break;
                        case "knownuserlogout":
                            Cookie.dispose("knownUser"), new URI("/logout").go()
                    }
                })
            })) : document.id("debug") && document.id("debug").destroy())
        })
    }), define("/sites/default/themes/common/js/debug.js", function() {}), Avatar = new Class({
        input: null,
        currentId: null,
        avatars: null,
        currentAvatar: null,
        preloaderReady: !1,
        preloaderWait: null,
        container: null,
        initialize: function(e, t) {
            this.input = e, this.container = t, void 0 !== this.container && (this.avatars = this.container.getChildren(), this.currentAvatar = this.avatars[0]), this.currentId = this.getValue(), void 0 !== this.container ? ImagePreloader.getInstance().addEvent("readyForEvents", function() {
                this.displayCurrentAvatar(), this.preloaderReady = !0
            }.bind(this)) : this.preloaderReady = !0
        },
        getValue: function() {
            return this.input.get("value")
        },
        setValue: function(e) {
            if (e !== this.getValue()) {
                if (!this.preloaderReady) return clearTimeout(this.preloaderWait), void(this.preloaderWait = this.setValue.pass(arguments, this).delay(250, this));
                this.currentId = e, this.input.set("value", e), void 0 !== this.container && this.displayCurrentAvatar()
            }
        },
        fade: function(e, t) {
            Browser.Engine.trident ? (t.addClass("hide"), e.removeClass("hide")) : (e.setStyle("opacity", 0), e.removeClass("hide"), e.fade("in"), t.fade("out")), t.removeClass("active"), e.addClass("active")
        },
        displayCurrentAvatar: function() {
            if (this.currentId) {
                var e = "avatar" + this.currentId,
                    t = document.id("avatarPicture" + this.currentId),
                    n = this.currentAvatar;
                this.currentAvatar = t, this.avatars.each(function(e, t, i) {
                    e !== this.currentAvatar && e !== n && (e.addClass("hide"), e.removeClass("active"))
                }), ImagePreloader.getInstance().addEvent(e, this.fade.pass([t, n], this)), ImagePreloader.getInstance().fireEvent("loadImages", e)
            }
        }
    }), AvatarSlider = new Class({
        Implements: [Events],
        slider: null,
        avatar: null,
        left: null,
        right: null,
        scroll: null,
        elements: null,
        currentPosition: null,
        slideRange: 3,
        maxPosition: 0,
        slideTask: null,
        initialize: function(e, t, n, i) {
            if (this.slider = e, this.avatar = t, this.left = Helper.isRTL() ? i : n, this.right = Helper.isRTL() ? n : i, this.elements = this.slider.getElements("td"), this.scroll = new Fx.Scroll(this.slider, {
                    link: "cancel",
                    wheelStops: !1
                }), this.currentElement = this.elements[0], this.maxPosition = this.elements.length - this.slideRange, this.avatar.getValue()) {
                var s = this._getPositionById(this.avatar.getValue());
                this.slideToPosition(s), this.elements[s].addClass("checked")
            } else this.slideToPosition(0);
            this.elements.each(function(e) {
                e.getElement("span.smallAvatar").addEvent("click", function() {
                    this.avatar.setValue(e.get("id").substr(6)), this.elements.each(function(e) {
                        e.removeClass("checked")
                    }), e.addClass("checked")
                }.bind(this))
            }, this), this.right.addEvents({
                mousedown: this.startSlideRight.bind(this),
                mouseup: this.stopSlide.bind(this)
            }), this.left.addEvents({
                mousedown: this.startSlideLeft.bind(this),
                mouseup: this.stopSlide.bind(this)
            }), this.slider.getParent("fieldset").addEvent("mousewheel", function(e) {
                e.stop(), e.wheel > 0 ? this.slideLeft() : this.slideRight()
            }.bind(this)), this.addEvent("slide:throttle(250)", this.slideToPosition.bind(this))
        },
        _getPositionById: function(e) {
            var t = null;
            return this.elements.each(function(n, i) {
                n.get("id").substr(6) === e && (t = i)
            }.bind(this)), t
        },
        slideRight: function() {
            this.fireEvent("slide", this.currentPosition + this.slideRange)
        },
        slideLeft: function() {
            this.fireEvent("slide", this.currentPosition - this.slideRange)
        },
        startSlideRight: function() {
            this.slideRight(), this.slideContinuously(this.slideRight)
        },
        startSlideLeft: function() {
            this.slideLeft(), this.slideContinuously(this.slideLeft)
        },
        slideContinuously: function(e) {
            this.slideTask = e.periodical(500, this)
        },
        stopSlide: function() {
            clearInterval(this.slideTask)
        },
        slideToPosition: function(e) {
            e > this.maxPosition ? e = this.maxPosition : 0 > e && (e = 0), e !== this.currentPosition && (this.currentPosition = e, this.scroll.toElement(this.elements[this.currentPosition]), 0 === e ? (this.stopSlide(), this.left.addClass("inactive"), this.right.removeClass("inactive")) : e === this.maxPosition ? (this.stopSlide(), this.left.removeClass("inactive"), this.right.addClass("inactive")) : (this.left.removeClass("inactive"), this.right.removeClass("inactive")))
        }
    }), define("/sites/default/themes/common/js/common/avatar.js", function() {}),
    function() {
        function e() {
            var e = document.getElementById("cookieNotificationBanner");
            null != e && e.hasClass("active") && (Cookie.write("cookies_policy_accepted", "accepted", {
                duration: 365
            }), e.removeClass("active"), i())
        }

        function t(e) {
            null === e || "communitypage" !== htmlClasses[0] && "home" !== htmlClasses[1] || (window.addEventListener("resize", function() {
                window.innerWidth > 1780 ? (e.style.width = "1780px", e.style.marginLeft = "-890px", e.style.left = "50%") : (e.style.width = "100%", e.style.marginLeft = "0", e.style.left = "0")
            }, !0), window.innerWidth > 1780 && (e.style.width = "1780px", e.style.marginLeft = "-890px", e.style.left = "50%"))
        }

        function n() {
            if ("mmho" === gameThemeDirectory) {
                var e = document.getElementsByClassName("footerWrapper"),
                    t = document.getElementById("footerWrapper"),
                    n = document.getElementsByClassName("discoverUniverseBanner");
                e.length > 0 ? e[0].style.marginBottom = "50px" : null !== t ? t.style.marginBottom = "50px" : n.length > 0 && (n[0].style.marginBottom = "230px")
            }
            if ("siedler" === gameThemeDirectory) {
                var i = document.getElementById("footer");
                null !== i && (i.style.marginBottom = "45px")
            }
            var s = document.getElementsByTagName("footer");
            if (s.length > 0)
                if ("mmho" === gameThemeDirectory && 0 === n.length) s[0].style.marginBottom = "70px";
                else if ("mmho" !== gameThemeDirectory)
                for (var r in s) r % 1 === 0 && (s[r].style.marginBottom = "40px")
        }

        function i() {
            if ("mmho" === gameThemeDirectory) {
                var e = document.getElementsByClassName("footerWrapper"),
                    t = document.getElementById("footerWrapper");
                e.length > 0 ? e[0].style.marginBottom = "0" : null !== t && (t.style.marginBottom = "0")
            }
            if ("siedler" === gameThemeDirectory) {
                var n = document.getElementById("footer");
                null !== n && (n.style.marginBottom = "0")
            }
            var i = document.getElementsByTagName("footer");
            if (i.length > 0)
                for (var s in i) s % 1 === 0 && (i[s].style.marginBottom = "0")
        }
        var s = Cookie.read("cookies_policy_accepted");
        if (null !== s) Cookie.write("cookies_policy_accepted", "accepted", {
            duration: 365
        });
        else {
            var r = document.getElementById("cookieNotificationBanner");
            if (null !== r) {
                r.addClass("active");
                var o = document.getElementsByClassName("btnAccept");
                o.length > 0 && o[0].addEventListener("click", function(t) {
                    e()
                }), n(), "siedler" === gameThemeDirectory && t(r)
            }
        }
    }(), define("/sites/default/themes/common/js/common/cookiesNotification.js", function() {}), require([], function() {
        jQuery.noConflict(), jQuery(document).ready(function(e) {
            function t() {
                e("#partnerFrameApi") && e("#partnerFrameApi").remove(), e('<iframe id="partnerFrameApi" width="0" height="0" style="display:none;" src="http://spielaffe.de/game/remote_ssoiframe_js?height=' + e("body").height() + '"></iframe>').appendTo("body")
            }
            if (partneriFrameOpenInNewWindow) {
                var n = e("body").height();
                setInterval(function() {
                    e("body").height() != n && (n = e("body").height(), t())
                }, 500), t()
            }
        })
    }), define("/sites/default/themes/siedler/js/common/kaisergames.js", function() {}), BrowserDegraded = new Class({
        _conditions: null,
        _title: null,
        _message: null,
        initialize: function(e, t) {
            this._title = e, this._message = t, this._conditions = new Array
        },
        addCondition: function(e, t) {
            this._conditions.push({
                agent: e,
                version: t
            })
        },
        check: function() {
            for (var e = 0; e < this._conditions.length; e++)
                if (Browser.name == this._conditions[e].agent && Browser.version < this._conditions[e].version) return !1;
            return !0
        },
        checkAndAlert: function() {
            if (!this.check()) {
                var e = new gm_dialog_Plain(this._title, this._message, {
                    hasCancelButton: !0
                });
                gm_DialogHandler.add(e)
            }
        }
    }), define("/sites/default/themes/siedler/js/common/browserdegrade.js", function() {}), window.addEvent("domready", function() {
        var e = $("promotionRegisterLink");
        e && new HoverButton($("promotionRegisterLink").getElement("img")), $$(".promotionBannerFlash").each(function(e) {
            var t = e.getFirst().get("html");
            swfobject.embedSWF(t, e.get("id") + "F", e.getWidth().toString(), e.getHeight().toString(), "9.0.0", "", {}, {
                menu: "false",
                scale: "noScale",
                allowScriptAccess: "always",
                bgcolor: "#FFFFFF",
                wmode: "transparent"
            })
        })
    }), define("/sites/default/themes/common/js/common/promotion.js", function() {}), require(["/sites/default/themes/common/js/common/form.js"], function() {
        window.addEvent("domready", function() {
            if (["menu", "submenu"].each(function(e) {
                    var t = document.id(e);
                    t && t.getElements(".doi").each(function(e) {
                        e.addEvent("click", function(t) {
                            t.stop();
                            var n = e.get("class").match(/doi_\w*/);
                            return gm_DialogHandler.add(new gm_dialog_DoiRepeat(window["doiText_" + n[0] + "_title"], window["doiText_" + n[0] + "_text"], e.getElement("a").get("href"))), !1
                        })
                    })
                }), document.id("gameLanguageForm")) {
                var e = new gm_Form(document.id("gameLanguageForm"), {
                    continueOnStatusFailed: !0,
                    continueOnStatusOkay: !0
                });
                document.id("gameLanguageForm").getElements("input").addEvent("change", function() {
                    e.onSubmit()
                })
            }
        })
    }), define("/sites/default/themes/common/js/common/menu.js", function() {}), $$("html.communitypage").length && require(["/sites/default/themes/common/js/common/promotion.js", "/sites/default/themes/common/js/common/menu.js"], function() {
        window.addEvent("load", function() {
            Browser.chrome33 && document.id("menu") && document.id("menu").getElements("li a").hide().show()
        })
    }), define("/sites/default/themes/common/js/communitypage/base.js", function() {}), require(["/sites/default/themes/common/js/communitypage/base.js"]), define("/sites/default/themes/siedler/js/communitypage/base.js", function() {}), ($$("html.communitypage.home").length || $$("html.communitypage.news").length) && window.addEvent("domready", function() {
        document.getElements("a").each(function(e) {
            if (!e.hasClass("video")) {
                var t = /(\w+:\/\/)?www\.youtube\.\w+\/watch([\?&][a-zA-Z0-9_]+=[a-zA-Z0-9\-_]+)*/,
                    n = /[\?&]v=([a-zA-Z0-9\-_]+)/,
                    i = e.get("href");
                if (null != i) {
                    var s = i.match(t);
                    if (s && s[2]) {
                        s = s[2];
                        var r = s.match(n);
                        r && r[1] && (new Element("iframe").set({
                            src: "http://www.youtube.com/embed/" + r[1] + "?wmode=opaque",
                            width: 460,
                            height: 275,
                            frameborder: "0",
                            allowfullscreen: "true"
                        }).inject(new Element("div").addClass("videolink").inject(e, "after")), e.destroy())
                    }
                }
            }
        })
    }), define("/sites/default/themes/siedler/js/common/youtube.js", function() {}), ($$(".communitypage.home").length || $$(".communitypage.news").length || $$(".communitypage.spa").length) && require(["/sites/default/themes/siedler/js/common/youtube.js"], function() {
        window.addEvent("domready", function() {
            $("loginForm") && $("loginButton") && (new FormOverlay($("loginForm")), login = null, $("loginButton").addEvent("click", function(e) {
                null === login && (login = new Login), login.show(e.page.x, e.page.y)
            })), $("news") && $("news").getElements("h3").each(function(e) {
                e.addEvent("click", function() {
                    var t = e.getElement(".expander");
                    e.hasClass("small") ? t.set("src", t.get("src").replace(/\/plus\./, "/minus.")) : t.set("src", t.get("src").replace(/\/minus\./, "/plus.")), e.toggleClass("small"), e.getNext().toggleClass("hide")
                })
            })
        })
    }), define("/sites/default/themes/siedler/js/communitypage/site/news.js", function() {}), Waitingoverlay = new Class({
        overlay: null,
        active: !1,
        initialize: function(e) {
            this.overlay = e
        },
        show: function() {
            this.active || (this.active = !0, Overlay.show(), this.overlay.removeClass("hide"), new gm_CenterElement(this.overlay))
        },
        hide: function() {
            this.active = !1, this.overlay.addClass("hide"), Overlay.hide()
        }
    }), Waitingoverlay._instance = null, Waitingoverlay.getInstance = function() {
        if (null === Waitingoverlay._instance) {
            var e = document.id("waitingOverlay");
            Waitingoverlay._instance = new Waitingoverlay(e)
        }
        return Waitingoverlay._instance
    }, Waitingoverlay.show = function() {
        Waitingoverlay.getInstance().show()
    }, Waitingoverlay.hide = function() {
        Waitingoverlay.getInstance().hide()
    }, define("/sites/default/themes/siedler/js/common/waitingoverlay.js", function() {}), require(["/sites/default/themes/siedler/js/common/waitingoverlay.js"], function() {
        var e = new Class({
            maxRunningTime: 5e3,
            timer: null,
            container: null,
            initialize: function() {
                this.show(), window.addEvent("load", this.limitReached.bind(this)), this.timer = this.limitReached.delay(this.maxRunningTime, this)
            },
            show: function() {
                Waitingoverlay.getInstance().overlay.addClass("pageloadingbar"), Overlay.getInstance().overlay.addClass("pageloadingbar"), Waitingoverlay.show()
            },
            hide: function() {
                Waitingoverlay.getInstance().overlay.removeClass("pageloadingbar"), Overlay.getInstance().overlay.removeClass("pageloadingbar"), Waitingoverlay.hide()
            },
            limitReached: function() {
                clearTimeout(this.timer), this.hide()
            }
        });
        window.addEvent("domready", function() {
            document.body.hasClass("ru") && new e
        })
    }), define("/sites/default/themes/siedler/js/common/PageLoadingBar.js", function() {}), gm_Expander = new Class({
        expandArea: null,
        expander: null,
        expanded: !1,
        initialize: function(e) {
            this.expandArea = e.getElement(".expandArea"), this.expander = e.getElement("img.expander"), this.expanded = !this.expandArea.hasClass("hide"), e.getElement("h4").addEvent("click", this.switchStatus.bind(this))
        },
        switchStatus: function() {
            this.expanded ? this.contract() : this.expand()
        },
        expand: function() {
            this.expandArea.removeClass("hide"), this.expander.set("src", this.expander.get("src").replace(/\/plus\./, "/minus.")), this.expanded = !0
        },
        contract: function() {
            this.expandArea.addClass("hide"), this.expander.set("src", this.expander.get("src").replace(/\/minus\./, "/plus.")), this.expanded = !1
        }
    }), window.addEvent("domready", function() {
        $$("div.content.collapsible").each(function(e) {
            if (e.getElement(".expandArea")) {
                var t = new gm_Expander(e),
                    n = e.getChildren("a")[0];
                n && n.get("name") === decodeURIComponent(new URI(window.location).get("fragment")) && (t.expand(), function() {
                    new Fx.Scroll(window, {
                        duration: 1500,
                        transition: Fx.Transitions.Quad.easeOut
                    }).toElement(e.getPrevious("h3"))
                }.delay(500))
            }
        })
    }), define("/sites/default/themes/common/js/common/collapsible-content.js", function() {}), window.addEvent("domready", function() {
        var e = $("faqDoiLink");
        e && e.addEvent("click", function() {
            return new gm_Request({
                url: "/" + languagePath + "/api/user/profile/repeatdoi",
                onStatusOkay: function(e) {
                    gm_DialogHandler.add(new gm_dialog_Plain(successDialog.repeatdoiForm.title, successDialog.repeatdoiForm.message))
                }
            }).post({
                type: "repeatdoi"
            }), !1
        })
    }), define("/sites/default/themes/common/js/common/faqDoiLink.js", function() {}), ($$(".communitypage.faq").length || $$(".communitypage.spa").length) && require(["/sites/default/themes/common/js/common/collapsible-content.js", "/sites/default/themes/common/js/common/dialog.js", "/sites/default/themes/common/js/common/faqDoiLink.js"], function() {
        FaqForm = new Class({
            initialize: function(e) {
                var t = new gm_Form_Html(e, {
                    continueOnStatusFailed: !0,
                    removeValuesAfterSuccess: !0,
                    continueOnStatusOkay: !0
                }, {}, e.getElement("a.submit"));
                document.id("faqCaptchaBox") && document.getElement(".content.collapsible").getElement("img.expander").addEvent("click", function() {
                    gm_Captcha.create(recaptchaPublicKey), document.id("faq_recaptcha_response_field").removeEvents("blur"), document.id("faq_recaptcha_response_field").removeEvents("focus"), document.id("faq_recaptcha_response_field").removeEvents("change"), document.id("faq_recaptcha_response_field").removeEvents("keyup"), document.id("faq_recaptcha_response_field").set("value", "")
                });
                var n = document.id("faq_recaptcha_response_field");
                t.addEvent("formValidate", function() {
                    1 !== t.fieldsFailed || document.id("faqCaptchaBox").isDisplayed() ? t.fieldsFailed > 0 && (document.id("faqCaptchaBox").hasClass("error") || (document.id("faqCaptchaBox").hide(), n.destroy())) : (document.id("faqCaptchaBox").show(), document.id("faqCaptchaBox").removeClass("error"), document.id("faqCaptchaBox").getElements("div.message").destroy())
                }), t.addEvent("elementFail", function(e, t) {
                    e === n && (gm_Captcha.reload(), document.id("faq_recaptcha_response_field").set("value", ""))
                }), location.href.split("#")[1] && location.href.split("#")[1].toInt() > 0 && $("category") && ($("category").value = location.href.split("#")[1].toInt())
            },
            _showSuccessMessage: function(e, t) {
                var n = new gm_dialog_Plain(e, t);
                n.addEvent("okay", function() {
                    location.href = "/"
                }.pass(n)), gm_DialogHandler.add(n)
            }
        }), window.addEvent("domready", function() {
            $$(".faqForm, #faqForm") && $$(".faqForm, #faqForm").each(function(e) {
                new FaqForm(e)
            })
        })
    }), define("/sites/default/themes/siedler/js/communitypage/site/faq.js", function() {}), $$(".communitypage.faq_category").length && require(["/sites/default/themes/common/js/common/collapsible-content.js"]), define("/sites/default/themes/siedler/js/communitypage/site/faq_category.js", function() {}), $$("html.communitypage.gamesload").length && (gamesloadForm = new Class({
        initialize: function() {
            new gm_Form($("gamesloadForm"), {
                continueOnStatusFailed: !0
            }, {
                onStatusOkay: function(e) {
                    this._showSuccessMessage(successDialog, successDialogMessage)
                }.bind(this),
                onStatusFailed: function(e) {
                    gm_DialogHandler.add(new gm_dialog_Plain(failDialog, failDialogMessage))
                }
            }, $("gamesloadForm").getElement("a"))
        },
        _showSuccessMessage: function(e, t) {
            var n = new gm_dialog_Plain(e, t);
            n.addEvent("okay", function() {
                location.href = "/"
            }.pass(n)), gm_DialogHandler.add(n)
        }
    }), window.addEvent("domready", function() {
        $("gamesloadForm") && new gamesloadForm
    })), define("/sites/default/themes/siedler/js/communitypage/site/gamesload.js", function() {}), $$("html.communitypage.links").length && window.addEvent("domready", function() {
        $("links").getElements("h3").each(function(e) {
            e.addEvent("click", function() {
                var t = e.getElement(".expander");
                e.hasClass("small") ? t.set("src", t.get("src").replace(/\/plus\./, "/minus.")) : t.set("src", t.get("src").replace(/\/minus\./, "/plus.")), e.toggleClass("small"), e.getNext().getNext().toggleClass("hide")
            })
        })
    }), define("/sites/default/themes/siedler/js/communitypage/site/links.js", function() {}), window.twttr || (window.twttr = {}),
    function() {
        function e(e, t) {
            return t = t || "", "string" != typeof e && (e.global && t.indexOf("g") < 0 && (t += "g"), e.ignoreCase && t.indexOf("i") < 0 && (t += "i"), e.multiline && t.indexOf("m") < 0 && (t += "m"), e = e.source), new RegExp(e.replace(/#\{(\w+)\}/g, function(e, t) {
                var n = twttr.txt.regexen[t] || "";
                return "string" != typeof n && (n = n.source), n
            }), t)
        }

        function t(e, t) {
            return e.replace(/#\{(\w+)\}/g, function(e, n) {
                return t[n] || ""
            })
        }

        function n(e, t, n) {
            var i = String.fromCharCode(t);
            return n !== t && (i += "-" + String.fromCharCode(n)), e.push(i), e
        }

        function i(e) {
            var t = {};
            for (var n in e) e.hasOwnProperty(n) && (t[n] = e[n]);
            return t
        }

        function s(e, t, n) {
            return n ? !e || e.match(t) && RegExp["$&"] === e : "string" == typeof e && e.match(t) && RegExp["$&"] === e
        }
        twttr.txt = {}, twttr.txt.regexen = {};
        var r = {
            "&": "&amp;",
            ">": "&gt;",
            "<": "&lt;",
            '"': "&quot;",
            "'": "&#39;"
        };
        twttr.txt.htmlEscape = function(e) {
            return e && e.replace(/[&"'><]/g, function(e) {
                return r[e]
            })
        };
        var o = String.fromCharCode,
            a = [o(32), o(133), o(160), o(5760), o(6158), o(8232), o(8233), o(8239), o(8287), o(12288)];
        n(a, 9, 13), n(a, 8192, 8202), twttr.txt.regexen.spaces_group = e(a.join("")), twttr.txt.regexen.spaces = e("[" + a.join("") + "]"), twttr.txt.regexen.punct = /\!'#%&'\(\)*\+,\\\-\.\/:;<=>\?@\[\]\^_{|}~/, twttr.txt.regexen.atSigns = /[@ï¼ ]/, twttr.txt.regexen.extractMentions = e(/(^|[^a-zA-Z0-9_])(#{atSigns})([a-zA-Z0-9_]{1,20})(?=(.|$))/g), twttr.txt.regexen.extractReply = e(/^(?:#{spaces})*#{atSigns}([a-zA-Z0-9_]{1,20})/), twttr.txt.regexen.listName = /[a-zA-Z][a-zA-Z0-9_\-\u0080-\u00ff]{0,24}/, twttr.txt.regexen.extractMentionsOrLists = e(/(^|[^a-zA-Z0-9_])(#{atSigns})([a-zA-Z0-9_]{1,20})(\/[a-zA-Z][a-zA-Z0-9_\-]{0,24})?(?=(.|$))/g);
        var l = [];
        n(l, 1024, 1279), n(l, 1280, 1319), n(l, 11744, 11775), n(l, 42560, 42655), n(l, 4352, 4607), n(l, 12592, 12677), n(l, 43360, 43391), n(l, 44032, 55215), n(l, 55216, 55295), n(l, 65441, 65500), n(l, 12449, 12538), n(l, 12540, 12542), n(l, 65382, 65439), n(l, 65392, 65392), n(l, 65296, 65305), n(l, 65313, 65338), n(l, 65345, 65370), n(l, 12353, 12438), n(l, 12441, 12446),
            n(l, 13312, 19903), n(l, 19968, 40959), n(l, 173824, 177983), n(l, 177984, 178207), n(l, 194560, 195103), n(l, 12293, 12293), n(l, 12347, 12347), twttr.txt.regexen.nonLatinHashtagChars = e(l.join("")), twttr.txt.regexen.latinAccentChars = e("Ã€ÃÃ‚ÃƒÃ„Ã…Ã†Ã‡ÃˆÃ‰ÃŠÃ‹ÃŒÃÃŽÃÃÃ‘Ã’Ã“Ã”Ã•Ã–Ã˜Ã™ÃšÃ›ÃœÃÃžÃŸÃ Ã¡Ã¢Ã£Ã¤Ã¥Ã¦Ã§Ã¨Ã©ÃªÃ«Ã¬Ã­Ã®Ã¯Ã°Ã±Ã²Ã³Ã´ÃµÃ¶Ã¸Ã¹ÃºÃ»Ã¼Ã½Ã¾ÅŸ\\303\\277"), twttr.txt.regexen.endScreenNameMatch = e(/^(?:#{atSigns}|[#{latinAccentChars}]|:\/\/)/), twttr.txt.regexen.hashtagBoundary = e(/(?:^|$|#{spaces}|[ã€Œã€ã€‚ã€.,!ï¼?ï¼Ÿ:;"'])/), twttr.txt.regexen.hashtagAlpha = e(/[a-z_#{latinAccentChars}#{nonLatinHashtagChars}]/i), twttr.txt.regexen.hashtagAlphaNumeric = e(/[a-z0-9_#{latinAccentChars}#{nonLatinHashtagChars}]/i), twttr.txt.regexen.autoLinkHashtags = e(/(#{hashtagBoundary})(#|ï¼ƒ)(#{hashtagAlphaNumeric}*#{hashtagAlpha}#{hashtagAlphaNumeric}*)/gi), twttr.txt.regexen.autoLinkUsernamesOrLists = /(^|[^a-zA-Z0-9_]|RT:?)([@ï¼ ]+)([a-zA-Z0-9_]{1,20})(\/[a-zA-Z][a-zA-Z0-9_\-]{0,24})?/g, twttr.txt.regexen.autoLinkEmoticon = /(8\-\#|8\-E|\+\-\(|\`\@|\`O|\&lt;\|:~\(|\}:o\{|:\-\[|\&gt;o\&lt;|X\-\/|\[:-\]\-I\-|\/\/\/\/Ã–\\\\\\\\|\(\|:\|\/\)|âˆ‘:\*\)|\( \| \))/g, twttr.txt.regexen.validPrecedingChars = e(/(?:[^-\/"'!=A-Za-z0-9_@ï¼ \.]|^)/), twttr.txt.regexen.invalidDomainChars = t(" #{punct}#{spaces_group}", twttr.txt.regexen), twttr.txt.regexen.validDomainChars = e(/[^#{invalidDomainChars}]/), twttr.txt.regexen.validSubdomain = e(/(?:(?:#{validDomainChars}(?:[_-]|#{validDomainChars})*)?#{validDomainChars}\.)/), twttr.txt.regexen.validDomainName = e(/(?:(?:#{validDomainChars}(?:-|#{validDomainChars})*)?#{validDomainChars}\.)/), twttr.txt.regexen.validGTLD = e(/(?:(?:aero|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel)(?=[^a-zA-Z]|$))/), twttr.txt.regexen.validCCTLD = e(/(?:(?:ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|ss|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|za|zm|zw)(?=[^a-zA-Z]|$))/), twttr.txt.regexen.validPunycode = e(/(?:xn--[0-9a-z]+)/), twttr.txt.regexen.validDomain = e(/(?:#{validSubdomain}*#{validDomainName}(?:#{validGTLD}|#{validCCTLD}|#{validPunycode}))/), twttr.txt.regexen.validShortDomain = e(/^#{validDomainName}#{validCCTLD}$/), twttr.txt.regexen.validPortNumber = e(/[0-9]+/), twttr.txt.regexen.validGeneralUrlPathChars = e(/[a-z0-9!\*';:=\+\$\/%#\[\]\-_,~|&#{latinAccentChars}]/i), twttr.txt.regexen.wikipediaDisambiguation = e(/(?:\(#{validGeneralUrlPathChars}+\))/i), twttr.txt.regexen.validUrlPathChars = e(/(?:#{wikipediaDisambiguation}|@#{validGeneralUrlPathChars}+\/|[\.,]?#{validGeneralUrlPathChars}?)/i), twttr.txt.regexen.validUrlPathEndingChars = e(/(?:[\+\-a-z0-9=_#\/#{latinAccentChars}]|#{wikipediaDisambiguation})/i), twttr.txt.regexen.validUrlQueryChars = /[a-z0-9!\*'\(\);:&=\+\$\/%#\[\]\-_\.,~|]/i, twttr.txt.regexen.validUrlQueryEndingChars = /[a-z0-9_&=#\/]/i, twttr.txt.regexen.extractUrl = e("((#{validPrecedingChars})((https?:\\/\\/)?(#{validDomain})(?::(#{validPortNumber}))?(\\/(?:#{validUrlPathChars}+#{validUrlPathEndingChars}|#{validUrlPathChars}+#{validUrlPathEndingChars}?|#{validUrlPathEndingChars})?)?(\\?#{validUrlQueryChars}*#{validUrlQueryEndingChars})?))", "gi"), twttr.txt.regexen.validateUrlUnreserved = /[a-z0-9\-._~]/i, twttr.txt.regexen.validateUrlPctEncoded = /(?:%[0-9a-f]{2})/i, twttr.txt.regexen.validateUrlSubDelims = /[!$&'()*+,;=]/i, twttr.txt.regexen.validateUrlPchar = e("(?:#{validateUrlUnreserved}|#{validateUrlPctEncoded}|#{validateUrlSubDelims}|[:|@])", "i"), twttr.txt.regexen.validateUrlScheme = /(?:[a-z][a-z0-9+\-.]*)/i, twttr.txt.regexen.validateUrlUserinfo = e("(?:#{validateUrlUnreserved}|#{validateUrlPctEncoded}|#{validateUrlSubDelims}|:)*", "i"), twttr.txt.regexen.validateUrlDecOctet = /(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9]{2})|(?:2[0-4][0-9])|(?:25[0-5]))/i, twttr.txt.regexen.validateUrlIpv4 = e(/(?:#{validateUrlDecOctet}(?:\.#{validateUrlDecOctet}){3})/i), twttr.txt.regexen.validateUrlIpv6 = /(?:\[[a-f0-9:\.]+\])/i, twttr.txt.regexen.validateUrlIp = e("(?:#{validateUrlIpv4}|#{validateUrlIpv6})", "i"), twttr.txt.regexen.validateUrlSubDomainSegment = /(?:[a-z0-9](?:[a-z0-9_\-]*[a-z0-9])?)/i, twttr.txt.regexen.validateUrlDomainSegment = /(?:[a-z0-9](?:[a-z0-9\-]*[a-z0-9])?)/i, twttr.txt.regexen.validateUrlDomainTld = /(?:[a-z](?:[a-z0-9\-]*[a-z0-9])?)/i, twttr.txt.regexen.validateUrlDomain = e(/(?:(?:#{validateUrlSubDomainSegment]}\.)*(?:#{validateUrlDomainSegment]}\.)#{validateUrlDomainTld})/i), twttr.txt.regexen.validateUrlHost = e("(?:#{validateUrlIp}|#{validateUrlDomain})", "i"), twttr.txt.regexen.validateUrlUnicodeSubDomainSegment = /(?:(?:[a-z0-9]|[^\u0000-\u007f])(?:(?:[a-z0-9_\-]|[^\u0000-\u007f])*(?:[a-z0-9]|[^\u0000-\u007f]))?)/i, twttr.txt.regexen.validateUrlUnicodeDomainSegment = /(?:(?:[a-z0-9]|[^\u0000-\u007f])(?:(?:[a-z0-9\-]|[^\u0000-\u007f])*(?:[a-z0-9]|[^\u0000-\u007f]))?)/i, twttr.txt.regexen.validateUrlUnicodeDomainTld = /(?:(?:[a-z]|[^\u0000-\u007f])(?:(?:[a-z0-9\-]|[^\u0000-\u007f])*(?:[a-z0-9]|[^\u0000-\u007f]))?)/i, twttr.txt.regexen.validateUrlUnicodeDomain = e(/(?:(?:#{validateUrlUnicodeSubDomainSegment}\.)*(?:#{validateUrlUnicodeDomainSegment}\.)#{validateUrlUnicodeDomainTld})/i), twttr.txt.regexen.validateUrlUnicodeHost = e("(?:#{validateUrlIp}|#{validateUrlUnicodeDomain})", "i"), twttr.txt.regexen.validateUrlPort = /[0-9]{1,5}/, twttr.txt.regexen.validateUrlUnicodeAuthority = e("(?:(#{validateUrlUserinfo})@)?(#{validateUrlUnicodeHost})(?::(#{validateUrlPort}))?", "i"), twttr.txt.regexen.validateUrlAuthority = e("(?:(#{validateUrlUserinfo})@)?(#{validateUrlHost})(?::(#{validateUrlPort}))?", "i"), twttr.txt.regexen.validateUrlPath = e(/(\/#{validateUrlPchar}*)*/i), twttr.txt.regexen.validateUrlQuery = e(/(#{validateUrlPchar}|\/|\?)*/i), twttr.txt.regexen.validateUrlFragment = e(/(#{validateUrlPchar}|\/|\?)*/i), twttr.txt.regexen.validateUrlUnencoded = e("^(?:([^:/?#]+):\\/\\/)?([^/?#]*)([^?#]*)(?:\\?([^#]*))?(?:#(.*))?$", "i");
        var c = "tweet-url",
            u = "list-slug",
            d = "username",
            h = "hashtag",
            m = ' rel="nofollow"';
        twttr.txt.autoLink = function(e, t) {
            return t = i(t || {}), twttr.txt.autoLinkUsernamesOrLists(twttr.txt.autoLinkUrlsCustom(twttr.txt.autoLinkHashtags(e, t), t), t)
        }, twttr.txt.autoLinkUsernamesOrLists = function(e, n) {
            if (n = i(n || {}), n.urlClass = n.urlClass || c, n.listClass = n.listClass || u, n.usernameClass = n.usernameClass || d, n.usernameUrlBase = n.usernameUrlBase || "http://twitter.com/", n.listUrlBase = n.listUrlBase || "http://twitter.com/", !n.suppressNoFollow) var s = m;
            for (var r = "", o = twttr.txt.splitTags(e), a = 0; a < o.length; a++) {
                var l = o[a];
                0 !== a && (r += a % 2 === 0 ? ">" : "<"), r += a % 4 !== 0 ? l : l.replace(twttr.txt.regexen.autoLinkUsernamesOrLists, function(e, i, r, o, a, l, c) {
                    var u = c.slice(l + e.length),
                        d = {
                            before: i,
                            at: r,
                            user: twttr.txt.htmlEscape(o),
                            slashListname: twttr.txt.htmlEscape(a),
                            extraHtml: s,
                            preChunk: "",
                            chunk: twttr.txt.htmlEscape(c),
                            postChunk: ""
                        };
                    for (var h in n) n.hasOwnProperty(h) && (d[h] = n[h]);
                    if (a && !n.suppressLists) {
                        var m = d.chunk = t("#{user}#{slashListname}", d);
                        return d.list = twttr.txt.htmlEscape(m.toLowerCase()), t('#{before}#{at}<a class="#{urlClass} #{listClass}" href="#{listUrlBase}#{list}"#{extraHtml}>#{preChunk}#{chunk}#{postChunk}</a>', d)
                    }
                    return u && u.match(twttr.txt.regexen.endScreenNameMatch) ? e : (d.chunk = twttr.txt.htmlEscape(o), d.dataScreenName = n.suppressDataScreenName ? "" : t('data-screen-name="#{chunk}" ', d), t('#{before}#{at}<a class="#{urlClass} #{usernameClass}" #{dataScreenName}href="#{usernameUrlBase}#{chunk}"#{extraHtml}>#{preChunk}#{chunk}#{postChunk}</a>', d))
                })
            }
            return r
        }, twttr.txt.autoLinkHashtags = function(e, n) {
            if (n = i(n || {}), n.urlClass = n.urlClass || c, n.hashtagClass = n.hashtagClass || h, n.hashtagUrlBase = n.hashtagUrlBase || "http://twitter.com/search?q=%23", !n.suppressNoFollow) var s = m;
            return e.replace(twttr.txt.regexen.autoLinkHashtags, function(e, i, r, o) {
                var a = {
                    before: i,
                    hash: twttr.txt.htmlEscape(r),
                    preText: "",
                    text: twttr.txt.htmlEscape(o),
                    postText: "",
                    extraHtml: s
                };
                for (var l in n) n.hasOwnProperty(l) && (a[l] = n[l]);
                return t('#{before}<a href="#{hashtagUrlBase}#{text}" title="##{text}" class="#{urlClass} #{hashtagClass}"#{extraHtml}>#{hash}#{preText}#{text}#{postText}</a>', a)
            })
        }, twttr.txt.autoLinkUrlsCustom = function(e, n) {
            n = i(n || {}), n.suppressNoFollow || (n.rel = "nofollow"), n.urlClass && (n["class"] = n.urlClass, delete n.urlClass);
            var s, r, o;
            if (n.urlEntities)
                for (s = {}, r = 0, o = n.urlEntities.length; o > r; r++) s[n.urlEntities[r].url] = n.urlEntities[r];
            return delete n.suppressNoFollow, delete n.suppressDataScreenName, delete n.listClass, delete n.usernameClass, delete n.usernameUrlBase, delete n.listUrlBase, e.replace(twttr.txt.regexen.extractUrl, function(e, i, r, o, a, l, c, u, d) {
                if (a) {
                    var h = "";
                    for (var m in n) h += t(' #{k}="#{v}" ', {
                        k: m,
                        v: n[m].toString().replace(/"/, "&quot;").replace(/</, "&lt;").replace(/>/, "&gt;")
                    });
                    var f = {
                        before: r,
                        htmlAttrs: h,
                        url: twttr.txt.htmlEscape(o)
                    };
                    return s && s[o] && s[o].display_url ? f.displayUrl = twttr.txt.htmlEscape(s[o].display_url) : f.displayUrl = f.url, t('#{before}<a href="#{url}"#{htmlAttrs}>#{displayUrl}</a>', f)
                }
                return i
            })
        }, twttr.txt.extractMentions = function(e) {
            for (var t = [], n = twttr.txt.extractMentionsWithIndices(e), i = 0; i < n.length; i++) {
                var s = n[i].screenName;
                t.push(s)
            }
            return t
        }, twttr.txt.extractMentionsWithIndices = function(e) {
            if (!e) return [];
            var t = [],
                n = 0;
            return e.replace(twttr.txt.regexen.extractMentions, function(i, s, r, o, a) {
                if (!a.match(twttr.txt.regexen.endScreenNameMatch)) {
                    var l = e.indexOf(r + o, n);
                    n = l + o.length + 1, t.push({
                        screenName: o,
                        indices: [l, n]
                    })
                }
            }), t
        }, twttr.txt.extractMentionsOrListsWithIndices = function(e) {
            if (!e) return [];
            var t = [],
                n = 0;
            return e.replace(twttr.txt.regexen.extractMentionsOrLists, function(i, s, r, o, a, l) {
                if (!l.match(twttr.txt.regexen.endScreenNameMatch)) {
                    a = a || "";
                    var c = e.indexOf(r + o + a, n);
                    n = c + o.length + a.length + 1, t.push({
                        screenName: o,
                        listSlug: a,
                        indices: [c, n]
                    })
                }
            }), t
        }, twttr.txt.extractReplies = function(e) {
            if (!e) return null;
            var t = e.match(twttr.txt.regexen.extractReply);
            return t ? t[1] : null
        }, twttr.txt.extractUrls = function(e) {
            for (var t = [], n = twttr.txt.extractUrlsWithIndices(e), i = 0; i < n.length; i++) t.push(n[i].url);
            return t
        }, twttr.txt.extractUrlsWithIndices = function(e) {
            if (!e) return [];
            var t = [];
            return e.replace(twttr.txt.regexen.extractUrl, function(n, i, s, r, o, a, l, c, u) {
                if (o || c || !a.match(twttr.txt.regexen.validShortDomain)) {
                    var d = e.indexOf(r, h),
                        h = d + r.length;
                    t.push({
                        url: r,
                        indices: [d, h]
                    })
                }
            }), t
        }, twttr.txt.extractHashtags = function(e) {
            for (var t = [], n = twttr.txt.extractHashtagsWithIndices(e), i = 0; i < n.length; i++) t.push(n[i].hashtag);
            return t
        }, twttr.txt.extractHashtagsWithIndices = function(e) {
            if (!e) return [];
            var t = [],
                n = 0;
            return e.replace(twttr.txt.regexen.autoLinkHashtags, function(i, s, r, o) {
                var a = e.indexOf(r + o, n);
                n = a + o.length + 1, t.push({
                    hashtag: o,
                    indices: [a, n]
                })
            }), t
        }, twttr.txt.splitTags = function(e) {
            for (var t, n, i = e.split("<"), s = [], r = 0; r < i.length; r += 1)
                if (n = i[r]) {
                    t = n.split(">");
                    for (var o = 0; o < t.length; o += 1) s.push(t[o])
                } else s.push("");
            return s
        }, twttr.txt.hitHighlight = function(e, t, n) {
            var i = "em";
            if (t = t || [], n = n || {}, 0 === t.length) return e;
            var s, r, o, a, l, c, u, d = n.tag || i,
                h = ["<" + d + ">", "</" + d + ">"],
                m = twttr.txt.splitTags(e),
                f = "",
                p = 0,
                g = m[0],
                v = 0,
                y = 0,
                w = !1,
                b = g,
                _ = [];
            for (s = 0; s < t.length; s += 1)
                for (r = 0; r < t[s].length; r += 1) _.push(t[s][r]);
            for (o = 0; o < _.length; o += 1) {
                for (a = _[o], l = h[o % 2], c = !1; null != g && a >= v + g.length;) f += b.slice(y), w && a === v + b.length && (f += l, c = !0), m[p + 1] && (f += "<" + m[p + 1] + ">"), v += b.length, y = 0, p += 2, g = m[p], b = g, w = !1;
                c || null == g ? c || (c = !0, f += l) : (u = a - v, f += b.slice(y, u) + l, y = u, w = o % 2 === 0 ? !0 : !1)
            }
            if (null != g)
                for (y < b.length && (f += b.slice(y)), o = p + 1; o < m.length; o += 1) f += o % 2 === 0 ? m[o] : "<" + m[o] + ">";
            return f
        };
        var f = 140,
            p = [o(65534), o(65279), o(65535), o(8234), o(8235), o(8236), o(8237), o(8238)];
        twttr.txt.isInvalidTweet = function(e) {
            if (!e) return "empty";
            if (e.length > f) return "too_long";
            for (var t = 0; t < p.length; t++)
                if (e.indexOf(p[t]) >= 0) return "invalid_characters";
            return !1
        }, twttr.txt.isValidTweetText = function(e) {
            return !twttr.txt.isInvalidTweet(e)
        }, twttr.txt.isValidUsername = function(e) {
            if (!e) return !1;
            var t = twttr.txt.extractMentions(e);
            return 1 === t.length && t[0] === e.slice(1)
        };
        var g = e(/^#{autoLinkUsernamesOrLists}$/);
        twttr.txt.isValidList = function(e) {
            var t = e.match(g);
            return !(!t || "" != t[1] || !t[4])
        }, twttr.txt.isValidHashtag = function(e) {
            if (!e) return !1;
            var t = twttr.txt.extractHashtags(e);
            return 1 === t.length && t[0] === e.slice(1)
        }, twttr.txt.isValidUrl = function(e, t, n) {
            if (null == t && (t = !0), null == n && (n = !0), !e) return !1;
            var i = e.match(twttr.txt.regexen.validateUrlUnencoded);
            if (!i || i[0] !== e) return !1;
            var r = i[1],
                o = i[2],
                a = i[3],
                l = i[4],
                c = i[5];
            return (!n || s(r, twttr.txt.regexen.validateUrlScheme) && r.match(/^https?$/i)) && s(a, twttr.txt.regexen.validateUrlPath) && s(l, twttr.txt.regexen.validateUrlQuery, !0) && s(c, twttr.txt.regexen.validateUrlFragment, !0) ? t && s(o, twttr.txt.regexen.validateUrlUnicodeAuthority) || !t && s(o, twttr.txt.regexen.validateUrlAuthority) : !1
        }, "undefined" != typeof module && module.exports && (module.exports = twttr.txt)
    }(), TWTR = window.TWTR || {}, Array.forEach || (Array.prototype.filter = function(e, t) {
        for (var n = t || window, i = [], s = 0, r = this.length; r > s; ++s) e.call(n, this[s], s, this) && i.push(this[s]);
        return i
    }, Array.prototype.indexOf = function(e, t) {
        for (var n = 0; n < this.length; ++n)
            if (this[n] === e) return n;
        return -1
    }),
    function() {
        function e(e, t, n) {
            for (var i = 0, s = e.length; s > i; ++i) t.call(n || window, e[i], i, e)
        }

        function t(e, t, n) {
            this.el = e, this.prop = t, this.from = n.from, this.to = n.to, this.time = n.time, this.callback = n.callback, this.animDiff = this.to - this.from
        }

        function n(e) {
            if (!twttr.widgets) {
                e = e || window.event;
                for (var t, n, r, c, u, d = e.target || e.srcElement; d && "a" !== d.nodeName.toLowerCase();) d = d.parentNode;
                d && "a" === d.nodeName.toLowerCase() && d.href && (t = d.href.match(i), t && (n = 550, r = t[2] in s ? 420 : 560, c = Math.round(l / 2 - n / 2), u = 0, a > r && (u = Math.round(a / 2 - r / 2)), window.open(d.href, "intent", o + ",width=" + n + ",height=" + r + ",left=" + c + ",top=" + u), e.returnValue = !1, e.preventDefault && e.preventDefault()))
            }
        }
        if (!TWTR || !TWTR.Widget) {
            t.canTransition = function() {
                    var e = document.createElement("twitter");
                    return e.style.cssText = "-webkit-transition: all .5s linear;", !!e.style.webkitTransitionProperty
                }(), t.prototype._setStyle = function(e) {
                    switch (this.prop) {
                        case "opacity":
                            this.el.style[this.prop] = e, this.el.style.filter = "alpha(opacity=" + 100 * e + ")";
                            break;
                        default:
                            this.el.style[this.prop] = e + "px"
                    }
                }, t.prototype._animate = function() {
                    return this.now = new Date, this.diff = this.now - this.startTime, this.diff > this.time ? (this._setStyle(this.to), this.callback && this.callback.call(this), void clearInterval(this.timer)) : (this.percentage = Math.floor(this.diff / this.time * 100) / 100, this.val = this.animDiff * this.percentage + this.from, void this._setStyle(this.val))
                }, t.prototype.start = function() {
                    var e = this;
                    this.startTime = new Date, this.timer = setInterval(function() {
                        e._animate.call(e)
                    }, 15)
                }, TWTR.Widget = function(e) {
                    this.init(e)
                },
                function() {
                    function n(e) {
                        var t = {};
                        for (var n in e) e.hasOwnProperty(n) && (t[n] = e[n]);
                        return t
                    }

                    function i(e, t, n) {
                        this.job = e, this.decayFn = t, this.interval = n, this.decayRate = 1, this.decayMultiplier = 1.25, this.maxDecayTime = 18e4
                    }

                    function s(e, t, n) {
                        this.time = e || 6e3, this.loop = t || !1, this.repeated = 0, this.callback = n, this.haystack = []
                    }

                    function o(e) {
                        var t = '<div class="twtr-tweet-wrap">         <div class="twtr-avatar">           <div class="twtr-img"><a target="_blank" href="http://twitter.com/intent/user?screen_name=' + e.user + '"><img alt="' + e.user + ' profile" src="' + d(e.avatar) + '"></a></div>         </div>         <div class="twtr-tweet-text">           <p>             <a target="_blank" href="http://twitter.com/intent/user?screen_name=' + e.user + '" class="twtr-user">' + e.user + "</a> " + e.tweet + '             <em>            <a target="_blank" class="twtr-timestamp" time="' + e.timestamp + '" href="http://twitter.com/' + e.user + "/status/" + e.id + '">' + e.created_at + '</a> &middot;            <a target="_blank" class="twtr-reply" href="http://twitter.com/intent/tweet?in_reply_to=' + e.id + '">reply</a> &middot;             <a target="_blank" class="twtr-rt" href="http://twitter.com/intent/retweet?tweet_id=' + e.id + '">retweet</a> &middot;             <a target="_blank" class="twtr-fav" href="http://twitter.com/intent/favorite?tweet_id=' + e.id + '">favorite</a>             </em>           </p>         </div>       </div>',
                            n = document.createElement("div");
                        n.id = "tweet-id-" + ++o._tweetCount, n.className = "twtr-tweet", n.innerHTML = t, this.element = n
                    }
                    var a = window.twttr || {},
                        l = location.protocol.match(/https/),
                        c = /^.+\/profile_images/,
                        u = "https://s3.amazonaws.com/twitter_production/profile_images",
                        d = function(e) {
                            return l ? e.replace(c, u) : e
                        },
                        h = {},
                        m = function(e) {
                            var t = h[e];
                            return t || (t = new RegExp("(?:^|\\s+)" + e + "(?:\\s+|$)"), h[e] = t), t
                        },
                        f = function(e, t, n, i) {
                            for (var t = t || "*", n = n || document, s = [], r = n.getElementsByTagName(t), o = m(e), a = 0, l = r.length; l > a; ++a) o.test(r[a].className) && (s[s.length] = r[a], i && i.call(r[a], r[a]));
                            return s
                        },
                        p = function() {
                            var e = navigator.userAgent;
                            return {
                                ie: e.match(/MSIE\s([^;]*)/)
                            }
                        }(),
                        g = function(e) {
                            return "string" == typeof e ? document.getElementById(e) : e
                        },
                        v = function(e) {
                            return e.replace(/^\s+|\s+$/g, "")
                        },
                        y = function() {
                            var e = self.innerHeight,
                                t = document.compatMode;
                            return (t || p.ie) && (e = "CSS1Compat" == t ? document.documentElement.clientHeight : document.body.clientHeight), e
                        },
                        w = function(e) {
                            try {
                                return e && 3 == e.nodeType ? e.parentNode : e
                            } catch (t) {}
                        },
                        b = function(e) {
                            var t = e.relatedTarget;
                            return t || ("mouseout" == e.type ? t = e.toElement : "mouseover" == e.type && (t = e.fromElement)), w(t)
                        },
                        _ = function(e, t) {
                            t.parentNode.insertBefore(e, t.nextSibling)
                        },
                        x = function(e) {
                            try {
                                e.parentNode.removeChild(e)
                            } catch (t) {}
                        },
                        E = function(e) {
                            return e.firstChild
                        },
                        S = function(e) {
                            for (var t = b(e); t && t != this;) try {
                                t = t.parentNode
                            } catch (n) {
                                t = this
                            }
                            return t != this ? !0 : !1
                        },
                        C = function() {
                            return document.defaultView && document.defaultView.getComputedStyle ? function(e, t) {
                                var n = null,
                                    i = document.defaultView.getComputedStyle(e, "");
                                i && (n = i[t]);
                                var s = e.style[t] || n;
                                return s
                            } : document.documentElement.currentStyle && p.ie ? function(e, t) {
                                var n = e.currentStyle ? e.currentStyle[t] : null;
                                return e.style[t] || n
                            } : void 0
                        }(),
                        k = {
                            has: function(e, t) {
                                return new RegExp("(^|\\s)" + t + "(\\s|$)").test(g(e).className)
                            },
                            add: function(e, t) {
                                this.has(e, t) || (g(e).className = v(g(e).className) + " " + t)
                            },
                            remove: function(e, t) {
                                this.has(e, t) && (g(e).className = g(e).className.replace(new RegExp("(^|\\s)" + t + "(\\s|$)", "g"), ""))
                            }
                        },
                        j = {
                            add: function(e, t, n) {
                                e.addEventListener ? e.addEventListener(t, n, !1) : e.attachEvent("on" + t, function() {
                                    n.call(e, window.event)
                                })
                            },
                            remove: function(e, t, n) {
                                e.removeEventListener ? e.removeEventListener(t, n, !1) : e.detachEvent("on" + t, n)
                            }
                        },
                        T = function() {
                            function e(e) {
                                return parseInt(e.substring(0, 2), 16)
                            }

                            function t(e) {
                                return parseInt(e.substring(2, 4), 16)
                            }

                            function n(e) {
                                return parseInt(e.substring(4, 6), 16)
                            }
                            return function(i) {
                                return [e(i), t(i), n(i)]
                            }
                        }(),
                        P = {
                            bool: function(e) {
                                return "boolean" == typeof e
                            },
                            def: function(e) {
                                return !("undefined" == typeof e)
                            },
                            number: function(e) {
                                return "number" == typeof e && isFinite(e)
                            },
                            string: function(e) {
                                return "string" == typeof e
                            },
                            fn: function(e) {
                                return "function" == typeof e
                            },
                            array: function(e) {
                                return e ? P.number(e.length) && P.fn(e.splice) : !1
                            }
                        },
                        O = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                        $ = function(e) {
                            function t() {
                                var e = new Date;
                                return e.getDate() != n.getDate() || e.getYear() != n.getYear() || e.getMonth() != n.getMonth() ? " - " + O[n.getMonth()] + " " + n.getDate() + ", " + n.getFullYear() : ""
                            }
                            var n = new Date(e);
                            p.ie && (n = Date.parse(e.replace(/( \+)/, " UTC$1")));
                            var i = "",
                                s = function() {
                                    var e = n.getHours();
                                    return e > 0 && 13 > e ? (i = "am", e) : 1 > e ? (i = "am", 12) : (i = "pm", e - 12)
                                }(),
                                r = n.getMinutes();
                            n.getSeconds();
                            return s + ":" + r + i + t()
                        },
                        A = function(e) {
                            var t = new Date,
                                n = new Date(e);
                            p.ie && (n = Date.parse(e.replace(/( \+)/, " UTC$1")));
                            var i = t - n,
                                s = 1e3,
                                r = 60 * s,
                                o = 60 * r,
                                a = 24 * o;
                            return isNaN(i) || 0 > i ? "" : 2 * s > i ? "right now" : r > i ? Math.floor(i / s) + " seconds ago" : 2 * r > i ? "about 1 minute ago" : o > i ? Math.floor(i / r) + " minutes ago" : 2 * o > i ? "about 1 hour ago" : a > i ? Math.floor(i / o) + " hours ago" : i > a && 2 * a > i ? "yesterday" : 365 * a > i ? Math.floor(i / a) + " days ago" : "over a year ago"
                        };
                    a.txt.autoLink = function(e, t) {
                            return t = options_links = t || {}, t.hasOwnProperty("extraHtml") && (options_links = n(t), delete options_links.extraHtml), a.txt.autoLinkUsernamesOrLists(a.txt.autoLinkUrlsCustom(a.txt.autoLinkHashtags(e, t), options_links), t)
                        }, TWTR.Widget.ify = {
                            autoLink: function(e) {
                                return options = {
                                    extraHtml: "target=_blank",
                                    target: "_blank",
                                    urlEntities: []
                                }, e.needle.entities && (e.needle.entities.urls && (options.urlEntities = e.needle.entities.urls), e.needle.entities.media && (options.urlEntities = options.urlEntities.concat(e.needle.entities.media))), a && a.txt ? a.txt.autoLink(e.needle.text, options).replace(/([@ï¼ ]+)(<[^>]*>)/g, "$2$1") : e.needle.text
                            }
                        }, i.prototype = {
                            start: function() {
                                return this.stop().run(), this
                            },
                            stop: function() {
                                return this.worker && window.clearTimeout(this.worker), this
                            },
                            run: function() {
                                var e = this;
                                this.job(function() {
                                    e.decayRate = e.decayFn() ? Math.max(1, e.decayRate / e.decayMultiplier) : e.decayRate * e.decayMultiplier;
                                    var t = e.interval * e.decayRate;
                                    t = t >= e.maxDecayTime ? e.maxDecayTime : t, t = Math.floor(t), e.worker = window.setTimeout(function() {
                                        e.run.call(e)
                                    }, t)
                                })
                            },
                            destroy: function() {
                                return this.stop(), this.decayRate = 1, this
                            }
                        }, s.prototype = {
                            set: function(e) {
                                this.haystack = e
                            },
                            add: function(e) {
                                this.haystack.unshift(e)
                            },
                            start: function() {
                                if (this.timer) return this;
                                this._job();
                                var e = this;
                                return this.timer = setInterval(function() {
                                    e._job.call(e)
                                }, this.time), this
                            },
                            stop: function() {
                                return this.timer && (window.clearInterval(this.timer), this.timer = null), this
                            },
                            _next: function() {
                                var e = this.haystack.shift();
                                return e && this.loop && this.haystack.push(e), e || null
                            },
                            _job: function() {
                                var e = this._next();
                                return e && this.callback(e), this
                            }
                        }, o._tweetCount = 0, a.loadStyleSheet = function(e, t) {
                            if (!TWTR.Widget.loadingStyleSheet) {
                                TWTR.Widget.loadingStyleSheet = !0;
                                var n = document.createElement("link");
                                n.href = e, n.rel = "stylesheet", n.type = "text/css", document.getElementsByTagName("head")[0].appendChild(n);
                                var i = setInterval(function() {
                                    var e = C(t, "position");
                                    "relative" == e && (clearInterval(i), i = null, TWTR.Widget.hasLoadedStyleSheet = !0)
                                }, 50)
                            }
                        },
                        function() {
                            var e = !1;
                            a.css = function(t) {
                                function n() {
                                    document.getElementsByTagName("head")[0].appendChild(i)
                                }
                                var i = document.createElement("style");
                                if (i.type = "text/css", p.ie) i.styleSheet.cssText = t;
                                else {
                                    var s = document.createDocumentFragment();
                                    s.appendChild(document.createTextNode(t)), i.appendChild(s)
                                }!p.ie || e ? n() : window.attachEvent("onload", function() {
                                    e = !0, n()
                                })
                            }
                        }(), TWTR.Widget.isLoaded = !1, TWTR.Widget.loadingStyleSheet = !1, TWTR.Widget.hasLoadedStyleSheet = !1, TWTR.Widget.WIDGET_NUMBER = 0, TWTR.Widget.REFRESH_MIN = 6e3, TWTR.Widget.ENTITY_RANGE = 100, TWTR.Widget.ENTITY_PERCENTAGE = 80, TWTR.Widget.matches = {
                            mentions: /^@[a-zA-Z0-9_]{1,20}\b/,
                            any_mentions: /\b@[a-zA-Z0-9_]{1,20}\b/
                        }, TWTR.Widget.jsonP = function(e, t) {
                            var n = document.createElement("script"),
                                i = document.getElementsByTagName("head")[0];
                            return n.type = "text/javascript", n.src = e, i.insertBefore(n, i.firstChild), t(n), n
                        }, TWTR.Widget.randomNumber = function(e) {
                            return r = Math.floor(Math.random() * e), r
                        }, TWTR.Widget.SHOW_ENTITIES = TWTR.Widget.randomNumber(TWTR.Widget.ENTITY_RANGE) <= TWTR.Widget.ENTITY_PERCENTAGE, TWTR.Widget.prototype = function() {
                            var n = window.twttr || {},
                                r = l ? "https://" : "http://",
                                a = "twitter.com",
                                c = r + "search." + a + "/search.",
                                u = r + "api." + a + "/1/statuses/user_timeline.",
                                h = r + a + "/favorites/",
                                m = r + "api." + a + "/1/",
                                v = 25e3,
                                w = l ? "https://twitter-widgets.s3.amazonaws.com/j/1/default.gif" : "http://widgets.twimg.com/j/1/default.gif";
                            return {
                                init: function(e) {
                                    var t = this;
                                    return this._widgetNumber = ++TWTR.Widget.WIDGET_NUMBER, TWTR.Widget["receiveCallback_" + this._widgetNumber] = function(e) {
                                        t._prePlay.call(t, e)
                                    }, this._cb = "TWTR.Widget.receiveCallback_" + this._widgetNumber, this.opts = e, this._base = c, this._isRunning = !1, this._hasOfficiallyStarted = !1, this._hasNewSearchResults = !1, this._rendered = !1, this._profileImage = !1, this._isCreator = !!e.creator, this._setWidgetType(e.type), this.timesRequested = 0, this.runOnce = !1, this.newResults = !1, this.results = [], this.jsonMaxRequestTimeOut = 19e3, this.showedResults = [], this.sinceId = 1, this.source = "TWITTERINC_WIDGET", this.id = e.id || "twtr-widget-" + this._widgetNumber, this.tweets = 0, this.setDimensions(e.width, e.height), this.interval = e.interval ? Math.max(e.interval, TWTR.Widget.REFRESH_MIN) : TWTR.Widget.REFRESH_MIN, this.format = "json", this.rpp = e.rpp || 50, this.subject = e.subject || "", this.title = e.title || "", this.setFooterText(e.footer), this.setSearch(e.search), this._setUrl(), this.theme = e.theme ? e.theme : this._getDefaultTheme(), e.id || document.write('<div class="twtr-widget" id="' + this.id + '"></div>'), this.widgetEl = g(this.id), e.id && k.add(this.widgetEl, "twtr-widget"), e.version >= 2 && !TWTR.Widget.hasLoadedStyleSheet && (l ? n.loadStyleSheet("https://twitter-widgets.s3.amazonaws.com/j/2/widget.css", this.widgetEl) : e.creator ? n.loadStyleSheet("/stylesheets/widgets/widget.css", this.widgetEl) : n.loadStyleSheet("http://widgets.twimg.com/j/2/widget.css", this.widgetEl)), this.occasionalJob = new i(function(e) {
                                        t.decay = e, t._getResults.call(t)
                                    }, function() {
                                        return t._decayDecider.call(t)
                                    }, v), this._ready = P.fn(e.ready) ? e.ready : function() {}, this._isRelativeTime = !0, this._tweetFilter = !1, this._avatars = !0, this._isFullScreen = !1, this._isLive = !0, this._isScroll = !1, this._loop = !0, this._behavior = "default", this.setFeatures(this.opts.features), this.intervalJob = new s(this.interval, this._loop, function(e) {
                                        t._normalizeTweet(e)
                                    }), this
                                },
                                setDimensions: function(e, t) {
                                    return this.wh = e && t ? [e, t] : [250, 300], "auto" == e || "100%" == e ? this.wh[0] = "100%" : this.wh[0] = (this.wh[0] < 150 ? 150 : this.wh[0]) + "px", this.wh[1] = (this.wh[1] < 100 ? 100 : this.wh[1]) + "px", this
                                },
                                setRpp: function(e) {
                                    var e = parseInt(e);
                                    return this.rpp = P.number(e) && e > 0 && 100 >= e ? e : 30, this
                                },
                                _setWidgetType: function(e) {
                                    switch (this._isSearchWidget = !1, this._isProfileWidget = !1, this._isFavsWidget = !1, this._isListWidget = !1, e) {
                                        case "profile":
                                            this._isProfileWidget = !0;
                                            break;
                                        case "search":
                                            this._isSearchWidget = !0, this.search = this.opts.search;
                                            break;
                                        case "faves":
                                        case "favs":
                                            this._isFavsWidget = !0;
                                            break;
                                        case "list":
                                        case "lists":
                                            this._isListWidget = !0
                                    }
                                    return this
                                },
                                setFeatures: function(e) {
                                    if (e) {
                                        if (P.def(e.filters) && (this._tweetFilter = e.filters), P.def(e.dateformat) && (this._isRelativeTime = !("absolute" === e.dateformat)), P.def(e.fullscreen) && P.bool(e.fullscreen) && e.fullscreen) {
                                            this._isFullScreen = !0, this.wh[0] = "100%", this.wh[1] = y() - 90 + "px";
                                            var t = this;
                                            j.add(window, "resize", function(e) {
                                                t.wh[1] = y(), t._fullScreenResize()
                                            })
                                        }
                                        if (P.def(e.loop) && P.bool(e.loop) && (this._loop = e.loop), P.def(e.behavior) && P.string(e.behavior)) switch (e.behavior) {
                                            case "all":
                                                this._behavior = "all";
                                                break;
                                            case "preloaded":
                                                this._behavior = "preloaded";
                                                break;
                                            default:
                                                this._behavior = "default"
                                        }
                                        if (P.def(e.avatars) && P.bool(e.avatars))
                                            if (e.avatars) {
                                                var i = this._isFullScreen ? "90px" : "40px";
                                                n.css("#" + this.id + " .twtr-avatar { display: block; } #" + this.id + " .twtr-user { display: inline; } #" + this.id + " .twtr-tweet-text { margin-left: " + i + "; }"), this._avatars = !0
                                            } else n.css("#" + this.id + " .twtr-avatar { display: none; } #" + this.id + " .twtr-tweet-text { margin-left: 0; }"), this._avatars = !1;
                                        else this._isProfileWidget ? (this.setFeatures({
                                            avatars: !1
                                        }), this._avatars = !1) : (this.setFeatures({
                                            avatars: !0
                                        }), this._avatars = !0);
                                        P.def(e.live) && P.bool(e.live) && (this._isLive = e.live), P.def(e.scrollbar) && P.bool(e.scrollbar) && (this._isScroll = e.scrollbar)
                                    } else(this._isProfileWidget || this._isFavsWidget) && (this._behavior = "all");
                                    return this
                                },
                                _fullScreenResize: function() {
                                    f("twtr-timeline", "div", document.body, function(e) {
                                        e.style.height = y() - 90 + "px"
                                    })
                                },
                                setTweetInterval: function(e) {
                                    return this.interval = e, this
                                },
                                setBase: function(e) {
                                    return this._base = e, this
                                },
                                setUser: function(e, t) {
                                    return this.username = e, this.realname = t || " ", this._isFavsWidget ? this.setBase(h + e + ".") : this._isProfileWidget && this.setBase(u + this.format + "?screen_name=" + e), this.setSearch(" "), this
                                },
                                setList: function(e, t) {
                                    return this.listslug = t.replace(/ /g, "-").toLowerCase(), this.username = e, this.setBase(m + e + "/lists/" + this.listslug + "/statuses."), this.setSearch(" "), this
                                },
                                setProfileImage: function(e) {
                                    return this._profileImage = e, this.byClass("twtr-profile-img", "img").src = d(e), this.byClass("twtr-profile-img-anchor", "a").href = "http://twitter.com/intent/user?screen_name=" + this.username, this
                                },
                                setTitle: function(e) {
                                    return this.title = e, this.widgetEl.getElementsByTagName("h3")[0].innerHTML = this.title, this
                                },
                                setCaption: function(e) {
                                    return this.subject = e, this.widgetEl.getElementsByTagName("h4")[0].innerHTML = this.subject, this
                                },
                                setFooterText: function(e) {
                                    return this.footerText = P.def(e) && P.string(e) ? e : "Join the conversation", this._rendered && (this.byClass("twtr-join-conv", "a").innerHTML = this.footerText), this
                                },
                                setSearch: function(e) {
                                    if (this.searchString = e || "", this.search = encodeURIComponent(this.searchString), this._setUrl(), this._rendered) {
                                        var t = this.byClass("twtr-join-conv", "a");
                                        t.href = "http://twitter.com/" + this._getWidgetPath()
                                    }
                                    return this
                                },
                                _getWidgetPath: function() {
                                    return this._isProfileWidget ? this.username : this._isFavsWidget ? this.username + "/favorites" : this._isListWidget ? this.username + "/lists/" + this.listslug : "#search?q=" + this.search
                                },
                                _setUrl: function() {
                                    function e() {
                                        return "&" + +new Date + "=cachebust"
                                    }

                                    function t() {
                                        return 1 == n.sinceId ? "" : "&since_id=" + n.sinceId + "&refresh=true"
                                    }
                                    var n = this;
                                    return this._isProfileWidget ? this.url = this._includeEntities(this._base + "&callback=" + this._cb + "&include_rts=true&count=" + this.rpp + t() + "&clientsource=" + this.source) : this._isFavsWidget || this._isListWidget ? this.url = this._includeEntities(this._base + this.format + "?callback=" + this._cb + t() + "&clientsource=" + this.source) : (this.url = this._includeEntities(this._base + this.format + "?q=" + this.search + "&callback=" + this._cb + "&rpp=" + this.rpp + t() + "&clientsource=" + this.source), this.runOnce || (this.url += "&result_type=recent")), this.url += e(), this
                                },
                                _includeEntities: function(e) {
                                    return TWTR.Widget.SHOW_ENTITIES ? e + "&include_entities=true" : e
                                },
                                _getRGB: function(e) {
                                    return T(e.substring(1, 7))
                                },
                                setTheme: function(e, t) {
                                    var i = this,
                                        s = " !important",
                                        r = window.location.hostname.match(/twitter\.com/) && window.location.pathname.match(/goodies/);
                                    (t || r) && (s = ""), this.theme = {
                                        shell: {
                                            background: function() {
                                                return e.shell.background || i._getDefaultTheme().shell.background
                                            }(),
                                            color: function() {
                                                return e.shell.color || i._getDefaultTheme().shell.color
                                            }()
                                        },
                                        tweets: {
                                            background: function() {
                                                return e.tweets.background || i._getDefaultTheme().tweets.background
                                            }(),
                                            color: function() {
                                                return e.tweets.color || i._getDefaultTheme().tweets.color
                                            }(),
                                            links: function() {
                                                return e.tweets.links || i._getDefaultTheme().tweets.links
                                            }()
                                        }
                                    };
                                    var o = "#" + this.id + " .twtr-doc,                      #" + this.id + " .twtr-hd a,                      #" + this.id + " h3,                      #" + this.id + " h4 {            background-color: " + this.theme.shell.background + s + ";            color: " + this.theme.shell.color + s + ";          }          #" + this.id + " .twtr-tweet a {            color: " + this.theme.tweets.links + s + ";          }          #" + this.id + " .twtr-bd, #" + this.id + " .twtr-timeline i a,           #" + this.id + " .twtr-bd p {            color: " + this.theme.tweets.color + s + ";          }          #" + this.id + " .twtr-new-results,           #" + this.id + " .twtr-results-inner,           #" + this.id + " .twtr-timeline {            background: " + this.theme.tweets.background + s + ";          }";
                                    return p.ie && (o += "#" + this.id + " .twtr-tweet { background: " + this.theme.tweets.background + s + "; }"), n.css(o), this
                                },
                                byClass: function(e, t, n) {
                                    var i = f(e, t, g(this.id));
                                    return n ? i : i[0]
                                },
                                render: function() {
                                    var e = this;
                                    if (!TWTR.Widget.hasLoadedStyleSheet) return window.setTimeout(function() {
                                        e.render.call(e)
                                    }, 50), this;
                                    this.setTheme(this.theme, this._isCreator), this._isProfileWidget && k.add(this.widgetEl, "twtr-widget-profile"), this._isScroll && k.add(this.widgetEl, "twtr-scroll"), this._isLive || this._isScroll || (this.wh[1] = "auto"), this._isSearchWidget && this._isFullScreen && (document.title = "Twitter search: " + escape(this.searchString)), this.widgetEl.innerHTML = this._getWidgetHtml();
                                    var t = this.byClass("twtr-timeline", "div");
                                    if (this._isLive && !this._isFullScreen) {
                                        var n = function(t) {
                                                "all" !== e._behavior && S.call(this, t) && e.pause.call(e)
                                            },
                                            i = function(t) {
                                                "all" !== e._behavior && S.call(this, t) && e.resume.call(e)
                                            };
                                        this.removeEvents = function() {
                                            j.remove(t, "mouseover", n),
                                                j.remove(t, "mouseout", i)
                                        }, j.add(t, "mouseover", n), j.add(t, "mouseout", i)
                                    }
                                    return this._rendered = !0, this._ready(), this
                                },
                                removeEvents: function() {},
                                _getDefaultTheme: function() {
                                    return {
                                        shell: {
                                            background: "#8ec1da",
                                            color: "#ffffff"
                                        },
                                        tweets: {
                                            background: "#ffffff",
                                            color: "#444444",
                                            links: "#1985b5"
                                        }
                                    }
                                },
                                _getWidgetHtml: function() {
                                    function e() {
                                        return n._isProfileWidget ? '<a target="_blank" href="http://twitter.com/" class="twtr-profile-img-anchor"><img alt="profile" class="twtr-profile-img" src="' + w + '"></a>                      <h3></h3>                      <h4></h4>' : "<h3>" + n.title + "</h3><h4>" + n.subject + "</h4>"
                                    }

                                    function t() {
                                        return n._isFullScreen ? " twtr-fullscreen" : ""
                                    }
                                    var n = this,
                                        i = l ? "https://twitter-widgets.s3.amazonaws.com/i/widget-logo.png" : "http://widgets.twimg.com/i/widget-logo.png";
                                    this._isFullScreen && (i = "https://twitter-widgets.s3.amazonaws.com/i/widget-logo-fullscreen.png");
                                    var s = '<div class="twtr-doc' + t() + '" style="width: ' + this.wh[0] + ';">            <div class="twtr-hd">' + e() + '             </div>            <div class="twtr-bd">              <div class="twtr-timeline" style="height: ' + this.wh[1] + ';">                <div class="twtr-tweets">                  <div class="twtr-reference-tweet"></div>                  <!-- tweets show here -->                </div>              </div>            </div>            <div class="twtr-ft">              <div><a target="_blank" href="http://twitter.com"><img alt="" src="' + i + '"></a>                <span><a target="_blank" class="twtr-join-conv" style="color:' + this.theme.shell.color + '" href="http://twitter.com/' + this._getWidgetPath() + '">' + this.footerText + "</a></span>              </div>            </div>          </div>";
                                    return s
                                },
                                _appendTweet: function(e) {
                                    return this._insertNewResultsNumber(), _(e, this.byClass("twtr-reference-tweet", "div")), this
                                },
                                _slide: function(e) {
                                    var n = this,
                                        i = E(e).offsetHeight;
                                    return this.runOnce && new t(e, "height", {
                                        from: 0,
                                        to: i,
                                        time: 500,
                                        callback: function() {
                                            n._fade.call(n, e)
                                        }
                                    }).start(), this
                                },
                                _fade: function(e) {
                                    return t.canTransition ? (e.style.webkitTransition = "opacity 0.5s ease-out", e.style.opacity = 1, this) : (new t(e, "opacity", {
                                        from: 0,
                                        to: 1,
                                        time: 500
                                    }).start(), this)
                                },
                                _chop: function() {
                                    if (this._isScroll) return this;
                                    var e = this.byClass("twtr-tweet", "div", !0),
                                        t = this.byClass("twtr-new-results", "div", !0);
                                    if (e.length) {
                                        for (var n = e.length - 1; n >= 0; n--) {
                                            var i = e[n],
                                                s = parseInt(i.offsetTop);
                                            if (!(s > parseInt(this.wh[1]))) break;
                                            x(i)
                                        }
                                        if (t.length > 0) {
                                            var r = t[t.length - 1],
                                                o = parseInt(r.offsetTop);
                                            o > parseInt(this.wh[1]) && x(r)
                                        }
                                    }
                                    return this
                                },
                                _appendSlideFade: function(e) {
                                    var t = e || this.tweet.element;
                                    return this._chop()._appendTweet(t)._slide(t), this
                                },
                                _createTweet: function(e) {
                                    return e.tweet = TWTR.Widget.ify.autoLink(e), e.timestamp = e.created_at, e.created_at = this._isRelativeTime ? A(e.created_at) : $(e.created_at), this.tweet = new o(e), this._isLive && this.runOnce && (this.tweet.element.style.opacity = 0, this.tweet.element.style.filter = "alpha(opacity:0)", this.tweet.element.style.height = "0"), this
                                },
                                _getResults: function() {
                                    var e = this;
                                    this.timesRequested++, this.jsonRequestRunning = !0, this.jsonRequestTimer = window.setTimeout(function() {
                                        e.jsonRequestRunning && (clearTimeout(e.jsonRequestTimer), e.jsonRequestTimer = null), e.jsonRequestRunning = !1, x(e.scriptElement), e.newResults = !1, e.decay()
                                    }, this.jsonMaxRequestTimeOut), TWTR.Widget.jsonP(e.url, function(t) {
                                        e.scriptElement = t
                                    })
                                },
                                clear: function() {
                                    var t = this.byClass("twtr-tweet", "div", !0),
                                        n = this.byClass("twtr-new-results", "div", !0);
                                    return t = t.concat(n), e(t, function(e) {
                                        x(e)
                                    }), this
                                },
                                _sortByMagic: function(e) {
                                    var t = this;
                                    switch (this._tweetFilter && (this._tweetFilter.negatives && (e = e.filter(function(e) {
                                        return t._tweetFilter.negatives.test(e.text) ? void 0 : e
                                    })), this._tweetFilter.positives && (e = e.filter(function(e) {
                                        return t._tweetFilter.positives.test(e.text) ? e : void 0
                                    }))), this._behavior) {
                                        case "all":
                                            this._sortByLatest(e);
                                            break;
                                        case "preloaded":
                                        default:
                                            this._sortByDefault(e)
                                    }
                                    return this._isLive && "all" !== this._behavior && (this.intervalJob.set(this.results), this.intervalJob.start()), this
                                },
                                _sortByLatest: function(e) {
                                    return this.results = e, this.results = this.results.slice(0, this.rpp), this.results.reverse(), this
                                },
                                _sortByDefault: function(t) {
                                    var n = function(e) {
                                        return new Date(e).getTime()
                                    };
                                    this.results.unshift.apply(this.results, t), e(this.results, function(e) {
                                        e.views || (e.views = 0)
                                    }), this.results.sort(function(e, t) {
                                        return n(e.created_at) > n(t.created_at) ? -1 : n(e.created_at) < n(t.created_at) ? 1 : 0
                                    }), this.results = this.results.slice(0, this.rpp), this.results = this.results.sort(function(e, t) {
                                        return e.views < t.views ? -1 : e.views > t.views ? 1 : 0
                                    }), this._isLive || this.results.reverse()
                                },
                                _prePlay: function(e) {
                                    if (this.jsonRequestTimer && (clearTimeout(this.jsonRequestTimer), this.jsonRequestTimer = null), p.ie || x(this.scriptElement), e.error) this.newResults = !1;
                                    else if (e.results && e.results.length > 0) this.response = e, this.newResults = !0, this.sinceId = e.max_id_str, this._sortByMagic(e.results), this.isRunning() && this._play();
                                    else if ((this._isProfileWidget || this._isFavsWidget || this._isListWidget) && P.array(e) && e.length) {
                                        if (this.newResults = !0, !this._profileImage && this._isProfileWidget) {
                                            var t = e[0].user.screen_name;
                                            this.setProfileImage(e[0].user.profile_image_url), this.setTitle(e[0].user.name), this.setCaption('<a target="_blank" href="http://twitter.com/intent/user?screen_name=' + t + '">' + t + "</a>")
                                        }
                                        this.sinceId = e[0].id_str, this._sortByMagic(e), this.isRunning() && this._play()
                                    } else this.newResults = !1;
                                    this._setUrl(), this._isLive && this.decay()
                                },
                                _play: function() {
                                    var t = this;
                                    return this.runOnce && (this._hasNewSearchResults = !0), this._avatars && this._preloadImages(this.results), !this._isRelativeTime || "all" != this._behavior && "preloaded" != this._behavior || e(this.byClass("twtr-timestamp", "a", !0), function(e) {
                                        e.innerHTML = A(e.getAttribute("time"))
                                    }), this._isLive && "all" != this._behavior && "preloaded" != this._behavior || (e(this.results, function(e) {
                                        e.retweeted_status && (e = e.retweeted_status), t._isProfileWidget && (e.from_user = e.user.screen_name, e.profile_image_url = e.user.profile_image_url), (t._isFavsWidget || t._isListWidget) && (e.from_user = e.user.screen_name, e.profile_image_url = e.user.profile_image_url), e.id = e.id_str, t._createTweet({
                                            id: e.id,
                                            user: e.from_user,
                                            tweet: e.text,
                                            avatar: e.profile_image_url,
                                            created_at: e.created_at,
                                            needle: e
                                        });
                                        var n = t.tweet.element;
                                        "all" == t._behavior ? t._appendSlideFade(n) : t._appendTweet(n)
                                    }), "preloaded" == this._behavior) ? this : this
                                },
                                _normalizeTweet: function(e) {
                                    var t = this;
                                    e.views++, this._isProfileWidget && (e.from_user = t.username, e.profile_image_url = e.user.profile_image_url), (this._isFavsWidget || this._isListWidget) && (e.from_user = e.user.screen_name, e.profile_image_url = e.user.profile_image_url), this._isFullScreen && (e.profile_image_url = e.profile_image_url.replace(/_normal\./, "_bigger.")), e.id = e.id_str, this._createTweet({
                                        id: e.id,
                                        user: e.from_user,
                                        tweet: e.text,
                                        avatar: e.profile_image_url,
                                        created_at: e.created_at,
                                        needle: e
                                    })._appendSlideFade()
                                },
                                _insertNewResultsNumber: function() {
                                    if (!this._hasNewSearchResults) return void(this._hasNewSearchResults = !1);
                                    if (this.runOnce && this._isSearchWidget) {
                                        var e = this.response.total > this.rpp ? this.response.total : this.response.results.length,
                                            t = e > 1 ? "s" : "",
                                            n = this.response.warning && this.response.warning.match(/adjusted since_id/) ? "more than" : "",
                                            i = document.createElement("div");
                                        k.add(i, "twtr-new-results"), i.innerHTML = '<div class="twtr-results-inner"> &nbsp; </div><div class="twtr-results-hr"> &nbsp; </div><span>' + n + " <strong>" + e + "</strong> new tweet" + t + "</span>", _(i, this.byClass("twtr-reference-tweet", "div")), this._hasNewSearchResults = !1
                                    }
                                },
                                _preloadImages: function(t) {
                                    this._isProfileWidget || this._isFavsWidget || this._isListWidget ? e(t, function(e) {
                                        var t = new Image;
                                        t.src = d(e.user.profile_image_url)
                                    }) : e(t, function(e) {
                                        (new Image).src = d(e.profile_image_url)
                                    })
                                },
                                _decayDecider: function() {
                                    var e = !1;
                                    return this.runOnce ? this.newResults && (e = !0) : (this.runOnce = !0, e = !0), e
                                },
                                start: function() {
                                    var e = this;
                                    return this._rendered ? (this._isLive ? this.occasionalJob.start() : this._getResults(), this._isRunning = !0, this._hasOfficiallyStarted = !0, this) : (setTimeout(function() {
                                        e.start.call(e)
                                    }, 50), this)
                                },
                                stop: function() {
                                    return this.occasionalJob.stop(), this.intervalJob && this.intervalJob.stop(), this._isRunning = !1, this
                                },
                                pause: function() {
                                    return this.isRunning() && this.intervalJob && (this.intervalJob.stop(), k.add(this.widgetEl, "twtr-paused"), this._isRunning = !1), this._resumeTimer && (clearTimeout(this._resumeTimer), this._resumeTimer = null), this
                                },
                                resume: function() {
                                    var e = this;
                                    return !this.isRunning() && this._hasOfficiallyStarted && this.intervalJob && (this._resumeTimer = window.setTimeout(function() {
                                        e.intervalJob.start(), e._isRunning = !0, k.remove(e.widgetEl, "twtr-paused")
                                    }, 2e3)), this
                                },
                                isRunning: function() {
                                    return this._isRunning
                                },
                                destroy: function() {
                                    return this.stop(), this.clear(), this.runOnce = !1, this._hasOfficiallyStarted = !1, this._profileImage = !1, this._isLive = !0, this._tweetFilter = !1, this._isScroll = !1, this.newResults = !1, this._isRunning = !1, this.sinceId = 1, this.results = [], this.showedResults = [], this.occasionalJob.destroy(), this.jsonRequestRunning && clearTimeout(this.jsonRequestTimer), k.remove(this.widgetEl, "twtr-scroll"), this.removeEvents(), this
                                }
                            }
                        }()
                }();
            var i = /twitter\.com(\:\d{2,4})?\/intent\/(\w+)/,
                s = {
                    tweet: !0,
                    retweet: !0,
                    favorite: !0
                },
                o = "scrollbars=yes,resizable=yes,toolbar=no,location=yes",
                a = screen.height,
                l = screen.width;
            document.addEventListener ? document.addEventListener("click", n, !1) : document.attachEvent && document.attachEvent("onclick", n)
        }
    }(), define("/sites/default/themes/siedler/js/external/TwitterWidget.js", function() {}), require(["/sites/default/themes/siedler/js/external/TwitterWidget.js"], function() {
        twitter = function(e) {
            new TWTR.Widget({
                version: 2,
                type: "profile",
                rpp: 2,
                interval: 3e4,
                width: 229,
                height: 189,
                theme: {
                    shell: {
                        background: "#94794a",
                        color: "#ffffff"
                    },
                    tweets: {
                        background: "#362a08",
                        color: "#ffffff",
                        links: "#dfc767"
                    }
                },
                features: {
                    scrollbar: !0,
                    loop: !1,
                    live: !1,
                    behavior: "all"
                }
            }).render().setUser(e).start()
        }
    }), define("/sites/default/themes/siedler/js/common/twitter.js", function() {}), $$("html.communitypage.maintenance").length && require(["/sites/default/themes/siedler/js/common/twitter.js", "/sites/default/themes/siedler/js/common/youtube.js"]), define("/sites/default/themes/siedler/js/communitypage/site/maintenance.js", function() {}), $$(".communitypage.media_category").length && window.addEvent("domready", function() {
        var e = $$("ul.downloadSelectBox");
        e.each(function(t) {
            var n = t.getParent().getElement("a");
            n.addEvent("mouseover", function(n) {
                e.each(function(e) {
                    e.setStyle("display", "none")
                }), t.setStyle("left", n.event.layerX - 80), t.setStyle("top", n.event.layerY - 20), t.setStyle("display", "block")
            }), t.addEvent("mouseout", function(e) {
                var n = t.getElements("*");
                n.push(t), n.contains(e.relatedTarget) || t.setStyle("display", "none")
            }.bindWithEvent(t))
        })
    }), define("/sites/default/themes/siedler/js/communitypage/site/media_category.js", function() {}), define("/sites/default/themes/common/js/common/promoCountdown.js", function() {}), $$("html.communitypage.home").length && require(["/sites/default/themes/siedler/js/communitypage/site/news.js", "/sites/default/themes/siedler/js/common/PageLoadingBar.js"]), $("homePromotion") && require(["/sites/default/themes/common/js/common/promoCountdown.js"], function() {
        var e = $("homePromotion").getElements(".endDate");
        promoCountdown(e)
    }), define("/sites/default/themes/siedler/js/communitypage/site/home.js", function() {}), require(["/sites/default/themes/common/js/common/checkbox.js"], function() {
        $$("html.communitypage.reset").length && (ProfileForm = new Class({
            initialize: function(e) {
                var t = [$("passwordForm")];
                t.each(function(e) {
                    $defined(e) && new gm_Form(e, {
                        continueOnStatusFailed: !0,
                        removeValuesAfterSuccess: !0,
                        continueOnStatusOkay: !0
                    }, {
                        onStatusOkay: function(t) {
                            this._showSuccessMessage(successDialog[e.get("id")].title, successDialog[e.get("id")].message, t)
                        }.bind(this),
                        onStatusFailed: function(t) {
                            gm_DialogHandler.add(new gm_dialog_Plain(errorDialog[e.get("id")].title, errorDialog[e.get("id")].message))
                        }
                    }, e.getElement("a.submit"))
                }.bind(this)), $defined($("terms")) && new Checkbox(document.id("terms"))
            },
            _showSuccessMessage: function(e, t) {
                var n = new gm_dialog_Plain(e, t);
                n.addEvent("okay", function() {
                    location.href = "/"
                }.pass(n)), gm_DialogHandler.add(n)
            }
        }), window.addEvent("domready", function() {
            $("profile") && new ProfileForm($("profile"))
        }))
    }), define("/sites/default/themes/common/js/communitypage/site/reset.js", function() {}), $$("html.communitypage.reset").length && require(["/sites/default/themes/common/js/communitypage/site/reset.js"]), define("/sites/default/themes/siedler/js/communitypage/site/reset.js", function() {}), require([""], function() {
        $$(".communitypage.shop").length && (Product = new Class({
            id: null,
            element: null,
            _request: null,
            _handler: null,
            _url: null,
            _iframeIsOpen: !1,
            _defaultOptions: {
                maxWidth: 665,
                maxHeight: 400,
                minWidth: 665,
                minHeight: 400,
                border: 100
            },
            _waitElement: null,
            _trackingParameter: {},
            initialize: function(e) {
                this._url = "/" + languagePath + "/api/buy", this.id = e.getProperty("rel"), this.element = e, e.addEvent("click", this._click.bindWithEvent(this)), e.getParent(".bg") && (this._waitElement = e.getParent(".bg").getElement(".wait")), this._request = new gm_Request({
                    url: this._url,
                    onRequest: function() {
                        this.element.addClass("busy"), this._waitElement && this._waitElement.removeClass("hide")
                    }.bind(this),
                    onComplete: function() {
                        this.element.removeClass("busy"), this._waitElement && this._waitElement.addClass("hide")
                    }.bind(this),
                    onStatusFailed: function(e) {
                        this._error(e)
                    }.bind(this),
                    onStatusOkay: function(e) {
                        "object" === $type(e) && "object" === $type(e.paymentParameter) ? this._success(e) : new URI(e.paymentParameter).go()
                    }.bind(this)
                })
            },
            _click: function(e) {
                e.preventDefault(), e.stop(), this._iframeIsOpen !== !1 || payHandler.getIsPaymentInProgress() || (this.element.addClass("busy"), this._waitElement && this._waitElement.removeClass("hide"), this._request.get({
                    id: this.id
                }))
            },
            _success: function(e) {
                dataLayer.push({
                    event: "productBuyClick",
                    productData: e.trackingParameter
                }), Product.trackingParameter = e.trackingParameter, e.paymentParameter.iframe ? this._openIframe(e.paymentParameter.url, e.paymentParameter.dimensions) : payHandler.openPayWindow(e.paymentParameter)
            },
            _openIframe: function(e, t) {
                this._iframeIsOpen = !0;
                var n = this._getOptions(t);
                Overlay.show();
                var i = (new URI).getData("origin"),
                    s = new URI(e);
                i && s.get("host") === (new URI).get("host") && s.setData("origin", i);
                var r = new Element("div", {
                    id: "paymentIframe"
                }).inject(document.body);
                new Element("iframe", {
                    src: s.toString(),
                    width: n.minWidth,
                    height: n.minHeight
                }).inject(r);
                var o = new Element("div", {
                    html: iframeCloseButton
                }).addClass("close").inject(r).addEvent("click", function() {
                    Overlay.hide(), r.destroy(), this._iframeIsOpen = !1
                }.bind(this));
                window.closeInternalIFrameError = function() {
                    o.fireEvent("click"), gm_DialogHandler.add(new gm_dialog_Plain(commonErrorMessageTitle, commonErrorMessage, {
                        redirectOnClose: window.location
                    }))
                }.bind(this), new gm_ResizeElement(r.getElement("iframe"), n), new gm_CenterElement(r, null, -15)
            },
            _getOptions: function(e) {
                return Object.merge(this._defaultOptions, e)
            },
            _error: function(e) {
                gm_DialogHandler.add(new gm_dialog_Plain(e.title, e.text))
            }
        }), UltimatePayHandler = new Class({
            payingSuccess: null,
            ultimatePayParams: null,
            trackingParams: null,
            _request: null,
            _paymentInProgress: !1,
            initDone: !1,
            container: null,
            initialize: function() {},
            init: function() {
                if (!this.initDone) {
                    if ("object" != typeof ulp) return void gm_DialogHandler.add(new gm_dialog_Plain(commonErrorMessageTitle, commonErrorMessage, {
                        redirectOnClose: window.location
                    }));
                    if (this.initDone = !0, this.container = new Element("div", {
                            id: "ulpbox"
                        }).inject(document.body), new Element("div", {
                            id: "div_b"
                        }).inject(this.container), "undefined" != typeof showIframeDescription && showIframeDescription && "undefined" != typeof iframeDescription && iframeDescription) {
                        var e = new Element("p", {
                            id: "ulpboxdesc"
                        }).inject(this.container, "bottom");
                        e.innerHTML = iframeDescription
                    }
                    new gm_CenterElement(this.container), ulp.on("closeLB", function(e) {
                        this.processCloseLightbox()
                    }.bind(this)), ulp.on("paymentSuccess", function(e) {
                        this.payingSuccess = !0
                    }.bind(this)), window.addEvent("unload", this.onUnload.bind(this))
                }
            },
            openPayWindow: function(e) {
                this.init(), this.initDone && (this._paymentInProgress = !0, this.ultimatePayParams = e, startPaymentProcess = !0, ultimatePayParams = e, this.payingSuccess = !1, Overlay.getInstance().show(), this.container.show(), ulp.ultimatePay = !0, e.testmode && (ulp.upLiveUrl = "https://upay2sbxwww.ultimatepay.com/app/api/live/?"), ulp.displayUltimatePay())
            },
            processCloseLightbox: function() {
                if (this.container.hide(), this.payingSuccess) {
                    gm_DialogHandler.add(new gm_dialog_Plain(dialogText.paySuccess.title, dialogText.paySuccess.message));
                    var e = new Tracker;
                    e.track("special", "afterBuy", Product.trackingParameter)
                } else Overlay.getInstance().hide();
                this._paymentInProgress = !1
            },
            getIsPaymentInProgress: function() {
                return this._paymentInProgress
            },
            onUnload: function() {
                this.getIsPaymentInProgress() && this.processCloseLightbox()
            }
        }), window.addEvent("domready", function() {
            if ($("shop")) {
                if (document.id("countryDropdown")) {
                    var e = Dropdowns.getInstance().get(document.id("countryDropdown"));
                    e && (e.addEvent("change", function(e) {
                        var t = document.id("countrySelectorName");
                        t && t.set("html", countryNames[e]);
                        var n = !1;
                        document.id("shop").getElements(".product[class*=country]").each(function(t) {
                            n = !0, t.hasClass("country" + e) ? t.removeClass("hide") : t.addClass("hide")
                        }), document.id("shop").getElements("span.taxInfo").each(function(t) {
                            "DE" == e ? t.hasClass("hide") && t.removeClass("hide") : t.hasClass("hide") || t.addClass("hide")
                        }), n && (i = 0, document.id("shop").getElements(".product").each(function(e) {
                            e.hasClass("hide") || (i % 2 === 0 ? e.addClass("bg0").removeClass("bg1") : e.addClass("bg1").removeClass("bg0"), i++)
                        }))
                    }), e.fireEvent("change", document.id("country").value))
                }
                if ("1" === loggedInUserSuspended && gm_DialogHandler.add(new gm_dialog_Plain(dialogText.suspended.title, dialogText.suspended.message)), document.id("shop") && (payHandler = new UltimatePayHandler, document.id("shop").getElements("a.submit").each(function(e) {
                        new Product(e)
                    })), null !== allopass)
                    if ("object" === typeOf(allopass) && allopass.payment) {
                        Overlay.show(), document.id("waitingOverlay").removeClass("hide"), new gm_CenterElement(document.id("waitingOverlay"));
                        var t = function() {
                                var e = window.location.href.toURI().clearData();
                                window.location.href = e + "#success"
                            },
                            n = new Tracker;
                        n.track("special", "afterBuy", allopass.tracking), n.addEvent("complete", function() {
                            t.delay(1e3)
                        }), t.delay(5e3)
                    } else gm_DialogHandler.add(new gm_dialog_Plain(dialogText.generalError.title, dialogText.generalError.message));
                window.location.href.match(/.*#success/) && gm_DialogHandler.add(new gm_dialog_Plain(dialogText.paySuccess.title, dialogText.paySuccess.message))
            }
        }))
    }), define("/sites/default/themes/common/js/communitypage/site/shop.js", function() {}), $$(".communitypage.shop").length && require(["/sites/default/themes/common/js/communitypage/site/shop.js"]), define("/sites/default/themes/siedler/js/communitypage/site/shop.js", function() {}), window.Modernizr = function(e, t, n) {
        function i(e) {
            w.cssText = e
        }

        function s(e, t) {
            return i(E.join(e + ";") + (t || ""))
        }

        function r(e, t) {
            return typeof e === t
        }

        function o(e, t) {
            return !!~("" + e).indexOf(t)
        }

        function a(e, t) {
            for (var i in e) {
                var s = e[i];
                if (!o(s, "-") && w[s] !== n) return "pfx" == t ? s : !0
            }
            return !1
        }

        function l(e, t, i) {
            for (var s in e) {
                var o = t[e[s]];
                if (o !== n) return i === !1 ? e[s] : r(o, "function") ? o.bind(i || t) : o
            }
            return !1
        }

        function c(e, t, n) {
            var i = e.charAt(0).toUpperCase() + e.slice(1),
                s = (e + " " + C.join(i + " ") + i).split(" ");
            return r(t, "string") || r(t, "undefined") ? a(s, t) : (s = (e + " " + k.join(i + " ") + i).split(" "), l(s, t, n))
        }

        function u() {
            f.input = function(n) {
                for (var i = 0, s = n.length; s > i; i++) O[n[i]] = n[i] in b;
                return O.list && (O.list = !!t.createElement("datalist") && !!e.HTMLDataListElement), O
            }("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")), f.inputtypes = function(e) {
                for (var i, s, r, o = 0, a = e.length; a > o; o++) b.setAttribute("type", s = e[o]), i = "text" !== b.type, i && (b.value = _, b.style.cssText = "position:absolute;visibility:hidden;", /^range$/.test(s) && b.style.WebkitAppearance !== n ? (g.appendChild(b), r = t.defaultView, i = r.getComputedStyle && "textfield" !== r.getComputedStyle(b, null).WebkitAppearance && 0 !== b.offsetHeight, g.removeChild(b)) : /^(search|tel)$/.test(s) || (i = /^(url|email)$/.test(s) ? b.checkValidity && b.checkValidity() === !1 : b.value != _)), P[e[o]] = !!i;
                return P
            }("search tel url email datetime date month week time datetime-local number range color".split(" "))
        }
        var d, h, m = "2.8.3",
            f = {},
            p = !0,
            g = t.documentElement,
            v = "modernizr",
            y = t.createElement(v),
            w = y.style,
            b = t.createElement("input"),
            _ = ":)",
            x = {}.toString,
            E = " -webkit- -moz- -o- -ms- ".split(" "),
            S = "Webkit Moz O ms",
            C = S.split(" "),
            k = S.toLowerCase().split(" "),
            j = {
                svg: "http://www.w3.org/2000/svg"
            },
            T = {},
            P = {},
            O = {},
            $ = [],
            A = $.slice,
            D = function(e, n, i, s) {
                var r, o, a, l, c = t.createElement("div"),
                    u = t.body,
                    d = u || t.createElement("body");
                if (parseInt(i, 10))
                    for (; i--;) a = t.createElement("div"), a.id = s ? s[i] : v + (i + 1), c.appendChild(a);
                return r = ["&#173;", '<style id="s', v, '">', e, "</style>"].join(""), c.id = v, (u ? c : d).innerHTML += r, d.appendChild(c), u || (d.style.background = "", d.style.overflow = "hidden", l = g.style.overflow, g.style.overflow = "hidden", g.appendChild(d)), o = n(c, e), u ? c.parentNode.removeChild(c) : (d.parentNode.removeChild(d), g.style.overflow = l), !!o
            },
            N = function(t) {
                var n = e.matchMedia || e.msMatchMedia;
                if (n) return n(t) && n(t).matches || !1;
                var i;
                return D("@media " + t + " { #" + v + " { position: absolute; } }", function(t) {
                    i = "absolute" == (e.getComputedStyle ? getComputedStyle(t, null) : t.currentStyle).position
                }), i
            },
            L = function() {
                function e(e, s) {
                    s = s || t.createElement(i[e] || "div"), e = "on" + e;
                    var o = e in s;
                    return o || (s.setAttribute || (s = t.createElement("div")), s.setAttribute && s.removeAttribute && (s.setAttribute(e, ""), o = r(s[e], "function"), r(s[e], "undefined") || (s[e] = n), s.removeAttribute(e))), s = null, o
                }
                var i = {
                    select: "input",
                    change: "input",
                    submit: "form",
                    reset: "form",
                    error: "img",
                    load: "img",
                    abort: "img"
                };
                return e
            }(),
            F = {}.hasOwnProperty;
        h = r(F, "undefined") || r(F.call, "undefined") ? function(e, t) {
            return t in e && r(e.constructor.prototype[t], "undefined")
        } : function(e, t) {
            return F.call(e, t)
        }, Function.prototype.bind || (Function.prototype.bind = function(e) {
            var t = this;
            if ("function" != typeof t) throw new TypeError;
            var n = A.call(arguments, 1),
                i = function() {
                    if (this instanceof i) {
                        var s = function() {};
                        s.prototype = t.prototype;
                        var r = new s,
                            o = t.apply(r, n.concat(A.call(arguments)));
                        return Object(o) === o ? o : r
                    }
                    return t.apply(e, n.concat(A.call(arguments)))
                };
            return i
        }), T.flexbox = function() {
            return c("flexWrap")
        }, T.canvas = function() {
            var e = t.createElement("canvas");
            return !!e.getContext && !!e.getContext("2d")
        }, T.canvastext = function() {
            return !!f.canvas && !!r(t.createElement("canvas").getContext("2d").fillText, "function")
        }, T.webgl = function() {
            return !!e.WebGLRenderingContext
        }, T.touch = function() {
            var n;
            return "ontouchstart" in e || e.DocumentTouch && t instanceof DocumentTouch ? n = !0 : D(["@media (", E.join("touch-enabled),("), v, ")", "{#modernizr{top:9px;position:absolute}}"].join(""), function(e) {
                n = 9 === e.offsetTop
            }), n
        }, T.geolocation = function() {
            return "geolocation" in navigator
        }, T.postmessage = function() {
            return !!e.postMessage
        }, T.websqldatabase = function() {
            return !!e.openDatabase
        }, T.indexedDB = function() {
            return !!c("indexedDB", e)
        }, T.hashchange = function() {
            return L("hashchange", e) && (t.documentMode === n || t.documentMode > 7)
        }, T.history = function() {
            return !!e.history && !!history.pushState
        }, T.draganddrop = function() {
            var e = t.createElement("div");
            return "draggable" in e || "ondragstart" in e && "ondrop" in e
        }, T.websockets = function() {
            return "WebSocket" in e || "MozWebSocket" in e
        }, T.rgba = function() {
            return i("background-color:rgba(150,255,150,.5)"), o(w.backgroundColor, "rgba")
        }, T.hsla = function() {
            return i("background-color:hsla(120,40%,100%,.5)"), o(w.backgroundColor, "rgba") || o(w.backgroundColor, "hsla")
        }, T.multiplebgs = function() {
            return i("background:url(https://),url(https://),red url(https://)"), /(url\s*\(.*?){3}/.test(w.background)
        }, T.backgroundsize = function() {
            return c("backgroundSize")
        }, T.borderimage = function() {
            return c("borderImage")
        }, T.borderradius = function() {
            return c("borderRadius")
        }, T.boxshadow = function() {
            return c("boxShadow")
        }, T.textshadow = function() {
            return "" === t.createElement("div").style.textShadow
        }, T.opacity = function() {
            return s("opacity:.55"), /^0.55$/.test(w.opacity)
        }, T.cssanimations = function() {
            return c("animationName")
        }, T.csscolumns = function() {
            return c("columnCount")
        }, T.cssgradients = function() {
            var e = "background-image:",
                t = "gradient(linear,left top,right bottom,from(#9f9),to(white));",
                n = "linear-gradient(left top,#9f9, white);";
            return i((e + "-webkit- ".split(" ").join(t + e) + E.join(n + e)).slice(0, -e.length)), o(w.backgroundImage, "gradient")
        }, T.cssreflections = function() {
            return c("boxReflect")
        }, T.csstransforms = function() {
            return !!c("transform")
        }, T.csstransforms3d = function() {
            var e = !!c("perspective");
            return e && "webkitPerspective" in g.style && D("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function(t, n) {
                e = 9 === t.offsetLeft && 3 === t.offsetHeight
            }), e
        }, T.csstransitions = function() {
            return c("transition")
        }, T.fontface = function() {
            var e;
            return D('@font-face {font-family:"font";src:url("https://")}', function(n, i) {
                var s = t.getElementById("smodernizr"),
                    r = s.sheet || s.styleSheet,
                    o = r ? r.cssRules && r.cssRules[0] ? r.cssRules[0].cssText : r.cssText || "" : "";
                e = /src/i.test(o) && 0 === o.indexOf(i.split(" ")[0])
            }), e
        }, T.generatedcontent = function() {
            var e;
            return D(["#", v, "{font:0/0 a}#", v, ':after{content:"', _, '";visibility:hidden;font:3px/1 a}'].join(""), function(t) {
                e = t.offsetHeight >= 3
            }), e
        }, T.video = function() {
            var e = t.createElement("video"),
                n = !1;
            try {
                (n = !!e.canPlayType) && (n = new Boolean(n), n.ogg = e.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""), n.h264 = e.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), n.webm = e.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, ""))
            } catch (i) {}
            return n
        }, T.audio = function() {
            var e = t.createElement("audio"),
                n = !1;
            try {
                (n = !!e.canPlayType) && (n = new Boolean(n), n.ogg = e.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""), n.mp3 = e.canPlayType("audio/mpeg;").replace(/^no$/, ""), n.wav = e.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""), n.m4a = (e.canPlayType("audio/x-m4a;") || e.canPlayType("audio/aac;")).replace(/^no$/, ""))
            } catch (i) {}
            return n
        }, T.localstorage = function() {
            try {
                return localStorage.setItem(v, v), localStorage.removeItem(v), !0
            } catch (e) {
                return !1
            }
        }, T.sessionstorage = function() {
            try {
                return sessionStorage.setItem(v, v), sessionStorage.removeItem(v), !0
            } catch (e) {
                return !1
            }
        }, T.webworkers = function() {
            return !!e.Worker
        }, T.applicationcache = function() {
            return !!e.applicationCache
        }, T.svg = function() {
            return !!t.createElementNS && !!t.createElementNS(j.svg, "svg").createSVGRect
        }, T.inlinesvg = function() {
            var e = t.createElement("div");
            return e.innerHTML = "<svg/>", (e.firstChild && e.firstChild.namespaceURI) == j.svg
        }, T.smil = function() {
            return !!t.createElementNS && /SVGAnimate/.test(x.call(t.createElementNS(j.svg, "animate")))
        }, T.svgclippaths = function() {
            return !!t.createElementNS && /SVGClipPath/.test(x.call(t.createElementNS(j.svg, "clipPath")))
        };
        for (var I in T) h(T, I) && (d = I.toLowerCase(), f[d] = T[I](), $.push((f[d] ? "" : "no-") + d));
        return f.input || u(), f.addTest = function(e, t) {
                if ("object" == typeof e)
                    for (var i in e) h(e, i) && f.addTest(i, e[i]);
                else {
                    if (e = e.toLowerCase(), f[e] !== n) return f;
                    t = "function" == typeof t ? t() : t, "undefined" != typeof p && p && (g.className += " " + (t ? "" : "no-") + e), f[e] = t
                }
                return f
            }, i(""), y = b = null,
            function(e, t) {
                function n(e, t) {
                    var n = e.createElement("p"),
                        i = e.getElementsByTagName("head")[0] || e.documentElement;
                    return n.innerHTML = "x<style>" + t + "</style>", i.insertBefore(n.lastChild, i.firstChild)
                }

                function i() {
                    var e = y.elements;
                    return "string" == typeof e ? e.split(" ") : e
                }

                function s(e) {
                    var t = v[e[p]];
                    return t || (t = {}, g++, e[p] = g, v[g] = t), t
                }

                function r(e, n, i) {
                    if (n || (n = t), u) return n.createElement(e);
                    i || (i = s(n));
                    var r;
                    return r = i.cache[e] ? i.cache[e].cloneNode() : f.test(e) ? (i.cache[e] = i.createElem(e)).cloneNode() : i.createElem(e), !r.canHaveChildren || m.test(e) || r.tagUrn ? r : i.frag.appendChild(r)
                }

                function o(e, n) {
                    if (e || (e = t), u) return e.createDocumentFragment();
                    n = n || s(e);
                    for (var r = n.frag.cloneNode(), o = 0, a = i(), l = a.length; l > o; o++) r.createElement(a[o]);
                    return r
                }

                function a(e, t) {
                    t.cache || (t.cache = {}, t.createElem = e.createElement, t.createFrag = e.createDocumentFragment, t.frag = t.createFrag()), e.createElement = function(n) {
                        return y.shivMethods ? r(n, e, t) : t.createElem(n)
                    }, e.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + i().join().replace(/[\w\-]+/g, function(e) {
                        return t.createElem(e), t.frag.createElement(e), 'c("' + e + '")'
                    }) + ");return n}")(y, t.frag)
                }

                function l(e) {
                    e || (e = t);
                    var i = s(e);
                    return y.shivCSS && !c && !i.hasCSS && (i.hasCSS = !!n(e, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")), u || a(e, i), e
                }
                var c, u, d = "3.7.0",
                    h = e.html5 || {},
                    m = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
                    f = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
                    p = "_html5shiv",
                    g = 0,
                    v = {};
                ! function() {
                    try {
                        var e = t.createElement("a");
                        e.innerHTML = "<xyz></xyz>", c = "hidden" in e, u = 1 == e.childNodes.length || function() {
                            t.createElement("a");
                            var e = t.createDocumentFragment();
                            return "undefined" == typeof e.cloneNode || "undefined" == typeof e.createDocumentFragment || "undefined" == typeof e.createElement
                        }()
                    } catch (n) {
                        c = !0, u = !0
                    }
                }();
                var y = {
                    elements: h.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
                    version: d,
                    shivCSS: h.shivCSS !== !1,
                    supportsUnknownElements: u,
                    shivMethods: h.shivMethods !== !1,
                    type: "default",
                    shivDocument: l,
                    createElement: r,
                    createDocumentFragment: o
                };
                e.html5 = y, l(t)
            }(this, t), f._version = m, f._prefixes = E, f._domPrefixes = k, f._cssomPrefixes = C, f.mq = N, f.hasEvent = L, f.testProp = function(e) {
                return a([e])
            }, f.testAllProps = c, f.testStyles = D, f.prefixed = function(e, t, n) {
                return t ? c(e, t, n) : c(e, "pfx")
            }, g.className = g.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (p ? " js " + $.join(" ") : ""), f
    }(this, this.document),
    function(e, t, n) {
        function i(e) {
            return "[object Function]" == g.call(e)
        }

        function s(e) {
            return "string" == typeof e
        }

        function r() {}

        function o(e) {
            return !e || "loaded" == e || "complete" == e || "uninitialized" == e
        }

        function a() {
            var e = v.shift();
            y = 1, e ? e.t ? f(function() {
                ("c" == e.t ? h.injectCss : h.injectJs)(e.s, 0, e.a, e.x, e.e, 1)
            }, 0) : (e(), a()) : y = 0
        }

        function l(e, n, i, s, r, l, c) {
            function u(t) {
                if (!m && o(d.readyState) && (w.r = m = 1, !y && a(), d.onload = d.onreadystatechange = null, t)) {
                    "img" != e && f(function() {
                        _.removeChild(d)
                    }, 50);
                    for (var i in k[n]) k[n].hasOwnProperty(i) && k[n][i].onload()
                }
            }
            var c = c || h.errorTimeout,
                d = t.createElement(e),
                m = 0,
                g = 0,
                w = {
                    t: i,
                    s: n,
                    e: r,
                    a: l,
                    x: c
                };
            1 === k[n] && (g = 1, k[n] = []), "object" == e ? d.data = n : (d.src = n, d.type = e), d.width = d.height = "0", d.onerror = d.onload = d.onreadystatechange = function() {
                u.call(this, g)
            }, v.splice(s, 0, w), "img" != e && (g || 2 === k[n] ? (_.insertBefore(d, b ? null : p), f(u, c)) : k[n].push(d))
        }

        function c(e, t, n, i, r) {
            return y = 0, t = t || "j", s(e) ? l("c" == t ? E : x, e, t, this.i++, n, i, r) : (v.splice(this.i++, 0, e), 1 == v.length && a()), this
        }

        function u() {
            var e = h;
            return e.loader = {
                load: c,
                i: 0
            }, e
        }
        var d, h, m = t.documentElement,
            f = e.setTimeout,
            p = t.getElementsByTagName("script")[0],
            g = {}.toString,
            v = [],
            y = 0,
            w = "MozAppearance" in m.style,
            b = w && !!t.createRange().compareNode,
            _ = b ? m : p.parentNode,
            m = e.opera && "[object Opera]" == g.call(e.opera),
            m = !!t.attachEvent && !m,
            x = w ? "object" : m ? "script" : "img",
            E = m ? "script" : x,
            S = Array.isArray || function(e) {
                return "[object Array]" == g.call(e)
            },
            C = [],
            k = {},
            j = {
                timeout: function(e, t) {
                    return t.length && (e.timeout = t[0]), e
                }
            };
        h = function(e) {
            function t(e) {
                var t, n, i, e = e.split("!"),
                    s = C.length,
                    r = e.pop(),
                    o = e.length,
                    r = {
                        url: r,
                        origUrl: r,
                        prefixes: e
                    };
                for (n = 0; o > n; n++) i = e[n].split("="), (t = j[i.shift()]) && (r = t(r, i));
                for (n = 0; s > n; n++) r = C[n](r);
                return r
            }

            function o(e, s, r, o, a) {
                var l = t(e),
                    c = l.autoCallback;
                l.url.split(".").pop().split("?").shift(), l.bypass || (s && (s = i(s) ? s : s[e] || s[o] || s[e.split("/").pop().split("?")[0]]), l.instead ? l.instead(e, s, r, o, a) : (k[l.url] ? l.noexec = !0 : k[l.url] = 1, r.load(l.url, l.forceCSS || !l.forceJS && "css" == l.url.split(".").pop().split("?").shift() ? "c" : n, l.noexec, l.attrs, l.timeout), (i(s) || i(c)) && r.load(function() {
                    u(), s && s(l.origUrl, a, o), c && c(l.origUrl, a, o), k[l.url] = 2
                })))
            }

            function a(e, t) {
                function n(e, n) {
                    if (e) {
                        if (s(e)) n || (d = function() {
                            var e = [].slice.call(arguments);
                            h.apply(this, e), m()
                        }), o(e, d, t, 0, c);
                        else if (Object(e) === e)
                            for (l in a = function() {
                                    var t, n = 0;
                                    for (t in e) e.hasOwnProperty(t) && n++;
                                    return n
                                }(), e) e.hasOwnProperty(l) && (!n && !--a && (i(d) ? d = function() {
                                var e = [].slice.call(arguments);
                                h.apply(this, e), m()
                            } : d[l] = function(e) {
                                return function() {
                                    var t = [].slice.call(arguments);
                                    e && e.apply(this, t), m()
                                }
                            }(h[l])), o(e[l], d, t, l, c))
                    } else !n && m()
                }
                var a, l, c = !!e.test,
                    u = e.load || e.both,
                    d = e.callback || r,
                    h = d,
                    m = e.complete || r;
                n(c ? e.yep : e.nope, !!u), u && n(u)
            }
            var l, c, d = this.yepnope.loader;
            if (s(e)) o(e, 0, d, 0);
            else if (S(e))
                for (l = 0; l < e.length; l++) c = e[l], s(c) ? o(c, 0, d, 0) : S(c) ? h(c) : Object(c) === c && a(c, d);
            else Object(e) === e && a(e, d)
        }, h.addPrefix = function(e, t) {
            j[e] = t
        }, h.addFilter = function(e) {
            C.push(e)
        }, h.errorTimeout = 1e4, null == t.readyState && t.addEventListener && (t.readyState = "loading", t.addEventListener("DOMContentLoaded", d = function() {
            t.removeEventListener("DOMContentLoaded", d, 0), t.readyState = "complete"
        }, 0)), e.yepnope = u(), e.yepnope.executeStack = a, e.yepnope.injectJs = function(e, n, i, s, l, c) {
            var u, d, m = t.createElement("script"),
                s = s || h.errorTimeout;
            m.src = e;
            for (d in i) m.setAttribute(d, i[d]);
            n = c ? a : n || r, m.onreadystatechange = m.onload = function() {
                !u && o(m.readyState) && (u = 1, n(), m.onload = m.onreadystatechange = null)
            }, f(function() {
                u || (u = 1, n(1))
            }, s), l ? m.onload() : p.parentNode.insertBefore(m, p)
        }, e.yepnope.injectCss = function(e, n, i, s, o, l) {
            var c, s = t.createElement("link"),
                n = l ? a : n || r;
            s.href = e, s.rel = "stylesheet", s.type = "text/css";
            for (c in i) s.setAttribute(c, i[c]);
            o || (p.parentNode.insertBefore(s, p), f(n, 0))
        }
    }(this, document), Modernizr.load = function() {
        yepnope.apply(window, [].slice.call(arguments, 0))
    }, define("/sites/default/themes/common/js/external/modernizr-2.8.3.min.js", function() {}), define("/sites/default/themes/common/js/common/ProfileForm.js", ["/sites/default/themes/common/js/common/collapsible-content.js", "/sites/default/themes/common/js/common/checkbox.js", "/sites/default/themes/common/js/common/form.js"], function() {
        ProfileForm = new Class({
            initialize: function(e) {
                e.getElements("form").each(function(e) {
                    $defined(e) && (this.formInstance = new gm_Form(e, {
                        continueOnStatusFailed: !0,
                        removeValuesAfterSuccess: e.hasClass("holdValues") ? !1 : !0,
                        continueOnStatusOkay: !0
                    }, {
                        onStatusOkay: function(t) {
                            if (e.get("id")) this._showMessage(successDialog[e.get("id")].title, successDialog[e.get("id")].message, t);
                            else {
                                var n = e.className.split(/\s+/),
                                    i = this;
                                jQuery.each(n, function(e, n) {
                                    return successDialog[n] ? (i._showMessage(successDialog[n].title, successDialog[n].message, t), !1) : void 0
                                })
                            }
                        }.bind(this),
                        onStatusFailed: function(t) {
                            if (t && t.title && t.refresh) this._showMessageRefresh(t.title, t.text, t.api, t.id);
                            else if (t && t.title) this._showMessage(t.title, t.text, "reload");
                            else if (e.get("id")) this._showMessage(errorDialog[e.get("id")].title, errorDialog[e.get("id")].message, "reload");
                            else {
                                var n = e.className.split(/\s+/),
                                    i = this;
                                jQuery.each(n, function(e, t) {
                                    return errorDialog[t] ? (i._showMessage(errorDialog[t].title, errorDialog[t].message, "reload"), !1) : void 0
                                })
                            }
                        }.bind(this)
                    }, e.getElement("a.submit")), e.getElements("input[type=checkbox]").each(function(e) {
                        new Checkbox(e)
                    }))
                }.bind(this))
            },
            _showMessage: function(e, t, n) {
                var i = null;
                "reload" == n ? i = window.location : "home" == n ? i = "/" : "string" == typeof n && n.match(/^[A-Z]{2}-[a-z]{2}$/) && (i = Language.getUrlByLanguageIdent(n)), gm_DialogHandler.add(new gm_dialog_Plain(e, t, {
                    redirectOnClose: i
                }))
            },
            _showMessageRefresh: function(e, t, n, i) {
                gm_DialogHandler.add(new gm_dialog_Refresh(e, t, n, {
                    id: i
                }))
            }
        })
    });
var Scrollable = new Class({
    Implements: [Options, Events],
    options: {
        autoHide: 1,
        fade: 1,
        className: "scrollbar",
        proportional: !0,
        proportionalMinHeight: 15
    },
    initialize: function(e, t) {
        if (this.setOptions(t), "elements" == typeOf(e)) {
            var n = [];
            return e.each(function(e) {
                n.push(new Scrollable(e, t))
            }), n
        }
        var i = this;
        return this.element = document.id(e), this.element ? (this.active = !1, this.container = new Element("div", {
            "class": this.options.className,
            html: '<div class="knob"></div>'
        }).inject(document.body, "bottom"), this.slider = new Slider(this.container, this.container.getElement("div"), {
            mode: "vertical",
            onChange: function(e) {
                this.element.scrollTop = (this.element.scrollHeight - this.element.offsetHeight) * (e / 100)
            }.bind(this)
        }), this.knob = this.container.getElement("div"), this.reposition(), this.options.autoHide || this.container.fade("show"), this.element.addEvents({
            mouseenter: function() {
                this.scrollHeight > this.offsetHeight && i.showContainer(), i.reposition()
            },
            mouseleave: function(e) {
                i.isInside(e) || i.active || i.hideContainer()
            },
            mousewheel: function(e) {
                e.preventDefault(), (e.wheel < 0 && this.scrollTop < this.scrollHeight - this.offsetHeight || e.wheel > 0 && this.scrollTop > 0) && (this.scrollTop = this.scrollTop - 30 * e.wheel, i.reposition())
            },
            "Scrollable:contentHeightChange": function() {
                i.fireEvent("contentHeightChange")
            }
        }), this.container.addEvent("mouseleave", function() {
            i.active || i.hideContainer()
        }), this.knob.addEvent("mousedown", function(e) {
            i.active = !0, window.addEvent("mouseup", function(e) {
                i.active = !1, i.isInside(e) || i.hideContainer(), this.removeEvents("mouseup")
            })
        }), window.addEvents({
            resize: function() {
                i.reposition.delay(50, i)
            },
            mousewheel: function() {
                i.element.isVisible() && i.reposition()
            }
        }), this) : 0
    },
    reposition: function() {
        if (function() {
                this.size = this.element.getComputedSize(), this.position = this.element.getPosition();
                var e = this.container.getSize();
                this.container.setStyle("height", this.size.height).setPosition({
                    x: this.position.x + this.size.totalWidth - e.x,
                    y: this.position.y + this.size.computedTop
                }), this.slider.autosize()
            }.bind(this).delay(50), this.options.proportional === !0) {
            if (isNaN(this.options.proportionalMinHeight) || this.options.proportionalMinHeight <= 0) throw new Error('Scrollable: option "proportionalMinHeight" is not a positive number.');
            var e = Math.abs(this.options.proportionalMinHeight),
                t = this.element.offsetHeight * (this.element.offsetHeight / this.element.scrollHeight);
            this.knob.setStyle("height", Math.max(t, e))
        }
        this.slider.set(Math.round(this.element.scrollTop / (this.element.scrollHeight - this.element.offsetHeight) * 100))
    },
    scrollBottom: function() {
        this.element.scrollTop = this.element.scrollHeight, this.reposition()
    },
    scrollTop: function() {
        this.element.scrollTop = 0, this.reposition()
    },
    isInside: function(e) {
        return e.client.x > this.position.x && e.client.x < this.position.x + this.size.totalWidth && e.client.y > this.position.y && e.client.y < this.position.y + this.size.totalHeight ? !0 : !1
    },
    showContainer: function(e) {
        this.options.autoHide && this.options.fade && !this.active || e && this.options.fade ? this.container.fade("in") : (this.options.autoHide && !this.options.fade && !this.active || e && !this.options.fade) && this.container.fade("show")
    },
    hideContainer: function(e) {},
    terminate: function() {
        this.container.destroy()
    }
});
define("/sites/default/themes/siedler/js/external/scrollable.js", function() {});
var gm = gm || {};
gm.shopCarousel = new Class({
    positionX: 0,
    entryWidth: 111,
    entryCount: null,
    slideElement: null,
    arrowLeft: null,
    arrowRight: null,
    visibleWidth: null,
    totalWidth: null,
    initialize: function(e) {
        this.domElement = e, this.entryCount = e.getElements("img").length, this.slideElement = e.getChildren("div")[0].getChildren("div")[0], this.arrowLeft = e.getChildren(".left"), this.arrowRight = e.getChildren(".right"), this.visibleWidth = e.getChildren("div")[0].getSize().x, this.totalWidth = this.slideElement.getSize().x, this.arrowLeft.addEvent("click", this.move.bind(this, -1)), this.arrowRight.addEvent("click", this.move.bind(this, 1)), e.addEvent("mousewheel", function(e) {
            var t = 0;
            e.wheel > 0 ? t = -1 : e.wheel < 0 && (t = 1), this.move(t), e.stop()
        }.bind(this)), this.updateArrows(this.getArrowsValues())
    },
    move: function(e) {
        var t = this.getArrowsValues();
        (t.left > 0 && 1 != e || t.right > 0 && 1 == e) && (1 == e ? this.positionX = this.positionX - t.right : this.positionX = this.positionX + t.left, this.slideElement.setStyle("left", this.positionX)), this.updateArrows(this.getArrowsValues())
    },
    updateArrows: function(e) {
        0 == e.left ? this.arrowLeft.addClass("hide") : this.arrowLeft.removeClass("hide"), 0 == e.right ? this.arrowRight.addClass("hide") : this.arrowRight.removeClass("hide")
    },
    getEntryWidth: function(e) {
        if (0 > e || e >= this.entryCount) return 0;
        var t = this.domElement.getElements("img")[e],
            n = t.getSize().x + t.getStyle("margin-left").toIntNoNan() + t.getStyle("margin-right").toIntNoNan() + t.getStyle("border-right-width").toIntNoNan() + t.getStyle("border-left-width").toIntNoNan();
        return n
    },
    getArrowsValues: function() {
        for (var e = -1 * this.slideElement.getStyle("left").toIntNoNan(), t = e + this.visibleWidth, n = 0, i = 0, s = 0, r = 0, o = 0; o < this.entryCount && 0 == r; o++) n = i, i = n + this.getEntryWidth(o), (n.limit(e, t) == n || i.limit(e, t) == i) && (e > n && (s = e - n), n == e && 0 != o && (s = this.getEntryWidth(o - 1)), i > t && (r = i - t), i == t && o != this.entryCount - 1 && (r = this.getEntryWidth(o + 1)));
        return {
            right: r,
            left: s
        }
    }
}), String.prototype.toIntNoNan = function() {
    return isNaN(this.toInt()) ? 0 : this.toInt()
}, define("/sites/default/themes/common/js/common/carousel.js", function() {}), require(["/sites/default/themes/common/js/common/location_hash.js", "/sites/default/themes/common/js/common/promoCountdown.js"], function() {
    var e = $$("html.kongregate").length > 0,
        t = $$("html.steam").length > 0;
    window.gm = gm || {}, gm.shop2Object = new Class({
        masterElement: null,
        iframeId: "paymentIframe",
        formCountryId: "wsc",
        formProductId: "wsp",
        formPaymentMethodId: "wspm",
        formOriginId: "wso",
        steamOverlayEnabled: !0,
        initialize: function(e) {
            this.masterElement = e, this.generalFunctionality(), this.step1Functionality(), this.step2Functionality(), this.step3Functionality(), this.registerEventListeners()
        },
        generalFunctionality: function() {
            this.allowBackButton(), this.showPageByUrl(!0), $("steps") && this.interactiveSteps($("steps")), $("payForm") && this.secondStepPay($("payForm"), ".wait", $(this.formCountryId), $(this.formProductId), $(this.formPaymentMethodId), $(this.formOriginId))
        },
        step1Functionality: function() {
            $("country") && this.countryDropDown($("country")), $("promotion") && this.promotionEndTime($(document.body).getElements(".endDate")), $("prodlist") && this.mobileProductPopup($("prodlist").getElements(".mobile .product")), $("packageWrapper") && this.packageTypesTabs($("packageWrapper")), this.step1ToStep2($(document.body).getElements("#specialOffer, #prodlist li.standard > a")), $("carousel") && new gm.shopCarousel($("carousel"))
        },
        step2Functionality: function() {
            $(document.body).getElements(".methodType h3") && this.methodTypesTabs($(document.body).getElements(".methodType h3")), $(document.body).getElements(".methodsBuy") && this.secondStepClickBuy($(document.body).getElements(".methodsBuy"), $("payForm"))
        },
        step3Functionality: function() {
            $("confirmationContent") && $("confirmationContent").getElements(".continueShoppingButton") && this.clearLinkAndGoToStep1($("confirmationContent").getElements(".continueShoppingButton")), "undefined" != typeof transactionId && (window.self !== window.parent && window.parent.postMessage('{"method":"psmPaymentSuccess", "successUrl": "' + successUrl + '"}', "*"), this.checkTransactionStatus($("currentStatus").getElement(".processing"), $("currentStatus").getElement(".done")))
        },
        registerEventListeners: function() {
            var e = this;
            window.addEventListener ? addEventListener("message", function(t) {
                e.receiveEvent(t)
            }, !1) : attachEvent("onmessage", function(t) {
                e.receiveEvent(t)
            })
        },
        receiveEvent: function(e) {
            if ("closepop" == e.data) return void this._closeIframe();
            try {
                var t = JSON.parse(e.data);
                if (void 0 != t.method) switch (t.method) {
                    case "psmPaymentSuccess":
                        this._psmPaymentSuccess(t);
                        break;
                    case "psmPaymentFailure":
                        this._psmPaymentFailure(t)
                }
            } catch (n) {}
        },
        _psmPaymentSuccess: function(e) {
            this._closeIframe();
            var t;
            t = void 0 != e.successUrl && e.successUrl.length > 0 ? e.successUrl : window.location.href, window.location.href = t
        },
        _psmPaymentFailure: function(e) {
            this._closeIframe();
            var t;
            t = void 0 != e.failureUrl && e.failureUrl.length > 0 ? e.failureUrl : window.location.href, window.location.href = t
        },
        _closeIframe: function() {
            Overlay.hide(), $(this.iframeId).destroy(), _isIframeOpen = !1
        },
        countryDropDown: function(e) {
            e && e.addEvent("change", function() {
                "" != $(this).getProperty("value") && $(this).getParent("form").submit()
            })
        },
        promotionEndTime: function(e) {
            promoCountdown(e)
        },
        mobileProductPopup: function(e) {
            var t = this;
            e.addEvent("click", function(e) {
                e.preventDefault(), t.showRequestAnimationMobile($(this).getElements(".buyButton"));
                var n = {
                    c: $(this).get("data-language"),
                    p: $(this).get("data-product"),
                    pm: $(this).get("data-pm")
                };
                t.makeOwlientRequest(n)
            })
        },
        packageTypesTabs: function(e) {
            var t = this;
            e.getElements("h3").addEvent("click", function() {
                $(this).hasClass("packages") && (t.masterElement.removeClass("mobile"), gm_LocationHash.setVar("shop", "main")), $(this).hasClass("mobile") && (t.masterElement.addClass("mobile"), gm_LocationHash.setVar("shop", "mobile"))
            })
        },
        interactiveSteps: function(e) {
            var t = this;
            e.getElements(".step1").addEvent("click", function() {
                gm_LocationHash.setVar("shop", "main"), t.showPageByUrl(!1)
            })
        },
        clearLinkAndGoToStep1: function(e) {
            e.addEvent("click", function(e) {
                gm_LocationHash.setVar("shop", "main"), e.preventDefault()
            })
        },
        methodTypesTabs: function(e) {
            var t, n = this;
            e.addEvent("click", function(e) {
                if (!$(this).hasClass("active")) {
                    for (var i = $(document.body).getElements(".methodType h3.active").getProperty("rel"), s = $(this).getProperty("rel"), r = ["methodListRecommended", "methodListExtra"], o = 0; 2 > o; o++) $(r[o]).getElements("li.infoSelected").each(function(e) {
                        e.removeClass("infoSelected")
                    });
                    $(i[0]) && $(i[0]).setStyles({
                        display: "none"
                    }), $(document.body).getElements(".methodType h3").removeClass("active"), methodsWrapper.removeAttribute("class"), methodsWrapper.addClass("clear"), methodsWrapper.addClass(s), $(this).addClass("active"), $(s).setStyles({
                        display: "block"
                    }), n.clearScrollBar(), "undefined" != typeof e && ("methodListExtra" == s ? (t = gm_LocationHash.getVar("shop"), t && (t = t.split(";"), (2 == t.length || "t2" != t[2]) && gm_LocationHash.setVar("shop", t[0] + ";" + t[1] + ";t2"))) : (t = gm_LocationHash.getVar("shop"), t && (t = t.split(";"), 3 == t.length && gm_LocationHash.setVar("shop", t[0] + ";" + t[1]))))
                }
                e && e.preventDefault()
            })
        },
        clearScrollBar: function() {
            $(document.body).getElements(".wsscrollbar")[0] && $(document.body).getElements(".wsscrollbar")[0].dispose()
        },
        checkTransactionStatus: function(e, t) {
            var n = this;
            n.checkWsPaymentStatus = setInterval(function() {
                i.send()
            }, 3e4);
            var i = new Request({
                url: "/" + languagePath + "/api/shop2/transactionstatus/" + transactionId,
                method: "POST",
                onSuccess: function(i, s) {
                    var r = JSON.parse(i);
                    switch (r.status) {
                        case "OKAY":
                            "done" == r.data && (e.addClass("hide"), t.removeClass("hide"), clearInterval(n.checkPaymentStatus));
                            break;
                        case "FAILED":
                            gm_DialogHandler.add(new gm_dialog_Plain(r.data.title, r.data.text)), clearInterval(n.checkWsPaymentStatus)
                    }
                },
                onFailure: function(e) {
                    alert("Something went wrong with your request. Please try again later.")
                }
            });
            window.addEvent("domready", function() {
                i.send()
            })
        },
        secondStepPay: function(e, t, n, i, s, r) {
            var o = this;
            e.addEvent("submit", function(e) {
                if ("" == s) return gm_DialogHandler.add(new gm_dialog_Plain(dialogText.selectMessage.title, dialogText.selectMessage.message)), o.masterElement.getElements(t) && $$(t).dispose(), !1;
                var a = {
                    c: n.getProperty("value"),
                    p: i.getProperty("value"),
                    pm: s.getProperty("value"),
                    o: r.getProperty("value")
                };
                o.makeOwlientRequest(a)
            })
        },
        secondStepClickBuy: function(e, t) {
            var n = this;
            e.addEvent("click", function(e) {
                e.preventDefault(), $(this).hasClass("active") ? (n.masterElement.getElement(".wait") || n.showRequestAnimationMobile($(this)), e.stopPropagation(), t.fireEvent("submit")) : gm_DialogHandler.add(new gm_dialog_Plain(dialogText.selectMessage.title, dialogText.selectMessage.message))
            })
        },
        step1ToStep2: function(e) {
            var n = this;
            e.addEvent("click", function(e) {
                e.preventDefault(), t && "function" == typeof showContent && showContent("shop2"), gm_LocationHash.setVar("shop", this.href.split("#").pop()), n.selectPackageForStep2($(this))
            })
        },
        step2AddPaymentMethod: function(e, t) {
            var n = this,
                i = new Element("li", {
                    "data-price": t.price
                }),
                s = new Element("section", {
                    "class": "method"
                }),
                r = new Element("a", {
                    "class": "seeInfo",
                    href: "#methodListExtra"
                });
            r.addEvent("click", function(e) {
                e.preventDefault(), $(this).getParents("li").hasClass("infoSelected") && ($(this).getParents("li").getSiblings().each(function(e) {
                    e.removeClass("infoSelected"), n.clearScrollBar()
                }), $(this).getParents("li").addClass("infoSelected"), new Scrollable($(this).getParents("li").getElements(".info")[0], {
                    className: "wsscrollbar",
                    fade: 0
                }))
            }), $(s).adopt(r), $(s).adopt(new Element("a", {
                "class": "paymentMethod",
                href: "#2",
                rel: t.code
            }).setStyle("background-image", "url('" + t.logo + "')")), $(s).adopt(new Element("a", {
                "class": "button",
                href: "#2",
                rel: t.code,
                title: t.title,
                text: t.title
            })), $(i).adopt(s);
            var o = new Element("section", {
                    "class": "info"
                }),
                a = new Element("a", {
                    "class": "seeMethod",
                    href: "#methodListExtra"
                });
            a.addEvent("click", function(e) {
                e.preventDefault(), $(this).getParents("li").toggleClass("infoSelected"), n.clearScrollBar()
            }), $(o).adopt(a), $(o).adopt(new Element("p", {
                "class": "title",
                text: t.title
            })), $(o).adopt(new Element("p", {
                "class": "description",
                text: t.description
            })), $(i).adopt(o), $(e).adopt(i)
        },
        step2SetselectedPackage: function(e, t, n, i, s, r, o) {
            t ? e.getElements(".specialPImage").setStyles({
                "background-image": "url('" + t + "')",
                display: "block"
            }) : e.getElements(".specialPImage").setStyles({
                "background-image": "none",
                display: "none"
            }), i ? (e.getElements("header.title .main").addClass("hasExtra").set("text", n), e.getElements("header.title .info").removeClass("hide").set("text", i), e.getElements(".shop2-promo").removeClass("hide").set("text", "+" + s + "%")) : (e.getElements("header.title .main").removeClass("hasExtra").set("text", n), e.getElements("header.title .info").addClass("hide").set("text", ""), e.getElements(".shop2-promo").addClass("hide").set("text", "")), e.getElements(".offer .price").set("text", "");
            var a = e.getElements(".offer .price"),
                l = 0;
            for (var c in o) l > 0 && a.adopt(new Element("br")), a.adopt(new Element("span", {
                text: o[c].min,
                "class": "priceContainer"
            })), o[c].min != o[c].max && (a.innetHTML = a.innetHTML + " - ", a.adopt(new Element("span", {
                text: o[c].max,
                "class": "priceContainer"
            }))), l++;
            l > 1 ? e.getElements(".offer").addClass("multi") : e.getElements(".offer").removeClass("multi")
        },
        paymentMethodClick: function(e, t) {
            var n = this;
            e.addEvent("click", function(e) {
                e.preventDefault();
                var i = String($(this.getParent()).getElements(".paymentMethod").getStyle("background-image")).replace(/^url\(['"]{0,1}(.+)['"]{0,1}\)/, "$1");
                n.step2PickPaymentMethod($(this).getProperty("rel"), i, $(this).getParent("li").getProperty("data-price"), t.getElements(".offer")[0]);
                for (var s = ["methodListRecommended", "methodListExtra"], r = 0; 2 > r; r++) $(s[r]).getElements("li").each(function(e) {
                    e.removeClass("infoSelected"), e.removeClass("methodSelected")
                });
                $(this).getParents("li")[0].addClass("methodSelected"), n.clearScrollBar()
            })
        },
        step2PickPaymentMethod: function(e, t, n, i) {
            $(document.body).getElements(".methodsBuy").addClass("active"), $(document.body).getElements(".payingWith").addClass("active"), $(this.formPaymentMethodId).set("value", e), $(document.body).getElements(".payingWith img").setProperty("src", t), i.getElements(".price").set("html", n), i.hasClass("multi") && i.removeClass("multi")
        },
        closeIframe: function(e, t) {
            new Element("div", {
                html: iframeCloseButton
            }).addClass("close").inject(e).addEvent("click", function() {
                Overlay.hide(), e.destroy(), $$(".showWhileBusy").hide(), $$(".buyButton").removeClass("inactive"), $$(".buttonText").setStyle("opacity", "100"), _isIframeOpen = !1
            }.bind(t))
        },
        steamOverlayIsDisabled: function() {
            this.steamOverlayEnabled = !1
        },
        openIframe: function(e, n, i) {
            if (t && this.steamOverlayEnabled && "function" == typeof openShopInOverlay) return void openShopInOverlay(e, this);
            var s = {
                maxWidth: n,
                maxHeight: i,
                minWidth: n,
                minHeight: i,
                border: 100
            };
            Overlay.show();
            var r = new Element("div", {
                id: this.iframeId
            }).inject(document.body);
            new Element("iframe", {
                src: e.toString(),
                width: s.minWidth,
                height: s.minHeight
            }).inject(r), this.closeIframe(r, this), window.closeInternalIFrameError = function() {
                closeButton.fireEvent("click"), gm_DialogHandler.add(new gm_dialog_Plain(commonErrorMessageTitle, commonErrorMessage, {
                    redirectOnClose: window.location
                }))
            }.bind(this), new gm_ResizeElement(r.getElement("iframe"), s), new gm_CenterElement(r, null, -15)
        },
        makeOwlientRequest: function(e) {
            var n = this;
            if (_isIframeOpen) return !1;
            _isIframeOpen = !0;
            var i = new Request({
                url: "/" + languagePath + "/api/shop2/buy",
                method: "POST",
                data: e,
                onSuccess: function(e, i) {
                    var s = JSON.parse(e);
                    switch (s.status) {
                        case "OKAY":
                            "object" === $type(s.data) && (0 === s.data.newWindow || t ? n.openIframe(s.data.url, s.data.width, s.data.height) : (window.open(s.data.url, "_blank"), _isIframeOpen = !1));
                            break;
                        case "FAILED":
                            gm_DialogHandler.add(new gm_dialog_Plain(s.data.title, s.data.text)), _isIframeOpen = !1
                    }
                },
                onFailure: function(e) {
                    _isIframeOpen = !1, alert("Something went wrong with your request. Please try again later.")
                },
                onRequest: function() {}.bind(this),
                onComplete: function() {
                    n.removeRequestAnimation()
                }.bind(this)
            });
            i.send()
        },
        allowBackButton: function() {
            var e = this;
            window.addEventListener("hashchange", function(t) {
                e.showPageByUrl(!1)
            })
        },
        showPageByUrl: function(e) {
            var t, n = gm_LocationHash.getVar("shop");
            if (null != n) switch (n) {
                case "mobile":
                    e && $("packageWrapper").getElements(".mobile").fireEvent("click");
                    break;
                case "main":
                case "":
                    e || (this.masterElement.removeClass("step2"), this.clearScrollBar(), this.masterElement.removeClass("step3"), "undefined" != typeof this.checkWsPaymentStatus && clearInterval(this.checkWsPaymentStatus), this.masterElement.removeClass("mobile"));
                default:
                    t = n.split(";"), "s2" == t[0] && "undefined" != typeof t[1] && e && (this.selectPackageForStep2($$("a[data-product=" + t[1].replace(/[^a-z0-9_]/gi, "") + "]")[0]), $$("h3[rel=methodListRecommended]").fireEvent("click"))
            }
        },
        selectPackageForStep2: function(t) {
            if (e) {
                if (_isIframeOpen) return;
                _isIframeOpen = !0;
                var n = t.getProperty("data-product"),
                    i = {
                        p: n
                    },
                    s = new Request({
                        url: "/" + languagePath + "/api/partnerintegration/kongregate/callback?event=buy",
                        method: "POST",
                        data: i,
                        onSuccess: function(e, t) {
                            var n = JSON.parse(e);
                            switch (n.status) {
                                case "OKAY":
                                    if ("object" == typeof n.data && n.data.hasOwnProperty("order_info")) {
                                        var i = function(e) {
                                            e.success && gm_DialogHandler.add(new gm_dialog_Plain(dialogText.paySuccess.title, dialogText.paySuccess.message)), _isIframeOpen = !1
                                        };
                                        (new KongregateAPI).purchaseItemsRemote(n.data.order_info, i)
                                    } else _isIframeOpen = !1;
                                    break;
                                case "FAILED":
                                    gm_DialogHandler.add(new gm_dialog_Plain(n.data.title, n.data.text)), _isIframeOpen = !1
                            }
                        },
                        onFailure: function(e) {
                            alert("Something went wrong with your request. Please try again later."), _isIframeOpen = !1
                        }
                    });
                return void s.send()
            }
            this.step2SetselectedPackage($("selectedPackage"), t.getProperty("data-image"), t.getProperty("data-quantity"), t.getProperty("data-promoAmount"), t.getProperty("data-promoPercent"), t.getProperty("data-taxline"), JSON.parse(t.getProperty("data-prices")));
            var r = JSON.parse(t.getProperty("data-pms"));
            r.length < 7 ? $("methodsWrapper").getElements("header.methodType").addClass("hide") : $("methodsWrapper").getElements("header.methodType").removeClass("hide"), $("methodsWrapper").removeAttribute("class"), $("methodsWrapper").addClass("clear"), $("methodListRecommended").empty(), $("methodListExtra").empty();
            for (var o = $("methodListRecommended"), a = 0; a < r.length; a++) this.step2AddPaymentMethod(o, r[a]), 5 == a && (o = $("methodListExtra")), a > 5 && $("methodsWrapper").addClass("methodListRecommended");
            this.paymentMethodClick($(document.body).getElements(".method .paymentMethod, .method .button"), $("selectedPackage")), $(this.formProductId).set("value", t.getProperty("data-product")), this.masterElement.addClass("step2").removeClass("mobile"), $(document.body).getElements(".methodsBuy").removeClass("active"), $(document.body).getElements(".payingWith").removeClass("active"), $(document.body).getElements(".payingWith img").setProperty("src", "data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="), $(this.formPaymentMethodId).set("value", ""), $("methodsWrapper").getFirst(".methodType h3").fireEvent("click")
        },
        removeRequestAnimation: function() {
            this.masterElement && this.masterElement.getElement(".wait") && this.masterElement.getElement(".wait").dispose(), $(document).getElement(".busyImg") && $(document).getElement(".busyImg").removeClass("busyImg").setStyle("padding", "0").set("src", buyImg)
        },
        showRequestAnimationMobile: function(e) {
            e.getElements("img")[0].set("src", waitImg).setStyle("padding", "9px 42px").addClass("busyImg")
        }
    })
}), define("/sites/default/themes/common/js/communitypage/site/shop2.js", function() {}), ($$(".communitypage.shop2").length || $$(".communitypage.spa").length) && require(["/sites/default/themes/common/js/external/modernizr-2.8.3.min.js", "/sites/default/themes/common/js/common/ProfileForm.js", "/sites/default/themes/siedler/js/external/scrollable.js", "/sites/default/themes/common/js/common/carousel.js", "/sites/default/themes/common/js/communitypage/site/shop2.js"], function() {
    window.addEvent("domready", function() {
        new gm.shop2Object($("newLO"))
    })
}), define("/sites/default/themes/siedler/js/communitypage/site/shop2.js", function() {}), require(["/sites/default/themes/common/js/common/ProfileForm.js"], function() {
    ($$(".communitypage.profile").length || $$(".communitypage.spa").length) && window.addEvent("domready", function() {
        $("btnDepartnerize") && $("btnDepartnerize").addEvent("click", function(e) {
            var t = confirm(confirmDialog.departnerizeaccountForm.message);
            t || e.stop()
        }), $("realmDropdown") && $("realmDropdownLabel") && Dropdowns.getInstance().get(document.id("realmDropdown")).initLabel(document.id("realmDropdownLabel")), $("birthdayContainer") && Dropdowns.getInstance().get(document.id("birthdayDay")).initLabel(document.id("birthdayLabel")), $("profile") && new ProfileForm($("profile")), "undefined" != typeof profileNameChangeDialogTitle && gm_DialogHandler.add(new gm_dialog_Plain(profileNameChangeDialogTitle, profileNameChangeDialogText)), $$("div.doi_forum.doi") && $$("div.doi_forum.doi").addEvent("click", function(e) {
            e.preventDefault(), gm_DialogHandler.add(new gm_dialog_DoiRepeat(window.doiText_doi_forum_title, window.doiText_doi_forum_text, this.getElement("a").get("href")))
        })
    })
}), define("/sites/default/themes/common/js/communitypage/site/profile.js", ["/sites/default/themes/common/js/common/ProfileForm.js"], function() {}), ($$(".communitypage.profile").length || $$(".communitypage.spa").length) && require(["/sites/default/themes/common/js/communitypage/site/profile.js"]), define("/sites/default/themes/siedler/js/communitypage/site/profile.js", ["/sites/default/themes/common/js/common/ProfileForm.js"], function() {}), $$("html.communitypage.profile").length && window.addEvent("domready", function() {
    jQuery.noConflict(),
        function(e) {
            function t(e) {
                return e.is(":checked")
            }

            function n() {
                r.find(".showWhileBusy").show();
                var n = {
                    ShowSettlers: t(e("#showsettlers")),
                    ShowBuildingSmoke: t(e("#showbuildingsmoke")),
                    ShowAnimals: t(e("#showanimals")),
                    ShowBuffAnimations: t(e("#showbuffanimations")),
                    UseHalfSizeGraphics: t(e("#usehalfsizegraphics"))
                };
                e.ajax({
                    url: "/api/gameserver/settings",
                    type: "PUT",
                    dataType: "text",
                    data: JSON.stringify(n),
                    success: function(e) {
                        r.find(".showWhileBusy").hide(), gm_DialogHandler.add(new gm_dialog_Plain(successDialog.gameSettingsForm.title, successDialog.gameSettingsForm.message))
                    },
                    error: function(e) {
                        r.find(".showWhileBusy").hide(), gm_DialogHandler.add(new gm_dialog_Plain(errorDialog.gameSettingsForm.title, errorDialog.gameSettingsForm.message))
                    }
                })
            }

            function i() {
                l.addClass("hide"), e.getJSON("/api/gameserver/settings").done(function(t) {
                    200 == t.code ? (null != t.data ? (e.each(t.data, function(t, n) {
                        n ? (e("#" + t.toLowerCase()).prop("checked", !0), e("#" + t.toLowerCase() + "label").addClass("checked")) : (e("#" + t.toLowerCase()).prop("checked", !1), e("#" + t.toLowerCase() + "label").removeClass("checked"))
                    }), a.addClass("hide"), o.removeClass("hide")) : (a.addClass("hide"), l.removeClass("hide")), 0 === c.length ? $("gameSettingForm").getElements("input[type=checkbox]").each(function(e) {
                        c.push(new Checkbox(e))
                    }) : e.each(c, function(e, t) {
                        t.refreshStateByInput()
                    })) : (a.addClass("hide"), l.removeClass("hide"))
                }).fail(function() {
                    a.addClass("hide"), l.removeClass("hide")
                })
            }
            var s = e(".settingsExpander"),
                r = e("#gameSettingsContainer"),
                o = e("#gameSettingForm"),
                a = e("#loadingAni"),
                l = e("#gameSettingsError"),
                c = [];
            s.click(function() {
                r.hasClass("hide") && (a.removeClass("hide"), o.addClass("hide"), i())
            }), o.find(".submit").click(function(e) {
                e.preventDefault(), n()
            })
        }(jQuery)
}), define("/sites/default/themes/siedler/js/communitypage/site/profile.gamesettings.js", function() {}), $$("html.communitypage.ranking, .spa-communitypage.ranking").length && window.addEvent("domready", function() {
    jQuery.noConflict(),
        function(e) {
            function t() {
                h == u.getMonth() + 1 && d == u.getFullYear() ? o.css("visibility", "hidden") : o.css("visibility", "visible"), h == g && d == p ? a.css("visibility", "hidden") : a.css("visibility", "visible")
            }

            function n() {
                var t = e("#rankingTable"),
                    n = e(".rankingContainer .headline"),
                    i = e("#month"),
                    o = r.find("option:selected").val(),
                    a = s.find("option:selected").val(),
                    c = "",
                    u = "",
                    p = 0,
                    g = 10,
                    v = 1;
                m.EfficiencyExpeditions = new Array, m.EfficiencyExpeditions.value = titleEfficiencyExpeditionsValue, m.EfficiencyExpeditions.extravalue = titleEfficiencyExpeditionsExtraValue, m.EfficiencyExpeditions.error = titleEfficiencyExpeditionsError, m.AverageTimeSmallExpeditions = new Array, m.AverageTimeSmallExpeditions.value = titleAverageTimeSmallExpeditionsValue, m.AverageTimeSmallExpeditions.extravalue = titleAverageTimeSmallExpeditionsExtraValue, m.AverageTimeSmallExpeditions.error = titleAverageTimeSmallExpeditionsError, m.AverageTimeMediumExpeditions = new Array, m.AverageTimeMediumExpeditions.value = titleAverageTimeMediumExpeditionsValue, m.AverageTimeMediumExpeditions.extravalue = titleAverageTimeMediumExpeditionsExtraValue, m.AverageTimeMediumExpeditions.error = titleAverageTimeMediumExpeditionsError, m.AverageTimeLargeExpeditions = new Array, m.AverageTimeLargeExpeditions.value = titleAverageTimeLargeExpeditionsValue, m.AverageTimeLargeExpeditions.extravalue = titleAverageTimeLargeExpeditionsExtraValue, m.AverageTimeLargeExpeditions.error = titleAverageTimeLargeExpeditionsError, m.MinTimeSmallExpeditions = new Array, m.MinTimeSmallExpeditions.value = titleMinTimeSmallExpeditionsValue, m.MinTimeSmallExpeditions.extravalue = titleMinTimeSmallExpeditionsExtraValue, m.MinTimeSmallExpeditions.error = titleMinTimeSmallExpeditionsError, m.MinTimeMediumExpeditions = new Array, m.MinTimeMediumExpeditions.value = titleMinTimeMediumExpeditionsValue, m.MinTimeMediumExpeditions.extravalue = titleMinTimeMediumExpeditionsExtraValue, m.MinTimeMediumExpeditions.error = titleMinTimeMediumExpeditionsError, m.MinTimeLargeExpeditions = new Array, m.MinTimeLargeExpeditions.value = titleMinTimeLargeExpeditionsValue, m.MinTimeLargeExpeditions.extravalue = titleMinTimeLargeExpeditionsExtraValue, m.MinTimeLargeExpeditions.error = titleMinTimeLargeExpeditionsError, t.html(""), l.removeClass("hide"), e.getJSON("/api/Rank/co/" + a + "/" + o + "/" + d + "/" + h + "/" + g + "/" + v + "/?playerId=" + playerId + "&country=" + currentCountry).done(function(n) {
                    200 == n.code && null != n.data ? (e.each(n.data, function(e, t) {
                        if (null != n.data[e]) {
                            var i = "";
                            0 == p && (i = 'id="firstTable"'), c = c.concat("<table " + i + ">");
                            for (var s = 0; s < n.data[e].length; s++) {
                                var r = "";
                                0 == p && (u = u.concat("<thead><tr>"), "undefined" != typeof n.data[e][s].position && (u = u.concat('<th class="col1"><div>' + titleRank + "</div></th>")), "undefined" != typeof n.data[e][s].country && (u = u.concat('<th class="col2"><div>' + titleCountry + "</div></th>")), "undefined" != typeof n.data[e][s].username && (u = u.concat('<th class="col3"><div>' + titleUsername + "</div></th>")), "undefined" != typeof n.data[e][s].value && (u = u.concat('<th class="col4"><div>' + m[a].value + "</div></th>")), "undefined" != typeof n.data[e][s].extraParams && "undefined" != typeof n.data[e][s].extraParams.extraValue && (u = u.concat('<th class="col5"><div>' + m[a].extravalue + "</div></th>")), u = u.concat("</tr></thead>")), p % 2 != 0 && (r = 'class="alt"'), c = c.concat("<tr " + r + ">"), "undefined" != typeof n.data[e][s].position && (c = c.concat('<td class="col1">' + n.data[e][s].position + "</td>")), "undefined" != typeof n.data[e][s].country && (c = c.concat('<td class="col2"><img src="/sites/default/themes/common/images/common/flags/common/' + n.data[e][s].country.toLowerCase() + '.png"></td>')), "undefined" != typeof n.data[e][s].username && (c = c.concat('<td class="col3">' + n.data[e][s].username + "</td>")), "undefined" != typeof n.data[e][s].value && (c = c.concat('<td class="col4">' + n.data[e][s].value + "</td>")), "undefined" != typeof n.data[e][s].extraParams && "undefined" != typeof n.data[e][s].extraParams.extraValue && (c = c.concat('<td class="col5">' + n.data[e][s].extraParams.extraValue + "</td>")), c = c.concat("</tr>"), p++
                            }
                            c = c.concat("</table><hr>")
                        } else 0 == p ? (c = c.concat('<p class="error">' + titleNoData + "</p><hr>"), p++) : c = c.concat('<p class="error">' + m[a].error + "</p>")
                    }), l.addClass("hide"), t.html(c), e("#firstTable").prepend(u)) : (l.addClass("hide"), t.html('<p class="error">' + titleNoData + "</p>"));
                }).fail(function() {
                    l.addClass("hide"), t.html('<p class="error">' + titleNoData + "</p>")
                }), n.html(s.find("option:selected").text() + " " + titleRanking), i.html(f[h - 1] + " " + d)
            }
            if ("undefined" != typeof titleMonthJanuary) {
                var s = e("#rankingSelection"),
                    r = e("#worldSelection"),
                    o = e("#nextMonth"),
                    a = e("#prevMonth"),
                    l = e("#loadingAni"),
                    c = 12,
                    u = new Date,
                    d = u.getFullYear(),
                    h = u.getMonth() + 1,
                    m = new Array,
                    f = new Array,
                    p = d,
                    g = h;
                for (f[0] = titleMonthJanuary, f[1] = titleMonthFebruary, f[2] = titleMonthMarch, f[3] = titleMonthApril, f[4] = titleMonthMay, f[5] = titleMonthJune, f[6] = titleMonthJuly, f[7] = titleMonthAugust, f[8] = titleMonthSeptember, f[9] = titleMonthOctober, f[10] = titleMonthNovember, f[11] = titleMonthDecember, i = 1; i < c; i++) g - 1 == 0 ? (p--, g = 12) : g--;
                s.on("chosen:ready", function() {
                    n()
                }), s.on("change", function() {
                    n()
                }), r.on("change", function() {
                    n()
                }), s.chosen({
                    disable_search: !0
                }), r.chosen({
                    disable_search: !0
                }), o.css("visibility", "hidden"), o.click(function(e) {
                    e.preventDefault(), h + 1 == 13 ? (d++, h = 1) : h++, n(), t()
                }), a.click(function(e) {
                    e.preventDefault(), h - 1 == 0 ? (d--, h = 12) : h--, n(), t()
                })
            }
        }(jQuery)
}), define("/sites/default/themes/siedler/js/communitypage/site/ranking.js", function() {}), $$(".communitypage.spa.kongregate").length && (! function() {
    transformNonSpaLinks = function() {
        $$("a").each(function(e) {
            var t = e.get("href");
            parent.kongregate ? e.hasClass("linkedTerritoryLink") || t && "#" != t.substr(0, 1) && 0 !== t.indexOf("mailto") && 0 !== t.indexOf("logout") && e.set("target", "_blank") : t && "#" != t.substr(0, 1) && 0 !== t.indexOf("mailto") && 0 !== t.indexOf("logout") && e.set("target", "_blank")
        })
    }
}(), window.addEvent("domready", function() {
    parent.kongregate || ($$("body").hide(), setTimeout(function() {
        window.location.replace(kongregateUrl)
    }, 1e3))
})), define("/sites/default/themes/siedler/js/communitypage/site/spa/kongregate.js", function() {}), $$("html.spa.steam").length && window.addEvent("domready", function() {
    $$("#cookieNotificationBanner").length && $$("#cookieNotificationBanner a").each(function(e) {
        e.addEvent("click", function() {
            return showExternalLayer(e.getAttribute("href")), !1
        })
    }), $$("#simpleDialog").length && $$("#simpleDialog.terms a").each(function(e) {
        e.addEvent("click", function() {
            return "_blank" === e.target && showExternalLayer(e.getAttribute("href")), !1
        })
    }), "function" == typeof steamDomReady && steamDomReady()
}), define("/sites/default/themes/siedler/js/communitypage/site/spa/steam.js", function() {}), $$(".communitypage.spa").length && 0 == $$(".communitypage.spa.steam").length && (("ie" == Browser.name || "ActiveXObject" in window) && $$("html")[0].addClass("isIE"), require(["/sites/default/themes/siedler/js/communitypage/site/news.js", "/sites/default/themes/common/js/common/ProfileForm.js", "/sites/default/themes/siedler/js/communitypage/site/faq.js"], function() {
    var e, t = null;
    e = function() {
        if (void 0 != window.pageYOffset) return [pageXOffset, pageYOffset];
        var e, t, n = document,
            i = n.documentElement,
            s = n.body;
        return e = i.scrollLeft || s.scrollLeft || 0, t = i.scrollTop || s.scrollTop || 0, [e, t]
    }, showSpecialLinkArea = function(e, t) {
        var n = $$(e)[0];
        if (n.parentNode.getElementsByClassName("expandArea").length) {
            var i = n.parentNode.getElementsByClassName("expandArea")[0];
            i.hasClass("hide") && ($$(t)[0].click(), i.scrollIntoView && i.scrollIntoView())
        }
    }, handleStateChange = function() {
        var e, n = (gm_LocationHash.getVars(), gm_LocationHash.getVar("pagei")),
            i = gm_LocationHash.getVar("pagearea");
        if ("play" == n && $$(".spa-communitypage.p-play").length < 1) return openGameLayer(), !1;
        if (!n && !initialTab) return !1;
        if (n && t == n) {
            if (i) switch (i) {
                case "voucher":
                    var s = "a[name=" + voucherAnchorName + "]",
                        r = "a[name=" + voucherAnchorName + "] + h4 > img.expander";
                    showSpecialLinkArea(s, r);
                    break;
                case "recruitment":
                    var s = "a[name=" + recruitmentAnchorName + "]",
                        r = "a[name=" + recruitmentAnchorName + "] + h4 > img.expander";
                    showSpecialLinkArea(s, r)
            }
            return !0
        }
        if (e = $$(".spa-communitypage." + n), e.length > 0) e = e[0];
        else if (e = $$(".spa-communitypage")[0], n = e.getAttribute("data-ident"), initialTab && $$('.communitypage.spa .spa-communitypage[data-ident="' + initialTab + '"]').length) n = initialTab, e = $$(".spa-communitypage." + n), e = e[0], gm_LocationHash.setVar("pagei", n, !0), delete initialTab;
        else {
            var o = null,
                a = $$(".communitypage.spa #menu .menu li");
            if (a.length > 1 && (a[0].hasClass("menu-item-carousel") || a[0].hasClass("menu-item-play")) ? o = a[1] : a.length && (o = a[0]), o) return $(o.getElement("a")).fireEvent("click", null, 10), !1
        }
        if ($$(".spa-communitypage").addClass("hide"), e.removeClass("hide"), $$("#menu li.active").removeClass("active"), $$(".menu-item-" + n).addClass("active"), t = n, $$("#category option").each(function(e) {
                e.hasClass("faq-delete-category") ? (e.set("selected", !1), e.hide()) : e.show()
            }), $$("#subject").set("value", ""), $$("#subject").set("readonly", !1), $$("#text").set("value", ""), $$("#text").set("readonly", !1), i) switch (i) {
            case "voucher":
                var s = "a[name=" + voucherAnchorName + "]",
                    r = "a[name=" + voucherAnchorName + "] + h4 > img.expander";
                showSpecialLinkArea(s, r);
                break;
            case "recruitment":
                var s = "a[name=" + recruitmentAnchorName + "]",
                    r = "a[name=" + recruitmentAnchorName + "] + h4 > img.expander";
                showSpecialLinkArea(s, r);
                break;
            case "faq-delete-account":
                var s = 'a[name="contact-form"]',
                    r = 'a[name="contact-form"] + h4 > img.expander';
                $$("#category option").each(function(e) {
                    e.hasClass("faq-delete-category") ? e.show() : e.hide()
                }), "undefined" != typeof faq_request_account_delete_subject && ($$("#subject").set("value", faq_request_account_delete_subject), $$("#subject").set("readonly", !0)), "undefined" != typeof faq_request_account_delete_message && ($$("#text").set("value", faq_request_account_delete_message), $$("#text").set("readonly", !0)), showSpecialLinkArea(s, r)
        }
    }, window.addEventListener("popstate", handleStateChange), ("ie" == Browser.name || "ActiveXObject" in window) && window.addEventListener("hashchange", handleStateChange), window.addEvent("domready", function() {
        var t;
        $$("#menu li a, a.page-faq").addEvent("click", function(t) {
            if ("_blank" != this.getAttribute("target")) {
                t && t.preventDefault(), scrollY = e()[1], setTimeout(function() {
                    window.scrollTo(0, scrollY)
                }, 1);
                var n = this.getAttribute("data-page"),
                    i = this.get("data-linktype");
                i ? gm_LocationHash.updateVars({
                    pagei: n,
                    pagearea: i
                }) : gm_LocationHash.setVar("pagei", n, !0)
            }
        }), t = {}, $$("div.spa-communitypage").each(function(e) {
            var n = e.getChildren("div > h2");
            0 == n.length && (n = e.getChildren("h2")), n.length > 0 && (n[0].innerHTML = e.getAttribute("data-title")), e.getAttribute("data-path") && e.getAttribute("data-ident") && (t[e.getAttribute("data-path")] = e.getAttribute("data-ident"))
        }), $$("#promotionShopLink").length && ($("promotionShopLink").set("href", shopTab), $("promotionShopLink").set("target", null)), $$("a").each(function(e) {
            var n, i = e.get("href");
            i && (n = i.split("/").pop(), n = decodeURIComponent(n), t.hasOwnProperty(n) && (e.set("href", "#spa-" + t[n]), e.removeAttribute("target"), e.addEvent("click", function(e) {
                e && e.preventDefault();
                var i = this.get("data-linktype");
                i ? gm_LocationHash.updateVars({
                    pagei: t[n],
                    pagearea: i
                }) : gm_LocationHash.setVar("pagei", t[n], !0)
            })))
        }), transformNonSpaLinks ? transformNonSpaLinks() : $$("a").each(function(e) {
            var t = e.get("href");
            t && "#" != t.substr(0, 1) && 0 !== t.indexOf("mailto") && 0 !== t.indexOf("logout") && e.set("target", "_blank")
        }), $$("#menu .bottom #shieldButtonOverlay").length && $$("#menu .bottom #shieldButtonOverlay").addEvent("click", function(e) {
            e && e.preventDefault(), gm_LocationHash.setVar("pagei", "play", !0)
        }), $("news") && $$(".newsarchiv > a").destroy(), setTimeout(function() {
            handleStateChange()
        }, 250)
    })
})), define("/sites/default/themes/siedler/js/communitypage/site/spa.js", function() {}), $$(".communitypage.voucher").length && require(["/sites/default/themes/common/js/common/ProfileForm.js"], function() {
    window.addEvent("domready", function() {
        document.id("voucherFormContainer") && new ProfileForm(document.id("voucherFormContainer"))
    })
}), define("/sites/default/themes/common/js/communitypage/site/voucher.js", ["/sites/default/themes/common/js/common/ProfileForm.js"], function() {}), $$("html.landingpage").length && (0 == $$(".allow-prefilled-avatar input[name=name]").length && $("name") && ($("name").set("value", ""), $("name").getNext("span").removeClass("hide")), $("email") && ($("email").set("value", ""), $("email").getNext("span").removeClass("hide")), $("password") && ($("password").set("value", ""), $("password").getNext("span").removeClass("hide")), $("termsLabel") && $("termsLabel").removeClass("checked"), $("privacyLabel") && $("privacyLabel").removeClass("checked")), define("/sites/default/themes/siedler/js/landingpage/base.js", function() {}), require(["/sites/default/themes/common/js/common/checkbox.js", "/sites/default/themes/common/js/common/avatar.js", "/sites/default/themes/common/js/common/form.js"], function() {
    window.addEvent("domready", function() {
        if ($$(".landingpage ").length > 0) {
            if (new FormOverlay($("registerForm")), $("ubibirthday")) {
                if ($("year")) var e = $("year").get("value");
                $("ubiBirthdayContainer").getElements(".part").addEvent("change", function() {
                    $("age") ? ($("ubibirthday").set("value", $("currentYear").get("value") - $("age").get("value") + "-01-01"), $("year").set("value", $("currentYear").get("value") - $("age").get("value")), $("ubibirthday").fireEvent("change")) : ($("ubibirthday").set("value", $("year").get("value") + "-" + $("month").get("value") + "-" + $("day").get("value")), e !== $("year").get("value") && $("ubibirthday").fireEvent("change"))
                })
            }
            if ($("registerForm").getElements("input[type=checkbox]").each(function(e) {
                    new Checkbox(e)
                }), document.id("registerFieldsetContainer") && document.id("registerFieldsetContainer").hasClass("settlers")) {
                var t = new Tracker({
                    pid: trackingPid
                });
                t.track("special", "onRegistrationSecondStep", {})
            }
            document.id("register") && document.id("avatar") && document.id("registerFieldsetContainer") && document.id("registerFieldsetContainer").hasClass("settlers") && "settlers" == document.id("registerForm").getElements("input[name=mode]").get("value") && (Overlay.show(), document.id("register").setStyle("z-index", 500))
        }
    }), gm_RegisterForm = new Class({
        Extends: gm_Form
    }), gm_RegisterForm._instance = null, gm_RegisterForm.getInstance = function() {
        return null === gm_RegisterForm._instance && (gm_RegisterForm._instance = new gm_RegisterForm(document.id("registerForm"), {}, {}, document.id("shieldButton"))), gm_RegisterForm._instance
    }
}), define("/sites/default/themes/common/js/common/register.js", ["/sites/default/themes/common/js/common/form.js"], function() {}), require([""], function() {
    NoPHP = new Class({
        _url: new URI,
        initialize: function() {
            isCdn && (this._ipBlock(), this._redirectToCommunityPage(), this._saveTrackingParams(), this._saveRefferer(), this._redirectToRecruitment(), this._saveRecruitment())
        },
        _ipBlock: function() {
            if (fastApiHost) {
                var e = new URI("/xx/api/ip");
                e.set("host", fastApiHost).set("scheme", "https").set("port", 443), new gm_Request({
                    url: e.toString(),
                    onStatusFailed: function() {
                        window.location = "/forbidden"
                    },
                    onStatusOkay: function(e) {
                        e && new gm_ShowBetterTerritoryDialog(e)
                    }
                }).get()
            }
        },
        _saveTrackingParams: function() {
            cdn.trackingParams.each(function(e) {
                var t = this._getGetValue(e);
                t && this._addToTrackingCookie(e, t)
            }, this)
        },
        _saveRefferer: function() {
            var e = document.referrer;
            if (e && !this._getTrackingCookieItem(cdn.refererCookieName)) {
                var t = new URI(e),
                    n = new URI;
                t.get("host") !== n.get("host") && this._addToTrackingCookie(cdn.refererCookieName, document.referrer)
            }
        },
        _addToTrackingCookie: function(e, t) {
            var n = JSON.decode(Cookie.read(cdn.trackingCookie));
            n || (n = {}), n[e] = t, this._saveCookie(cdn.trackingCookie, JSON.encode(n))
        },
        _getTrackingCookieItem: function(e) {
            var t = JSON.decode(Cookie.read(cdn.trackingCookie));
            if (t) return t[e]
        },
        _redirectToRecruitment: function() {
            var e = this._getGetValue(cdn.recruitmentGetParameter);
            e || (e = Cookie.read(cdn.recruitmentCookieName), e && (this._url.setData(cdn.recruitmentGetParameter, e), this._url.go()))
        },
        _redirectToCommunityPage: function() {
            var e = JSON.decode(Cookie.read(cdn.knownUserCookieName));
            if (e && e.status && e.name) {
                var t = new URI(cdn.communityPageHome);
                t.go()
            }
        },
        _saveRecruitment: function() {
            var e = this._getGetValue(cdn.recruitmentGetParameter);
            e && this._saveCookie(cdn.recruitmentCookieName, e)
        },
        _getGetValue: function(e) {
            var t = this._url.get("data");
            return t ? t[e] : null
        },
        _saveCookie: function(e, t) {
            Cookie.write(e, t, {
                duration: 365
            })
        }
    })
}), define("/sites/default/themes/common/js/common/noPHP.js", function() {}), require(["/sites/default/themes/common/js/common/register.js", "/sites/default/themes/common/js/common/dropdown.js", "/sites/default/themes/common/js/common/gametour.js", "/sites/default/themes/common/js/common/noPHP.js"], function() {
    function e() {
        return document.id("input_mode") ? "settlers" == document.id("input_mode").get("value") ? "Two" : "One" : null
    }
    window.addEvent("domready", function() {
        if ($$(".landingpage").length > 0) {
            if (isCdn && new NoPHP, document.id("countrySelector")) {
                var t = document.id(document.body).getElements("a.legal"),
                    n = document.id("registerForm").getElements("input[type=checkbox].validate-required-check"),
                    i = document.id("country") || document.id("countrySelector");
                i.addEvent("change", function() {
                    if ("select" === i.get("tag")) var e = i.getSelected()[0].get("value");
                    else var e = i.get("value");
                    t.each(function(t) {
                        var n = new URI(t.get("href"));
                        n.setData("country", e), t.set("href", n.toString())
                    }), n.each(function(t) {
                        "RU" === e || "US" === e ? t.set("checked", "checked").fireEvent("change") : t.removeProperty("checked").fireEvent("change")
                    })
                }), i.fireEvent("change")
            }
            $("gametourButton") && ("img" === $("gametourButton").get("tag") && new HoverButton($("gametourButton")), $("gametourButton").addEvent("click", function() {
                var e = new Tracker;
                e.track("special", "clickGametour"), Gametour.getInstance().show()
            })), $("loginButton") && new HoverButton($("loginButton").getElement("img")), gm_RegisterForm.getInstance().request.addEvent("statusOkay", function(t) {
                playNowforwardUrl = t.url;
                var n = !1,
                    i = function() {
                        n || (n = !0, "uplay" !== t.step && (dataLayer.push({
                            event: "registerComplete"
                        }), document.id("input_partner") && dataLayer.push({
                            event: "register" + document.id("input_partner").get("value") + "Finish"
                        })), "uplay" !== t.step && "undefined" != typeof debugMode ? confirm("redirect to gameclient?") ? new PlayNowLink(!0) : new URI("/").go() : setTimeout(function() {
                            new PlayNowLink(!0)
                        }, 2e3))
                    };
                loggedInUserCountry = t.userCountry;
                var s = new Tracker({
                    pid: t.pid
                });
                s.addEvent("complete", function() {
                    i.delay(1e3)
                }), "settlers" === t.step || "combined" === t.step || "uplay_optional" === t.step ? s.trackMany("special", ["afterRegisterWithoutFirst", "afterRegister"], {
                    userId: t.user,
                    campaignId: t.cid
                }) : "standalone" === t.step ? s.track("special", "afterRegister", {
                    userId: t.user,
                    campaignId: t.cid
                }) : s.track("special", "afterRegistrationFirstStep", {
                    userId: t.user
                }), dataLayer.push({
                    event: "registerStep" + e() + "Finish",
                    userId: t.user
                }), i.delay(5e3)
            }), gm_RegisterForm.getInstance().request.addEvent("statusFailed", function(t) {
                dataLayer.push({
                    event: "registerStep" + e() + "SubmitError"
                })
            }), gm_RegisterForm.getInstance().addEvent("formValidate", function(t, n, i, s, r) {
                if (!t) {
                    var o = [];
                    Array.each(r, function(e, t) {
                        o.push(e.getAttribute("name") || e.getAttribute("id"))
                    });
                    var a = [];
                    Array.each(s, function(e, t) {
                        a.push(e.getAttribute("name") || e.getAttribute("id"))
                    }), dataLayer.push({
                        event: "registerStep" + e() + "Invalid",
                        invalidFields: o,
                        validFields: a
                    })
                }
            }), gm_RegisterForm.getInstance().addEvent("onFormSubmit", function() {
                dataLayer.push({
                    event: "registerStep" + e() + "Submit"
                })
            }), gm_RegisterForm.getInstance().addEvent("uplayDown", function() {
                dataLayer.push({
                    event: "registerStep" + e() + "UplayDown"
                })
            });
            var s = $("screenshotBox");
            if (s) {
                var r = new Tracker;
                s.getElements("a").addEvent("click", function() {
                    r.track("special", "clickScreenshot")
                })
            }
            e() && (dataLayer.push({
                currentPage: "registerPage" + e()
            }), dataLayer.push({
                event: "pageView"
            }));
            var s = $("screenshotBox");
            if (s) {
                var r = new Tracker;
                s.getElements("a").addEvent("click", function() {
                    r.track("special", "clickScreenshot")
                })
            }
        }
    })
}), define("/sites/default/themes/common/js/landingpage/common.js", ["/sites/default/themes/common/js/common/register.js"], function() {}), $$("html.landingpage.home").length && require(["/sites/default/themes/common/js/landingpage/common.js"], function() {
    ImagePreloader.disableImagePreloader = !0, window.addEvent("load", function() {
        if (("MX" === territoryIdent || "ES" === territoryIdent) && document.id("deranger")) {
            var e = document.id("deranger").getElement("a"),
                t = document.id("register"),
                n = document.id("login");
            e && t && n && e.addEvent("click", function(e) {
                e.stop(), Overlay.getInstance().overlay.addEvent("click:once", function() {
                    Overlay.getInstance().hide(), t.setStyle("z-index", null), n.setStyle("z-index", null), Overlay.getInstance().overlay.setStyle("z-index", null)
                }), Overlay.getInstance().show(), Overlay.getInstance().overlay.setStyle("z-index", "97"), t.setStyle("z-index", "98"), n.setStyle("z-index", "99"), t.getElement("input[type=text]").focus(), new Fx.Scroll(document.id(document.body)).toElement(t)
            })
        }
        var i = function() {
            $("backgroundTop").setStyle("background-image", "url(" + websiteContentPath + "images/common/background/top.jpg)"), $("backgroundMiddle").setStyle("background-image", "url(" + websiteContentPath + "images/common/background/" + languageIdent + "/middle.jpg)"), $("backgroundBottom1").setStyle("background-image", "url(" + websiteContentPath + "images/common/background/bottom1.jpg)"), $("backgroundBottom2").setStyle("background-image", "url(" + websiteContentPath + "images/common/background/bottom2.jpg)");
            var e = [websiteContentPath + "images/common/background/top.jpg", websiteContentPath + "images/common/background/" + languageIdent + "/middle.jpg", websiteContentPath + "images/common/background/bottom1.jpg", websiteContentPath + "images/common/background/bottom2.jpg"];
            new Asset.images(e, {
                onComplete: function() {
                    $("html").setStyle("background", "url(" + websiteContentPath + "images/common/background/bottom3.jpg) repeat-y center #dde6d3"), ImagePreloader.getInstance().preload()
                }
            })
        };
        i(), document.id("slider") && document.id("avatar") && new AvatarSlider(document.id("slider"), new Avatar(document.id("avatar"), document.id("contentOrnament")), document.id("sliderLeft"), document.id("sliderRight"))
    })
}), define("/sites/default/themes/siedler/js/landingpage/site/home.js", function() {}), $$("html.landingpage.home5").length && require(["/sites/default/themes/common/js/landingpage/common.js", "/sites/default/themes/siedler/js/common/PageLoadingBar.js"], function() {
    window.addEvent("domready", function() {
        document.id("slider") && document.id("avatar") && new AvatarSlider(document.id("slider"), new Avatar(document.id("avatar")), document.id("sliderLeft"), document.id("sliderRight"));
        var e = $("playButton").getElement("img");
        e.addEvents({
            mouseover: function() {
                $("playButton").setStyle("background-position", "0 -104px")
            },
            mouseout: function() {
                $("playButton").setStyle("background-position", "0 0")
            }
        })
    })
}), define("/sites/default/themes/siedler/js/landingpage/site/home5.js", function() {}), $$("html.landingpage.home10").length && require(["/sites/default/themes/common/js/landingpage/common.js", "/sites/default/themes/siedler/js/common/PageLoadingBar.js"], function() {
    window.addEvent("domready", function() {
        document.id("slider") && document.id("avatar") && new AvatarSlider(document.id("slider"), new Avatar(document.id("avatar")), document.id("sliderLeft"), document.id("sliderRight"));
        var e = $("playButton").getElement("img");
        e.addEvents({
            mouseover: function() {
                $("playButton").setStyle("background-position", "0 -104px")
            },
            mouseout: function() {
                $("playButton").setStyle("background-position", "0 0")
            }
        })
    })
}), define("/sites/default/themes/siedler/js/landingpage/site/home10.js", function() {}), $$("html.landingpage.home11").length && require(["/sites/default/themes/common/js/landingpage/common.js", "/sites/default/themes/siedler/js/common/PageLoadingBar.js"], function() {
    window.addEvent("domready", function() {
        document.id("slider") && document.id("avatar") && new AvatarSlider(document.id("slider"), new Avatar(document.id("avatar")), document.id("sliderLeft"), document.id("sliderRight"));
        var e = $("playButton").getElement("img");
        e.addEvents({
            mouseover: function() {
                $("playButton").setStyle("background-position", "0 -104px")
            },
            mouseout: function() {
                $("playButton").setStyle("background-position", "0 0")
            }
        })
    })
}), define("/sites/default/themes/siedler/js/landingpage/site/home11.js", function() {}), require(["/sites/default/themes/common/js/common/register.js"], function() {
    $$("html.landingpage.merge").length && window.addEvent("domready", function() {
        var e = new gm_Form($("loginFormMerge"), {
            continueOnStatusFailed: !0,
            removeValuesAfterSuccess: !0
        }, {
            onStatusOkay: function(e) {
                window.location.reload()
            }.bind(this),
            onStatusFailed: function(e) {
                $("loginFormButtonMessageMerge").removeClass("hide"), e && ($("loginFormButtonMessageMerge").getElement("h6").set("html", e.title), $("loginFormButtonMessageMerge").getElement("p").set("html", e.text))
            }
        }, $("loginFormButtonMerge"));
        new FormOverlay($("loginFormMerge")), new FormOverlay($("registerForm")), registerForm = new gm_Form($("registerForm"), {}, {}, $("shieldButton")), registerForm.request.addEvent("statusOkay", function(e) {
            if ("settlers" === e.step) var t = function() {
                new GameLink
            };
            else var t = function() {
                new PlayNowLink
            };
            var n = new Tracker({
                pid: e.pid
            });
            n.addEvent("complete", function() {
                t()
            }), "settlers" === e.step ? n.trackMany("special", ["afterRegisterWithoutFirst", "afterRegister"], {
                userId: e.user
            }) : n.track("special", "afterRegistrationFirstStep", {
                userId: e.user
            }), t.delay(5e3)
        }), $("loginFormMerge").getElement(".password").addEvent("click", function() {
            if ("" != $("loginUsername").value) {
                var e = new gm_dialog_Plain(passwordResetDialogTitle, passwordResetDialogMessage, {
                    hasCancelButton: !0
                });
                e.addEvent("okay", function() {
                    new gm_Request({
                        url: "/" + languagePath + "/api/user/uplayResetPassword",
                        onStatusFailed: function(e) {
                            gm_DialogHandler.add(new gm_dialog_Plain(e.title, e.message))
                        }.bind(this),
                        onStatusOkay: function(e) {
                            gm_DialogHandler.add(new gm_dialog_Plain(e.title, e.message))
                        }.bind(this)
                    }).post({
                        username: $("loginUsername").value
                    })
                }), gm_DialogHandler.add(e)
            } else gm_DialogHandler.add(new gm_dialog_Plain(passwordResetEmptyDialogTitle, passwordResetEmptyDialogMessage));
            return !1
        }.bind(this)), [e, registerForm].each(function(e, t, n) {
            e.fields.each(function(t) {
                t.addEvent("focus", function() {
                    e.fireEvent("removeBoxes")
                })
            }), e.addEvent("formValidate", function() {
                e.fireEvent("removeBoxes")
            }), e.addEvent("removeBoxes", function() {
                $("loginFormButtonMessageMerge").addClass("hide");
                for (var e = 0; e < n.length; e++) e !== t && n[e].fields.each(function(t) {
                    n[e].getValidators(t).combine(n[e].getAjaxValidators(t)).each(function(i) {
                        n[e].hideAdvice(i, t)
                    })
                })
            })
        })
    })
}), define("/sites/default/themes/common/js/landingpage/site/merge.js", function() {}), $$("html.landingpage.merge").length && require(["/sites/default/themes/common/js/landingpage/site/merge.js"]), define("/sites/default/themes/siedler/js/landingpage/site/merge.js", function() {}), $$("html.landingpage.partner1").length && require(["/sites/default/themes/common/js/landingpage/common.js", "/sites/default/themes/common/js/common/register.js", "/sites/default/themes/siedler/js/common/PageLoadingBar.js"], function() {
    window.addEvent("domready", function() {
        document.id("slider") && document.id("avatar") && new AvatarSlider(document.id("slider"), new Avatar(document.id("avatar")), document.id("sliderLeft"), document.id("sliderRight"));
        var e = $("playButton").getElement("img");
        e.addEvents({
            mouseover: function() {
                $("playButton").setStyle("background-position", "0 -104px")
            },
            mouseout: function() {
                $("playButton").setStyle("background-position", "0 0")
            }
        }), setTimeout(function() {
            gm_RegisterForm.getInstance().oldFieldValues.name = "", gm_RegisterForm.getInstance().validateField("name")
        }, 750)
    })
}), define("/sites/default/themes/siedler/js/landingpage/site/partner1.js", function() {}), $$("html.landingpage.kongregate").length && require([], function() {
    window.addEvent("domready", function() {
        if ($$(".guestplay").length) {
            var e = parent;
            $("playButton").addEventListener("click", function(t) {
                t.stopPropagation(), e.postMessage('{"method":"showRegisterBox"}', "*")
            })
        }
        $$("#footer ul li a").set("target", "_blank")
    })
}), define("/sites/default/themes/siedler/js/landingpage/site/kongregate.js", function() {}), $$("html.landingpage.partner_gamigo").length && require(["/sites/default/themes/common/js/landingpage/common.js", "/sites/default/themes/siedler/js/common/PageLoadingBar.js"], function() {
    window.addEvent("domready", function() {
        document.id("slider") && document.id("avatar") && new AvatarSlider(document.id("slider"), new Avatar(document.id("avatar")), document.id("sliderLeft"), document.id("sliderRight")), document.id("logo").getElements("a").addEvent("click", function(e) {
            return e.stop(), !1
        });
        var e = $("playButton").getElement("img");
        e.addEvents({
            mouseover: function() {
                $("playButton").setStyle("background-position", "0 -104px")
            },
            mouseout: function() {
                $("playButton").setStyle("background-position", "0 0")
            }
        })
    })
}), define("/sites/default/themes/siedler/js/landingpage/site/partner_gamigo.js", function() {}), $$("html.landingpage.partner_generic").length && require(["/sites/default/themes/common/js/landingpage/common.js", "/sites/default/themes/siedler/js/common/PageLoadingBar.js"], function() {
    window.addEvent("domready", function() {
        document.id("slider") && document.id("avatar") && new AvatarSlider(document.id("slider"), new Avatar(document.id("avatar")), document.id("sliderLeft"), document.id("sliderRight"));
        var e = $("playButton").getElement("img");
        e.addEvents({
            mouseover: function() {
                $("playButton").setStyle("background-position", "0 -104px")
            },
            mouseout: function() {
                $("playButton").setStyle("background-position", "0 0")
            }
        }), gm_RegisterForm.getInstance().oldFieldValues.name = "", gm_RegisterForm.getInstance().validateField("name")
    })
}), define("/sites/default/themes/siedler/js/landingpage/site/partner_generic.js", function() {}), $$("html.landingpage.steam").length && require(["/sites/default/themes/common/js/landingpage/common.js", "/sites/default/themes/siedler/js/common/PageLoadingBar.js"], function() {
    window.addEvent("domready", function() {
        document.id("slider") && document.id("avatar") && new AvatarSlider(document.id("slider"), new Avatar(document.id("avatar")), document.id("sliderLeft"), document.id("sliderRight"));
        var e = $("playButton").getElement("img");
        e.addEvents({
            mouseover: function() {
                $("playButton").setStyle("background-position", "0 -104px")
            },
            mouseout: function() {
                $("playButton").setStyle("background-position", "0 0")
            }
        }), $$("#cookieNotificationBanner").length && $$("#cookieNotificationBanner a").each(function(e) {
            e.addEvent("click", function() {
                return showExternalLayer(e.getAttribute("href")), !1
            })
        })
    })
}), define("/sites/default/themes/siedler/js/landingpage/site/partner_steam.js", function() {}), requirejs.config({
    shim: {
        "/sites/default/themes/common/js/common/dialog/DoiRepeat.js": ["/sites/default/themes/common/js/common/dialog/Plain.js"],
        "/sites/default/themes/common/js/common/dialog/Refresh.js": ["/sites/default/themes/common/js/common/dialog/Plain.js"],
        "/sites/default/themes/common/js/common/dialog/Plain.js": ["/sites/default/themes/common/js/common/dialog.js"],
        "/sites/default/themes/common/js/common/login.js": ["/sites/default/themes/common/js/common/form.js"],
        "/sites/default/themes/common/js/common/register.js": ["/sites/default/themes/common/js/common/form.js"],
        "/sites/default/themes/common/js/common/form.js": ["/sites/default/themes/common/js/common/request.js"],
        "/sites/default/themes/common/js/common/ProfileForm.js": ["/sites/default/themes/common/js/common/form.js"],
        "/sites/default/themes/common/js/common/tracker/Iframe.js": ["/sites/default/themes/common/js/common/tracker/ABase.js"],
        "/sites/default/themes/common/js/common/tracker/Javascript.js": ["/sites/default/themes/common/js/common/tracker/ABase.js"],
        "/sites/default/themes/common/js/common/tracker/Image.js": ["/sites/default/themes/common/js/common/tracker/ABase.js"],
        "/sites/default/themes/common/js/common/tracker/Function.js": ["/sites/default/themes/common/js/common/tracker/ABase.js"],
        "/sites/default/themes/common/js/common/form_pgo.js": ["/sites/default/themes/common/js/common/form.js"],
        "/sites/default/themes/common/js/common/overlayTween.js": ["/sites/default/themes/common/js/common/overlay.js"],
        "/sites/default/themes/common/js/common/message/BetterTerritory.js": ["/sites/default/themes/common/js/common/message/BetterTerritoryLite.js"],
        "/sites/default/themes/common/js/landingpage/common.js": ["/sites/default/themes/common/js/common/register.js"],
        "/sites/default/themes/common/js/communitypage/site/profile.js": ["/sites/default/themes/common/js/common/ProfileForm.js"],
        "/sites/default/themes/common/js/communitypage/site/voucher.js": ["/sites/default/themes/common/js/common/ProfileForm.js"],
        "/sites/default/themes/siedler/js/communitypage/site/profile.js": ["/sites/default/themes/common/js/common/ProfileForm.js"],
        "/sites/default/themes/siedler/js/landingpage/site/home2.js": ["/sites/default/themes/common/js/common/register.js"],
        "/sites/default/themes/siedler/js/landingpage/site/home3.js": ["/sites/default/themes/common/js/common/register.js"]
    }
}), require(["/sites/default/themes/siedler/js/external/mootools-core-1.4.1-full-compat.js", "/sites/default/themes/siedler/js/external/jquery-1.10.2.js"], function() {
    require(["/sites/default/themes/siedler/js/external/mootools-more-1.4.0.1.js", "/sites/default/themes/common/js/common/helper.js"], function() {
        require(["/sites/default/themes/common/js/external/swfobject.js", "/sites/default/themes/siedler/js/external/mediaboxAdv-1.3.4b.js", "/sites/default/themes/siedler/js/external/jquery.chosen.js", "/sites/default/themes/common/js/external/string.utf8.js", "/sites/default/themes/common/js/external/string.md5.js", "/sites/default/themes/common/js/common/location_hash.js", "/sites/default/themes/common/js/common/element.js", "/sites/default/themes/common/js/common/imagePreloader.js", "/sites/default/themes/common/js/common/cookie.js", "/sites/default/themes/common/js/common/CenterElement.js", "/sites/default/themes/common/js/common/ResizeElement.js", "/sites/default/themes/common/js/common/game.js", "/sites/default/themes/common/js/common/language.js", "/sites/default/themes/common/js/common/overlay.js", "/sites/default/themes/common/js/common/overlayTween.js", "/sites/default/themes/common/js/common/request.js", "/sites/default/themes/common/js/common/form.js", "/sites/default/themes/common/js/common/formOverlay.js", "/sites/default/themes/common/js/common/login.js", "/sites/default/themes/common/js/common/LoginRedirect.js", "/sites/default/themes/common/js/common/checkbox.js", "/sites/default/themes/common/js/common/links.js", "/sites/default/themes/common/js/common/gamelink.js", "/sites/default/themes/common/js/common/hover.js", "/sites/default/themes/common/js/common/shield.js", "/sites/default/themes/common/js/common/gametour.js", "/sites/default/themes/common/js/common/helper.js", "/sites/default/themes/common/js/common/tracker.js", "/sites/default/themes/common/js/common/tracker/ABase.js", "/sites/default/themes/common/js/common/tracker/Iframe.js", "/sites/default/themes/common/js/common/tracker/Image.js", "/sites/default/themes/common/js/common/tracker/Javascript.js", "/sites/default/themes/common/js/common/tracker/Function.js", "/sites/default/themes/common/js/common/dropdown.js", "/sites/default/themes/common/js/common/DialogHandler.js", "/sites/default/themes/common/js/common/dialog.js", "/sites/default/themes/common/js/common/dialog/Plain.js", "/sites/default/themes/common/js/common/dialog/DoiRepeat.js", "/sites/default/themes/common/js/common/dialog/Plain.js", "/sites/default/themes/common/js/common/message/BetterTerritoryLite.js", "/sites/default/themes/common/js/common/message/BetterTerritory.js", "/sites/default/themes/common/js/common/message/Terms.js", "/sites/default/themes/common/js/common/checkbox.js", "/sites/default/themes/common/js/common/kongregate.js", "/sites/default/themes/common/js/common/captcha.js", "/sites/default/themes/common/js/debug.js", "/sites/default/themes/common/js/common/avatar.js", "/sites/default/themes/common/js/common/cookiesNotification.js", "/sites/default/themes/siedler/js/common/kaisergames.js", "/sites/default/themes/siedler/js/common/browserdegrade.js", "/sites/default/themes/siedler/js/communitypage/base.js", "/sites/default/themes/siedler/js/communitypage/site/news.js", "/sites/default/themes/siedler/js/common/youtube.js", "/sites/default/themes/siedler/js/common/PageLoadingBar.js", "/sites/default/themes/siedler/js/common/waitingoverlay.js", "/sites/default/themes/siedler/js/communitypage/site/faq.js", "/sites/default/themes/siedler/js/communitypage/site/faq_category.js", "/sites/default/themes/siedler/js/communitypage/site/gamesload.js", "/sites/default/themes/siedler/js/communitypage/site/links.js", "/sites/default/themes/siedler/js/communitypage/site/maintenance.js", "/sites/default/themes/siedler/js/communitypage/site/media_category.js", "/sites/default/themes/siedler/js/communitypage/site/home.js", "/sites/default/themes/siedler/js/communitypage/site/reset.js", "/sites/default/themes/siedler/js/communitypage/site/shop.js", "/sites/default/themes/siedler/js/communitypage/site/shop2.js", "/sites/default/themes/siedler/js/communitypage/site/profile.js", "/sites/default/themes/siedler/js/communitypage/site/profile.gamesettings.js", "/sites/default/themes/siedler/js/communitypage/site/ranking.js", "/sites/default/themes/siedler/js/communitypage/site/spa/kongregate.js", "/sites/default/themes/siedler/js/communitypage/site/spa/steam.js", "/sites/default/themes/siedler/js/communitypage/site/spa.js", "/sites/default/themes/common/js/communitypage/site/voucher.js", "/sites/default/themes/siedler/js/common/browserdegrade.js", "/sites/default/themes/siedler/js/landingpage/base.js", "/sites/default/themes/siedler/js/landingpage/site/home.js", "/sites/default/themes/siedler/js/landingpage/site/home5.js", "/sites/default/themes/siedler/js/landingpage/site/home10.js", "/sites/default/themes/siedler/js/landingpage/site/home11.js", "/sites/default/themes/siedler/js/landingpage/site/merge.js", "/sites/default/themes/siedler/js/landingpage/site/partner1.js", "/sites/default/themes/siedler/js/landingpage/site/kongregate.js", "/sites/default/themes/siedler/js/landingpage/site/partner_gamigo.js", "/sites/default/themes/siedler/js/landingpage/site/partner_generic.js", "/sites/default/themes/siedler/js/landingpage/site/partner_steam.js"], function() {
            window.addEvent("domready", function() {
                function clearHashtag() {
                    -1 !== location.href.indexOf("#lightbox") && (location.hash = "")
                }

                function getHashtag() {
                    var e = location.href;
                    return hashtag = -1 !== e.indexOf("#lightbox") ? decodeURI(e.substring(e.indexOf("#lightbox") + 10, e.length)) : !1, hashtag
                }
                if ($$("html.communitypage").length) {
                    var str = getHashtag();
                    if (str) {
                        var dotIndex = str.lastIndexOf("."); - 1 != dotIndex ? Mediabox.open("/sites/default/files/" + str, "", "") : Mediabox.open("//www.youtube.com/embed/" + str, "", ""), clearHashtag()
                    }
                } else getHashtag() && clearHashtag();
                var url = new URI;
                if ("AE-ar" == languageIdent && "undefined" != typeof ie_lt9_title && "undefined" != typeof ie_lt9_message) {
                    var bd = new BrowserDegraded(ie_lt9_title, ie_lt9_message);
                    bd.addCondition("ie", 9), bd.checkAndAlert()
                }
                if (messageDialogData) {
                    var vars = [];
                    messageDialogData.params.each(function(e, t) {
                        vars.push("messageDialogData[ 'params' ][ " + t + " ]")
                    }), eval("var messageDialog = new " + messageDialogData.className + "(" + vars.join(", ") + ");"), gm_DialogHandler.add(messageDialog)
                }
                Asset.javascript(websiteContentPathLocal + "js/common/tracker/trackingData.js", {
                    onLoad: function() {
                        var e = new Tracker;
                        e.trackCurrentPage()
                    }
                })
            }), window.addEvent("domready", function() {
                self !== top && $$("a.fbiframe").each(function(e) {
                    e.addEvent("click", function(t) {
                        t.stop();
                        var n = new URI(e.get("href")),
                            i = new URI(n.getData("redirect_uri"));
                        i.setData("closewindow", "1"), n.setData("redirect_uri", i.toString()), gm_DialogHandler.add(new gm_dialog_Plain(facebookIframeMessageTitle, facebookIframeMessage, {
                            redirectOnClose: "/"
                        }));
                        var s = window.open(n.toString());
                        s.focus()
                    })
                }), (new URI).getData("closewindow") && window.opener && (window.opener.location.reload(!0), window.opener.focus(), window.close(), stop()), Dropdowns.getInstance(), Helper.prepareRenderer(), $$("form fieldset.ssl,form fieldset img.ssl, form > div > a.ssl").each(function(e) {
                    e.addEvent("click", function() {
                        gm_DialogHandler.add(new gm_dialog_Plain(sslDialogTitle, sslDialogMessage, {
                            icon: websiteContentPath + "images/common/dialog/icon/ssl.png"
                        }))
                    })
                }), $$("a.removeKnownUser").each(function(e) {
                    e.addEvent("click", function() {
                        Cookie.dispose("knownUser")
                    })
                }), $$("._hover").each(function(e) {
                    new HoverButton(e)
                }), gm_DialogHandler.start()
            })
        })
    })
}), define("../drupal/sites/default/themes/siedler/js/base", function() {});