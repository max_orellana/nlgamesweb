<?php /* dev 
 *
 * Any reproduction or redistribution of the code of this project 
 * is expressly prohibited by law, and may result in civil and 
 * criminal penalties.
 *
 * @author Martin Frith
 * @email martin at devmeta.net
 * @date Jan 06 2016
 * Copyright 2016 NLGAMES.net 
 */

use \Bootie\App as App;

App::route('/test_account',		['uses' => 'Controller\TestController@test_account']);

//App::route('/fix_advertiser_capture',		['uses' => 'Controller\TestController@fix_advertiser_capture']);

//App::route('/test_insert',		['uses' => 'Controller\TestController@test_insert']);
//App::route('/testheroes',		['uses' => 'Controller\TestController@testheroes']);
//App::route('/testcoin',		['uses' => 'Controller\TestController@testcoin']);
//App::route('/mailtest',		['uses' => 'Controller\TestController@mailtest']);
//App::route('/mailtemplate',		['uses' => 'Controller\TestController@mailtemplate']);

App::route('/mobile',		['uses' => 'Controller\HomeController@mobile']);


/* public */
//App::route('/',			['uses' => 'Controller\HomeController@intro']);
App::route('/', 	[ 'uses' => 'Controller\HomeController@index']);
App::route('/news',	['uses' => 'Controller\HomeController@news']);
App::route('/captcha',		['uses' => 'Controller\HomeController@captcha']);
App::route('/blog',		['uses' => 'Controller\BlogController@index']);
App::route('/blog/([^/]+)', [ 'uses' => 'Controller\BlogController@show']);
App::route('/88e889e9bfa395/account_name/([^/]+)', [ 'uses' => 'Controller\AuthController@account']);
App::route('/88e889e9bfa395/fix_account_codes', [ 'uses' => 'Controller\TestController@fix_account_codes']);
App::route('/88e889e9bfa395/add_coins/([^/]+)/(\d+)', [ 'uses' => 'Controller\AuthController@add_coins']);
App::route('/files/(\w+)/(\d+)', [ 'uses' => 'Controller\BlogController@files']);
App::route('/blog/tag/([^/]+)', [ 'uses' => 'Controller\BlogController@tag']);
App::route('/modals/(.*)', [ 'uses' => 'Controller\HomeController@modal']);
//App::route('/profile', 	[ 'uses' => 'Controller\AccountController@profile','before' => 'auth.user']);
App::route('/joinbeta', 	[ 'uses' => 'Controller\HomeController@joinbeta']);
App::route('/login', 	[ 'uses' => 'Controller\AuthController@login','method' => 'post']);
App::route('/recoverpassword', 	[ 'uses' => 'Controller\AuthController@recoverpassword','method' => 'post']);
App::route('/updatepasswordcode', 	[ 'uses' => 'Controller\AuthController@updatepasswordcode','method' => 'post','before' => 'auth.user']);
App::route('/updatepassword', 	[ 'uses' => 'Controller\AuthController@updatepassword','method' => 'post','before' => 'auth.user']);

App::route('/register', 	[ 'uses' => 'Controller\AuthController@register','method' => 'post']);
App::route('/wannajoinbeta', 	[ 'uses' => 'Controller\User\PanelController@wannajoinbeta','method' => 'post']);
App::route('/logout', 	[ 'uses' => 'Controller\AuthController@logout']);
App::route('/reset-pass', 	[ 'uses' => 'Controller\AuthController@reset_pass','method' => 'post']);
App::route('/username-check', 	[ 'uses' => 'Controller\AuthController@username_check']);
App::route('/character-check', 	[ 'uses' => 'Controller\AuthController@character_check']);
App::route('/email-check', 	[ 'uses' => 'Controller\AuthController@email_check']);
App::route('/video/(.*)', [ 'uses' => 'Controller\VideoStream@start']);

/* group user */
App::route('/account', 	[ 'uses' => 'Controller\AuthController@showgroup','before' => 'auth.user']);
App::route('/account/index', 	[ 'uses' => 'Controller\User\PanelController@index','before' => 'auth.user']);
App::route('/account/updatepassword', 	[ 'uses' => 'Controller\User\PanelController@updatepassword']);
App::route('/account/joinbeta', 	[ 'uses' => 'Controller\User\PanelController@joinbeta','method' => 'post','before' => 'auth.user']);
App::route('/account/items', 	[ 'uses' => 'Controller\ItemController@index','before' => 'auth.user']);

//App::route('/account/balance', 	[ 'uses' => 'Controller\User\PanelController@balance','before' => 'auth.user']);
//App::route('/account/coins', 	[ 'uses' => 'Controller\User\PanelController@coins','before' => 'auth.user']);
//App::route('/account/payment', 	[ 'uses' => 'Controller\User\PanelController@payment','before' => 'auth.user']);
//App::route('/account/resume', 	[ 'uses' => 'Controller\User\PanelController@resume','method' => 'post','before' => 'auth.user']);
//App::route('/account/order', 	[ 'uses' => 'Controller\User\PanelController@order','method' => 'post','before' => 'auth.user']);

App::route('/account/checkout', 	[ 'uses' => 'Controller\OrderController@checkout','method' => 'post','before' => 'auth.user']);
App::route('/account/postulations', 	[ 'uses' => 'Controller\User\PanelController@postulations','before' => 'auth.user']);
App::route('/account/postulations/send', 	[ 'uses' => 'Controller\User\PanelController@postulations_send','method' => 'post','before' => 'auth.user']);
App::route('/account/tickets', 	[ 'uses' => 'Controller\User\PanelController@tickets','before' => 'auth.user']);
App::route('/account/tickets/([^/]+)', [ 'uses' => 'Controller\User\PanelController@ticket','before' => 'auth.user']);
App::route('/account/tickets/send', 	[ 'uses' => 'Controller\User\PanelController@tickets_send','method' => 'post','before' => 'auth.user']);
App::route('/account/profile', 	[ 'uses' => 'Controller\User\PanelController@profile','before' => 'auth.user']);
App::route('/account/forum', 	[ 'uses' => 'Controller\User\PanelController@forum','before' => 'auth.user']);
App::route('/account/forum/connect', 	[ 'uses' => 'Controller\User\PanelController@forum_connect','method' => 'post','before' => 'auth.user']);
App::route('/account/forum/disconnect', 	[ 'uses' => 'Controller\User\PanelController@forum_disconnect','method' => 'post','before' => 'auth.user']);


/* group advertiser */
App::route('/advertiser/index', 	[ 'uses' => 'Controller\Advertiser\PanelController@index','before' => 'auth.advertiser']);
App::route('/advertiser/links', 	[ 'uses' => 'Controller\Advertiser\PanelController@links','before' => 'auth.advertiser']);
App::route('/advertiser/progress', [ 'uses' => 'Controller\Advertiser\PanelController@progress','before' => 'auth.advertiser']);
App::route('/advertiser/progress/([^/]+)', [ 'uses' => 'Controller\Advertiser\PanelController@progress','before' => 'auth.advertiser']);
App::route('/advertiser/goals', [ 'uses' => 'Controller\Advertiser\PanelController@goals','before' => 'auth.advertiser']);

/* payments */
App::route('/payu/response', 	[ 'uses' => 'Controller\OrderController@getPayuResponse']);
App::route('/payu/confirmation', 	[ 'uses' => 'Controller\OrderController@getPayuConfirmation','method' => 'post']);
App::route('/paypal/return', 	[ 'uses' => 'Controller\OrderController@doPaypalExpressCheckoutPayment']);
App::route('/paypal/cancel', 	[ 'uses' => 'Controller\OrderController@getPaymentCancel']);
App::route('/paypal/success', 	[ 'uses' => 'Controller\OrderController@getPaymentSuccess']);

/* group admin */

App::resource('/admin/postulations', [ 'uses' => 'Controller\Admin\PostulationController','before' => 'auth.admin']);
App::resource('/admin/posts', [ 'uses' => 'Controller\Admin\PostController','before' => 'auth.admin']);
App::resource('/admin/items', [ 'uses' => 'Controller\Admin\ItemController','before' => 'auth.admin']);
App::resource('/admin/items-types', [ 'uses' => 'Controller\Admin\ItemTypeController','before' => 'auth.admin']);
App::resource('/admin/items-grades', [ 'uses' => 'Controller\Admin\ItemGradeController','before' => 'auth.admin']);
App::resource('/admin/items-classes', [ 'uses' => 'Controller\Admin\ItemClassController','before' => 'auth.admin']);
App::resource('/admin/words', [ 'uses' => 'Controller\Admin\WordController','before' => 'auth.admin']);
App::resource('/admin/heroes', [ 'uses' => 'Controller\Admin\HeroeController','before' => 'auth.admin']);
App::resource('/admin/orders', [ 'uses' => 'Controller\Admin\OrderController','before' => 'auth.admin']);
App::resource('/admin/payments', [ 'uses' => 'Controller\Admin\PaymentController','before' => 'auth.admin']);
App::resource('/admin/packs', [ 'uses' => 'Controller\Admin\PackController','before' => 'auth.admin']);
App::resource('/admin/advertisers', [ 'uses' => 'Controller\Admin\AdvertiserController','before' => 'auth.admin']);
App::resource('/admin/tasks', [ 'uses' => 'Controller\Admin\TaskController','before' => 'auth.admin']);
App::resource('/admin/accounts', [ 'uses' => 'Controller\Admin\AccountController','before' => 'auth.admin']);
App::resource('/admin/coupons', [ 'uses' => 'Controller\Admin\CouponController','before' => 'auth.admin']);
App::resource('/admin/coupons-types', [ 'uses' => 'Controller\Admin\CouponTypeController','before' => 'auth.admin']);

App::route('/admin/alerts', 	[ 'uses' => 'Controller\Admin\PanelController@alerts','before' => 'auth.admin']);
App::route('/admin/alert', 	[ 'uses' => 'Controller\Admin\PanelController@alert','method' => 'post','before' => 'auth.admin']);
App::route('/admin/dash', 	[ 'uses' => 'Controller\Admin\PanelController@dash','before' => 'auth.admin']);
App::route('/admin/search', 	[ 'uses' => 'Controller\Admin\PanelController@search','before' => 'auth.admin']);
App::route('/admin/online', 	[ 'uses' => 'Controller\Admin\PanelController@online','before' => 'auth.admin']);
App::route('/admin/dash/realtime', 	[ 'uses' => 'Controller\Admin\PanelController@realtime','before' => 'auth.admin']);
App::route('/admin/tickets', [ 'uses' => 'Controller\Admin\TicketController@index','before' => 'auth.admin']);
App::route('/admin/tickets/(\d+)', [ 'uses' => 'Controller\Admin\TicketController@edit','before' => 'auth.admin']);
App::route('/admin/tickets/send', [ 'uses' => 'Controller\Admin\TicketController@send','method' => 'post','before' => 'auth.admin']);
App::route('/admin/tickets/delete/(\d+)', [ 'uses' => 'Controller\Admin\TicketController@delete','method' => 'post','before' => 'auth.admin']);
App::route('/admin/postulations-relation', [ 'uses' => 'Controller\Admin\PostulationRelationController@index','before' => 'auth.admin']);
App::route('/admin/postulations-relation/(\d+)', [ 'uses' => 'Controller\Admin\PostulationRelationController@edit','before' => 'auth.admin']);
App::route('/admin/postulations-relation/update/(\d+)', [ 'uses' => 'Controller\Admin\PostulationRelationController@update','method' => 'post','before' => 'auth.admin']);
App::route('/admin/postulations-relation/delete/(\d+)', [ 'uses' => 'Controller\Admin\PostulationRelationController@delete','method' => 'post','before' => 'auth.admin']);
App::route('/admin/tags/relation/remove/(\w+)/(\d+)', [ 'uses' => 'Controller\TagController@remove_relation','method' => 'post','before' => 'auth.admin']);
App::route('/admin/tags/relation/add/(\w+)/(\d+)', [ 'uses' => 'Controller\TagController@add_relation','before' => 'auth.admin','method' => 'post']);
App::route('/admin/tags/(\w+)/(\d+)', [ 'uses' => 'Controller\TagController@tags','before' => 'auth.admin']);
App::route('/admin/files/resize', [ 'uses' => 'Controller\FileController@resize','method' => 'post','before' => 'auth.admin']);
App::route('/admin/files/upload', [ 'uses' => 'Controller\FileController@upload','method' => 'post','before' => 'auth.admin']);
App::route('/admin/files/order', [ 'uses' => 'Controller\FileController@order','method' => 'post','before' => 'auth.admin']);
App::route('/admin/files/remove', [ 'uses' => 'Controller\FileController@destroy','method' => 'post','before' => 'auth.admin']);
App::route('/admin/profile', 	[ 'uses' => 'Controller\Admin\PanelController@profile','before' => 'auth.user']);
App::route('/ping', [ 'uses' => 'Controller\LiveController@ping']);
App::route('/panel', [ 'uses' => 'Controller\LiveController@panel']);
App::route('/toggle', [ 'uses' => 'Controller\LiveController@toggle','method' => 'post','before' => 'auth.user']);

/* inbox */
App::route('/account/inbox', [ 'uses' => 'Controller\InboxController@index','before' => 'auth.user']);
App::route('/account/inbox/sent', [ 'uses' => 'Controller\InboxController@sent','before' => 'auth.user']);
App::route('/account/inbox/(\d+)', [ 'uses' => 'Controller\InboxController@read','before' => 'auth.user']);
App::route('/account/inbox/fetch/(\d+)', [ 'uses' => 'Controller\InboxController@fetch','before' => 'auth.user']);
App::route('/account/inbox/compose', [ 'uses' => 'Controller\InboxController@compose','before' => 'auth.admin']);
App::route('/account/inbox/send', [ 'uses' => 'Controller\InboxController@send','method' => 'post','before' => 'auth.admin']);
App::route('/account/inbox/delete/(\d+)', [ 'uses' => 'Controller\InboxController@delete','method' => 'post','before' => 'auth.user']);
App::route('/account/inbox/recipients', [ 'uses' => 'Controller\InboxController@recipients','before' => 'auth.user']);

/* guides */
App::route('/guides', ['uses' => 'Controller\GuidesController@index']);
App::route('/guides/heroes', ['uses' => 'Controller\GuidesController@layout']);
App::route('/guides/inferno', ['uses' => 'Controller\GuidesController@layout']);
App::route('/guides/items', ['uses' => 'Controller\GuidesController@layout']);
App::route('/guides/heroes/([^/]+)', ['uses' => 'Controller\GuidesController@heroe']);
App::route('/guides/([^/]+)', ['uses' => 'Controller\GuidesController@show']);
App::route('/heroes/([^/]+)', ['uses' => 'Controller\GuidesController@heroe']);

//App::route('/guides/([^/]+)', [ 'uses' => 'Controller\GuidesController@page']);

/* pages */
App::route('/story', ['uses' => 'Controller\HomeController@story']);
App::route('/(.*)', ['uses' => 'Controller\HomeController@page']);

/* filters */
App::filter('auth.user',function(){

	if( ! empty(\Bootie\Cookie::get('nl_pass'))) {
		\Controller\AuthController::autologin(\Bootie\Cookie::get('nl_user'),\Bootie\Cookie::get('nl_pass'));
	}

	if( ! session('login')){
		die(\Controller\AuthController::logout());
	}
});

App::filter('auth.advertiser',function(){

	if( ! empty(\Bootie\Cookie::get('nl_pass'))) {
		\Controller\AuthController::autologin(\Bootie\Cookie::get('nl_user'),\Bootie\Cookie::get('nl_pass'));
	}

	if( ! session('login') || session('group') !== 'advertiser'){
		die(\Controller\AuthController::logout());
	}
});

App::filter('auth.admin',function(){

	if( ! empty(\Bootie\Cookie::get('nl_pass'))) {
		\Controller\AuthController::autologin(\Bootie\Cookie::get('nl_user'),\Bootie\Cookie::get('nl_pass'));
	}

	if( ! session('login') || session('group') !== 'admin'){
		die(\Controller\AuthController::logout());
	}
});