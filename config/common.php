<?php
/**
 * Core Bootstrap
 *
 * This file contains all common system functions and View and Controller classes.
 *
 * @package		MicroMVC
 * @author		David Pennington
 * @copyright	(c) 2011 MicroMVC Framework
 * @license		http://micromvc.com/license
 ********************************** 80 Columns *********************************
 */


/**
 * Attach (or remove) multiple callbacks to an event and trigger those callbacks when that event is called.
 *
 * @param string $event name
 * @param mixed $value the optional value to pass to each callback
 * @param mixed $callback the method or function to call - FALSE to remove all callbacks for event
 */

$message_types = array(
	'success' => "ion-checkmark-circled",
	'danger' => "ion-close-circled",
	'warning' => "ion-alert-circled",
	'info' => "ion-information-circled"
);

function event($event, $value = NULL, $callback = NULL)
{
	static $events;

	// Adding or removing a callback?
	if($callback !== NULL)
	{
		if($callback)
		{
			$events[$event][] = $callback;
		}
		else
		{
			unset($events[$event]);
		}
	}
	elseif(isset($events[$event])) // Fire a callback
	{
		foreach($events[$event] as $function)
		{
			$value = call_user_func($function, $value);
		}
		return $value;
	}
}


/**
 * Fetch a config value from a module configuration file
 *
 * @param string $file name of the config
 * @param boolean $clear to clear the config object
 * @return object
 */
function config($file = 'config', $clear = FALSE)
{
	static $configs = array();

	if($clear)
	{
		unset($configs[$file]);
		return;
	}

	if(empty($configs[$file]))
	{
		//$configs[$file] = new \Micro\Config($file);
		require(SP . 'config/' . $file . EXT);
		$configs[$file] = (object) $config;
		//print dump($configs);
	}

	return $configs[$file];
}


/**
 * Return an HTML safe dump of the given variable(s) surrounded by "pre" tags.
 * You can pass any number of variables (of any type) to this function.
 *
 * @param mixed
 * @return string
 */
function dump()
{
	$string = '';
	foreach(func_get_args() as $value)
	{
		$string .= '<pre>' . h($value === NULL ? 'NULL' : (is_scalar($value) ? $value : print_r($value, TRUE))) . "</pre>\n";
	}
	return $string;
}

function dd()
{
	$string = '';
	foreach(func_get_args() as $value)
	{
		$string .= '<pre>' . h($value === NULL ? 'NULL' : (is_scalar($value) ? $value : print_r($value, TRUE))) . "</pre>\n";
	}
	die($string);
}

function template($tpl,$data=array())
{
	@extract($data);

	$content="";
	$template = SP . 'app/views/' . ( str_replace(".","/",$tpl) ) . '.php';

	if(file_exists($template)){
		ob_start();
		require $template;
		$content = ob_get_clean();
	}

	return utf8_decode($content);
}

function segments($i=0)
{
	$args = explode('/', rtrim(PATH, '/'));
	if( $i )
	{
		if( isset($args[$i]))
		{
			return $args[$i];
		}

		return false;
	}
	
	return $args;
}

/**
 * Safely fetch a $_POST value, defaulting to the value provided if the key is
 * not found.
 *
 * @param string $key name
 * @param mixed $default value if key is not found
 * @param boolean $string TRUE to require string type
 * @return mixed
 */
function post($key, $default = NULL, $string = FALSE)
{
	if(isset($_POST[$key]))
	{
		return $string ? (string)$_POST[$key] : $_POST[$key];
	}
	return $default;
}


/**
 * Safely fetch a $_GET value, defaulting to the value provided if the key is
 * not found.
 *
 * @param string $key name
 * @param mixed $default value if key is not found
 * @param boolean $string TRUE to require string type
 * @return mixed
 */
function get($key, $default = NULL, $string = FALSE)
{
	if(isset($_GET[$key]))
	{
		return $string ? (string)$_GET[$key] : $_GET[$key];
	}
	return $default;
}


/**
 * Safely fetch a $_SESSION value, defaulting to the value provided if the key is
 * not found.
 *
 * @param string $k the post key
 * @param mixed $d the default value if key is not found
 * @return mixed
 */
function session($k, $d = NULL)
{
	return isset($_SESSION[$k]) ? $_SESSION[$k] : $d;
}

/**
 * Safely update a $_SESSION value
 *
 * @param string $k the post key
 * @param mixed $d the default value if key is not found
 * @return mixed
 */
function session_put($k, $d = NULL)
{
	if(isset($_SESSION[$k])) unset($_SESSION[$k]);
	$_SESSION[$k] = $d;
	return $d;
}


/**
 * Create a random 32 character MD5 token
 *
 * @return string
 */
function token()
{
	return md5(str_shuffle(chr(mt_rand(32, 126)) . uniqid() . microtime(TRUE)));
}


/**
 * Write to the application log file using error_log
 *
 * @param string $message to save
 * @return bool
 */
function log_message($message)
{
	$path = SP . 'storage/log/' . date('Y-m-d') . '.log';

	// Append date and IP to log message
	return error_log(date('H:i:s ') . getenv('REMOTE_ADDR') . " $message\n", 3, $path);
}


/**
 * Send a HTTP header redirect using "location" or "refresh".
 *
 * @param string $url the URL string
 * @param int $c the HTTP status code
 * @param string $method either location or redirect
 */
function redirect($url = NULL, $messages = array(), $code = 302, $method = 'location')
{
	if(strpos($url, '://') === FALSE)
	{
		$url = site_url($url);
	}

	if( ! empty($messages))
	{
		flash($messages);
	}

	if( AJAX_REQUEST )
	{
		return array('redirect' => $url);
	}

	header($method == 'refresh' ? "Refresh:0;url = $url" : "Location: $url", TRUE, $code);
}

function flash($messages = array())
{
	
	global $message_types;

	foreach($message_types as $type => $data){
		if( session($type))
		{
			unset($_SESSION[$type]);
		}
	}

	if(@array_key_exists('code',$messages)){
		session_put($messages['code'],$messages);
	} else {
		foreach($messages as $data)
		{
			session_put($data['code'],$data);
		}
	}
}

function messages(){

	global $message_types;

	$messages = [];

	foreach($message_types as $type => $icon){
		if( $message = session($type)) {
			if(array_key_exists('position',$message) AND $message['position'] == 'modal'){
				$messages[] = "<script>
				      BootstrapDialog.show({
				        type: '{$type}',
				        cssClass: 'alert-dialog btn-dark',
				        size: 'size-wide',
				        title: '{$message['title']}',
				        message: '{$message['message']}',
				        buttons: [{
				          icon: 'ion-checkmark-round',
				          label: 'Entiendo',            
				          action: function(dialogRef){
				            dialogRef.close();
				          }
				        }]        
				      });
				      </script>

				 ";
			} else {
				$messages[] = "<div class='alert alert-{$type}'><i class='{$icon}'></i> &nbsp; " . $message['message'] . "</div>";
			}
			unset($_SESSION[$type]);
		}
	}

	if(empty($messages)){
		$messages[]= '<div class="alert alert-warning hide"></div>';
	}

	return implode('',$messages);
}

/*
* Generic listing quick search
*/

function search($fields, $slug = false){
	if(! $slug) $slug = segments(2);
	return '<input class="form-control" type="text" id="search" name="search" data-options=\'{"slug":"' . $slug . '","fields":"' . $fields . '"}\' data-url="/search" data-minlength="3" placeholder="' . $fields . '"><div id="search-results" class="ps-wrapper hide"></div>';
}


/*
 * Return the full URL to a path on this site or another.
 *
 * @param string $uri may contain another sites TLD
 * @return string
 *
function site_url($uri = NULL)
{
	return (strpos($uri, '://') === FALSE ? \Micro\URL::get() : '') . ltrim($uri, '/');
}
*/

/**
 * Return the full URL to a location on this site
 *
 * @param string $path to use or FALSE for current path
 * @param array $params to append to URL
 * @return string
 */
function site_url($path = NULL, array $params = NULL)
{
	// In PHP 5.4, http_build_query will support RFC 3986
	return DOMAIN . ($path ? '/'. trim($path, '/') : PATH)
		. ($params ? '?'. str_replace('+', '%20', http_build_query($params, TRUE, '&')) : '');
}

function site_asset($path = NULL, array $params = NULL, $version = VERSION)
{
	// In PHP 5.4, http_build_query will support RFC 3986
	return DOMAIN . ($path ? '/assets/'. $version . '/' . trim($path, '/') : PATH)
		. ($params ? '?'. str_replace('+', '%20', http_build_query($params, TRUE, '&')) : '');
}


/**
 * Return the current URL with path and query params
 *
 * @return string
 *
function current_url()
{
	return DOMAIN . getenv('REQUEST_URI');
}
*/

/**
 * Convert a string from one encoding to another encoding
 * and remove invalid bytes sequences.
 *
 * @param string $string to convert
 * @param string $to encoding you want the string in
 * @param string $from encoding that string is in
 * @return string
 */
function encode($string, $to = 'UTF-8', $from = 'UTF-8')
{
	// ASCII is already valid UTF-8
	if($to == 'UTF-8' AND is_ascii($string))
	{
		return $string;
	}

	// Convert the string
	return @iconv($from, $to . '//TRANSLIT//IGNORE', $string);
}


/**
 * Tests whether a string contains only 7bit ASCII characters.
 *
 * @param string $string to check
 * @return bool
 */
function is_ascii($string)
{
	return ! preg_match('/[^\x00-\x7F]/S', $string);
}


/**
 * Encode a string so it is safe to pass through the URL
 *
 * @param string $string to encode
 * @return string
 */
function base64_url_encode($string = NULL)
{
	return strtr(base64_encode($string), '+/=', '-_~');
}


/**
 * Decode a string passed through the URL
 *
 * @param string $string to decode
 * @return string
 */
function base64_url_decode($string = NULL)
{
	return base64_decode(strtr($string, '-_~', '+/='));
}


/**
 * Convert special characters to HTML safe entities.
 *
 * @param string $string to encode
 * @return string
 */
function h($string)
{
	return htmlspecialchars($string, ENT_QUOTES, 'utf-8');
}


/**
 * Filter a valid UTF-8 string so that it contains only words, numbers,
 * dashes, underscores, periods, and spaces - all of which are safe
 * characters to use in file names, URI, XML, JSON, and (X)HTML.
 *
 * @param string $string to clean
 * @param bool $spaces TRUE to allow spaces
 * @return string
 */
function sanitize($string, $spaces = TRUE)
{
	$search = array(
		'/[^\w\-\. ]+/u',			// Remove non safe characters
		'/\s\s+/',					// Remove extra whitespace
		'/\.\.+/', '/--+/', '/__+/'	// Remove duplicate symbols
	);

	$string = preg_replace($search, array(' ', ' ', '.', '-', '_'), $string);

	if( ! $spaces)
	{
		$string = preg_replace('/--+/', '-', str_replace(' ', '-', $string));
	}

	return trim($string, '-._ ');
}


/**
 * Create a SEO friendly URL string from a valid UTF-8 string.
 *
 * @param string $string to filter
 * @return string
 */
function sanitize_url($string)
{
	return urlencode(mb_strtolower(sanitize($string, FALSE)));
}


/**
 * Filter a valid UTF-8 string to be file name safe.
 *
 * @param string $string to filter
 * @return string
 */
function sanitize_filename($string)
{
	return sanitize($string, FALSE);
}


/**
 * Return a SQLite/MySQL/PostgreSQL datetime string
 *
 * @param int $timestamp
 */
function sql_date($timestamp = NULL)
{
	return date('Y-m-d H:i:s', $timestamp ?: time());
}


/**
 * Make a request to the given URL using cURL.
 *
 * @param string $url to request
 * @param array $options for cURL object
 * @return object
 */
function curl_request($url, array $options = NULL)
{
	$ch = curl_init($url);

	$defaults = array(
		CURLOPT_HEADER => 0,
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_TIMEOUT => 5,
	);

	// Connection options override defaults if given
	curl_setopt_array($ch, (array) $options + $defaults);

	// Create a response object
	$object = new stdClass;

	// Get additional request info
	$object->response = curl_exec($ch);
	$object->error_code = curl_errno($ch);
	$object->error = curl_error($ch);
	$object->info = curl_getinfo($ch);

	curl_close($ch);

	return $object;
}


/**
 * Create a RecursiveDirectoryIterator object
 *
 * @param string $dir the directory to load
 * @param boolean $recursive to include subfolders
 * @return object
 */
function directory($dir, $recursive = TRUE)
{
	$i = new \RecursiveDirectoryIterator($dir);

	if( ! $recursive) return $i;

	return new \RecursiveIteratorIterator($i, \RecursiveIteratorIterator::SELF_FIRST);
}

function filename_unique($dir, $filename, $prepend = '')
{

    $info = pathinfo($filename);
    $token = token();
    $filename = $prepend . $token;

    while (file_exists($dir . $prepend . $token . '.' . $info['extension'])) {
    	$token = token();
    }

    return $filename . '.' . $info['extension'];
}

/**
 * Make sure that a directory exists and is writable by the current PHP process.
 *
 * @param string $dir the directory to load
 * @param string $chmod value as octal
 * @return boolean
 */
function directory_is_writable($dir, $chmod = 0755)
{
	// If it doesn't exist, and can't be made
	if(! is_dir($dir) AND ! mkdir($dir, $chmod, TRUE)) return FALSE;

	// If it isn't writable, and can't be made writable
	if(! is_writable($dir) AND ! chmod($dir, $chmod)) return FALSE;

	return TRUE;
}


/**
 * Convert any given variable into a SimpleXML object
 *
 * @param mixed $object variable object to convert
 * @param string $root root element name
 * @param object $xml xml object
 * @param string $unknown element name for numeric keys
 * @param string $doctype XML doctype
 */
function to_xml($object, $root = 'data', $xml = NULL, $unknown = 'element', $doctype = "<?xml version = '1.0' encoding = 'utf-8'?>")
{
	if(is_null($xml))
	{
		$xml = simplexml_load_string("$doctype<$root/>");
	}

	foreach((array) $object as $k => $v)
	{
		if(is_int($k))
		{
			$k = $unknown;
		}

		if(is_scalar($v))
		{
			$xml->addChild($k, h($v));
		}
		else
		{
			$v = (array) $v;
			$node = array_diff_key($v, array_keys(array_keys($v))) ? $xml->addChild($k) : $xml;
			//self::from($v, $k, $node);
		}
	}

	return $xml;
}


/**
 * Return an IntlDateFormatter object using the current system locale
 *
 * @param string $locale string
 * @param integer $datetype IntlDateFormatter constant
 * @param integer $timetype IntlDateFormatter constant
 * @param string $timezone Time zone ID, default is system default
 * @return IntlDateFormatter
 */
function __date($locale = NULL, $datetype = IntlDateFormatter::MEDIUM, $timetype = IntlDateFormatter::SHORT, $timezone = NULL)
{
	return new IntlDateFormatter($locale ?: setlocale(LC_ALL, 0), $datetype, $timetype, $timezone);
}


/**
 * Format the given string using the current system locale
 * Basically, it's sprintf on i18n steroids.
 *
 * @param string $string to parse
 * @param array $params to insert
 * @return string
 */
function __($string, array $params = NULL)
{
	return msgfmt_format_message(setlocale(LC_ALL, 0), $string, $params);
}


/**
 * Color output text for the CLI
 *
 * @param string $text to color
 * @param string $color of text
 * @param string $background color
 */
function colorize($text, $color, $bold = FALSE)
{
	// Standard CLI colors
	$colors = array_flip(array(30 => 'gray', 'red', 'green', 'yellow', 'blue', 'purple', 'cyan', 'white', 'black'));

	// Escape string with color information
	return"\033[" . ($bold ? '1' : '0') . ';' . $colors[$color] . "m$text\033[0m";
}


function date_locale($datestr){
	$str = $datestr;
	if( LOCALE !== 'en'){
		switch(LOCALE){
			case 'es':
				$str = str_replace([

					// months
					'Jan',
					'Apr',
					'Aug',
					'Dec',

					// week
					'Sun',
					'Mon',
					'Tue',
					'Wed',
					'Thu',
					'Fri',
					'Sat'
				],[

					// months es
					'Ene',
					'Abr',
					'Ago',
					'Dic',

					// week es
					'Dom',
					'Lun',
					'Mar',
					'Mie',
					'Jue',
					'Vie',
					'Sab'
				],$str);

				break;
		}
	}
	return $str;
}
/**
 * Color output text for dates
 *
 * @param int $ts unix timestamp
 */
function timespan($ts) 
{
	$mins = (time() - $ts) / 60;
	$mins = round($mins);
	$span="";
	
	if($mins==0)
	{
		$span = locale('Now');
	} 
	elseif($mins > 483840)
	{ // años
		/*
		$ratio = round($mins / 483840) ;
		$span = $ratio . "y";
		*/
		$span = date_locale(date('d M y',$ts));
	} 
	elseif($mins > 40319)
	{ // meses
		/*
		$ratio = round($mins / 40320);
		$span = $ratio . "m";;
		*/
		$span = date_locale(date('d M y',$ts));
	} 
	elseif($mins > 5079 && $mins < 40319)
	{ // semanas
		/*
		$ratio = round($mins / 10080);
		$span = $ratio . "w";;
		*/
		$span = date_locale(date('d M',$ts));
	} 
	elseif($mins > 1439 && $mins < 5079)
	{ // dias
		$ratio = round($mins / 1440);
		if(LOCALE == 'en')	$span = $ratio . ' ' . locale('day') . ($ratio>1?'s':'') . ' ' . locale('date_ago');
		else $span = locale('date_ago') . ' ' . $ratio . ' ' . locale('day') . ($ratio>1?'s':'');
		//$span = date_locale(date('d.M',$ts));
	} 
	elseif($mins > 59 && $mins < 1439)
	{ // horas
		if(LOCALE == 'en')	$span = min2hour(round($mins)) . ' ' . locale('date_ago');
		else $span = locale('date_ago') . ' ' . min2hour(round($mins));
	} 
	else 
	{
		if(LOCALE == 'en')	$span = $mins . ' min ' . locale('date_ago');
		else $span = locale('date_ago') . ' ' . $mins . ' min';
	}
	
	return $span;
}

function min2hour($mins) 
{ 
    if ($mins < 0) 
    { 
        $min = Abs($mins); 
    } 
    else 
    { 
        $min = $mins; 
    } 

    $H = Floor($min / 60); 
    $M = ($min - ($H * 60)) / 100; 
    $hours = $H +  $M; 

    if ($mins < 0) 
    { 
        $hours = $hours * (-1); 
    } 

    $expl = explode(".", $hours); 
    $H = $expl[0]; 

    if (empty($expl[1])) 
    { 
        $expl[1] = 00; 
    } 
    
    $M = $expl[1]; 
    
    if (strlen($M) < 2) 
    { 
        $M = $M . 0; 
    }
    
    $hours = $H;
    
    if($M > 0 && $H < 3)
    {
    	$hours.= ":" . $M;
    }
    
    //$s = ($H > 1 || $M > 1) ? "s":"";
    $hours.= "h";//.$s; 
    
    return $hours; 
} 

function words($str,$words=30,$del='...')
{
	return str_word_count($str) < $words ? $str : implode(' ',array_slice(explode(' ',$str),0,$words)) . ' ' . $del;
}

function breadcrumb()
{
	$segments = array_values(array_filter(segments()));

	if(count($segments) < 2) return '';

	$shift = array_shift($segments);
	$breadcrumb = '';
	$acc = '';
	foreach($segments as $segment)
	{
		$acc.= '/' . segments(1) . '/' . $segment;
		$breadcrumb.= '<li><a href="' . $acc . '" title="">' . locale($segment) . '</a>/</li>';
	}
	return $breadcrumb;
}

function locale($key,$language = null,$data = array())
{
	$value = \Model\Word::select('column','word_' . ($language?:LOCALE),null,[
		'word_key' => $key
	])?:$key;

	return preg_replace_callback('/{{([^}]+)}}/', function ($m) use ($data){
		if(empty($data)) $data = $_SESSION;
        return isset($data[$m[1]]) ? $data[$m[1]] : $m[1]; 
    }, $value);
}

// End

// Custom functions

function monetize_coins($coins){
	return $coins;
}

function get_order_reference( $id ){
	return date('y') . sprintf("%04s", $id);
}

function get_order_id( $reference ){
	return (int) substr($reference, -4);
}	

function message($login, $title, $message, $sender = 'system', $links = array(), $data = array(), $email = true){

	$user = \Model\Account::find($login);

	if($user){
		// inbox

		$title = locale($title,$user->language,$data);
		$message = locale($message,$user->language,$data);

		$inbox = new \Model\Inbox;
		$inbox->title = $title;
		$inbox->content = $message;
		$inbox->save(true);

		$relation = new \Model\InboxRelation;
		$relation->inbox_id = $inbox->id;
		$relation->recipient = $login;
		$relation->login = $sender;
		$relation->sent_time = TIME;
		$relation->save(true);

		if($email AND $_SERVER['REMOTE_ADDR'] != '127.0.0.1'){
			return \Bootie\Mail\Mailer::send($user->email, $title,[ 
				'title' => locale($title),
				'message' => locale($message),
				'links'	=> (object) $links
			],'emails.base','info@nlgames.net');
		}

		return false;
	}
}

function avatar($user, $url = false){
	
	$avatar_dir = SP . '/public/upload/forum/';
	$filename = $user . '.jpg';
	$avatar = $avatar_dir . '/' . $filename;

	if(is_file($avatar)){
		return site_url('/upload/forum/' . $filename );
	}

	if($url AND substr($url,0,4) == 'http') {
		// we need to copy and save.
		@copy($url,$avatar);
	}

	return site_url('/upload/forum/' . $filename );
}

function bubble_sort($data,$key,$order='asc'){
	// sort by time update, (BUBBLE)
	$array_size = count($data);
	
	if($order=='asc')
		for($x = 0; $x < $array_size; $x++) {
			for($y = 0; $y < $array_size; $y++) {
				if($data[$x][$key] < $data[$y][$key]) {
					$hold = $data[$x];
					$data[$x] = $data[$y];
					$data[$y] = $hold;
				}
			}
		}
	elseif($order=='desc')
		for($x = 0; $x < $array_size; $x++) {
			for($y = 0; $y < $array_size; $y++) {
				if($data[$x][$key] > $data[$y][$key]) {
					$hold = $data[$x];
					$data[$x] = $data[$y];
					$data[$y] = $hold;
				}
			}
		}

	return $data;
}