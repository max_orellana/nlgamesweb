<?php namespace Model;

class ItemType extends \Bootie\ORM
{
    public static $table = 'items_types';
	public static $foreign_key = 'type_id';

    public static $belongs_to = [
    	'type_id'	=> '\Model\Item'
    ];
}