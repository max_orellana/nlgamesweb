<?php namespace Model;

class MailCode extends \Bootie\ORM 
{
    public static $table = 'email_codes';
	public static $foreign_key = 'id';

	public static $belongs_to = array(
		'login' => '\Model\Account'
	);
}