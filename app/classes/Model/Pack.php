<?php namespace Model;

class Pack extends \Bootie\ORM
{
    public static $table = 'accounts_packages';
    public static $foreign_key = 'package_id';

    public static $belongs_to = [
        'package' => '\Model\Package',
        'account' => '\Model\Account'
    ];
}