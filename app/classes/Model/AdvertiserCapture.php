<?php namespace Model;

class AdvertiserCapture extends \Bootie\ORM { 
    public static $table = 'advertiser_captures';
    public static $key = 'id';
    public static $foreign_key = 'login';

    public static $belongs_to = array(
        'account' => '\Model\Account'
    );
}