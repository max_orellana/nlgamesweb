<?php namespace Model;

class Order extends \Bootie\ORM
{
    public static $table = 'orders';
	public static $foreign_key = 'order_id';

    public static $has = array(
        'payment' => '\Model\Payment',
	);

    public static $belongs_to = [
        'account' => '\Model\Account'
    ];	
}