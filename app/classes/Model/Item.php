<?php namespace Model;
 
class Item extends \Bootie\ORM { 
    public static $table = 'items';
    public static $foreign_key = 'item_id';
    public static $order_by = ['id' => "DESC"];

    public static $belongs_to = array(
        'type' => '\Model\ItemType',
        'grade' => '\Model\ItemGrade',
        'class' => '\Model\ItemClass',
    );
}