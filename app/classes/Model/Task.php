<?php namespace Model;
 
class Task extends \Bootie\ORM
{
    public static $table = 'tasks';
    public static $icons = [
    	'bug' => 'bug text-danger',
    	'issue' => 'tasks test-success'
    ];
}