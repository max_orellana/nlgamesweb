<?php namespace Model\Forum;

class Post extends \Bootie\ORM { 
	public static $connection = 'forum';
    public static $table = 'forums_topics';
    public static $key = 'tid';    
}