<?php namespace Model\Forum;

class Account extends \Bootie\ORM { 
	public static $connection = 'forum';
    public static $table = 'forummembers';
    public static $key = 'member_id';    
	public static $foreign_key = 'member_id';    

	public static $belongs_to = array(
		'user' => '\Model\User',
	);
}