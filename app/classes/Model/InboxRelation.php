<?php namespace Model;

class InboxRelation extends \Bootie\ORM
{
    public static $table = 'inbox_relation';
	public static $foreign_key = 'id';

	public static $belongs_to = array(
		'forum'	=> '\Model\Account',
		'inbox' => '\Model\Inbox',
	);	
}