<?php namespace Model;
 
class CouponRelation extends \Bootie\ORM
{
    public static $table = 'coupons_relation';
    public static $foreign_key = 'id';
	public static $cascade_delete = false;
	
    public static $has = array(
        'coupon' => '\Model\Coupon'
    );
}