<?php namespace Model;
 
class PostTag extends \Bootie\ORM
{
    public static $table = 'tags_relation';
    public static $foreign_key = 'id';

    public static $has = array(
        'post' => '\Model\Post',
        'tag' => '\Model\Tag',
    );
}