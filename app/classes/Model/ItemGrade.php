<?php namespace Model;

class ItemGrade extends \Bootie\ORM
{
    public static $table = 'items_grades';
	public static $foreign_key = 'grade_id';

    public static $belongs_to = [
    	'grade_id'	=> '\Model\Item'
    ];
}