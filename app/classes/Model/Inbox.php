<?php namespace Model;

class Inbox extends \Bootie\ORM
{
    public static $table = 'inbox';
    public static $key = 'id';
	public static $foreign_key = 'inbox_id';
}