<?php namespace Model;

class Online extends \Bootie\ORM
{
    public static $table = 'online';

	public static $belongs_to = array(
		'account'	=> '\Model\Account',
	);		
}