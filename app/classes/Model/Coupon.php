<?php namespace Model;

class Coupon extends \Bootie\ORM
{
    public static $table = 'coupons';
	public static $foreign_key = 'coupon_id';

    public static $belongs_to = [
    	'type'	=> '\Model\CouponType',
    	'coupon_id'	=> '\Model\CouponRelation'
    ];
}