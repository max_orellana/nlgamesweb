<?php namespace Model;

class AccountPackage extends \Bootie\ORM { 
    public static $table = 'accounts_packages';
    public static $key = 'id';

	public static $belongs_to = [
		'package' => '\Model\Package'
	];
}