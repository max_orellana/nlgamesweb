<?php namespace Model;

class Account extends \Bootie\ORM { 
    public static $table = 'accounts';
    public static $key = 'login';
	public static $foreign_key = 'login';

    public static $belongs_to = array(
        'package' => '\Model\Package'        
    );
}