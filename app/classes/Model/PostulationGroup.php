<?php namespace Model;

class PostulationGroup extends \Bootie\ORM
{
    public static $table = 'postulations_groups';
	public static $foreign_key = 'group_id';

    public static $has = [
        'postulations' => '\Model\Postulation'
    ];
}