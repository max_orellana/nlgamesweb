<?php namespace Model;

class PostulationRelation extends \Bootie\ORM
{
    public static $table = 'postulations_relation';
	public static $foreign_key = 'id';

    public static $belongs_to = [
        'postulation' => '\Model\Postulation'
    ];
}