<?php namespace Model;

class Package extends \Bootie\ORM
{
    public static $table = 'packages';
    public static $foreign_key = 'package_id';

    public static $belongs_to = array(
        'package' => '\Model\Pack'
    );

}