<?php namespace Model\Game;

class Character extends \Bootie\ORM {
	public static $connection = 'game';
	public static $key = 'charId';
    public static $table = 'characters';
}