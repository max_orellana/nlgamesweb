<?php namespace Model\Game;

class Group extends \Bootie\ORM {
    public static $table = 'groups';
    public static $foreign_key = 'group_id'; 
}