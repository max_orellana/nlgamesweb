<?php namespace Model\Game;

class Item extends \Bootie\ORM {
	public static $connection = 'game';
	public static $key = 'owner_id';
    public static $table = 'items';
}