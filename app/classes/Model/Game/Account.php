<?php namespace Model\Game;

class Account extends \Bootie\ORM {
	public static $connection = 'game';
	public static $key = 'login';
    public static $table = 'accounts';
}