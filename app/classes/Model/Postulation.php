<?php namespace Model;

class Postulation extends \Bootie\ORM
{
    public static $table = 'postulations';
	public static $foreign_key = 'postulation_id';
	public static $perpage = 10;

    public static $belongs_to = array(
    	'group'	=> '\Model\PostulationGroup',
        'postulation_id' => '\Model\PostulationRelation'
    );
}