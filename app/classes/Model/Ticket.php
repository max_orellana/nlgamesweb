<?php namespace Model;

class Ticket extends \Bootie\ORM
{
    public static $table = 'tickets';
	public static $foreign_key = 'ticket_id';

    public static $belongs_to = array(
        'tag'	=>  '\Model\Tag',
        'ticket_id'	=>  '\Model\Ticket',
    );
}