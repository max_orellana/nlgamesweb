<?php namespace Model;

class ItemClass extends \Bootie\ORM
{
    public static $table = 'items_classes';
	public static $foreign_key = 'class_id';

    public static $belongs_to = [
    	'class_id'	=> '\Model\Item'
    ];
}