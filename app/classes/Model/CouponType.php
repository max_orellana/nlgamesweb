<?php namespace Model;

class CouponType extends \Bootie\ORM
{
    public static $table = 'coupons_types';
	public static $foreign_key = 'type_id';

    public static $belongs_to = [
    	'type_id'	=> '\Model\Coupon'
    ];
}