<?php namespace Controller;

class BaseController {
	
	public function __construct(){
		if(isset($_REQUEST['at'])){
			if(isset($_SESSION['advertise_token'])){
				unset($_SESSION['advertise_token']);
			}
			$_SESSION['advertise_token'] = $_REQUEST['at'];
		}
	}
}