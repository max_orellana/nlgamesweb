<?php namespace Controller\Admin;

class TicketController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){

		$entries = \Model\Ticket::paginate([
			'updated' => "DESC"
		],[
			'ticket_id' => 0
		],10);  

		return \Bootie\App::view('panel.admin.tickets.index',[
			'entries'	=> $entries,
		]);
	}

	public function create(){
		return \Bootie\App::view('panel.admin.tickets.edit');
	}

	public function edit($id){
		if(is_numeric($id))
		{
			return \Bootie\App::view('panel.admin.tickets.edit',[
				'rows' => \Model\Ticket::fetch([
					'id = ' . $id . ' OR ticket_id IN(' . $id . ')'
				],0,0,[
					'created' => "ASC"
				]),
				'tags'	=> \Model\Tag::fetch([
					'type'	=> 'ticket'
				])
			]);
		}
		
		return \Exception('Ticket ID was not provided');
	}

	public function send(){

		@extract($_POST);

		if(strlen($description)){

			$login = session('login');

			$row = new \Model\Ticket;

			if(isset($ticket_id)){
				$row->ticket_id = $ticket_id;

				$parent = new \Model\Ticket;
				$parent->id = $ticket_id;
				$parent->status_id = $status_id;
				$parent->save();
			}

			$row->user = $login;
			$row->operator = session('login');
			$row->group_id = 1;
			$row->content = $description;
			$row->created = TIME;
			$row->updated = TIME;

			$row->save();

			return redirect("/admin/tickets",[
				'code' => 'success',
				'message' => locale('ticket_updated')
			]);
		}

		return redirect('/admin/tickets',[
			'code' => 'danger',
			'message' => "Entry description was not provided"
		]);
	}

	public function update($id){

		if(is_numeric($id))
		{
			extract($_POST);

			$entry = \Model\Ticket::row([
				'id'	=> $id
			]);

			$entry->id = $id;
			$entry->word_key = $word_key;
			$entry->word_es = $word_es;
			$entry->word_en = $word_en;

			$result = $entry->save();

			return redirect('/admin/tickets',[
				'code' => 'success',
				'message' => "Entry <strong>{$entry->title}</strong> has been updated"
			]);
		}

		return redirect('/admin/tickets',[
			'code' => 'danger',
			'message' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$user_id = session('user_id');

			$entry = \Model\Ticket::row([
				'id' => $id
			]);

			if( $entry )
			{

				$id = $entry->id;
				$entry->delete();

				return redirect('/admin/tickets',[
					'code' => 'success',
					'message' => "Entry <strong>{$id}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/tickets',[
			'code' => 'danger',
			'message' => "Entry was not found"
		]);
	}
}