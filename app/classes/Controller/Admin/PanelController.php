<?php namespace Controller\Admin;

class PanelController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function alerts(){
		return \Bootie\App::view('panel.admin.alerts');
	}

	public function alert(){
		extract($_POST);
		return [
			'code' => $code,
			'position'	=> 'modal',
			'title' => locale('modal_alert_beta_title'),
			'message' => locale('modal_alert_beta_text'),
		];
	}

	public function dash(){
		return \Bootie\App::view('panel.admin.dash',[
			'posts_count'	=> \Model\Post::count()
		]);
	}

	public function realtime(){

		$data = \Model\Online::fetch([
			'last_activity > ' . (TIME - 60) .' AND login <> \'' . session('login') . '\''
		]);

		$online=[];
		foreach($data as $row){
			$online[] = [
				'login' => $row->login, 
				'since' => timespan($row->account->last_login)
			];
		}

		$json = [
			'online' => $online,
			'postcount' => \Model\Post::count(),
			'usercount' => \Model\Account::count()
		];

		return \Bootie\App::ajax($json);
	}

	public function search(){
		@extract($_GET);

		$json = [
			'search' => $search,
			'no_match' =>  locale('no_match'),
			'slug' => $options['slug'],
			'rows' => array()
		];

		if(strlen($search) > 3){
			$db = new \Bootie\Database();
			$fields = explode(',',$options['fields']);
			$where =  implode(" like '%{$search}%' OR ", $fields) . " like '%{$search}%'";
			$sql = 'select ' . $options['fields'] . ' from ' . str_replace('-','_',$options['slug']) .  ' where ' . $where;

			$rows = $db->fetch($sql);

			foreach($rows as $i => $row){
				foreach($row as $key  => $val){
					if(strlen($val) > 255) $row->{$key} = strip_tags(words($val,10));
				}
			}

			$json['rows'] = $rows;
		}
			
		return \Bootie\App::ajax($json);
	}

	public function online(){

		$prep = 0;
		$rows = \Model\Online::fetch([
			'last_activity > ' . (TIME - 60),
			'login <> \'' . session('login') . '\''
		]);

		if(empty($online)){
			$prep = 1;
			$rows = \Model\Online::fetch([
				'last_activity > ' . (TIME - (3600 * 24 * 1)),
				"login <> '" . session('login') . "'"
			],0,0,[
				'last_activity' => "DESC"
			]);
		}
		return \Bootie\App::view('panel.admin.online',[
			'entries' => $rows,
			'prep' => $prep
		]);
	}	
}