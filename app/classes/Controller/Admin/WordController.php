<?php namespace Controller\Admin;

class WordController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){
		return \Bootie\App::view('panel.admin.words.index',[
			'entries'	=> \Model\Word::paginate([
				'id' => "DESC"
			],null,10)
		]);
	}

	public function create(){
		return \Bootie\App::view('panel.admin.words.create');
	}

	public function edit($id){

		if(is_numeric($id))
		{
			$entry = \Model\Word::row([
				'id' => $id
			]);
			
			return \Bootie\App::view('panel.admin.words.edit',[
				'entry'	=> $entry
			]);
		}
		
		return \Exception('Word ID was not provided');
	}

	public function update($id){

		@extract($_POST);

		$entry = new \Model\Word();		

		if($id)
		{
			$entry->id = $id;
		}

		$entry->word_key = $word_key;
		$entry->word_es = $word_es;
		$entry->word_en = $word_en;

		$result = $entry->save();

		if($result){
			return redirect('/admin/words',[
				'code' => 'success',
				'message' => "Entry <strong>{$entry->word_key}</strong> has been updated"
			]);
		}

		return redirect('/admin/words',[
			'code' => 'danger',
			'message' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$user_id = session('user_id');

			$entry = \Model\Word::row([
				'id' => $id
			]);

			if( $entry )
			{

				$title = $entry->word_key;
				$entry->delete();

				return redirect('/admin/words',[
					'code' => 'success',
					'message' => "Entry <strong>{$title}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/words',[
			'code' => 'danger',
			'message' => "Entry was not found"
		]);
	}
}