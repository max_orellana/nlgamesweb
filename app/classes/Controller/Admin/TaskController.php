<?php namespace Controller\Admin;

class TaskController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){
		return \Bootie\App::view('panel.admin.tasks.index',[
			'entries'	=> \Model\Task::paginate([
				'id' => "DESC"
			],null,10)
		]);
	}

	public function create(){
		return \Bootie\App::view('panel.admin.tasks.create');
	}

	public function edit($id){

		if(is_numeric($id))
		{
			$entry = \Model\Task::row([
				'id' => $id
			]);
			
			return \Bootie\App::view('panel.admin.tasks.edit',[
				'entry'	=> $entry
			]);
		}
		
		return \Exception('Task ID was not provided');
	}

	public function update($id){

		@extract($_POST);

		$entry = new \Model\Task();		

		if($id)
		{
			$entry->id = $id;
			$entry->updated = TIME;
		} else {
			$entry->created = TIME;
		}

		$entry->title = $title;
		$entry->caption = $caption;
		$entry->controller = $controller;
		$entry->developer = $developer;
		$entry->updated_by = session('login');
		$entry->type = empty($type)?'issue':$type;
		$entry->urgent = empty($urgent)?0:1;
		$entry->due = $due;

		$result = $entry->save();

		if($result){
			return redirect('/admin/tasks',[
				'code' => 'success',
				'message' => "Entry <strong>{$entry->title}</strong> has been updated"
			]);
		}

		return redirect('/admin/tasks',[
			'code' => 'danger',
			'message' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$user_id = session('user_id');

			$entry = \Model\Task::row([
				'id' => $id
			]);

			if( $entry )
			{

				$title = $entry->title;
				$entry->delete();

				return redirect('/admin/tasks',[
					'code' => 'success',
					'message' => "Entry <strong>{$title}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/tasks',[
			'code' => 'danger',
			'message' => "Entry was not found"
		]);
	}
}