<?php namespace Controller\Admin;

class PostulationRelationController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){
		return \Bootie\App::view('panel.admin.postulations-relation.index',[
			'entries'	=> \Model\PostulationRelation::paginate([
				'updated' => "DESC"
			],null,10)
		]);
	}

	public function create(){

		$p = new \Model\Postulation();
		$p->title = 'New Postulation';
		$p->user_id = session('user_id');
		$p->created = TIME;
		$p->updated = TIME;
		$p->author = session('title');
		$p->save();
		$id = $p->id;

		$entry = \Model\Postulation::row([
			'id' => $id
		]);
		
		return \Bootie\App::view('panel.admin.postulations-relation.edit',[
			'entry'	=> $entry
		]);
	}

	public function edit($id){

		if(is_numeric($id))
		{
			$entry = \Model\PostulationRelation::row([
				'id' => $id
			]);

			$parts = explode(" ",$entry->interview_schedule);
			$date = explode("-",$parts[0]);
			$time = explode(":",$parts[1]);
			$schedule = (object) [
				'date' => $date,
				'time' => $time
			];
			
			return \Bootie\App::view('panel.admin.postulations-relation.edit',[
				'entry'	=> $entry,
				'schedule' => $schedule
			]);
		}
		
		return \Exception('Postulation ID was not provided');
	}

	public function update($id){

		if(is_numeric($id))
		{
			extract($_POST);

			$entry = \Model\PostulationRelation::find($id);
			$erro = "";

			if($entry){
				if($status == 4){
					if(empty($interview_schedule)) $erro = locale('datetime_year_mandatory');
				}

				if( ! empty($erro)){
					return redirect('/admin/postulations-relation',[
						'code' => 'danger',
						'message' => $erro
					]);
				}

				$entry->status = $status;
				$entry->interview_schedule = $interview_schedule;
				$entry->save();

				/* Send notification if active */

				if($status > 1) {
					message($entry->login,'notification_postulation_update_title','notification_postulation_status_' . $status,'system',null,['user' => $entry->login]);
				}

				if($status == 5) {
					// check user access level
					if($entry->postulation->accesslevel){

						// update account group

						$group = \Controller\AuthController::find_group_accesslvl($entry->postulation->accesslevel);

						$account = \Model\Account::find($entry->login);
						$account->group_id = $group->id;
						$account->save();

						\Controller\User\PanelController::set_access($entry->postulation->accesslevel,$entry->login);
					}
				}
			}

			return redirect('/admin/postulations-relation',[
				'code' => 'success',
				'message' => "Postulation <strong>{$entry->login}</strong> has been updated"
			]);
		}

		return redirect('/admin/postulations-relation',[
			'code' => 'danger',
			'message' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$user_id = session('user_id');

			$entry = \Model\PostulationRelation::row([
				'id' => $id
			]);

			if( $entry )
			{

				$id = $entry->id;
				$entry->delete();

				return redirect('/admin/postulations-relation',[
					'success' => "Entry <strong>{$id}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/postulations-relation',[
			'danger' => "Entry was not found"
		]);
	}
}