<?php namespace Controller\Admin;

class ItemClassController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){
		return \Bootie\App::view('panel.admin.items-classes.index',[
			'entries'	=> \Model\ItemClass::paginate([
				'id' => "DESC"
			],null,10)
		]);
	}

	public function create(){
		return \Bootie\App::view('panel.admin.items-classes.create');
	}

	public function edit($id){

		if(is_numeric($id))
		{
			$entry = \Model\ItemClass::row([
				'id' => $id
			]);
			
			return \Bootie\App::view('panel.admin.items-classes.edit',[
				'entry'	=> $entry
			]);
		}
		
		return \Exception('Item ID was not provided');
	}

	public function update($id){

		@extract($_POST);

		$entry = new \Model\ItemClass();		

		if($id)
		{
			$entry->id = $id;
		}
		else 
		{
			$exists = \Model\ItemClass::row([
				'slug' => $slug
			]);

			if($exists) {
				return redirect('/admin/items-classes',[
					'code' => 'danger',
					'message' => "Entry {$slug} already exists"
				]);
			}			
		}

		$entry->slug = $slug;

		$word = \Model\Word::row([
			'word_key' => $slug
		]);

		if( ! $word){
			$word = new \Model\Word();
			$word->word_key = $slug;
		}

		$word->word_es = $title_es;
		$word->word_en = $title_en;
		$word->save();

		$result = $entry->save();

		if($result){
			return redirect('/admin/items-classes',[
				'code' => 'success',
				'message' => "Entry <strong>{$entry->slug}</strong> has been updated"
			]);
		}

		return redirect('/admin/items-classes',[
			'code' => 'danger',
			'message' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$entry = \Model\ItemClass::row([
				'id' => $id
			]);

			if( $entry )
			{

				$title = $entry->title;
				$entry->delete();

				return redirect('/admin/items-classes',[
					'code' => 'success',
					'message' => "Entry <strong>{$title}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/items-classes',[
			'code' => 'danger',
			'message' => "Entry was not found"
		]);
	}
}