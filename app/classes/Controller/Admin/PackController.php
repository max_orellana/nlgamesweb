<?php namespace Controller\Admin;

class PackController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){
		return \Bootie\App::view('panel.admin.packs.index',[
			'entries'	=> \Model\Pack::paginate([
				'id' => "DESC"
			],null,10)
		]);
	}

	public function create(){
		return \Bootie\App::view('panel.admin.packs.create');
	}

	public function edit($id){

		if(is_numeric($id))
		{
			$entry = \Model\Pack::row([
				'id' => $id
			]);
			
			return \Bootie\App::view('panel.admin.packs.edit',[
				'entry'	=> $entry
			]);
		}
		
		return \Exception('Pack ID was not provided');
	}

	public function update($id){

		@extract($_POST);

		$entry = new \Model\Pack();		

		if($id)
		{
			$entry->id = $id;
		}

		$entry->title = $title;
		$entry->slug = $slug;
		$entry->caption = $caption;
		$entry->damage = $damage;
		$entry->armor = $armor;
		$entry->agility = $agility;
		$entry->utility = $utility;
		$entry->speed = $speed;
		$entry->difficulty = $difficulty;

		$result = $entry->save();

		if($result){
			return redirect('/admin/Packs',[
				'code' => 'success',
				'message' => "Entry <strong>{$entry->title}</strong> has been updated"
			]);
		}

		return redirect('/admin/Packs',[
			'code' => 'danger',
			'message' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$user_id = session('user_id');

			$entry = \Model\Pack::row([
				'id' => $id
			]);

			if( $entry )
			{

				$title = $entry->title;
				$entry->delete();

				return redirect('/admin/Packs',[
					'code' => 'success',
					'message' => "Entry <strong>{$title}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/Packs',[
			'code' => 'danger',
			'message' => "Entry was not found"
		]);
	}
}