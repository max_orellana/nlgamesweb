<?php namespace Controller\Admin;

class OrderController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){
		return \Bootie\App::view('panel.admin.orders.index',[
			'entries'	=> \Model\Order::paginate([
				'id' => "DESC"
			],null,10)
		]);
	}

	public function create(){
		return \Bootie\App::view('panel.admin.orders.create');
	}

	public function edit($id){

		if(is_numeric($id))
		{
			$entry = \Model\Order::row([
				'id' => $id
			]);
			
			return \Bootie\App::view('panel.admin.orders.edit',[
				'entry'	=> $entry
			]);
		}
		
		return \Exception('Order ID was not provided');
	}

	public function update($id){

		@extract($_POST);

		$entry = new \Model\Order();		

		if($id)
		{
			$entry->id = $id;
		}

		$entry->title = $title;
		$entry->slug = $slug;
		$entry->caption = $caption;
		$entry->damage = $damage;
		$entry->armor = $armor;
		$entry->agility = $agility;
		$entry->utility = $utility;
		$entry->speed = $speed;
		$entry->difficulty = $difficulty;

		$result = $entry->save();

		if($result){
			return redirect('/admin/Orders',[
				'code' => 'success',
				'message' => "Entry <strong>{$entry->title}</strong> has been updated"
			]);
		}

		return redirect('/admin/Orders',[
			'code' => 'danger',
			'message' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$user_id = session('user_id');

			$entry = \Model\Order::row([
				'id' => $id
			]);

			if( $entry )
			{

				$title = $entry->title;
				$entry->delete();

				return redirect('/admin/Orders',[
					'code' => 'success',
					'message' => "Entry <strong>{$title}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/Orders',[
			'code' => 'danger',
			'message' => "Entry was not found"
		]);
	}
}