<?php namespace Controller\Admin;

class PostController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){

		extract($_GET);

		$filter = [];

		if(!empty($tag)){
			$posts_ids = \Model\PostTag::select('fetch','post_id',null,[
				'tag_id IN(' . $tag . ')'
			]);

			$filter = [
				'id IN (' . implode(',',$posts_ids) . ')'
			];
		}

		return \Bootie\App::view('panel.admin.posts.index',[
			'tags'	=> self::find_all_tags(),
			'total'	=> $entries = \Model\Post::count(),
			'entries'	=> $entries = \Model\Post::paginate([
				'updated' => "DESC"
			],$filter,10)
		]);
	}

	public function find_all_tags(){

		$count = [];

		$posts_ids = \Model\Post::select('fetch','id');

		if(count($posts_ids)){

			$tags_ids = \Model\PostTag::select('fetch','tag_id',null,[
				'post_id IN(' . implode(',',$posts_ids) . ')'
			]);

			if(count($tags_ids)){

				foreach($tags_ids as $tag){
					if(!isset($count[$tag])) $count[$tag] = 0;
					$count[$tag]++;
				}

				$tags_unique = array_unique($tags_ids);

				//dd($count);

				$tags = \Model\Tag::fetch([
					'id IN(' . implode(',',$tags_unique) . ')'
				]);

				return [
					'entries' => $tags,
					'count' => $count
				];
			}
		}

		return [];
	}


	public function create(){

		$p = new \Model\Post();
		$p->title_es = 'New Post';
		$p->user_id = session('user_id');
		$p->created = TIME;
		$p->updated = TIME;
		$p->login = session('title');
		$p->save();
		$id = $p->id;

		$entry = \Model\Post::row([
			'id' => $id
		]);
		
		return \Bootie\App::view('panel.admin.posts.edit',[
			'entry'	=> $entry
		]);
	}

	public function edit($id){

		if(is_numeric($id))
		{
			$entry = \Model\Post::row([
				'id' => $id
			]);
			
			return \Bootie\App::view('panel.admin.posts.edit',[
				'entry'	=> $entry
			]);
		}
		
		return \Exception('Post ID was not provided');
	}

	public function update($id){

		if(is_numeric($id))
		{

			extract($_POST);	

			if($content_es == '<p><br></p>') $content_es = "";
			if($content_en == '<p><br></p>') $content_en = "";

			$entry = new \Model\Post();
			$entry->id = $id;
			$entry->slug = ! empty($slug) ? $slug : sanitize($title_es,false);
			$entry->title_es = $title_es;
			$entry->title_en = $title_en;
			$entry->caption_es = $caption_es;
			$entry->caption_en = $caption_en;
			$entry->content_es = $content_es;
			$entry->content_en = $content_en;
			$entry->privacy_id = $privacy_id;
			$entry->button_text = $button_text;
			$entry->external_link = $external_link;
			$entry->login = session('login');
			$entry->updated = TIME;
			$result = $entry->save();

			return redirect('/admin/posts',[
				'code' => 'success',
				'message' => "Entry <strong>{$entry->title_es}</strong> has been updated"
			]);
		}

		return redirect('/admin/posts',[
			'code' => 'danger',
			'message' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$user_id = session('user_id');

			$entry = \Model\Post::row([
				'id' => $id
			]);

			if( $entry )
			{

				foreach($entry->tags() as $tag)
				{

					$tag_id = \Model\Tag::select('column','id',null,[
						'tag' => $tag->tag,
						'user_id' => $user_id
					]);

					$tags = \Model\TagRelation::row([
						'id' => $tag_id,
						'post_id' => $id
					]);

					if($tags)
					{
						$tags->delete();
					}
				}

				foreach($entry->files() as $file)
				{
					\Bootie\Image::destroy_group($file->name,'posts');

					\Model\File::row([
						'id' => $file->id
					])->delete();
				}

				$title = $entry->title_es;
				$entry->delete();

				return redirect('/admin/posts',[
					'code' => 'success',
					'message' => "Entry <strong>{$title}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/posts',[
			'code' => 'danger',
			'message' => "Entry was not found"
		]);
	}
}