<?php namespace Controller\Admin;

class PostulationController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){

		$requested = [];
		$applicants = [];
		$entries = \Model\Postulation::paginate([
			'updated' => "DESC"
		],null,10);

		foreach($entries as $entry){
			$count = \Model\PostulationRelation::count([
				'postulation_id' => $entry->id,
				'status' => 1
			]);

			$applicants[$entry->id] = ($count + $entry->base);

		}

		return \Bootie\App::view('panel.admin.postulations.index',[
			'entries'	=> $entries,
			'requested'	=> $requested,
			'applicants'	=> $applicants
		]);
	}

	public function edit($id){

		if(is_numeric($id))
		{
			$entry = \Model\Postulation::row([
				'id' => $id
			]);

			$groups = \Model\PostulationGroup::fetch();

			return \Bootie\App::view('panel.admin.postulations.edit',[
				'entry'	=> $entry,
				'groups'	=> $groups
			]);
		}
		
		return \Exception('Postulation ID was not provided');
	}

	public function create(){

		$groups = \Model\PostulationGroup::fetch();

		return \Bootie\App::view('panel.admin.postulations.create',[
			'groups'	=> $groups
		]);		
	}

	public function update($id){

		if(is_numeric($id))
		{

			extract($_POST);

			if( $id ){
				$entry = \Model\Postulation::find($id);
			} else {
				$entry = new \Model\Postulation;
				$entry->created = TIME;
			}

			$entry->slug = ! empty($slug) ? sanitize($slug) : sanitize($title,false);
			$entry->group_id = $group_id;
			$entry->vacancies = $vacancies;
			$entry->base = $base;
			$entry->updated = TIME;
			$result = $entry->save();

			$word = \Model\Word::row([
				'word_key' => 'postulations_' . $entry->slug
			]);

			if( ! $word) $word = new \Model\Word;

			$word->word_key = 'postulations_' . $slug;

			foreach(config()->languages as $language){
				$word->{'word_' . $language} = $_POST["title_{$language}"];
			}
			
			$word->save();

			$word = \Model\Word::row([
				'word_key' => 'postulations_' . $entry->slug . '_text'
			]);

			if( ! $word) $word = new \Model\Word;
			
			$word->word_key = 'postulations_' . $slug . '_text';

			foreach(config()->languages as $language){
				$word->{'word_' . $language} = $_POST["description_{$language}"];
			}

			$word->save();

			return redirect('/admin/postulations',[
				'code' => 'danger',
				'message' => "Entry <strong>{$title_es}</strong> has been updated"
			]);
		}

		return redirect('/admin/postulations',[
			'code' => 'danger',
			'message' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$user_id = session('user_id');

			$entry = \Model\Postulation::row([
				'id' => $id
			]);

			if( $entry )
			{

				$id = $entry->id;
				$entry->delete();

				return redirect('/admin/postulations',[
					'code' => 'danger',
					'message' => "Entry <strong>{$id}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/postulations',[
			'code' => 'danger',
			'message' => "Entry was not found"
		]);
	}
}