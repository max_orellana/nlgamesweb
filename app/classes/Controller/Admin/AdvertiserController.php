<?php namespace Controller\Admin;

class AdvertiserController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){
		$group =  \Model\Game\Group::row(['slug' => "advertiser"]);
		return \Bootie\App::view('panel.admin.advertisers.index',[
			'entries'	=> \Model\Account::paginate([
				'id' => "DESC"
			],[
				'group_id' => $group->id
			],10)
		]);
	}

	public function create(){
		return \Bootie\App::view('panel.admin.advertisers.create');
	}

	public function edit($id){

		if(is_numeric($id))
		{
			return \Bootie\App::view('panel.admin.advertisers.edit',[
				'entry'	=> \Model\Account::row([
					'id' => $id
				]),
				'orders' => \Model\Order::fetch([
					'source' => "advertiser"
				])
			]);
		}
		
		return \Exception('Advertiser ID was not provided');
	}

	public function update($id){

		@extract($_POST);

		$entry = new \Model\Advertiser();		

		if($id)
		{
			$entry->id = $id;
		}

		$entry->title = $title;
		$entry->slug = $slug;
		$entry->caption = $caption;
		$entry->damage = $damage;
		$entry->armor = $armor;
		$entry->agility = $agility;
		$entry->utility = $utility;
		$entry->speed = $speed;
		$entry->difficulty = $difficulty;

		$result = $entry->save();

		if($result){
			return redirect('/admin/Advertisers',[
				'code' => 'success',
				'message' => "Entry <strong>{$entry->title}</strong> has been updated"
			]);
		}

		return redirect('/admin/Advertisers',[
			'code' => 'danger',
			'message' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$user_id = session('user_id');

			$entry = \Model\Advertiser::row([
				'id' => $id
			]);

			if( $entry )
			{

				$title = $entry->title;
				$entry->delete();

				return redirect('/admin/Advertisers',[
					'code' => 'success',
					'message' => "Entry <strong>{$title}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/Advertisers',[
			'code' => 'danger',
			'message' => "Entry was not found"
		]);
	}
}