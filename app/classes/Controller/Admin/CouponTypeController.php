<?php namespace Controller\Admin;

class CouponTypeController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){
		return \Bootie\App::view('panel.admin.coupons-types.index',[
			'entries'	=> \Model\CouponType::paginate([
				'id' => "DESC"
			],null,10)
		]);
	}

	public function create(){
		return \Bootie\App::view('panel.admin.coupons-types.create');
	}

	public function edit($id){

		if(is_numeric($id))
		{
			$entry = \Model\CouponType::row([
				'id' => $id
			]);
			
			return \Bootie\App::view('panel.admin.coupons-types.edit',[
				'entry'	=> $entry
			]);
		}
		
		return \Exception('CouponType ID was not provided');
	}

	public function update($id){

		@extract($_POST);

		$entry = new \Model\CouponType();		

		if($id)
		{
			$entry->id = $id;
		}

		$entry->slug = $slug;
		$entry->caption = $caption;

		$result = $entry->save();

		if($result){
			return redirect('/admin/coupons-types',[
				'code' => 'success',
				'message' => "Entry <strong>{$entry->slug}</strong> has been updated"
			]);
		}

		return redirect('/admin/coupons-types',[
			'code' => 'danger',
			'message' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$user_id = session('user_id');

			$entry = \Model\CouponType::row([
				'id' => $id
			]);

			if( $entry )
			{

				$title = $entry->word_key;
				$entry->delete();

				return redirect('/admin/coupons-types',[
					'code' => 'success',
					'message' => "Entry <strong>{$title}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/coupons-types',[
			'code' => 'danger',
			'message' => "Entry was not found"
		]);
	}
}