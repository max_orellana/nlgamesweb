<?php namespace Controller\Admin;

class ItemController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){
		return \Bootie\App::view('panel.admin.items.index',[
			'entries'	=> \Model\Item::paginate([
				'id' => "DESC"
			],null,10)
		]);
	}

	public function create(){
		return \Bootie\App::view('panel.admin.items.create',[
			'types' => \Model\ItemType::fetch(),
			'grades' => \Model\ItemGrade::fetch(),
			'classes' => \Model\ItemClass::fetch()
		]);
	}

	public function edit($id){

		if(is_numeric($id))
		{
			$entry = \Model\Item::row([
				'id' => $id
			]);

			return \Bootie\App::view('panel.admin.items.edit',[
				'types' => \Model\ItemType::fetch(),
				'grades' => \Model\ItemGrade::fetch(),
				'classes' => \Model\ItemClass::fetch(),
				'entry'	=> $entry
			]);
		}
		
		return \Exception('Item ID was not provided');
	}

	public function update($id){

		@extract($_POST);

		$entry = new \Model\Item();		

		if($id)
		{
			$entry->id = $id;
		}

		$entry->price = $price;
		$entry->game_id = $game_id;
		$entry->type_id = $type_id;
		$entry->grade_id = $grade_id;
		$entry->class_id = $class_id;
		$entry->original_skin = $original_skin;
		$entry->title = $title;

		if(isset($_FILES['video']) AND ! $_FILES['video']['error']){
			$entry->video = \Controller\FileController::upload('items','video');
		}

		if(isset($_FILES['image']) AND ! $_FILES['image']['error']){
			$entry->image = \Controller\FileController::upload('items','image');
		}

		$result = $entry->save();

		if($result){
			return redirect('/admin/items',[
				'code' => 'success',
				'message' => "Entry <strong>{$entry->title}</strong> has been updated"
			]);
		}

		return redirect('/admin/items',[
			'code' => 'danger',
			'message' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$user_id = session('user_id');

			$entry = \Model\Item::row([
				'id' => $id
			]);

			if( $entry )
			{

				$title = $entry->title;
				$entry->delete();

				return redirect('/admin/items',[
					'code' => 'success',
					'message' => "Entry <strong>{$title}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/items',[
			'code' => 'danger',
			'message' => "Entry was not found"
		]);
	}
}