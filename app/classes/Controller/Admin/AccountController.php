<?php namespace Controller\Admin;

class AccountController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){

		$data = \Model\Online::fetch([
			'last_activity > ' . (TIME - 60) 
		]);

		$online=[];
		foreach($data as $row){
			$online[$row->login] = [
				'since' => timespan($row->account->last_login)
			];
		}

		return \Bootie\App::view('panel.admin.accounts.index',[
			'online' => $online,
			'entries'	=> \Model\Account::paginate([
				'last_login' => "DESC"
			],null,10)
		]);
	}

	public function create(){
		return \Bootie\App::view('panel.admin.accounts.create');
	}

	public function edit($id){

		if(is_numeric($id))
		{
			$entry = \Model\Account::row([
				'id' => $id
			]);
			
			return \Bootie\App::view('panel.admin.accounts.edit',[
				'entry'	=> $entry
			]);
		}
		
		return \Exception('User ID was not provided');
	}

	public function update($id){

		@extract($_POST);

		$entry = new \Model\Account();		

		if($id)
		{
			$entry->id = $id;
		}

		$entry->title = $title;
		$entry->slug = $slug;
		$entry->caption = $caption;
		$entry->damage = $damage;
		$entry->armor = $armor;
		$entry->agility = $agility;
		$entry->utility = $utility;
		$entry->speed = $speed;
		$entry->difficulty = $difficulty;

		$result = $entry->save();

		if($result){
			return redirect('/admin/accounts',[
				'code' => 'success',
				'message' => "Entry <strong>{$entry->title}</strong> has been updated"
			]);
		}

		return redirect('/admin/accounts',[
			'code' => 'danger',
			'message' => "Entry ID was not provided"
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$account_id = session('account_id');

			$entry = \Model\Account::row([
				'id' => $id
			]);

			if( $entry )
			{

				$title = $entry->login;
				$entry->delete();

				return redirect('/admin/accounts',[
					'code' => 'success',
					'message' => "Entry <strong>{$title}</strong> has been deleted"
				]);
			}
		}

		return redirect('/admin/accounts',[
			'code' => 'danger',
			'message' => "Entry was not found"
		]);
	}
}