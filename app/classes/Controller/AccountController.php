<?php namespace Controller;

class AccountController extends \Controller\BaseController {
	
	public function profile(){
		return \Bootie\App::view('profile',[
			'posts_count' => \Model\Post::count()
		]);
	}
}