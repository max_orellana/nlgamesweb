<?php namespace Controller;

class LiveController extends \Controller\BaseController {
	
	public function ping(){

		$json = [];
		$json['messages'] = [];
		$json['notification'] = [];

		$posts = \Controller\BlogController::find_by_tag('notification');

		foreach($posts as $row){

			$avatar = \Model\Account::select('column','forum_avatar',null,[
				'login' => $row->login
			]);

			$avatar = avatar($row->login,$avatar);			

			$json['notification']['data'][] = [
				'id' => $row->id,
				'sender' => $row->login,
				'url' => $row->external_link?:0,
				'avatar' => $avatar ?: site_asset('img/profile_default.png'),
				'login' => $row->login,
				'time' => timespan($row->created),
				'title' => $row->{'title_' . LOCALE},
				'caption' => words($row->{'caption_' . LOCALE},20),
			];
		}

		// notification (tickets, postulations)

		if(session('login')) {

			$tickets = [];

			// messages (inbox)

			$inbox = \Model\InboxRelation::fetch([
				'recipient' => session('login'),
				'read_time'	=> 0,
				'notified'	=> 0
			]);

			$unread = \Model\InboxRelation::count([
				'recipient' => session('login'),
				'read_time'	=> 0
			]);

			$json['messages']['title'] = implode(' ',array(locale('you_have'),count($inbox),locale('messages')));
			$json['read_all'] = locale('read_all');
			$json['unread'] = $unread;

			foreach($inbox as $row){

				$avatar = \Model\Account::select('column','forum_avatar',null,[
					'login' => $row->login
				]);

				$avatar = avatar($row->login,$avatar);
				$json['messages']['data'][] = [
					'id' => $row->id,
					'sender' => $row->login,
					'url' => '/account/inbox/' . $row->id,
					'avatar' => $avatar ?: site_asset('img/profile_default.png'),
					'time' => timespan($row->sent_time),
					'title' => $row->inbox->title
				];

				$row->notified = 1;
				$row->save();
			}

			if(session('group') == 'admin'){
				$tickets = \Model\Ticket::fetch([
					'ticket_id' => 0,
					'status_id' => 1
				],0,0,[
					'created' => "DESC"
				]);

				$postulations = \Model\PostulationRelation::fetch([
					'status' => 0
				],0,0,[
					'created' => "DESC"
				]);
			}
			else if(session('group') == 'user'){
				$data = \Model\Ticket::fetch([
					'user' => session('login'),
					'status_id' => 1
				]);

				foreach($data as $row){
					$last = \Model\Ticket::fetch([
						'ticket_id' => $row->id
					],1,0,[
						'id' => "DESC"
					]);

					if(isset($last[0]) AND $last[0]->group_id == 1){
						$tickets[] = $row;
					}
				}

				$postulations = \Model\PostulationRelation::fetch([
					'status <> 0 ',
					'login = \'' . session('login') . '\''
				],0,0,[
					'created' => "DESC"
				]);			
			}

			/*
			foreach($tickets as $row){
				$avatar = \Model\Account::select('column','forum_avatar',null,[
					'login' => $row->user
				]);

				$json['notification']['data'][] = [
					'id' => $row->id,
					'sender' => $row->user,
					'icon'	=> "ion-pricetags",
					'url' => '/' . session('group') . '/tickets/' . (session('group') == 'user' ? $row->code : $row->id),
					'avatar' => $avatar ?: site_asset('img/profile_default.png'),
					'time' => timespan($row->created),
					'title' => locale($row->tag->tag)
				];
			}

			foreach($postulations as $row){
				$avatar = \Model\Account::select('column','forum_avatar',null,[
					'login' => $row->login
				]);

				$json['notification']['data'][] = [
					'id' => $row->id,
					'sender' => $row->login,
					'icon'	=> "ion-ios-people",
					'url' => '/' . session('group') . '/postulations-relation/' . $row->id,
					'avatar' => $avatar ?: site_asset('img/profile_default.png'),
					'time' => timespan($row->created),
					'title' => locale($row->postulation->slug)
				];
			}
			*/

			// log activity

			$user = \Model\Online::row([
				'login' => session('login')
			]);

			if( ! $user){
				$user = new \Model\Online();
				$user->login = session('login');
			}

			$user->last_activity = TIME;	
			$user->save();		
		}

		return \Bootie\App::ajax($json);
	}

	public function panel(){
		return \Bootie\App::view('panel.live',null,null);
	}

	public function toggle(){

		@extract($_POST);

		$icons = explode(",",$icons);
		$rowstatus = explode(",",$rowstatus);
		$model = "\\Model\\$model";
		$value = ($value == 1 ? 0 : 1);
		$entry = new $model;
		$entry->id = $id;
		$entry->$field = $value;
		$entry->save();

		return \Bootie\App::ajax([
			'value'	=> $value,
			'icon'  => $icons[$value],
			'rowstatus'  => $rowstatus[$value]
		]);
	}
}