<?php namespace Controller;
 
class OrderController extends \Controller\BaseController {

  static $layout = "panel";

  protected $_currencyCode = 'ARS';

  /***
  * Sandbox : qwertyasdf0123456789
  * Live : 6133322N90N06U44
  *****/    
  

  private $_payuClave = '6133322N90N06U44';
  private $order = null;
  //protected $_payuClave = 'qwertyasdf0123456789';

  protected $producto_descripcion = "Donación en NLGames";

  private $payment_methods_avaliable = array(
    'payu' => "Tarjeta de Crédito / Débito",
  );

  public function getPaymentSuccess()
  {
    return \Bootie\App::view('payments.success')
      ->with('payment_data',$_SESSION['payment_data']);
  }

  public function getPaymentBank()
  {
    return \Bootie\App::view('payments.bank')
      ->with('payment_data',$_SESSION['payment_data']);
  }

  public function getPayuPaymentError()
  {
    return \Bootie\App::view('payments.payu-error')
      ->with('payment_data',$_SESSION['payment_data']); 
  }

  public function getPaymentError()
  {
    return \Bootie\App::view('payments.error')
      ->with('payment_data',$_SESSION['payment_data']);
  }

  public function getPaymentCancel()
  {
    return \Bootie\App::view('payments.cancel')
      ->with('payment_data',$_SESSION['payment_data']); 
  }
  
  private function coins_amount($coins){
    return sprintf('%0.2f',$coins * session('dc_conversion_value'));
  }

  public function checkout()
  {

    extract($_POST);


    if( empty($coins)) {
      return ['code' => 'warning','message' => locale('coins_value_empty')];
    }

    $order = new \Model\Order();

    $order->title = locale('donations_coins_acquirement');
    $order->login = session('login');
    $order->char_id = session('char_id');    
    $order->source = "coins";
    $order->coins = $coins;
    $order->amount = self::coins_amount($coins);
    $order->payment_method = $payment_method;
    $order->currency = "ARS";
    $order->created = TIME;
    $order->updated = TIME;
    $order->save(true);
  
    $order->reference = get_order_reference($order->id);
    $order->save();

    $this->order = $order;

    switch($order->payment_method)
    {
      case 'paypal':
        
        return $this->setPaypalExpressCheckout();

      case 'payu':
        
        return $this->setPayuWebCheckout();

    }

    return false;
  }

  public function setPaypalExpressCheckout()
  {

    // Search for the pending order for this user

    if( ! $this->order )
    {
      return \Bootie\App::view('payments.error');
    }

    $orderParams = array(
       'PAYMENTREQUEST_0_AMT' => $this->order->amount,
       'PAYMENTREQUEST_0_PAYMENTACTION' => "Sale",
       'PAYMENTREQUEST_0_CUSTOM' => $this->order->reference,
       'PAYMENTREQUEST_0_CURRENCYCODE' => $this->order->currency,
    );

    $item = array(
       'L_PAYMENTREQUEST_0_NAME0' => $this->order->title,
       'L_PAYMENTREQUEST_0_DESC0' => $this->order->comments,
       'L_PAYMENTREQUEST_0_AMT0' => $this->order->amount,
       'L_PAYMENTREQUEST_0_QTY0' => $this->order->quantity
    );

    $paypal = new \Service\Payment\Paypal();
    $response = $paypal->request('SetExpressCheckout',$paypal->_redirectURLs + $orderParams + $item);

    if(is_array($response) && $response['ACK'] == 'Success') { //Request successful
      $token = $response['TOKEN'];

      $this->order->token = $token;
      $this->order->save();

      return redirect( $paypal->_flowPoint  . '?cmd=_express-checkout&token=' . urlencode($token));
    } else {
      return \Bootie\App::view('payments.error', array(
        'response' => $response,
        'order' => $this->order
      ));
    }    
  }

  public function doPaypalExpressCheckoutPayment()
  {

    extract($_REQUEST);

    if( empty($token))
    {
      return \Bootie\App::view('payments.error'); 
    }

    // Token parameter exists

    // Search for the pending order for this user

    $order = \Model\Order::row([
      'token' => $token
    ]);

    if( ! $order )
    {
      return \Bootie\App::view('payments.error');
    }

    // Get checkout details, including buyer information.
    // We can save it for future reference or cross-check with the data we have
    $paypal = new \Service\Payment\Paypal();
    $checkoutDetails = $paypal->request('GetExpressCheckoutDetails', array('TOKEN' => $token));

    // Complete the checkout transaction
    $requestParams = array(
       'TOKEN' => $token,
       'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
       'PAYERID' => $PayerID,
       'PAYMENTREQUEST_0_AMT' => $order->amount, // Same amount as in the original request
       'PAYMENTREQUEST_0_CURRENCYCODE' => $order->currency // Same currency as the original request
    );

    $response = $paypal->request('DoExpressCheckoutPayment',$requestParams);

    if( is_array($response) && in_array($response['ACK'],['Success','SuccessWithWarning'])) { // Payment successful
      // We'll fetch the transaction ID for internal bookkeeping
      $transactionId = $response['PAYMENTINFO_0_TRANSACTIONID'];

      /*
      |--------------------------------------------------------------------------
      | Update order status
      |--------------------------------------------------------------------------
      |
      */

      $order->status = 'approved';
      $order->save();

      /*
      |--------------------------------------------------------------------------
      | Payment store 
      |--------------------------------------------------------------------------
      |
      */

      $payment = new \Model\Payment();
      $payment->order_id = $order->id;
      $payment->Paypal_Transaction_id = $transactionId;
      $payment->Paypal_Payer_id = $PayerID;
      $payment->amount = $order->amount;
      $payment->source = 'paypal';
      $payment->complete = 1;
      $payment->save();

      $recipient = \Model\Game\Account::find( $order->login );

      /* Paquete o Credito */
      OrderController::complete_order($order->id);

      if( ! $recipient )
      {
        return \Bootie\App::view('errors.custom',[
          'title' => "Usuario no encontrado",
          'description' => "Lo lamento, el usuario al cual hace referencia el pedido no ha podido ser hallado."
        ]);
      }

      $data = array(
        'recipient' => $recipient,
        'order' => $order,
        'methods_avaliable' => $this->payment_methods_avaliable
      );

      // Envio de email al usuario.
      /*
    if($_SERVER['REMOTE_ADDR'] != "127.0.0.1"):

      // notification to user
      \Bootie\Mail\Mailer::send($recipient->email,'[NLGames.net] Información de pago','emails.nuevo-pago',[
        'recipient' => $recipient,
        'order'  => $order
      ]);

      // notification to admin 
      \Bootie\Mail\Mailer::send('notifications@nlgames.net','[NLGames.net] Información de pago','emails.nuevo-pago',[
        'recipient' => $recipient,
        'order'  => $order
      ]);

    endif;
    */

      return \Bootie\App::view('payments.success',[
        'response' => $response
      ]);
    }

    return \Bootie\App::view('payments.error');
  }


  public function setPayuWebCheckout()
  {

    // Search for the pending order for this user

    if( ! $this->order )
    {
      return \Bootie\App::view('payments.error');
    }

    if( ! $this->order ) return \Bootie\App::view('payments.error');
    if( ! $this->order->id || $this->order->amount < 1) return \Bootie\App::view('errors.missing');

    try 
    {
        $payu = new \Service\Payment\Payu();

        $payu->set_entorno('real');
        $payu->amount($this->order->amount);
        //$payu->amount(3);
        $payu->currency($this->order->currency);
        $payu->pedido($this->order->reference);
        $payu->producto_descripcion($this->order->title);
        $payu->clave($this->_payuClave);
        $payu->firma();
        $payu->set_nameform('form_payu');
        $formulario = $payu->create_form();

        echo $formulario;

        $payu->ejecutarRedireccion(); 

    }
    catch(Exception $e)
    {
        die($e->getMessage());   
    }    
  }

  public function getPayuConfirmation()
  {

    extract($_POST);

    $payu = new \Service\Payment\Payu;

    $ApiKey = $payu->_apikey;

    // signature validation

    if(substr($value,-2) == '00') $value = number_format($value, 1);
    $signature = md5(implode('~',[$ApiKey,$merchant_id,$reference_sale,$value,$currency,$state_pol]));

    if(strtoupper($sign) == strtoupper($signature)){

      $order = \Model\Order::row([
        'reference' => $reference_sale
      ]);

      if( ! empty($order)) {

        $order->status = $state_pol == 4 ? 'approved' : 'rejected';
        $order->updated = TIME;
        $order->save();

        $payment = new \Model\Payment();
        $payment->order_id = $order->id;
        $payment->amount = $value;
        $payment->transactionId = $transaction_id;
        $payment->polResponseCode = $state_pol;
        $payment->dump = json_encode($_POST);
        $payment->created = TIME;
        $payment->save(true);

        if($state_pol == 4){
          self::complete_order($order->id);
        }
      }
    }
  }

  public function getPayuResponse()
  {
    
    $payu = new \Service\Payment\Payu;
    $ApiKey = $payu->_apikey;

    $merchant_id = $_REQUEST['merchantId'];
    $referenceCode = $_REQUEST['referenceCode'];
    $TX_VALUE = $_REQUEST['TX_VALUE'];
    $New_value = number_format($TX_VALUE, 1, '.', '');
    $currency = $_REQUEST['currency'];
    $transactionState = $_REQUEST['transactionState'];
    $firma_cadena = "$ApiKey~$merchant_id~$referenceCode~$New_value~$currency~$transactionState";
    $firmacreada = md5($firma_cadena);
    $firma = $_REQUEST['signature'];
    $reference_pol = $_REQUEST['reference_pol'];
    $cus = $_REQUEST['cus'];
    $extra1 = $_REQUEST['description'];
    $pseBank = $_REQUEST['pseBank'];
    $lapPaymentMethod = $_REQUEST['lapPaymentMethod'];
    $transactionId = $_REQUEST['transactionId'];

    if ($_REQUEST['transactionState'] == 4 ) {
      $estadoTx = "Transacción aprobada";
    }

    else if ($_REQUEST['transactionState'] == 6 ) {
      $estadoTx = "Transacción rechazada";
    }

    else if ($_REQUEST['transactionState'] == 104 ) {
      $estadoTx = "Error";
    }

    else if ($_REQUEST['transactionState'] == 7 ) {
      $estadoTx = "Transacción pendiente";
    }

    else {
      $estadoTx=$_REQUEST['mensaje'];
    }

    if (strtoupper($firma) == strtoupper($firmacreada)) {

      $order_id = get_order_id($referenceCode);
      $order = \Model\Order::find($order_id);

      if( empty($order)) {
        return \Bootie\App::view('payments.error',[
          'error' => locale('order_not_found')
        ]);
      }

      if($order->status == 'pending'){
        return  redirect('/joinbeta',[
          'code' => 'success',
          'message' => locale("dc_acquirement_pending_text")
        ]);        
      }

      if ($order->status == 'approved' AND $_REQUEST['transactionState'] == 4 ) {

        if( isset($_SESSION['coins'])){
          unset($_SESSION['coins']);
        }

        $_SESSION['coins'] = \Controller\User\PanelController::get_coins(session('char_id'));

        return  redirect('/joinbeta',[
          'code' => 'success',
          'message' => locale("dc_acquirement_success_text")
        ]);
      }


      return  redirect('/joinbeta',[
        'code' => 'error',
        'message' => locale("dc_acquirement_error_text")
      ]);

      /*
      return \Bootie\App::view('payments.result',[
        'estadoTx' => $estadoTx,
        'transactionId' => $transactionId,
        'reference_pol' => $reference_pol,
        'referenceCode' => $referenceCode,
        'pseBank' => $pseBank,
        'cus' => $cus,
        'TX_VALUE' => $TX_VALUE,
        'currency' => $currency,
        'extra1' => $extra1,
        'lapPaymentMethod' => $lapPaymentMethod
      ]);*/


    }
    else
    {
      return  redirect('/joinbeta',[
        'code' => 'error',
        'message' => locale("dc_acquirement_error_text")
      ]);
      /*      
      return \Bootie\App::view('payments.error',[
        'error' => 'Error validando firma digital'
      ]);
      */
    }
  }

  public function complete_order($order_id){

    $order = \Model\Order::find($order_id);

    if( ! $order) {
        return \Bootie\App::view('errors.custom',[
          'title' => locale('order_not_found'),
          'caption' => locale('order_not_found_caption'),
          'text' => locale('order_not_found_text')
        ]);
    }

    switch( $order->source ){
      case "coins":
        
        $coins = \Controller\User\PanelController::set_coins($order->coins,$order->char_id);
        message($order->login,'dc_acquirement_success_title','dc_acquirement_success_text','system',null,['coins' => $coins,'login' => $order->login]);      

        break;

      case "package":

        break;
    }

    return false;
  }

  public function advertisers_goals(){

    

  }
}