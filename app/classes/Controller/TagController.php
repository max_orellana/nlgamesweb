<?php namespace Controller;

class TagController extends \Controller\BaseController {
	
	static $layout = "admin";

	public function tags($type,$id){

		$tags_ids = $included = $excluded = [];

		$tags_ids = \Model\TagRelation::select('fetch','tag_id',null,[
			$type . '_id' => $id
		]);

		if( ! count($tags_ids)){
			$tags_ids = [0];
		}
		
		$included = \Model\Tag::select('fetch','tag',null,[
			'id IN(' . implode(',',$tags_ids) . ')',
			'type' => $type
		]);

		$excluded = \Model\Tag::select('fetch','tag',null,[
			'id NOT IN(' . implode(',',$tags_ids) . ')',
			'type' => $type
		]);	
			
		return [
			'included' => $included,
			'excluded' => $excluded
		];
	}

	public function add_relation($type,$id){

		extract($_POST);

		$included = [];

		if(isset($tags)){

			foreach( $tags as $tag ){

				$tag_id = \Model\Tag::select('column','id',null,[
					'tag' => $tag
				]);

				if( ! $tag_id ){
					$tag2 = new \Model\Tag();
					$tag2->tag = $tag;
					$tag2->type = $type;
					$tag2->save();

					$tag_id = $tag2->id;
				}

				$type_id = $type . '_id';
				$post_tag = new \Model\TagRelation();
				$post_tag->tag_id = $tag_id;
				$post_tag->{$type_id} = $id;
				$post_tag->save();

				$included[] = $tag;
			}
		}

		return [
			'included' => $included
		];
	}

	public function remove_relation($type,$id){

		extract($_POST);
		$excluded = [];

		if(isset($tags)){
			
			foreach( $tags as $tag ){

				$tag_id = \Model\Tag::select('column','id',null,[
					'tag' => $tag
				]);

				$entry = \Model\TagRelation::row([
					'tag_id' => $tag_id,
					'post_id' => $id
				]);

				if($entry) $entry->delete();

				$excluded[] = $tag;
			}
		}

		return [
			'excluded' => $excluded
		];
	}	
}