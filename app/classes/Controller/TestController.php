<?php namespace Controller;

class TestController extends \Controller\BaseController {
	
	static $layout = "default";

	public function test_account(){
		$gid = 30;

		$group = \Controller\AuthController::find_group_accesslvl($gid);

		$account = \Model\Account::find(session('login'));
		$account->group_id = $group->id;
		$account->save();
	}


	public function test_account2(){
		$account = new \Model\Account;
		$account->asdaslogin = "asdads";
		$account->save(true);		
	}
	
	public function fix_advertiser_capture(){
		$captures = \Model\Account::fetch(["advertise_token <> ''"]);

		foreach($captures as $account){
			$advertiser = \Model\Account::row(['token' => $account->advertise_token]);

			if($advertiser){
				$entry = new \Model\AdvertiserCapture();
				$entry->advertiser = $advertiser->login;
				$entry->login = $account->login;
				$entry->created = $account->created;
				$entry->save(true);
			}
		}
	}


	public function fix_account_codes(){

		$accounts = \Model\Account::fetch(["token = ''"]);

		foreach($accounts as $account){
			$token = token();

			while(\Model\Account::row(['token' => $token])){
				$token = token();
			}

			$account->token = $token;
			$account->save();
		}
	}

	public function modal1(){
		return \Bootie\App::view("shared.error",[
			'error' => '<a class="btn btn-info" href="/testmodal2">Test modal</a>'
		]);		
	}
	public function testheroes(){
		$rows =  \Model\Heroe::fetch();
		echo "<pre>";
		foreach($rows as $row){
			echo '

			/* '  . $row->slug . ' */
#hero .hero-wrapper .hero-body-wrapper #hero-details .detail-row .passive-wrapper .passive.hero_'  . $row->slug . '.p-1 {
    background-image: url("../img/heroes/'  . $row->slug . '/p1.jpg")
}
#hero .hero-wrapper .hero-body-wrapper #hero-details .detail-row .ability-wrapper .ability.hero_'  . $row->slug . '.key-1 {
    background-image: url("../img/heroes/'  . $row->slug . '/f1.jpg")
}
#hero .hero-wrapper .hero-body-wrapper #hero-details .detail-row .ability-wrapper .ability.hero_'  . $row->slug . '.key-2 {
    background-image: url("../img/heroes/'  . $row->slug . '/f2.jpg")
}
#hero .hero-wrapper .hero-body-wrapper #hero-details .detail-row .ability-wrapper .ability.hero_'  . $row->slug . '.key-3 {
    background-image: url("../img/heroes/'  . $row->slug . '/f3.jpg")
}
#hero .hero-wrapper .hero-body-wrapper #hero-details .detail-row .ability-wrapper .ability.hero_'  . $row->slug . '.key-4 {
    background-image: url("../img/heroes/'  . $row->slug . '/f4.jpg")
}
#hero .hero-wrapper .hero-body-wrapper #hero-details .detail-row .ability-wrapper .ability.hero_'  . $row->slug . '.key-5 {
    background-image: url("../img/heroes/'  . $row->slug . '/f5.jpg")
}
#hero .hero-wrapper .hero-body-wrapper #hero-details .detail-row .ability-wrapper .ability.hero_'  . $row->slug . '.key-6 {
    background-image: url("../img/heroes/'  . $row->slug . '/f6.jpg")
}
#hero .hero-wrapper .hero-body-wrapper #hero-details .detail-row .ability-wrapper .ability.hero_'  . $row->slug . '.key-7 {
    background-image: url("../img/heroes/'  . $row->slug . '/f7.jpg")
}
#hero .hero-wrapper .hero-body-wrapper #hero-details .detail-row .ability-wrapper .ability.hero_'  . $row->slug . '.key-8 {
    background-image: url("../img/heroes/'  . $row->slug . '/f8.jpg")
}


';

		}
	}

	public function testcoin(){
		$item = \Model\Game\Item::row(['owner_id' => session('char_id'), 'item_id' => config()->game_dc_item_id]);
		if(empty($item)){
			$item = new \Model\Game\Item;
			$item->owner_id = session('char_id');
			$item->item_id = config()->game_dc_item_id;
			$item->count = 0;     
			$item->enchant_level = 0;
			$item->loc = 'INVENTORY';
			$item->loc_data = 0;
			$item->save(true);
		}

		$item->count=101;
		$item->save();

	}

	public function testgame(){
		$account = \Model\Game\Account::row(['login' => 'checho']);
		$account->login = "checho2";
		$account->save();

		echo "<pre>";
		var_dump($account);
	}


	public function mailtemplate(){
		return \Bootie\App::view("emails.base",[
			'title' => locale('modal_alert_beta_title'),
			'message' => locale('modal_alert_beta_text'),
		],null,'full');		
	}

	public function testnotify(){
		message('developer','modal_alert_beta_title','modal_alert_beta_text');
	}

	public function modal2(){
		// return redirect('#register');
		return modal("modal",['title' => "Ttitle test", 'message' => "Message test ", 'continueto' => "/"]);
	}

	public function forum(){
		// return redirect('#register');
		$posts = \Model\Forum\Post::fetch();
		echo "<pre>";
		foreach($posts as $post){
			echo $post->post;
			echo "\n";
			echo "* escrito por: " . $post->author_name;
			echo "\n---fin del post ---";
			echo "\n";
		}

		dd();

	}


	public function databasetest(){

		echo "<pre>";
		$row = \Model\Forum\Account::row(['member_id' => 1]);
		var_dump($row);
		$row2 = \Model\Account::row(['id' => 5]);
		var_dump($row2);

		dd();
	}

	public function words(){

		include SP . '/app/locales/es.php';
		include SP . '/app/locales/en.php';

		foreach($es as $key => $value){
			$word = new \Model\Word;
			$word->word_key = $key;
			$word->word_es = $es[$key];
			$word->word_en = $en[$key];

			$word->save();
		}

		dd("ok");

	}

	public function mailtest(){

		$sent = \Bootie\Mail\Mailer::send(
			get('recipient',"telemagico@gmail.com"),
			get('subject',"-- Mail Test --"),
			[
				'title'	=> get('title',"-- Title Here --"),
				'username'	=> get('username',"-- Username Here --")
			],get('view',"emails.welcome")
		);

		if( $sent ) {
			$title = "<i class='ion-checkmark-circled'></i> Enviado!";
			$caption = "La plantilla de email ha sido enviada con éxito";
		} else {
			$title = "<i class='ion-close-circled'></i> Error!";
			$caption = "La plantilla de email no ha sido enviada con éxito";
		}

		return \Bootie\App::view("system.alert",[
			'title' => $title,
			'caption' => $caption,
			'content' => ""
		],'plain');
	}

	public function forumlogintest(){

		@extract($_REQUEST);

		$user = \Model\Forum\Account::row(['name' => $username]);

		if($user){

			$user = $user[0];

			$hash = md5( md5( $user->members_pass_salt ) . md5($password) );

			if( $hash === $user->members_pass_hash){
				return \Bootie\App::ajax(['Usuario OK!']);
			}
				
			return \Bootie\App::ajax(['Usuario OK pero clave erronea']);
		}

		return \Bootie\App::ajax(['No existe el usuario']);
	}

	public function gameservertest(){
		$db = \Bootie\App::load_database('game');
		$result = $db->fetch('select * from accounts limit 1');

		return print \Bootie\App::ajax($result);
	}
}