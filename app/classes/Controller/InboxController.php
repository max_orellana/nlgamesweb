<?php namespace Controller;

class InboxController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){
		/*return \Bootie\App::view('panel.inbox.index',[
			'entries'	=> \Model\InboxRelation::paginate([
				'sent_time' => "DESC"
			],[
				'recipient' => session('login')
			],10)
		]);*/

		return \Bootie\App::view('panel.inbox.index',[
			'entries'	=> \Model\InboxRelation::fetch([
				'recipient' => session('login')
			],0,0,[
				'sent_time' => "DESC"	
			])
		]);

	}

	public function sent(){
		return \Bootie\App::view('panel.inbox.sent',[
			'entries'	=> \Model\InboxRelation::paginate([
				'sent_time' => "DESC"
			],[
				'login' => session('login')
			],10)
		]);
	}

	public function compose(){
		extract($_GET);
		
		$recipients = [];

		if( ! empty($r)){
			$recipients = explode(",",$r);
		}

		return \Bootie\App::view('panel.inbox.compose',[
			'recipients' => $recipients
		]);
	}

	public function send(){

		@extract($_POST);

		// mail data

		$recipients = explode(",",$recipients);

		foreach($recipients as $recipient){
			message($recipient, $title, $content, session('login'));
		}

		return redirect('/account/inbox',[
			'code' => 'success',
			'message' => locale('mail_sent')
		]);
	}

	public function fetch($id){

		if(is_numeric($id))
		{
			$entry = \Model\InboxRelation::find($id);

			if($entry){
				$entry->read_time = TIME;
				$entry->save();

				return \Bootie\App::ajax([
					'title' => $entry->inbox->title,
					'content' => $entry->inbox->content,
					'sent' => timespan($entry->sent_time),
					'login' => $entry->login
				]);
			}
		}
		
		return [
			'code' => 'error',
			'position'	=> 'modal',
			'title' => locale('modal_alert_error'),
			'message' => locale('modal_alert_error_text')
		];
	}

	public function read($id){

		if(is_numeric($id))
		{
			$entry = \Model\InboxRelation::row([
				'id' => $id
			]);

			if($entry){

				$entry->read_time = TIME;
				$entry->save();
				
				return \Bootie\App::view('panel.inbox.view',[
					'entry'	=> $entry
				]);
			}
		}
		
		return \Bootie\App::view('shared.error',[
			'error'	=> locale('entry_not_found')
		]);
	}

	public function delete($id){

		if(is_numeric($id))
		{
			$user_id = session('user_id');

			$entry = \Model\Inbox::row(['id' => $id]);

			if( $entry )
			{
				$entry->delete();
			}

			$relation = \Model\InboxRelation::row(['inbox_id' => $id]);

			if( $relation )
			{
				$relation->delete();
			}

			return [
				'code' => 'success',
				'message' => ""
			];
		}

		return [
			'code' => 'error',
			'message' => locale('operation_error')
		];
	}

	public function recipients(){

		@extract($_REQUEST);

		$json = [];
		$users = \Model\Game\Account::select('fetch','login',null,[
			"login like '%{$recipients_input}%'"
		]);

		if($users) {
			foreach($users as $key => $login){

				$avatar = \Model\Account::select('column','forum_avatar',null,[
					'login' => $login
				]);

				$json[] = [
					'title' => $login,
					'avatar' => $avatar ?: site_asset('img/profile_default.png'),
				];
			}
		}

		return \Bootie\App::ajax($json);
	}
}