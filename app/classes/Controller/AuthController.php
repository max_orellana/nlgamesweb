<?php namespace Controller;

class AuthController extends \Controller\BaseController {
	
	static $groups = [];

	public function login(){

		@extract($_REQUEST);

		$json = [
			'code' => 'warning',
			'message' => locale('invalid_credentials')
		];

		$email = strtolower($email);
		$password = \Controller\AuthController::l2_password_encrypt($password);

		$user = \Model\Game\Account::row([
			"login = '" .  $email . "' AND password = '" . $password . "' OR email = '" .  $email . "' AND password = '" . $password . "'"
		]);

		if($user){

			if( ! empty($rememberme) AND $rememberme == "forever"){
				\Bootie\Cookie::set('nl_remember',1);
				\Bootie\Cookie::set('nl_user',$email);
				\Bootie\Cookie::set('nl_pass',$password);
			}

			if($user->group = \Controller\AuthController::find_group_accesslvl($user->accessLevel)){

				$account = \Model\Account::find($user->login);

				// create local account entry if needed
				if( empty($account) ){
					$account = new \Model\Account;
					$account->login = $user->login;
					$account->email = $user->email;
					$account->language = LOCALE;
					$account->save(true);
				}

				// copy char id if needed
				if( empty($account->char_id) ){
					$character = \Model\Game\Character::row(['account_name' => $user->login]);
					// create char entry if needed					
					if(empty($character)){
						$character = new \Model\Game\Character;
						$character->account_name = $account->login;
						$character->char_name = $account->login;
						$character->save(1);
					}

					$account->char_id = $character->charId;
					$account->char_name = $character->char_name;
				}

				$account->last_login = TIME;
				$account->save();

				$item = \Model\Game\Item::row(['owner_id' => $account->char_id, 'item_id' => config()->game_dc_item_id]);

				if(!empty($item->count))	$account->coins = $item->count;

				$user->account = $account;

				$json['code'] = 'success';
				$json['message'] = locale("redirecting");

				if( empty($redirect) AND ! empty($_SESSION['redirect'])){
					$redirect = $_SESSION['redirect'];
					unset($_SESSION['redirect']);
				}

				if( empty($redirect)) $redirect = '/account/index';

				$json['redirect'] = $redirect;

				\Controller\AuthController::authenticate($user);
			}
		}

		return $json;
	}

	public function register(){

		if(!isset($_POST['g-recaptcha-response'])){
			return ['code' => 'danger','message' => locale('invalid_captcha')];
		}

		$recaptcha = new \ReCaptcha\ReCaptcha(config()->gcaptchasecret);
		$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

	    if ( ! $resp->isSuccess()){
			return ['code' => 'danger', 'message' => locale('captcha_not_valid')];
	    }

		extract($_POST);

		$login = $username;
		$email = strtolower($email);

		if( ! filter_var($email, FILTER_VALIDATE_EMAIL)){
			return ['code' => 'warning','message' => locale('invalid_email')];
		}

		if(preg_match('/[^a-z_\-0-9]/i', $username)){
		  return ['code' => 'warning','message' => locale('symbols_not_allowed') . ' : ' . $username];
		}

		if(strlen($username) < 6 OR strlen($username) > 16){
		  return ['code' => 'warning','message' => locale('char_limit_6_16') . ' : ' . $username];
		}

		if(preg_match('/[^a-z_\-0-9]/i', $character)){
		  return ['code' => 'warning','message' => locale('symbols_not_allowed') . ' : ' . $character];
		}

		if(strlen($character) < 2 OR strlen($character) > 16){
		  return ['code' => 'warning','message' => locale('char_limit_2_16') . ' : ' . $character];
		}

		if($password !== $password2){
		  return ['code' => 'warning','message' => locale('passwords_do_not_match')];
		}

		try {
			$exists = \Model\Game\Account::row([
				"login = '" .  $email . "' OR email = '" .  $email . "'"
			]);
		}
		catch(\Exception $e){
			return [
				'code' => 'danger',
				'title' => locale('error_game_server_connection'),
				'message' => locale('error_game_server_connection_text')
			];		
		}

		if( $exists ) {
			return ['code' => 'warning','message' => locale('email_exists') . ' : ' . $email];
		}

		$exists = \Model\Game\Character::row(["char_name = '" .  $character . "'"]);

		if( $exists ) {
			return ['code' => 'warning','message' => locale('character_exists') . ' : ' . $character];
		}

		$json = [];

		$accesslevel = -1;
		$accountlevel = 0;


		// game account
		$user = new \Model\Game\Account();

		var_dump($user);

		$user->login = $login;
		$user->accessLevel = $accesslevel;
		$user->password = \Controller\AuthController::l2_password_encrypt($password);
		$user->email = $email;
		$user->lastServer = 0;
		$user->lastIP = 0;
		$user->pcIp = 0;
		$user->hop1 = 0;
		$user->hop2 = 0;
		$user->hop3 = 0;
		$user->hop4 = 0;

		$user->save(true);

		$user = \Model\Game\Account::find($login);

		// character
		$char = new \Model\Game\Character();
		$char->account_name = $login;
		$char->char_name = $character;
		$char->level = 1;
		$char->maxHp = 138;
		$char->curHp = 138;
		$char->maxCp = 55;
		$char->curCp = 55;
		$char->maxMp = 38;
		$char->curMp = 38;
		$char->face = 0;
		$char->hairStyle = 2;
		$char->hairColor = 2;
		$char->sex = $gender;
		$char->heading = 17722;
		$char->x = -114370;
		$char->y = 259988;
		$char->z = -1200;
		$char->karma = 0;
		$char->pvpkills = 0;
		$char->pkkills = 0;
		$char->clanid = 0;
		$char->race = 0;
		$char->classid = 0;
		$char->cancraft = 0;
		$char->title = '';
		$char->accesslevel = $accesslevel;
		$char->online = 0;
		$char->onlinetime = 0;
		$char->power_grade = 0;
		$char->pccafe_points = 0;
		$char->vitality_points = 0;
		$char->language = LOCALE;
		$char->save(true);

		if($user->group = \Controller\AuthController::find_group_accesslvl($user->accessLevel)){

			$account = \Model\Account::find($user->login);

			if( empty($account) ){
				$account = new \Model\Account;
				$account->login = $login;
				$account->email = $email;
				$account->joinbeta = !empty($wannajoinbeta)?1:0;
				$account->char_id = $char->charId;
				$account->char_name = $char->char_name;
				$account->heroe = 0;
				$account->advertise_token = session('advertise_token',"");
				$account->language = LOCALE;
				$account->created = TIME;
				$account->last_login = TIME;
				$account->created = TIME;
				$account->save(true);
			}

			$account = \Model\Account::find($user->login);
			$token = token();

			while(\Model\Account::row(['token' => $token])){
				$token = token();
			}

			$account->token = $token;
			$account->save();

			if(session('advertise_token')){

				$advertiser = \Model\Account::row(['token' => session('advertise_token')]);

				if($advertiser){
					$capture = new \Model\AdvertiserCapture();
					$capture->advertiser = $advertiser->login;
					$capture->login = $login;
					$capture->created = TIME;
					$capture->save(true);
				}
			}

			$user->account = $account;

			\Controller\AuthController::authenticate($user);

			$json['success'] = locale('redirecting');
			$json['message'] = locale('redirecting');

			if(!empty($wannajoinbeta) AND $wannajoinbeta){

				message(session('login'),'modal_alert_beta_title','modal_alert_beta_text');

				return [
					'code' => 'success',
					'position'	=> 'modal',
					'title' => locale('modal_alert_beta_title'),
					'message' => locale('modal_alert_beta_text'),
					'continue' => '/account/index'
				];

			} else {

				message(session('login'),'modal_alert_no_beta_title','modal_alert_no_beta_text');
				
				return [
					'code' => 'success',
					'position'	=> 'modal',
					'title' => locale('modal_alert_no_beta_title'),
					'message' => locale('modal_alert_no_beta_text'),
					'continue' => '/account/index'
				];

				//$json['modal'] = "welcome";
				//$json['redirect'] = "/user";
			}
		}

		return $json;
	}


	public function find_group_accesslvl($level){
		$groups = \Model\Game\Group::select('fetch','id, slug, rule');
		if(is_numeric($level)){
			foreach($groups as $group){
				if(eval("return $group->rule;")){
					return $group;
				}	
			}
		}
		return false;
	}

	
	public function add_coins($login,$coins){

		$account = \Model\Game\Account::find($login);


		echo "<pre>";
		if(!empty($account)){
			print_r($account);
		}

		$char = \Model\Game\Character::row(['account_name' => $login]);


		if(!empty($char)){
			print_r($char);
		}

		$coins =  \Controller\User\PanelController::set_coins($coins,$char->charId);

		dd($coins);

	}

	public function account($login){

		$account = \Model\Game\Account::find($login);


		echo "<pre>";
		if(!empty($account)){
			print_r($account);
		}

		$char = \Model\Game\Character::row(['account_name' => $login]);


		if(!empty($char)){
			print_r($char);
		}

		$coins = \Controller\User\PanelController::get_coins($char->charId);

		echo "\ncoins:\n";
		echo $coins;
	}

	public function autologin($email,$password){

		$user = \Model\Game\Account::row([
			"login = '" .  $email . "' AND password = '" . $password . "' OR email = '" .  $email . "' AND password = '" . $password . "'"
		]);

		if($user){
			if($user->group = \Controller\AuthController::find_group_accesslvl($user->accessLevel)){

				$account = \Model\Account::find($user->login);

				if( empty($account) ){
					$account = new \Model\Account;
					$account->login = $user->login;
					$account->email = $user->email;
					$account->language = LOCALE;
					//$account->last_activity = TIME;
					$account->save(true);
				}

				$user->account = $account;
				
				\Controller\AuthController::authenticate($user);
			}
		}
	}

	private function authenticate($data){

		$currency = \Model\CoinCurrency::row(['currency' => 'ARS']);
		
		session_put('login',$data->login);
		session_put('group',$data->group->slug);
		session_put('email',$data->email);
		session_put('token',$data->account->token);
		session_put('char_name',$data->account->char_name);
		session_put('char_id',$data->account->char_id);
		session_put('coins', ! empty($data->account->coins) ? $data->account->coins:0);
		session_put('lastserver', $data->lastServer);
		session_put('dc_currency', $currency->currency);
		session_put('dc_conversion_value', $currency->value);
		session_put('dc_conversion_discount', $currency->discount);
		session_put('lastip', $data->lastIP);
		session_put('pcip',$data->pcIp);
		session_put('hop1',$data->hop1);
		session_put('hop2',$data->hop2);
		session_put('hop3',$data->hop3);
		session_put('hop4',$data->hop4);

		if(! empty($data->account->forum_name)) session_put('forum_name',$data->account->forum_name);
		if(! empty($data->account->forum_avatar)) session_put('forum_avatar',$data->account->forum_avatar);
		if(! empty($data->account->forum_posts)) session_put('forum_posts',$data->account->forum_posts);

		return true;
	}

	public function logout(){

		foreach($_SESSION as $k=>$v){
			unset($_SESSION[$k]);
		}

		if( ! empty($_SERVER['REQUEST_URI']) AND $_SERVER['REQUEST_URI'] !== '/logout')
			$_SESSION['redirect'] = $_SERVER['REQUEST_URI'];

		// if set, stop rememering user!
		if( ! empty(\Bootie\Cookie::get('nl_pass'))) {
			\Bootie\Cookie::set('nl_remember',"");
			\Bootie\Cookie::set('nl_user',"");
			\Bootie\Cookie::set('nl_pass',"");
		}		

		return redirect("/");
	}

	public function login_check(){

		@extract($_GET);

		if($login AND ! \Model\Game\Account::row(['login' => $login]) AND strlen($login) >= 5 AND strlen($login) <= 16){
			return [
				'ion'	=> 'ion-checkmark-circled',
				'status'	=> 'success',
				'message'	=> locale('available')
			];
		}
		return [
			'ion'	=> 'ion-close-circled',
			'status'	=> 'danger',
			'message'	=> locale('unavailable')
		];
	}

	public function username_check(){

		@extract($_GET);

		if(strlen($username) < 6 OR strlen($username) > 16){
			return [
				'ion'	=> 'ion-close-circled',
				'status'	=> 'danger',
				'message'	=> locale('char_limit_6_16')
			];
		}

		if(preg_match('/[^a-z_\-0-9]/i', $username)){
			return [
				'ion'	=> 'ion-close-circled',
				'status'	=> 'danger',
				'message'	=> locale('symbols_not_allowed')
			];
		}

		if(\Model\Game\Account::row(['login' => $username])){
			return [
				'ion'	=> 'ion-close-circled',
				'status'	=> 'danger',
				'message'	=> locale('unavailable')
			];
		}

		return [
			'ion'	=> 'ion-checkmark-circled',
			'status'	=> 'success',
			'message'	=> locale('available')
		];
	}

	public function character_check(){

		@extract($_GET);

		if(strlen($character) < 2 OR strlen($character) > 16){
			return [
				'ion'	=> 'ion-close-circled',
				'status'	=> 'danger',
				'message'	=> locale('char_limit_2_16')
			];
		}

		if(preg_match('/[^a-z_\-0-9]/i', $character)){
			return [
				'ion'	=> 'ion-close-circled',
				'status'	=> 'danger',
				'message'	=> locale('symbols_not_allowed')
			];
		}

		if(\Model\Game\Character::row(['char_name' => $character])){
			return [
				'ion'	=> 'ion-close-circled',
				'status'	=> 'danger',
				'message'	=> locale('unavailable')
			];
		}

		return [
			'ion'	=> 'ion-checkmark-circled',
			'status'	=> 'success',
			'message'	=> locale('available')
		];
	}

	public function email_check(){
		@extract($_GET);

		if( ! filter_var($email, FILTER_VALIDATE_EMAIL)){
			return [
				'ion'	=> 'ion-close-circled',
				'status'	=> 'danger',
				'message'	=> locale('invalid_email')
			];
		}

		if(\Model\Game\Account::row(['email' => $email])){

			return [
				'ion'	=> 'ion-close-circled',
				'status'	=> 'danger',
				'message'	=> locale('unavailable')
			];
		}

		return [
			'ion'	=> 'ion-checkmark-circled',
			'status'	=> 'success',
			'message'	=> locale('available')
		];
	}

	public function updatepasswordcode(){
		extract($_REQUEST);

		if(! $skiprequestpassword AND empty($password) ){
			return [
				'code' => 'danger',
				'message' => locale('input_password')
			];				
		}

		if(empty($password1) OR empty($password2) OR $password1 != $password2) {
			return [
				'code' => 'danger',
				'message' => locale('passwords_must_match')
			];				
		}

		// update mail code, set expired
		$mailcode = \Model\MailCode::row(['code' => $code, 'login' => session('login')]);

		if($mailcode) {
			$mailcode->expired = 1;
			$mailcode->save();
		}

		// proceed to update password

		$newpassword = \Controller\AuthController::l2_password_encrypt($password1);

		$user = \Model\Game\Account::find(session('login'));
		$user->password = $newpassword;
		$user->save();

		return [
			'code' => 'success',
			'message' => locale('password_updated')
		];				
	}


	public function updatepassword(){
		extract($_REQUEST);

		if(empty($password1) OR empty($password2) OR $password1 != $password2) {
			return [
				'code' => 'danger',
				'message' => locale('passwords_must_match')
			];				
		}

		$email = session('login');
		$password = \Controller\AuthController::l2_password_encrypt($password);

		$user = \Model\Game\Account::row([
			"login = '" .  $email . "' AND password = '" . $password . "'"
		]);

		if(!$user){
			return [
				'code' => 'danger',
				'message' => locale('wrong_password')
			];				
		}

		// proceed to update password

		$newpassword = \Controller\AuthController::l2_password_encrypt($password1);
		$user->password = $newpassword;
		$user->save();

		return [
			'code' => 'success',
			'message' => locale('password_updated')
		];				
	}

	public function recoverpassword(){

		extract($_REQUEST);

		if(! empty($code)) {

			$mailcode = \Model\MailCode::row([
				"login = '" .  $email . "' AND code = '" . $code . "' AND expired = 0" 
			]);

			if($mailcode){

				// autologin

				$user = \Model\Game\Account::row([
					"login = '" .  $email . "' OR email = '" .  $email . "'"
				]);

				if($user){
					if($user->group = \Controller\AuthController::find_group_accesslvl($user->accessLevel)){

						$account = \Model\Account::find($user->login);

						if( empty($account) ){
							$account = new \Model\Account;
							$account->login = $user->login;
							$account->email = $user->email;
							$account->language = LOCALE;
							//$account->last_activity = TIME;
							$account->save(true);
						}

						$user->account = $account;
						
						\Controller\AuthController::authenticate($user);

						return redirect('/account/updatepassword?code=' . $mailcode->code);
					}
				}
			}

			return [
				'code' => 'danger',
				'message' => locale('invalid_code')
			];					
		}

		$email = strtolower($email);

		$user = \Model\Game\Account::row([
			"login = '{$email}' OR email = '{$email}'"
		]);

		if($user){

			$code = chr( mt_rand( ord( 'a' ) ,ord( 'z' ) ) ) . substr( md5( time( ) ) ,1 );
			$emailcode = new \Model\MailCode();
			$emailcode->login = $email;
			$emailcode->code = $code;
			$emailcode->created = TIME;
			$emailcode->updated = TIME;
			$emailcode->save(true);

			if(getenv('REMOTE_ADDR') !== '127.0.0.1'){
				\Bootie\Mail\Mailer::send(
					$user->email,
					locale('lost_password'),[
						'title'	=> locale('lost_password'),
						'message'	=> locale('recover_password_text') . "\n<br><br><p style='text-align:center;font-weight:bold'>" . $code . '</p>'
					],"emails.base",'info@nlgames.net');
			}

			return [
				'code' => 'success',
				'message' => locale('email_sent')
			];			

		} 

		return [
			'code' => 'warning',
			'message' => locale('invalid_email')
		];		
	}	


	/*
	 * Hash methods used by other services
	 * Lineage2 - Game Server
	 */

	private function l2_password_encrypt($password){
		return base64_encode( pack( "H*", sha1( $password )));
	}


	/*
	 * Hash methods by other services
	 * IP.Board - Forum
	 */

	private function forum_password_encrypt($salt, $password){
		//return md5( md5( $salt ) . md5($password) );
		/* New password style introduced in IPS4 using Blowfish */
		return md5( md5( $salt ) . md5( \Controller\AuthController::legacyEscape( $password ) ) );
	}

	/**
	 * Encrypt a plaintext password
	 *
	 * @param	string	$password	Password to encrypt
	 * @return	string	Encrypted password
	 * @todo	[Future] When we increase minimum PHP version, adjust blowfish to $2y$
	 */
	public function encryptedPassword( $members_pass_salt, $password )
	{
		/* New password style introduced in IPS4 using Blowfish */
		if ( mb_strlen( $members_pass_salt ) === 22 )
		{
			return crypt( $password, '$2a$13$' . $members_pass_salt );
		}
		/* Old encryption style using md5 */
		else
		{
			return md5( md5( $members_pass_salt ) . md5( \Controller\AuthController::legacyEscape( $password ) ) );
		}
	}

	public function showgroup(){
		return [
			'code' => 'warning',
			'position'	=> 'modal',
			'title' => locale(session('group')),
			'message' => locale('group') . ' ' . locale(session('group')),
			'continue' => '/' . session('group') . '/dash'
		];		
	}


	/**
	 * Old IPB escape-on-input routine
	 *
	 * @param	string	$val	The unescaped text
	 * @return	string			The IPB3-style escaped text
	 */
	private static function legacyEscape( $val )
	{
    	$val = str_replace( "&"			, "&amp;"         , $val );
    	$val = str_replace( "<!--"		, "&#60;&#33;--"  , $val );
    	$val = str_replace( "-->"		, "--&#62;"       , $val );
    	$val = str_ireplace( "<script"	, "&#60;script"   , $val );
    	$val = str_replace( ">"			, "&gt;"          , $val );
    	$val = str_replace( "<"			, "&lt;"          , $val );
    	$val = str_replace( '"'			, "&quot;"        , $val );
    	$val = str_replace( "\n"		, "<br />"        , $val );
    	$val = str_replace( "$"			, "&#036;"        , $val );
    	$val = str_replace( "!"			, "&#33;"         , $val );
    	$val = str_replace( "'"			, "&#39;"         , $val );
    	$val = str_replace( "\\"		, "&#092;"        , $val );
    	
    	return $val;
	}

	public static function compareHashes( $expected, $provided )
	{
		if ( !is_string( $expected ) || !is_string( $provided ) )
		{
			return FALSE;
		}
	
		$len = \strlen( $expected );
		if ( $len !== \strlen( $provided ) )
		{
			return FALSE;
		}
	
		$status = 0;
		for ( $i = 0; $i < $len; $i++ )
		{
			$status |= ord( $expected[ $i ] ) ^ ord( $provided[ $i ] );
		}
		
		return $status === 0;
	}


}