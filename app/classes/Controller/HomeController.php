<?php namespace Controller;

class HomeController extends \Controller\BaseController {
	
	static $layout = "default";

	public function intro(){
		return \Bootie\App::view('intro',null,'intro');
	}
	
	public function news(){
		return \Bootie\App::view('index',array(
			'slides'	=> \Controller\BlogController::find_by_tag('slide'),
			'feature'	=> \Controller\BlogController::find_by_tag('feature'),
			'about'	=> \Controller\BlogController::find_by_tag('about'),
			'forum_latest' => [], /*\Model\Forum\Post::fetch([
				'approved' => 1
			],4,0,[
				'start_date' => "DESC"
			])*/
		));
	}

	public function index(){
		return \Bootie\App::view("promo",null,'promo');
	}

	public function mobile(){
		return \Bootie\App::view("mobile",null,'mobile');
	}

	public function joinbeta(){

		$packages = \Model\AccountPackage::fetch(['login' => session('login') ]);
		$acquirements = [];

        if( isset($_SESSION['coins'])){
          unset($_SESSION['coins']);
        }

        $_SESSION['coins'] = \Controller\User\PanelController::get_coins(session('char_id'));

		foreach($packages as $package){
			$acquirements[]= $package->package->slug;
		}

		return \Bootie\App::view("joinbeta",[
			'packages' => \Model\Package::fetch(['coins > 0'],0,0,['coins' => 'ASC']),
			'acquirements' => $acquirements
		],'full');
	}

	public function story(){
		return \Bootie\App::view("static.story",null,'story');
	}

	public function page($slug){
		return \Bootie\App::view("static.$slug");
	}

	public function modal($slug){
		if(substr($slug,0,7) == 'account' AND !session('login')){
			die(\Controller\AuthController::logout());
		}

		$data = $_REQUEST;

		if($slug == 'account.coins'){
			$promos = \Model\Package::fetch(['discount > 0']);
			$ret  ="";
			$type = "general";
			$gdisc = 0;
			$data['packs'] = [];

			foreach($promos as $promo){
				if($ret!=""){
					if($ret <> $promo->discount){
						$type = "selective";
					}
					$gdisc = $promo->discount;
				}
				$ret = $promo->discount;
			}

			if($type=="general")	{
				$data['packs']['hero']= locale('promos_all_package_discount',null,['discount'=>round($gdisc)]);
			} else {
				foreach($promos as $promo){	
					$data['packs'][$promo->slug]= locale('promos_package_discount',null,['package'=>$promo->slug,'discount'=>round($promo->discount)]);
				}
			}
		}

		return \Bootie\App::view("modals.$slug",$data,null,true);
	}

	public function captcha(){

		// Draw the image
		if( isset($_GET['_CAPTCHA']) ) {

		    //session_start();

		    $captcha_config = unserialize($_SESSION['_CAPTCHA']['config']);
		    if( !$captcha_config ) exit();

		    unset($_SESSION['_CAPTCHA']);

		    // Pick random background, get info, and start captcha
		    $background = $captcha_config['backgrounds'][mt_rand(0, count($captcha_config['backgrounds']) -1)];
		    list($bg_width, $bg_height, $bg_type, $bg_attr) = getimagesize($background);

		    $captcha = imagecreatefrompng($background);

		    $color = hex2rgb($captcha_config['color']);
		    $color = imagecolorallocate($captcha, $color['r'], $color['g'], $color['b']);

		    // Determine text angle
		    $angle = mt_rand( $captcha_config['angle_min'], $captcha_config['angle_max'] ) * (mt_rand(0, 1) == 1 ? -1 : 1);

		    // Select font randomly
		    $font = $captcha_config['fonts'][mt_rand(0, count($captcha_config['fonts']) - 1)];

		    // Verify font file exists
		    if( !file_exists($font) ) throw new Exception('Font file not found: ' . $font);

		    //Set the font size.
		    $font_size = mt_rand($captcha_config['min_font_size'], $captcha_config['max_font_size']);
		    $text_box_size = imagettfbbox($font_size, $angle, $font, $captcha_config['code']);

		    // Determine text position
		    $box_width = abs($text_box_size[6] - $text_box_size[2]);
		    $box_height = abs($text_box_size[5] - $text_box_size[1]);
		    $text_pos_x_min = 0;
		    $text_pos_x_max = ($bg_width) - ($box_width);
		    $text_pos_x = mt_rand($text_pos_x_min, $text_pos_x_max);
		    $text_pos_y_min = $box_height;
		    $text_pos_y_max = ($bg_height) - ($box_height / 2);
		    if ($text_pos_y_min > $text_pos_y_max) {
		        $temp_text_pos_y = $text_pos_y_min;
		        $text_pos_y_min = $text_pos_y_max;
		        $text_pos_y_max = $temp_text_pos_y;
		    }
		    $text_pos_y = mt_rand($text_pos_y_min, $text_pos_y_max);

		    // Draw shadow
		    if( $captcha_config['shadow'] ){
		        $shadow_color = hex2rgb($captcha_config['shadow_color']);
		        $shadow_color = imagecolorallocate($captcha, $shadow_color['r'], $shadow_color['g'], $shadow_color['b']);
		        imagettftext($captcha, $font_size, $angle, $text_pos_x + $captcha_config['shadow_offset_x'], $text_pos_y + $captcha_config['shadow_offset_y'], $shadow_color, $font, $captcha_config['code']);
		    }

		    // Draw text
		    imagettftext($captcha, $font_size, $angle, $text_pos_x, $text_pos_y, $color, $font, $captcha_config['code']);

		    // Output image
		    header("Content-type: image/png");
		    imagepng($captcha);

		}
	}

}