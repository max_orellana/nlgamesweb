<?php namespace Controller;

class BlogController extends \Controller\BaseController  {

	static $layout = "default";
		
	public function index(){
		return \Bootie\App::view('blog.index',[
			'posts'	=> \Model\Post::paginate(['id' => 'DESC'],['privacy_id' => 1],6),
			'tags'	=> self::find_all_tags(),
			'about'	=> \Controller\BlogController::find_by_tag('about')
		]);
	}

	public function tag($tag){
		return \Bootie\App::view('blog.tags',[
			'posts'	=> self::find_by_tag($tag),
			'tags'	=> self::find_all_tags(),
			'about'	=> \Controller\BlogController::find_by_tag('about'),
			'tag'	=> $tag
		]);
	}

	public function show($slug){

		$tags_ids = [];

		$entry = \Model\Post::row([
			'slug' => urldecode($slug)
		]);

		if($entry) {

			// attach social network metadata
			$meta = new \stdClass();
			$meta->og_title = $entry->{'title_' . LOCALE};
			$meta->og_description = $entry->{'caption_' . LOCALE};
			$posts_id = $related = [];

			foreach($entry->tags() as $tag){
				if( isset($tag->id)){
					$tags_ids[] = $tag->id;
				}
			}

			/*
			if(count($entry->files())){
				$meta->og_image = site_url('upload/posts/std/' . $entry->files()[0]->name);
			}
			*/

			if(count($tags_ids)){
				$tag_obs = \Model\TagRelation::fetch([
					'tag_id IN(' . implode(',',array_unique($tags_ids)) . ')'
				]);

				foreach($tag_obs as $tag){
					if($tag->post_id == $entry->id) continue;
					$posts_id[] = $tag->post_id;
				}

				if(count($posts_id)){
					$related = \Model\Post::fetch([
						'id IN(' . implode(',',array_unique($posts_id)) . ')',
						"content_" . LOCALE . " <> '<p><br></p>'"
					]);
				}
			}

			return \Bootie\App::view('blog.entry',[
				'entry'	=> $entry,
				'meta'	=> $meta,
				'related' => $related
			]);
		} 

		return \Bootie\App::view('errors.missing');
	}


	public function find_by_tag($tags,$where = [], $limit = 0){

		$tags = "'" . str_replace(",","','",$tags) . "'";
		$posts = [];

		$tags_id = \Model\Tag::select('fetch','id',null,[
			'tag IN(' . $tags . ')'
		]);

		if(is_array($tags_id)){

			$posts_id = \Model\TagRelation::select('fetch','post_id',null,[
				'tag_id IN(' . implode(',',$tags_id) . ')'
			]);

			$wheres = [
				'id IN(' . implode(',',array_unique($posts_id)) . ')',
				'privacy_id' => 1
			];

			if(count($where)) $wheres[] = implode(' AND ', $where);

			if(count($posts_id)){
				$posts = \Model\Post::paginate([
					'id' => 'DESC'
				],$wheres,($limit?:10));
			}

			return $posts;
		}

		return FALSE;
	}

	public function find_all_tags(){

		$posts_id = \Model\Post::select('fetch','id');

		if(count($posts_id)){

			$tags_ids = \Model\TagRelation::select('fetch','tag_id',null,[
				'post_id IN(' . implode(',',$posts_id) . ')'
			]);

			if(count($tags_ids)){
				return \Model\Tag::fetch([
					'id IN(' . implode(',',array_unique($tags_ids)) . ')'
				]);
			}
		}

		return FALSE;
	}

	public function files($type, $id){
		$files = [];

		if($id){
			$files = \Model\File::select('fetch','*',null,[
				$type . '_id' => $id
			]);
		}

		return $files;
	}	
}