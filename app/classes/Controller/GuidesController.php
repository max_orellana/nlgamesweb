<?php namespace Controller;

class GuidesController extends \Controller\BaseController {
	
	static $layout = "guides";

	public function index(){

		return \Bootie\App::view('guides.index',[
			'latest' => \Controller\BlogController::find_by_tag('feature',[
				"content_" . LOCALE . " <> ''","content_" . LOCALE . " <> '<p><br></p>'" 
			],4),
			'forum_latest' => \Model\Forum\Post::fetch([
				'approved' => 1
			],4,0,[
				'start_date' => "DESC"
			])
		]);
	}


	public function show($slug){

		$tags_ids = [];

		$entry = \Model\Post::row([
			'slug' => urldecode($slug)
		]);

		if($entry) {

			// attach social network metadata
			$meta = new \stdClass();
			$meta->og_title = $entry->{'title_' . LOCALE};
			$meta->og_description = $entry->{'caption_' . LOCALE};
			$posts_id = $related = [];

			foreach($entry->tags() as $tag){
				if( isset($tag->id)){
					$tags_ids[] = $tag->id;
				}
			}

			/*
			if(count($entry->files())){
				$meta->og_image = site_url('upload/posts/std/' . $entry->files()[0]->name);
			}
			*/

			if(count($tags_ids)){
				$tag_obs = \Model\TagRelation::fetch([
					'tag_id IN(' . implode(',',array_unique($tags_ids)) . ')'
				]);

				foreach($tag_obs as $tag){
					if($tag->post_id == $entry->id) continue;
					$posts_id[] = $tag->post_id;
				}

				if(count($posts_id)){
					$related = \Model\Post::fetch([
						'id IN(' . implode(',',array_unique($posts_id)) . ')',
						"content_" . LOCALE . " <> '<p><br></p>'"
					]);
				}
			}

			return \Bootie\App::view('guides.entry',[
				'entry'	=> $entry,
				'meta'	=> $meta,
				'related' => $related
			]);
		} 

		return \Bootie\App::view('errors.missing');
	}
	
	public function heroe($slug){
		$heroe = \Model\Heroe::row(['slug' => $slug]);
		if(empty($heroe)) die("empty");
		return \Bootie\App::view("guides.heroes.".segments(2),[
			'heroe' => $heroe
		],'heroe');
	}

	public function layout(){
		return \Bootie\App::view("guides.".segments(2),null,segments(2));
	}

	public function page($slug){
		return \Bootie\App::view("guides.pages.$slug",null,'guides');
	}
}