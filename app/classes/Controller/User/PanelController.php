<?php namespace Controller\User;

class PanelController extends \Controller\BaseController {
	
	static $layout = "panel";
	static $coins = [50,100,250,500,1000];

	public function index(){

		$rows = \Controller\BlogController::find_by_tag('user-dash');
		//dd(session('advertise_token'));

		return \Bootie\App::view('panel.user.dash',[
			'pack' => \Model\AccountPackage::row(['login' => session('login')]),
			'account' => \Model\Account::find(session('login'))
		]);
	}

	public function profile(){
		return \Bootie\App::view('panel.user.profile',[
			'package' => \Model\AccountPackage::row(['login' => session('login')]),
			'game' => \Model\Game\Account::row([
				'login' => session('login')
			])
		]);
	}

	public function joinbeta(){

		extract($_POST);

		$package = \Model\Package::row(['slug' => $id ]);

		if(empty($package)){
			return [
				'code' => 'danger',
				'position'	=> 'modal',
				'title' => locale('package_acquirement_error_title'),
				'message' => locale('package_acquirement_error_no_selection')
			];
		}

		$user = \Model\Account::row(['login' => session('login')]);

		if(empty($user)){
			return [
				'code' => 'danger',
				'position'	=> 'modal',
				'title' => locale('package_acquirement_error_title'),
				'message' => locale('login_wrong')
			];
		}

		$realcoins = $package->coins - intval($package->discount / 100 * $package->coins);
		if( session('coins') < $realcoins ){
			return [
				'code' => 'warning',
				'position'	=> 'modal',
				'title' => locale('package_acquirement_error_title'),
				'message' => locale('not_enought_coins_to_proceed')
			];
		}

		$exists = \Model\AccountPackage::row([
			'login' => session('login'),
			'package_id' => $package->id
		]);

		if( $exists ){
			return [
				'code' => 'warning',
				'position'	=> 'modal',
				'title' => locale('package_acquirement_error_title'),
				'message' => locale('package_acquirement_error_already_acquired')
			];
		}

		// save purchase history
		$accountpackage = new \Model\AccountPackage();
		$accountpackage->login = session('login');
		$accountpackage->package_id = $package->id;
		$accountpackage->created = TIME;
		$accountpackage->updated = TIME;
		$accountpackage->save(true);

		// update user wallet
		$balance = $package->refund - $realcoins;
		$coins = self::set_coins( $balance, session('char_id'));

		if(isset($_SESSION['coins'])){
			unset($_SESSION['coins']);
		}

		$_SESSION['coins'] = $coins;

		if(isset($_SESSION['package'])){
			unset($_SESSION['package']);
		}

		$_SESSION['package'] = strtoupper($package->slug);

		message(session('login'),'package_acquirement_success_title','package_acquirement_success_text');

		return [
			'balance' => $coins,
			'slug' => $package->slug,
			'code' => 'success',
			'position'	=> 'modal',
			'title' => locale('package_acquirement_success_title'),
			'message' => locale('package_acquirement_success_text'),
			'continue' => '/joinbeta'
		];
	}

	public function get_coins($char_id){
		$item = \Model\Game\Item::row(['owner_id' => $char_id, 'item_id' => config()->game_dc_item_id]);
		return isset($item->count)?$item->count:0;
	}

	public function set_access($accesslevel, $login){

		$user = \Model\Game\Account::find($login);
		if($user){
			$user->accessLevel = $accesslevel;
			$user->save();
		}
	}

	public function set_coins($coins, $char_id){
		// update user wallet

		if(empty($char_id) OR empty($coins)){
			\log_message("error setting coins -- [char_id: " . $char_id . ", coins:" . $char_id . "]");
			return ['code' => 'danger', 'message' => locale('not_valid_input')];
		}

		$item = \Model\Game\Item::row(['owner_id' => $char_id, 'item_id' => config()->game_dc_item_id]);

		if(empty($item)){

			$lastobjectid =  \Model\Game\Item::select('column','MAX(object_id)');

			$item = new \Model\Game\Item;
			$item->owner_id = $char_id;
			$item->object_id = $lastobjectid+1;
			$item->item_id = config()->game_dc_item_id;
			$item->count = 0;     
			$item->enchant_level = 0;
			$item->loc = 'INVENTORY';
			$item->loc_data = 0;

			$item->save(true);
		}

		if(!empty($coins)){
			$item = \Model\Game\Item::row(['owner_id' => $char_id, 'item_id' => config()->game_dc_item_id]);
			$item->count = ($item->count + $coins);
			$item->save();			
		}

		return $item->count;
	}

	public function updatepassword(){

		extract($_GET);

		$skiprequestpassword = 0;

		if(!empty($code)) {
			$mailcode = \Model\MailCode::row([
				"code = '" . $code . "' AND expired = 0" 
			]);

			if($mailcode) {
				$skiprequestpassword = 1;
			}
		}

		return \Bootie\App::view('panel.user.updatepassword',[
			'skiprequestpassword' => $skiprequestpassword
		]);
	}

	public function packages(){
		return \Bootie\App::view('panel.user.packages',[
			'entry' => \Model\Account::find(session('login'))
		]);
	}

	public function balance(){
		return \Bootie\App::view('panel.user.balance',[
			'entry' => \Model\Account::row([
				'login' => session('login')
			]),
			'orders_pending' => \Model\Order::fetch([
				'login' => session('login'),
				'status' => 'pending'
			])
		]);
	}

	public function coins(){
		return \Bootie\App::view('panel.user.coins',[
			'entry' => \Model\Account::row([
				'login' => session('login')
			])
		]);
	}

	public function payment(){

		extract($_GET);

		/*
		 * Look in db for previous existing orders not completed
		 */

		if( ! empty($ref)) {
			$id = get_order_id($ref);
			$order = \Model\Order::find($id);
		}

		/*
		 * If no previous uncompleted order, then create a new one
		 */

		if( empty($order) ){
			$order = new \Model\Order();
			$order->created = TIME;
			$order->login = session('login');
			$order->char_id = session('char_id');
			$order->title = locale('donations_coins_acquirement');
			$order->comments = locale('donations_coins_acquirement_text');
			$order->quantity = 1;
			$order->source = $source;
			$order->save();

			$order->reference = get_order_reference($order->id);
		} 

		if( ! empty($coins)){
			$order->coins = $coins;
			$order->amount = monetize_coins($coins);
		}

		$order->updated = TIME;
		$order->save();

		return \Bootie\App::view('panel.user.payment',[
			'order' => $order
		]);
	}


	public function resume(){
		
		extract($_POST);

		/*
		 * Look in db for previous existing orders not completed
		 */

		$order = \Model\Order::find($order_id);

		/*
		 * If user has no order pending send him back to balance.
		 */

		if( ! $order ) {
			redirect('/account/balance');
			exit();
		}

		$order->payment_method = $payment_method;
		$order->currency = $payment_method == 'paypal' ? "USD" : "ARS";
		$order->updated = TIME;
		$order->save();

		return \Bootie\App::view('panel.user.resume',[
			'order' => $order
		]);
	}

	public function order(){

		extract($_POST);
		/*
		 * Look in db for previous existing orders not completed
		 */

		if( empty($id)) {
			redirect('/user/balance',[
				'code' => 'danger',
				'message' => locale("order_not_found")
			]);

			exit();
		}

		$order = \Model\Order::find($id);

		/*
		 * If user has no order pending send him back to balance.
		 */

		if( ! $order ) {
			redirect('/user/balance',[
				'code' => 'danger',
				'message' => locale("order_not_found")
			]);

			exit();
		}

		return \Bootie\App::view('payments.connecting',[
			'order' => $order
		]);
	}


	public function wannajoinbeta(){

		extract($_POST);

		$user = \Model\Account::find(session('login'));
		$user->experience = $experience; 
		$user->role = $role; 
		$user->heroe = $heroe; 
		$user->week_play_hours = $week_play_hours; 
		$user->save(); 

		return [
			'code' => 'success',
			'position' => 'modal',
			'title' => locale('wannajoibeta_success_title'),
			'message' => locale('wannajoibeta_success_text'),
			'continue' => '/account/index'
		];
	}

	public function forum(){
		return \Bootie\App::view('panel.user.forum',[
			'row' => \Model\Account::row([
				'login' => session('login')
			])
		]);
	}

	public function forum_connect(){

		@extract($_POST);

		$login = trim($login);
		$password = trim($password);

		$json = [
			'code' => 'danger',
			'position'	=> 'modal',
			'title' => locale('forum_connection'),
			'message' => locale('forum_connection_wrong_user_password')
		];


		$db = \Bootie\App::load_database('forum');
		$user = $db->fetch("select * from core_members 
			where name = '{$login}'");

		if($user){

			$user = $user[0];

			if ( \Controller\AuthController::compareHashes( $user->members_pass_hash, \Controller\AuthController::encryptedPassword( $user->members_pass_salt, $password ) ) ){		


				$connected = \Model\Account::row([
					"LOWER(forum_name) = '{$login}'"
				]);

				if($connected) {
					return [
						'code' => 'danger',
						'position'	=> 'modal',
						'title' => locale('forum_connection_already_exists'),
						'message' => locale('forum_connection_already_exists_text')
					];
				}

				// find posts counts
				//$postcount = $db->row("select count(*) as total from forumposts where author_id = $user->member_id");

				$row = \Model\Account::find(session('login'));

				if(empty($row))	$row = new \Model\Account;

				$photo = "";
				if( ! empty($user->pp_thumb_photo))	$photo = config()->forum_url . '/uploads/' . $user->pp_thumb_photo;

				$row->forum_id = $user->member_id;
				$row->forum_name = $user->name;
				$row->forum_title = $user->member_title;
				$row->forum_avatar = $photo;
				$row->forum_members_day_posts = $user->members_day_posts;
				$row->forum_joined = $user->joined;
				$row->forum_timezone = $user->timezone;
				$row->forum_last_activity = $user->last_activity;
				$row->forum_last_visit = $user->last_visit;
				
				$row->save();

				session_put('forum_avatar',$photo);
				session_put('forum_name',$row->forum_name);
				session_put('forum_members_day_posts',$row->forum_members_day_posts);
				session_put('forum_last_activity',$row->forum_last_activity);
				session_put('forum_joined',$row->forum_joined);

				$json = [
					'code' => 'success',
					'position'	=> 'modal',
					'title' => locale('forum_connection'),
					'message' => locale('forum_connection_success'),
					'continue' => '/account/forum'
				];

			}
		}

		return \Bootie\App::ajax($json);
	}

	public function forum_disconnect(){

		$row = \Model\Account::find(session('login'));

		$json = [
			'code' => 'danger',
			'position'	=> 'modal',
			'title' => locale('forum_connection'),			
			'message' => locale('forum_disconnection_user_not_found')
		];

		if($row){


			$row->forum_id = "";
			$row->forum_name = "";
			$row->forum_avatar = "";
			$row->forum_members_day_posts = "";
			$row->forum_timezone = "";
			$row->forum_joined = "";
			$row->forum_last_activity = "";

			$row->save();

			session_put('forum_name');
			session_put('forum_avatar');
			session_put('forum_members_day_posts');
			session_put('forum_joined');
			session_put('forum_last_activity');

			$json = [
				'code' => 'success',
				'position'	=> 'modal',
				'title' => locale('forum_connection'),				
				'message' => locale('forum_disconnection_success'),
				'continue' => '/account/forum'
			];
		}

		return \Bootie\App::ajax($json);
	}

	public function postulations(){

		$groups = \Model\PostulationGroup::fetch();
		$applicants = [];
		$mys = [];

		foreach($groups as $group){
			foreach($group->postulations() as $postulation){
				$count = \Model\PostulationRelation::count([
					'postulation_id' => $postulation->id,
					'status' => 5
				]);

				$my = \Model\PostulationRelation::count([
					'postulation_id' => $postulation->id,
					'login' => session('login')
				]);

				$applicants[$postulation->id] = ($count + $postulation->base);
				$mys[$postulation->id] = $my;
			}
		}

		return \Bootie\App::view('panel.user.postulations',[
			'groups' => $groups,
			'mys' => $mys,
			'applicants' => $applicants
		]);
	}

	public function postulations_send(){

		@extract($_POST);

		if(is_numeric($postulation_id)){

			$login = session('login');

			// check 1 postulation type per user
			$exists = \Model\PostulationRelation::row([
				'postulation_id' => $postulation_id,
				'login' => $login
			]);

			if( $exists ){
				return redirect("/account/postulations",[
					'code' => 'danger',
					'position' => 'modal',
					'title' => locale('postulations'),
					'message' => locale('postulations_duplicate')
				]);
			}

			$row = new \Model\PostulationRelation;
			$row->postulation_id = $postulation_id;
			$row->login = $login;
			$row->message = $message;
			$row->languages = $languages;
			$row->age = $age;
			$row->fullname = $fullname;
			$row->residence = $residence;
			$row->availability = $availability;
			$row->skype = $skype;
			$row->medium = $medium;
			$row->medium_reach = $medium_reach;
			$row->experience = $experience;
			$row->created = TIME;
			$row->updated = TIME;

			if(isset($_FILES['file']) AND ! $_FILES['file']['error']){
				$row->file = \Controller\FileController::upload('postulations');
			}

			$row->save();

			//message(session('login'),'notification_postulation_sent_title','notification_postulation_sent_message');
 			
			return redirect("/account/postulations",[
				'code' => 'success',
				'position'	=> 'modal',
				'title' => locale('postulations'),
				'message' => locale('postulations_recieved')
			]);
		}

		return redirect("/account/postulations",[
			'code' => 'danger',
			'position'	=> 'modal',
			'message' => locale('postulations_error')
		]);
	}	

	public function tickets(){
		return \Bootie\App::view('panel.user.tickets',[
			'rows' => \Model\Ticket::fetch([
				'user' => session('login'),
				'ticket_id' => 0,
				'status_id' => 1
			],0,0,[
				'created' => 'DESC'
			]),
			'tags'	=> \Model\Tag::fetch([
				'type'	=> 'ticket'
			])
		]);
	}

	public function ticket($code){

		$thread = \Model\Ticket::row([
			'code' => $code
		]);

		return \Bootie\App::view('panel.user.ticket',[
			'thread' => $thread,
			'rows' => \Model\Ticket::fetch([
				'id = ' . $thread->id . ' OR ticket_id IN(' . $thread->id . ')'
			],0,0,[
				'created' => 'ASC'
			])
		]);
	}

	public function tickets_send(){

		@extract($_POST);

		if(strlen($description)){

			$login = session('login');

			$row = new \Model\Ticket;

			if(isset($ticket_id)){
				$row->ticket_id = $ticket_id;
			} else {
				$row->tag_id = $tag_id;
				$row->title = $title;
			}

			if(isset($_FILES['file']) AND ! $_FILES['file']['error']){
				$row->file = \Controller\FileController::upload('tickets');
			}

			$row->user = $login;
			$row->group_id = 2;
			$row->content = $description;

			$row->created = TIME;
			$row->updated = TIME;
			$row->save();

			$code = hash('sha1', $row->id);
			$row->code = substr(md5($row->id),0,8);
			$row->save();
			
			return redirect("/account/tickets",[
				'code' => 'success',
				'message' => locale('ticket_updated')
			]);
		}

		return redirect("/account/tickets",[
			'code' => 'danger',
			'message' => locale('error')
		]);
	}	
}