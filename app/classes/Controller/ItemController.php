<?php namespace Controller;

class ItemController extends \Controller\BaseController {
	
	static $layout = "panel";

	public function index(){
		return \Bootie\App::view('items.index',[
			'entries'	=> \Model\Item::fetch()
		]);
	}
}