<?php namespace Controller\Advertiser;

class PanelController extends \Controller\BaseController {
	
	static $layout = "panel";
	static $cache = [];
	static $rules = [[	
			'coins' => 700,
			'registers' => 0,
			'perc' => 0.15
		],[
			'coins' => 700,
			'registers' => 100,
			'perc' => 0.15
		],[
			'coins' => 3000,
			'registers' => 100,
			'perc' => 0.2
		],[
			'coins' => 3000,
			'registers' => 225,
			'perc' => 0.2
		],[
			'coins' => 10000,
			'registers' => 425,
			'perc' => 0.25
		],[
			'coins' => 99999999,
			'registers' => 99999999,
			'perc' => 0.3
		]
	];

	static $periods = [
		'day' 	=> TIME - (3600 * 24 * 1),
		'week' 	=> TIME - (3600 * 24 * 7),
		'month' => TIME - (3600 * 24 * 30),
		'total' => 0
	];

	public function index(){


		// stats calculation
		// registers
		$registers_day = count(self::registers('day'));
		$registers_week = count(self::registers('week'));
		$registers_month = count(self::registers('month'));
		$registers_total = count(self::registers());

		$coins_perc = self::perc();

		// packs
		$packs_day = count(self::packs('day'));
		$packs_week = count(self::packs('week'));
		$packs_month = count(self::packs('month'));
		$packs_total = count(self::packs());

		// coins day
		$coins_day = self::coins('day');
		$coins_day+= $registers_day;

		if($coins_day){
			$coins_day = round($coins_day * $coins_perc);
		}

		// coins week
		$coins_week = self::coins('week');
		$coins_week+= $registers_week;

		if($coins_week){
			$coins_week = round($coins_week * $coins_perc);
		}

		// coins month
		$coins_month = self::coins('month');
		$coins_month+= $registers_month;

		if($coins_month){
			$coins_month = round($coins_month * $coins_perc);
		}

		// coins total

		$coins_total = self::coins();
		$coins_total+= $registers_total;

		if($coins_total){
			$coins_total = round($coins_total * $coins_perc);
		}


		return \Bootie\App::view('panel.advertiser.dash',[
			'pack' => \Model\AccountPackage::row(['login' => session('login')]),
			'account' => \Model\Account::find(session('login')),
			'registers_day' => $registers_day,
			'registers_week' => $registers_week,
			'registers_month' => $registers_month,
			'registers_total' => $registers_total,
			'packs_day' => $packs_day,
			'packs_week' => $packs_week,
			'packs_month' => $packs_month,
			'packs_total' => $packs_total,
			'coins_day' => $coins_day,
			'coins_week' => $coins_week,
			'coins_month' => $coins_month,
			'coins_total' => $coins_total
		]);
	}

	public function links(){
		return \Bootie\App::view('panel.advertiser.links');
	}

	public function progress($period = 'total'){

		$registers = self::registers($period);
		$orders = self::orders($period);
		$data = [];

		foreach($registers as $row){
			$data[] = [
				'css'	=> 'info',
				'login' => $row->login, 
				'avatar' => (!empty($row->account->forum_avatar) ? avatar($row->login,$row->account->forum_avatar) : ''),
				'unixtime' => $row->created,
				'since' => timespan($row->created),
				'value' => 1,
				'datetime' => date('M d H:i',$row->created)
			];
		}

		foreach($orders as $row){
			$data[] = [
				'css'	=> 'success',
				'login' => $row->login, 
				'avatar' => (!empty($row->account->forum_avatar) ? avatar($row->login,$row->account->forum_avatar) : ''),
				'unixtime' => $row->created,
				'since' => timespan($row->created),
				'value' => $row->coins,
				'datetime' => date('M d H:i',$row->created)
			];
		}

		$rows = bubble_sort($data,'unixtime','desc');

		return \Bootie\App::view('panel.advertiser.progress',[
			'entries' => $rows
		]);
	}

	public function goals(){
		$level = self::level();

		return \Bootie\App::view('panel.advertiser.goals',[
			'rules' => self::$rules,
			'level' => $level,
			'coins'	=> self::coins(),
			'registers'	=> self::registers(),
			'bar_perc' => self::bar_perc()
		]);		
	}

	public function advertees(){
		if(!empty(self::$cache['advertees'])) return self::$cache['advertees'];

		$advertees = \Model\AdvertiserCapture::select('fetch','login',null, [
			'advertiser' => session('login')
		]);

		self::$cache['advertees'] = $advertees;
		return $advertees;
	}


	public function coins($period = 'total'){

		$coins = 0;

		$time = self::$periods[$period];
		$advertees = self::advertees();

		// coins
		$orders = \Model\Order::fetch([
			"login IN('" . implode("','",$advertees) . "')",
			"status = 'approved'",
			'created > ' . $time
		]);

		foreach($orders as $order){
			$coins+= $order->coins;
		}

		return $coins;
	}

	public function orders($period = 'total'){

		$coins = 0;

		$time = self::$periods[$period];
		$advertees = self::advertees();

		// coins
		$orders = \Model\Order::fetch([
			"login IN('" . implode("','",$advertees) . "')",
			"status = 'approved'",
			'created > ' . $time
		]);

		return $orders;
	}

	public function packs($period = 'total'){

		$time = self::$periods[$period];
		$advertees = self::advertees();

		$packs = \Model\Pack::fetch([
			"login IN('" . implode("','",$advertees) . "')",
			'created > ' . $time
		]);

		return $packs;
	}

	public function registers($period = 'total'){

		$time = self::$periods[$period];

		$registers = \Model\AdvertiserCapture::fetch([
			'advertiser' => session('login'),
			'created > ' . $time
		]);

		return $registers;
	}

	public function level(){

		if(!empty(self::$cache['level'])) return self::$cache['level'];

		$level = 0;
		$registers = count(self::registers());
		$coins = self::coins();
		$rules = self::$rules;

		foreach($rules as $i => $rule){
			if($i){
				if($rule['coins'] < $coins AND $rule['registers'] < $registers){
					$level = $i;
					break;
				}
			}
		}

		self::$cache['level'] = $level;
		return $level;
	}

	public function bar_perc(){
		$level = self::level();
		$coins = self::coins();
		$registers = count(self::registers());
		$next_level = $level?$level:$level+1;

		if(!empty(self::$rules[$next_level])){
			$coins_next = self::$rules[$next_level]['coins'];
			$registers_next = self::$rules[$next_level]['registers'];
			$coins_perc = $coins / $coins_next * 100;
			$registers_perc = $registers / $registers_next * 100;

			return ['coins' => $coins_perc, 'registers' => $registers_perc];
		}

		return false;
	}

	public function perc(){
		$level = self::level();
		return self::$rules[$level]['perc'];
	}	
}
