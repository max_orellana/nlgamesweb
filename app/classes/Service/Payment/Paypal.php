<?php namespace Service\Payment;


class Paypal {
   /**
    * Last error message(s)
    * @var array
    */
   protected $_errors = array();


    /**
    * API Version
    * @var string
    */

    protected $_version = '74.0';

   /**
    * API Credentials
    * Use the correct credentials for the environment in use (Live / Sandbox)
    * @var array
    */

   /**
    * Sandbox
    * ===============
    * Username:
    * caracochepablo-facilitator_api1.hotmail.com
    * Password:
    * 3YF2ENYMK3Q58MLH
    * Signature:
    * AFcWxV21C7fd0v3bYYYRCpSSRl31AGmI7LGfI4Vt1OHyjGFtmYDEdXgC
    *
    *
    * Live
    * ===============
    * Username:
    * caracochepablo_api1.hotmail.com
    * Password:
    * 28U9DGU59NVN7RVE
    * Signature:
    * AFcWxV21C7fd0v3bYYYRCpSSRl31AHGcYIqhoBBCgXhvz2JZ2akrrUw8
    */

   protected $_credentials = array(
      'USER' => 'caracochepablo-facilitator_api1.hotmail.com',
      'PWD' => '3YF2ENYMK3Q58MLH',
      'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AGmI7LGfI4Vt1OHyjGFtmYDEdXgC',
   );

   /**
    * API endpoint
    * Sandbox - https://api-3t.sandbox.paypal.com/nvp
    * Live - https://api-3t.paypal.com/nvp
    * @var string
    */

    protected $_endPoint = 'https://api-3t.sandbox.paypal.com/nvp';


    /**
    * API flowpoint
    * Sandbox - https://www.sandbox.paypal.com/cgi-bin/webscr
    * Live - https://www.paypal.com/webscr
    * @var string
    */

    public $_flowPoint = 'https://www.sandbox.paypal.com/cgi-bin/webscr';


    //Our request parameters
    public $_redirectURLs = array(
     'RETURNURL' => 'https://nlgames.net/paypal/return',
     'CANCELURL' => 'https://nlgames.net/paypal/cancel'
    );

   /**
    * Make API request
    *
    * @param string $method string API method to request
    * @param array $params Additional request parameters
    * @return array / boolean Response array / boolean false on failure
    */

    public function request($method,$params = array()) {
      $this->_errors = array();
      if( empty($method) ) { //Check if API method is not empty
         $this->_errors = array('API method is missing');
         return false;
      }

      //Our request parameters
      $requestParams = array(
         'METHOD' => $method,
         'VERSION' => $this->_version
      ) + $this->_credentials;

      //Building our NVP string
      $request = http_build_query($requestParams + $params);

      //cURL settings
      $curlOptions = array (
         CURLOPT_URL => $this->_endPoint,
         CURLOPT_VERBOSE => 1,
         CURLOPT_SSL_VERIFYPEER => true,
         CURLOPT_SSL_VERIFYHOST => 2,
         CURLOPT_SSL_CIPHER_LIST => 'TLSv1',
         //CURLOPT_CAINFO => dirname(__FILE__) . '/cacert.pem', //CA cert file
         CURLOPT_RETURNTRANSFER => 1,
         CURLOPT_POST => 1,
         CURLOPT_POSTFIELDS => $request
      );

      $ch = curl_init();
      curl_setopt_array($ch,$curlOptions);

      //Sending our request - $response will hold the API response
      $response = curl_exec($ch);

      //Checking for cURL errors
      if (curl_errno($ch)) {
         $this->_errors = curl_error($ch);
         curl_close($ch);
         return false;
         //Handle errors
      } else  {
         curl_close($ch);
         $responseArray = array();
         parse_str($response,$responseArray); // Break the NVP string to an array
         return $responseArray;
      }
   }
}