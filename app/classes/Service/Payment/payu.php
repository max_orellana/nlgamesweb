 

Api key:
4ZTqWJnREFrL7bNxy6eqd5mb01
Api login:
xK8WMLNel34JtBj
Public key:
PKe6FA4r5n0vb13DU1KH40F9qA
Merchant Id:
549868


 <form method="post" action="https://stg.gateway.payulatam.com/ppp-web-gateway/">
  <input name="merchantId"    type="hidden"  value="500238"   > + 
  <input name="accountId"     type="hidden"  value="500538" >
  <input name="description"   type="hidden"  value="Test PAYU"  > + 
  <input name="referenceCode" type="hidden"  value="TestPayU" > + 
  <input name="amount"        type="hidden"  value="3"   > + 
  <input name="tax"           type="hidden"  value="0"  >
  <input name="taxReturnBase" type="hidden"  value="0" >
  <input name="currency"      type="hidden"  value="ARS" > + 
  <input name="signature"     type="hidden"  value="be2f083cb3391c84fdf5fd6176801278"  > + 
  <input name="test"          type="hidden"  value="1" >
  <input name="buyerEmail"    type="hidden"  value="test@test.com" >
  <input name="responseUrl"    type="hidden"  value="http://www.test.com/response" >
  <input name="confirmationUrl"    type="hidden"  value="http://www.test.com/confirmation" >
  <input name="Submit"        type="submit"  value="Enviar" >
</form>

Once you generate the HTML form you should aim at the following URL:
Test: https://stg.gateway.payulatam.com/ppp-web-gateway/

Live: https://gateway.payulatam.com/ppp-web-gateway/


The signature is a unique way to validate payments made through the platform, ensuring its authenticity. It consists of a character string to which the MD5 or SHA algorithm is applied for encryption. The string is composed as follows:

“ApiKey~merchantId~referenceCode~amount~currency”.

For example:

merchantId: 500238
ApiKey: 6u39nqhq8ftd0hlvnjfs66eh8c
referenceCode: TestPayU
amount: 3
currency: USD
accountId: 500537
buyerEmail: test@test.com

The signature would be:

"6u39nqhq8ftd0hlvnjfs66eh8c~500238~TestPayU~3~USD"

To this signature MD5 is applied:

"be2f083cb3391c84fdf5fd6176801278
