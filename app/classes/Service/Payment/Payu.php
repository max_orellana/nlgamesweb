<?php namespace Service\Payment;

class Payu {

    public $_apikey;
    private $_setEntorno;
    private $_amount;
    private $_currency;
    private $_referenceCode;
    private $_setProductoDescripcion;
    private $_setTitular;
    private $_merchantID;
    private $_accountID;
    private $_apilogin;
    private $_publickey;
    private $_setTerminal;
    private $_setTransactionType;
    private $_setUrlNotificacion;
    private $_setClave;
    private $_setUrlOk;
    private $_setUrlKo;
    private $_setFirma;
    private $_setNombreComercio;
    private $_setIdioma;
    private $_setMethods;
    private $_setNameForm;
    private $_setSubmit;

    public function __construct(){
        $this->set_nameform('servired_form');
        
        // prod env

        $this->accountID('552158');
        $this->merchantID('549868');
        $this->apikey('4ZTqWJnREFrL7bNxy6eqd5mb01');
        $this->apilogin('xK8WMLNel34JtBj');
        $this->publickey('PKe6FA4r5n0vb13DU1KH40F9qA');

        /*        
        // test env
        $this->accountID('509171');
        $this->merchantID('500238');
        $this->apikey('6u39nqhq8ftd0hlvnjfs66eh8c');
        $this->apilogin('11959c415b33d0c');
        */
    }


    #-#####################
    # desc: Asignamos el idioma por defecto 001 = Español
    # param: codeidioma: codigo de idioma [Castellano-001, Inglés-002, Catalán-003, Francés-004, Alemán-005,
    #                   Holandés-006, Italiano-007, Sueco-008, Portugués-009, Valenciano-010, Polaco-011, Gallego-012 y Euskera-013.]
    public function set_idioma($codeidioma){
        $this->_setIdioma = $codeidioma;
    }#set_idioma()

    #-######################
    #desc: Asignamos que tipo de entorno vamos a usar si Pruebas o Real (por defecto esta en pruebas)
    #param: entorno (pruebas, real)
    public function set_entorno($entorno='pruebas'){
        if(trim($entorno) == 'real'){
            //real
            $this->_setEntorno='https://gateway.payulatam.com/ppp-web-gateway/';
        }
        else{
            //pruebas
            $this->_setEntorno ='https://stg.gateway.payulatam.com/ppp-web-gateway/';
        }
    }#-#set_entorno()

    #-######################
    #desc: Retornamos la URL que va en el form para los pagos ya sea en pruebas o real
    #return: URL payu (real o pruebas)
    public function geturlentorno(){
        return $this->_setEntorno;
    }#-#geturlentorno()

    #-######################
    #desc: Retornamos la URL que va en el form para los pagos ya sea en pruebas o real
    #return: URL payu (real o pruebas)
    public function apikey($value){
        return $this->_apikey = $value;
    }#-#geturlentorno()

    #-######################
    #desc: Asignamos el amount de la compra
    #param: amount: total de la compra a pagar
    #return: Retornamos el amount ya modificado
    public function amount($amount=0){
        $amount = $this->parseFloat($amount);

        // payu nos dice Para Euros las dos últimas posiciones se consideran decimales.
        //$amount = intval($amount*100);
        $this->_amount=$amount;
        return $amount;
    }#-#amount()


    #-##############################
    #desc: Asignamos el tipo de currency
    #param: currency: 978 para Euros, 840 para Dólares, 826 para libras esterlinas y 392 para Yenes.
    public function currency($currency='ARS'){
        if($currency == 'ARS' || $currency =='USD'){
            $this->_currency = $currency;
        }
        else{
            throw new \Exception('Moneda no valida');
        }

    }#-#currency()

    #-##############################
    #desc: Asignamos el número de pedido a nuestra compra (Los 4 primeros dígitos deben ser numéricos)
    #param: pedido: Numero de pedido alfanumérico
    public function pedido($pedido=''){
        if(strlen(trim($pedido))> 0){
            $this->_referenceCode = $pedido;
        }
        else{
            throw new Exception('Falta agregar el número de pedido');
        }

    }#-#pedido()

    #-##############################
    #desc: Asigmanos la descripción del producto (Obligatorio)
    #param: producto: Descripción del producto
    public function producto_descripcion($producto=''){
        if(strlen(trim($producto)) > 0){
            //asignamos el producto
            $this->_setProductoDescripcion = $producto;
        }
        else{
            throw new Exception('Falta agregar la descripción del producto');
        }
    }#-#producto_descripcion()

    #-###############################
    #desc: Asigmanos el nombre del usuario que realiza la compra (Obligatorio)
    #param: titular: Nombre del usuario (por ejemplo Juan Perez)
    public function titular($titular=''){
        if(strlen(trim($titular)) > 0){
            $this->_setTitular = $titular;
        }
        else{
            throw new Exception('Falta agregar el titular que realiza la compra');
        }
    }#-#titular()

    #-###########################
    #desc: Asignamos el código FUC del comercio (Obligatorio)
    #param: fuc: Código fuc que nos envía payu
    public function accountID($fuc=''){
        if(strlen(trim($fuc)) > 0){
            $this->_accountID = $fuc;
        }
        else{
            throw new Exception('Falta agregar el código accountId');
        }
    }#-#merchantID


    #-###########################
    #desc: Asignamos el código FUC del comercio (Obligatorio)
    #param: fuc: Código fuc que nos envía payu
    public function merchantID($fuc=''){
        if(strlen(trim($fuc)) > 0){
            $this->_merchantID = $fuc;
        }
        else{
            throw new Exception('Falta agregar el código merchantId');
        }
    }#-#merchantID

    #-###########################
    #desc: Asignamos el código FUC del comercio (Obligatorio)
    #param: fuc: Código fuc que nos envía payu
    public function apykey($key=''){
        if(strlen(trim($key)) > 0){
            $this->_apikey = $key;
        }
        else{
            throw new Exception('Falta agregar el api key');
        }
    }#-#merchantID

    #-###########################
    #desc: Asignamos el código FUC del comercio (Obligatorio)
    #param: fuc: Código fuc que nos envía payu
    public function apilogin($key=''){
        if(strlen(trim($key)) > 0){
            $this->_apilogin = $key;
        }
        else{
            throw new Exception('Falta agregar el apilogin');
        }
    }#-#merchantID

    #-###########################
    #desc: Asignamos el código FUC del comercio (Obligatorio)
    #param: fuc: Código fuc que nos envía payu
    public function publickey($key=''){
        if(strlen(trim($key)) > 0){
            $this->_publickey = $key;
        }
        else{
            throw new Exception('Falta agregar el publickey');
        }
    }#-#merchantID


    #-################################
    #desc: Si se envía será utilizado como URLOK ignorando el configurado en el módulo de administración en caso de tenerlo (Opcional).
    #param: url: Url donde mostrar un mensaje si se realizo correctamente el pago
    public function url_ok($url=''){
        $this->_setUrlOk = $url;
    }#-#url_ok()

    #-################################
    #desc: Si se envía será utilizado como URLKO ignorando el configurado en el módulo de administración en caso de tenerlo (Opcional).
    #param: url: Url donde mostrar un mensaje si se produjo un problema al realizar el pago
    public function url_ko($url=''){
        $this->_setUrlKo = $url;
    }#-#url_ko()

    #-###############################
    #desc: Clave proporcionada por el banco (Obligatorio)
    #param: clave: Clave asignada
    public function clave($clave=''){
        if(strlen(trim($clave)) > 0){
            $this->_setClave = $clave;
        }
        else{
            throw new Exception('Falta agregar la clave proporcionada por payu');
        }
    }#-#clave()

    #-################################
    # desc: Tipo de pago [T = Pago con Tarjeta, R = Pago por Transferencia, D = Domiciliacion]
    #       por defecto es T
    public function methods($metodo='T'){
        $this->_setMethods= $metodo;
    }#-#methods()


    #-################################
    #desc: Firma que se le envía a payu (Obligatorio)
    public function firma(){

        //$mensaje = $this->_amount0 . $this->_referenceCode . $this->_merchantID . $this->_currency . $this->_setTransactionType . $this->_setUrlNotificacion . $this->_setClave;

        $firma = implode('~',[$this->_apikey,$this->_merchantID,$this->_referenceCode,$this->_amount,$this->_currency]);

        if(strlen(trim($firma)) > 0){
            // Cálculo del SHA1
            
            //$this->_setFirma = 'be2f083cb3391c84fdf5fd6176801278';
    		$this->_setFirma = md5($firma);
        }
        else{
            throw new Exception('Falta agregar la firma, Obligatorio');
        }
    }#-#firma()

    /**
     * Asignar el nombre del formulario
     * @param string nombre Nombre y ID del formulario
     */

    public function set_nameform($nombre = 'servired_form')
    {
        $this->_setNameForm = $nombre;
    }

    /**
    * Generar boton submit
    * @param string nombre Nombre y ID del botón submit
    * @param string texto Texto que se mostrara en el botón
    */

    public function submit($nombre = 'submitpayu',$texto='Enviar')
    {
        if(strlen(trim($nombre))==0)
            throw new Exception('Asigne nombre al boton submit');

        $btnsubmit = '<input type="submit" name="'.$nombre.'" id="'.$nombre.'" value="'.$texto.'" />';
        $this->_setSubmit = $btnsubmit;
    }

    /**
    * Ejecutar la redirección hacia el tpv
    */

    public function ejecutarRedireccion() {
    	echo $this->create_form();
	    echo '<script>document.forms["'.$this->_setNameForm.'"].submit();</script>';
    }


    #-##########################
    #des: Generamos el form a incluir en nuestro html
    public function create_form(){

        $formulario='
        <form action="'.$this->_setEntorno.'" method="post" id="'.$this->_setNameForm.'" name="'.$this->_setNameForm.'">
            <input type="hidden" name="responseUrl" value="https://nlgames.net/payu/response" />
            <input type="hidden" name="confirmationUrl" value="https://nlgames.net/payu/confirmation" />
            <input type="hidden" name="merchantId" value="'.$this->_merchantID.'" />
            <input type="hidden" name="accountId" value="'.$this->_accountID.'" />
            <input type="hidden" name="ApiKey" value="'.$this->_apikey.'" />
            <input type="hidden" name="referenceCode" value="'.$this->_referenceCode.'" />
            <input type="hidden" name="description" value="'.$this->_setProductoDescripcion.'" />
            <input type="hidden" name="amount" value="'.$this->_amount.'" />
            <input type="hidden" name="tax" value="0" />
            <input type="hidden" name="taxReturnBase" value="0" />            
            <input type="hidden" name="currency" value="'.$this->_currency.'" />
            <input type="hidden" name="signature" value="'.$this->_setFirma.'" />
            <input type="hidden" name="buyerEmail" value="' . session('email') . '" />
        ';

        /*
        $formulario='
        <form action="'.$this->_setEntorno.'" method="post" id="'.$this->_setNameForm.'" name="'.$this->_setNameForm.'">
            <input type="hidden" name="responseUrl" value="http://dev-009.nlgames.net/payu/response" >
            <input type="hidden" name="confirmationUrl" value="http://dev-009.nlgames.net/payu/confirmation" >        
            <input type="hidden" name="merchantId" value="500238" />
            <input type="hidden" name="accountId" value="509171" />
            <input type="hidden" name="ApiKey" value="6u39nqhq8ftd0hlvnjfs66eh8c" />
            <input type="hidden" name="referenceCode" value="'.$this->_referenceCode.'" />
            <input type="hidden" name="description" value="Test PAYU" />
            <input type="hidden" name="amount" value="'.$this->_amount.'" />
            <input type="hidden" name="tax" value="0" />
            <input type="hidden" name="taxReturnBase" value="0" />            
            <input type="hidden" name="currency" value="ARS" />
            <input type="hidden" name="signature" value="'.$this->_setFirma.'" />
            <input type="hidden" name="test" value="1" />
            <input type="hidden" name="buyerEmail" value="test@test.com" />
        ';
        */

        //dd($formulario);
        $formulario.=$this->_setSubmit;
        $formulario.='
        </form>
        ';
        return $formulario;
    }#-#create_form()

    private function parseFloat($ptString)
    {
    	if (strlen($ptString) == 0) {
    		return false;
    	}
    	$pString = str_replace(" ", "", $ptString);
    	if (substr_count($pString, ",") > 1)
    	$pString = str_replace(",", "", $pString);
    	if (substr_count($pString, ".") > 1)
    	$pString = str_replace(".", "", $pString);
    	$pregResult = array();
    	$commaset = strpos($pString,',');
    	if ($commaset === false) {
    		$commaset = -1;
    	}
    	$pointset = strpos($pString,'.');
    	if ($pointset === false) {
    		$pointset = -1;
    	}
    	$pregResultA = array();
    	$pregResultB = array();
    	if ($pointset < $commaset) {
    		preg_match('#(([-]?[0-9]+(\.[0-9])?)+(,[0-9]+)?)#', $pString, $pregResultA);
    	}
    	preg_match('#(([-]?[0-9]+(,[0-9])?)+(\.[0-9]+)?)#', $pString, $pregResultB);
    	if ((isset($pregResultA[0]) && (!isset($pregResultB[0])
    	|| strstr($preResultA[0],$pregResultB[0]) == 0
    	|| !$pointset))) {
    		$numberString = $pregResultA[0];
    		$numberString = str_replace('.','',$numberString);
    		$numberString = str_replace(',','.',$numberString);
    	}
    	elseif (isset($pregResultB[0]) && (!isset($pregResultA[0])
    	|| strstr($pregResultB[0],$preResultA[0]) == 0
    	|| !$commaset)) {
    		$numberString = $pregResultB[0];
    		$numberString = str_replace(',','',$numberString);
    	}
    	else {
    		return false;
    	}
    	$result = (float)$numberString;
    	return $result;
    }
}

?>
