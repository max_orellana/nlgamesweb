<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--meta property="og:title" content="Newline New MOBA - Historia" />
    <meta property="og:site_name" content="Newline New MOBA - Historia"/>
    <meta property="og:description" content="" />
    <meta property="og:image" content="/assets/009/story/img/facebook-share.jpg" /-->
    <link rel="shortcut icon" type="image/x-icon" href="<?php print site_asset('favicon.ico');?>">

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <title>Newline - Story</title>

    <style type="text/css">

        *,:after,:before{box-sizing:inherit}html{box-sizing:border-box;width:100%;height:100%;overflow:hidden}body{overflow:hidden;width:100%;height:100%;background:#000;color:#c2c2b6;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}@media (min-width:768px){body{font-size:14px;}}.u-h1,.u-h2,.u-h3,.u-h4,.u-h5,.u-h6,h1,h2,h3,h4,h5,h6{font-family:beaufort;font-variant:small-caps;color:#dfdfd8;margin:0 0 25px;line-height:1;font-weight:400}.u-el .u-h1,.u-el .u-h2,.u-el .u-h3,.u-el .u-h4,.u-el .u-h5,.u-el .u-h6,.u-el h1,.u-el h2,.u-el h3,.u-el h4,.u-el h5,.u-el h6{font-variant:normal}.u-h1,h1{font-size:1.71429em}@media (min-width:768px){.u-h1,h1{font-size:2.28571em}}.u-h2,h2{font-size:1.57143em}@media (min-width:768px){.u-h2,h2{font-size:2em}}.u-h3,h3{font-size:1.42857em}@media (min-width:768px){.u-h3,h3{font-size:1.71429em}}.u-h4,h4{font-size:1.28571em}@media (min-width:768px){.u-h4,h4{font-size:1.5em}}.u-h5,h5{font-size:1em}.u-h6,h6{font-size:.85714em}dl,ol,p,ul{margin:0 0 25px}a,button{color:#e3d19a;text-decoration:none}a:active,a:focus,a:hover,button:active,button:focus,button:hover{outline:0;text-decoration:none}img{vertical-align:middle;margin:0 0 25px}hr{margin:0 0 25px;border:solid #3a3a3a;border-width:0 0 1px}.o-wrap{overflow:hidden;width:100%;height:100%}.o-container{max-width:1280px;margin:0 auto;padding:0 20px}.o-fullscreen{display:table;width:100%;height:100%;padding:25px 25px 75px}@media (min-width:1024px){.o-fullscreen{padding:40px 40px 120px}}.o-fullscreen__content{display:table-cell;max-width:1000px;vertical-align:middle;position:relative;z-index:251}.o-overlay{overflow:auto;width:100%;background:#000;position:fixed;top:40px;left:0;bottom:0;z-index:500}.o-overlay:after{content:'';pointer-events:none;display:block;width:100%;height:50px;position:fixed;bottom:0;left:0;z-index:0;background:-webkit-linear-gradient(top,transparent 0,#000 75%);background:linear-gradient(to bottom,transparent 0,#000 75%)}@media (min-height:768px){.o-overlay:after{height:250px}}.o-overlay__container{max-width:1000px;margin:auto;padding:0 15px 50px;position:relative}@media (min-height:768px){.o-overlay__container{padding-bottom:125px}}.o-overlay__body{overflow:auto;overflow-x:hidden;height:100%;position:relative}.c-overlay__footer{display:none;width:100%;text-align:center;position:absolute;bottom:0;left:0;z-index:250}@media (min-height:768px){.c-overlay__footer{display:block}}.c-section__footer{width:100%;text-align:center;position:fixed;bottom:0;left:0}.c-section__footer-block{display:inline-block;margin:-10px 10px 10px}@media (min-width:768px){.c-section__footer-block{margin-bottom:20px}}.c-section__footer-title{margin-bottom:20px}.c-options{margin:10px;position:fixed;z-index:1000}@media (min-width:1024px){.c-options{margin:25px}}@media (min-width:1200px){.c-options{margin:40px}}.c-options--absolute{position:absolute}.c-options--top{top:40px}.c-options--right{right:0}.c-options--bottom{bottom:0}.c-options--left{left:0}.c-options--on-top{z-index:5000}.c-options__list-item{display:block;margin-bottom:6px}@media (min-width:768px){.c-options__list-item{margin-bottom:13px}}.c-intro{text-align:center;background:url(/assets/009/story/img/landing-overlay-bg.jpg) center bottom no-repeat #000;background-size:cover}.c-intro__content{margin:auto;max-width:680px}.c-intro__img{margin-bottom:-20px;max-width:50%;height:auto}@media (min-width:768px){.c-intro__img{max-width:20vw}}.c-intro__controls{margin-bottom:25px}.c-intro__controls-item{padding:6px 0}@media (min-width:768px){.c-intro__controls-item{padding:0 10px;display:inline-block}}.c-intro__title{font-size:2.5em;color:#e3d19a}@media (min-width:1024px){.c-intro__title{font-size:3em}}.c-intro__subtitle{font-variant:small-caps;text-transform:lowercase;display:block;color:#dfdfd8;font-size:.61905em}.u-el .c-intro__subtitle{font-variant:normal;text-transform:none}.c-scene__act{float:left;position:relative}.c-scene__act-layer{display:block;width:100%;height:100%;position:absolute;top:0;left:0}.c-scene__act-layer--mask{opacity:0}.c-scene__act-layer--blur{-webkit-filter:blur(2px) brightness(.9);filter:blur(2px) brightness(.9);-webkit-transition:.4s;transition:.4s;will-change:filter}.c-scene__act-layer--blur.is-focused{-webkit-filter:blur(0) brightness(1);filter:blur(0) brightness(1)}.c-world{overflow:scroll;cursor:all-scroll;width:100%;position:absolute;top:40px;right:0;bottom:0;left:0}.c-world__acts{position:relative;overflow:hidden}.c-world__acts-poi{width:100%;height:100%;position:absolute;top:0;left:0}.c-world__acts-wrap{height:100%}.c-world__scene{position:relative}.c-world__body{display:inline-block;height:100%;position:relative}.c-world__background{height:100%}.c-world__layer{width:100%;height:100%;margin:0;position:absolute;top:0;left:0}.c-act-controls__block{margin:0 10px;position:fixed;top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}@media (min-width:1024px){.c-act-controls__block{margin:0 25px}}@media (min-width:1200px){.c-act-controls__block{margin:0 40px}}.c-act-controls__block--left{text-align:left;left:0}.c-act-controls__block--right{text-align:right;right:0}.c-act-controls__block-link{border:0;background:0 0;text-decoration:none}.c-act-controls__block-link:active,.c-act-controls__block-link:focus,.c-act-controls__block-link:hover{text-decoration:none}.c-act-controls__block-icon{display:inline-block}.c-act-controls__block--right .c-act-controls__block-icon{margin-left:13px}.c-act-controls__block--left .c-act-controls__block-icon{margin-right:13px}.c-act-controls__block-title{text-transform:uppercase;display:inline-block;vertical-align:middle;margin:0;font-size:18px;max-width:200px}.u-el .c-act-controls__block-title{text-transform:none}div#riotbar-alerts{position:fixed!important;top:-100%!important;z-index:10000!important}.has-cookie-alert div#riotbar-alerts{top:0!important}div#riotbar-bar{position:fixed!important;top:0;z-index:10000!important}div#riotbar-subbar{position:fixed!important;top:40px;z-index:9999!important}body.riotbar-present{margin:0!important;padding:40px 0 0!important}body.has-cookie-alert{padding:87px 0 0!important}.u-hidden{display:none}@media screen{.u-hidden-screen{display:none}}@media print{.u-hidden-print{display:none}}@media (-webkit-min-device-pixel-ratio:2),(min-resolution:192dpi){.u-hidden-high-res{display:none}}@media (min-width:480px){.u-hidden-xs{display:none}}@media (max-width:479px){.u-hidden-less-than-xs{display:none}}@media screen and (min-width:480px) and (max-width:767px){.u-hidden-xs-only{display:none}}@media (min-width:768px){.u-hidden-sm{display:none}}@media (max-width:767px){.u-hidden-less-than-sm{display:none}}@media screen and (min-width:768px) and (max-width:1023px){.u-hidden-sm-only{display:none}}@media (min-width:1024px){.u-hidden-md{display:none}}@media (max-width:1023px){.u-hidden-less-than-md{display:none}}@media screen and (min-width:1024px) and (max-width:1199px){.u-hidden-md-only{display:none}}@media (min-width:1200px){.u-hidden-lg{display:none}}@media (max-width:1199px){.u-hidden-less-than-lg{display:none}}@media screen and (min-width:1200px) and (max-width:1499px){.u-hidden-lg-only{display:none}}@media (min-width:1500px){.u-hidden-xl{display:none}}@media (max-width:1499px){.u-hidden-less-than-xl{display:none}}@media (min-height:480px){.u-hidden-short{display:none}}@media (max-height:479px){.u-hidden-less-than-short{display:none}}@media screen and (min-height:480px) and (max-height:767px){.u-hidden-short-only{display:none}}@media (min-height:768px){.u-hidden-medium{display:none}}@media (max-height:767px){.u-hidden-less-than-medium{display:none}}@media screen and (min-height:768px) and (max-height:1023px){.u-hidden-medium-only{display:none}}@media (min-height:1024px){.u-hidden-tall{display:none}}@media (max-height:1023px){.u-hidden-less-than-tall{display:none}}.u-sr-only{overflow:hidden;clip:rect(1px,1px,1px,1px);height:1px;width:1px;padding:0;border:0;position:absolute}

        .c-preloader {
            display: table;
            vertical-align: middle;
            width:  100%;
            height: 100%;
            margin: 0;
            background: #000;
            text-align: center;
            position: fixed;
            top:    0;
            left:   0;
            z-index: 1000000;
        }
        .c-preloader__body {
            display: table-cell;
            vertical-align: middle;
        }
        .c-preloader__loader {
            position: relative;
            margin: 0;
            width: 200px;
        }
        .c-preloader .fallback {
            display: block;
            vertical-align: middle;
            width: 100%;
            color: white;
            font-style: normal;
            font-size:23px;
            position: absolute;
            top: 50%;
        }
        .c-preloader__percent {
            font-family: serif;
            display: block;
            color: #b9b9a8;
            font-size: 32px;
        }
        .c-preloader__metric {
            font-size: 21px;
            position: relative;
            top: -.25em;
        }
        @media (min-width: 600px) {
            .c-preloader__loader {
                width: auto;
                margin: -100px 0 0;
            }
        }

    </style>

    <link rel="stylesheet" href="<?php print site_asset('/css/perfect-scrollbar.css');?>">
    <script type="text/javascript" src="<?php print site_url('/min/?g=js.story&v=' . VERSION);?>"></script>  

    <script>
        // Get asset path
        var assetsPath =  "/assets/009/story";
    </script>

  </head>
  <body class="u-es i18n-es">
    <?php include SP . 'app/views/shared/analytics.php';?>    
    <div class="ps-wrapper ps-body">    
        <section class="all-content">
            <div class="header-wrapper">
                <?php include SP . 'app/views/shared/header.php';?>
            </div>         
        </section>

        <div class="o-wrap">
            <?php echo $content;?>
        </div>
    </div>

    <script type="text/javascript">
    //init
    var loaderAnimation = $(".js-preloader").LoaderAnimation({
        onComplete:function(){
            console.log("preloader animation completed!");
        }
    });
    $.html5Loader({
            filesToLoad: assetsPath + '/files.json',
            assetsPath: assetsPath,
            onComplete: function () {
                console.log("All the assets are loaded!");
            },
            onUpdate: loaderAnimation.update
    });
    </script>
    <script src="<?php print site_url('/assets/009/js/TweenMax.min.js');?>"></script>
    <script src="<?php print site_url('/assets/009/js/story.js');?>"></script>
  </body>
</html>        