<!DOCTYPE html>
<html class="no-js" lang="en-US">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width" />
        <title>Newline - Heroes</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php print site_asset('favicon.ico');?>">
        <link rel="stylesheet" href="<?php print site_url('/min/?g=css.heroe&v=' . VERSION);?>">
        <script type="text/javascript" src="<?php print site_url('/min/?g=js.heroe&v=' . VERSION);?>"></script>
    </head>
    <div class="ps-wrapper ps-body container-height container-hero">
      <div id="classes-prev-next" class="no-select">
        <div class="table prev-next" style="width:75px; position: absolute; top: 0; left: 0;">
          <div class="table-row">
            <div class="table-cell text-center"> <span class="prev skill-prev-next prev-next-link skill-prev-next-disabled"></span> </div>
          </div>
        </div>
        <div class="table prev-next" style="width:120px; position: absolute; top: 0; right: 0;">
          <div class="table-row">
            <div class="table-cell text-center"> <span class="next skill-prev-next prev-next-link skill-prev-next-disabled"></span> </div>
          </div>
        </div>
      </div>

      <?php echo $content;?>
	</div>
</html>