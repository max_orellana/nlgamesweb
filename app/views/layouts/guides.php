<!DOCTYPE html>
<html class="no-js" lang="en-US">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width" />
        <title>Newline - Guides</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php print site_asset('favicon.ico');?>">
        <link rel="stylesheet" href="<?php print site_url('/min/?g=css.guides&v=' . VERSION);?>">
        <script type="text/javascript" src="<?php print site_url('/min/?g=js.guides&v=' . VERSION);?>"></script>
    </head>
    <body>
        <?php include SP . 'app/views/shared/analytics.php';?>    
        <?php include SP . 'app/views/shared/social.php';?>        
        <div class="ps-wrapper ps-body container-height">
            <section class="all-content">
                <?php include SP . 'app/views/shared/header.php';?>
                <div class="container">
                    <div class="row">
                        <div class="logo"></div>
                    </div>
                </div>            
                <div class="container">
                    <div class="row">
                        <?php include SP . 'app/views/guides/navbar.php';?>
                        <?php echo $content;?>
                    </div>
                    <?php // include SP . 'app/views/shared/footer.php';?>
                </div>  
    		</section>
    	</div>
    </body>
</html>