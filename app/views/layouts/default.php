<!DOCTYPE html>
<html class="no-js" lang="en-US">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width" />
        <title>Newline - L2 Moba</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php print site_asset('favicon.ico');?>">
        <link rel="stylesheet" href="/min/?g=css.default&v=<?php print VERSION;?>">
        <script type="text/javascript" src="/min/?g=js.default&v=<?php print VERSION;?>"></script>     
    </head>
    <body>
        <?php include SP . 'app/views/shared/analytics.php';?>    
        <?php include SP . 'app/views/shared/social.php';?>
        <div class="ps-wrapper ps-body">
            <section class="all-content">
                <?php include SP . 'app/views/shared/header.php';?>
                <div class="container">
                    <div class="row">
                        <div class="logo"></div>
                    </div>
                </div>            
            <?php if(session('group') == 'admin'):?>
                <?php include SP . 'app/views/panel/aside-admin.php';?>
            <?php endif;?>
                <div class="container">
                    <div class="row">
                        <?php include SP . 'app/views/shared/navbar.php';?>
                        <?php echo $content;?>
                    </div>
                </div>  
    		</section>
            <?php //include SP . 'app/views/shared/footer.php';?>
        </div>
    </body>
</html>