<!doctype html>
<html ng-app="website">
	<head>
		<meta charset="utf-8">
		<title>Newline - Panel</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	    <link rel="shortcut icon" type="image/x-icon" href="<?php print site_asset('favicon.ico');?>">
        <link rel="stylesheet" href="<?php print site_url('/min/?g=css.panel&v=' . VERSION);?>">
        <script type="text/javascript" src="<?php print site_url('/assets/ckeditor/ckeditor.js');?>"></script>  
        <script type="text/javascript" src="<?php print site_url('/min/?g=js.panel&v=' . VERSION);?>"></script>  
        <script type="text/javascript" src="<?php print site_url('/assets/ckeditor/samples/js/sample.js');?>"></script>  

	</head>
	<body>
	    <?php include SP . 'app/views/shared/analytics.php';?>    
		<div class="ps-wrapper ps-body">
			<?php include SP . 'app/views/shared/header.php';?>
			<?php include SP . 'app/views/panel/aside-user.php';?>
			<?php print $content;?>
		</div>
	</body>
</html>
