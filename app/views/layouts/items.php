<!DOCTYPE html>
<html class="no-js" lang="en-US">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width" />
        <title>Newline - Items</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php print site_asset('favicon.ico');?>">
        <link rel="stylesheet" href="<?php print site_url('/min/?g=css.items&v=' . VERSION);?>">
        <script type="text/javascript" src="<?php print site_url('/min/?g=js.items&v=' . VERSION);?>"></script>   
    </head>
    <div class="ps-wrapper ps-body">
        <?php include SP . 'app/views/shared/header.php';?>
        <?php echo $content;?>
	</div>
</html>