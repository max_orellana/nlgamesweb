<!DOCTYPE html>
<html class="no-js" lang="en-US">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width" />
        <title>Newline - Mobile</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php print site_asset('favicon.ico');?>">
        <link rel="stylesheet" href="<?php print site_url('/assets/009/css/bootstrap.min.css');?>">
        <link rel="stylesheet" href="<?php print site_url('/assets/009/css/mobile.css');?>">
        <link rel="stylesheet" href="<?php print site_url('/assets/009/css/spiegel.css');?>">      
    </head>
    <body>

        <div class="container">     
            <div class="col-md-12 col-xs-12 text-center">  
                <img src="<?php print site_url('/assets/mobile/img/logo.png');?>" class="logo" />
                <div class="message">
                    <div class="sup"></div>
                    <div class="cornered"></div>
                    <h2>Comunidad:</h2>
                    <p> Actualmente nos encontramos trabajando sobre el responsive mobil de todos nuestros sitios. Lamentamos informarles que hasta no tener este trabajo terminado, solo se podra acceder a todos nuestros sitios desde notebooks o ordenadores de escritorio.</p> 
                    <h5>Disculpas por las molestias ocasionadas.</h5>
                </div>
                <div class="append">
                    <div class="link"><a href="<?php print config()->forum_url;?>">Viajar al foro</a></div>
                    <div class="keys">Betakeys entregadas hasta el momento: <span>0</span></div>
                </div>
            </div>
        </div>
    </body>
</html>