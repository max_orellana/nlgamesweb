<!DOCTYPE html>
<html class="no-js" lang="en-US">
    
<!-- Mirrored from bless-source.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Feb 2015 18:39:21 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width" />
        <title>Newline New MOBA</title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="shortcut icon" type="image/x-icon" href="<?php print site_asset('favicon.ico');?>">
        <link rel="stylesheet" href="<?php print site_url('/min/?g=css.plain&v=' . VERSION);?>">
        <script type="text/javascript" src="<?php print site_url('/min/?g=js.plain&v=' . VERSION);?>"></script>  
    </head>
    <div class="ps_wrapper ps_body">
        <section class="all-content">
            <?php include SP . 'app/views/shared/header.php';?>
            <div class="container">
                <div class="row">
                    <?php echo $content;?>
                </div>            
                <?php include SP . 'app/views/shared/footer.php';?>
            </div>  
        </section>
    </div>
</html>           