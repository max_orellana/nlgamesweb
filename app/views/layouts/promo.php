<!DOCTYPE html>
<html class="no-js" lang="en-US">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width" />
        <title>Newline - L2 Moba</title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="shortcut icon" type="image/x-icon" href="<?php print site_asset('favicon.ico');?>">
        <!--link rel="stylesheet" href="<?php print site_url('/min/?g=css.promo&v=' . VERSION);?>"-->
        <link rel="stylesheet" href="<?php print site_asset('/css/perfect-scrollbar.css');?>">
        <script type="text/javascript" src="<?php print site_url('/min/?g=js.promo&v=' . VERSION);?>"></script>
    <style>

        html, body {overflow: hidden;}

        .c-preloader {
            display: table;
            vertical-align: middle;
            width:  100%;
            height: 100%;
            margin: 0;
            background: #000;
            text-align: center;
            position: fixed;
            top:    0;
            left:   0;
            z-index: 1000000;
        }
        .c-preloader__body {
            display: table-cell;
            vertical-align: middle;
        }
        .c-preloader__loader {
            position: relative;
            margin: 0;
            width: 200px;
        }
        .c-preloader .fallback {
            display: block;
            vertical-align: middle;
            width: 100%;
            color: white;
            font-style: normal;
            font-size:23px;
            position: absolute;
            top: 50%;
        }
        .c-preloader__percent {
            display: none;
            font-family: serif;
            display: block;
            color: #b9b9a8;
            font-size: 32px;
        }
        .c-preloader__metric {
            font-size: 21px;
            position: relative;
            top: -.25em;
        }
        @media (min-width: 600px) {
            .c-preloader__loader {
                width: auto;
                margin: -100px 0 0;
            }
        }

        </style>        
    </head>
    <body>
        <?php include SP . 'app/views/shared/analytics.php';?>    
        <div class="c-preloader js-preloader">
            <div class="c-preloader__body">
                <span class="c-preloader__percent"><span class="js-preloader-percent">0</span><span class="c-preloader__metric">%</span></span>
            </div>
        </div>

        <div class="ps-wrapper ps-body" style="display:none">
            <?php include SP . 'app/views/shared/header.php';?>
            <?php echo $content;?>
    	</div>
    </body>
</html>


<script>
    // Get asset path
    var assetsPath =  "/assets/009";
</script>

<script type="text/javascript">

//init
var loaderAnimation = $(".js-preloader").LoaderAnimation({
    onComplete:function(){
        //console.log("preloader animation completed!");
        $('.ps-body').fadeIn();
        $('.ps-body').perfectScrollbar('update');
        if(location.hash){ 
            $('.ps-body').scrollTop($(location.hash).offset().top + $('.ps-body').scrollTop() - 44).perfectScrollbar('update');
        }
    }
});
$.html5Loader({
        filesToLoad: assetsPath + '/promo/files.json',
        assetsPath: assetsPath,
        onComplete: function () {
            //console.log("All the assets are loaded!");
        },
        onUpdate: loaderAnimation.update
});
</script>