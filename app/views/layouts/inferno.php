<!DOCTYPE html>
<html class="no-js" lang="en-US">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width" />
        <title>Newline - Classic Arena</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php print site_asset('favicon.ico');?>">
        <link rel="stylesheet" href="<?php print site_url('/min/?g=css.inferno&v=' . VERSION);?>">
        <script type="text/javascript" src="<?php print site_url('/min/?g=js.inferno&v=' . VERSION);?>"></script>   
        <?php print isset($_GET['orig'])?'<style>.content-height{box-shadow: inset 0 0 150px #000, inset 0 0 180px #000;}.content-height {height:100%!important}</style>':'';?>
    </head>
    <div class="ps-wrapper <?php print isset($_GET['orig'])?'content-height':'ps-body';?>">
        <?php if(!isset($_GET['orig'])):?>
        <?php include SP . 'app/views/shared/header.php';?>
        <?php endif;?>
        <?php if(isset($_GET['orig'])):?><div class="darkfade content-height"><?php endif;?>
        <?php echo $content;?>
        <?php if(isset($_GET['orig'])):?></div><?php endif;?>
	</div>
</html>