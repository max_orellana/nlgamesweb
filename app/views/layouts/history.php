<!DOCTYPE html>
<!--[if IE 8]>            <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="o-fullscreen no-js" lang="<?php print LOCALE;?>"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
        <link rel="shortcut icon" type="image/x-icon" href="<?php print site_asset('favicon.ico');?>">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Newline - Story</title>
        <link rel="stylesheet" href="<?php print site_url('/min/?g=css.history&v=' . VERSION);?>">


        <style type="text/css">
            *,:after,:before{box-sizing:inherit}html{box-sizing:border-box}body{background:#000;color:#fff;font-family:beaufort;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}@media (min-width:1500px){body{padding-top:0;}}canvas{display:block}.o-fullscreen{height:100%;width:100%}@media (min-width:768px){.o-fullscreen\@sm{height:100%;width:100%}}.o-wrap{overflow:hidden}.c-preloader{display:none;text-align:center;color:#ba9a6f;font-size:100px;font-style:italic;font-family:spiegel;position:fixed;top:0;left:0;z-index:100}@media (min-width:768px){.c-preloader{display:block}}.c-preloader__progress{text-align:center;position:relative}.c-preloader__graphic{display:inline-block;vertical-align:middle;margin:0 20px}.c-preloader__progress-bar{display:inline-block;vertical-align:middle;width:40%;height:1px;-webkit-transition:.3s;transition:.3s}.c-preloader__progress-bar--left{background:-webkit-linear-gradient(right,#6a6353 0,rgba(106,99,83,0) 100%);background:linear-gradient(to left,#6a6353 0,rgba(106,99,83,0) 100%);-webkit-transform-origin:right center;-ms-transform-origin:right center;transform-origin:right center}.c-preloader__progress-bar--right{background:-webkit-linear-gradient(left,#6a6353 0,rgba(106,99,83,0) 100%);background:linear-gradient(to right,#6a6353 0,rgba(106,99,83,0) 100%);-webkit-transform-origin:left center;-ms-transform-origin:left center;transform-origin:left center}
        </style>

        <script type="text/javascript" src="<?php print site_url('/min/?g=js.default&v=' . VERSION);?>"></script>  

        <script>
            var base = document.createElement('a');
            base.href = location.href.replace(/\#.*$/, '');
            var ASSETS = '/assets/';
        </script>

    </head>

    <body class="o-fullscreen@sm i18n-es">
        <div id="main" class="website-wrapper">
            <div class="header-wrapper">
                <?php include SP . 'app/views/shared/header.php';?>
            </div>         
        </div>
       <?php echo $content;?>
    </body>
</html>
