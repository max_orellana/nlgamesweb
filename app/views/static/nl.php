<div class="col-md-12 col-xs-12 content text-shadow border-radius">
  <div class="col-md-12 col-xs-12 single">
    <div class="col-md-12 col-xs-12 no-pad light-gold main-title">¿Qué es Newline?</div>
      <div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
        <span class="gold">Redline</span> | 28 de Diciembre del 2014
      </div>
      <div class="col-md-12 col-xs-12 text">
        <div class="group-control">&nbsp;</div>
        <div class="text-center">
          <img src="<?php echo site_asset('img/nl.png');?>">
        </div>
        <div class="group-control">&nbsp;</div>
        <p>Newline es un juego en línea competitivo de estilo MOBA que combina, fusiona y explota todas las habilidades/mecánicas individuales de cada jugador, con las estrategias sinérgicas de todo el equipo. En cada partida se irán enfrentando cara a cara dos equipos con héroes únicos respecto a sus roles, fortalezas y desventajas. El objetivo principal del juego será sacar el máximo provecho de estos, para intentar destruir la base enemiga antes de que el rival lo haga. Para ello, los jugadores deberán aprender a dominar, sus héroes e items preferidos a la perfección.<br><br><center><strong>¿<u>Que nos trae Newline</u>?</strong><br></center>
        <br>
      La novedad que trae Newline para este año entrante es sumamente interesante. Una experiencia única e inigualable! Una combinación de lo que tanto amamos y disfrutamos: Lineage 2 y el tipo de rol “Moba”. Creamos una jugabilidad totalmente nueva y la adaptamos a la crónica Ertheia, con una plataforma distinta (basada en JV), lo que nos permitió crear un sistema totalmente divertido y dinámico para todos nuestros usuarios. Gracias a esto, también hemos logrado solucionar los problemas más conocidos de este antiguo juego. Lo explicaremos a continuación: <br> <br>1- Inestabilidad por Estabilidad:<br>
    Basta ya de servidores privados inestables que terminan cerrando de un día para el otro sin importarles nada. Los motivos de estas caidas como bien sabemos, suelen ser varios. Desde la caida bruzca de login final, hasta la falta de ingresos para la mantención, o bien falta de experiencia para brindar un servicio digno. En Newline, tenemos la confianza y la seguridad de saber que este no será ningún problema, ya que contamos con todo lo necesario para ofrecerle a nusetra comunidad una estabilidad plena, como se lo merece.  <br><br>
    2- Dedicacion obligatoria por dedicacion opcional:<br>
    Sabemos en detalle lo tedioso que se torna volver a empezar en un servidor nuevo desde cero una, y otra, y otra vez. Sobre todo porque la carga horaria que requería anteriormente este juego (como la mayoría de todos los MMorpg) era demasiado alta. Farmear/levelear se volvía cada vez más pesado y monótono con el correr del tiempo, pero por suerte esto se acabó!
Hoy en día los usuarios podrán dedicarle el tiempo que quieran o puedan a jugar, sabiendo que gracias a nuestro nuevo sistema de emparejamiento (un sistema totalmente transparente), estarán jugando siempre en igualdad de condiciones con otros usuarios de la comunidad que se encuentren a un nivel similar al suyo. 
<br><br>3- Sistema injusto de recompensas por Equidad total:<br>
    En la mayoría de los servidores de Lineage 2, la contribución o donación que efectúan los usuarios, hace que se termine desbalanceando totalmente el juego debido a su sistema totalmente injusto de recompensas. La diferencia entre donadores y no donadores suele ser abrumadora, a tal punto que los usuarios se vuelven prácticamente un Dios dentro del juego. Nuestro nuevo sistema de donaciones, pasara a ser totalmente distinto. Como en la mayoría de los MOBAs, los jugadores podrán conseguir mediante su contribución ítems de apariencia visual. Gracias a esto, todos nuestros usuarios estarán en igualdad de condiciones, por lo que solo se destacaran sobre el resto, los jugadores con un alto nivel de juego. Apostaremos a su esfuerzo de principio a fin!<br><br>
    4- Cambios bruscos de estructura por una estructura constante:<br>
    Cuando un servidor se encuentra descuidado o mal proyectado, es comun que los administradores empiezen a generar cambios bruscos sobre la marcha en cuanto a su estructura (Rates, Precios, Recompensas, Etc.) Creemos que este tipo de desiciones es totalmente injusto para cualquier usuario, ya que si un jugador opta por jugar "x" servidor, probablemente sea por la estructura planteada desde un principio. Pongamos un ejemplo para ser mas claros: Supongamos que estamos buscando un servidor low rates, ya que queremos dedicarle bastante tiempo al farmeo y al juego en equipo. Si de un dia para el otro el servidor al cual jugamos pasa a subir sus rates, es totalmente entendible que nos enojemos. Ya que hemos dedicado probablemnte mucho tiempo al leveleo, mientras que un usuario nuevo, pasara a cumplir nuestros mismos objetivos en un periodo mas corto de tiempo. Injusto verdad? Asi lo creemos,
    por lo que le aseguramos que Newline mantendra su jugabilidad y estructura inicial de principio a fin! Puede jugar tranquilo con ello, su esfuerzo aquí sera valorado SIEMPRE.
    
        <br><br>
      <center>¡<u><strong>Conviértete en el mejor</strong></u><strong>!</strong><br></center>
    <br>
    </p>
            <p>Comprende a todas las clases medievales de este nuevo MOBA, a tal punto de convertirte en el mejor! Con la práctica aprenderás a desenvolverte en todas tus partidas dependiendo de los rivales que tengas en frente. Sabrás automáticamente cuando será el momento oportuno para usar tus habilidades, como también cuando será necesaria una retirada. Disfruta al máximo de esta nueva etapa! Deja todo tu esfuerzo en ella para llegar a las grandes ligas de Newline y probablemente nos encontremos en nuestros futuros torneos!

      </div>
    </div>
  </div>
</div>