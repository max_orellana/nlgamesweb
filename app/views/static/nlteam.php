<div class="col-md-12 col-xs-12 content text-shadow border-radius">
  <div class="col-md-12 col-xs-12 single">
    <div class="col-md-12 col-xs-12 no-pad light-gold main-title">Newline Team</div>
      <div class="col-md-12 col-xs-12 no-pad light-gold creator">Publicado por
        <span class="gold">Redline</span> | 28 de Diciembre del 2014
      </div>
      <div class="col-md-12 col-xs-12 text">
        <p>Somos “Newline Team” y en ese nombre llevamos toda nuestra esencia. Responsabilidad, madurez, experiencia y entre otras tantas una de las que creemos más importante “Actitud positiva ante TODO”. Esta postura impuesta en nosotros es la que nos permite superarnos individual y grupalmente día a día, ante cualquier obstáculo inesperado que pueda presentarse.</p>
        <p>Es importante destacar desde un comienzo, que nuestro equipo de trabajo tendrá siempre como objetivos principales, poder brindarle a cada uno de nuestros usuarios: Estabilidad, seguridad, atención y balance dentro del juego. Para lograr esto se reunió a un conjunto de cuarenta y tres personas, todas altamente capacitadas para cada una de las funciones necesarias. 
    <br>Te invitamos a que las conozcas a continuación:</p>
        <center><p><span style="color: #AA6919;"><u>Producer Team</u></span><br><br>
          <u>Director - Executive Producer</u>: “Redline” <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br>
          <u>Publisher</u>: “Raptor” <img src="<?php print site_asset('fl/esp.png');?>" width="20" height="13"><br>
          <u>Supervising Producer - Translator</u>: “Chuck” <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br>
		  <u>Producer</u>: “Jovirone” <img src="<?php print site_asset('fl/usa.png');?>" width="20" height="13"><br><br>
		  <center><p><span style="color: #AA6919;"><u>Game Development</u></span><br><br>
		  <u>Lead Server Programmer</u>: "Orchu" <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br>
		  <u>Server Programmer</u>: “Blackpwnd” <img src="<?php print site_asset('fl/esp.png');?>" width="20" height="13"><br>
		  <u>Game Surveillance Unit Agent - Hosting Infrastructure</u>: "MgbHard" <img src="<?php print site_asset('fl/ru.png');?>" width="20" height="13"> <br>
		  <u>Lead Game Designer</u>: “Respect” <img src="<?php print site_asset('fl/uc.png');?>" width="20" height="13"><br>
		  <u>Game Designer</u>: “deMev” <img src="<?php print site_asset('fl/uc.png');?>" width="20" height="13"><br>
          <u>Balance Leader - Test Leader</u>: "Mobb" <img src="<?php print site_asset('fl/uru.png');?>" width="20" height="13"><br>
		  <u>Game Master</u>: “Softtek” <img src="<?php print site_asset('fl/arg.png');?>"width="20" height="13"><br>
		  <u>UI Programmer</u>: “Savo” <img src="<?php print site_asset('fl/ru.png');?>" width="20" height="13"><br>
		  <u>UI Designer</u>: “Walkata” <img src="<?php print site_asset('fl/bu.png');?>" width="20" height="13"><br>
		  <u>UP Designer</u>: “MrAve” <img src="<?php print site_asset('fl/po.png');?>" width="20" height="13"><br>
		  <u>Game Tester</u>: “Hounter” <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br>
          <u>Game Tester</u>: “Ludmila” <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br>
          <u>Game Tester</u>: “Woodz” <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br>
          <u>Game Tester</u>: “Crack” <img src="<?php print site_asset('fl/uru.png');?>" width="20" height="13"><br>
          <u>Game Tester</u>: “Freeze” <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br>
          <u>Game Tester</u>: “Shygo” <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br>
          <u>Game Tester</u>: “Eomer” <img src="<?php print site_asset('fl/mex.png');?>" width="20" height="13"><br>
          <u>Game Tester</u>: “Beosted” <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br>
          <u>Game Tester</u>: “Riggle” <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br>
          <u>Game Tester</u>: “Shantao” <img src="<?php print site_asset('fl/uru.png');?>" width="20" height="13"><br>
          <u>Game Tester</u>: “Ezian” <img src="<?php print site_asset('fl/esp.png');?>" width="20" height="13"><br>
          <u>Game Tester</u>: “Hares” <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br><br>
		  <center><p><span style="color: #AA6919;"><u>Web Development</u></span><br><br>
          <u>Web Programmer - Web Analyst</u>: “Freed” <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br>
          <u>Web Designer</u>: “Haldier" <img src="<?php print site_asset('fl/mex.png');?>" width="20" height="13"><br>
		  <u>Web Designer</u>: “MrSmith" <img src="<?php print site_asset('fl/kha.png');?>" width="20" height="13"><br>
		  <u>Web Surveillance Unit Agent</u>: “Heyow" <img src="<?php print site_asset('fl/usa.png');?>" width="20" height="13"><br><br>
		  <center><p><span style="color: #AA6919;"><u>Multimedia Development</u></span><br><br>
          <u>Ilustrator</u>: “ZeitExmind ” <img src="<?php print site_asset('fl/mex.png');?>" width="20" height="13"><br>
          <u>3D Animator</u>: “BSB” <img src="<?php print site_asset('fl/usa.png');?>" width="20" height="13"><br>
		  <u>3D Animator</u>: “BioHazard” <img src="<?php print site_asset('fl/usa.png');?>" width="20" height="13"><br>
          <u>Sound Director</u>: “Rekk” <img src="<?php print site_asset('fl/esp.png');?>" width="20" height="13"><br><br>
		  <center><p><span style="color: #AA6919;"><u>Extras</u></span><br><br>
          <u>Advertiser</u>: "Cherry" <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br>
		  <u>Advertiser</u>: "Telder" <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"><br>
          <u>Actor Voice</u>: Juan Gutiérrez <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"> <br>
          <u>Actor Voice</u>: Marcelo Garay <img src="<?php print site_asset('fl/esp.png');?>" width="20" height="13"> <br>
          <u>Actor Voice</u>: Claudia Cáceres <img src="<?php print site_asset('fl/arg.png');?>" width="20" height="13"> <br>
          <u>Actor Voice</u>: Martin Friedman <img src="<?php print site_asset('fl/usa.png');?>" width="20" height="13"><br>
          <u>Actor Voice</u>:  Melina Walsh <img src="<?php print site_asset('fl/usa.png');?>" width="20" height="13"><br>
          <u>Actor Voice</u>: Gustavo Gamallo <img src="<?php print site_asset('fl/esp.png');?>" width="20" height="13"> <br>
          <u>Actor Voice</u>: Alejo Vidal <img src="<?php print site_asset('fl/uru.png');?>" width="20" height="13"> <br>
          <br>
        </p></center>
        <p>Creemos que entre todos nosotros, formaremos un servidor único e inigualable. Pero al mismo tiempo, sabemos que no sera para nada fácil...
        Cada integrante de nuestro staff, se ve obligado a dejar lo mejor de si para cada función o tarea realizada, sin darle tanta importancia al nivel de dificultad que ellas posean. Cada detalle debe importar <u>MUCHO</u>, desde la creación de un "simple post" en donde uno tratara siempre de hacerlo lo mas completo y prolijo posible, hasta los complejos altos niveles de programación y diseño. Aquí todo se piensa varias veces, luego se debate y finalmente se cumple. <u>No exíste regla mas importante que esa</u>.</p>
        <p>En nuestro grupo de trabajo hay gente mayor y también gente joven, la mayoría están avalados por uno o incluso dos títulos universitarios, todos nosotros ya hemos formado parte de otros servidores, incluso actualmente dos, son dueños de famosas comunidades, reconocidas y respetadas mundialmente hablando. (Las cuales preferimos evitar nombrar, porque que no vienen al caso). Pero a pesar de todas estas diferencias que puedan o no marcarnos, acá ninguno se siente superior a otro, porque a Newline se llego con el mismo sueño compartido, lograr lo mejor de lo mejor, destacarse en cada punto y que cada detalle mínimo, hable por si solo....</p>
        <p>Una de las cosas que también me parece interesante comentar, es que en el transcurso del armado del servidor, algunas de las personas que se iban enterando de este proyecto (Sobre todo la gente mas cercana a nosotros) nos solía repetir esta pregunta en particular una y otra vez: "Tienen como meta formar una comunidad global con el correr del tiempo?". A lo que les respondimos: "Por el momento, solo tenemos en nuestra cabeza crear uno de los mejores servidores de Lineage 2 y para terminar cumpliendo esto necesitamos de mucho esfuerzo, trabajo, dedicación y por sobre todas las cosas de mucho tiempo. De todas maneras es entendible y lógico que con el staff que hemos logrado reunir, uno se entusiasme muchísimo con la idea de una comunidad global a futuro, con el solo hecho de pensarlo. Pero conocemos bien la dimensión que esto conlleva, y para poder mantener el nivel que estamos buscando, tendríamos que pensar infinidades de cosas antes de si quiera arrancar a proyectarlo. En fin, quien sabe que nos puede deparar este futuro verdad? Pero por el momento y por un largo periodo de tiempo, solo seremos Newline, lo cual ya es muchísimo para todos nosotros".</p>
        <p>Finalmente y como para ir cerrando un poco la idea de este apartado, le resumiremos a continuación que piensa y como actúa Newline en relación a los puntos que según nosotros, tienen mas importancia como: La estabilidad, la seguridad, la atención, el balance y la calidad en cada detalle!</p><br>
        <center><p><span style="color: #FFF;"><strong><u>ESTABILIDAD</u></strong></span><br></center>
          <br>
          La palabra "Estabilidad", no figura escrita por casualidad en primer lugar. Creemos que el significado de esta palabra, termina acaparando a todas las demás de alguna, u otra forma. Encontrar estabilidad en lo que respecta a: Seguridad, atención, jugabilidad y calidad, es sin duda uno de los objetivos primordiales de Newline.
        Para alcanzar el mayor logro de estabilidad en cada uno de estos puntos, el staff elabora el mismo proceso de siempre. Analizando una situación en primer lugar, luego comentándola con el grupo, (poniéndola así en debate) y finalmente llegando a un acuerdo/solución. De esta manera, usted entiende que podrá encontrar en este servidor una estabilidad plena en lo que respecta a:
          <br>
          <br>
          -<u>Conexión</u>: Un año atrás, cuando apenas comenzaba a formarse el proyecto "Newline", el staff decidía empezar con una meticulosa investigación respecto a las conexiones de redes de todo el mundo. De esta forma, se estaría asegurando de poder brindarle a cada país del mundo, la mayor estabilidad posible en cuanto a conexión. Para mas información sobre este tema, haz <a href="http://nlgames.net/blog/Conectando-al-mundo" target="_blank">click aquí</a><br>
          <br>
          -<u>Seguridad</u>: La seguridad en Newline es tomada muy en serio. "A menos fallos, mayor estabilidad, y viceversa" comentaba hace unos días "Sensitive" y sin duda alguna, creemos que así lo es. Por eso es que se han tomado muchísimos recaudos, para brindarle una estabilidad plena en este punto hemos generado sistemas de backup en alojamientos exteriores cada 2 horas, tanto del SVG como también del SVF, modificaciones dentro del cliente (No evitables), protecciones contra diversos programas malignos y un EXCELENTE sistema de monitoreo general, para que nuestros encargados logren controlar cada movimiento realizado por los usuarios.
        <br><br>
          -<u>Atención</u>: La estabilidad en la atención es otra obligación. Cada integrante de este staff como bien saben, posee diferentes roles. Incluso hay quienes se encargan de que cada uno cumpla con los suyos. Por dicho motivo, usted debe saber que durante todo el transcurso de Newline, siempre le llegara una respuesta de este lado ante su inquietud. (Obviamente mientras usted se mantenga respetando las reglas estipuladas)<br><br>
          -<u>Jugabilidad</u>: Usted puede jugar tranquilo, Newline le promete conservar la estabilidad de juego siempre. Sabemos bien lo tedioso que es escoger un servidor por su estructura o por su estilo de manejo y que luego con el tiempo esto se vea modificado/revertido. Por eso mismo, este staff le asegura que los servidores mantendrán hasta el final la postura que hoy antes de dar comienzo, le estamos proponiendo. En ningún momento el usuario se vera afectado por cambios mayores.<br>
        </p><br>
        <center><p><span style="color: #FFF;"><strong><u>SEGURIDAD</u></strong></span><br></center>
          <br>
           Como mencionábamos anteriormente, nuestro equipo de desarrollo se viene preparando hace ya muchísimo tiempo. Sensitive y Banshe se especializaron desde el comienzo en la seguridad de todos nuestros sitios, aun cuando ni si quiera estaban corriendo, ellos ya empezaban a planificar nuestro sistema de defensa ante futuros ataques.<br>En primer lugar, se aseguraron de encontrar una empresa prestigiosa que brinde una fuerte seguridad respecto a los ataques de negación de servicio (Ataques DDoS). Uno de los ataques más frecuentes al día de la fecha. En segundo lugar, nuestros sysadmins, ingeniaron un sistema de monitoreo general para cada uno de nuestros usuarios. Logrando de esta manera, el control absoluto de cada movimiento realizado por ellos. De esta forma, ante cualquier situación anormal que se presente, nuestros expertos podrán analizar el inconveniente de manera única, llegando en un breve periodo de tiempo a la solución. Por otra parte sostificaron el sistema de backups automáticos, los cuales se activan cada dos horas (Tanto para el SVN como también para el SVF). Las copias son efectuadas en un disco secundario primordialmente, y cada cuatro días estas son enviadas a un servidor externo, en donde se almacenaran por un periodo de cuatro meses, para mayor seguridad. También se han encargado de que la información entre la base de datos y el menú/web se maneje por procedimientos remotos, creados dentro de la base para prevenir cualquier tipo de información errónea y limitando al mismo tiempo, el acceso de solo consulta, sin posibilidad alguna de recibir cualquier tipo de modificación. Los llamados procedimientos remotos son previamente verificados por un “auth server” quien corrobora que los datos enviados al stored procedure concuerden con los del cliente. Estos mismos se mandan por llamados internos.<br>
       Como última información que podríamos acercarle, le aclaramos que nuestro updater fue hecho para que corrobore todos los archivos del cliente, para prevenir de esta manera cualquier posible modificación de parte del usuario. De encontrar alguna diferencia, este descargara automáticamente los archivos originales de nuestro repositorio y reemplazara los previamente modificados. Una vez que todos los archivos sean comprobados, el updater dara el OK y permitirá la ejecución del menú externo, quien será el encargado de detectar y abrir automáticamente el cliente. De esta manera, nos aseguramos de que no exisita posibilidad alguna de abrir un "cliente de juego", sin antes haber pasado por nuestro actualizador.<br>
         <br>
         En fin, podríamos continuar nombrando muchas mas prevenciones de seguridad, pero estaríamos perdiendo parte de ella si lo hiciéramos (Los "trucos" nunca se revelan). Por todo esto y más, usted podrá jugar tranquilo en nuestros servidores, sabiendo que sus logros están dentro de nuestra propia "caja fuerte".</p><br>
        <center><p><span style="color: #FFF;"><strong><u>ATENCIÓN</u></strong></span><br></center>
          <br>
          La atención del usuario es algo que realmente a Newline le compete. Por eso, es que hemos integrado a tres personas más a nuestro staff, a titulo de GAMEMASTER dentro del juego y MODERADORES dentro del foro. Estas tres personas, serán las encargadas de atender todas las inquietudes de nuestros usuarios de la manera más eficaz posible (Iremos agregando mas personal, a medida que lo sintamos necesario). De todas maneras, para evitar que el usuario tenga que recurrir a nuestro personal, trataremos siempre de dejar toda la información necesaria, incluso más dentro de nuestra guía de juego. De esta forma las dudas serán reducidas y por ende los motivos de comunicación también. Cabe destacar, que este personal tiene terminantemente prohibido atender a los usuarios mediante el juego, solamente ingresaran para sancionar a quienes no estén cumpliendo con las reglas estipuladas. Para atender a nuestros usuarios, utilizaremos principalmente nuestro sistema de tickets mediante el panel de usuario, un simple sistema que permite hacer un seguimiento personal ante cualquier inconveniente que pueda surgirles. Por otra parte, también responderán las dudas GENERALES dentro de nuestro foro oficial en las secciones correspondientes. Los temas personales serán cerrados automáticamente, ya que no podrán ser tratados dentro del foro. Le acercaremos mas información respecto a esto en los siguientes días.<br>
        <u>Nuestros idiomas de atención serán por el momento</u>: INGLES Y ESPAÑOL.</p><br>
        <center><p><span style="color: #FFF;"><strong><u>BALANCE</u></strong></span><br></center>
          <br>
        Uno de los puntos más fuertes de Newline, sin duda alguna, es el balance general que podrá encontrar el usuario dentro del juego. Principalmente por la gran cantidad de años con experiencia que nos abalan a la mayoría de todos nosotros como usuarios, quienes intervenimos constantemente de forma directa en el balance del juego. Gracias a ello, nuestros usuarios notarán el punto óptimo no solo a nivel de “PvP & GvG” entre clases, sino también a nivel estructural (recompensas, precios, tiempos, distancias, etc). Todo esto empezara a recaer fuertemente desde el principio en el conocimiento y la habilidad que posea sobre el juego, será totalmente clave, y marcara la diferencia en todo momento. No existirán clases con mayores ventajas o desventajas a otras (Cada una tendrá siempre, su punto débil y su punto fuerte) Las hemos analizado meticulosamente para que así sea! Por ende, le aseguramos que podrá elegir su raza/clase favorita y sacar el máximo provecho de ella.
        Para mas información sobre el concepto "Balance" simplemente haz <a href="balance">click aquí</a></p>
        <br>
        <p>
        Podríamos estar hablando de nosotros por mucho tiempo más, pero seguramente a esta altura ya lo estamos aburriendo. Así que preferible ir cerrando el tema. Pero no queremos irnos sin antes dejarle en claro, que difícilmente usted encuentre un servidor con tanto trabajo, empeño, esfuerzo y por sobre todas las cosas, con un staff altamente capacitado para que acompañe a todas estas cualidades.
        Por todos los motivos que le hemos mencionado anteriormente y por muchos más que no hemos llegado a hacerlo, usted seguramente este jugando uno de los mejores servidores de Lineage 2, somos Newline y años de experiencia nos abalan para hablar de esta manera.<br>
        <center>¡Bienvenido!</center></p>
      </div>
    </div>
  </div>
</div>