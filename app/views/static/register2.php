<div class="col-md-12 login">
	<div class="row">
		<div class="col-md-5">
			<div class="row">
				<div class="group-control">&nbsp;</div>
				<div class="group-control">&nbsp;</div>
				<div class="group-control">&nbsp;</div>
				<div class="group-control">&nbsp;</div>
				<div class="group-control">&nbsp;</div>
				<div class="group-control">&nbsp;</div>
				<div class="group-control">&nbsp;</div>	
				<div class="group-control">&nbsp;</div>	
				<div class="group-control">&nbsp;</div>	
				<div class="group-control">&nbsp;</div>	

			    <div id="contentBox" class="contentBox">
			      <?php echo messages();?>
			      <form action="/register" method="post" id="registerForm" class="form form-ajax validator validate-register">
			        <div id="registerTop"></div>
			          <div id="register" class="uplay">
			            <div id="registerHeader">
			          </div>
			          <div id="registerFields" class="unselectable">
						<fieldset>
							<input name="username" class="validate-ajax-ubiname" value="" type="text" required>
							<span class="overlay"><?php echo locale('username');?></span>
							<div id="messages-username"></div>
						</fieldset>

						<fieldset>
							<input name="character" class="validate-ajax-ubiname" value="" type="text" required>
							<span class="overlay"><?php echo locale('character');?></span>
						</fieldset>

			            <fieldset>
			              <input name="email" class="validate-ajax-ubipassword send-password warning" value="" type="email" data-validation-email-message="Ingresa un correo electrónico válido" required>
			              <span class="overlay "><?php echo locale('email');?></span>
			              <div id="messages-email"></div>
			            </fieldset>

						<fieldset>
							<div class="text-center"><?php echo locale('gender');?></div>
							<select name="gender" class="chosen">
								<option value=""><?php echo locale('none');?></option>
								<option value="1"><?php echo locale('female');?></option>
								<option value="0"><?php echo locale('male');?></option>
							</select>
						</fieldset>

			            <fieldset style="height:auto!important">
			              <input id="password" name="password" class="validate-ajax-ubipassword send-password warning" value="" type="password" placeholder="<?php echo locale('password');?>" required>
			              <span class="overlay "><?php echo locale('password');?></span>
			            </fieldset>

			            <fieldset>
			              <input id="password2" name="password2" class="validate-ajax-ubipassword send-password warning" value="" type="password" placeholder="<?php echo locale('password-confirm');?>" required>
			              <span class="overlay "><?php echo locale('password');?></span>
			            </fieldset>
			            <hr>
			            <fieldset class="label">
			              <input id="beta" name="beta" value="1" class="custom-check" type="checkbox">
			              <label class="" for="beta" id="betaLabel"><?php echo locale('join-beta');?></label>
			            </fieldset>
			            <div class="beta hide">
							<fieldset>
								<div class="text-center"><?php echo locale('moba-experience');?></div>
								<select name="experience" class="chosen">
									<option value=""><?php echo locale('none');?></option>
									<option value="1"><?php echo locale('less-than-year');?></option>
									<option value="2"><?php echo locale('more-than-year');?></option>
									<option value="3"><?php echo locale('more-than-two-year');?></option>
									<option value="4"><?php echo locale('more-than-three-year');?></option>
								</select>
							</fieldset>

							<fieldset>
								<div class="text-center"><?php echo locale('favorite-role');?></div>
								<select name="role" class="chosen">
									<option value=""><?php echo locale('role-support');?></option>
									<option value="1"><?php echo locale('role-tank');?></option>
									<option value="2"><?php echo locale('role-magician');?></option>
									<option value="3"><?php echo locale('role-hunter-shooter');?></option>
								</select>
							</fieldset>

							<fieldset>
								<div class="text-center"><?php echo locale('favorite-heroe');?></div>
								<select name="heroe" class="chosen">
									<option value="1">Paladin</option>
									<option value="2">Dark Avenger</option>
									<option value="3">Warlord</option>
									<option value="4">Gladiator</option>
									<option value="5">Treasure Hunter</option>
									<option value="6">Hawkeye</option>
									<option value="7">Sorcerer</option>
									<option value="8">Necromancer</option>
									<option value="9">Warlock</option>
									<option value="10">Bishop</option>
									<option value="11">Prophet</option>
									<option value="12">Temple Knight</option>
									<option value="13">Sword Singer</option>
									<option value="14">Plains Walker</option>
									<option value="15">Silver Ranger</option>
									<option value="16">Spellsinger</option>
									<option value="17">Elemental Summoner</option>
									<option value="18">Elven Elder</option>
									<option value="19">Shillien Knight</option>
									<option value="20">Bladedancer</option>
									<option value="21">Abyss Walker</option>
									<option value="22">Phantom Ranger</option>
									<option value="23">Spellhowler</option>
									<option value="24">Phantom Summoner</option>
									<option value="25">Shillien Elder</option>
									<option value="26">Destroyer</option>
									<option value="27">Tryant</option>
									<option value="28">Overlord</option>
									<option value="29">Warcry</option>
									<option value="30">Bounty Hunter</option>
									<option value="31">Warsmith</option>
								</select>
							</fieldset>

							<fieldset style="z-index: 87;">
								<div class="text-center"><?php echo locale('week-play-hours');?></div>
								<select name="week_play_hours" class="chosen">
									<option value="1"><?php echo locale('6-or-more');?></option>
									<option value="2"><?php echo locale('14-or-more');?></option>
									<option value="3"><?php echo locale('24-or-more');?></option>
									<option value="4"><?php echo locale('36-or-more');?></option>
									<option value="5"><?php echo locale('44-or-more');?></option>
									<option value="6"><?php echo locale('56-or-more');?></option>
								</select>
							</fieldset>
			            </div>
						<hr>
			            <fieldset style="z-index: 86;" class="label">
			              <input id="terms" name="terms" value="1" class="validate-required-check errorTitle-errorTermsTitle errorText-errorTermsText custom-check" type="checkbox">
			              <label class="" for="terms" id="termsLabel"><?php echo locale('agree');?> <a href="#tos" role="button" data-toggle="modal"> <?php echo locale('tos_and_privacy');?> </a> </label>
			            </fieldset>

			          </div>
			        </div>
			        <div id="registerBottom"></div>
			        <fieldset id="playButton" class="button">
			                  <img src="<?php print $_SESSION['captcha']['image_src'];?>">
			        	<span><?php print locale('register');?></span>
			        </fieldset>
			        <input style="display: block; height: 0px; width: 0px; position: absolute; top: 0px; left: 0px; border: medium none; background: transparent none repeat scroll 0% 0%;" type="submit">
			      </form>
				</div>
			</div>
		</div>
		<div class="col-md-7"></div>
	</div>
</div>

<script>
$(function(){

	$('#beta').click(function(){
		if($(this).is(':checked')) $('.beta').removeClass('hide').hide().slideDown();
		else $('.beta').slideUp();
	});

	$('#playButton').click(function(){
		$('input[type="submit"]').click();
	});

	$('.chosen').chosen();
	$('input[name="username"]').focus();

	setTimeout(function(){

		var options = {
		    onKeyUp: function (e) {
		      if(isEmpty($(e.target).val())){
                $('.password-strength-title, .password-verdict, .progress').hide();
		        $(e.target).parent().removeClass('has-success').addClass('has-error');
		      } else {
				$('.password-strength-title, .password-verdict, .progress').show();
		        $(e.target).parent().removeClass('has-error').addClass('has-success');
		      }
		      $(e.target).pwstrength("outputErrorList");
		    },
		    verdicts: ["<?php print locale('weak');?>","<?php print locale('normal');?>","<?php print locale('medium');?>","<?php print locale('strong');?>","<?php print locale('optimum');?>"],
		    errorMessages: {
		      password_to_short: "<?php echo locale('password_too_short');?>",
		      same_as_username: "<?php echo locale('password_same_as_username');?>"
		    }        
		};

		$('#password').pwstrength(options);

		onkeyup({
		  target:'input[name="username"]',
		  url:'/username-check',
		  focus: false,
		  success:function(json){
		    $('#messages-username').html('<span class="text-' + json.status + '"><i class="' + json.ion + '"></i> ' + json.message + '</span>');
		  }
		});

		onkeyup({
		  target:'input[name="email"]',
		  url:'/email-check',
		  focus: false,
		  success:function(json){
		    $('#messages-email').html('<span class="text-' + json.status + '"><i class="' + json.ion + '"></i> ' + json.message + '</span>');
		  }
		});

		$('#password').after('<span class="password-strength-title"><?php print locale('password_strength');?>: </span>');
		$('.password-strength-title, .password-verdict, .progress').hide();

	},100);
});
</script>