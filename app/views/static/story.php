      
    <div class="c-preloader js-preloader">
        <div class="c-preloader__body">
            <!--img src="<?php print site_asset('/story/img/loader.gif');?>" alt="" class="c-preloader__loader u-img-respond"-->
            <span class="c-preloader__percent"><span class="js-preloader-percent">0</span><span class="c-preloader__metric">%</span></span>
        </div>
    </div>

    <div id="world" class="c-world js-scrollbar js-world js-dragscrollable js-alert-offset">
        <div class="c-world__acts js-act-size">
            <div class="c-world__acts-wrap js-acts-wrap js-act-wrap-size">

            <div id="scene" class="c-scene">
    <div class="c-scene__act js-act-size js-scene" data-scene-act="1">
        <img src="<?php print site_asset('/story/img/act-1/act-1-world-layer-base.jpg');?>" class="c-scene__act-layer u-img-full">
        <img src="<?php print site_asset('/story/img/act-1/act-1-world-layer-middleground.jpg');?>" class="c-scene__act-layer c-scene__act-layer--fade u-img-full js-scene-layer" data-scene-layer="mid">
        <img src="<?php print site_asset('/story/img/act-1/act-1-world-layer-foreground.jpg');?>" class="c-scene__act-layer c-scene__act-layer--fade u-img-full js-scene-layer" data-scene-layer="fore">
        <img src="<?php print site_asset('/story/img/act-1/act-1-world-layer-background.jpg');?>" class="c-scene__act-layer c-scene__act-layer--fade u-img-full js-scene-layer" data-scene-layer="back">
    </div>
    <div class="c-scene__act js-act-size js-scene" data-scene-act="2">
        <img src="<?php print site_asset('/story/img/act-2/act-2-world-layer-base.jpg');?>" class="c-scene__act-layer u-img-full">
        <img src="<?php print site_asset('/story/img/act-2/act-2-world-layer-middleground.jpg');?>" class="c-scene__act-layer c-scene__act-layer--fade u-img-full js-scene-layer" data-scene-layer="mid">
        <img src="<?php print site_asset('/story/img/act-2/act-2-world-layer-foreground.jpg');?>" class="c-scene__act-layer c-scene__act-layer--fade u-img-full js-scene-layer" data-scene-layer="fore">
        <img src="<?php print site_asset('/story/img/act-2/act-2-world-layer-background.jpg');?>" class="c-scene__act-layer c-scene__act-layer--fade u-img-full js-scene-layer" data-scene-layer="back">
    </div>
    <div class="c-scene__act js-act-size js-scene" data-scene-act="3">
        <img src="<?php print site_asset('/story/img/act-3/act-3-world-layer-base.jpg');?>" class="c-scene__act-layer u-img-full">
        <img src="<?php print site_asset('/story/img/act-3/act-3-world-layer-middleground.jpg');?>" class="c-scene__act-layer c-scene__act-layer--fade u-img-full js-scene-layer" data-scene-layer="mid">
        <img src="<?php print site_asset('/story/img/act-3/act-3-world-layer-foreground.jpg');?>" class="c-scene__act-layer c-scene__act-layer--fade u-img-full js-scene-layer" data-scene-layer="fore">
        <img src="<?php print site_asset('/story/img/act-3/act-3-world-layer-background.jpg');?>" class="c-scene__act-layer c-scene__act-layer--fade u-img-full js-scene-layer" data-scene-layer="back">
    </div>
</div>

            </div>
            <div class="c-world__acts-poi">
                <div class="c-act c-act--1 js-act-size js-act js-act-1">
    <ul class="o-list-unstyled c-poi-list js-scene-controls js-scene-controls-1">
        <li class="c-poi-list__item">
            <div id="act-1-popup-1" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="back">
                <button class="c-poi__marker c-poi__marker--character js-open-poi" style="left:23.661718398560506%;top:55.2%;" data-target-poi="1"></button>
                <div class="c-poi__popup c-poi__popup--character c-poi__popup--graves js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title u-h2">Malcolm Graves, el Forajido</h4>
                        <div class="c-poi__content">Malcolm Graves es un hombre buscado en todos los reinos, ciudades e imperios que ha visitado. Duro, determinado y, sobre todo, implacable, logró amasar una pequeña fortuna (que luego perdió) gracias a una vida de crimen.</div>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-1-popup-2" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="fore">
                <button class="c-poi__marker js-open-poi" style="left:24.426450742240217%;top:12%;" data-target-poi="2"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Estandartes de Ganchos Dentados</h4>
                        <span>Una de las pandillas más antiguas y feroces de Aguasturbias, los Ganchos Dentados le juran su lealtad a Gangplank. su nombre proviene de las armas curvas y retorcidas que usan para cazar monstruos marinos.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-1-popup-3" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="fore">
                <button class="c-poi__marker js-open-poi" style="left:79.1722896986055%;top:52.480000000000004%;" data-target-poi="3"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">La Gran Barbuda</h4>
                        <span>También conocida como la Serpiente Madre por las culturas indígenas alrededor de la ciudad, la Gran Barbuda es el la deidad suprema de Aguasturbias. Sus mitos ancestrales cuentan sobre marineros descuidados que olvidan dejar el diezmo tradicional en el Pozo de la Serpiente al navegar su barco hacia los puertos de Aguasturbias.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-1-popup-4" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="fore">
                <button class="c-poi__marker js-open-poi" style="left: 35.208727%;top: 82.64%;" data-target-poi="4"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Serpientes de Plata</h4>
                        <span>Cada día comerciantes, mercaderes y corsarios de toda Runaterra llevan bienes y riquezas al próspero puerto. Solo un tonto rechazaría el oro, sin importar el rostro del dignatario extranjero que aparezca en la moneda. Aguasturbias sin embargo acuña su propio dinero, incluidos los Krakens de Oro.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-1-popup-5" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="mid">
                <button class="c-poi__marker js-open-poi" style="left:38.37156995051732%;top:58.879999999999995%;" data-target-poi="5"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Cañón de mano</h4>
                        <span>Al ser relativamente baratas, estas armas reparadas son populares entre la gran gama de pandillas de los muelles, bandoleros comunes y corsarios jóvenes.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-1-popup-6" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="fore">
                <button class="c-poi__marker js-open-poi" style="left:76.47323436797122%;top:77.28%;" data-target-poi="6"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Daga de rollo carmesí</h4>
                        <span>Alguien estaba dispuesto a pagar una considerable suma por esta modesta daga del tesoro de Gangplank. Tanto su origen como su uso son desconocidos.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-1-popup-7" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="mid">
                <button class="c-poi__marker js-open-poi" style="left:57.13000449842555%;top:48.8%;" data-target-poi="7"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">La bodega</h4>
                        <span>Ubicada al final de un muelle, rodeada de aguas infestadas de tiburones y peces navaja, y por la peligrosa pandilla de los Ganchos Dentados, la bodega de Gangplank está llena de tesoros y botines provenientes de todo el mundo.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-1-popup-8" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="fore">
                <button class="c-poi__marker js-open-poi" style="left:12.145748987854251%;top:60.8%;" data-target-poi="8"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Enemigos caídos</h4>
                        <span>Uno no toma el control de Aguasturbias sin acabar con un par de enemigos en el proceso.</span>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
                <div class="c-act c-act--2 js-act-size js-act js-act-2">
    <ul class="o-list-unstyled c-poi-list js-scene-controls js-scene-controls-2">
        <li class="c-poi-list__item">
            <div id="act-2-popup-1" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="mid">
                <button class="c-poi__marker c-poi__marker--character js-open-poi" style="left:23.661718398560506%;top:55.2%;" data-target-poi="1"></button>
                <div class="c-poi__popup c-poi__popup--character c-poi__popup--tf js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title u-h2">Twisted Fate, el Maestro de las Cartas</h4>
                        <div class="c-poi__content">Twisted Fate es un afamado experto en juegos de cartas y estafador. Ha apostado y usado su encanto en gran parte del mundo conocido, lo que le ha ganado el odio y la admiración de ricos y tontos por igual. Rara vez se toma las cosas en serio, despierta cada día con una burlesca sonrisa y un descuidado aire de fanfarrón. Por donde se vea, Twisted Fate siempre tiene un as bajo la manga.</div>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-2-popup-2" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="back">
                <button class="c-poi__marker js-open-poi" style="left:92.12775528565003%;top:34.64%;" data-target-poi="2"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Arquitectura</h4>
                        <span>A Aguasturbias le faltan recursos naturales para la construcción, lo que obliga a sus habitantes a reusar todo lo que pueden. Es normal ver restos de barcos de lugares tan lejanos como Jonia, Demacia y el mismo Fréljord como parte de su arquitectura.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-2-popup-3" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="mid">
                <button class="c-poi__marker js-open-poi" style="left:49.887539361223574%;top:38.72%;" data-target-poi="3"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Flota asesina</h4>
                        <span>Hay flotas que salen del puerto todos los días al anochecer para cazar monstruos marinos. Siendo rivales que se identifican con símbolos y tradiciones únicas, muchas de las flotas luchan constantemente entre sí para establecer su dominio.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-2-popup-4" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="mid">
                <button class="c-poi__marker js-open-poi" style="left:17.139001349527668%;top:34.64%;" data-target-poi="4"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Cobertizos del matadero</h4>
                        <span>Tras una exitosa caza de monstruos marinos, las flotas asesinas regresan a los muelles del matadero para rebajar a las criaturas a carne, huesos y pieles acorazadas dentro de estos enormes cobertizos. Un lucrativo mercado de glándulas, órganos y secreciones sacadas de las serpientes prospera en Aguasturbias.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-2-popup-5" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="mid">
                <button class="c-poi__marker js-open-poi" style="left:43.18488529014845%;top:64.48%;" data-target-poi="5"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Monstruos marinos</h4>
                        <span>Los enormes monstruos marinos son una constante amenaza en los mares que rodean a Aguasturbias y, con el paso de los siglos, se creó una industria de mercado a partir de la caza y faena de estas. Se desconoce el motivo por el cual estas bestias se acercan a las islas, pero su impacto es innegable.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-2-popup-6" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="fore">
                <button class="c-poi__marker js-open-poi" style="left:15.744489428699953%;top:76%;" data-target-poi="6"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Ratas de muelle</h4>
                        <span>Una terrorífica mezcla entre tiburón y rata, estas criaturas son más grandes que los perros y son conocidas por acechar a borrachos y pescadores solitarios en las noches más oscuras. Generalmente viajan en manada y pueden arrancarle con facilidad la pierna a un hombre de una sola mordida.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-2-popup-7" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="fore">
                <button class="c-poi__marker js-open-poi" style="left:60.68376068376068%;top:78.56%;" data-target-poi="7"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Tallado de Tahm Kench</h4>
                        <span>La imagen del viejo Tahm Kench marca los antros de avaricia en todo Aguasturbias. Símbolo de codicia y libertad extrovertida, el rostro del Rey del Río no solo aparece en forma de grafiti, sino que también sirve de guía para los que buscan saciar apetitos indecentes. </span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-2-popup-8" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="mid">
                <button class="c-poi__marker js-open-poi" style="left:36.032388663967616%;top:11.600000000000001%;" data-target-poi="8"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Gun’dola</h4>
                        <span>Estas plataformas elevadas transportan bienes y carne, huesos y grasa de monstruo marino faenado por todas las islas sobre viejos rieles. Algunas de las góndolas tienen instalados cañones, una astuta inspiración para el nombre de este lugar.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-2-popup-9" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="mid">
                <button class="c-poi__marker js-open-poi" style="left:55.10571300044984%;top:9.120000000000001%;" data-target-poi="9"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">El Puente del Carnicero</h4>
                        <span>Este puente de piedra fue alguna vez el camino hacia la entrada de un templo, pero ahora es una peligrosa conexión entre los muelles del matadero y las barriadas de Aguasturbias.</span>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
                <div class="c-act c-act--3 js-act-size js-act js-act-3">
    <ul class="o-list-unstyled c-poi-list js-scene-controls js-scene-controls-3">
        <li class="c-poi-list__item">
            <div id="act-3-popup-1" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="back">
                <button class="c-poi__marker c-poi__marker--character js-open-poi" style="left:69.23076923076923%;top:42.88%;" data-target-poi="1"></button>
                <div class="c-poi__popup c-poi__popup--character c-poi__popup--mf js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title u-h2">Miss Fortune, la Cazarrecompensas</h4>
                        <div class="c-poi__content">Belleza y peligro: son muy pocos los que pueden igualar a Miss Fortune en tales atributos. Es una de las cazarrecompensas más infames de Aguasturbias, que creó su leyenda sobre un reguero de cadáveres acribillados y holgazanes capturados. El eco atronador de sus pistolas gemelas rebota en los hediondos atracaderos y las míseras casuchas de la ciudad portuaria, como señal inequívoca de que otra riña por un botín de los más buscados se está llevando a cabo.</div>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-3-popup-2" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="fore">
                <button class="c-poi__marker c-poi__marker--character js-open-poi" style="left:35.17768780926676%;top:44.32%;" data-target-poi="2"></button>
                <div class="c-poi__popup c-poi__popup--character c-poi__popup--gangplank js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title u-h2">Gangplank, el Azote de los Mares</h4>
                        <div class="c-poi__content">Impredecible y brutal, el destronado rey de los saqueadores a quien todo el mundo conoce como Gangplank es temido a lo largo y ancho del mundo. Doquier que dirija sus negras velas, muerte y ruina habrán de acompañarlo, pues tal es su mala reputación que la mera visión de su insignia en el horizonte pone a temblar hasta al marino más curtido.</div>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-3-popup-3" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="fore">
                <button class="c-poi__marker js-open-poi" style="left:51.02879%;top:32.92%;" data-target-poi="3"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">El Masacre</h4>
                        <span>Una enorme bestia de tres mástiles, el buque insignia de Gangplank es una de las naves más infames de toda Runaterra. Heredada tras su despiadado parricidio, el Masacre es un buen recordatorio del temible poder del capitán y la manifestación física de este.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-3-popup-4" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="back">
                <button class="c-poi__marker js-open-poi" style="left:79.4421952316689%;top:22.720000000000002%;" data-target-poi="4"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Invocadores de Serpientes</h4>
                        <span>Ya sea debido a la magia o a su diseño arquitectónico ancestral, los invocadores de serpientes usan estos pilares huecos para imitar los gritos y chillidos de los horrores que habitan el fondo marino, para traerlos a la superficie o asustarlos.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-3-popup-5" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="fore">
                <button class="c-poi__marker js-open-poi" style="left:59.37921727395412%;top:80%;" data-target-poi="5"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">El Barquero</h4>
                        <span>En Aguasturbias, los muertos no se sepultan, se devuelven al océano. El barquero se lleva los cuerpos de los muertos a los distintos cementerios dispersos por los canales que rodean a la ciudad.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-3-popup-6" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="back">
                <button class="c-poi__marker js-open-poi" style="left:72.42465137201978%;top:64.8%;" data-target-poi="6"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Cementerio</h4>
                        <span>Los cementerios constan de muchísimas boyas que flotan en la superficie y que están amarradas a los cadáveres en el fondo marino. La gente rica es enterrada en costosos ataúdes debajo de espectaculares lápidas que se mecen en el agua, mientras que los pobres por lo general son amarrados en masa a viejas anclas debajo de barriles anegados.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-3-popup-7" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="fore">
                <button class="c-poi__marker js-open-poi" style="left:24.20152946468736%;top:80%;" data-target-poi="7"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Aguas infestadas de tiburones</h4>
                        <span>El mar alrededor de los muelles del matadero por lo general se oscurece con la sangre de los monstruos marinos asesinados. El cebo atrae a tiburones y otros depredadores hacia los distintos puertos, lo que da paso a espumarajos violentos.</span>
                    </div>
                </div>
            </div>
        </li>
        <li class="c-poi-list__item">
            <div id="act-3-popup-8" class="c-poi c-poi-list__poi js-poi js-mask-layer" data-mask-layer="fore">
                <button class="c-poi__marker js-open-poi" style="left:34.02879%;top:69.92%;" data-target-poi="8"></button>
                <div class="c-poi__popup js-poi-popup">
                    <button class="c-poi__close js-close-poi"></button>
                    <div class="s-poi__body">
                        <h4 class="c-poi__title">Botín de guerra</h4>
                        <span>Cuando el barco de Gangplank regresa al puerto, cargado con el tesoro de su última victoria o atraco en alta mar, primero pasa por los muelles del matadero para dejar sus nuevas riquezas en la bodega del capitán.</span>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
            </div>


        </div>

        <div id="controls-act-1" class="c-act-controls js-scene-controls js-scene-controls-1"  style="visibility: inherit; opacity: 1;">
    <aside class="c-act-controls__block c-act-controls__block--right u-hidden-less-than-sm">
        <a href="#act-2" class="c-act-controls__block-link js-next-act">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-right"></i></span>
        </a>
    </aside>
    <footer class="c-section__footer u-hidden-less-than-sm">
        <div class="c-title-group c-section__footer-title">
            <h5 class="c-title-group__subtitle"><span>Explora el Mundo</span><span>:</span></h5>
            <h4 class="c-title-group__title u-text-gradient" title="La Bodega">La Bodega</h4>
        </div>
        <div class="c-section__footer-block">
            <div class="c-panel">
                <ul class="o-list-unstyled c-panel__list">
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-1-marker-8" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="8">Enemigos caídos</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-1-marker-1" class="c-panel__list-link c-panel__list-link--character js-poi-marker js-open-poi is-disabled" data-target-poi="1">Malcolm Graves, el Forajido</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-1-marker-2" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="2">Estandartes de Ganchos Dentados</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-1-marker-4" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="4">Serpientes de Plata</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-1-marker-5" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="5">Cañón de mano</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-1-marker-7" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="7">La Bodega</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-1-marker-6" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="6">Daga de rollo carmesí</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-1-marker-3" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="3">La Gran Barbuda</button></li>
                    <li class="c-panel__list-item c-poi-toggle"><button class="c-poi-toggle__button is-active js-toggle-poi" data-poi-state="true">encendido</button> / <button class="c-poi-toggle__button js-toggle-poi" data-poi-state="false">apagado</button></li>
                </ul>
            </div>
        </div>
        <div class="c-section__footer-block"><a href="#story-1" class="c-btn c-btn--default js-toggle-overlay"><span class="c-btn__content">Lee la historia</span></a></div>
    </footer>
</div>
<div id="controls-act-2" class="c-act-controls js-scene-controls js-scene-controls-2">
    <aside class="c-act-controls__block c-act-controls__block--left u-hidden-less-than-sm">
        <a href="#act-1" class="c-act-controls__block-link js-prev-act">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-left c-circle__icon"></i></span>
        </a>
    </aside>
    <aside class="c-act-controls__block c-act-controls__block--right u-hidden-less-than-sm">
        <a href="#act-3" class="c-act-controls__block-link js-next-act">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-right"></i></span>
        </a>
    </aside>
    <footer class="c-section__footer u-hidden-less-than-sm">
        <div class="c-title-group c-section__footer-title">
            <h5 class="c-title-group__subtitle"><span>Explora el Mundo</span><span>:</span></h5>
            <h4 class="c-title-group__title u-text-gradient" title="Los Muelles del Matadero">Los Muelles del Matadero</h4>
        </div>
        <div class="c-section__footer-block">
            <div class="c-panel">
                <ul class="o-list-unstyled c-panel__list">
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-2-marker-6" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="6">Ratas de muelle</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-2-marker-4" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="4">Cobertizos del matadero</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-2-marker-1" class="c-panel__list-link c-panel__list-link--character js-poi-marker js-open-poi is-disabled" data-target-poi="1">Twisted Fate, el Maestro de las Cartas</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-2-marker-8" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="8">Gun’dola</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-2-marker-5" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="5">Monstruos marinos</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-2-marker-3" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="3">Flota asesina</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-2-marker-9" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="9">El Puente del Carnicero</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-2-marker-7" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="7">Tallado de Tahm Kench</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-2-marker-2" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="2">Arquitectura</button></li>
                    <li class="c-panel__list-item c-poi-toggle"><button class="c-poi-toggle__button is-active js-toggle-poi" data-poi-state="true">ON</button> / <button class="c-poi-toggle__button js-toggle-poi" data-poi-state="false">OFF</button></li>
                </ul>
            </div>
        </div>
        <div class="c-section__footer-block"><a href="#story-2" class="c-btn c-btn--default js-toggle-overlay"><span class="c-btn__content">Lee la historia</span></a></div>
    </footer>
</div>
<div id="controls-act-3" class="c-act-controls js-scene-controls js-scene-controls-3">
    <aside class="c-act-controls__block c-act-controls__block--left u-hidden-less-than-sm">
        <a href="#act-2" class="c-act-controls__block-link js-prev-act">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-left c-circle__icon"></i></span>
        </a>
    </aside>
    <footer class="c-section__footer u-hidden-less-than-sm">
        <div class="c-title-group c-section__footer-title">
            <h5 class="c-title-group__subtitle">Explora el Mundo:</h5>
            <h4 class="c-title-group__title u-text-gradient" title="El Masacre">El Masacre</h4>
        </div>
        <div class="c-section__footer-block">
            <div class="c-panel">
                <ul class="o-list-unstyled c-panel__list">
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-3-marker-7" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="7">Aguas infestadas de tiburones</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-3-marker-8" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="8">Botín de guerra</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-3-marker-2" class="c-panel__list-link c-panel__list-link--character js-poi-marker js-open-poi is-disabled" data-target-poi="2">Gangplank, el Azote de los Mares</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-3-marker-3" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="3">El Masacre</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-3-marker-5" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="5">El Barquero</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-3-marker-1" class="c-panel__list-link c-panel__list-link--character js-poi-marker js-open-poi is-disabled" data-target-poi="1">Miss Fortune, la Cazarrecompensas</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-3-marker-6" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="6">Cementerio</button></li>
                    <li class="c-panel__list-item c-panel__list-marker"><button id="act-3-marker-4" class="c-panel__list-link js-poi-marker js-open-poi is-disabled" data-target-poi="4">Invocadores de Serpientes</button></li>
                    <li class="c-panel__list-item c-poi-toggle"><button class="c-poi-toggle__button is-active js-toggle-poi" data-poi-state="true">ON</button> / <button class="c-poi-toggle__button js-toggle-poi" data-poi-state="false">OFF</button></li>
                </ul>
            </div>
        </div>
        <div class="c-section__footer-block"><a href="#story-3" class="c-btn c-btn--default js-toggle-overlay"><span class="c-btn__content">Lee la historia</span></a></div>
    </footer>
</div>

    </div>

    <div id="stories" class="c-stories-overlay js-overlay js-alert-offset" data-overlay="story">
        <div class="c-stories-overlay__wrap js-stories-wrap" style="width:400%;">

            <article id="article-1" class="c-story c-stories-overlay__story js-story js-story-1 is-current" style="width:25%;">
    <div class="c-scroll c-story__scroll js-scroll js-scroll-list-position" data-scroll-sync="#story-1">
        <div class="c-scroll__bar js-story-scrollbar">
            <div class="c-scroll__bar-marker js-story-scrollbar-marker"></div>
        </div>
        <ol class="o-list-unstyled c-scroll__list">
            <li class="c-scroll__list-item js-scroll-marker is-active" data-scroll-marker="part-1"><a href="#story-1-part-1" class="c-scroll__list-link js-scroll-to" data-context="story-1">1</a></li>
            <li class="c-scroll__list-item js-scroll-marker" data-scroll-marker="part-2"><a href="#story-1-part-2" class="c-scroll__list-link js-scroll-to" data-context="story-1">2</a></li>
            <li class="c-scroll__list-item js-scroll-marker" data-scroll-marker="part-3"><a href="#story-1-part-3" class="c-scroll__list-link js-scroll-to" data-context="story-1">3</a></li>
        </ol>
    </div>
    <div class="c-story__graphic js-story-fade-scroll">
        <img src="<?php print site_asset('/story/img/act-1/story-backdrop-act-1.jpg');?>" class="c-story__graphic-img">
    </div>



    <div class="o-overlay__body js-scrollbar js-story-scrolling-body">
        <div class="o-overlay__container js-story-container">
            <div id="story-body-1" class="c-story__body">
                <section id="story-1-part-1" class="c-story__block js-story-block js-story-block-1" data-scroll-checkpoint="part-1">
                    <header class="c-story__header js-scroll-checkpoint">
                        <img src="<?php print site_asset('/story/img/act-1/chapter-heading-act-1-part-1.png');?>" alt="Capítulo I" class="c-story__section-header u-hidden-less-than-sm">
                        <h1>Capítulo I</h1>
                        <h4>El Principio del Cambio</h4>
                    </header>
                    <div class="c-story__content">

<p>Solo voy a contarles lo que recuerdo del principio de esta historia. Del resto... Preferiría ni hablar por ahora. Hace unos meses, o al menos eso creo, el sistema para algunos paso a ser perfecto. Los dioses de Aden proveían de vuestra vida. En las guerras, y en cualquier rutina diaria, si uno moría… En cuestión de segundos volvería a estar con vida. Desde mi opinión; ese fue el comienzo de este caos.</p><p>Nuestro antiguo mundo se vio afectado por la ira de los dioses, una cantidad infinita de desastres naturales arremetían constantemente nuestro planeta. Las victimas? Aún me niego a creer que sean todos, aunque algunos se esfuercen por demostrar lo contrario. Por el momento, entre todos los seres que se encuentran en este limbo (así es como la mayoría ha decidido llamarle), solo me he reencontrado con una vieja amiga, y aún ni ella y yo sabemos algo de los nuestros.</p><p>Las esperanzas aquí cada vez son menos, con el correr del tiempo todas las razas van perdiendo poco a poco sus sentimientos. Al principio pensaba que era tan solo un síntoma de este nuevo sistema de combate, si es que así puede llamársele. Pero cuando nos toco a nosotras,entendí que estaba equivocada… Ya que hasta el día de hoy jamás hemos luchado.</p><p>Lo primero en presentarse fue la falta de apetito, aquí todos llevan meses enteros sin alimentarse. Lo segundo fue el sueño… Los primeros días cuando el sol caía, mi cuerpo agotado de tanto llorar se dormía rendido. Hoy creo cumplir el octavo día sin dormir...</p><p>Y aunque todo esto sea una milésima parte de todo lo que viene ocurriendo y ya suene terrible, desde aquí parece que todas las razas se han acostumbrado a esto, una lucha constante sin fin que no deja nada, un futuro sin sentimientos, un vacio infinito, un maldito infierno en llamas.</p>

                    </div>
                </section>

                <section id="story-1-part-2" class="c-story__block js-story-block js-story-block-2" data-scroll-checkpoint="part-2">
                    <header class="c-story__header js-scroll-checkpoint">
                        <img src="<?php print site_asset('/story/img/act-1/chapter-heading-act-1-part-2.png');?>" alt="Primer Acto – Segunda Parte" class="c-story__section-header u-hidden-less-than-sm">
                        <h1>Capítulo II</h1>
                        <h4>La Corona</h4>
                    </header>
                    <div class="c-story__content">
<p>El tiempo sigue transcurriendo... Estos dos últimos años no han sido fáciles para nadie. Pero hoy después de tanto tiempo, la esperanza vuelve a renacer en la mayoría de nosotros, viéndose reflejada en un valiente pequeñaco humano, "Drago".</p><p>—Oye Drago! Lo he estado pensando y… ¿No crees que deberíamos adentrarnos en algún campo de nuevo? Esto aquí abuuuurre tío, y aún yo no pierdo ese sentimiento. ¿Vale?</p><p>—Yair. ¿No te cansas de meterte en problemas verdad? Ya escuchaste a tu madre y a mi padre.</p><p>—Vaaaamos drago! ¿Que es lo peor que podría pasarnos acá? ¿Cuál sería el castigo, eh? Aaaanda será la última esta vez, lo prometo.</p><p>—Cuantas veces ya te habré escuchado decir eso....</p><p>—No nos rendiremos! No bajaremos los brazos ahora. Somos guerreros! Y nacemos luchando por los nuestros… Puede que estemos perdiendo algunos sentimientos, pero jamás perderémos nuestra esencia, ya que nos corre por las venas! - Gritaba el rey Regane ante una multitud.</p><p>—Já! que iluso es Regane… Aún cree que puede seguir liderando desde aquí arriba, cuando ni si quiera podía hacerlo en Giran. – Se burlaba Yair.</p><p>—Mi padre sigue siendo uno de sus fieles seguidores y lo respeta, a tal punto de seguir usando los portales por él. Y puedo asegurarte que no es el único, así que nada me sorprendre. Solo quisiera que todo vuelva a ser como antes tío… - Respondía desilusionado Drago.</p><p>Los valientes pequeñacos se decidían a usar otra vez más los portales. Un nuevo combate los estaba esperando, una batalla que nadie olvidaría jamás. Una batalla que quedaría en la historia.</p><p>—Oye Drago</p><p>—Si?</p><p>—Solo ten cuidado por donde andas quieres.– Le aconsejaba Yair.</p><p>—Recuerdalo. Somos guerreros… nacemos luchando - Respondía Drago.</p><p>—Jajaja, que así sea entonces tío!</p><p>—Menos charla y mas acción pequeñacos! – Gritaba Frand. Uno de los enanos más fuertes del mundo.</p><p>Y así, Drago, Yair y su equipo, comenzaban la batalla de sus vidas. Gracias a los portales, he tenido el placer de ser uno de los espectadores y supongo que por ello, mi fe por Drago es mucho mayor. Se notaba la tensión en el aire desde un comienzo, cada estrategia de ambos equipos terminaba contrarrestando la anterior. Los aliados de Drago, en muchos momentos se vieron perdidos. Sin embargo, fue él en el medio del caos quien los alentó a seguir mostrándoles el camino con su espada dorada. Y cuando todo parecía perdido… él cambio la historia con una jugada magistral.</p><p>El equipo rival invadía su base y bien consciente era de esto. Sus aliados, al no saber donde se encontraba, intentaban defender el Nexus posicionándose detrás de sus torres, de la manera más defensiva posible para no sufrir ningúna perdida. Pero sabían que la derrota estaba a un abrir y cerrar de ojos... De repente, se escucho la voz que nadie esperaba oir: “El inhibidor enemigo ha sido destruido”. El equipo rival un tanto desconcertado, se dio cuenta de que Drago estaba destruyendo su base, por lo que no les quedo otro remedio que retroceder para defender su propio Nexus. Al regresar, vieron que el pequeñaco estaba a punto de destruir el corazón del lado oscuro y salieron a correrlo. Drago estando solo contra todos, no tuvo otro remedio que huir, pero la partida ya estaba ganada. Sus aliados aprovecharon la ocasión para terminar de derribar el nexus rival cuando el equipo contrario dejo desprotejida su base, llegando así, a una victoria realmente épica.</p><p>Como en toda partida al finalizar, los participantes se quedaron totalmente paralizados por unos breves segundos antes de regresar al limbo, pero esta vez a Drago no le pasaría. El muchacho siguió corriendo pensando que sus rivales aún lo estaban persiguiendo, pero cuando miro hacia atrás, se dio cuenta que ya no había nadie. Sorprendido, comenzó a mirar hacia todos lados y fue en ese entonces, donde encontró una corona brillante tirada en el piso. Cuando se digno a sujetarla, el portal lo regreso nuevamente al limbo, donde la noticia ya había empezado a correr.</p><p>—Ei drago, drago ven aquí!</p><p>—Ahora no Asteric. ¿Que sucede?</p><p>—Escucha, lo eh visto todo en el portal y no fui el único, vi el momento en el que te apareció esa corona enfrente tuyo mágicamente. ¿Me dejas verla?</p><p>—No sería lo indicado, no aquí en el medio del limbo, ya me eh metido en bastante problemas últimamente. ¿No crees?.</p><p>—No quiero ser negativo ni mucho menos Drago, pero dudo mucho que puedas escaparte de esta. Tu padre ha ido tras de ti cuando se entero que estabas en los campos de nuevo, y por lo visto, no ha entrado a tu misma batalla. Ademas, mientras venia hacia aquí corriendo, eh escuchado varios murmullos de otros individuos que vieron también lo ocurrido en esa batalla. Tío… ellos piensa que eres el nuevo elegido, y que su única posibilidad de salir de esta pesadilla, ahora eres tú.</p><p>—Que locura todo esto, mi padre no me hablara por algún tiempo… Creo Asteric, que será lo mejor dejar todo esto por un tiempo…</p><p>—Ey su voluntad! Porque no le pide a los dioses una buena cerveza eh? – Gritaba exaltado Yair</p><p>—Yair baja la voz ya! Aún no entiendes en el lio en el que me has metido verdad? – Contestaba Drago enojado</p><p>—Vamos tío relájate, no sería tan malo tener un poooquito mas de poder que el resto aquí, no crees?</p><p>—Yair tu definitivamente perdiste varios sentimientos, ya te has olvidado lo que ocurrió en el mundo al que realmente pertenecemos? Te has olvidado de mi hermano? Eh? A caso te has olvidado de Kelly? Aún no los encuentro maldita seas!</p><p>La corona de Drago cayó de sus manos, y se elevo sola hasta la parte superior de su cabeza. Emanando una luz clara y enceguesedora, que cubrió cada rincón del limbo. Parecía como si estuviese conectada con él, como si esta entendiese todo el dolor que llevaba dentro.</p>

                    </div>
                </section>

                <section id="story-1-part-3" class="c-story__block js-story-block js-story-block-3" data-scroll-checkpoint="part-3">
                    <header class="c-story__header js-scroll-checkpoint">
                        <img src="<?php print site_asset('/story/img/act-1/chapter-heading-act-1-part-3.png');?>" alt="Primer Acto – Tercera Parte" class="c-story__section-header u-hidden-less-than-sm">
                        <h1>Capítulo III</h1>
                        <h4>Aclaman al Nuevo Héroe</h4>
                    </header>
                    <div class="c-story__content">
<p>—Asteric, ya no sé cuanto más pueda aguantar esto. Mi cabeza no deja de pensar en miles de cosas ultimamente. Principalmente en como se supone que le diré a mi hermano, si es que algún día vuelvo a encontrarlo, que nuestro padre se ha ido realmente por intentar protegerme, de las actitudes infantiles de siempre...</p><p>—Drago no imagino cuán difícil es para ti todo esto, ni tampoco imagino cuánto pesa esa corona que llevas puesta en tu frente simplemente por obligación. Sé que quieres reencontrar a tu padre, pero piénsalo bien hermano, de todas las veces que hemos luchado allí, cuantas veces has visto algo diferente? Siempre es el mismo Spheal de mierda. Pero allá en el limbo Drago, mi familia y miles de personas más de razas y tonos de pieles diferentes, esperan ansiosos a que siempre regreses. Creen en ti, tienen fe ciega en tu corona. Te has vuelto su esperanza hace tiempo… Y podría decirte que también eres la mía. Solo pido que dejes de arriesgarte, te necesitamos vivo Drago.</p><p>—Déjame pensarlo tranquilo Asteric. Gracias por el consejo de todos modos, veré que hare mañana..</p><p>Al día siguiente...</p><p>—Shane. Necesito que me autorices a subir? - Precionaba drago al guardia.</p><p>—Lo siento chico, ya sabes que ordenes son ordenes, y Regane fue muy claro cuando dijo que aquí nadie sube sin su autorización.</p><p>— Bleh, dile a Regane que si no me hacer llamar en el transcurso de los siguientes 20 minutos, entonces estare iniciando una revolucion. Y si te pregunta quien sera su cabecilla, dile que esta corona... Tambien habla</p><p>—Tranquilo Drago, sabes que le hare llegar tu mensaje.</p><p>Ahora en el Limbo Regane estaba "al mando". Había aprovechado de mala manera la dura situación que Drago estaba viviendo. Sin embargo, yo estaba seguro de que si Drago se proponía a derrocarlo, lo haría sin ningún tipo de problema. La gente seguía creyendo en él, tenían fe ciega en su corona.</p><p>Quince minutos más tarde...</p><p>—Drago, te buscan arriba - El guardia no había tardado en volver.</p><p>Drago se alisto rapido y se decidio a subir.</p><p>—Regane creo simplemente que es hora de irte - Amenazaba Drago al Rey.</p><p>—Ei drago... Tranquilo, todavía estas alterado por lo de tu padre verdad? - Retrucaba Regane dandole en el punto más debil</p><p>—Hijo de …</p><p>—Eieiei calmaos, calmaos, calmaos. Disculpa Drago, es que mis guardias se toman todo muy personal, solo te llamaba para que veas esta bonita corona. Por cierto muy similar a la tuya , no crees?</p><p>—Maldito desgraciado, a caso te atreviste a duplicarla?</p><p>—¿Duplicarla? Já! Yo talvez lo llamaría de otra manera... ¿Energía portalera? nonono, El elegido de los dioooooses! esa si me gusta más. Vamos, que se valla este malcriado de una vez.</p><p>—Voy a matarte!</p><p>—Que tan seguro estas de eso pequeñaco?</p><p>Drago se lanzo de lleno ante los guardias derribándolos a todos con su imponente armadura. Pegó un gran salto estirando su espada dorada para asesinar a Regane, pero justo antes de alcanzarlo, los dioses volvieron a actuar.Teletransportaron a ambos a un nuevo campo de batalla suspendido en el aire. Se abrió un portal en la ciudad nunca antes visto que mostraba dicha escena. Todos en el Limbo se quedaron atónitos, sospechando de entrada que solo de uno de ellos regresaría vivo.</p>

                    </div>
                </section>


                <footer class="c-story__footer">
                    <ul class="o-list-unstyled">
                        <!--li class="c-story__footer-item"><a href="javascript:void(0)" class="c-btn"><span class="c-btn__content">Continuará...</span></a></li-->
                        <strong><em>CONTINUARÁ... </em></strong>
                        <!--li class="c-story__footer-item u-hidden-less-than-sm"><a href="#act-1" class="c-btn c-btn--dark js-toggle-overlay"><span class="c-btn__content">Explora el Mundo</span></a></li-->
                    </ul>
                </footer>
            </div>
        </div>
    </div>
</article>
            <article id="article-2" class="c-story c-stories-overlay__story js-story js-story-2" style="width:25%;">
    <div class="c-scroll c-story__scroll js-scroll js-scroll-list-position" data-scroll-sync="#story-2">
        <div class="c-scroll__bar js-story-scrollbar">
            <div class="c-scroll__bar-marker js-story-scrollbar-marker"></div>
        </div>
        <ol class="o-list-unstyled c-scroll__list">
            <li class="c-scroll__list-item js-scroll-marker is-active" data-scroll-marker="part-1"><a href="#story-2-part-1" class="c-scroll__list-link js-scroll-to" data-context="story-2">1</a></li>
            <li class="c-scroll__list-item js-scroll-marker" data-scroll-marker="part-2"><a href="#story-2-part-2" class="c-scroll__list-link js-scroll-to" data-context="story-2">2</a></li>
            <li class="c-scroll__list-item js-scroll-marker" data-scroll-marker="part-3"><a href="#story-2-part-3" class="c-scroll__list-link js-scroll-to" data-context="story-2">3</a></li>
            <li class="c-scroll__list-item js-scroll-marker" data-scroll-marker="part-4"><a href="#story-2-part-4" class="c-scroll__list-link js-scroll-to" data-context="story-2">4</a></li>
        </ol>
    </div>
    <div class="c-story__graphic js-story-fade-scroll">
        <img src="<?php print site_asset('/story/img/act-2/story-backdrop-act-2.jpg');?>" class="c-story__graphic-img">
    </div>
    <div class="o-overlay__body js-scrollbar js-story-scrolling-body">
        <div class="o-overlay__container js-story-container">
            <div id="story-body-2" class="c-story__body">
                <section id="story-2-part-1" class="c-story__block js-story-block js-story-block-1">
                    <header class="c-story__header js-scroll-checkpoint">
                        <img src="<?php print site_asset('/story/img/act-2/chapter-heading-act-2-part-1.png');?>" alt="Segundo Acto - Primera Parte" class="c-story__section-header u-hidden-less-than-sm">
                        <h1>Segundo Acto - Primera Parte</h1>
                        <h4>Lucha en los muelles, El Puente del Carnicero, Una ráfaga</h4>
                    </header>
                    <div class="c-story__content">
                        <p>Estoy tosiendo negro. El humo del incendio de la bodega me carcome los pulmones, pero no tengo tiempo para recobrar el aliento. Fate se escapa. Preferiría morir antes que pasar otra eternidad acechándolo por toda Runaterra. Es tiempo de ponerle punto final.</p>
                        <p>El bastardo me ve venir. Empuja a un par de estibadores para sacarlos de su camino y corre a lo largo del muelle. Está tratando de trazar su vía de escape, pero le estoy pisando los talones; así le será imposible concentrarse.</p>
                        <p>Hay más Ganchos pululando en el perímetro, como moscas en una letrina. Antes de que puedan interponerse en su camino, Fate lanza un par de sus cartas explosivas y acaba con los matones. Un par de Ganchos no son nada para él. Pero yo sí. Lo voy a hacer pagar y Fate lo sabe. Se escabulle por el muelle tan rápido como puede.</p>
                        <p>Su riña con los matones del embarcadero me da el tiempo suficiente para alcanzarlo. Cuando me ve, se lanza tras una enorme pila de vértebras de ballena. Con un disparo de mi arma acabo con su escondite al tiempo que una multitud de huesos vuela por los aires.</p>
                        <p>Me responde tratando de arrancarme la cabeza, pero logro dispararle a su carta en pleno vuelo. Explota como si fuera una bomba y nos envía de rodillas al suelo. Se pone de pie rápidamente y huye. Le disparo con Destino a todo lo que da.</p>
                        <p>Algunos Ganchos se nos acercan con cadenas y sables. Doy un giro violento y les vuelo las entrañas hasta que se les salen por las espaldas. Echo a correr antes de poder escuchar el golpe húmedo de sus tripas estrellándose contra el embarcadero. Pongo la mira en Fate, pero me interrumpe el disparo de una pistola. Se aproximan más Ganchos, mejor armados.</p>
                        <p>Me escondo tras el casco de un viejo arrastrero para regresarles el fuego. Mi gatillo suena sin más. Tengo que recargar. Inserto munición nueva en el cilindro, escupo mi enfado en el piso y me sumerjo de nuevo en el caos.</p>
                        <p>A mi alrededor veo cajas de madera en pedazos, reventadas a punta de disparos y explosiones. Un disparo me arranca una buena parte de la oreja. Me armo de valor y empiezo a abrirme camino con el dedo en el gatillo. Destino acaba con quien se le ponga al frente. Un Gancho Dentado pierde la mandíbula. Otro sale volando en dirección a la bahía. Un tercero queda reducido a un puñado de tendones y músculos.</p>
                        <p>En medio del caos, diviso a Fate escapando hacia los rincones más lejanos de los muelles del matadero. Paso corriendo por el lado de un pescadero colgando anguilas de caza. Acaban de despellejar a una; sus tripas seguían desparramándose por el muelle. El hombre se voltea hacia mí empuñando su gancho de carnicero.</p>
                        <p>BAM.</p>
                        <p>Le vuelo una pierna.</p>
                        <p>BAM.</p>
                        <p>Continúo con un tiro en la cabeza.</p>
                        <p>Aparto el cadáver pestilente de un pez navaja de mi camino y sigo avanzando. La sangre acumulada de los peces y los Ganchos que derribamos me llega hasta los tobillos. Suficiente para que a un tipo elegante como Fate le dé un ataque. Incluso conmigo detrás de él, desacelera el paso para no mancharse las faldas.</p>
                        <p>Antes de que pueda alcanzarlo, Fate se echa a correr. Siento que me quedo sin aliento.</p>
                        <p>—¡Date la vuelta y enfréntame! —le grito.</p>
                        <p>¿Qué clase de hombre no se hace cargo de sus problemas?</p>
                        <p>Un ruido a mi derecha reclama mi atención hacia un balcón con dos Ganchos más. Le disparo, se derrumba y cae en pedazos hacia el muelle.</p>
                        <p>El humo de la pistola y los escombros es muy denso. No puedo ver un demonio. Corro hacia el sonido de sus delicadas botas que retumban contra las tablas de madera. Se está haciendo camino hacia el Puente del Carnicero, al borde de los muelles del matadero. La única salida de la isla. Por nada del mundo dejaré que se me escape de nuevo.</p>
                        <p>Al llegar al puente, Fate se detiene en seco a medio correr. Al principio pienso que se va rendir. Luego me doy cuenta de por qué se detuvo: En el otro extremo, una masa de bastardos con espadas se interpone en su camino. Pero yo no pienso detenerme.</p>
                        <p>Fate voltea para evitar el filo de las espadas, pero solo se topa conmigo. Soy su pared. Está atrapado. Mira a un costado del puente, en dirección al agua. Está pensando en saltar, pero sé que no lo hará.</p>
                        <p>Ya no le queda alternativa. Comienza a caminar en dirección a mí.</p>
                        <p>—Mira, Malcolm. Ninguno de los dos tiene que morir aquí. Tan pronto como salgamos de esta…</p>
                        <p>—Te vas a largar corriendo de nuevo. Como siempre lo has hecho.</p>
                        <p>No me responde nada. De pronto ya no le preocupo demasiado. Me doy vuelta para ver a qué le presta tanta atención.</p>
                        <p>Detrás de mí, veo cómo todas las escorias capaces de cargar una pistola o una espada invaden los muelles. Gangplank debe haber llamado a sus muchachos de todas partes de la ciudad. Seguir avanzando solo firmaría nuestra sentencia de muerte.</p>
                        <p>Por otro lado, morir no es mi mayor preocupación el día de hoy.</p>
                    </div>
                </section>

                <section id="story-2-part-2" class="c-story__block js-story-block js-story-block-2" data-scroll-checkpoint="part-2">
                    <header class="c-story__header js-scroll-checkpoint">
                        <img src="<?php print site_asset('/story/img/act-2/chapter-heading-act-2-part-2.png');?>" alt="Segundo Acto — Segunda Parte" class="c-story__section-header u-hidden-less-than-sm">
                        <h1>Segundo Acto — Segunda Parte</h1>
                        <h4>Se acercan, Sobre el abismo, Dar el salto</h4>
                    </header>
                    <div class="c-story__content">
                        <p>Los Ganchos no tienen motivo para apresurarse. Ya no. Saben que nos tienen atrapados. Detrás de ellos, parece que todos los asesinos y sabandijas despiadados en Aguasturbias estuvieran diciendo presente. No hay vuelta atrás.</p>
                        <p>En el extremo más lejano del puente, bloqueando mi ruta de escape hasta el laberinto de las barriadas de Aguasturbias, aparece ni más ni menos que toda la banda de los Sombreros Rojos. Ellos dominan el lado este de la ribera. Sirven a Gangplank, al igual que los Ganchos Dentados y casi toda la maldita ciudad.</p>
                        <p>Graves está a mis espaldas, acercándose más con cada pisada. Al obstinado hijo de perra no le interesa el desastre en el que nos vinimos a meter. Realmente me cuesta creerlo. Aquí estamos otra vez, como hace tantos años. Hasta las rodillas de problemas y no logro que me escuche.</p>
                        <p>Me gustaría poder contarle qué fue lo que ocurrió realmente aquel día, pero no tendría sentido. No me creería ni por un segundo. Una vez que se le aloja un pensamiento en el cráneo, extirpárselo toma su tiempo. Claramente, el tiempo es algo que no nos sobra.</p>
                        <p>Me retiro hacia un costado del puente. Cerca del riel veo malacates y poleas suspendidos debajo de mí, a muchos metros sobre el océano. Mi cabeza da vueltas y mi estómago se me cae hasta las botas. Cuando vuelvo al centro del puente, me doy cuenta de la encrucijada en la que me he metido.</p>
                        <p>A la distancia puedo ver el barco de velas negras de Gangplank. Desde ahí se nos aproxima ni más ni menos que una armada de botes a toda marcha. Al parecer todos sus hombres vienen en camino.</p>
                        <p>No puedo escapar ni de los Ganchos, ni de los Sombreros, ni del cabeza dura de Graves.</p>
                        <p>Solo me queda una salida.</p>
                        <p>Pongo un pie sobre la verja del puente. No me había percatado de la altura. El viento azota mi abrigo y hace que se agite como vela al viento. Jamás debí volver a Aguasturbias.</p>
                        <p>—Sal de ahí ahora mismo —dice Graves. Estoy seguro de haber notado una cuota de desesperación en su voz. Quedaría destrozado si muriera antes de obtener la confesión que tanto ha buscado.</p>
                        <p>Respiro profundo. La caída sí que es larga.</p>
                        <p>—Tobías —dice Malcolm—. Retrocede.</p>
                        <p>Me detengo. Hacía mucho que no escuchaba ese nombre.</p>
                        <p>Un momento después, doy el salto.</p>
                    </div>
                </section>

                <section id="story-2-part-3" class="c-story__block js-story-block js-story-block-3" data-scroll-checkpoint="part-3">
                    <header class="c-story__header js-scroll-checkpoint">
                        <img src="<?php print site_asset('/story/img/act-2/chapter-heading-act-2-part-3.png');?>" alt="Segundo Acto – Tercera Parte" class="c-story__section-header u-hidden-less-than-sm">
                        <h1>Segundo Acto – Tercera Parte</h1>
                        <h4>El espectáculo, Un observador, Cae la noche</h4>
                    </header>
                    <div class="c-story__content">
                        <p>La Hidra Descarada era una de las pocas tabernas de Aguasturbias que no tenía aserrín en el piso. No era frecuente que un trago terminara en el suelo; para que hablar de un charco de sangre. Pero esta noche, el bullicio se escuchaba hasta allá por el Risco del Saltador.</p>
                        <p>Hombres de relativa reputación y mejores recursos echaban sapos y culebras cantando melodías fantásticas sobre las peores fechorías que habían cometido.</p>
                        <p>Y ahí, en medio del tumulto, una persona conducía el jolgorio de la noche.</p>
                        <p>Se contoneaba brindando a la salud del capitán del puerto y todos sus serenos. Su brillante cabello rojo se movía con soltura y cautivaba la mirada de todos los hombres presentes, quienes de todas formas no habían puesto los ojos en nada que no fuera ella.</p>
                        <p>Aunque la sirena de cabello carmesí se había asegurado de que ninguna copa quedara vacía esta noche, los hombres no se sentían atraídos hacia ella por la mera alegría de estar borrachos. Lo que anhelaban era la gloria de contemplar su siguiente sonrisa.</p>
                        <p>Con la taberna todavía rebosando de júbilo, se abrió la puerta principal, desde donde apareció un hombre vestido de manera sobria. Pasando tan desapercibido como solo es posible tras años de práctica, caminó hacia la barra y pidió un trago.</p>
                        <p>La joven mujer tomó un vaso recién servido de cerveza ambarina de entre un mostrador destartalado.</p>
                        <p>—Amigos míos, me temo que debo retirarme —dijo la dama con un gesto dramático.</p>
                        <p>Los hombres del puerto le respondieron con sendos alaridos de protesta.</p>
                        <p>—Bueno, bueno. Ya la pasamos bien —dijo en tono de tierno reproche—. Pero tengo una noche ajetreada por delante y ustedes ya van tarde si pretenden llegar a sus puestos.</p>
                        <p>Sin bacilar, se subió a una mesa, pero antes miró a todo el mundo a su alrededor con un dejo de regocijo y triunfo.</p>
                        <p>—¡Que la Serpiente Madre tenga piedad por nuestros pecados!</p>
                        <p>Les concedió la más cautivante de sus sonrisas, se llevó la jarra a los labios y bebió la cebada de un solo trago.</p>
                        <p>—¡En especial los más grandes! —dijo golpeando el vaso contra la mesa.</p>
                        <p>Se limpió la cerveza de la boca entre un estruendo apoteósico de aprobación y le lanzó un beso a la multitud.</p>
                        <p>Enseguida todo el mundo se retiró, como súbditos tras su reina.</p>
                        <p>El amable capitán del puerto le sostuvo la puerta a la dama. Esperaba conseguir una última mirada de aprobación, pero ella ya caminaba por las calles antes de que pudiera fijarse en su cortés y tambaleante reverencia.</p>
                        <p>Fuera de la taberna, la luna se había ocultado tras el Nidal del Manumiso y las penumbras de la noche parecían extenderse hasta alcanzar a la mujer. Cada paso que la alejaba de la taberna era más resoluto y seguro que el anterior. Su fachada despreocupada se había disuelto para revelar su verdadero ser.</p>
                        <p>Ya no quedaba un ápice de lo que hace unos segundos inspiraba alegría y entusiasmo. Miró con desaliento, no hacia las calles ni a los callejones alrededor suyo, sino a lo lejos, pensando en las miles de posibilidades que traía consigo esta noche.</p>
                        <p>Detrás de ella, el hombre de atuendo sencillo de la taberna le seguía el paso. Su pisada era silenciosa, pero desconcertantemente veloz.</p>
                        <p>En el lapso de un latido, sincronizó sus pasos a la perfección con los de ella, a unos centímetros de su hombro, justo fuera de su campo visual.</p>
                        <p>—¿Está todo en orden, Rafen? —preguntó ella.</p>
                        <p>Después de todos estos años, aún no podía creer que todavía no fuera capaz de sorprenderla.</p>
                        <p>—Sí, Capitana —dijo.</p>
                        <p>—¿No te detectaron?</p>
                        <p>—No —contestó resentido, luego de controlar su disgusto por la pregunta—. El capitán del puerto no tenía a nadie vigilando y en el barco no había ni una mosca.</p>
                        <p>—¿Y el chico?</p>
                        <p>—Hizo su parte.</p>
                        <p>—Muy bien. Nos vemos en el Sirena.</p>
                        <p>Luego de recibir su orden, Rafen se alejó y desapareció entre la oscuridad.</p>
                        <p>Ella siguió adelante mientras la noche la envolvía. Todo estaba puesto en marcha. Solo faltaba que los actores empezaran con el espectáculo.</p>
                    </div>
                </section>

                <section id="story-2-part-4" class="c-story__block js-story-block js-story-block-4" data-scroll-checkpoint="part-4">
                    <header class="c-story__header js-scroll-checkpoint">
                        <img src="<?php print site_asset('/story/img/act-2/chapter-heading-act-2-part-4.png');?>" alt="Segundo Acto – Cuarta Parte" class="c-story__section-header u-hidden-less-than-sm">
                        <h1>Segundo Acto – Cuarta Parte</h1>
                        <h4>El salto, Unas botas finísimas, Naranjas</h4>
                    </header>
                    <div class="c-story__content">
                        <p>Escucho rugir a Graves mientras me zambullo. Todo lo que alcanzo a ver es la cuerda debajo de mí. No es tiempo de pensar en la caída o en las desconocidas y tétricas profundidades.</p>
                        <p>Todo se vuelve una mezcla borrosa de vientos huracanados.</p>
                        <p>Casi grito de alegría cuando alcanzo la cuerda, pero me quema la mano como un fierro al rojo vivo. Mi caída se detiene súbitamente cuando llego al punto de amarre.</p>
                        <p>Me quedo ahí por un momento, maldiciendo.</p>
                        <p>Había escuchado que caer al agua de una altura como esta no bastaba para matar a un hombre, pero prefiero correr el riesgo de lanzarme hacia el muelle de carga de piedra del que me separan al menos unos quince metros. Moriré, pero prefiero mil veces eso que ahogarme.</p>
                        <p>Entre donde estoy y la plataforma de piedra hay un par de cables de trabajo pesado que se extienden de aquí al continente, uno de ida y el otro de vuelta. Los impulsan unos mecanismos ruidosos y rudimentarios. Se utilizan para transportar partes faenadas de las bestias marinas a los mercados de todo Aguasturbias.</p>
                        <p>Los cables vibran mientras un balde pesado y oxidado, tan grande como una casa, surca su camino hacia mí.</p>
                        <p>Dejo que una sonrisa se dibuje en mi cara por un segundo. Al menos hasta que veo lo que está en el interior del carro. Estoy a punto de caer con los pies por delante a una cuba humeante de órganos de pescado.</p>
                        <p>Tardé meses en ganarme la moneda que pagué por estas botas. Flexibles como la gasa y fuertes como el acero templado, son una obra artesanal fabricada con la piel de un dragón marino abisal. Hay menos de cuatro pares en todo el mundo.</p>
                        <p>Demonios.</p>
                        <p>Coordino mi salto con precisión y aterrizo justo en medio del balde de bocado. El cebo frío se cuela por todas las fibras cosidas a mano de mis preciadas botas. Por lo menos mi sombrero sigue limpio.</p>
                        <p>De pronto escucho el ladrido de la maldita escopeta una vez más.</p>
                        <p>La línea de amarre explota.</p>
                        <p>El carro emite un chillido al liberarse de los cables. Me quedo sin aire cuando el balde se estrella contra la plataforma de piedra. Siento que los cimientos del muelle tiemblan antes de volcarse hacia un lado.</p>
                        <p>Todo cae sobre mi cabeza, incluida una tonelada de vísceras de pescado.</p>
                        <p>Lucho por mantenerme de pie mientras busco otra salida. Siento que los barcos de Gangplank se aproximan. Ya casi están aquí.</p>
                        <p>Me arrastro mareado hacia un bote pequeño atracado en el muelle de carga. No alcanzo a llegar a la mitad del tramo cuando un escopetazo le abre el casco de par en par hasta echarlo a pique.</p>
                        <p>Viendo cómo el bote se hunde, caigo al suelo de rodillas, muerto de cansancio. Trato de recuperar un poco el aliento soportando mi propio hedor. Malcolm está de pie junto a mí. De algún modo consiguió llegar hasta aquí también. Claro que pudo lograrlo.</p>
                        <p>—Ya no te ves tan elegante, ¿eh? —Graves sonríe y me mira de arriba abajo.</p>
                        <p>—¿Cuándo vas a aprender? —digo, poniéndome de pie—. Cada vez que intento ayudarte, me...</p>
                        <p>Graves le dispara al suelo en frente de mí. Estoy seguro de que algo me golpeó la espinilla. —Si solo me escucha...</p>
                        <p>—Ya te escuché lo suficiente, amigo mío —me interrumpe, mascullando cada palabra—. Era el atraco más grande de nuestras vidas y tú te escapas antes de que pudiera darme cuenta.</p>
                        <p>—¿Antes de qué? Te lo dije...</p>
                        <p>Le sigue otro disparo y otra lluvia de piedras, pero ya me tiene sin cuidado.</p>
                        <p>—Traté de que saliéramos de ahí. Todos los demás nos dimos cuenta de que nada estaba saliendo como esperábamos —le dije—. Pero tú no querías ceder. Como siempre —la carta está en mi mano antes de siquiera darme cuenta.</p>
                        <p>—Te lo dije entonces, todo lo que debías hacer era apoyarme. Habríamos salido de ahí, felices y forrados. Pero tú optaste por correr —me dice, dando un paso adelante. El hombre que solía conocer parece haberse perdido tras una capa de odio acumulada durante años.</p>
                        <p>No intento decir otra palabra. Ahora lo veo en sus ojos. Algo dentro de él se quebrantó.</p>
                        <p>Por sobre su hombro veo un resplandor; es un mosquete de chispa. La vanguardia de la tropa de Gangplank viene hacia nosotros.</p>
                        <p>Lanzo una carta sin pensarlo. Atraviesa el aire justo en dirección hacia Graves.</p>
                        <p>Su arma da un tronido.</p>
                        <p>Mi carta acaba con uno de los hombres de Gangplank. Su pistola estaba apuntando a la altura de la espalda de Malcolm.</p>
                        <p>Detrás de mí, otro miembro de su banda cae al suelo empuñando un cuchillo. Si Graves no le hubiera disparado, podría haber acabado conmigo sin más.</p>
                        <p>Ambos nos miramos. No perdimos el hábito.</p>
                        <p>Los hombres de Gangplank ahora nos rodean por todas partes, acercándose cada vez más entre aullidos y abucheos. No podemos luchar contra tantos.</p>
                        <p>A Graves eso no lo detiene. Levanta su arma y se da cuenta de que no le quedan balas.</p>
                        <p>No saco ninguna carta. No tiene sentido.</p>
                        <p>Malcolm da un rugido y se abalanza contra ellos. Esa es su forma de hacer las cosas. Con la culata de la pistola, le quiebra la nariz a un bastardo, pero la turba le da una paliza.</p>
                        <p>Siento que unas manos me agarran y me contienen los brazos. Levantan a Malcolm del suelo. Cae sangre de su rostro.</p>
                        <p>De pronto dejo de oír los gritos y aullidos de la turba. Siento escalofríos.</p>
                        <p>La muralla de matones se retira para darle paso a una silueta, un hombre con un abrigo rojo que se dirige a nosotros.</p>
                        <p>Es Gangplank.</p>
                        <p>De cerca es mucho más grande de lo que imaginaba. Y más viejo. Las líneas de su cara son profundas y definidas.</p>
                        <p>Con una mano sostiene una naranja mientras le quita la cáscara con una navaja para tallar. Lo hace lentamente, concentrándose en cada corte.</p>
                        <p>—Cuéntenme, camaradas —dice. Su voz es un gruñido ronco y profundo—. ¿Les gustan los tallados en hueso?</p>
                    </div>
                </section>

                <footer class="c-story__footer">
                    <ul class="o-list-unstyled">
                        <li class="c-story__footer-item"><a href="#story-3" class="c-btn"><span class="c-btn__content">Tercer Acto</span></a></li>
                        <li class="c-story__footer-item u-hidden-less-than-sm"><a href="#act-2" class="c-btn c-btn--dark js-toggle-overlay"><span class="c-btn__content">Explora el Mundo</span></a></li>
                    </ul>
                </footer>
            </div>
        </div>
    </div>
</article>
            <article id="article-3" class="c-story c-stories-overlay__story js-story js-story-3" style="width:25%;">
    <div class="c-scroll c-story__scroll js-scroll js-scroll-list-position" data-scroll-sync="#story-3">
        <div class="c-scroll__bar js-story-scrollbar">
            <div class="c-scroll__bar-marker js-story-scrollbar-marker"></div>
        </div>
        <ol class="o-list-unstyled c-scroll__list">
            <li class="c-scroll__list-item js-scroll-marker is-active" data-scroll-marker="part-1"><a href="#story-3-part-1" class="c-scroll__list-link js-scroll-to" data-context="story-3">1</a></li>
            <li class="c-scroll__list-item js-scroll-marker" data-scroll-marker="part-2"><a href="#story-3-part-2" class="c-scroll__list-link js-scroll-to" data-context="story-3">2</a></li>
            <li class="c-scroll__list-item js-scroll-marker" data-scroll-marker="part-3"><a href="#story-3-part-3" class="c-scroll__list-link js-scroll-to" data-context="story-3">3</a></li>
            <li class="c-scroll__list-item js-scroll-marker" data-scroll-marker="part-4"><a href="#story-3-part-4" class="c-scroll__list-link js-scroll-to" data-context="story-3">4</a></li>
        </ol>
    </div>
    <div class="c-story__graphic js-story-fade-scroll">
        <img src="<?php print site_asset('/story/img/act-3/story-backdrop-act-3.jpg');?>" class="c-story__graphic-img">
    </div>
    <div class="o-overlay__body js-scrollbar js-story-scrolling-body">
        <div class="o-overlay__container js-story-container">
            <div id="story-body-3" class="c-story__body">
                <section id="story-3-part-1" class="c-story__block js-story-block js-story-block-1" data-scroll-checkpoint="part-1">
                    <header class="c-story__header js-scroll-checkpoint">
                        <img src="<?php print site_asset('/story/img/act-3/chapter-heading-act-3-part-1.png');?>" alt="Tercer Acto - Primera Parte" class="c-story__section-header u-hidden-less-than-sm">
                        <h1>Tercer Acto - Primera Parte</h1>
                        <h4>Sangre, Verdad, La Hija de la Muerte</h4>
                    </header>
                    <div class="c-story__content">
                        <p>El puño choca contra mi cara otra vez. Me desplomo de golpe contra la cubierta del barco de Gangplank. Un par de esposas hechas de arrabio se me clavan en las muñecas.</p>
                        <p>Me ponen de pie con dificultad y me obligan a arrodillarme junto a Fate. Lo cierto es que no podría haberme puesto de pie si esta manada de matones virolentos no me hubiese obligado a hacerlo.</p>
                        <p>El enorme y musculoso imbécil que me golpeó entra y sale de mi vista.</p>
                        <p>—Vamos, hijo —le digo—. Lo estás haciendo mal.</p>
                        <p>No veo venir el siguiente golpe. Siento una explosión de dolor y mi cara vuelve a la cubierta. Vuelven a levantarme y a ponerme de rodillas. Escupo sangre y algunos dientes. Luego sonrío.</p>
                        <p>—Hijo, mi abuelita pega más fuerte que tú y eso que la enterramos hace ya cinco años.</p>
                        <p>Se acerca para golpearme otra vez, pero una palabra de Gangplank hace que se detenga enseguida.</p>
                        <p>—Es suficiente —dijo el capitán.</p>
                        <p>Bamboleando un poco, intento concentrarme en la borrosa silueta de Gangplank. Mi vista se aclara lentamente. Veo que lleva colgado en su cinturón la maldita daga que Fate intentó robarse.</p>
                        <p>—Twisted Fate, ¿no? Me dijeron que eras bueno. Y yo no soy de aquellos que menosprecian la obra de un gran ladrón —dice Gangplank. Da un paso adelante y se queda mirando a Fate—. Pero un buen ladrón sabría que es mejor evitar robarme a mí. —Se agacha y me mira fijamente a los ojos.</p>
                        <p>—Y tú... Si fueras un poco más listo, sabrías que lo mejor habría sido poner tu arma a mi servicio. Pero ya no importa.</p>
                        <p>Gangplank se levanta y nos da la espalda.</p>
                        <p>—No soy un hombre poco razonable —continúa—. No le pido a la gente que se arrodille ante mí. Todo lo que pido es un mínimo de respeto... algo sobre lo que ustedes escupieron encima. Y eso no puede quedar impune.</p>
                        <p>Su tripulación empieza a acercarse, como perros esperando la orden para despedazarnos. De cualquier manera, no me siento nervioso. No pienso darles esa satisfacción.</p>
                        <p>—Hazme un favor —le digo, apuntando hacia Fate con la cabeza—. Mátalo a él primero.</p>
                        <p>Gangplank suelta una risita.</p>
                        <p>Le hace un gesto a un hombre de su tripulación, quien comienza a hacer sonar la campana del barco. En respuesta, suenan una docena más en toda la ciudad puerto. Borrachos, marineros y tenderos empiezan a brotar de las calles, atraídos por el alboroto. El maldito quiere hacerlo público.</p>
                        <p>—Aguasturbias nos mira, muchachos —dice Gangplank—. Es tiempo de darles un espectáculo. ¡Traigan a la Hija de la Muerte!</p>
                        <p>Escucho un vitoreo mientras la cubierta retumba con el clamor de pisotones en el suelo. Traen un viejo cañón. Puede que esté oxidado y verde de lo viejo que es, pero sigue siendo una belleza.</p>
                        <p>Doy un vistazo hacia donde está Fate. Tiene la cabeza gacha y no dice una palabra. Le quitaron sus cartas... una vez que acabaron de encontrarlas. Ni siquiera le permitieron que se quedara con su estúpido sombrero de dandi; ahora veo a un pequeño malnacido usándolo entre la multitud.</p>
                        <p>De todos los años que conozco a Fate, siempre había sido capaz de hallar una salida. Ahora que está acorralado, puedo ver la derrota en su cara.</p>
                        <p>Bien.</p>
                        <p>—Es lo que te merecías, maldito infeliz —le gruño.</p>
                        <p>Él me devuelve la mirada. Todavía hay fuego en sus ojos.</p>
                        <p>—No me enorgullece la manera en que se dieron las cosas...</p>
                        <p>—¡Dejaste que me pudriera ahí adentro! —lo interrumpo.</p>
                        <p>—Yo y el resto de la tripulación intentamos sacarte de ahí. ¡Y eso les costó la vida! —responde furioso—. Perdimos a Kolt, a Wallach, al Ladrillo... a todos ellos. Y todo por tratar de salvar tu obstinado pellejo.</p>
                        <p>—Pero tú saliste sano y salvo —contesto—. ¿Sabes por qué? Porque no eres más que un cobarde. Y nada de lo que puedas decir va a cambiar eso.</p>
                        <p>Mis palabras lo hieren como puñetazos. No intenta responder. La última chispa de lucha en sus ojos se esfuma al tiempo que sus hombros se desploman. Todo terminó para él.</p>
                        <p>Ni siquiera Fate podría ser tan buen actor. Mi ira se disipa.</p>
                        <p>De pronto me siento cansado. Cansado y viejo.</p>
                        <p>—Todo se fue al diablo y supongo que ambos tenemos la culpa —dice—. Pero no te estaba mintiendo. De verdad tratamos de sacarte. Ya no importa. Solo vas a creer lo que quieras de todos modos.</p>
                        <p>Me toma un momento digerir sus palabras; me toma un poco más darme cuenta de que le creo.</p>
                        <p>Demonios, tiene razón.</p>
                        <p>Hago las cosas a mi modo. Siempre lo hice. Cada vez que me pasaba de la raya, él estaba ahí, apoyándome. Era él el que siempre hallaba una salida.</p>
                        <p>Pero no le puse atención ese día, ni lo hice desde entonces.</p>
                        <p>Por eso terminé haciendo que nos maten a ambos.</p>
                        <p>De pronto nos levantan de un tirón y nos arrastran hacia el cañón. Gangplank lo acaricia como si fuera su mascota.</p>
                        <p>—La Hija de la Muerte me ha servido bien —dice—. Hace tiempo que buscaba la oportunidad de despedirla como se merece.</p>
                        <p>Un grupo de marineros arrastra una gruesa cadena y comienza a enrollarla alrededor del cañón. Ahora me doy cuenta de lo que pretenden hacer.</p>
                        <p>Nos ponen espalda con espalda, y la misma cadena nos recorre las piernas y las esposas. Un cerrojo se cierra de golpe, atándonos a la cadena.</p>
                        <p>Una compuerta de embarque se abre en la borda del barco, hasta donde llevan al cañón a ocupar su lugar. Los mirones repletan los pantalanes y los muelles de Aguasturbias, dispuestos a presenciar el espectáculo.</p>
                        <p>Gangplank apoya el tacón de su bota en el cañón.</p>
                        <p>—Bueno, no pude librarnos de esta —dice Fate por encima del hombro—. Siempre supe que algún día serías mi ruina.</p>
                        <p>Dejo escapar una risa cuando lo dice. Ha pasado mucho tiempo desde la última vez que me reí.</p>
                        <p>Nos arrastran hasta la orilla del barco cual ganado al matadero.</p>
                        <p>Supongo que este será mi final. Tuve una buena vida mientras pude. Pero la suerte no es eterna para nadie.</p>
                        <p>Es justo ahí que me doy cuenta de lo que tengo que hacer.</p>
                        <p>Con mucho cuidado y haciendo presión contra mis esposas, logro meter una mano en el bolsillo de atrás. Sigue ahí; es la carta de Fate que encontré en la bodega. Tenía planeado usarla para metérsela en su condenada garganta.</p>
                        <p>Los matones revisaron a Fate de arriba abajo para ver si tenía cartas, pero no a mí.</p>
                        <p>Le doy un empujón. Gracias a que estamos encadenados juntos, es fácil darle la carta a Fate sin que lo noten. Puedo sentirlo titubear cuando se la entrego.</p>
                        <p>—Como tributo son insignificantes, pero al menos me servirán —dice Gangplank—. Denle mis saludos a la Gran Barbuda.</p>
                        <p>Mientras saluda a la multitud, Gangplank empuja al cañón por la borda con una patada. El armatoste cae a las aguas oscuras de un chapuzón y empieza a hundirse rápidamente. La cadena sobre el muelle cae con él un momento después. </p>
                        <p>Ahora que se acerca nuestro fin, le creo a Fate. Sé que lo intentó todo con tal de sacarme, al igual que todas las veces que trabajamos juntos. Pero en esta ocasión, y por primera vez, yo soy el que tiene la solución. Al menos puedo compensarlo con eso.</p>
                        <p>—Lárgate de aquí.</p>
                        <p>Fate empieza a hacer sus movimientos y gira la carta entre los dedos. A medida que empieza a acumular poder, siento una presión incómoda en la nuca. Siempre odié estar cerca suyo cuando hacía este truco.</p>
                        <p>De pronto, se esfuma.</p>
                        <p>Las cadenas que ataban a Fate caen al muelle estruendosamente, lo que saca algunos gritos entre la multitud. Mis cadenas siguen estando bien apretadas. Aunque no salga de esta, solo ver la expresión en la cara de Gangplank ya hace que valga la pena.</p>
                        <p>La cadena del cañón me hace caer. Me doy de bruces contra el muelle y gruño de dolor. Un instante después caigo del bote.</p>
                        <p>El agua fría me golpea y me deja sin aliento.</p>
                        <p>Estoy sumergido. Me hundo con rapidez. La oscuridad me arrastra.</p>
                    </div>
                </section>

                <section id="story-3-part-2" class="c-story__block js-story-block js-story-block-2" data-scroll-checkpoint="part-2">
                    <header class="c-story__header js-scroll-checkpoint">
                        <img src="<?php print site_asset('/story/img/act-3/chapter-heading-act-3-part-2.png');?>" alt="Tercer Acto - Segunda Parte" class="c-story__section-header u-hidden-less-than-sm">
                        <h1>Tercer Acto - Segunda Parte</h1>
                        <h4>La zambullida, Una lucha con la oscuridad, Paz</h4>
                    </header>
                    <div class="c-story__content">
                        <p>La carta que Malcolm puso en mi mano podría llevarme fácilmente hasta el muelle. Estoy tan cerca de la costa; desde ahí, desaparecer entre la multitud sería facilísimo. Podría escapar de este chiquero de isla en menos de una hora. Esta vez nadie me encontraría.</p>
                        <p>Un momento después, todo lo que veo es su cara enfurecida a medida que desaparece en las profundidades.</p>
                        <p>Maldito sea.</p>
                        <p>No puedo abandonarlo. No después de lo que pasó la última vez. Escapar no es una opción. Sé a dónde tengo que ir.</p>
                        <p>Siento que la presión se acumula y luego desaparezco.</p>
                        <p>En un instante estoy justo detrás de Gangplank, listo para hacer lo mío.</p>
                        <p>Un miembro de su tripulación me detecta sin tener explicación para cómo llegué aquí. Mientras piensa sobre el asunto, le doy un puñetazo justo en la cara. Su cuerpo cae sobre un montón de grumetes perplejos. Todos voltean a mirarme, los sables listos en sus manos. Gangplank lidera el ataque y trata de cortarme justo a la altura de la garganta.</p>
                        <p>Pero soy más rápido que él. En un único y sutil movimiento, me deslizo por debajo del acero arqueado y despojo a Gangplank de la adorada daga de plata guardada en su cinturón. Detrás de mí, escucho maldiciones que podrían quebrar el mástil en dos.</p>
                        <p>Salto hasta la cubierta y me guardo la daga en los pantalones mientras el extremo de la cadena se rompe contra la orilla del barco. Me estiro y agarro el último eslabón de acero antes de que desaparezca por la borda.</p>
                        <p>Al romperse, la cadena me tira hacia un costado y ahí es donde me doy cuenta de lo que acabo de hacer.</p>
                        <p>El agua viene hacia mí rápidamente. En ese instante eterno, cada parte de mi cuerpo quiere soltar la cadena. El hecho de ser un hombre de agua dulce que no sabe nadar me ha perseguido toda la vida. Y ahora me condenará a la muerte.</p>
                        <p>Respiro una última bocanada de aire. De pronto, el disparo de un mosquetón me perfora el hombro. Doy un grito de dolor y dejo ir mi último suspiro justo antes de que el mar me arrastre.</p>
                        <p>El agua congelada me golpea el rostro a medida que me hundo en su sofocante eternidad azul.</p>
                        <p>Es mi peor pesadilla.</p>
                        <p>Siento cómo se acumula el pánico. Trato de contenerlo. Casi me supera. Más disparos atraviesan el agua sobre mi cabeza.</p>
                        <p>Veo tiburones y mantas cerca. Pueden saborear la sangre. Me siguen mientras caigo en el abismo. Sigo hundiéndome.</p>
                        <p>Solo vive en mí el terror, de dolor no hay nada. Siento el latido de mi corazón en los oídos. Mi pecho arde. Debo evitar tragar agua. La oscuridad se retuerce en torno a mí. Es demasiada profundidad. No hay vuelta atrás. Ya lo tengo claro.</p>
                        <p>Pero todavía puedo salvar a Malcolm.</p>
                        <p>Escucho un ruido sordo bajo mis pies. Entonces, la cadena queda floja. Es el cañón, que acaba de chocar contra el suelo marino.</p>
                        <p>Uso la cadena para arrastrarme hacia las sombras. Veo una silueta más abajo. Creo que es Graves. Me arrastro con desesperación hacia él.</p>
                        <p>De pronto veo que está frente a mí, aunque a duras penas distingo el contorno de su cara. Creo que agita la cabeza para mostrar su furia porque regresé.</p>
                        <p>Voy a desmayarme. Mi brazo está entumecido y siento que me aplastan el cráneo.</p>
                        <p>Suelto la cadena para sacar la daga del pantalón. Mi mano tiembla.</p>
                        <p>Busco a ciegas en la oscuridad. Por obra de algún milagro, encuentro la cerradura de las esposas de Graves. Inserto la daga para tratar de forzarla, tal como lo hice con otras miles de cerraduras. Pero mis manos no dejan de temblar.</p>
                        <p>Incluso Graves debe estar aterrorizado. A estas alturas, sus pulmones deberían haberse rendido. La cerradura no cede.</p>
                        <p>¿Qué haría Malcolm en mi lugar?</p>
                        <p>Giro la daga. Adiós a la delicadeza. No queda más que recurrir a la fuerza bruta.</p>
                        <p>Siento que algo cede. Creo que me corté la mano. La daga cae. En dirección al abismo. Y sigue su curso... ¿Qué es ese brillo?</p>
                        <p>Justo sobre mí, percibo un rojo intenso. Rojo y naranja. Está en todas partes. Es hermoso… Así que esto es morir.</p>
                        <p>Me río.</p>
                        <p>Empiezo a tragar agua.</p>
                        <p>Es agradable.</p>
                    </div>
                </section>

                <section id="story-3-part-3" class="c-story__block js-story-block js-story-block-3" data-scroll-checkpoint="part-3">
                    <header class="c-story__header js-scroll-checkpoint">
                        <img src="<?php print site_asset('/story/img/act-3/chapter-heading-act-3-part-3.png');?>" alt="Tercer Acto - Tercera Parte" class="c-story__section-header u-hidden-less-than-sm">
                        <h1>Tercer Acto - Tercera Parte</h1>
                        <h4>Fuego y ruina, Una conclusión, La peor parte</h4>
                    </header>
                    <div class="c-story__content">
                        <p>Miss Fortune mira hacia la bahía desde la cubierta de su barco, el Sirena. Las llamas se reflejaban en sus ojos mientras trataba de asimilar el nivel de destrucción que había causado.</p>
                        <p>Lo único que quedaba del barco de Gangplank eran escombros en llamas. La tripulación había muerto en la explosión, ahogada entre todo el caos o devorada por una colonia de peces navaja.</p>
                        <p>Había sido un momento apoteósico. Una descomunal bola de fuego rodante había iluminado la noche como un nuevo sol.</p>
                        <p>La mitad de la ciudad lo había presenciado; Gangplank se había asegurado de que así fuera, tal como ella lo esperaba. Tuvo que humillar a Twisted Fate y a Graves en frente de todo Aguasturbias. Tuvo que recordarle a todo el mundo por qué es mejor no cruzarse con él. Para Gangplank, las personas solo son herramientas que puedes usar para mantener el control. Esta fue la carta que ella utilizó para matarlo.</p>
                        <p>Los gritos y las campanadas hacían eco por toda la ciudad portuaria. La noticia se propagaría como reguero de pólvora.</p>
                        <p>Gangplank está muerto.</p>
                        <p>Las comisuras de sus labios dibujaron una sonrisa.</p>
                        <p>Esta noche solo era el fruto de todos sus esfuerzos: contratar a Fate, avisarle a Graves, todo solo para distraer a Gangplank. Cobrar su venganza le había tomado años.</p>
                        <p>La sonrisa de Miss Fortune se esfumó.</p>
                        <p>Desde el momento en que él irrumpió en el taller de su familia a rostro cubierto con una pañoleta roja, ella se había estado preparando para este momento.</p>
                        <p>Sarah perdió a sus padres ese día. Aunque ella era tan solo una niña, él de todos modos fue capaz de dispararle mientras veía cómo sus padres se desangraban en el piso.</p>
                        <p>Gangplank le había enseñado una dura lección: sin importar cuán seguro puedas sentirte, todos tus logros, tus metas, tus seres queridos... en fin, tu mundo, puede derrumbarse en un abrir y cerrar de ojos.</p>
                        <p>El único error que había cometido Gangplank fue no haberse asegurado de que ella muriera. Su ira y su odio le habían permitido soportar esa dura y fría noche, y así todas las noches después de esa.</p>
                        <p>Durante quince años se dedicó a reunir todo lo que necesitaba, esperando hasta que Gangplank la olvidara, bajara la guardia y se encontrara cómodo en la vida que había construido. Solo entonces podría realmente perderlo todo. Solo entonces sabría cómo se siente perder tu hogar, perder tu mundo.</p>
                        <p>Debería sentirse dichosa, pero solo se sentía vacía.</p>
                        <p>Rafen se une a ella en la borda e interrumpe su ensimismamiento.</p>
                        <p>—Ya está —dice—. Se acabó.</p>
                        <p>—No —responde Miss Fortune—. Aún no.</p>
                        <p>Dejó de ver hacía la bahía para poner la mirada en Aguasturbias. Sarah esperaba que terminando con él acabaría con su odio. Sin embargo, lo único que logró fue desatarlo. Por primera vez desde ese día, se sentía realmente poderosa.</p>
                        <p>—Esto apenas empieza —dice—. Quiero que me traigan a todos los que hayan jurado lealtad ante él. Quiero las cabezas de sus lugartenientes colgadas en mi pared. Quema cada burdel, taberna o bodega que lleve su marca. Y quiero su cadáver.</p>
                        <p>Rafen se estremeció. Había escuchado palabras como esas alguna vez, pero nunca de su boca.</p>
                    </div>
                </section>

                <section id="story-3-part-4" class="c-story__block js-story-block js-story-block-4" data-scroll-checkpoint="part-4">
                    <header class="c-story__header js-scroll-checkpoint">
                        <img src="<?php print site_asset('/story/img/act-3/chapter-heading-act-3-part-4.png');?>" alt="Tercer Acto - Cuarta Parte" class="c-story__section-header u-hidden-less-than-sm">
                        <h1>Tercer Acto - Cuarta Parte</h1>
                        <h4>Cielo rojo, Carnada para tiburones, Reconciliación</h4>
                    </header>
                    <div class="c-story__content">
                        <p>Pensé mucho acerca de la forma en que me gustaría abandonar este mundo. ¿Encadenado como un perro en el fondo del mar? Esa no se me había ocurrido. Por fortuna, Fate logra abrir el candado de mis grilletes justo antes de dejar caer la daga.</p>
                        <p>Me desenredo de las cadenas, sediento de aire. Me doy vuelta hacia donde está Fate. El pobre no mueve un músculo. Pongo mi mano alrededor de su cuello y empiezo a patalear hacia la superficie.</p>
                        <p>A medida que subimos, todo se ilumina con un tono rojizo.</p>
                        <p>Una onda expansiva me tumba hasta que ni siquiera sé dónde está la superficie. Caen trozos de hierro. Un cañón se sumerge a unos metros, seguido de un pedazo de timón chamuscado. También hay cuerpos. Una cara cubierta de tatuajes me mira conmocionada. La cabeza cercenada luego desaparece lentamente en la oscuridad bajo nuestros pies.</p>
                        <p>Nado más rápido, con los pulmones a punto de reventar.</p>
                        <p>Una eternidad después alcanzo la superficie, tosiendo agua salada y jadeando en busca de aire. El problema es que arriba es casi irrespirable. El humo me ahoga y se me clava en los ojos. Vi muchas cosas arder en mi vida, pero jamás algo así; parece que hubieran incendiado el mundo entero.</p>
                        <p>—Maldito sea... —me escucho murmurar.</p>
                        <p>El barco de Gangplank ya no está. Hay trozos de escombros echando humo repartidos por toda la bahía. Los islotes de madera al rojo vivo colapsan por todas partes y emiten un silbido a medida que se hunden. Una vela ardiendo cae justo en frente de nosotros y casi nos arrastra a Fate y a mí por última vez. Los hombres en llamas saltan desesperados de entre los restos chamuscados al agua para acallar sus gritos. El olor se parece al apocalipsis; es una mezcla de sulfuro, ceniza y muerte, entre cabellos quemados y piel derretida.</p>
                        <p>Me fijo en Fate para ver cómo está. Me cuesta mantenerlo a flote. El desgraciado es más pesado de lo que parece y no ayuda en nada que yo tenga las costillas rotas. Encuentro un trozo humeante de casco flotando cerca de mí. Parece ser lo suficientemente sólido. Nos echo a ambos encima. No es precisamente una embarcación, pero nos servirá de todos modos.</p>
                        <p>Por primera vez puedo observar a Fate con detención. Veo que no respira. Presiono su pecho con mis puños. Justo cuando empiezo a preocuparme de dónde van a terminar sus costillas, Fate tose un montón de agua salada. Me desplomo y agito la cabeza una vez más cuando empieza a recuperar la conciencia.</p>
                        <p>—¡Maldito estúpido! ¿Para qué regresaste?</p>
                        <p>Responderme le toma un minuto.</p>
                        <p>—Pensé que podía intentar hacerlo a tu manera —murmura arrastrando las palabras—. Quería saber qué se sentía ser un cabeza dura. —Sigue tosiendo más agua—. Se siente horrible.</p>
                        <p>Los peces navaja y otras criaturas marinas incluso más viles empiezan a rodearnos. No pienso ser carnada para nadie. Aparto mis pies de la orilla.</p>
                        <p>Un tripulante mutilado aparece en la superficie y se sostiene de nuestra balsa. Le pongo la bota en la cara y lo saco flotando de mi vista. Un grueso tentáculo le recorre el cuello y lo arrastra de vuelta a lo profundo. Ahora los peces tienen algo más para distraerse.</p>
                        <p>Antes de que se queden sin carne fresca, tomo una tabla de nuestra balsa y la uso para remar lejos de la carnicería.</p>
                        <p>Empujo contra el agua por lo que parece una eternidad. Mis brazos se sienten pesados y adoloridos, pero sé muy bien que no puedo detenerme. Cuando logro alejarme un poco de la masacre, me tumbo boca arriba.</p>
                        <p>Estoy agotado como un cartucho de escopeta mientras miro hacia la bahía. Está teñida de rojo con la sangre de Gangplank y su tripulación. No hay sobrevivientes a la vista.</p>
                        <p>¿Cómo es que todavía respiro? Tal vez sea el hombre con mejor suerte de Runaterra. O tal vez la buena fortuna de Fate alcanza para ambos.</p>
                        <p>Veo un cuerpo flotando cerca con un objeto que me parece familiar. Es el pequeño malnacido que estaba con Gangplank, con el sombrero de Fate entre las manos. Se lo quito y lo lanzo hacia Fate. No veo un gesto de sorpresa en su cara, como si siempre hubiera sabido que lo iba a recuperar.</p>
                        <p>—Ahora solo falta encontrar tu arma —dice.</p>
                        <p>—¿En serio piensas ir allá abajo otra vez? —le respondo, apuntando hacia el fondo.</p>
                        <p>La tez de Fate toma un particular tono verde.</p>
                        <p>—No tenemos tiempo. Quien quiera que haya hecho esto dejó a Aguasturbias sin jefe —digo—. Esto va a ponerse feo en cualquier momento.</p>
                        <p>—¿Me estás diciendo que puedes vivir sin tu escopeta? —me pregunta.</p>
                        <p>—Tal vez no —respondo—. Pero conozco a un buen armero en Piltóver.</p>
                        <p>—Piltóver... —dice, perdido entre sus pensamientos.</p>
                        <p>—Hay mucho dinero circulando ahí en estos momentos —digo.</p>
                        <p>Fate se concentra por un momento.</p>
                        <p>—Hmm. No estoy seguro de si me gustaría que fuéramos socios de nuevo... eres más estúpido de lo que ya solías ser —dice finalmente.</p>
                        <p>—Está bien. No sé si me gustaría tener un socio que se llame Twisted Fate. ¿A quién demonios se le ocurrió ese nombrecito?</p>
                        <p>—Bueno, es mil veces mejor que mi verdadero nombre —dice Fate entre risas.</p>
                        <p>—Tienes razón —admito.</p>
                        <p>Sonrío. Se siente como en los viejos tiempos. Enseguida borro toda expresión de mi rostro y lo miro directamente a los ojos.</p>
                        <p>—Solo tengo una condición: si se te ocurre dejarme a mí otra vez cargando la bolsa, te vuelo la condenada cabeza. Y sin derecho a réplica.</p>
                        <p>Fate acalla su risa y me devuelve la mirada por un momento. Luego de un rato, sonríe.</p>
                        <p>—Es un trato.</p>
                    </div>
                </section>

                <footer class="c-story__footer">
                    <ul class="o-list-unstyled">
                        <li class="c-story__footer-item"><a href="#story-4" class="c-btn"><span class="c-btn__content">Epílogo</span></a></li>
                        <li class="c-story__footer-item u-hidden-less-than-sm"><a href="#act-3" class="c-btn c-btn--dark js-toggle-overlay"><span class="c-btn__content">Explora el Mundo</span></a></li>
                    </ul>
                </footer>
            </div>
        </div>
    </div>
</article>
            <article id="article-4" class="c-story c-stories-overlay__story js-story js-story-4" style="width:25%;">
    <div class="c-scroll c-story__scroll js-scroll js-scroll-list-position" data-scroll-sync="#story-4">
        <div class="c-scroll__bar js-story-scrollbar">
            <div class="c-scroll__bar-marker js-story-scrollbar-marker"></div>
        </div>
        <ol class="o-list-unstyled c-scroll__list">
            <li class="c-scroll__list-item js-scroll-marker is-active" data-scroll-marker="part-1"><a href="#story-4-part-1" class="c-scroll__list-link js-scroll-to" data-context="story-4">1</a></li>
        </ol>
    </div>
    <div class="c-story__graphic js-story-fade-scroll">
        <img src="<?php print site_asset('/story/img/epilogue/story-backdrop-epilogue.jpg');?>" class="c-story__graphic-img">
    </div>
    <div class="o-overlay__body js-scrollbar js-story-scrolling-body">
        <div class="o-overlay__container js-story-container">
            <div id="story-body-4" class="c-story__body">
                <section id="story-4-part-1" class="c-story__block js-story-block js-story-block-1" data-scroll-checkpoint="part-1">
                    <header class="c-story__header js-scroll-checkpoint">
                        <img src="<?php print site_asset('/story/img/epilogue/chapter-heading-epilogue.png');?>" alt="Epílogo" class="c-story__section-header u-hidden-less-than-sm">
                        <h1>Epílogo</h1>
                        <h4>Caos, El hombre arruinado, Propósito</h4>
                    </header>
                    <div class="c-story__content">
                        <p>Aguasturbias se estaba devorando a sí misma. Las calles resonaban con los alaridos de los desesperados y los moribundos. Los incendios en los barrios humildes hacían caer cenizas por toda la ciudad. El control se había perdido y ahora cada banda se daba prisa en llenar el vacío de poder tras la caída de un solo hombre. Había empezado una guerra después de que se dieran a conocer tres sencillas palabras: Gangplank está muerto.</p>
                        <p>Las ambiciones despiadadas y los resentimientos de poca monta que se habían mantenido a raya durante años ahora podían volverse algo concreto.</p>
                        <p>En los muelles, un grupo de balleneros se abalanzaron sobre un pescador rival. Lo ensartaron con arpones y dejaron su cuerpo colgando de un palangre.</p>
                        <p>En el punto más alto de la isla, las imponentes compuertas que estaban ahí desde que se fundó Aguasturbias no eran más que un recuerdo. Al cobarde líder de una banda un rival lo arrancó de su cama. Sus gritos y lloriqueos se silenciaron cuando lanzaron su cabeza contra el mármol tallado a mano de la escalera de su propia casa.</p>
                        <p>A lo largo del muelle, un Sombrero Rojo huye e intenta cortar la hemorragia de una herida en su cabeza. Mira por encima del hombro para ver si aún lo persiguen, pero no logra ver a nadie. Los Ganchos Dentados se rebelaron contra los Sombreros. El chico tuvo que volver a su escondite a advertirle al resto de sus camaradas.</p>
                        <p>Dobló la esquina y gritó para que sus compañeros reunieran sus armas y se le unieran. Pero su sed de sangre le secó la garganta. Justo frente a la guarida de los Sombreros Rojos se encontraba un grupo de Ganchos. Sus espadas goteaban sangre. A la cabeza, una figura enjuta, apenas un hombre, dibujaba una sonrisa maliciosa en su cara marcada por la viruela.</p>
                        <p>El Sombrero Rojo tuvo tiempo para maldecir una última vez.</p>
                        <p>Al otro lado de la bahía, en una callejuela oscura y silenciosa, un médico intentaba ejercer su oficio. El oro que había recibido era más que suficiente para pagar por sus servicios... y comprar su silencio.</p>
                        <p>Le había tomado media hora arrancar el abrigo empapado de la carne despellejada del brazo de su paciente. El doctor había visto lesiones horribles en su carrera, pero no pudo evitar retroceder al ver el brazo magullado. Se detuvo un momento, aterrorizado por el efecto que podrían provocar sus siguientes palabras.</p>
                        <p>—Lo... lo siento. No puedo salvar su brazo.</p>
                        <p>Entre las sombras de la habitación iluminada con velas, la ruina sangrienta de un hombre trata de componerse antes de ponerse de pie a duras penas. La mano que todavía le servía se disparó como un látigo y tomó por el cuello al tembloroso doctor. Lo levantó con mesura y lentitud del suelo y lo puso contra la pared.</p>
                        <p>Por un momento de terror, el hombre salvaje lo miró con indiferencia, pensando en qué haría con él a su merced. De pronto lo soltó.</p>
                        <p>Perdido entre el pánico y la confusión, el matasanos tosió violentamente mientras la masa ensombrecida caminaba hacia el extremo de la habitación. Atravesando la luz de la linterna del cirujano, el paciente se estiró para alcanzar el primer cajón de un armario a mal traer. De forma minuciosa, el hombre abrió cada cajón buscando lo que necesitaba. Finalmente, se detuvo.</p>
                        <p>—Todo tiene un propósito —dijo contemplando su brazo mutilado.</p>
                        <p>Sacó un objeto del estuche y lo lanzó a los pies del doctor. Ahí, a la luz de la linterna, brillaba el acero pulido de una sierra para huesos.</p>
                        <p>—Córtelo —dijo—. Tengo trabajo por hacer.</p>
                    </div>
                </section>

                <footer class="c-story__footer">
                    <ul class="o-list-unstyled">
                        <li class="c-story__footer-item u-hidden-less-than-sm"><a href="#act-3" class="c-btn c-btn--dark js-toggle-overlay"><span class="c-btn__content">Explora el Mundo</span></a></li>
                    </ul>
                </footer>
            </div>
        </div>
    </div>
</article>

        </div>

        <div id="controls-story-1" class="c-act-controls js-scene-controls js-scene-controls-1 js-act-size">
    <aside class="c-act-controls__block c-act-controls__block--right u-hidden-less-than-sm">
        <a href="#story-2" class="c-act-controls__block-link">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-right"></i></span>
        </a>
    </aside>
    <footer class="c-overlay__footer u-hidden-less-than-sm">
        <div class="c-title-group c-section__footer-title">
            <h5 class="c-title-group__subtitle"><span>La vida es un juego, juégalo</span><span></span></h5>
            <h4 class="c-title-group__title u-text-gradient" title="Primer Acto">Newline</h4>
        </div>
    </footer>
</div>

<div id="controls-story-2" class="c-act-controls js-scene-controls js-scene-controls-2 js-act-size">
    <aside class="c-act-controls__block c-act-controls__block--left u-hidden-less-than-lg">
        <a href="#story-1" class="c-act-controls__block-link">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-left c-circle__icon"></i></span>
        </a>
    </aside>
    <aside class="c-act-controls__block c-act-controls__block--right u-hidden-less-than-lg">
        <a href="#story-3" class="c-act-controls__block-link">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-right"></i></span>
        </a>
    </aside>
    <footer class="c-overlay__footer u-hidden-less-than-sm">
        <div class="c-title-group c-section__footer-title">
            <h5 class="c-title-group__subtitle"><span>Lee la historia</span><span>:</span></h5>
            <h4 class="c-title-group__title u-text-gradient" title="Segundo Acto">Segundo Acto</h4>
        </div>
    </footer>
</div>

<div id="controls-story-3" class="c-act-controls js-scene-controls js-scene-controls-3 js-act-size">
    <aside class="c-act-controls__block c-act-controls__block--left u-hidden-less-than-lg">
        <a href="#story-2" class="c-act-controls__block-link">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-left c-circle__icon"></i></span>
        </a>
    </aside>
    <aside class="c-act-controls__block c-act-controls__block--right u-hidden-less-than-lg">
        <a href="#story-4" class="c-act-controls__block-link">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-right"></i></span>
        </a>
    </aside>
    <footer class="c-overlay__footer u-hidden-less-than-sm">
        <div class="c-title-group c-section__footer-title">
            <h5 class="c-title-group__subtitle"><span>Lee la historia</span><span>:</span></h5>
            <h4 class="c-title-group__title u-text-gradient" title="Tercer Acto">Tercer Acto</h4>
        </div>
    </footer>
</div>

<div id="controls-story-4" class="c-act-controls js-scene-controls js-scene-controls-4 js-act-size">
    <aside class="c-act-controls__block c-act-controls__block--left u-hidden-less-than-lg">
        <a href="#story-3" class="c-act-controls__block-link">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-left c-circle__icon"></i></span>
        </a>
    </aside>
    <footer class="c-overlay__footer u-hidden-less-than-sm">
        <div class="c-title-group c-section__footer-title">
            <h5 class="c-title-group__subtitle"><span>Lee la historia</span><span>:</span></h5>
            <h4 class="c-title-group__title u-text-gradient" title="Epílogo">Epílogo</h4>
        </div>
    </footer>
</div>

    </div>

    <div id="brochure" class="o-overlay c-brochure js-overlay js-alert-offset" data-overlay="brochure">
        <div class="c-brochure__body js-brochure-body">
            <div class="c-brochure__wrap js-brochure-wrap">

                <div id="brochure-page-1" class="c-brochure__page">
    <div class="o-overlay__body c-brochure__overlay-body js-brocure-body js-body-scroll-brochure-1 js-scrollbar">
        <div class="o-overlay__container c-brochure__overlay-container">

            <section class="c-brochure__section">
                <h3 class="c-brochure-title"><span class="c-brochure-title__title">Peleadores de Mercado Negro</span></h3>
                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-12">
                        <div class="c-brochure-item">
                            <img src="<?php print site_asset('/story/img/brochure/bilgewater-hirelings.png');?>" alt="" class="u-img-respond">
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title u-h2">En Aguasturbias, todo tiene un precio</h3>
                            </header>
                            <div class="c-brochure-item__body c-brochure-item__body--wide">
                                <p>Tras la destrucción del Masacre, las bodegas repletas de tesoros de Gangplank fueron saqueadas y quienes seguían al temible pirata abandonaron su lealtad.</p>
                                <p>Gana y gasta krakens para contratar a súbditos peleadores y mejorar sus habilidades, ataque o defensa. Una vez contratados, reemplazarán a los súbditos regulares y serán regenerados en las oleadas de cada uno de los tres carriles durante toda la partida.</p>
                                <h3 class="u-text-yellow-light">Recibe krakens de varias formas durante el juego.</h3>
                                <ul>
                                   <li>&bull; Un kraken cada sesenta segundos</li>
                                   <li>&bull; Dos krakens por cada muerte</li>
                                   <li>&bull; Un kraken por cada asistencia</li>
                                   <li>&bull; Un kraken por cada monstruo épico eliminado</li>
                                   <li>&bull; Un kraken por matar a un monstruo grande en la jungla enemiga</li>
                                </ul>
                                <h3 class="u-text-yellow-light">Gasta krakens para mejorar a tus súbditos peleadores y obtener sinergia con tu equipo o para aprovecharte de las debilidades enemigas.</h3>
                                <ul>
                                    <li>&bull; Mejorar habilidades de peleador</li>
                                    <li>&bull; Aumentar ataque de peleador</li>
                                    <li>&bull; Aumentar defensa de peleador</li>
                                </ul>
                                <p>NOTA: Destruir un inhibidor en Peleadores de Mercado Negro no invoca supersúbditos, pero mejora drásticamente a todos los súbditos y al peleador de ese carril.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="c-brochure-item c-section-head js-body-scroll-hireling-types">
                    <img src="<?php print site_asset('/story/img/brochure/brochure-section-divider.png');?>" class="u-img-respond">
                    <header class="c-brochure-item__header">
                        <h2 class="c-brochure-item__title">Tipos de Mercenario</h2>
                    </header>
                </div>


                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item c-bundle">
                            <img src="<?php print site_asset('/story/img/brochure/merc-ironbacks.png');?>" class="c-brochure-item__image u-img-respond">
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title u-h2">Lomoférreos</h3>
                                <h5 class="c-brochure-item__subtitle">(Melé: Tanque de asedio)</h5>
                            </header>
                            <div class="c-brochure-item__body">
                                <p>Las mejoras de habilidades otorgan un escudo, reducen el daño de los súbditos o reducen el daño de las estructuras.</p>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item c-bundle">
                            <img src="<?php print site_asset('/story/img/brochure/merc-ocklepods.png');?>" class="c-brochure-item__image u-img-respond">
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title u-h2">Ocklépodos</h3>
                                <h5 class="c-brochure-item__subtitle">(Tirador: Utilidad de soporte)</h5>
                            </header>
                            <div class="c-brochure-item__body">
                                <p>Las habilidades se mejoran para otorgar escudos a los súbditos aliados, mirar la jungla cercana con efectos de adivinación y proteger contra emboscadas.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item c-bundle">
                            <img src="<?php print site_asset('/story/img/brochure/merc-plundercrabs.png');?>" class="c-brochure-item__image u-img-respond">
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title u-h2">Cangrejos Saqueadores</h3>
                                <h5 class="c-brochure-item__subtitle">(Tirador: Hostigacampeones)</h5>
                            </header>
                            <div class="c-brochure-item__body">
                                <p>Las mejoras de habilidades aumentan el control de zona, elevan la velocidad de ataque y, al final, se daña a todos los campeones enemigos en la zona de alcance.</p>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item c-bundle">
                            <img src="<?php print site_asset('/story/img/brochure/merc-razorfins.png');?>" class="c-brochure-item__image u-img-respond">
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title u-h2">Aletafiladas</h3>
                                <h5 class="c-brochure-item__subtitle">(Melé: Persiguecampeones)</h5>
                            </header>
                            <div class="c-brochure-item__body">
                                <p>Las mejoras de habilidades ayudan a los aletafiladas a perseguir campeones enemigos, causar daño verdadero y, al final, agobiar al enemigo con superioridad numérica.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="c-brochure-item c-section-head js-body-scroll-blackmarket-items">
                    <img src="<?php print site_asset('/story/img/brochure/brochure-section-divider.png');?>" class="u-img-respond">
                    <header class="c-brochure-item__header">
                        <h2 class="c-brochure-item__title">Objetos del Mercado Negro</h2>
                    </header>
                </div>

                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3433"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/lost-chapter.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Capítulo Perdido</h4>
                                    <p>El capítulo perdido de un antiguo libro.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3430"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/rite-of-ruin.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Rito de Ruina</h4>
                                    <p>Destruye edificios más rápido que nunca.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3431"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/netherside-grimoire.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Grimorio del Camino Abisal</h4>
                                    <p>Otorga un aumento de velocidad cuando los enemigos son golpeados por un hechizo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3434"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/pox-arcana.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Plaga Arcana</h4>
                                    <p>Armado en el Capítulo Perdido, este libro afecta a los enemigos golpeados por hechizos con una plaga. La plaga se puede eliminar para causar más daño.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3911"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/martyrs-gambit.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Apuesta del Mártir</h4>
                                    <p>Los tanques pueden compartir brevemente el dolor de un campeón aliado, para sacrificar algo de vida en lugar del aliado.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3742"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/dead-mans-plate.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Placa del Hombre Muerto</h4>
                                    <p>Acumula velocidad e impulso con esta fuerte armadura mientras corres antes de chocar contra tu enemigo para causar más daño.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3844"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/murksphere.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Esfera Lúgubre</h4>
                                    <p>Protege a los aliados contra el daño y a la vez recibe oro.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3841"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/swindlers-orb.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Orbe del Timador</h4>
                                    <p>Protege a los aliados contra el daño y a la vez recibe oro.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3840"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/globe-of-trust.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Globo de Confianza</h4>
                                    <p>Protege a los aliados contra el daño y a la vez recibe oro.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3652"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/typhoon-claws.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Garras del Tifón</h4>
                                    <p>Al luchar contra un enemigo, utiliza ataques de tornado periódicos.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3829"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/tricksters-glass.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Espejo del Bromista</h4>
                                    <p>Disfraza a su portador de campeón aliado para confundir a los enemigos.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3924"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/flesheater.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Comecarne</h4>
                                    <p>Alimenta a esta espada con súbditos para que su portador sane y aumente el poder del arma.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3744"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/staff-of-flowing-water.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Vara de Agua que Fluye:</h4>
                                    <p>Otorga un aumento de poder al luchar en el río, lo que brinda más regeneración de maná y velocidad de movimiento.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3745"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/puppeteer.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Titiritero</h4>
                                    <p>Aplica cadenas a los objetivos de los autoataques que se pueden jalar para desplazar a los enemigos afectados.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="3150"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/mirage-blade.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Espada Espejismo</h4>
                                    <p>Marca a los objetivos con un autoataque y luego se aleja a una distancia segura de este.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item u-text-left">
                            <div class="o-media">
                                <div class="o-media__graphic js-prevent-default" data-rg-name="item" data-rg-id="1338"><span class="c-brochure-icon"><img src="<?php print site_asset('/story/img/brochure/icons/boot-enchant-teleport.png');?>" class="u-img-full"></span></div>
                                <div class="o-media__content c-brochure-item__body">
                                    <h4 class="c-brochure-item__title">Encantamiento de Bota: Teleportación</h4>
                                    <p>Te teleporta a una unidad aliada marcada.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <section class="c-brochure__section js-body-scroll-aram">

                <h3 class="c-brochure-title"><span class="c-brochure-title__title">ARAM en el Puente del Carnicero</span></h3>

                <div class="c-brochure-item">
                    <div class="c-video-frame">
                        <div class="c-video-frame__body">
                            <div class="o-video-container"><!-- <iframe src="https://youtube.com/embed/QzXD7zbPkLM?wmode=opaque" class="o-video-container__video"></iframe> -->
                            <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
                            <div id="player" class="o-video-container__video"></div>


                            </div>
                        </div>
                    </div>
                    <header class="c-brochure-item__header">
                        <h3 class="c-brochure-item__title u-h2">Peleas cuerpo a cuerpo totalmente aleatorias en el Puente del Carnicero</h3>
                    </header>
                    <div class="c-brochure-item__body">
                        <p>Este puente de piedra fue alguna vez el camino hacia la entrada de un templo, pero ahora el Puente del Carnicero es una peligrosa conexión entre los muelles del matadero y las barriadas de Aguasturbias.</p>
                        <p><a href="http://las.leagueoflegends.com/es/page/game-update/special-event/construyendo-el-puente-del-carnicero-evento-aguasturbias" class="c-brochure-item__link" target="_blank">Lee aquí el blog de desarrolladores</a></p>
                    </div>
                </div>

            </section>
        </div>
    </div>
</div>
                <div id="brochure-page-2" class="c-brochure__page">
    <div class="o-overlay__body c-brochure__overlay-body js-brocure-body js-body-scroll-brochure-2 js-scrollbar">
        <div class="o-overlay__container c-brochure__overlay-container">

            <section class="c-brochure__section">

                <h3 class="c-brochure-title"><span class="c-brochure-title__title">Aspectos del evento</span></h3>

                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item">
                            <span class="c-character-frame c-brochure-item__frame"><span class="c-character-frame__avatar">
                                <a href="<?php print site_asset('/story/img/modal/splash-cuthroat-cutpurse.jpg');?>" rel="graves" class="c-modal-link js-modal" title="Graves Robavidas">
                                    <img src="<?php print site_asset('/story/img/brochure/skin-cutthroat-graves.png');?>" class="c-character-frame__avatar-img">
                                </a>
                                <a href="<?php print site_asset('/story/img/modal/comic-cuthroat-cutpurse.jpg');?>" rel="graves" class="u-hidden c-modal-link js-modal" title="Graves Robavidas"></a>
                                </span>
                                <span class="c-price c-character-frame__price"><span>750</span> <span class="c-price__units c-character-frame__price-units">RP</span></span></span>
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title">Graves Robavidas</h3>
                            </header>
                            <div class="c-brochure-item__body">
                                <p>De joven, Malcolm Graves aprendió el valor de la vida humana y luego comenzó a cobrar caro cuando acababa con una.</p>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item">
                            <span class="c-character-frame c-brochure-item__frame"><span class="c-character-frame__avatar">
                                <a href="<?php print site_asset('/story/img/modal/splash-cuthroat-cutpurse.jpg');?>" rel="twisted-fate" class="c-modal-link js-modal" title="Twisted Fate Robacarteras">
                                    <img src="<?php print site_asset('/story/img/brochure/skin-cutpurse-twisted-fate.png');?>" class="c-character-frame__avatar-img">
                                </a>
                                <a href="<?php print site_asset('/story/img/modal/comic-cuthroat-cutpurse.jpg');?>" rel="twisted-fate" class="u-hidden c-modal-link js-modal" title="Twisted Fate Robacarteras"></a>
                            </span><span class="c-price c-character-frame__price"><span>750</span> <span class="c-price__units c-character-frame__price-units">RP</span></span></span>
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title">Twisted Fate Robacarteras</h3>
                            </header>
                            <div class="c-brochure-item__body">
                                <p>Twisted Fate no tardó mucho en descubrir la importancia de tener siempre un as (o una daga) bajo la manga.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item">
                            <span class="c-character-frame c-brochure-item__frame"><span class="c-character-frame__avatar">
                                <a href="<?php print site_asset('/story/img/modal/splash-captain-fortune.jpg');?>" rel="fortune" class="c-modal-link js-modal" title="Capitana Fortune">
                                    <img src="<?php print site_asset('/story/img/brochure/skin-captain-fortune.png');?>" class="c-character-frame__avatar-img">
                                </a>
                                <a href="<?php print site_asset('/story/img/modal/comic-captain-fortune.jpg');?>" rel="fortune" class="u-hidden c-modal-link js-modal" title="Capitana Fortune"></a>
                            </span><span class="c-price c-character-frame__price"><span>975</span> <span class="c-price__units c-character-frame__price-units">RP</span></span></span>
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title">Capitana Fortune</h3>
                            </header>
                            <div class="c-brochure-item__body">
                                <p>Cuando las olas ardían en medio del puerto, una mujer se quedó para poner de pie a una ciudad devastada. Reclama el poder con la nueva capitana de la ciudad, la reina sin corona de Aguasturbias.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <section class="c-brochure__section js-body-scroll-bilgewater-skins">
                <h3 class="c-brochure-title"><span class="c-brochure-title__title">Aspectos inspirados en Aguasturbias</span></h3>
                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item">
                            <span class="c-character-frame c-brochure-item__frame"><span class="c-character-frame__avatar">
                                <a href="<?php print site_asset('/story/img/modal/splash-inspired-skins.jpg');?>" rel="corsair-quinn" class="c-modal-link js-modal" title="Quinn la Corsaria">
                                    <img src="<?php print site_asset('/story/img/brochure/skin-corsair-quinn.png');?>" class="c-character-frame__avatar-img">
                                </a>
                                <a href="<?php print site_asset('/story/img/modal/comic-inspired-skins.jpg');?>" rel="corsair-quinn" class="u-hidden c-modal-link js-modal" title="Quinn la Corsaria"></a>
                            </span><span class="c-price c-character-frame__price"><span>750</span> <span class="c-price__units c-character-frame__price-units">RP</span></span></span>
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title">Quinn la Corsaria</h3>
                            </header>
                            <div class="c-brochure-item__body">
                                <p>Explora el horizonte en busca de más barcos y obtén monstruos para tu flota asesina con la Corsaria Quinn.</p>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item">
                            <span class="c-character-frame c-brochure-item__frame"><span class="c-character-frame__avatar">
                                <a href="<?php print site_asset('/story/img/modal/splash-inspired-skins.jpg');?>" rel="sea-hunter-aatrox" class="c-modal-link js-modal" title="Aatrox el Cazador Marino">
                                    <img src="<?php print site_asset('/story/img/brochure/skin-sea-hunter-aatrox.png');?>" class="c-character-frame__avatar-img">
                                </a>
                                <a href="<?php print site_asset('/story/img/modal/comic-inspired-skins.jpg');?>" rel="sea-hunter-aatrox" class="u-hidden c-modal-link js-modal" title="Aatrox el Cazador Marino"></a>
                            </span><span class="c-price c-character-frame__price"><span>750</span> <span class="c-price__units c-character-frame__price-units">RP</span></span></span>
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title">Aatrox el Cazador Marino</h3>
                            </header>
                            <div class="c-brochure-item__body">
                                <p>Acecha a las salvajes bestias marinas y mata a tu presa con Aatrox el Cazador Marino.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item">
                            <span class="c-character-frame c-brochure-item__frame"><span class="c-character-frame__avatar">
                                <a href="<?php print site_asset('/story/img/modal/splash-inspired-skins.jpg');?>" rel="admiral-garen" class="c-modal-link js-modal" title="Garen el Almirante Bandolero">
                                    <img src="<?php print site_asset('/story/img/brochure/skin-rogue-admiral-garen.png');?>" class="c-character-frame__avatar-img">
                                </a>
                                <a href="<?php print site_asset('/story/img/modal/comic-inspired-skins.jpg');?>" rel="admiral-garen" class="u-hidden c-modal-link js-modal" title="Garen el Almirante Bandolero"></a>
                            </span><span class="c-price c-character-frame__price"><span>750</span> <span class="c-price__units c-character-frame__price-units">RP</span></span></span>
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title">Garen el Almirante Bandolero</h3>
                            </header>
                            <div class="c-brochure-item__body">
                                <p>Comanda un galeón cazamonstruos y trae la gloria a tu flota asesina con Garen el Almirante Bandolero.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="c-brochure__section js-body-scroll-bundles">
                <h3 class="c-brochure-title"><span class="c-brochure-title__title">Paquetes del Evento</span></h3>
                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item c-bundle">
                            <img src="<?php print site_asset('/story/img/brochure/bundle-bilgewater-veterans-bundle.png');?>" class="c-brochure-item__image u-img-respond">
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title u-h2">Paquete Veteranos de Aguasturbias</h3>
                            </header>
                            <span class="c-price c-brochure-item__price"><span>50%</span> <span class="c-price__units c-brochure-item__price-units">de descuento</span></span>
                            <div class="c-brochure-item__body">
                                <p>Artistas y diseñadores inspirados en Aguasturbias crearon aspectos alternativos de fantasía, incluso antes de que la ciudad se convirtiera en el nuevo escenario de un evento de LoL. Añade estos aspectos clásicos a tu colección con este económico paquete por 2996 RP (4118 RP si necesitas los campeones) hasta las 4:00hs MX/CO/EC/PE, 04:30hs VE, 06:00hs AR/CL/UY del 10 de agosto. Este paquete incluye a Katarina de Aguasturbias, Swain de Aguasturbias, Rumble de Aguasturbias (Legado), Por las Barbas de Fiddlesticks, Tristana la Bucanera y el Pirata Ryze (Legado).</p>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item c-bundle">
                            <img src="<?php print site_asset('/story/img/brochure/bundle-bounty-hunter-bundle.png');?>" class="c-brochure-item__image u-img-respond">
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title u-h2">Paquete Miss Fortune Cazarrecompensas</h3>
                            </header>
                            <span class="c-price c-brochure-item__price"><span>25%</span> <span class="c-price__units c-brochure-item__price-units">de descuento</span></span>
                            <div class="c-brochure-item__body">
                                <p>¡Haz alarde con el Paquete Cazarrecompensas! Este paquete de costo flexible tiene un valor de 4547 RP (5139 si necesitas a MF), dura hasta las 4:00hs MX/CO/EC/PE, 04:30hs VE, 06:00hs AR/CL/UY del 10 de agosto e incluye a Miss Fortune de Arcadia, Miss Fortune Mafiosa, Miss Fortune de Caramelo (Legado), Miss Fortune Guerrera del Camino (Legado), Miss Fortune Agente Secreta, Miss Fortune Vaquera y Miss Fortune Waterloo.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="c-brochure__section js-body-scroll-ward-skins">
                <h3 class="c-brochure-title"><span class="c-brochure-title__title">Centinelas</span></h3>
                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-4">
                        <div class="c-brochure-item">
                            <img src="<?php print site_asset('/story/img/brochure/war-time-ward.png');?>" class="u-img-respond">
                            <h4 class="c-brochure-item__title"><span>Serpiente Madre</span><br><span class="c-price"><span>640</span> <span class="c-price__units">RP</span></span></h4>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-4">
                        <div class="c-brochure-item">
                            <img src="<?php print site_asset('/story/img/brochure/peace-time-ward.png');?>" class="u-img-respond">
                            <h4 class="c-brochure-item__title"><span>Flota Asesina</span><br><span class="c-price"><span>640</span> <span class="c-price__units">RP</span></span></h4>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-4">
                        <div class="c-brochure-item">
                            <img src="<?php print site_asset('/story/img/brochure/bilgewater-faction-icon.png');?>" class="u-img-respond">
                            <h4 class="c-brochure-item__title"><span>Ícono Emblema de Aguasturbias</span><br><span class="c-price"><span>250</span> <span class="c-price__units">RP</span></span></h4>
                        </div>
                    </div>
                </div>
            </section>

            
                <section class="c-brochure__section js-body-scroll-merch">
                    <h3 class="c-brochure-title"><span class="c-brochure-title__title">Mercado</span></h3>
                    <div class="c-merchandise">
                        <div class="o-layout o-layout--no-padding">
                            <div class="o-layout__item o-layout__item--sm-6">
                                <h3 class="c-brochure-item__header">Productos</h3>
                                <div class="c-brochure-item__body c-brochure-item__body--wide">
                                    <p>Haz del evento una realidad con los productos inspirados en Aguasturbias, que incluyen figuras, pósters, mousepads, una figura de campeón premium y más.</p>
                                    <p><a href="https://las.merch.riotgames.com/es/?utm_source=microsite&utm_medium=referral&utm_campaign=bilgewater%20promo" class="c-brochure-item__link" target="_blank">¡Visita el sitio de productos ahora!</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            
        </div>

    </div>

</div>
                <div id="brochure-page-3" class="c-brochure__page">
    <div class="o-overlay__body c-brochure__overlay-body js-brocure-body js-body-scroll-brochure-3 js-scrollbar">
        <div class="o-overlay__container c-brochure__overlay-container">

            <section class="c-brochure__section">
                <h3 class="c-brochure-title"><span class="c-brochure-title__title">Actualizaciones de campeones</span></h3>
                <div class="o-layout">
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item c-bundle">
                            <a href="<?php print site_asset('/story/img/modal/splash-gangplank-revealed.jpg');?>" class="c-modal-link js-modal" title="Gangplank">
                                <img src="<?php print site_asset('/story/img/brochure/champion-update-gp-revealed.png');?>" class="c-brochure-item__image u-img-respond">
                            </a>
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title u-h2">Gangplank</h3>
                            </header>
                            <div class="c-brochure-item__body">
                                <p>Todo debe tener un propósito. Gangplank volvió a descubrir el suyo. Lee aquí más información acerca de cómo el antiguo rey pirata planea reclamar su trono y mira su paquete de aspectos actualizados.</p>
                                <p><a href="http://las.leagueoflegends.com/es/page/champion-update/gangplank-navega-pbe" class="c-brochure-item__link" target="_blank">Leer Blog de Desarrolladores</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="o-layout__item o-layout__item--sm-6">
                        <div class="c-brochure-item c-bundle">
                            <a href="<?php print site_asset('/story/img/modal/splash-miss-fortune.jpg');?>" class="c-modal-link js-modal" title="Miss Fortune">
                                <img src="<?php print site_asset('/story/img/brochure/champion-update-mf.png');?>" class="c-brochure-item__image u-img-respond">
                            </a>
                            <header class="c-brochure-item__header">
                                <h3 class="c-brochure-item__title u-h2">Miss Fortune</h3>
                            </header>
                            <div class="c-brochure-item__body">
                                <p>Los leves ajustes de juego y las pequeñas actualizaciones visuales de Miss Fortune ya están disponibles. Lee más información acerca de la actualización de la Cazarrecompensas aquí y mira su paquete de aspectos actualizados.</p>
                                <p><a href="http://las.leagueoflegends.com/es/page/champion-update/miss-fortune-llega-pbe" class="c-brochure-item__link" target="_blank">Leer Blog de Desarrolladores</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="c-brochure__section js-body-scroll-rewards">
                <h3 class="c-brochure-title"><span class="c-brochure-title__title">Recompensas de Aguasturbias: Mareas de Fuego</span></h3>

                <div class="c-cta">
                    <div class="c-cta__body">
                        <div class="o-layout o-layout--no-padding">
                            <div class="o-layout__item o-layout__item--sm-6">
                                <h4 class="c-brochure-item__header">En cada acto, elije al campeón cuya historia quieras conocer</h4>
                                <div class="c-brochure-item__body c-brochure-item__body--wide">
                                    <p>Completa los objetivos para avanzar en la historia y desbloquear recompensas de íconos de invocador</p>
                                    <p>Vive la historia del evento de Aguasturbias con cada objetivo que completes</p>
                                    <p>¡Haz clic en la sección de recompensas de Aguasturbias: Mareas de Fuego de tu cliente del juego para empezar a ganar premios ya!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            
            <section class="c-brochure__section js-body-scroll-events">
                <h3 class="c-brochure-title"><span class="c-brochure-title__title">Fiestas de Abordaje</span></h3>

                <div class="c-brochure-item c-bundle">
                    <img src="<?php print site_asset('/story/img/brochure/boarding-parties.png');?>" alt="Sube a bordo de los Eventos de la Comunidad" class="c-brochure-item__image u-img-respond">
                    <header class="c-brochure-item__header">
                        <h3 class="c-brochure-item__title u-h2">Sube a bordo de los Eventos de la Comunidad</h3>
                    </header>
                    <div class="c-brochure-item__body">
                        <p>¡Aguasturbias se apodera de los Eventos de la Comunidad! Por tiempo limitado, inscríbete en Fiestas de Abordaje especiales, programadas en lugares en tu región o en línea de todo el mundo. Regístrate en una Fiesta de Abordaje y únete a otros jugadores aventureros como tú para completar el nuevo mapa de ARAM el Puente del Carnicero y el nuevo modo de juego Peleadores de Mercado Negro.</p>
                        <ul class="o-list-inline">
                            <li><a href="http://events.las.leagueoflegends.com/" class="c-brochure-item__link" target="_blank">Ver todos los eventos</a></li>

                                                                                                <li>|</li>
                                                            <li><a href="http://events.las.leagueoflegends.com/events/new" class="c-brochure-item__link" target="_blank">Crear evento</a></li>

                            
                        </ul>
                    </div>
                </div>

            </section>

            
            <section class="c-brochure__section js-body-scroll-papercraft">
                <h3 class="c-brochure-title"><span class="c-brochure-title__title">Figuras de papel</span></h3>

                <div class="c-brochure-item c-bundle">
                    <img src="<?php print site_asset('/story/img/brochure/paper-sets.png');?>" alt="Haz clic abajo para descargar las plantillas y luego comparte tus creaciones en línea con los hashtags #bilgewater y #Aguasturbias." class="c-brochure-item__image u-img-respond">
                    <header class="c-brochure-item__header">
                        <h3 class="c-brochure-item__title u-h2">Únete a la Campaña</h3>
                    </header>
                    <div class="c-brochure-item__body">
                        <p>Cuenta tu propia historia de Aguasturbias: Mareas de Fuego, imprimiendo y armando uno (o tres) de estos campeones de papel.<br> <span class="u-text-yellow-light">Haz clic abajo para descargar las plantillas y luego comparte tus creaciones en línea con los hashtags #bilgewater y #Aguasturbias.</span></p>

                        <p><a href="http://lolstatic-a.akamaihd.net/events/static/bilgewater-papercraft.pdf" class="c-brochure-item__link" target="_blank">Gangplank, Twisted Fate, Graves</a></p>
                    </div>
                </div>

            </section>

        </div>
    </div>
</div>

            </div>
        </div>

        <div id="brochure-controls-1" class="c-act-controls js-brochure-controls js-brochure-controls-1">
    <aside class="c-act-controls__block c-act-controls__block--right u-hidden-less-than-lg">
        <a href="#brochure-2" class="c-act-controls__block-link js-brochure-navigation" data-target-brochure-page="1">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-right c-circle__icon"></i></span>
        </a>
    </aside>
    <footer class="c-overlay__footer u-hidden-less-than-sm">
        <div class="c-title-group c-section__footer-title">
            <h5 class="c-title-group__subtitle"><span>Vive el Evento</span></h5>
            <h4 class="c-title-group__title u-text-gradient" title="Primer Acto">Navegar las Mareas de Fuego</h4>
        </div>
    </footer>
</div>

<div id="brochure-controls-2" class="c-act-controls js-brochure-controls js-brochure-controls-2">
    <aside class="c-act-controls__block c-act-controls__block--left u-hidden-less-than-lg">
        <a href="#brochure-1" class="c-act-controls__block-link js-brochure-navigation" data-target-brochure-page="0">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-left c-circle__icon"></i></span>
        </a>
    </aside>
    <aside class="c-act-controls__block c-act-controls__block--right u-hidden-less-than-lg">
        <a href="#brochure-3" class="c-act-controls__block-link js-brochure-navigation" data-target-brochure-page="2">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-right c-circle__icon"></i></span>
        </a>
    </aside>
    <footer class="c-overlay__footer u-hidden-less-than-sm">
        <div class="c-title-group c-section__footer-title">
            <h5 class="c-title-group__subtitle"><span>Vive el Evento</span></h5>
            <h4 class="c-title-group__title u-text-gradient" title="Primer Acto">Saquear Botín de Aguasturbias</h4>
        </div>
    </footer>
</div>

<div id="brochure-controls-3" class="c-act-controls js-brochure-controls js-brochure-controls-3">
    <aside class="c-act-controls__block c-act-controls__block--left u-hidden-less-than-lg">
        <a href="#brochure-2" class="c-act-controls__block-link js-brochure-navigation" data-target-brochure-page="1">
            <span class="c-circle c-act-controls__block-icon"><i class="c-icon c-icon--arrow-left c-circle__icon"></i></span>
        </a>
    </aside>
    <footer class="c-overlay__footer u-hidden-less-than-sm">
        <div class="c-title-group c-section__footer-title">
            <h5 class="c-title-group__subtitle"><span>Vive el Evento</span></h5>
            <h4 class="c-title-group__title u-text-gradient" title="Primer Acto">Pelear por Aguasturbias</h4>
        </div>
    </footer>
</div>

    </div>

    <header class="c-header js-header js-alert-offset">
    <button class="c-circle c-header__close js-toggle-header"><i class="c-icon c-icon--close">Cerrar Menú</i></button>
    <a href="#">
        <h1 class="c-logo c-logo--bilgewater-small c-header__logo u-hidden-less-than-sm"></h1>
    </a>
    <ul class="o-list-unstyled c-nav">
        <li class="c-nav__item js-toggle-sublist js-current-section" data-sublist="1" data-current-section="story">
            <span class="c-nav__link">Lee la historia</span>
            <ul class="o-list-unstyled c-nav__sub-list js-sublist" style="display: none;">
                <li class="c-nav__sub-item js-current-page" data-current-page="story-1"><a href="#story-1" class="c-nav__sub-link js-close-header">Primer Acto</a></li>
                <li class="c-nav__sub-item js-current-page" data-current-page="story-2"><a href="#story-2" class="c-nav__sub-link js-close-header">Segundo Acto</a></li>
                <li class="c-nav__sub-item js-current-page" data-current-page="story-3"><a href="#story-3" class="c-nav__sub-link js-close-header">Tercer Acto</a></li>
                <li class="c-nav__sub-item js-current-page" data-current-page="story-4"><a href="#story-4" class="c-nav__sub-link js-close-header">Epílogo</a></li>
            </ul>
        </li>
        <li class="c-nav__item u-hidden-less-than-sm js-toggle-sublist js-current-section" data-sublist="2" data-current-section="act">
            <span class="c-nav__link">Explora el Mundo</span>
            <ul class="o-list-unstyled c-nav__sub-list js-sublist" style="display: none;">
                <li class="c-nav__sub-item js-current-page" data-current-page="act-1"><a href="#act-1" class="c-nav__sub-link js-close-header">La Bodega</a></li>
                <li class="c-nav__sub-item js-current-page" data-current-page="act-2"><a href="#act-2" class="c-nav__sub-link js-close-header">Los Muelles del Matadero</a></li>
                <li class="c-nav__sub-item js-current-page" data-current-page="act-3"><a href="#act-3" class="c-nav__sub-link js-close-header">El Masacre</a></li>
            </ul>
        </li>
        <li class="c-nav__item js-toggle-sublist js-current-section" data-sublist="3" data-current-section="brochure">
            <span class="c-nav__link">Vive el Evento</span>
            <ul class="o-list-unstyled c-nav__sub-list js-sublist" style="display: none;">
                <li class="c-nav__sub-item js-current-page" data-current-page="brochure-1"><a href="#brochure-1" class="c-nav__sub-link js-close-header">Navegar las Mareas de Fuego</a></li>
                <li class="c-nav__sub-item js-current-page" data-current-page="brochure-2"><a href="#brochure-2" class="c-nav__sub-link js-close-header">Saquear Botín de Aguasturbias</a></li>
                <li class="c-nav__sub-item js-current-page" data-current-page="brochure-3"><a href="#brochure-3" class="c-nav__sub-link js-close-header">Pelear por Aguasturbias</a></li>
            </ul>
        </li>
    </ul>
</header>

    <div id="intro" class="o-overlay c-intro js-intro js-overlay js-section js-alert-offset js-scrollbar" data-overlay="intro">

    <div class="o-fullscreen">
        <div class="o-fullscreen__content">
            <div class="c-intro__content">
                <img src="<?php print site_asset('/story/img/crown.png');?>" class="c-intro__img js-intro-graphic u-hidden-xs-only">
                <h1 class="c-intro__title js-intro-title"><span class="c-intro__subtitle">Episodio I</span> La Corona</h1>
                <ul class="o-list-unstyled c-intro__controls js-intro-controls">
                    <li class="c-intro__controls-item"><a href="#story-1" class="c-btn c-btn--default c-btn--block"><span class="c-btn__content">Lee la <span class="c-btn__block">Historia</span></span></a></li>
                    <!---li class="c-intro__controls-item u-hidden-less-than-sm"><a href="#act-1" class="c-btn c-btn--default c-btn--alt c-btn--block"><span class="c-btn__content">Explora el <span class="c-btn__block">Mundo</span></span></a></li>
                    <li class="c-intro__controls-item"><a href="#brochure-1" class="c-btn c-btn--default c-btn--block js-toggle-overlay"><span class="c-btn__content">Vive el <span class="c-btn__block">Evento</span></span></a></li-->
                </ul>
                <div class="u-hidden-less-than-xs js-intro-desc">
                    <h4>El mundo de Aden ha tocado su fin.</h4>
                    <!--p>Si bien Twisted Fate y Graves lograron escapar, Aguasturbias se vuelve un caos a medida que las calles retumban con los gritos de los desesperados y moribundos. Se inició una guerra a poco de escucharse tres palabras: Gangplank está muerto.</p-->
                </div>
            </div>
        </div>
    </div>
</div>

<!--div class="c-options c-options--bottom c-options--left c-options--on-top js-overlay" data-overlay="intro">
    <ul class="o-list-inline">
        <li><a href="http://las.leagueoflegends.com/es/" class="c-logo c-logo--lol" target="_blank">League of Legends</a></li>
        <li><a href="#" class="js-toggle-footer">Legal [<span class="js-toggle-footer-indicator">+</span>]</a></li>
    </ul>
</div>

    <div class="c-options c-options--top c-options--left js-alert-offset">
        <ul class="o-list-unstyled c-options__list">
            <li class="c-options__list-item"><button class="c-circle js-toggle-header"><i class="c-icon c-icon--navicon"></i></button></li>
        </ul>
    </div-->

    <div class="c-options c-options--top c-options--right u-hidden-less-than-sm js-alert-offset">
        <button class="c-circle js-toggle-audio"><i class="c-icon c-icon--volume-mute js-audio-icon-mute" style="display: none;"></i><i class="c-icon c-icon--volume-low js-audio-icon-play"></i></button>
    </div>

    <footer class="c-footer js-footer">

    <div class="c-footer__content">

        <ul class="o-list-inline">
            <li><a href="http://www.riotgames.com/" class="c-logo c-logo--riot" target="_blank">Riot Games</a></li>
                            <li><img src="<?php print site_asset('/story/img/ratings/rating_es_MX.jpg');?>" class="c-footer__rating" alt=""></li>
                    </ul>

        <p>© 2015 Riot Games Inc. Todos los derechos reservados. Riot Games, League of Legends y PvP.net son marcas comerciales, marcas de servicios o marcas registradas de Riot Games, Inc.</p>

        <ul class="o-list-inline c-footer__links">
            <li><a href="http://las.leagueoflegends.com/es/legal/eula" class="c-footer__link">ALUF</a></li>
            <li><a href="http://las.leagueoflegends.com/es/legal/termsofuse" class="c-footer__link">Términos de Uso</a></li>
            <li><a href="http://las.leagueoflegends.com/es/legal/privacy" class="c-footer__link">Política de Privacidad</a></li>
                            <li><a href="http://las.leagueoflegends.com/es/legal/tribunal" class="c-footer__link">Política del Tribunal</a></li>
                    </ul>

    </div>

    <div class="c-options c-options--bottom c-options--right c-options--absolute">

        <ul class="o-list-inline c-social">

                        <li class="c-social__item">
                <a href="http://leagueoflegends.tumblr.com/" target="_blank">
                    <i class="c-icon c-icon--tumblr">Tumblr</i>
                </a>
            </li>
            
                        <li class="c-social__item">
                <a href="https://plus.google.com/+leagueoflegends/posts" target="_blank">
                    <i class="c-icon c-icon--google-plus">Google Plus</i>
                </a>
            </li>
            
            <li class="c-social__item">
                <a href="https://twitter.com/leagueoflegends" target="_blank">
                    <i class="c-icon c-icon--twitter">Twitter</i>
                </a>
            </li>
            <li class="c-social__item">
                <a href="https://www.facebook.com/LeagueofLegendsLatino" target="_blank">
                    <i class="c-icon c-icon--facebook">Facebook</i>
                </a>
            </li>

            

        </ul>

    </div><!-- /.o-options -->

</footer><!-- /.c-footer -->