<div class="col-md-12 col-xs-12 content text-shadow border-radius">
  <div class="col-md-12 col-xs-12 single">
    <div class="col-md-12 col-xs-12 no-pad light-gold main-title">Capitulo I "El principio del cambio" Año 1650 ADG</div>
      <div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
        <span class="gold">Redline</span> | 28 de Diciembre del 2014
      </div>
      <div class="col-md-12 col-xs-12 text">
        <center><p>Solo voy a contarles lo que recuerdo del principio de esta historia. Del resto... Preferiría ni hablar por ahora</p><br>
		<img src="<?php print site_asset('img/history/img1.png');?>"><br><br>
		Hace unos meses, o al menos eso creo, el sistema para algunos paso a ser perfecto. 
		Los dioses de Aden proveían de vuestra vida. En las guerras y en cualquier rutina diaria,
		si uno moría…<br> En cuestión de segundos volvería a estar con vida. Desde mi opinión; ese fue el comienzo de este caos.<br><br>
		<img src="<?php print site_asset('img/history/img2.png');?>"><br><br>
		Nuestro antiguo mundo se vio afectado por la ira de los dioses, una cantidad infinita de desastres naturales arremetían
		constantemente nuestro planeta. Las victimas? Aun me niego a creer que sean todos, aunque algunos se esfuercen por demostrar lo contrario.
		Por el momento, entre todos los seres que se encuentran en este limbo (así es como la mayoría ha decidido llamarle),
		solo me he reencontrado con una vieja amiga, y aún ni ella y yo sabemos algo de los nuestros.<br><br>
		<img src="<?php print site_asset('img/history/img3.png');?>"><br><br>
		Las esperanzas aquí cada vez son menos, con el correr del tiempo todas las razas van perdiendo poco a poco sus sentimientos.
		Al principio pensaba que era tan solo un síntoma de este nuevo sistema de combate, si es que así puede llamársele. 
		Pero cuando nos toco a nosotras, entendí que estaba equivocada… Ya que hasta el día de hoy jamás hemos luchado. 
		Lo primero en presentarse fue la falta de apetito, aquí todos llevan meses enteros sin alimentarse, lo segundo fue el sueño…
		Los primeros días cuando el sol caía, mi cuerpo agotado de tanto llorar se dormía rendido. Hoy creo cumplir el octavo día sin dormir...
        Y aunque todo esto sea una milésima parte de todo lo que viene ocurriendo y ya suene terrible, desde aquí parece que todas las razas
		se han acostumbrado a esto, una lucha constante sin fin que no deja nada, un futuro sin sentimientos, un vacio infinito,
		un maldito infierno en llamas.</center>
		
      </div>
    </div>
  </div>
</div>