<div class="c-novela-progress js-novela-progress-position" style="right: 188.5px;">

    <div class="c-novela-progress__bar">
        <div class="c-novela-progress__bar-meter js-novela-progress-meter" style="height: 86.3408%;"></div>
    </div>

    <ol class="o-list-unstyled c-novela-progress__list js-progress-marker-list">

                        
                                <li class="c-novela-progress__list-item js-position-marker" style="top: 0%">
                <a id="marker-1" href="#part-1" class="c-novela-progress__list-marker js-marker js-scroll-to is-active">I</a>
            </li>

        
                                <li class="c-novela-progress__list-item js-position-marker" style="top: 29.7207%;">
                <a id="marker-2" href="#part-2" class="c-novela-progress__list-marker js-marker js-scroll-to is-active">II</a>
            </li>

        
                                <li class="c-novela-progress__list-item js-position-marker" style="top: 46.566%;">
                <a id="marker-3" href="#part-3" class="c-novela-progress__list-marker js-marker js-scroll-to is-active">III</a>
            </li>

        
                                <li class="c-novela-progress__list-item js-position-marker" style="top: 82.092%;">
                <a id="marker-4" href="#part-4" class="c-novela-progress__list-marker js-marker js-scroll-to is-active">IV</a>
            </li>

        
    </ol>

</div>

<div class="col-md-12 col-xs-12 content text-shadow border-radius">
  <div class="col-md-12 col-xs-12 single">
    <div class="col-md-12 col-xs-12 no-pad light-gold main-title">Capitulo I "El principio del cambio" Año 1650 ADG</div>
      <div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
        <span class="gold">Redline</span> | 28 de Diciembre del 2014
      </div>
      <div class="col-md-12 col-xs-12 text">
      	<h1>Capítulo 1</h1>
        <center><p>Solo voy a contarles lo que recuerdo del principio de esta historia. Del resto... Preferiría ni hablar por ahora</p><br>
		<img src="<?php print site_asset('img/history/img1.png');?>"><br><br>
		Hace unos meses, o al menos eso creo, el sistema para algunos paso a ser perfecto. 
		Los dioses de Aden proveían de vuestra vida. En las guerras y en cualquier rutina diaria,
		si uno moría…<br> En cuestión de segundos volvería a estar con vida. Desde mi opinión; ese fue el comienzo de este caos.<br><br>
		<img src="<?php print site_asset('img/history/img2.png');?>"><br><br>
		Nuestro antiguo mundo se vio afectado por la ira de los dioses, una cantidad infinita de desastres naturales arremetían
		constantemente nuestro planeta. Las victimas? Aun me niego a creer que sean todos, aunque algunos se esfuercen por demostrar lo contrario.
		Por el momento, entre todos los seres que se encuentran en este limbo (así es como la mayoría ha decidido llamarle),
		solo me he reencontrado con una vieja amiga, y aún ni ella y yo sabemos algo de los nuestros.<br><br>
		<img src="<?php print site_asset('img/history/img3.png');?>"><br><br>
		Las esperanzas aquí cada vez son menos, con el correr del tiempo todas las razas van perdiendo poco a poco sus sentimientos.
		Al principio pensaba que era tan solo un síntoma de este nuevo sistema de combate, si es que así puede llamársele. 
		Pero cuando nos toco a nosotras, entendí que estaba equivocada… Ya que hasta el día de hoy jamás hemos luchado. 
		Lo primero en presentarse fue la falta de apetito, aquí todos llevan meses enteros sin alimentarse, lo segundo fue el sueño…
		Los primeros días cuando el sol caía, mi cuerpo agotado de tanto llorar se dormía rendido. Hoy creo cumplir el octavo día sin dormir...
        Y aunque todo esto sea una milésima parte de todo lo que viene ocurriendo y ya suene terrible, desde aquí parece que todas las razas
		se han acostumbrado a esto, una lucha constante sin fin que no deja nada, un futuro sin sentimientos, un vacio infinito,
		un maldito infierno en llamas.</center>
		

      	<hr>
      	<h1>Capítulo 2</h1>
        <center><p>El tiempo sigue transcurriendo y estos últimos dos años sin duda que no han sido fáciles para nadie.<br>
        Pero hoy después de tanto tiempo la esperanza vuelve a renacer en la mayoría de nosotros,
        viéndose reflejada en un valiente pequeñaco humano, "Drago".<br><br>
        <img src="/assets/009/img/history/img4.png"><br><span style="color: #fffff;"><br>Mientras tanto en el limbo...</span><br><br>
        - Oye Drago! Lo he estado pensando y… No crees que deberíamos adentrarnos en algún campo de nuevo? Esto aquí abuuuurre tío,
        y aún yo no pierdo ese sentimiento, vale?<br>
        - Yair no te cansas de meterte en problemas verdad? Ya escuchaste a tu madre y a mi padre.<br>
        - Vaaaamos drago! Que es lo peor que podría pasarnos acá, cuál sería el castigo eh? Aaaanda será la última esta vez, lo prometo.<br>
        - Cuantas veces ya te habré escuchado decir eso....<br><br>
        <img src="/assets/009/img/history/img5.png"><br><br>
        -No nos rendiremos! No bajaremos los brazos ahora. Somos guerreros! Y nacemos luchando por los nuestros…
        Puede que estemos perdiendo algunos sentimientos, pero jamás perderémos nuestra esencia,
        ya que nos corre por las venas! - Gritaba el rey Regane ante una multitud.<br>
        -Já! que iluso es Regane… Aún cree que puede seguir liderando desde aquí arriba, cuando ni si quiera podía hacerlo en Giran. – Se burlaba Yair.<br>
        -Mi padre sigue siendo uno de sus fieles seguidores y lo respeta, a tal punto de seguir usando los portales por él.
        Y puedo asegurarte que no es el único, así que nada me sorprendre.
        Solo quisiera que todo vuelva a ser como antes tío… - Respondía desilusionado Drago.<br><br>
        <img src="/assets/009/img/history/img6.png"><br><br>
        Los valientes pequeñacos se decidían a usar otra vez más los portales. Un nuevo combate los estaba esperando,
        una batalla que nadie olvidaría jamás. Una batalla que quedaría en la historia.<br><br>
        - Oye Drago<br>
        - Si?<br>
        - Solo ten cuidado por donde andas, quieres?– Le aconsejaba Yair.<br>
        - Recuerdalo. Somos guerreros… nacemos luchando - Respondía Drago.<br>
        - Jajaja, que así sea entonces tío!<br>
        - Menos charla y mas acción pequeñacos! – Gritaba Frand, uno de los enanos más fuertes del mundo.<br><br>
        <img src="/assets/009/img/history/img7.png"><br><br>
        Y así, Drago, Yair y su equipo, comenzaban la batalla de sus vidas. Gracias a los portales,
        he tenido el placer de ser uno de los espectadores y supongo que por ello, mi fe por Drago es mucho mayor.
        Se notaba la tensión en el aire desde un comienzo, cada estrategia de ambos equipos terminaba contrarrestando la anterior. 
        Los aliados de Drago, en muchos momentos se vieron perdidos, sin embargo fue él en el medio del caos, quien los alentó 
        a seguir mostrándoles el camino con su espada dorada.<br> Ese pequeñaco estaba dejando su vida en el combate.
        Y cuando todo parecía perdido… él cambio la historia con una jugada magistral.<br><br>
        El equipo rival invadía su base y bien consciente era de esto. Sus aliado, al no saber donde se encontraba,
        intentaban defender el Nexus posicionándose detrás de sus torres, 
        de la manera más defensiva posible para no sufrir ningúna perdida.
        Pero sabían que la derrota estaba a un abrir y cerrar de ojos... De repente, se escucho la voz que nadie esperaba oir:
        “El inhibidor enemigo ha sido destruido”. El equipo rival un tanto desconcertado se dio cuenta de que Drago estaba
        destruyendo su base, por lo que no les quedo otro remedio que retroceder para defender su propio Nexus. 
        Al regresar vieron que el pequeñaco estaba a punto de destruir el corazón del lado oscuro y salieron a correrlo. 
        Drago estando solo contra todos, no tuvo otro remedio que huir, pero la partida ya estaba ganada.
        Sus aliados aprovecharon la ocasión para terminar de derribar el nexus rival 
        cuando el equipo contrario dejo desprotejida su base, llegando así, a una victoria realmente épica.<br><br>
        <img src="/assets/009/img/history/img8.png"><br><br>
        Como en toda partida al finalizar, los participantes se quedaron totalmente paralizados por unos breves segundos antes de regresar al limbo,
        pero esta vez a Drago no le pasaría. El muchacho siguió corriendo pensando que sus rivales aún lo estaban persiguiendo,
        pero cuando miro hacia atrás, se dio cuenta que ya no había nadie. Sorprendido, comenzó a mirar hacia todos lados y fue en ese entonces,
        donde encontró una corona brillante tirada en el piso. Cuando se digno a sujetarla, el portal lo regreso nuevamente al limbo,
        donde la noticia ya había empezado a correr.<br><br>
        - Ei drago, drago ven aquí!<br>
        - Ahora no Asteric. Que sucede?<br>
        - Escucha, lo eh visto todo en el portal y no fui el único, vi el momento en el que te apareció esa corona enfrente tuyo mágicamente. 
        Me dejas verla?<br>
        - No sería lo indicado Asteric, no aquí en el medio del limbo, ya me eh metido en bastante problemas últimamente, no crees?.<br>
        - No quiero ser negativo ni mucho menos Drago, pero dudo mucho que puedas escaparte de esta, tu padre ha ido tras de ti cuando 
        se entero que estabas en los campos de nuevo, y por lo visto, no ha entrado a tu misma batalla.
        Ademas, mientras venia hacia aquí corriendo, eh escuchado varios murmullos de otros individuos que vieron también lo ocurrido en esa batalla.
        Tío… ellos piensa que eres el nuevo elegido, y que su única posibilidad de salir de esta pesadilla, ahora eres tú.<br>
        - Que locura todo esto, mi padre no me hablara por algún tiempo… Creo Asteric, que será lo mejor dejar todo esto por un tiempo…<br>
        - Ey su voluntad! Porque no le pide a los dioses una buena cerveza eh? – Gritaba exaltado Yair<br>
        - Yair baja la voz ya! Aún no entiendes en el lio en el que me has metido verdad? – Contestaba Drago enojado<br>
        - Vamos tío relájate, no sería tan malo tener un poooquito mas de poder que el resto aquí, no crees?<br>
        - Yair tu definitivamente perdiste varios sentimientos, ya te has olvidado lo que ocurrió en el mundo al que realmente pertenecemos?
        Te has olvidado de mi hermano? Eh? A caso te has olvidado de Kelly? Aún no los encuentro maldita seas!<br><br>
        La corona de Drago calló de sus manos, y se elevo sola hasta la parte superior de su cabeza.
        Emanando una luz clara y enceguesedora, que cubrió cada rincón del limbo.
        Parecía como si estuviese conectada con él, como si esta entendiese todo el dolor que llevaba dentro.</center>
        <hr>

	    <h1>Capítulo 3</h1>
        <center><p><br><img src="/assets/009/img/history/img9.png"><br><br>- Asteric, ya no sé cuanto más pueda aguantar esto. Mi cabeza no deja de pensar en miles
        de cosas ultimamente. Principalmente en como se supone que le diré a mi hermano, si es que algún día vuelvo a encontrarlo,
        que nuestro padre se ha ido realmente por intentar protegerme, de las actitudes infantiles de siempre...<br>
        -Drago no imagino cuán difícil es para ti todo esto, ni tampoco imagino cuánto pesa esa corona que llevas puesta en tu frente
        simplemente por obligación. Sé que quieres reencontrar a tu padre, pero piénsalo bien hermano, de todas las veces que hemos luchado allí,
        cuantas veces has visto algo diferente? Siempre es el mismo Spheal de mierda. Pero allá en el limbo Drago, mi familia y miles de 
        personas más de razas y tonos de pieles diferentes, esperan ansiosos a que siempre regreses.
        Creen en ti, tienen fe ciega en tu corona. Te has vuelto su esperanza hace tiempo…
        Y podría decirte que también eres la mía. Solo pido que dejes de arriesgarte, te necesitamos vivo Drago.<br>
        - Déjame pensarlo tranquilo Asteric. Gracias por el consejo de todos modos, veré que hare mañana..<br><br>
        <img src="/assets/009/img/history/img10.png"><br><span style="color: #fffff;"><br>Al día siguiente...</span><br><br>
        - Shane. Necesito que me autorices a subir? - Precionaba drago al guardia.<br>
        - Lo siento chico, ya sabes que ordenes son ordenes, y Regane fue muy claro cuando dijo que aquí nadie sube sin su autorización.<br>
        - Solo dile que si no me llaman en el transcurso de las dos horas siguientes, voy a iniciar una revolución.
        Y si te pregunta quién será el cabecilla de ella, dile que esta corona... También habla.<br>
        - Tranquilo Drago, sabes que le hare llegar tu mensaje.<br><br>
        Ahora en el Limbo Regane estaba "al mando". Había aprovechado de mala manera la dura situación que Drago estaba viviendo.
        Sin embargo, yo estaba seguro de que si Drago se proponía a derrocarlo, lo haría sin ningún tipo de problema. 
        La gente seguía creyendo en él, tenían fe ciega en su corona.<br><br>
        Quince minutos más tarde...<br>
        <img src="/assets/009/img/history/img11.png"><br><br>
        - Drago, te buscan arriba - El guardia no había tardado en volver.<br><br>
        Drago se alisto rapido y se decidio a subir.<br><br>
        - Regane creo simplemente que es hora de irte - Amenazaba Drago al Rey<br>
        - Ei drago... Tranquilo, todavía estas alterado por lo de tu padre verdad? - Retrucaba Regane dandole en el punto más debil<br>
        - Hijo de …<br>
        - Eieiei calmaos, calmaos. Disculpa Drago es que mis guardias se toman todo muy personal,
        solo te llamaba para que veas esta bonita corona. Por cierto, muy similar a la tuya no crees?<br>
        - Maldito desgraciado, a caso te atreviste a duplicarla?<br>
        - Duplicarla? Já! Yo talvez lo llamaría de otra manera... Energía portalera? nonono, El elegido de los dioooooses! esa si me gusta más, 
        vamos que se valla este malcriado de una vez.<br>
        - Voy a matarte! <br>
        - Que tan seguro estas de eso pequeñaco?<br><br>
        Drago se lanzo de lleno ante los guardias derribándolos a todos con su imponente armadura, pegó un gran salto estirando sus espadas doradas
        para asesinar a Regane, pero justo antes de alcanzarlo, los dioses volvieron a actuar. 
        Teletransportaron a ambos a un nuevo campo de batalla suspendido en el aire.<br>
        Se abrió un portal en la ciudad nunca antes visto que mostraba dicha escena. 
        Todos en el Limbo se quedaron atónitos, sospechando de entrada que solo de uno de ellos regresaría vivo.</center><br><br>
        <p align="right">Continuará...</p>
      </div>
    </div>
  </div>
</div>