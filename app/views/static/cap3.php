<div class="col-md-12 col-xs-12 content text-shadow border-radius">
  <div class="col-md-12 col-xs-12 single">
    <div class="col-md-12 col-xs-12 no-pad light-gold main-title">Capitulo III "Aclaman al nuevo Héroe" Año 1653 ADG</div>
      <div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
        <span class="gold">Redline</span> | 28 de Diciembre del 2014
      </div>
      <div class="col-md-12 col-xs-12 text">
        <center><p><br><img src="/assets/009/img/history/img9.png"><br><br>- Asteric, ya no sé cuanto más pueda aguantar esto. Mi cabeza no deja de pensar en miles
        de cosas ultimamente. Principalmente en como se supone que le diré a mi hermano, si es que algún día vuelvo a encontrarlo,
        que nuestro padre se ha ido realmente por intentar protegerme, de las actitudes infantiles de siempre...<br>
        -Drago no imagino cuán difícil es para ti todo esto, ni tampoco imagino cuánto pesa esa corona que llevas puesta en tu frente
        simplemente por obligación. Sé que quieres reencontrar a tu padre, pero piénsalo bien hermano, de todas las veces que hemos luchado allí,
        cuantas veces has visto algo diferente? Siempre es el mismo Spheal de mierda. Pero allá en el limbo Drago, mi familia y miles de 
        personas más de razas y tonos de pieles diferentes, esperan ansiosos a que siempre regreses.
        Creen en ti, tienen fe ciega en tu corona. Te has vuelto su esperanza hace tiempo…
        Y podría decirte que también eres la mía. Solo pido que dejes de arriesgarte, te necesitamos vivo Drago.<br>
        - Déjame pensarlo tranquilo Asteric. Gracias por el consejo de todos modos, veré que hare mañana..<br><br>
        <img src="/assets/009/img/history/img10.png"><br><span style="color: #fffff;"><br>Al día siguiente...</span><br><br>
        - Shane. Necesito que me autorices a subir? - Precionaba drago al guardia.<br>
        - Lo siento chico, ya sabes que ordenes son ordenes, y Regane fue muy claro cuando dijo que aquí nadie sube sin su autorización.<br>
        - Solo dile que si no me llaman en el transcurso de las dos horas siguientes, voy a iniciar una revolución.
        Y si te pregunta quién será el cabecilla de ella, dile que esta corona... También habla.<br>
        - Tranquilo Drago, sabes que le hare llegar tu mensaje.<br><br>
        Ahora en el Limbo Regane estaba "al mando". Había aprovechado de mala manera la dura situación que Drago estaba viviendo.
        Sin embargo, yo estaba seguro de que si Drago se proponía a derrocarlo, lo haría sin ningún tipo de problema. 
        La gente seguía creyendo en él, tenían fe ciega en su corona.<br><br>
        Quince minutos más tarde...<br>
        <img src="/assets/009/img/history/img11.png"><br><br>
        - Drago, te buscan arriba - El guardia no había tardado en volver.<br><br>
        Drago se alisto rapido y se decidio a subir.<br><br>
        - Regane creo simplemente que es hora de irte - Amenazaba Drago al Rey<br>
        - Ei drago... Tranquilo, todavía estas alterado por lo de tu padre verdad? - Retrucaba Regane dandole en el punto más debil<br>
        - Hijo de …<br>
        - Eieiei calmaos, calmaos. Disculpa Drago es que mis guardias se toman todo muy personal,
        solo te llamaba para que veas esta bonita corona. Por cierto, muy similar a la tuya no crees?<br>
        - Maldito desgraciado, a caso te atreviste a duplicarla?<br>
        - Duplicarla? Já! Yo talvez lo llamaría de otra manera... Energía portalera? nonono, El elegido de los dioooooses! esa si me gusta más, 
        vamos que se valla este malcriado de una vez.<br>
        - Voy a matarte! <br>
        - Que tan seguro estas de eso pequeñaco?<br><br>
        Drago se lanzo de lleno ante los guardias derribándolos a todos con su imponente armadura, pegó un gran salto estirando sus espadas doradas
        para asesinar a Regane, pero justo antes de alcanzarlo, los dioses volvieron a actuar. 
        Teletransportaron a ambos a un nuevo campo de batalla suspendido en el aire.<br>
        Se abrió un portal en la ciudad nunca antes visto que mostraba dicha escena. 
        Todos en el Limbo se quedaron atónitos, sospechando de entrada que solo de uno de ellos regresaría vivo.</center><br><br>
        <p align="right">Continuará...</p>
      </div>
    </div>
  </div>
</div>