<div class="col-md-12 col-xs-12 content text-shadow border-radius">
  <div class="col-md-12 col-xs-12 single">
    <div class="col-md-12 col-xs-12 no-pad light-gold main-title">Capitulo II "La Corona" Año 1652 ADG</div>
      <div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
        <span class="gold">Redline</span> | 28 de Diciembre del 2014
      </div>
      <div class="col-md-12 col-xs-12 text">
        <center><p>El tiempo sigue transcurriendo y estos últimos dos años sin duda que no han sido fáciles para nadie.<br>
        Pero hoy después de tanto tiempo la esperanza vuelve a renacer en la mayoría de nosotros,
        viéndose reflejada en un valiente pequeñaco humano, "Drago".<br><br>
        <img src="/assets/009/img/history/img4.png"><br><span style="color: #fffff;"><br>Mientras tanto en el limbo...</span><br><br>
        - Oye Drago! Lo he estado pensando y… No crees que deberíamos adentrarnos en algún campo de nuevo? Esto aquí abuuuurre tío,
        y aún yo no pierdo ese sentimiento, vale?<br>
        - Yair no te cansas de meterte en problemas verdad? Ya escuchaste a tu madre y a mi padre.<br>
        - Vaaaamos drago! Que es lo peor que podría pasarnos acá, cuál sería el castigo eh? Aaaanda será la última esta vez, lo prometo.<br>
        - Cuantas veces ya te habré escuchado decir eso....<br><br>
        <img src="/assets/009/img/history/img5.png"><br><br>
        -No nos rendiremos! No bajaremos los brazos ahora. Somos guerreros! Y nacemos luchando por los nuestros…
        Puede que estemos perdiendo algunos sentimientos, pero jamás perderémos nuestra esencia,
        ya que nos corre por las venas! - Gritaba el rey Regane ante una multitud.<br>
        -Já! que iluso es Regane… Aún cree que puede seguir liderando desde aquí arriba, cuando ni si quiera podía hacerlo en Giran. – Se burlaba Yair.<br>
        -Mi padre sigue siendo uno de sus fieles seguidores y lo respeta, a tal punto de seguir usando los portales por él.
        Y puedo asegurarte que no es el único, así que nada me sorprendre.
        Solo quisiera que todo vuelva a ser como antes tío… - Respondía desilusionado Drago.<br><br>
        <img src="/assets/009/img/history/img6.png"><br><br>
        Los valientes pequeñacos se decidían a usar otra vez más los portales. Un nuevo combate los estaba esperando,
        una batalla que nadie olvidaría jamás. Una batalla que quedaría en la historia.<br><br>
        - Oye Drago<br>
        - Si?<br>
        - Solo ten cuidado por donde andas, quieres?– Le aconsejaba Yair.<br>
        - Recuerdalo. Somos guerreros… nacemos luchando - Respondía Drago.<br>
        - Jajaja, que así sea entonces tío!<br>
        - Menos charla y mas acción pequeñacos! – Gritaba Frand, uno de los enanos más fuertes del mundo.<br><br>
        <img src="/assets/009/img/history/img7.png"><br><br>
        Y así, Drago, Yair y su equipo, comenzaban la batalla de sus vidas. Gracias a los portales,
        he tenido el placer de ser uno de los espectadores y supongo que por ello, mi fe por Drago es mucho mayor.
        Se notaba la tensión en el aire desde un comienzo, cada estrategia de ambos equipos terminaba contrarrestando la anterior. 
        Los aliados de Drago, en muchos momentos se vieron perdidos, sin embargo fue él en el medio del caos, quien los alentó 
        a seguir mostrándoles el camino con su espada dorada.<br> Ese pequeñaco estaba dejando su vida en el combate.
        Y cuando todo parecía perdido… él cambio la historia con una jugada magistral.<br><br>
        El equipo rival invadía su base y bien consciente era de esto. Sus aliado, al no saber donde se encontraba,
        intentaban defender el Nexus posicionándose detrás de sus torres, 
        de la manera más defensiva posible para no sufrir ningúna perdida.
        Pero sabían que la derrota estaba a un abrir y cerrar de ojos... De repente, se escucho la voz que nadie esperaba oir:
        “El inhibidor enemigo ha sido destruido”. El equipo rival un tanto desconcertado se dio cuenta de que Drago estaba
        destruyendo su base, por lo que no les quedo otro remedio que retroceder para defender su propio Nexus. 
        Al regresar vieron que el pequeñaco estaba a punto de destruir el corazón del lado oscuro y salieron a correrlo. 
        Drago estando solo contra todos, no tuvo otro remedio que huir, pero la partida ya estaba ganada.
        Sus aliados aprovecharon la ocasión para terminar de derribar el nexus rival 
        cuando el equipo contrario dejo desprotejida su base, llegando así, a una victoria realmente épica.<br><br>
        <img src="/assets/009/img/history/img8.png"><br><br>
        Como en toda partida al finalizar, los participantes se quedaron totalmente paralizados por unos breves segundos antes de regresar al limbo,
        pero esta vez a Drago no le pasaría. El muchacho siguió corriendo pensando que sus rivales aún lo estaban persiguiendo,
        pero cuando miro hacia atrás, se dio cuenta que ya no había nadie. Sorprendido, comenzó a mirar hacia todos lados y fue en ese entonces,
        donde encontró una corona brillante tirada en el piso. Cuando se digno a sujetarla, el portal lo regreso nuevamente al limbo,
        donde la noticia ya había empezado a correr.<br><br>
        - Ei drago, drago ven aquí!<br>
        - Ahora no Asteric. Que sucede?<br>
        - Escucha, lo eh visto todo en el portal y no fui el único, vi el momento en el que te apareció esa corona enfrente tuyo mágicamente. 
        Me dejas verla?<br>
        - No sería lo indicado Asteric, no aquí en el medio del limbo, ya me eh metido en bastante problemas últimamente, no crees?.<br>
        - No quiero ser negativo ni mucho menos Drago, pero dudo mucho que puedas escaparte de esta, tu padre ha ido tras de ti cuando 
        se entero que estabas en los campos de nuevo, y por lo visto, no ha entrado a tu misma batalla.
        Ademas, mientras venia hacia aquí corriendo, eh escuchado varios murmullos de otros individuos que vieron también lo ocurrido en esa batalla.
        Tío… ellos piensa que eres el nuevo elegido, y que su única posibilidad de salir de esta pesadilla, ahora eres tú.<br>
        - Que locura todo esto, mi padre no me hablara por algún tiempo… Creo Asteric, que será lo mejor dejar todo esto por un tiempo…<br>
        - Ey su voluntad! Porque no le pide a los dioses una buena cerveza eh? – Gritaba exaltado Yair<br>
        - Yair baja la voz ya! Aún no entiendes en el lio en el que me has metido verdad? – Contestaba Drago enojado<br>
        - Vamos tío relájate, no sería tan malo tener un poooquito mas de poder que el resto aquí, no crees?<br>
        - Yair tu definitivamente perdiste varios sentimientos, ya te has olvidado lo que ocurrió en el mundo al que realmente pertenecemos?
        Te has olvidado de mi hermano? Eh? A caso te has olvidado de Kelly? Aún no los encuentro maldita seas!<br><br>
        La corona de Drago calló de sus manos, y se elevo sola hasta la parte superior de su cabeza.
        Emanando una luz clara y enceguesedora, que cubrió cada rincón del limbo.
        Parecía como si estuviese conectada con él, como si esta entendiese todo el dolor que llevaba dentro.</center>
      </div>
    </div>
  </div>
</div>