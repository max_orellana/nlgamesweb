    <a id="skip-to-content" href="#content" class="u-sr-only">Saltar a contenido principal</a>

    <!--div class="c-nav js-nav is-active">
    <div class="c-nav__content">
        <div class="c-nav__section">
            <ul class="o-list-unstyled c-nav__list">
                <li class="c-nav__list-item">
                    <a href="/" class="c-nav__list-link">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.4 23" width="24.4" height="23" class="c-nav__list-svg">
                            <path d="M20.7 23h-6c-.3 0-.5-.2-.5-.5V16h-4v6.5c0 .3-.2.5-.5.5h-6c-.3 0-.5-.2-.5-.5V12h1v10h5v-6.5c0-.3.2-.5.5-.5h5c.3 0 .5.2.5.5V22h5v-9.5h1v10c0 .3-.2.5-.5.5z"/>
                            <path d="M23.7 12.7L12.2 1.2.7 12.7 0 12 11.9.1c.1-.1.5-.1.7 0L24.4 12l-.7.7zM20.2 5.5h-1V2h-3.5V1h4c.3 0 .5.2.5.5v4z"/>
                        </svg>
                    </a>
                </li>
            </ul>

        </div>
        <div class="c-nav__section">

            <ul class="o-list-unstyled c-nav__list">
                <li class="c-nav__list-item c-nav__list-item--divider u-hidden@less-than-sm"></li>
                <li class="c-nav__list-item">
                    <span class="c-nav__list-link">Capítulo</span>
                </li>

                
                    
                        <li class="c-nav__list-item c-nav__list-item--divider"></li>
                        <li class="c-nav__list-item">
                            <a href="#part-1" class="c-nav__list-link">I</a>
                        </li>

                    
                
                    
                        <li class="c-nav__list-item c-nav__list-item--divider"></li>
                        <li class="c-nav__list-item">
                            <a href="#part-2" class="c-nav__list-link">II</a>
                        </li>

                    
                
                    
                        <li class="c-nav__list-item c-nav__list-item--divider"></li>
                        <li class="c-nav__list-item">
                            <a href="#part-3" class="c-nav__list-link">III</a>
                        </li>

                    
                
                    
                
                    <li class="c-nav__list-item c-nav__list-item--divider u-hidden@less-than-sm"></li>
                    <li class="c-nav__list-item u-hidden@less-than-sm">
                        <a href="http://lolstatic-a.akamaihd.net/story/Shadow_and_Fortune_LAN-ES.pdf" class="c-nav__list-link c-nav__list-link--plain" download>Descarga la historia</a>
                    </li>

                
            </ul>

        </div>

                
        <div class="c-nav__section c-nav__section--right">

            <ul class="o-list-unstyled c-nav__list">
                <li class="c-nav__list-item u-hidden@less-than-sm">
                    <span class="c-nav__list-link c-nav__list-link--plain">Explorar más</span>
                </li>
                <li class="c-nav__list-item c-nav__list-item--divider u-hidden@less-than-sm"></li>

             
            </ul>

        </div-->

        
    </div>

    <!--button class="c-btn c-nav__toggle js-nav-toggle">
        <span class="u-sr-only"></span>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 14" class="c-nav__toggle-icon js-nav-toggle-icon" style="display: none" data-nav-icon-state="false">
            <path d="M0 0h18v1H0zM0 6h18v1H0zM0 12h18v1H0z"/>
        </svg>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12.4 18" class="c-nav__toggle-icon js-nav-toggle-icon"  data-nav-icon-state="true">
            <path d="M12.4 6.2L6.2 0 0 6.5l.7.7 5-5.2v16h1V1.9l5 5"/>
        </svg>
    </button-->

</div><!-- /.c-nav -->

        <div id="content" class="o-fullscreen@sm">

            

    
<div class="o-wrap c-novela js-novela">

    <div class="c-novela__graphic js-novela-graphic">
        <img src="<?php print site_asset('/splash/history/c1-backdrop.jpg');?>" alt="" class="c-novela__graphic-img 1">
        <img src="<?php print site_asset('/splash/history/c2-backdrop.jpg');?>" alt="" class="c-novela__graphic-img 2 hide">
        <img src="<?php print site_asset('/splash/history/c3-backdrop.jpg');?>" alt="" class="c-novela__graphic-img 3 hide">
    </div>

    <div class="c-novela__body">
                       
        
                        
            <section id="part-1" class="c-novela-block js-novela-block">


                <header class="c-novela-header">

                        <img src="<?php print site_asset('/splash/history/novela-emblem.png');?>" alt="" class="c-novela-header__crest">
                        <h1 class="c-novela-header__title" title="Capítulo Uno">Capítulo I</h1>
                        <h5 class="c-novela-header__chapter">El Principio del Cambio</h5>
                    
                    <div class="c-novela-block__marker">I</div>

                    <span class="js-scroll-logging" data-message="part-1-top"></span>

                </header>

                
                                       
                        <p>Solo voy a contarles lo que recuerdo del principio de esta historia. Del resto... Preferiría ni hablar por ahora.</p>

                    
                
                                       
                                       
                        <p>Hace unos meses, o al menos eso creo, el sistema para algunos paso a ser perfecto.</p>

                    
                
                                        
                                        
                        <p>Los dioses de Aden proveían de vuestra vida. En las guerras y en cualquier rutina diaria, si uno moría…</p>

                    
                
                                        
                                        
                        <p>En cuestión de segundos volvería a estar con vida. Desde mi opinión; ese fue el comienzo de este caos.</p>

                    
                
                                        
                                       
                        <p>Nuestro antiguo mundo se vio afectado por la ira de los dioses, una cantidad infinita de desastres naturales arremetían constantemente nuestro planeta. Las victimas? Aun me niego a creer que sean todos, aunque algunos se esfuercen por demostrar lo contrario. Por el momento, entre todos los seres que se encuentran en este limbo (así es como la mayoría ha decidido llamarle), solo me he reencontrado con una vieja amiga, y aún ni ella y yo sabemos algo de los nuestros.</p>
                    


                
                                        
                                        
                        <p>Las esperanzas aquí cada vez son menos, con el correr del tiempo todas las razas van perdiendo poco a poco sus sentimientos.</p>

                    
                
                                        
                                        
                        <p>Al principio pensaba que era tan solo un síntoma de este nuevo sistema de combate, si es que así puede llamársele.</p>

                    
                
                                        
                                        
                        <p>Pero cuando nos toco a nosotras, entendí que estaba equivocada… Ya que hasta el día de hoy jamás hemos luchado.</p>

                    
                
                                        
                                        
                        <p>Lo primero en presentarse fue la falta de apetito, aquí todos llevan meses enteros sin alimentarse, lo segundo fue el sueño…</p>

                    
                
                                        
                                        
                        <p>Los primeros días cuando el sol caía, mi cuerpo agotado de tanto llorar se dormía rendido. Hoy creo cumplir el octavo día sin dormir...</p>

                    
                
                                        
                                        
                        <p>Y aunque todo esto sea una milésima parte de todo lo que viene ocurriendo y ya suene terrible, desde aquí parece que todas las razas se han acostumbrado a esto, una lucha constante sin fin que no deja nada, un futuro sin sentimientos, un vacio infinito, un maldito infierno en llamas.</p>                   



                    
                
                <span class="js-scroll-logging" data-message="part-1-bottom"></span>

            </section><!-- /.c-novela-block -->

        
                        
            <section id="part-2" class="c-novela-block js-novela-block">


                <header class="c-novela-header">

                    <h1 class="c-novela-header__title" title="Capítulo Uno">Capítulo II</h1>
                    <h5 class="c-novela-header__chapter">La Corona</h5>

                                        
                    <div class="c-novela-block__marker">II</div>

                    <span class="js-scroll-logging" data-message="part-2-top"></span>

                </header>

                
                                        
                                        
                        <p>El tiempo sigue transcurriendo y estos últimos dos años sin duda que no han sido fáciles para nadie.</p>

                    
                
                                        
                                        
                        <p>Pero hoy después de tanto tiempo la esperanza vuelve a renacer en la mayoría de nosotros, viéndose reflejada en un valiente pequeñaco humano, "Drago".</p>

                    
                
                                        
                                        
                        <p>—Oye Drago! Lo he estado pensando y… No crees que deberíamos adentrarnos en algún campo de nuevo? Esto aquí abuuuurre tío, y aún yo no pierdo ese sentimiento, vale?</p>

                    
                
                                        
                                        
                        <p>—Yair no te cansas de meterte en problemas verdad? Ya escuchaste a tu madre y a mi padre.</p>

                    
                
                                        
                                        
                        <p>—Vaaaamos drago! Que es lo peor que podría pasarnos acá, cuál sería el castigo eh? Aaaanda será la última esta vez, lo prometo.</p>

                    
                
                                        
                                        
                        <p>—Cuantas veces ya te habré escuchado decir eso....</p>

                    
                
                                        
                                        
                        <p>—No nos rendiremos! No bajaremos los brazos ahora. Somos guerreros! Y nacemos luchando por los nuestros…</p>

                    
                
                                        
                                        
                        <p>Puede que estemos perdiendo algunos sentimientos, pero jamás perderémos nuestra esencia, ya que nos corre por las venas! - Gritaba el rey Regane ante una multitud.</p>

                    
                
                                        
                                        
                        <p>—Já! que iluso es Regane… Aún cree que puede seguir liderando desde aquí arriba, cuando ni si quiera podía hacerlo en Giran. – Se burlaba Yair.</p>

                    
                
                                        
                                        
                        <p>—Mi padre sigue siendo uno de sus fieles seguidores y lo respeta, a tal punto de seguir usando los portales por él.</p>

                    
                
                                        
                                        
                        <p>Y puedo asegurarte que no es el único, así que nada me sorprendre.</p>

                    
                
                                        
                                        
                        <p>Solo quisiera que todo vuelva a ser como antes tío… - Respondía desilusionado Drago.</p>

                    
                
                                       
                                        
                        <p>Los valientes pequeñacos se decidían a usar otra vez más los portales. Un nuevo combate los estaba esperando,
        una batalla que nadie olvidaría jamás. Una batalla que quedaría en la historia.</p>

                    
                
                                        
                                        
                        <p>—Oye Drago</p>

                    
                
                                        
                                        
                        <p>—Si?</p>

                    
                
                                        
                                        
                        <p>—Solo ten cuidado por donde andas, quieres?– Le aconsejaba Yair.</p>





                        <p>—Recuerdalo. Somos guerreros… nacemos luchando - Respondía Drago.</p>





                        <p>—Jajaja, que así sea entonces tío!</p>





                        <p>—Menos charla y mas acción pequeñacos! – Gritaba Frand, uno de los enanos más fuertes del mundo.</p>





                        <p>Y así, Drago, Yair y su equipo, comenzaban la batalla de sus vidas. Gracias a los portales, he tenido el placer de ser uno de los espectadores y supongo que por ello, mi fe por Drago es mucho mayor.</p>





                        <p>Se notaba la tensión en el aire desde un comienzo, cada estrategia de ambos equipos terminaba contrarrestando la anterior. </p>





                        <p>Los aliados de Drago, en muchos momentos se vieron perdidos, sin embargo fue él en el medio del caos, quien los alentó a seguir mostrándoles el camino con su espada dorada.</p>





                        <p>Y cuando todo parecía perdido… él cambio la historia con una jugada magistral.</p>





                        <p>El equipo rival invadía su base y bien consciente era de esto. Sus aliado, al no saber donde se encontraba, intentaban defender el Nexus posicionándose detrás de sus torres, de la manera más defensiva posible para no sufrir ningúna perdida.</p>





                        <p>Pero sabían que la derrota estaba a un abrir y cerrar de ojos... De repente, se escucho la voz que nadie esperaba oir:</p>




                        <p>“El inhibidor enemigo ha sido destruido”.</p>



                        <p>El equipo rival un tanto desconcertado se dio cuenta de que Drago estaba destruyendo su base, por lo que no les quedo otro remedio que retroceder para defender su propio Nexus.</p>






                        <p>Al regresar vieron que el pequeñaco estaba a punto de destruir el corazón del lado oscuro y salieron a correrlo. </p>
                        <p>Drago estando solo contra todos, no tuvo otro remedio que huir, pero la partida ya estaba ganada.</p>
                        <p>Sus aliados aprovecharon la ocasión para terminar de derribar el nexus rival cuando el equipo contrario dejo desprotejida su base, llegando así, a una victoria realmente épica.</p>

                        <p>Como en toda partida al finalizar, los participantes se quedaron totalmente paralizados por unos breves segundos antes de regresar al limbo, pero esta vez a Drago no le pasaría. El muchacho siguió corriendo pensando que sus rivales aún lo estaban persiguiendo, pero cuando miro hacia atrás, se dio cuenta que ya no había nadie. Sorprendido, comenzó a mirar hacia todos lados y fue en ese entonces, donde encontró una corona brillante tirada en el piso. Cuando se digno a sujetarla, el portal lo regreso nuevamente al limbo, donde la noticia ya había empezado a correr.</p>
                        <p>—Ei drago, drago ven aquí!</p>
                        <p>—Ahora no Asteric. Que sucede?</p>
                        <p>—Escucha, lo eh visto todo en el portal y no fui el único, vi el momento en el que te apareció esa corona enfrente tuyo mágicamente. Me dejas verla?</p>
                        <p>—No sería lo indicado Asteric, no aquí en el medio del limbo, ya me eh metido en bastante problemas últimamente, no crees?.</p>
                        <p>—No quiero ser negativo ni mucho menos Drago, pero dudo mucho que puedas escaparte de esta, tu padre ha ido tras de ti cuando se entero que estabas en los campos de nuevo, y por lo visto, no ha entrado a tu misma batalla.</p>
                        <p>Ademas, mientras venia hacia aquí corriendo, eh escuchado varios murmullos de otros individuos que vieron también lo ocurrido en esa batalla. Tío… ellos piensa que eres el nuevo elegido, y que su única posibilidad de salir de esta pesadilla, ahora eres tú.</p>
                        <p>—Que locura todo esto, mi padre no me hablara por algún tiempo… Creo Asteric, que será lo mejor dejar todo esto por un tiempo…</p>
                        <p>—Ey su voluntad! Porque no le pide a los dioses una buena cerveza eh? – Gritaba exaltado Yair</p>
                        <p>—Yair baja la voz ya! Aún no entiendes en el lio en el que me has metido verdad? – Contestaba Drago enojado</p>
                        <p>—Vamos tío relájate, no sería tan malo tener un poooquito mas de poder que el resto aquí, no crees?</p>
                        <p>—Yair tu definitivamente perdiste varios sentimientos, ya te has olvidado lo que ocurrió en el mundo al que realmente pertenecemos?</p>
                        <p>Te has olvidado de mi hermano? Eh? A caso te has olvidado de Kelly? Aún no los encuentro maldita seas!</p>
                        <p>La corona de Drago cayó de sus manos, y se elevo sola hasta la parte superior de su cabeza.</p>
                        <p>Emanando una luz clara y enceguesedora, que cubrió cada rincón del limbo.</p>
                        <p>Parecía como si estuviese conectada con él, como si esta entendiese todo el dolor que llevaba dentro.</p>

                
                <span class="js-scroll-logging" data-message="part-2-bottom"></span>

            </section><!-- /.c-novela-block -->

        
                        
            <section id="part-3" class="c-novela-block js-novela-block">


                <header class="c-novela-header">

                    <h1 class="c-novela-header__title" title="Capítulo Uno">Capítulo III</h1>
                    <h5 class="c-novela-header__chapter">Aclaman al Nuevo Héroe</h5>


                                        
                    <div class="c-novela-block__marker">III</div>

                    <span class="js-scroll-logging" data-message="part-3-top"></span>

                </header>

                
                                        
                        <p>—Asteric, ya no sé cuanto más pueda aguantar esto. Mi cabeza no deja de pensar en miles de cosas ultimamente.</p>

                    
                
                                        
                                        
                        <p>Principalmente en como se supone que le diré a mi hermano, si es que algún día vuelvo a encontrarlo, que nuestro padre se ha ido realmente por intentar protegerme, de las actitudes infantiles de siempre...</p>

                    
                
                                        
                                        
                        <p>—Drago no imagino cuán difícil es para ti todo esto, ni tampoco imagino cuánto pesa esa corona que llevas puesta en tu frente simplemente por obligación.</p>

                    
                
                                        
                                        
                        <p>Sé que quieres reencontrar a tu padre, pero piénsalo bien hermano, de todas las veces que hemos luchado allí, cuantas veces has visto algo diferente? Siempre es el mismo Spheal de mierda.</p>

                    
                
                                        
                                        
                        <p>Pero allá en el limbo Drago, mi familia y miles de personas más de razas y tonos de pieles diferentes, esperan ansiosos a que siempre regreses.</p>

                    
                
                                        
                                        
                        <p>Creen en ti, tienen fe ciega en tu corona. Te has vuelto su esperanza hace tiempo…</p>

                    
                
                                        
                                        
                        <p>Y podría decirte que también eres la mía. Solo pido que dejes de arriesgarte, te necesitamos vivo Drago.</p>

                    
                
                                        
                                        
                        <p>—Déjame pensarlo tranquilo Asteric. Gracias por el consejo de todos modos, veré que hare mañana..</p>

                    
                
                                        
                                        
                        <p><span style="color: #fffff;"><br>Al día siguiente...</span></p>

                    
                
                                        
                                        
                        <p>—Shane. Necesito que me autorices a subir? - Precionaba drago al guardia.</p>

                    
                
                                        
                                        
                        <p>—Lo siento chico, ya sabes que ordenes son ordenes, y Regane fue muy claro cuando dijo que aquí nadie sube sin su autorización.</p>

                    
                
                                        
                                        
                        <p>—Quién sabe —respondió Rafen—. No importa si pasó ni cómo. Gallar es capaz de salir con cualquier disparate para apoderarse de esa parte de los muelles.</p>

                    
                
                                        
                                        
                        <p>Y si te pregunta quién será el cabecilla de ella, dile que esta corona... También habla.</p>

                    
                
                                        
                                        
                        <p>—Tranquilo Drago, sabes que le hare llegar tu mensaje.</p>

                    
                
                                        
                                        
                        <p>Ahora en el Limbo Regane estaba "al mando".</p>

                    
                
                                        
                                        
                        <p>Había aprovechado de mala manera la dura situación que Drago estaba viviendo.</p>

                    
                
                                        
                                        
                        <p>Sin embargo, yo estaba seguro de que si Drago se proponía a derrocarlo, lo haría sin ningún tipo de problema.</p>

                    
                
                                        
                                        
                        <p>La gente seguía creyendo en él, tenían fe ciega en su corona.</p>

                    
                
                                        
                                        
                        <p>Quince minutos más tarde...</p>

                    
                
                                        
                                        
                        <p>—Drago, te buscan arriba - El guardia no había tardado en volver.</p>

                    
                
                                        
                                        
                        <p>Drago se alisto rapido y se decidio a subir.</p>

                    
                
                                        
                                        
                        <p>—Regane creo simplemente que es hora de irte - Amenazaba Drago al Rey</p>

                    
                
                                        
                                        
                        <p>—Ei drago... Tranquilo, todavía estas alterado por lo de tu padre verdad? - Retrucaba Regane dandole en el punto más debil</p>

                    
                
                                        
                                        
                        <p>—Hijo de …</p>

                    
                
                                        
                                        
                        <p>—Eieiei calmaos, calmaos. Disculpa Drago es que mis guardias se toman todo muy personal, solo te llamaba para que veas esta bonita corona. Por cierto, muy similar a la tuya no crees?</p>

                    
                
                                        
                                        
                        <p>—Maldito desgraciado, a caso te atreviste a duplicarla?</p>

                    
                
                                        
                                        
                        <p>—Duplicarla? Já! Yo talvez lo llamaría de otra manera... Energía portalera? nonono, El elegido de los dioooooses! esa si me gusta más, vamos que se valla este malcriado de una vez.</p>

                    
                
                                        
                                        
                        <p>—Voy a matarte!</p>

                    
                
                                        
                                        
                        <p>—Que tan seguro estas de eso pequeñaco?</p>

                    
                
                                        
                                        
                        <p>Drago se lanzo de lleno ante los guardias derribándolos a todos con su imponente armadura, pegó un gran salto estirando sus espadas doradas para asesinar a Regane, pero justo antes de alcanzarlo, los dioses volvieron a actuar.</p>

                    
                
                                        
                                        
                        <p>Teletransportaron a ambos a un nuevo campo de batalla suspendido en el aire.</p>

                    
                
                                        
                                        
                        <p>Se abrió un portal en la ciudad nunca antes visto que mostraba dicha escena.</p>

                    
                
                                        
                                        
                        <p>Todos en el Limbo se quedaron atónitos, sospechando de entrada que solo de uno de ellos regresaría vivo.</p>

                    
                
                                        
                                        
                        <p><p align="right">Continuará...</p></p>


                
                <span class="js-scroll-logging" data-message="part-3-bottom"></span>

            </section><!-- /.c-novela-block -->

        
       
                
            <footer class="c-novela-footer">

                <div class="c-novela-footer__cta">
                    <!--p><a href="chapter-2.html" class="c-btn c-btn--large">Capítulo Dos</a></p-->
                </div>

            </footer>

                
        <div class="c-novela-progress js-novela-progress-position">

            <div class="c-novela-progress__bar">
                <div class="c-novela-progress__bar-meter js-novela-progress-meter"></div>
            </div>

            <ol class="o-list-unstyled c-novela-progress__list js-progress-marker-list">

                                
                                        <li class="c-novela-progress__list-item js-position-marker" style="top: 0%">
                        <a id="marker-1" href="#part-1" class="c-novela-progress__list-marker js-marker js-scroll-to is-active">I</a>
                    </li>

                
                                        <li class="c-novela-progress__list-item js-position-marker" style="top: 0%">
                        <a id="marker-2" href="#part-2" class="c-novela-progress__list-marker js-marker js-scroll-to ">II</a>
                    </li>

                
                                        <li class="c-novela-progress__list-item js-position-marker" style="top: 0%">
                        <a id="marker-3" href="#part-3" class="c-novela-progress__list-marker js-marker js-scroll-to ">III</a>
                    </li>

                
                
            </ol>

        </div>

    </div>

</div><!-- /.c-novela -->

        </div><!-- /#content -->

        <footer class="c-footer js-footer">

    <button class="c-footer__trigger js-footer-toggle">
        <span class="u-sr-only">Ver nota al pie</span>
        <svg class="c-footer__trigger-icon" width="24" height="30" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23.4 30">
            <path d="M23.1 4L12.3 14.7c-.2.2-.4.3-.7.3-.3 0-.5-.1-.7-.3L.3 4c-.2-.2-.3-.4-.3-.7s.1-.5.3-.7L2.7.2c.2-.1.4-.2.6-.2.3 0 .5.1.7.3L11.7 8 19.4.3c.2-.2.4-.3.6-.3.3 0 .5.1.7.3l2.4 2.4c.2.2.3.4.3.7s-.1.4-.3.6zM23.1 19L12.3 29.7c-.2.2-.4.3-.7.3-.3 0-.5-.1-.7-.3L.3 19c-.2-.2-.3-.4-.3-.7 0-.3.1-.5.3-.7l2.4-2.4c.2-.1.4-.2.6-.2.3 0 .5.1.7.3l7.7 7.7 7.7-7.7c.2-.2.4-.3.7-.3.3 0 .5.1.7.3l2.4 2.4c.2.2.3.4.3.7-.1.2-.2.4-.4.6z"/>
        </svg>
    </button>

    <ul class="o-list-unstyled c-controls js-controls-bubble">
        <li class="c-controls__item">
            <a href="javascript:share('facebook')" class="c-controls__link" title="Share on Facebook">
                <svg class="c-controls__icon" width="12" height="21" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.3 24">
                    <path d="M9 24H3V12H0V7h3V5.3C3 4.1 3.4 0 8.2 0H13v5H9.5c-.4 0-.5.3-.5.6V7h4.3l-.5 5H9v12zm-5-1h4V11h3.9l.3-3H8V5.6C8 4.4 8.8 4 9.5 4H12V1H8.2C4.5 1 4 3.7 4 5.3V8H1v3h3v12z"/>
                </svg>
            </a>
        </li>
        <li class="c-controls__item">
            <a href="javascript:share('twitter')" class="c-controls__link" title="Share on Twitter">
                <svg class="c-controls__icon" width="21" height="15" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29.4 20.9">
                    <path d="M19.5 1.9c1.3 0 2.5.6 3.3 1.4 1-.2 2-.6 2.9-1.1-.3 1.1-.4 1.5-1.3 2 .4.2.7.3 1.1.3.4 0 .8-.1 1.2-.3-.7.9-1.7 1.5-2.6 2.2V7c0 6-4.6 12.9-12.9 12.9-2.6 0-5-.8-7-2 .4 0 .7.1 1.1.1 2 0 4-.7 5.6-1.9-2 0-3.7-1.4-4.2-3.2.3.1.6.1.9.1.4 0 .8-.1 1.2-.2-2.1-.4-3.6-2.3-3.6-4.5v-.1c.6.3 1.3.5 2.1.6C5.9 8 5.1 6.6 5.1 5c0-.8.2-1.6.6-2.3 2.2 2.8 5.6 4.6 9.4 4.7-.1-.3-.1-.6-.1-1 0-2.5 2-4.5 4.5-4.5m8-1.9l-2.3 1.3c-.7.4-1.3.7-2.1.9-1-.9-2.3-1.3-3.6-1.3-3 0-5.5 2.4-5.5 5.5-2.9-.4-5.6-2-7.5-4.3L5.6 1l-.7 1.2C4.4 3 4.1 4 4.1 5c0 .6.1 1.1.3 1.7l-.2-.2-.1 1.7v.1c0 1.3.4 2.5 1.2 3.4l.5 1.4c.4 1.4 1.4 2.5 2.7 3.2-1 .4-2 .6-3.1.6-.3 0-.6 0-1-.1L0 16.3l3.7 2.3c2.2 1.4 4.8 2.2 7.5 2.2 5.2 0 8.5-2.5 10.3-4.6 2.3-2.6 3.6-6 3.6-9.4v-.1l.1-.1c.8-.5 1.7-1.1 2.3-2l1.9-2.9L26.5 3c0-.1.1-.3.1-.4l.1-.3.8-2.3z"/>
                </svg>
            </a>
        </li>
        <li class="c-controls__item c-controls__item--divider u-hidden@less-than-sm"></li>
        <li class="c-controls__item">
            <a href="#" class="c-controls__link js-legal-toggle">&plus; Información legal</a>
        </li>
    </ul>
</footer>


<footer class="c-legal js-legal">

    <div class="o-container">
        <div class="c-legal__content">

            <div class="c-legal__content-block c-legal__content-block--small">

            </div><!-- /.c-legal__content-block -->

            <div class="c-legal__content-block c-legal__content-block--center">

                <p class="c-legal__disc">© 2015 Riot Games Inc. Todos los derechos reservados. Riot Games, League of Legends y PvP.net son marcas comerciales, marcas de servicios o marcas registradas de Riot Games, Inc.</p>

                <ul class="o-list-inline c-legal__links">
                    <li class="c-legal__links-item"><a href="http://lan.leagueoflegends.com/es/legal/eula" class="c-legal__links-link" target="_blank">ALUF</a></li>
                    <li class="c-legal__links-item c-legal__links-item--divider">|</li>
                    <li class="c-legal__links-item"><a href="http://lan.leagueoflegends.com/es/legal/privacy" class="c-legal__links-link" target="_blank">Política de Privacidad</a></li>
                    <li class="c-legal__links-item c-legal__links-item--divider">|</li>
                    <li class="c-legal__links-item"><a href="http://lan.leagueoflegends.com/es/legal/termsofuse" class="c-legal__links-link" target="_blank">Términos de Uso</a></li>
                    <li class="c-legal__links-item c-legal__links-item--divider">|</li>
                    <li class="c-legal__links-item"><a href="http://lan.leagueoflegends.com/es/legal/tribunal" class="c-legal__links-link" target="_blank">Política del Tribunal</a></li>
                </ul>

            </div><!-- /.c-legal__content-block -->

        </div><!-- /.c-legal__content -->

    </div><!-- /.o-container -->

</footer><!-- /.c-legal -->

<script src="<?php print site_asset('/js/history.js"></script>     

<script>
</script>