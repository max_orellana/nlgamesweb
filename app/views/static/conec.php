<div class="col-md-12 col-xs-12 content text-shadow border-radius">
	<div class="col-md-8 col-xs-12 single">
		                                            <div class="col-md-12 col-xs-12 no-pad light-gold main-title">Conectando al mundo</div>
                <div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
                    <span class="gold">Redline</span> | 10 de Enero del 2015</div>
                <div class="col-md-12 col-xs-12 text">
<p><center></center></p>
<p>Hace  tiempo que este equipo de trabajo, viene elaborando una meticulosa investigación  respecto a las conexiones de redes de todo el mundo. De esta manera, se asegura  de poder brindarle a todos sus usuarios, el mejor rendimiento posible del  servidor.
</p>
<p>A continuación,  trataremos de resumirle uno de los puntos más fuertes de dicha investigación, (Basado en el informe de la firma Akami) y al mismo tiempo,  nuestras propias conclusiones sobre ella.</p>
<p>La plataforma Inteligente mundialmente distribuida de Akamai  nos permite recopilar grandes cantidades de información sobre muchos  parámetros, incluyendo velocidades de conexión, tráfico de ataque, conectividad  de red/disponibilidad/problemas de latencia y crecimiento/progreso de  transición IPv6, así como los patrones de tráfico a través de los principales  sitios web y proveedores de medios digitales.</p>
<p>0.1 <u>Observaciones sobre ataques DDos</u></p>
<p>Desde el final de 2012, Akamai ha estado analizando los ataques  de denegación de servicio distribuidos (DDoS) comunicados por  sus clientes para el informe del estado de Internet. Los  adversarios que realizan ataques DDoS se esfuerzan cada vez  más para que sus ataques parezcan &lsquo;flash mobs&rsquo; legítimos en un  esfuerzo por eludir las defensas automatizadas; lo que crea la  carrera armamentística cada vez más intensa para automatizar  el análisis manual que a menudo conlleva la evaluación de si  un evento fue tráfico de ataque o tráfico legítimo debido a un  acontecimiento imprevisto. Debido a esto, Akamai debe confiar  en los clientes para informar de los ataques DDoS y ayudar a  diferenciar entre el tráfico legítimo y malicioso. Además, Akamai  no sigue los ataques dirigidos a las capas de la red de nivel  inferior, tales como las inundaciones SYN, inundaciones UDP y  otros tipos similares de ataques volumétricos, ya que se mitigan  de forma automática con mínima interacción humana en la  mayoría de las circunstancias. Los ataques de nivel superior que  se dirigen a la aplicación y las capas lógicas, como inundaciones  HTTP GET o ataques que descargan repetidamente archivos de  gran tamaño, se mitigan con la solución KONA Site Defender de  Akamai, y requieren la intervención de los analistas de Akamai  para crear, implementar y difundir las normas para poner fin a  estos ataques.</p>
<p>A continuación, le dejamos el grafico principal elaborado por Akami, sobre los paises con mayor influencia en ataques de negacion de servicio:</p>
<p><center>
  <p><img src="s2games.exceda.com/1.jpg" width="299" height="192"></p>
  <p>&nbsp;</p>
</center></p>
<p>0.2 <u>Velocidades de conexión medias mundiales</u></p>
<p>El informe de la firma Akamai, tambien determinó que la velocidad media global de conexión a internet aumentó un 29% para llegar a los 3,6 Mbps, mientras que el tráfico de datos en dispositivos móviles tuvo un crecimiento del 80%. Esto refleja la clara realidad de que el mundo se conecta cada vez más y más rápido.</p>

<p>De acuerdo con este estudio, Corea del Sur es el país con mayor velocidad: 13,3 Mbps, tras un aumento del 51% frente al año anterior. En ese ranking la siguen Japón (12,0 Mbps), Suiza (11,0), Hong Kong (10,8), Holanda (10,1) y República Checa co (9,8 Mbps).</p>

<p>En América Latina, México y Ecuador se mantienen por una clara ventaja como lideres, con velocidades de 3,6 Mbps y 3,4 Mbps, respectivamente, y son los únicos países de la región que están en línea con el promedio mundial.</p>

<p>Sobre una base comparativa para el continente americano, la velocidad promedio de conexión en EEUU fue de 8,7 Mbps, que registró un crecimiento del 31 por ciento en el tercer trimestre de 2013. Mientras que en Canadá fue de 8,2 Mbps.</p>

<p>En el puesto 68 del listado se ubica Chile (2,9 Mbps), acompañado por Colombia con la misma velocidad y la Argentina en el 80, con una velocidad de acceso promedio de 2,0 Mbps tras una mejora del 33% con respecto al año pasado.</p>

<p>Brasil se ubica en la posición 75 con (2,4 Mbps); Perú, 91 (1,7 Mbps); Uruguay, 97 (1,5 Mbps); seguidos por Costa Rica, Venezuela,  y Bolivia, quien se ubica en el puesto 126 y ofrece una velocidad de acceso de 1,1 Mbps. </p>
<p><center>
  <p><img src="s2games.exceda.com/2.jpg" width="674" height="872"></p></center>
  <p>El resultado presenta la media de las medidas (en Mbps) tomadas durante tres meses (del 4 de septiembre al 5 de diciembre de 2013), en localidades domésticas donde la distancia entre el servidor (el portal visitado) y el cliente (el dispositivo desde donde se realiza la visita) es menor a 480 Km.</p>
<p>Una rápido escaneo a la lista es suficiente para notar que Europa y Asia van adelante en cuanto a avances de conectividad, ya que dominan las primeras 15 posiciones, con tres países de Asia y 11 de Europa, todos con alrededor de 10 Mbps o más.</p>
  <p><u>Conclusión</u></p>
<p><strong>Antes de comenzar a desarrollar nuestra conclusión, es importante aclarar que la misma se basa en toda nuestra investigación y no solamente en el informe de la firma Akami resumido anteriormente, dicho esto, comenzamos</strong>. </p>
<p></p>
<p>&nbsp;</p>
          </div>
                            </div>
                            	<div class="col-md-4 col-xs-12 no-pad side-bar">
	    	<div class="col-md-12 col-xs-12 no-pad text-shadow latest-news">
            <div class="col-md-12 col-xs-12 light-gold main-title">Ultimas Noticias</div>
              <ul class="col-md-12 col-xs-12 no-pad posts">
                                    <a class='col-md-12 col-xs-12' href="tour">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Torneos. Hablemos del futuro</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="balance">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Un balance pleno</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="http://guides.nlgames.net/patch3.html">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Ultimas implementaciones</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="conec">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Conectando al mundo</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="nlteam">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Newline Team</div>
                        </li>
                    </a>
                                            </ul>
        </div>
    	<div class="col-md-12 col-xs-12 no-pad text-shadow facebook">
		<div class="col-md-12 col-xs-12 light-gold main-title">Facebook</div>
		<div class="col-md-12 col-xs-12 iframe">
        	<iframe id="facebook-iframe" src="http://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/www.nlgames.net&amp;show_border=false&amp;colorscheme=dark&amp;show_faces=true&amp;t&amp;stream=false&amp;header=false&amp;height=450" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
        </div>
	</div>
        <div class="col-md-12 col-xs-12 no-pad text-shadow editors-pic">
        <div class="col-md-12 col-xs-12 light-gold main-title">SOBRE NEWLINE</div>
       <div id="container1">
            <div id="feed-slider">
                <span class="control prev border-radius"><span class="fi-play"></span></span>
                <div id="inner-slider">
                    <ul>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://nlgames.net">
                <img class="avatar" src="/upload/2015/06/article_sub_img/4.jpg" alt="In No Man's Sky You can Destroy An Entire Planet">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="nlteam">
                <span>Un nuevo moba esta llegando!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide3.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/1.jpg" alt="Dream of Mirror Online Closed Beta Test Key Giveaway!">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide3.html">
                <span>Descubre todos nuestros heroes!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide4.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/2.jpg" alt="PvP-focused Sandbox Das Tal Permanent Alpha Test Code Giveaway">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide4.html">
                <span>Conoce el nuevo campo de batalla!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide5.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/3.jpg" alt="Korean New MMO ELOA Advanced Review Video [By Cartoon Girl]">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide5.html">
                <span>Explora todos nuestros items!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide8.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/5.jpg" alt="Nebula Online Revealed — F2P Hardcore Space MMO, No Cash Shop, Full PvP">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide8.html">
                <span>Te explicamos las distintas fases del juego</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide18.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/6.jpg" alt="Crowfall Details the Customizable Worlds and Reset Mechanic">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide18.html">
                <span>Crea tu propio equipo y conviertelo en el mejor!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="tour">
                <img class="avatar" src="/upload/2015/06/article_sub_img/7.jpg" alt="EOS(KR): New Gameplay Video Unveiling 3 New Party Dungeons and 1:1 Arena">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="tour">
                <span>Participa en torneos oficiales por efectivo!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide19.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/8.jpg" alt="Mobile TCG Mabinogi Deul from Nexon's devCAT Studio Recruited 2nd CB Testers">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide19.html">
                <span>Conviertete en un verdadero Heroe!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide14.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/9.jpg" alt="Albion Online Dev Team Q&amp;A: Loot System, Dungeon and More">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide14.html">
                <span>Comprende cuales son los objetivos imporantes</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide11.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/10.jpg" alt="Medieval Sandbox Albion Online First Look &amp; Thoughts">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide11.html">
                <span>Conoce todos los spells!</span>
            </a>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25171944_1/Crowfall-Details-the-Customizable-Worlds-and-Reset-Mechanic-by-Blake-Lau.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/05/article_sub_img/a0e0gg2cmnf3mha5/1.jpg" alt="Crowfall Details the Customizable Worlds and Reset Mechanic">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25171944_1/Crowfall-Details-the-Customizable-Worlds-and-Reset-Mechanic-by-Blake-Lau.htm">
                <span>Tus heroes favoritos! Con tus skins customs!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25170972_1/EOSKR-New-Gameplay-Video-Unveiling-3-New-Party-Dungeons-and-11-Arena-by-Natalie-Kuhn.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/05/article_sub_img/a0e0gdm9cgd7pnj6/_1.jpg" alt="EOS(KR): New Gameplay Video Unveiling 3 New Party Dungeons and 1:1 Arena">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25170972_1/EOSKR-New-Gameplay-Video-Unveiling-3-New-Party-Dungeons-and-11-Arena-by-Natalie-Kuhn.htm">
                <span>Eventos mensuales que iran variando!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25151218_1/Mobile-TCG-Mabinogi-Deul-from-Nexons-devCAT-Studio-Recruited-2nd-CB-Testers-by-Natalie-Kuhn.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/04/article_sub_img/a0e0g6eivirharcc/_1.jpg" alt="Mobile TCG Mabinogi Deul from Nexon's devCAT Studio Recruited 2nd CB Testers">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25151218_1/Mobile-TCG-Mabinogi-Deul-from-Nexons-devCAT-Studio-Recruited-2nd-CB-Testers-by-Natalie-Kuhn.htm">
                <span>La posibilidad de crear tu propio equipo!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25143525_1/Albion-Online-Dev-Team-QampA-by-DVS_eddie.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/04/article_sub_img/a0e0g6ivxh43pu7h/20150204160421a0e0fvtd11d8bsth.jpg" alt="Albion Online Dev Team Q&amp;A: Loot System, Dungeon and More">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25143525_1/Albion-Online-Dev-Team-QampA-by-DVS_eddie.htm">
                <span>Partidas clasificatorias! Un ambiente competitivo!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25142902_1/Albion-Online-First-Look-amp-Thoughts-by-DVS_eddie.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/04/article_sub_img/a0e0g6k9tloa3zzq/20150204155843a0e0fvnrxrkls0xy.jpg" alt="Medieval Sandbox Albion Online First Look &amp; Thoughts">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25142902_1/Albion-Online-First-Look-amp-Thoughts-by-DVS_eddie.htm">
                <span>Estabilidad, Balance y mucho más!</span>
            </a>
        </p>
    </div>
</li>
</ul>                </div>
                <span class="control next border-radius"><span class="fi-play"></span></span>
            </div>
            <div id="feed-slider-counts">
                                        <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                </div>
        </div>
    </div>
        </div>
    </div></div>
