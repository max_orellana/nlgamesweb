<div class="col-md-12 col-xs-12 content text-shadow border-radius">
	<div class="col-md-8 col-xs-12 single">
		                                            <div class="col-md-12 col-xs-12 no-pad light-gold main-title">Capitulo I “El principio del cambio”<br> Año 1650 ILT</div>
                <div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
                    <span class="gold">Redline</span> | 4 de Enero del 2015</div>
                <div class="col-md-12 col-xs-12 text"><p>Solo voy a contarles lo que recuerdo del principio de esta historia. Del resto… preferiría ni hablar por ahora.</p>
                  <p><img src="/upload/2015/03/slider2-900x350_c.jpg" width="900" height="348"></p>
<p>Hace unos meses, o al menos eso creo, el sistema para algunos paso a ser perfecto. Los dioses de Aden proveían de vuestra vida. En las guerras y en cualquier rutina diaria, si uno moría… en cuestión de segundos volvería a estar con vida. Desde mi opinión; ese fue el comienzo de este caos.</p>
<p><img src="../../upload/2015/03/slider7-800x300_c.png" width="900" height="348"></p>
<p>Nuestro antiguo mundo se vio afectado por la ira de los dioses, una cantidad infinita de desastres naturales arremetían constantemente nuestro planeta. Las victimas? Aun me niego a creer que sean todos, aunque algunos se esfuercen por demostrar lo contrario. Por el momento, entre todos los seres que se encuentran en este limbo (así es como la mayoría ha decidido llamarle), solo me he reencontrado con una vieja amiga y aún ni ella y yo sabemos algo de los nuestros.</p>
<p><img src="../../upload/2015/03/slider8-900x350_c.jpg" width="800" height="300"></p>
 <p>Las esperanzas aquí cada vez son menos, con el correr del tiempo todas las razas van perdiendo poco a poco sus sentimientos. Al principio pensaba que era tan solo un síntoma de este nuevo sistema de combate, si es que así puede llamársele. Pero cuando nos toco a nosotras, entendí que estaba equivocada… Ya que hasta el día de hoy jamás hemos luchado. 
Lo primero en presentarse fue la falta de apetito, aquí todos llevan meses enteros sin alimentarse, lo segundo fue el sueño… Los primeros días cuando el sol caía, mi cuerpo agotado de tanto llorar se dormía rendido. Hoy creo cumplir el octavo día sin dormir...</p>
<p>Y aunque todo esto sea una milésima parte de todo lo que viene ocurriendo y ya suene terrible, desde aquí parece que todas las razas se han acostumbrado a esto, una lucha constante sin fin que no deja nada, un futuro sin sentimientos, un vacio infinito, un maldito infierno en llamas.</p>
<div align="right"><br>
</div>
<p align="right">Continuara...</p>
</div>
                            </div>
                            	<div class="col-md-4 col-xs-12 no-pad side-bar">
	    	<div class="col-md-12 col-xs-12 no-pad text-shadow latest-news">
            <div class="col-md-12 col-xs-12 light-gold main-title">Ultimas Noticias</div>
            <ul class="col-md-12 col-xs-12 no-pad posts">
                                    <a class='col-md-12 col-xs-12' href="tour">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Torneos. Hablemos del futuro</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="balance">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Un balance pleno</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="http://guides.nlgames.net/patch3.html">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Ultimas implementaciones</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="conec">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Conectando al mundo</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="nlteam">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Newline Team</div>
                        </li>
                    </a>
                                            </ul>
        </div>
    	<div class="col-md-12 col-xs-12 no-pad text-shadow facebook">
		<div class="col-md-12 col-xs-12 light-gold main-title">Facebook</div>
		<div class="col-md-12 col-xs-12 iframe">
        	<iframe id="facebook-iframe" src="http://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/www.nlgames.net&amp;show_border=false&amp;colorscheme=dark&amp;show_faces=true&amp;t&amp;stream=false&amp;header=false&amp;height=450" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
        </div>
	</div>
        <div class="col-md-12 col-xs-12 no-pad text-shadow editors-pic">
        <div class="col-md-12 col-xs-12 light-gold main-title">SOBRE NEWLINE</div>
        <div id="container1">
            <div id="feed-slider">
                <span class="control prev border-radius"><span class="fi-play"></span></span>
                <div id="inner-slider">
                    <ul>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://nlgames.net">
                <img class="avatar" src="/upload/2015/06/article_sub_img/4.jpg" alt="In No Man's Sky You can Destroy An Entire Planet">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="nlteam">
                <span>Un nuevo moba esta llegando!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide3.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/1.jpg" alt="Dream of Mirror Online Closed Beta Test Key Giveaway!">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide3.html">
                <span>Descubre todos nuestros heroes!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide4.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/2.jpg" alt="PvP-focused Sandbox Das Tal Permanent Alpha Test Code Giveaway">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide4.html">
                <span>Conoce el nuevo campo de batalla!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide5.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/3.jpg" alt="Korean New MMO ELOA Advanced Review Video [By Cartoon Girl]">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide5.html">
                <span>Explora todos nuestros items!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide8.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/5.jpg" alt="Nebula Online Revealed — F2P Hardcore Space MMO, No Cash Shop, Full PvP">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide8.html">
                <span>Te explicamos las distintas fases del juego</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide18.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/6.jpg" alt="Crowfall Details the Customizable Worlds and Reset Mechanic">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide18.html">
                <span>Crea tu propio equipo y conviertelo en el mejor!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="tour">
                <img class="avatar" src="/upload/2015/06/article_sub_img/7.jpg" alt="EOS(KR): New Gameplay Video Unveiling 3 New Party Dungeons and 1:1 Arena">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="tour">
                <span>Participa en torneos oficiales por efectivo!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide19.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/8.jpg" alt="Mobile TCG Mabinogi Deul from Nexon's devCAT Studio Recruited 2nd CB Testers">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide19.html">
                <span>Conviertete en un verdadero Heroe!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide14.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/9.jpg" alt="Albion Online Dev Team Q&amp;A: Loot System, Dungeon and More">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide14.html">
                <span>Comprende cuales son los objetivos imporantes</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide11.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/10.jpg" alt="Medieval Sandbox Albion Online First Look &amp; Thoughts">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide11.html">
                <span>Conoce todos los spells!</span>
            </a>
        </p>
    </div>
</li>
</ul>                 </div>
                <span class="control next border-radius"><span class="fi-play"></span></span>
            </div>
            <div id="feed-slider-counts">
                                        <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                </div>
        </div>
    </div>
        </div>
    </div></div>
