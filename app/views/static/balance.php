<div class="col-md-12 col-xs-12 content text-shadow border-radius">
	<div class="col-md-8 col-xs-12 single">
		                                            <div class="col-md-12 col-xs-12 no-pad light-gold main-title">Un balance pleno</div>
                <div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
                    <span class="gold">Redline</span> | 1 de Febrero del 2015</div>
                <div class="col-md-12 col-xs-12 text">
<p>Hola a todos!.<br>Nos acercamos en esta ocasión, para hablar de un tema sumamente importante, el “Balance”.</p>
<p>Como ya hemos mencionado anteriormente en varios apartados, pensamos que esta parte del proyecto, es sin duda una de las que posee mayor dificultad, aunque puede que a simple vista no lo parezca. Encarar el balance general/total de un juego, no solo requiere de mucho tiempo y experiencia, sino también de poseer un equipo de balanceo con grandes cualidades. <br> Cuando hablamos de balance total, o general, estamos abarcando varios puntos. Los cuales le explicaremos a continuación:</p>
<p><span style="color: #ff6600;"><u>Balance de mapas:</u></span></p>
<p>Imagínate que uno de los dos equipos, tuviera un menor recorrido desde su base, hasta el centro del mapa. Resulta evidente que ya pasaría a tener una gran ventaja, verdad?. Pues claro, este equipo llegaría con anterioridad a cualquiera de los objetivos SIEMPRE. Por este motivo, es importante al crear un mapa, que todas las dimensiones y distancias se tengan en cuenta, de lo contrario sería imposible lograr un balance totalmente justo. Los mapas de newline, serán siempre totalmente simétricos, de esta manera, nos estaremos evitando muchísimos problemas.</p>
<p><span style="color: #ff6600;"><u>Balance de items/precios:</u></span></p>
<p>Cada ítem, posee un bonus, y un precio de venta diferente, por lo que es importante que esto esté totalmente balanceado. Para este punto en particular, se hacen muchísimos cálculos matemáticos, de esta manera, se llega siempre al precio de venta justo.</p>
<p><span style="color: #ff6600;"><u>Balance de npcs:</u></span></p>
<p>Como bien ya deben saber, a medida que las partidas transcurren, los stats de los personajes van aumentando por diferentes motivos. Los npcs, junto a sus buffs, no podían quedarse atrás. De lo contrario, a medida que los minutos vayan pasando dentro del juego, los personajes los derribarían con muchísima facilidad. Perdiendo así, todo sentido. Por dicho motivo, es sumamente importante que el aumento de stats que reciben ambas partes, sea equitativo de comienzo a fin.</p>
<p><span style="color: #ff6600;"><u>Balance de clases/skills:</u></span></p>
<p>Como ya hemos explicado en puntos anteriores, cada clase ocupa skills y stats propios, con un rol principal que los identifica del resto. La selección de estos skills, probablemente sea el punto más importante de todo el balance. Pongamos un ejemplo sencillo: La clase “Bishop”, cumple sin duda, el rol de un soporte nato. Podemos darnos cuenta de esto, en cada uno de sus skills. La mayoría, se basan en asistir a sus aliados o a uno mismo, recuperando grandes cantidades de vida. Por lo que si a esta clase, le agregariamos skills de daño magico, no solamente estariamos rompiendo la meta del mismo, sino que tambien le estariamos dando demasiada ventaja (Manteniendo los niveles de sus skills, claro esta). Es por esto, que la selección de cada skill en Newline, ha sido seleccionada minuciosamente. De esta manera, nos aseguramos que cada clase del juego se encuentre totalmente balanceada y cumpla por sobre todas las cosas un rol principal.
<p>En fin. Son muchos puntos a tener en cuenta para lograr alcanzar un balance pleno. Por eso, quienes trabajamos en él día a día, dejamos nuestro máximo en cada detalle. Esperamos que todos coincidan con nosotros en la totalidad de  estos puntos, de no ser así, siempre tendra a su disposición un apartado en el foro. Para acercarnos de esta manera, cualquier tipo de sugerencia que a usted le parezca.</div>
                            </div>
                            	<div class="col-md-4 col-xs-12 no-pad side-bar">
	    	<div class="col-md-12 col-xs-12 no-pad text-shadow latest-news">
            <div class="col-md-12 col-xs-12 light-gold main-title">Ultimas Noticias</div>
              <ul class="col-md-12 col-xs-12 no-pad posts">
                                    <a class='col-md-12 col-xs-12' href="tour">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Torneos. Hablemos del futuro</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="balance">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Un balance pleno</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="http://guides.nlgames.net/patch3.html">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Ultimas implementaciones</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="conec">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Conectando al mundo</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="nlteam">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Newline Team</div>
                        </li>
                    </a>
                                            </ul>
        </div>
    	<div class="col-md-12 col-xs-12 no-pad text-shadow facebook">
		<div class="col-md-12 col-xs-12 light-gold main-title">Facebook</div>
		<div class="col-md-12 col-xs-12 iframe">
        	<iframe id="facebook-iframe" src="http://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/www.nlgames.net&amp;show_border=false&amp;colorscheme=dark&amp;show_faces=true&amp;t&amp;stream=false&amp;header=false&amp;height=450" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
        </div>
	</div>
        <div class="col-md-12 col-xs-12 no-pad text-shadow editors-pic">
        <div class="col-md-12 col-xs-12 light-gold main-title">SOBRE NEWLINE</div>
       <div id="container1">
            <div id="feed-slider">
                <span class="control prev border-radius"><span class="fi-play"></span></span>
                <div id="inner-slider">
                    <ul>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://nlgames.net">
                <img class="avatar" src="/upload/2015/06/article_sub_img/4.jpg" alt="In No Man's Sky You can Destroy An Entire Planet">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="nlteam">
                <span>Un nuevo moba esta llegando!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide3.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/1.jpg" alt="Dream of Mirror Online Closed Beta Test Key Giveaway!">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide3.html">
                <span>Descubre todos nuestros heroes!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide4.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/2.jpg" alt="PvP-focused Sandbox Das Tal Permanent Alpha Test Code Giveaway">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide4.html">
                <span>Conoce el nuevo campo de batalla!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide5.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/3.jpg" alt="Korean New MMO ELOA Advanced Review Video [By Cartoon Girl]">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide5.html">
                <span>Explora todos nuestros items!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide8.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/5.jpg" alt="Nebula Online Revealed — F2P Hardcore Space MMO, No Cash Shop, Full PvP">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide8.html">
                <span>Te explicamos las distintas fases del juego</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide18.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/6.jpg" alt="Crowfall Details the Customizable Worlds and Reset Mechanic">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide18.html">
                <span>Crea tu propio equipo y conviertelo en el mejor!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="tour">
                <img class="avatar" src="/upload/2015/06/article_sub_img/7.jpg" alt="EOS(KR): New Gameplay Video Unveiling 3 New Party Dungeons and 1:1 Arena">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="tour">
                <span>Participa en torneos oficiales por efectivo!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide19.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/8.jpg" alt="Mobile TCG Mabinogi Deul from Nexon's devCAT Studio Recruited 2nd CB Testers">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide19.html">
                <span>Conviertete en un verdadero Heroe!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide14.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/9.jpg" alt="Albion Online Dev Team Q&amp;A: Loot System, Dungeon and More">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide14.html">
                <span>Comprende cuales son los objetivos imporantes</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide11.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/10.jpg" alt="Medieval Sandbox Albion Online First Look &amp; Thoughts">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide11.html">
                <span>Conoce todos los spells!</span>
            </a>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25171944_1/Crowfall-Details-the-Customizable-Worlds-and-Reset-Mechanic-by-Blake-Lau.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/05/article_sub_img/a0e0gg2cmnf3mha5/1.jpg" alt="Crowfall Details the Customizable Worlds and Reset Mechanic">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25171944_1/Crowfall-Details-the-Customizable-Worlds-and-Reset-Mechanic-by-Blake-Lau.htm">
                <span>Tus heroes favoritos! Con tus skins customs!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25170972_1/EOSKR-New-Gameplay-Video-Unveiling-3-New-Party-Dungeons-and-11-Arena-by-Natalie-Kuhn.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/05/article_sub_img/a0e0gdm9cgd7pnj6/_1.jpg" alt="EOS(KR): New Gameplay Video Unveiling 3 New Party Dungeons and 1:1 Arena">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25170972_1/EOSKR-New-Gameplay-Video-Unveiling-3-New-Party-Dungeons-and-11-Arena-by-Natalie-Kuhn.htm">
                <span>Eventos mensuales que iran variando!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25151218_1/Mobile-TCG-Mabinogi-Deul-from-Nexons-devCAT-Studio-Recruited-2nd-CB-Testers-by-Natalie-Kuhn.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/04/article_sub_img/a0e0g6eivirharcc/_1.jpg" alt="Mobile TCG Mabinogi Deul from Nexon's devCAT Studio Recruited 2nd CB Testers">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25151218_1/Mobile-TCG-Mabinogi-Deul-from-Nexons-devCAT-Studio-Recruited-2nd-CB-Testers-by-Natalie-Kuhn.htm">
                <span>La posibilidad de crear tu propio equipo!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25143525_1/Albion-Online-Dev-Team-QampA-by-DVS_eddie.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/04/article_sub_img/a0e0g6ivxh43pu7h/20150204160421a0e0fvtd11d8bsth.jpg" alt="Albion Online Dev Team Q&amp;A: Loot System, Dungeon and More">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25143525_1/Albion-Online-Dev-Team-QampA-by-DVS_eddie.htm">
                <span>Partidas clasificatorias! Un ambiente competitivo!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25142902_1/Albion-Online-First-Look-amp-Thoughts-by-DVS_eddie.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/04/article_sub_img/a0e0g6k9tloa3zzq/20150204155843a0e0fvnrxrkls0xy.jpg" alt="Medieval Sandbox Albion Online First Look &amp; Thoughts">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25142902_1/Albion-Online-First-Look-amp-Thoughts-by-DVS_eddie.htm">
                <span>Estabilidad, Balance y mucho más!</span>
            </a>
        </p>
    </div>
</li>
</ul>                </div>
                <span class="control next border-radius"><span class="fi-play"></span></span>
            </div>
            <div id="feed-slider-counts">
                                        <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                </div>
        </div>
    </div>
        </div>
    </div></div>