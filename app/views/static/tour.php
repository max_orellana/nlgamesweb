<div class="col-md-12 col-xs-12 content text-shadow border-radius">
	<div class="col-md-8 col-xs-12 single">
		                                            <div class="col-md-12 col-xs-12 no-pad light-gold main-title">Torneos. Hablemos del futuro...</div>
                <div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
                    <span class="gold">Redline</span> | 28 de Enero del 2015</div>
                <div class="col-md-12 col-xs-12 text">
<p><center><img src="s2games.exceda.com/jug.jpg" width="607" height="273"></center></p>
<p>Según la antropología y la genética, los seres vivos, somos  por naturaleza competitivos. Creemos que esa competencia sana, es el espíritu principal  de todos los juegos. Por lo que esto nos conlleva a querer desarrollarla en su máximo  esplendor.
</p>
<p>Pensamos que organizar torneos competitivos con importantes premios a obtener, incentivara a la mayoría de nuestros usuarios a terminar compitiendo en un alto nivel de juego y al mismo tiempo, se estará reconociendo el esfuerzo y la motivación de todos los participantes.</p>
<p>Para terminar de concluir esta idea, está claro que nuestro equipo deberá de extenderse (Cuando llegue el momento adecuado) con los distintos profesionales capacitados, para cada una de las funciones que esto conlleva. Y si bien nuestra experiencia en el tema actualmente es nula, sabemos bien que son muchos ítems los que se ponen en juego a la hora de convertir en realidad este proyecto, algunos que se nos vienen a la mente son:</p>
<p>
-Una sección web apartada y exclusiva para la transmisión e información del nivel competitivo.<br>
-Inscripciones prácticas y simples para todos los participantes.<br>
-Comentaristas de habla ingles y español.<br>
-Infraestructura de transmisión.<br> 
-Escenario de competencia.<br>
-Ambientación de sonido.<br>
-Competencias tanto en LAN, como ONLINE, para aquellas personas que no tengan la posibilidad de acercarse a un sitio en concreto.</p>
<p>Seguramente nos estén faltando muchísimos puntos por mencionar, de los cuales nos iremos dando cuenta a medida que el tiempo transcurra, ya que como bien mencionamos anteriormente, nuestro equipo no hace mucho decidió iniciarse en estos temas.</p>
<p>Esta noticia fue redactada con el fin de hacerle conocer nuestra propuesta a futuro, ya que creemos en la importancia de informarles las ideas a nuestros usuarios, antes de ser efectuadas. Le pedimos disculpas si cree que la misma se encuentra incompleta, y le aseguramos que estaremos trabajando en ella, para optimizarla cuanto antes.          </p>
          </div>
                            </div>
                            	<div class="col-md-4 col-xs-12 no-pad side-bar">
	    	<div class="col-md-12 col-xs-12 no-pad text-shadow latest-news">
            <div class="col-md-12 col-xs-12 light-gold main-title">Ultimas Noticias</div>
              <ul class="col-md-12 col-xs-12 no-pad posts">
                                    <a class='col-md-12 col-xs-12' href="tour">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Torneos. Hablemos del futuro</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="balance">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Un balance pleno</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="http://guides.nlgames.net/patch3.html">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Ultimas implementaciones</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="conec">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Conectando al mundo</div>
                        </li>
                    </a>
                                    <a class='col-md-12 col-xs-12' href="nlteam">
                        <div class='col-md-11 col-xs-11 no-pad wrap-shadow'></div>
                        <li class='col-md-12 col-xs-12 no-pad text'>
                            <div class="left border-radius bless-icon"></div>
                            <div class='col-md-9 col-xs-9 no-pad gold title'>Newline Team</div>
                        </li>
                    </a>
                                            </ul>
        </div>
    	<div class="col-md-12 col-xs-12 no-pad text-shadow facebook">
		<div class="col-md-12 col-xs-12 light-gold main-title">Facebook</div>
		<div class="col-md-12 col-xs-12 iframe">
        	<iframe id="facebook-iframe" src="http://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/www.nlgames.net&amp;show_border=false&amp;colorscheme=dark&amp;show_faces=true&amp;t&amp;stream=false&amp;header=false&amp;height=450" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
        </div>
	</div>
        <div class="col-md-12 col-xs-12 no-pad text-shadow editors-pic">
        <div class="col-md-12 col-xs-12 light-gold main-title">SOBRE NEWLINE</div>
       <div id="container1">
            <div id="feed-slider">
                <span class="control prev border-radius"><span class="fi-play"></span></span>
                <div id="inner-slider">
                    <ul>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://nlgames.net">
                <img class="avatar" src="/upload/2015/06/article_sub_img/4.jpg" alt="In No Man's Sky You can Destroy An Entire Planet">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="nlteam">
                <span>Un nuevo moba esta llegando!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide3.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/1.jpg" alt="Dream of Mirror Online Closed Beta Test Key Giveaway!">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide3.html">
                <span>Descubre todos nuestros heroes!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide4.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/2.jpg" alt="PvP-focused Sandbox Das Tal Permanent Alpha Test Code Giveaway">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide4.html">
                <span>Conoce el nuevo campo de batalla!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide5.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/3.jpg" alt="Korean New MMO ELOA Advanced Review Video [By Cartoon Girl]">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide5.html">
                <span>Explora todos nuestros items!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide8.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/5.jpg" alt="Nebula Online Revealed — F2P Hardcore Space MMO, No Cash Shop, Full PvP">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide8.html">
                <span>Te explicamos las distintas fases del juego</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide18.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/6.jpg" alt="Crowfall Details the Customizable Worlds and Reset Mechanic">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide18.html">
                <span>Crea tu propio equipo y conviertelo en el mejor!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="tour">
                <img class="avatar" src="/upload/2015/06/article_sub_img/7.jpg" alt="EOS(KR): New Gameplay Video Unveiling 3 New Party Dungeons and 1:1 Arena">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="tour">
                <span>Participa en torneos oficiales por efectivo!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide19.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/8.jpg" alt="Mobile TCG Mabinogi Deul from Nexon's devCAT Studio Recruited 2nd CB Testers">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide19.html">
                <span>Conviertete en un verdadero Heroe!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide14.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/9.jpg" alt="Albion Online Dev Team Q&amp;A: Loot System, Dungeon and More">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide14.html">
                <span>Comprende cuales son los objetivos imporantes</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://guides.nlgames.net/guide11.html">
                <img class="avatar" src="/upload/2015/06/article_sub_img/10.jpg" alt="Medieval Sandbox Albion Online First Look &amp; Thoughts">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://guides.nlgames.net/guide11.html">
                <span>Conoce todos los spells!</span>
            </a>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25171944_1/Crowfall-Details-the-Customizable-Worlds-and-Reset-Mechanic-by-Blake-Lau.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/05/article_sub_img/a0e0gg2cmnf3mha5/1.jpg" alt="Crowfall Details the Customizable Worlds and Reset Mechanic">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25171944_1/Crowfall-Details-the-Customizable-Worlds-and-Reset-Mechanic-by-Blake-Lau.htm">
                <span>Tus heroes favoritos! Con tus skins customs!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25170972_1/EOSKR-New-Gameplay-Video-Unveiling-3-New-Party-Dungeons-and-11-Arena-by-Natalie-Kuhn.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/05/article_sub_img/a0e0gdm9cgd7pnj6/_1.jpg" alt="EOS(KR): New Gameplay Video Unveiling 3 New Party Dungeons and 1:1 Arena">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25170972_1/EOSKR-New-Gameplay-Video-Unveiling-3-New-Party-Dungeons-and-11-Arena-by-Natalie-Kuhn.htm">
                <span>Eventos mensuales que iran variando!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25151218_1/Mobile-TCG-Mabinogi-Deul-from-Nexons-devCAT-Studio-Recruited-2nd-CB-Testers-by-Natalie-Kuhn.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/04/article_sub_img/a0e0g6eivirharcc/_1.jpg" alt="Mobile TCG Mabinogi Deul from Nexon's devCAT Studio Recruited 2nd CB Testers">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25151218_1/Mobile-TCG-Mabinogi-Deul-from-Nexons-devCAT-Studio-Recruited-2nd-CB-Testers-by-Natalie-Kuhn.htm">
                <span>La posibilidad de crear tu propio equipo!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25143525_1/Albion-Online-Dev-Team-QampA-by-DVS_eddie.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/04/article_sub_img/a0e0g6ivxh43pu7h/20150204160421a0e0fvtd11d8bsth.jpg" alt="Albion Online Dev Team Q&amp;A: Loot System, Dungeon and More">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25143525_1/Albion-Online-Dev-Team-QampA-by-DVS_eddie.htm">
                <span>Partidas clasificatorias! Un ambiente competitivo!</span>
            </a>
        </p>
    </div>
</li>
<li class="fans-list">
    <div class="fans-list-con">
        <p class="fans-list-avatar">
            <a class="title" href="http://2p.com/25142902_1/Albion-Online-First-Look-amp-Thoughts-by-DVS_eddie.htm">
                <img class="avatar" src="../../../i1.2pcdn.com/node14/201502/04/article_sub_img/a0e0g6k9tloa3zzq/20150204155843a0e0fvnrxrkls0xy.jpg" alt="Medieval Sandbox Albion Online First Look &amp; Thoughts">
            </a>
        </p>
        <p class="fans-list-title">
            <a class="title" href="http://2p.com/25142902_1/Albion-Online-First-Look-amp-Thoughts-by-DVS_eddie.htm">
                <span>Estabilidad, Balance y mucho más!</span>
            </a>
        </p>
    </div>
</li>
</ul>                </div>
                <span class="control next border-radius"><span class="fi-play"></span></span>
            </div>
            <div id="feed-slider-counts">
                                        <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                </div>
        </div>
    </div>
        </div>
    </div></div>
