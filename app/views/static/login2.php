<div class="col-md-12 login">
	<div class="group-control">&nbsp;</div>
	<div class="group-control">&nbsp;</div>
	<div class="group-control">&nbsp;</div>
	<div class="group-control">&nbsp;</div>	
    <div id="contentBox" class="contentBox">
      <div id="logo" class="text-center">
        <img src="<?php echo site_asset('img/nl.png');?>" alt="NLGames" height="97" width="286">
      </div>
      <?php echo messages();?>
      <form action="/login" method="post" id="registerForm" class="form form-ajax validator validate-register ">
        <div id="registerTop"></div>
          <div id="register" class="uplay">
            <div id="registerHeader">
          </div>
          <div id="registerFields" class="unselectable">
      			<fieldset style="z-index: 90;">
      				<input id="email" name="email" class="validate-ajax-ubiname" value="" type="text" required>
      				<span class="overlay"><?php echo locale('username_or_email');?></span>
      			</fieldset>

            <fieldset class="" style="z-index: 88;">
              <input id="password" name="password" class="validate-ajax-ubipassword send-password warning" value="" type="password"  required>
              <span class="overlay "><?php echo locale('password');?></span>
            </fieldset>

            <fieldset style="z-index: 86;" class="label">
              <input id="terms" name="terms" value="1" class="validate-required-check errorTitle-errorTermsTitle errorText-errorTermsText custom-check" type="checkbox">
              <label class="" for="terms" id="termsLabel"><?php print locale('remember');?></label>
            </fieldset>
          </div>
        </div>
        <div id="registerBottom"></div>
        <fieldset id="playButton" class="button">
          <span><?php print locale('login');?></span>
        </fieldset>
        <input style="display: block; height: 0px; width: 0px; position: absolute; top: 0px; left: 0px; border: medium none; background: transparent none repeat scroll 0% 0%;" type="submit">
      </form>
    </div>
</div>

<script>
$(function(){
	$('input[name="username"]').focus();
	$('#playButton').click(function(){
		$('input[type="submit"]').click();
	});
})
</script>