<style type="text/css">
.system_error {
	font: 15px/1.4em Monaco;
	background: #fff;
}
code.source {
	white-space: pre;
	background: #fff;
	padding: 1em;
	font-size: 15px;
	display: block;
	margin: 1em 0;
	border: 1px solid #bedbeb;
	border-radius: 5px;
}
.system_error .box {
	margin: 1em 0;
	background: #ebf2fa;
	font-size: 15px;
	padding: 10px;
	border: 1px solid #bedbeb;
	border-radius: 5px;
}
code.source em {background: #ffc;}
</style>

<div class="system_error">

	<b style="color: #990000">Something went simply wrong.</b>

</div>
