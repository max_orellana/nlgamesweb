<div class="bg-dark">
	<div class="row-spaced">
		<div class="row-top">
			<div class="col-md-1">
				<!--div class="form-group">
					<input type="text" class="form-control" name="title" placeholder="Buscador" value="" required>
				</div-->	
			</div>
			<div class="col-md-10">
				<div class="text-center item-list">
				<?php foreach($entries as $i => $entry):?>
					<a href="javascript:void(0)" 
						data-index="<?php print $i;?>" 
						data-title="<?php print $entry->title;?>" 
						data-grade="<?php print locale($entry->grade->slug);?>" 
						data-class="<?php print locale($entry->class->slug);?>" 
						data-type="<?php print locale($entry->type->slug);?>" 
						data-id="<?php print $entry->id;?>" 
						data-price="<?php print $entry->price;?>" 
						data-video="<?php print $entry->video;?>" 
						data-original-skin="<?php print $entry->original_skin;?>">
							<img width="75" height="75" src="<?php print strlen($entry->image) ? '/upload/items/' . $entry->image : '/assets/009/img/th-default-75x75.png';?>" />
					</a>
				<?php endforeach;?>
				</div>
			</div>
			<div class="col-md-1">
				<!--div class="form-group">
					<input type="text" class="form-control" name="title" placeholder="Filtros" value="" required>
				</div-->	
			</div><div class="clearfix"></div>
		</div>

		<div class="circle-progress c100 p0 dark">
	        <span>25%</span>
	        <div class="slice">
	            <div class="bar"></div>
	            <div class="fill"></div>
	        </div>
        </div>

		<div class="row-bottom">
			<div class="col-md-1 text-center">
				<h2><a href="javascript:void(0)" class="item-prev"><i class="ion-arrow-left-b"></i></a></h2>
			</div>
			<div class="col-md-10">
				<div class="text-center">
					<?php foreach($entries as $entry):?>

			        <section class="c-section">
			            <header class="c-section_header">
			                <h2 class="c-section_title u-text-gradient" id="item_title"><?php print $entry->title;?></h2>
			            </header>    
			        </section>

					<?php break; endforeach;?>
				</div>
			</div>
			<div class="col-md-1 text-center">
				<h2><a href="javascript:void(0)" class="item-next"><i class="ion-arrow-right-b"></i></a></h2>
			</div>
		</div>

		<div class="bottom-center-aligned">
	        <div id="item_video">

				<video id="example-video-element" autoplay loop preload="auto" width="100%">
					<source src="" type="video/webm">
				  Your browser does not support the video tag.
				</video>
			</div>
		</div>

		<div class="bottom-left-aligned">
			<div class="pad">
				<p><span><?php print locale('type');?>: <strong id="item_type"><?php print locale($entry->type->slug);?></strong></span></p>
				<p><span><?php print locale('grade');?>: <strong id="item_grade"><?php print locale($entry->grade->slug);?></strong></span></p>
				<p><span><?php print locale('class');?>: <strong id="item_class"><?php print locale($entry->class->slug);?></strong></span></p>
				<p><span><?php print locale('price');?>: <strong id="item_price"><?php print $entry->price;?> coins</strong></span></p>
				<p><span><?php print locale('skin-original');?>: <strong id="original_skin"><?php print $entry->original_skin;?></strong></span></p>
			</div>
		</div>
		<div class="bottom-right-aligned btn-custom">
			<div class="pad">
				<button class="btn btn-success"><i class="ion-android-cart"></i> <span><?php print locale('buy');?></span></button>
			</div>
		</div>		
	</div>
</div>
<style>

.circle-progress {
	position: absolute;
	left: 45%;
	top: 50%;
}

</style>

<script>

	var item_current = 0;
	var video;       
	var watchBuffer;

	loop = function() {

	    var b = video.buffered,
	        i = b.length,
	        vl = video.duration,
	        x1, x2;

	    while (i--) {
	        x1 = b.start(i) / vl;
	        x2 = b.end(i) / vl;
	    }

	    if( ! isNaN(x2)){
	    	var perc = (x2*100).toFixed(0);

	    	$('.circle-progress').removeClass().addClass('circle-progress c100 dark p' + perc).find('span').text(perc + '%');

	        if (x2 > 0.99) {
	            clearInterval(watchBuffer);
				$('.circle-progress').fadeOut(500,function(){
					$('#item_video video').fadeIn(500);	
				});                
	        }
	    }
	}

	function drawitem(item){

		$(".circle-progress").hide();

		if(watchBuffer){
			clearInterval(watchBuffer);
		}
		item.siblings().removeClass('img-thumbnail');
		item.addClass('img-thumbnail');

		$('#item_title').text(item.data('title'));
		$('#item_type').text(item.data('type'));
		$('#item_grade').text(item.data('grade'));
		$('#item_class').text(item.data('class'));
		$('#item_price').text(item.data('price'));
		$('#item_original-skin').text(item.data('original-skin'));

		$('#item_video video').fadeOut(500,function(){
			$('.circle-progress').find('span').text('');
			$('.circle-progress').removeClass().addClass('circle-progress c100 dark p0').fadeIn(500);

		    video = document.getElementsByTagName('video')[0];    
			video.src = '/upload/items/' + item.data('video');   
			video.load();

			watchBuffer = setInterval(loop, 500);
		});
	}

	$(function(){
		$('#item_video video').hide();
		$item = $($('.item-list').children().get(0));
		drawitem($item);

		$('.item-next').click(function(){
			if(item_current >= $('.item-list').children().length - 1) return;
			item_current++;
			$item = $($('.item-list').children().get(item_current));
			if(!$item.length) return;
			drawitem($item);
		});

		$('.item-prev').click(function(){
			if(item_current == 0) return;
			item_current--;
			$item = $($('.item-list').children().get(item_current));
			if(!$item.length) return;
			drawitem($item);
		});

		$('.item-list a').click(function(){
			item_current = $(this).data('index');
			$item = $($('.item-list').children().get(item_current));
			if(!$item.length) return;
			drawitem($item);
		});
	});

</script>