<link rel="stylesheet" href="<?php print site_asset('/css/packages.css');?>">

<div class="bg-container bg-packages container-fix darkfade">

<?php if(!empty(session('login'))):?>
    <div class="c-nav js-nav is-active">
        <div class="c-nav__content">
            <a href="#account.coins" data-json='{"coins":"0"}' data-toggle="modal">
            <div class="c-nav__section">
                <ul class="o-list-unstyled c-nav__list">
                    <li class="c-nav__list-item c-nav__icon-bag"></li>
                    <li class="c-nav__list-item">
                        <span class="c-nav__list-link"><?php print locale('donation_coins');?></span>
                    </li>
                    <li class="c-nav__list-item c-nav__icon-coin"></li>
                    <li class="c-nav__list-item">
                        <a href="#part-1" class="c-nav__list-link coins-balance"><?php print session('coins');?></a>
                    </li>
                </ul>
            </div><!-- /.c-nav__section -->
            </a>
        </div>
    </div><!-- /.c-nav -->
<?php endif;?>


    <header class="c-section_header section-admin bottom-margin">
        <h2 class="c-section_title u-text-gradient-admin"><?php print locale('joinbeta_slogan');?></h2>
    </header>  

    <div class="bg-content-wider">
    <?php foreach($packages as $pack):?>
        <div class="col-md-4">
            <div class="package<?php print (!empty($acquirements) AND !in_array($pack->slug,$acquirements)) ? ' not-available' : '';?>">
                <?php if(!empty($acquirements) AND !in_array($pack->slug,$acquirements)):?>
                    <div class="purchased-check"></div>
                <?php endif;?>                
                <div class="<?php print $pack->slug;?>">
                    <div class="circ">
                        <img src="<?php print site_asset('/img/packages/' . $pack->slug . '/circ.png');?>">
                    </div>
                    <div class="cofre <?php print (!empty($acquirements)) ? '' : ' avaliable';?>">
                        <img src="<?php print site_asset('/img/packages/' . $pack->slug . '/cofre.png');?>">
            <?php if(empty($acquirements)):?>
                <?php if(!session('login')):?>
                        <a href="#login" data-json='{"redirect":"<?php print site_url('/joinbeta');?>"}' data-toggle="modal">
                <?php else:?>
                    <?php if(session('coins') >= ($pack->coins - intval($pack->discount / 100 * $pack->coins))):?>
                        <a href="javascript:void(0)" class="confirm_package" data-json='{"id":"<?php print $pack->slug;?>","dcoins":"<?php print $pack->coins - intval($pack->discount / 100 * $pack->coins);?>"}'>
                    <?php else:?>
                        <a href="javascript:void(0)" class="confirm_donation" data-json='{"id":"<?php print $pack->slug;?>","coins":"<?php print $pack->coins - intval($pack->discount / 100 * $pack->coins);?>"}'>
                    <?php endif;?>
                <?php endif;?>                            
                            <div class="price">
                                <p><?php print $pack->coins - intval($pack->discount / 100 * $pack->coins);?></p>
                                <label><?php print locale('acquire');?></label>
                            </div>
                        </a>
            <?php endif;?>
                    </div>
                    <div class="toggle"><a href="javascript:void(0)"><img src="<?php print site_asset('/img/packages/' . $pack->slug . '/' . (in_array($pack->slug,$acquirements) AND in_array($pack->slug,['noble','baron'])) . 'boton.png');?>" id="off"></a></div>
                    <div class="text">
                        <h1><?php print ucfirst($pack->slug);?> <?php print locale('package');?></h1>
                <?php if(in_array($pack->slug,$acquirements)):?>
                        <p class="acquired"><?php print locale('acquired');?></p>
                        <p><?php print locale('fuse_heroes');?></p>
                <?php else:?>
                    <?php if($pack->discount):?>
                        <p class="discount"><?php print intval($pack->discount);?> <?php print locale('discounted');?></p>
                    <?php endif;?>
                        <p><?php print locale('reward');?></p>
                <?php endif;?>
                        
                    </div>
                    <div class="pack-info">
                        <?php if(!empty(locale('package_' . $pack->slug . '_features'))):?>
                        <ul>
                        <?php foreach(explode(",",locale('package_' . $pack->slug . '_features')) as $i => $feature):?>
                            <li><?php print $feature;?><?php print $i == 1 ? '&nbsp;&nbsp;<a href="javascript:void(0);" class="whatsthis" title="' . locale('whats_this') . '"><i class="fa fa-question-circle"></i></a>':'';?></li>
                        <?php endforeach;?>
                        </ul>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach;?>

    </div><div class="clearfix"></div>
</div>
<section id="preguntas-frecuentes" class="text-center">
    <h2 class="flourish"><span>Preguntas Frecuentes</span></h2>
    <div class="faq-group" data-vivaldi-spatnav-clickable="1">
        <div class="faq-item dscroll expanded">
            <h5>¿Por qué existen los paquetes recompensas?</h5>
            <p>Los paquetes recompensas fueron pensados y creados para (como bien su nombre lo indica) recompensar a quienes contribuyan en nuestra “etapa final de desarrollo” de cara al siguiente periodo. Esta recompensa se basa principalmente en el ingreso seguro a nuestra Beta cerrada, con la complementación de cierta cantidad de beneficios adicionales (a presente y futuro), dependiendo del paquete adquirido.</p>
        </div>
        <div class="faq-item dscroll">
            <h5>¿Tengo que conseguir un paquete recompensa para poder jugar Newline a futuro?</h5>
            <p>No. Newline será un juego gratuito cuando se lance oficialmente.</p>
        </div>
        <div class="faq-item dscroll">
            <h5>Si adquiero un paquete ¿Qué pasará después?</h5>
            <p>Al adquirir un paquete su cuenta se verá beneficiada automáticamente. En primer lugar, ya habrá asegurado su participación durante todo el transcuro del periodo BETA. También se le acreditaran automáticamente todas las recompensas acorde al paquete que haya obtenido. Además de esto, usted tendrá el privilegio de poder adquirir cualquier tipo de skin mediante sus Donations coins. (Las cuentas que no tengan acceso a la beta, deberán de esperar a poder jugar para poder conseguirlos).</p>
        </div>
        <div class="faq-item dscroll">
            <h5>¿Porque hay tres diferentes paquetes?</h5>
            <p>Entendemos que muchos jugadores poseen diferentes presupuestos y objetivos, e incluso no todos quieren colaborar con nuestro servidor de la misma forma. Por eso, hemos optado por ofrecerles una amplia gama de opciones diferentes. De esta manera, nos aseguramos de que todo el que apoye nuestro proyecto, tendrá la posibilidad de jugar en nuestra beta disfrutando al mismo tiempo, de varios beneficios adicionales.</p>
        </div>
        <div class="faq-item dscroll">
            <h5>¿Qué métodos puedo utilizar para conseguir estos paquetes? &nbsp; <i class="fa fa-support"></i></h5>
            <p>Actualmente los paquetes solo pueden ser adquiridos vía Payu (tarjeta – rapi pago/pago fácil). Los países que abarca esta empresa son los siguientes: Argentina, Brasil, Chile, Colombia, Czech Republic, Hungary, India, México, Nigeria, Panama, Peru, Poland, Romania, Russia, South Africa & Turkey. Solo los residentes de estos países serán quienes tengan la posibilidad de contribuir al servidor hoy en día. De todas formas, nos encontramos trabajando para implementar pronto Paypal, quien aumentará considerablemente la cantidad de países posibles. Sin embargo, nos gustaría lanzar nuestros sitios al inglés y al portugués antes de poder habilitarlo. Si desea ver la guía sobre como donar, haga <a href="<?php print config()->forum_url;?>/index.php?/topic/83-como-realizar-una-donaci%C3%B3n/#comment-150" target="_blank">click aquí</a>.</p>
        </div>
        <div class="faq-item dscroll">
            <h5>Si adquiero un paquete recompensa básico, o intermedio, ¿Puedo actualizarlo a un nivel más alto?</h5>
            <p>No. Actualmente no existe una opción de actualizar a un paquete de nivel mayor luego de efectuar su pago. Estamos investigando la posibilidad de añadir esta característica. Pero por ahora, lamentamos decirle que no será posible.</p>
        </div>
        <div class="faq-item dscroll">
            <h5>¿Puedo adquirir más de un paquete recompensa a la vez?</h5>
            <p>No. Mientras un paquete recompensa se mantenga efectivo sobre una cuenta, el usuario no podrá adquirir ningún otro paquete.</p>
        </div>
        <div class="faq-item dscroll">
            <h5 id="hero">¿Qué heroes ofrece cada paquete? &nbsp; <i class="fa fa-question-circle"></i></h5>
            <p class="hero">A continuación te mostramos que <a href="/guides/heroes" target="_blank">heroe</a> ofrece cada paquete.</p>
        </div>        
        <div class="faq-item dscroll">
            <h5>¿Qué pasa si mis Héroes favoritos no se incluyen en el paquete que adquirí? &nbsp; <i class="fa fa-star"></i></h5>
            <p>Hemos estado trabajando en este último tiempo sobre la implementación de una nueva función que soluciona este problema, sin la necesidad de tener que cambiar de paquete. Usted ahora podrá activar los treinta y un héroes con tan solo un click, a cambio de algunas Dona Coins. Si no sabe como hacerlo, haga <a href="<?php print config()->forum_url;?>/index.php?/topic/84-activando-todos-los-h%C3%A9roes/#comment-151" target="_blank">click aquí</a>.</p>
        </div>
        <div class="faq-item dscroll">
            <h5>¿Para qué sirven las “Donations Coins”? &nbsp; <i class="fa fa-star"></i></h5>
            <p>Las Donations Coins son y serán siempre las monedas más valiosas de Newline. Con ellas, el usuario podrá realizar cualquier tipo de compra que desee. Algunas de ellas podrían ser: Skins, Aghations, Cajas mágicas, Capas, Membresía VIP, Heroes, Dyes, Bonus de cuenta, Paquetes recompensas, etc.</p>
        </div>                                        
        <div class="faq-item dscroll">
            <h5>¿Cómo funciona el beneficio “Beta-Keys” del paquete Heroe? &nbsp; <i class="fa fa-star"></i></h5>
            <p>Las cuentas que posean activado el paquete Heroe gozarán de un privilegio único. Cada 15 días, automáticamente recibirán dos Beta-Keys de regalo. Estas Beta-Keys vencen al séptimo día de uso, por lo que podrían estar dándole acceso gratuito y constante a uno de sus mejores amigos. Lo más interesante de todo? Este beneficio se mantendrá de principio a fin durante todo el periodo BETA.</p>
        </div>                                        
        <div class="faq-item dscroll">
            <h5>¿Que es un usuario VIP? &nbsp; <i class="fa fa-star"></i></h5>
            <p>El usuario VIP es un usuario común, aunque con algunos privilegios únicos. Los privilegios son los siguientes:<br><br>
            • El usuario VIP puede cambiar el Nick de su personaje una vez por mes.<br>
            • El usuario VIP puede crear tickets VIP en nuestro sistema de soporte. Estos tickets tienen siempre un tiempo aproximado de respuesta mucho más corto.<br>
            • El usuario VIP puede escoger entre tres distintos colores para cambiar el color del Nick de su personaje.<br>
            • El usuario VIP puede cambiar el sexo de su personaje cuando así lo desee.<br>
            • El usuario VIP puede cambiar los rasgos físicos del personaje cuando así lo desee.<br>
            • El usuario VIP puede crear peticiones de abuso dentro del juego.<br><br>

<u>IMPORTANTE</u>: Ser usuario VIP no es definitivo. A futuro, quien se convierta en usuario VIP gozara de sus privilegios durante un periodo de dos mes. Luego, el VIP será retirado y el usuario deberá de volver a adquirirlo si así lo desea. El paquete héroe como bien menciona el detalle, ofrece membresía vip por 3 meses.</p>
        </div>                                        
        <div class="faq-item dscroll">
            <h5>¿Qué son y cómo funcionan las “Cajas Mágicas”?</h5>
            <p> Newline posee dos Cajas Mágicas diferentes. Por un lado se encuentra la caja intermedia, y por otro la caja Premium. Ambas cumplen la misma función. Al abrirlas mediante nuestro panel de usuario, las mismas podran ofrecernos diferentes skins sorpresas. Las cajas intermedias pueden tener cualquier tipo de skin grado A en su interior, mientras que las cajas premium pueden tener cualquier tipo de skin grado S. (No entran las skins únicas)</p>
        </div>                                        
        <div class="faq-item dscroll">
            <h5>¿Que son los “Bonus de cuenta”?</h5>
            <p>Los Bonus de cuenta se basan en la recarga adicional de Experiencia y Newline Points tras cada partida finalizada. Indiferentemente del resultado de las mismas, el usuario recibirá un 25% más de Experiencia y un 20% más de Newline Points de lo que debería. Creo que esta demás aclarar que estos bonus obviamente serán temporales. Al igual que las Beta-Keys, tendrán fecha de vencimiento.</p>
        </div>                                        
        <div class="faq-item dscroll">
            <h5>¿De qué trata la Beta cerrada?</h5>
            <p>El periodo “BETA CERRADA” será lanzado en Newline para cumplir con varios objetivos. El principal sin duda alguna será lograr analizar el adaptamiento y rendimiento de nuestro servidor, con mayores cantidades de usuarios jugando al mismo tiempo. Pudiendo recibir de forma ágil y ordenada todos los reportes de bugs surgidos en los usuarios mediante nuestro sistema de tickets. Por otra parte también estaremos puliendo los últimos detalles, y como no, analizando las primeras sugerencias de nuestros jugadores.</p>
        </div>                                        
        <div class="faq-item dscroll">
            <h5>¿Qué significa "Sorteo para participar del Alpha"?</h5>
            <p>Como bien ya mencionamos en nuestro foro, los primeros 500 usuarios que adquieran paquetes (baron o héroe) participaran automáticamente de un sorteo único, con el objetivo de intentar conseguir el acceso a la etapa final del periodo ALPHA. Entregaremos 60 accesos para dicha etapa.</p>
        </div>                                        
        <div class="faq-item dscroll">
            <h5>¿Por qué actualmente los paquetes se encuentran en promoción?</h5>
            <p>Actualmente hemos decidido poner los paquetes recompensas con un 30% de descuento debido a que no estamos dando una fecha exacta de lanzamiento. Se podría decir que lo hacemos principalmente para valorar la confianza absoluta de nuestros usuarios respecto al proyecto. Como muchos saben, Newline es una realidad desde el momento en el que se comenzó a desarrollar, pero también entendemos que no todos están tan al tanto de eso. A los que aún no lo esten, bastará solamente con ver los avances diarios. <br><br><u>IMPORTANTE</u>: Las promociones dejarán de surgir efecto desde el día en el que se anuncie la fecha de lanzamiento.</p>
        </div>          <div class="faq-item dscroll">
            <h5>¿Cuándo comenzara el acceso al Beta cerrada?</h5>
            <p>Al día de hoy no queremos comprometernos en dar una fecha exacta de lanzamiento debido a que la misma puede verse modificada por diversos motivos. De todas formas, como ya saben nos encontramos bastante cerca. El juego ya viene corriendo hace unos meses sin problemas, pero queremos lograr una BETA digna, destacada y divertida. Para ello, es necesario entrar como de costumbre en el detalle, y seguir trabajando duro como lo venimos haciendo hasta hoy. Lo mantendremos informado en el día a día respecto a esto.</p>
        </div>          <div class="faq-item dscroll">
            <h5>¿Cuándo comenzara la Beta abierta?</h5>
            <p>Al día de la fecha no tenemos manera de responderle esta pregunta con exactitud, ya que como bien mencionamos en el punto anterior, aún no sabemos cuánto puede llegar a demorar nuestra beta cerrada (etapa anterior a la beta abierta). Puede quedarse tranquilo de todas formas, ya que cuando sepamos la fecha exacta, lo anunciaremos con tiempo de anticipación.</p>
        </div>          <div class="faq-item dscroll">
            <h5>¿Habrá forma de entrar a la beta cerrada sin obtener paquetes de recompensa?</h5>
            <p>Es probable aunque será bastante difícil ya que abra escases de beta keys. Nuestra prioridad es asegurar en primer medida los lugares de quienes contribuyan al proyecto. Una vez estemos seguros de esto, entonces probablemente iremos sorteando una X cantidad de keys básicos (sin beneficios adicionales) que dependerá de la cantidad de paquetes entregados hasta el momento. Por ende, no podemos asegurarle ni su participación en la misma, ni tampoco la cantidad de keys que vayamos a entregar en esta etapa. </p>
        </div>          <div class="faq-item dscroll">
            <h5>Una vez finalizada la beta. ¿El servidor comenzara desde cero?</h5>
            <p>Sí y no. Una vez finalizada la beta abierta, el juego sufrirá un rollback. Los únicos datos que conservaremos serán los siguientes: Cuentas (todas), Nick de los personajes que participaron en la beta cerrada, Skins, Donations coins, Cajas mágicas & Cuentas VIP. Se perderá cualquier héroe adquirido en el transcurso de las betas, como también clanes, nivel de cuentas, listado de amigos, dyes, etc.</p>
        </div>          <div class="faq-item dscroll">
            <h5>¿Dónde puedo descargar el juego?</h5>
            <p>Días antes del lanzamiento BETA estaremos publicando el link de nuestro UPDATER, el cual podrán bajarse todos nuestros usuarios. Sin embargo, solo quienes tengan su cuenta activa para el BETA, serán quienes puedan ingresar a jugar.</p>
        </div>          
        <div class="faq-item dscroll">
            <h5>¿Cuáles son los requerimientos minimos del sistema para jugar?</h5>
            <p class="req">A continuación te mostramos una tabla con los requerimientos minimos y recomendados para jugar Newline</p>
        </div>  
        <div class="faq-item dscroll">
            <h5>¿Poseo algún privilegio más por ser partícipe de la beta?</h5>
            <p>Si. Si su cuenta es participe de la beta entonces pasara a reconocerse como FUNDADORA. Dentro del juego, el color del título de su personaje (al contrario del resto) se vera celeste. Por otra parte, usted ya podrá adquirir skins, y conservar su Nick de personaje.</p>
        </div>  

        <div class="faq-item dscroll">
            <h5>¿Todos los beneficios adicionales se consiguen de forma instantánea?</h5>
            <p>No. Si bien la mayoría de los beneficios se entregan de forma instantánea como ser: Donations Coins, Heroes, Membresía VIP, etc. Otros recién serán entregados en el lanzamiento del BETA, como: Las Beta-Keys, las Cajas Mágicas, etc. Y por último, también tendremos algunas entregas pauseadas para el lanzamiento oficial, debido a que de nada serviría entregarlas antes, como bien es el caso de los bonus de XP & NLP.</p>
        </div>  

    </div>

    <a href="javascript:void(0)" class="totop floating"></a>

</section><div class="clearfix"></div>    

<script type="text/javascript" src="<?php print site_asset('/js/packages.js');?>"></script>        
<script>

$(function(){

    $('.confirm_donation').click(function(){
        $this = $(this);
        var data = $(this).data("json")||null;
        var text = '<?php print str_replace(["\n","\r","\r\n"],"<br>",locale('confirm_donation'));?>';
        text = text.replace('pack',data.id);

        BootstrapDialog.confirm({
            type : BootstrapDialog.TYPE_WARNING,
            message: text,
            callback: function(result){
                if(result) {
                    modal_click('account.coins',jsonToQueryString(data));
                }
            }
        });
    });

    $('.confirm_package').click(function(){
        $this = $(this);
        var data = $(this).data("json")||null;
        var text = '<?php print str_replace(["\n","\r","\r\n"],"<br>",locale('confirm_package_acquirement'));?>';
        text = text.replace('pack',data.id);
        text = text.replace('dcoins',data.dcoins);
        BootstrapDialog.confirm(text, function(result){
            if(result) {
                $.ajax({
                    type:       'post',
                    url:        '/account/joinbeta',
                    data:       data,
                    }).done(function(json) {
                        form_lookup(json);

                        if(json.code=='success'){
                            $('.coins-balance').fadeOut().text(json.balance).fadeIn();
                        }

                    });
            }
        });
    });

    modal_click('modal',jsonToQueryString({"title":"joinbeta_alert_community_title","message":"joinbeta_alert_community_msg"},1),"alert-dialog info","size-wide");
});
</script>