<div class="bg-container">
    <div class="bg-content-wide">
        <div class="widget color red">
            <div class="database-migration">
				<h3><?php print locale('payment_result');?></h3>
				<p><?php print locale('payment_result_text');?></p>
				<h2>Resumen Transacción</h2>
				<table>
				<tr>
				<td>Estado de la transaccion</td>
				<td><?php echo $estadoTx; ?></td>
				</tr>
				<tr>
				<tr>
				<td>ID de la transaccion</td>
				<td><?php echo $transactionId; ?></td>
				</tr>
				<tr>
				<td>Referencia de la venta</td>
				<td><?php echo $reference_pol; ?></td> 
				</tr>
				<tr>
				<td>Referencia de la transaccion</td>
				<td><?php echo $referenceCode; ?></td>
				</tr>
				<tr>
				<?php
				if($pseBank != null) {
				?>
				<tr>
				<td>cus </td>
				<td><?php echo $cus; ?> </td>
				</tr>
				<tr>
				<td>Banco </td>
				<td><?php echo $pseBank; ?> </td>
				</tr>
				<?php
				}
				?>
				<tr>
				<td>Valor total</td>
				<td>$<?php echo number_format($TX_VALUE); ?></td>
				</tr>
				<tr>
				<td>Moneda</td>
				<td><?php echo $currency; ?></td>
				</tr>
				<tr>
				<td>Descripción</td>
				<td><?php echo ($extra1); ?></td>
				</tr>
				<tr>
				<td>Entidad:</td>
				<td><?php echo ($lapPaymentMethod); ?></td>
				</tr>
				</table>
			</div>
		</div>
    </div>
</div>		