<div class="bg-container">
    <div class="bg-content-wide">

        <div class="widget white">
            <div class="database-migration">
                <h3><?php print locale('payment_error');?></h3>
                <p><?php print locale('payment_error_text');?></p>
            <?php if( ! empty($error)):?>
            	<p><?php print $error;?></p>
            <?php endif;?>
            </div>
        </div>
    </div>
</div>