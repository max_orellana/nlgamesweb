<div class="bg-container">
    <div class="bg-content-wide">
        <div class="widget white">
            <div class="database-migration">
                <h3><i class="ion-ios-bolt fa-x1"></i> <?php print locale('payment_connecting');?></h3>
                <p><?php print locale('payment_connecting_text');?></p>
            </div>
        </div>

        <form class="form" action="<?php print site_url('/account/checkout');?>" method="post">
            <input type="hidden" name="id" value="<?php print $order->id;?>" />
        </form>
    </div>
</div>
<script type="text/javascript">

    function checkout(){
        return $('.form').submit();
    }

    setTimeout("checkout()",1000);

</script>