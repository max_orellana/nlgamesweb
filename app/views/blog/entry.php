<div class="col-md-12 col-xs-12 content text-shadow border-radius">
    <div class="col-md-12 col-xs-12 single">
        <div class="no-pad light-gold main-title pull-left"><?php echo $entry->{'title_' . LOCALE};?></div>
        <div class="no-pad light-gold creator pull-right"><?php print locale('published_by');?>
            <span class="gold"><?php print $entry->login;?></span> | <?php print timespan($entry->created);?>
        </div>
        <div class="col-md-12 col-xs-12 text">
    <?php if($entry->files()):?>
            <div class="text-center">
            <?php foreach($entry->files() as $file) : if( ! isset($file->name)) continue;?>
                <img class="avatar" src="<?php print site_url('/upload/posts/1090x200/' . $file->name);?>" alt="<?php echo $entry->{'title_' . LOCALE};?>">
            <?php endforeach;?>
            </div>
        <?php if(count($entry->files()) > 1):?>
            <div id="feed-slider-counts">
            <?php foreach($entry->files() as $file):?>
                <li></li>
            <?php endforeach;?>
            </div>
        <?php endif;?>
    <?php endif;?>  
            <div class="group-control">&nbsp;</div>
            <div class="entry-content">
                <?php echo $entry->{'content_' . LOCALE};?>
            </div>
            <div class="group-control">&nbsp;</div>
        </div>
    </div>
</div>