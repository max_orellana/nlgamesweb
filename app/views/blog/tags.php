<div class="group-control">&nbsp;</div>
<blockquote>
    <em><i class='ion-pound'></i> <?php echo $tag;?></em>
</blockquote>
<div class="col-md-12 col-xs-12 content border-radius">
    <div class="col-md-8 col-xs-12 no-pad main">

    <?php if($posts):?>
        <ul class="col-md-12 col-xs-12 no-pad latest-news">
        <?php foreach($posts as $post):?>
            <li class="col-md-12 col-xs-12">
                <div class="col-md-12 col-xs-12 text-shadow no-pad title">
                    <div class="col-md-3 col-xs-3 border-radius no-pad pic-thumb" style="background-image: url(<?php print site_asset('img/slider1-50x50_c.jpg');?>" alt="<?php echo $post->title;?>)"></div>
                    <div class="col-md-9 col-xs-9 light-gold main-title"><?php echo $post->title;?></div>
                    <div class="col-md-9 col-xs-9 light-gold creator">by <span class="gold"><?php echo $post->login;?></span></div>
                    <div class="col-6 col-xs-6 read-more border-radius right btn-expand"><span class="fi-plus"></span></div>
                </div>
                <a href="<?php echo site_url('blog/' . $post->slug);?>">
                    <div class="col-md-12 col-xs-12 no-pad back-thumb">
                        <div class="col-md-12 col-xs-12 no-pad border-radius pic" style="background-image:url(<?php echo site_url('upload/posts/800x300/' . (count($post->files()) ? $post->files()[0]->name : 'default.jpg'));?>)"></div>
                        <div class="col-md-12 col-xs-12 no-pad text-shadow text"><p><?php echo $post->caption;?></p></div>
                        <div class="col-6 col-xs-6 read-more border-radius right">Leer más</div>
                    </div>
                </a>
            </li>
        <?php endforeach;?>
        </ul>
    <?php endif;?>        
    </div>
    <div class="col-md-4 col-xs-12 no-pad side-bar">
       <div class="col-md-12 col-xs-12 no-pad text-shadow facebook">
           <div class="col-md-12 col-xs-12 light-gold main-title">Facebook</div>
           <div class="col-md-12 col-xs-12 iframe">
               <iframe id="facebook-iframe" src="http://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/www.nlgames.net&amp;show_border=false&amp;colorscheme=dark&amp;show_faces=true&amp;t&amp;stream=false&amp;header=false&amp;height=450" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
            </div>
        </div>
        <div class="col-md-12 col-xs-12 no-pad text-shadow latest-media">
            <div class="col-md-12 col-xs-12 light-gold main-title">Trailer</div>
            <a href="https://www.youtube.com/watch?v=EHfNokKrzrg" class="lightview" data-lightview-type="iframe" data-lightview-options="width: 854, height: 510, viewport: 'scale'" data-lightview-title="Closed Beta Gameplay">
                <div class="col-md-12 col-xs-12 no-pad video" style="background-image:url(<?php print site_asset('img/trailer.jpg');?>)"></div>
                <div class="col-md-12 col-xs-12 button-media">Capítulo I</div>
            </a>
        </div>
        <div class="col-md-12 col-xs-12 no-pad text-shadow editors-pic">
            <div class="col-md-12 col-xs-12 light-gold main-title">Sobre newline</div>
            <div id="container1">
                <div id="feed-slider">
                    <span class="control prev border-radius"><span class="fi-play"></span></span>
                    <div id="inner-slider">
                    <?php if($about):?>                    
                        <ul>
                        <?php foreach($about as $post):?>
                            <li class="fans-list">
                                <div class="fans-list-con">
                                    <p class="fans-list-avatar">
                                        <a class="title" href="<?php echo site_url('blog/' . $post->slug);?>">
                                            <img class="avatar" src="<?php echo site_url('upload/posts/300x200/' . (count($post->files()) ? $post->files()[0]->name : 'default.jpg'));?>" alt="<?php echo $post->title;?>">
                                        </a>
                                    </p>
                                    <p class="fans-list-title">
                                        <a class="title" href="<?php echo site_url('blog/' . $post->slug);?>">
                                            <span><?php echo $post->title;?></span>
                                        </a>
                                    </p>
                                </div>
                            </li>
                        <?php endforeach;?>
                        </ul>  
                    <?php endif;?>              
                    </div>
                    <span class="control next border-radius"><span class="fi-play"></span></span>
                </div>
                <div id="feed-slider-counts">
                    <?php foreach($about as $post):?>
                            <li></li>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xs-12 no-pad text-shadow editors-pic">
            <div class="col-md-12 col-xs-12 light-gold main-title">VOTA POR NOSOTROS</div>
            <div align="center"><br><br><br><br> <img src="<?php print site_asset('img/HOPZONE.png');?>" width="220" height="80"><br><br></div>
        </div>
    </div>
</div>
