<!-- This page exists here because you requested a url that is not handled back by this aplication. Please givus a hint to info@nlgames.net of what you are looking for and maybe we can add it to the views -->

<div class="col-md-12">
	<h1 class="page-header"> I just can't handle baby! <i class='ion-ios-musical-note'></i> <i class='ion-music-note'></i> </h1> 
    <blockquote>
		<em>Our apologies for the temporary inconvenience.</em>
	</blockquote>
  	<p>The following route <em><?php echo $_SERVER['REQUEST_URI'];?></em> is not actually handled directly by this application. This is why the server fell back here. We suggest you try one of the links below:</p><ul><li><b>Verify url and typos</b> - The web page you were attempting to view may not exist or may have moved - try <em>checking the web address for typos</em>.</li><li><b>E-mail us</b> - If you followed a link from somewhere, please let us know at <a href='mailto:info@nlgames.net'>info@nlgames.net</a>. Tell us where you came from and what you were looking for, and we'll do our best to fix it.</li></ul>
</div>