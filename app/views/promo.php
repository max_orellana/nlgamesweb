

<div class="c-options c-options--top c-options--right u-hidden-less-than-sm js-alert-offset hide">
    <button class="c-circle js-toggle-audio" data-original-title="" title=""><i class="c-icon c-icon--volume-mute js-audio-icon-mute"  style="display: none;"></i><i class="c-icon c-icon--volume-low js-audio-icon-play"></i></button>
</div>


<section id="section-1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 header">
                <div class="h2"><span>Life is a game, play it!</span></div>
                <div class="h1"><span>Newline coming soon...</span></div>
            </div>            
        </div>
    </div>
    <div class="btn-container">
        <a class="btn btn-primary btn-lg" href="<?php print site_url('/joinbeta');?>">
            <span><?php print locale('join_beta');?></span>
        </a><div class="clearfix"></div>
        <a class="btn-sub" href="<?php print site_url('/news');?>">
            <span><?php print locale('mainpage');?></span>
        </a>
        <a class="btn-sub-graphic goscroll floating" href="#section-3">
            <img src="<?php print site_asset('/img/arrow-down.png');?>" width=88 height=66>
        </a>
    </div>
</section>

<section id="section-3">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-9 col-centered">
                <div class="h2"><span>LA <a href="<?php print site_url('/story');?>" target="_blank">HISTORIA</a> Y LAS REGLAS HAN CAMBIADO.</span></div>
                <div class="h3"><span> ¡ESTO ES NEWLINE!</span></div>
                <p><span> Newline es un juego en línea sumamente competitivo de estilo MOBA (basado en Lineage2, y en tercera persona), quien combina y explota todas las mecánicas y habilidades propias de cada jugador, con la sinergia que este tenga con sus compañeros de equipo. En nuestro clásico modo de juego, dos equipos de cinco integrantes se enfrentaran cara a cara con sus Héroes en una batalla épica dentro de la nueva y famosa, <a href="#arena" class="goscroll">"Arena Inferno"</a>. El objetivo principal de los participantes, será lograr implementar tras cada partida la estrategia que crean más adecuada, para intentar derribar la base enemiga antes que el equipo contrario.</span>
                </p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-9 col-centered">
                <div class="h1"><span>¿QUE NOS TRAE NEWLINE PARA ESTE 2016?</span></div>
                <div class="h4"><span class="subtitle">Newline nos trae lo que muchos ya conociamos mejorado, y aún más.</span>
                </div>
                <p><span>Debes saberlo. El numeroso <a href="<?php print site_url('/nlteam');?>" target="_blank">Staff de Newline</a> ha trabajado meticulosamente sin cesar en estos últimos tres años para asegurarse de ofrecer un servidor totalmente único, e increíble. Trabajando duro en conjunto, y gracias a la nueva jugabilidad implementada, consideramos haber mejorado notablemente la experiencia del usuario. Por una parte, hemos solucionado grandes cantidades de problemas natos del juego, y por otra, hemos creado contenido realmente fascinante, y totalmente impensado al menos hasta ayer. Por dichos motivos, le aseguramos que Newline podrá ofrecerle desde sus principios.</span>
                </p>
            </div>
            <div class="bottom-content">
                <div class="col-xs-12 col-md-4">
                    <div class="h5"><span>UN BALANCE ÚNICO</span>
                    </div>
                    <p><span>Newline nos ofrece un excelente balance formado prácticamente desde cero, en donde la ventaja se verá reflejada solamente en aquellos usuarios que posean un buen manejo del juego. Hemos logrado armar un sistema mucho más transparente y prolijo de lo que estábamos acostumbrados. Sin posibilidad de personajes editados, y siempre en igualdad de condiciones.</span>
                    </p>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="h5"><span>EQUIDAD EN RECOMPENSAS </span>
                    </div>
                    <p><span>Newline se encuentra totalmente comprometido en este tema. Las recompensas que recibirán los usuarios que contribuyan al desarrollo del proyecto, serán siempre totalmente justas. Con esto queremos decir que jamás intervendrán de ninguna manera en la jugabilidad. Para ser aún más claros, los stats de los personajes no se verán modificados en ningún momento.</span>
                    </p>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="h5"><span>DEDICACIÓN A GUSTO</span>
                    </div>
                    <p><span>Sabemos que no todos los usuarios cuentan con la misma cantidad de tiempo disponible para dedicarle al juego. Gracias a esta nueva jugabilidad, Newline ofrece tranquilidad. Nuestro nuevo sistema de emparejamiento se encargará de que todos nuestros usuarios jueguen siempre con aliados y enemigos cercanos a su nivel de cuenta. De esta manera, usted podrá escalar dentro del juego al ritmo que desee.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="skin-tree hidden-xs hidden-sm">
            <div class="skin-item">
                <div class="type">
                    <div class="name"><span>TUS HÉROES FAVORITOS DE SIEMPRE Y MÁS</span>
                    </div>
                    <div class="skin-info">
                        <p><span>En Newline no solo estarán las razas y profesiones de tus héroes favoritos. Con el correr del tiempo llegarán muchas sorpresas más.</span>
                        <span><a href="<?php print site_url('/guides/heroes');?>" target="_blank">Conoce los héroes actuales</a></span>
                        </p>
                    </div>
                </div>
                <div class="skin">&nbsp;</div>
            </div>
            <div class="skin-item">
                <div class="type">
                    <div class="name"><span>MINIONS - MINIONS ELITE - BOSS - MOBS NEUTRALES</span>
                    </div>
                    <div class="skin-info">
                        <p><span>Criaturas con habilidades asombrosas lucharán desde la primera línea de batalla. Los minions elite estarán siempre listos para salir a ganar. Y tú?</span><br><span><a href="<?php print site_url('/guides/mobs');?>" target="_blank">Conoce los NPCs actuales</a></span>
                        </p>
                    </div>
                </div>
                <div class="skin floating">&nbsp;</div>
            </div>
            <div class="skin-item">
                <div class="type">
                    <div class="name"><span>SKINS ORIGINALES & CUSTOM</span>
                    </div>
                    <div class="skin-info">
                        <p><span>Newline ofrece skins originales gratuitos, y skins customs para quienes quieran cambiar su apariencia visual.</span><br><span><a href="<?php print site_url('/guides/items');?>" target="_blank">Conoce los items originales</a></span>
                        </p>
                    </div>
                </div>
                <div class="skin">&nbsp;</div>
            </div>
        </div>
    </div>
</section>

<iframe id="arena" class="embeded-content-height" width="100%" height="100%" scrolling="no" frameBorder="0" src="<?php print site_url('/guides/inferno?orig=promo');?>"></iframe>

<div class="section-shared-bg">
    <section id="section-4">
        <div class="container">
            <div class="row">
                <div>
                    <div class="col-xs-12 col-md-9">
                        <p></p>
                        <div class="h2"><span>Valoramos inmensamente su apoyo</span></div>
                        <p><span>Nos encontramos realmente felices de contar con tanto apoyo. Nos hace confirmar que estamos yendo por el camino correcto. Las contribuciones que hacen nuestros usuarios permiten que continuemos con el desarrollo del proyecto; realizando cada mantenimiento correspondiente, y optimizándolo de forma constante. Por eso, hemos implementado un método eficaz y justo de recompensa para todo donador. Cuando un usuario <a href="<?php print config()->forum_url;?>/index.php?/topic/59-donations/" target="_blank">realice una donación</a>, el staff lo recompensará otorgándole “Donations Coins” de forma automática. Estas monedas podrán ser utilizadas para ir adquiriendo diferentes cosas, como ser: Skins, Bonus de cuenta, Membresía VIP, Acceso al beta, etc. Si desea contribuir con el proyecto, 
                <?php if(!session('login')):?>
                        <a href="#login" data-json='{"redirect":"<?php print site_url('/joinbeta');?>"}' data-toggle="modal">haga click aquí</a>
                <?php else:?>
                        <a href="#account.coins" data-json='{"coins":"0"}' data-toggle="modal">haga click aquí.</a>
                <?php endif;?>                            
                        </span></p>
                    </div>
                    <div class="hidden-xs hidden-sm col-md-3">
                        <div class="image"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="section-5">
        <div class="container">
            <div class="row">
                <div>
                    <div class="hidden-xs hidden-sm col-md-6">
                        <!--div class="image"><img src="<?php print site_asset('/promo/img/bg-promo6.png');?>"></div-->
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <p></p>
                        <div class="h2"><span>COMPITE A LO GRANDE. SÉ UN HÉROE!</span></div>
                        <p><span>Somos fieles a nuestra creencia al sentir que el espíritu principal de todos los juegos es la competencia. Por lo que nos basamos en ello durante todo el desarrollo de Newline. Hoy en día, tras la nueva jugabilidad implementada, los objetivos han cambiado radicalmente, y con ello, también la forma de progresar. Los usuarios que decidan llevar su juego a un nivel más competitivo, serán quienes se adentren a participar en nuestras ligas, con el objetivo principal de ir escalando día a día en las distintas divisiones. A nivel grupal, los jugadores podrán jugar partidas clasificatorias con sus clanes, para intentar alcanzar el nivel 11 (el más alto del juego), mientras que a nivel individual, el jugador intentara llegar a la cima alcanzando la división Héroe.</span></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include SP . 'app/views/shared/footer.php';?>
<!--a href="javascript:void(0)" class="totop">
    <i class="fa fa-arrow-circle-up"></i>
</a-->

<div class="clearfix"></div>    
