<div class="website-wrapper container-height">
    <section class="content" id="hero" data-hero="<?php print $heroe->slug;?>">
        <div class="sigil-box <?php print $heroe->slug;?>"><h4><?php print $heroe->title;?></h4></div>
        <div class="hero-wrapper">
        	<section id="hero-header">
                <div class="header-left-container">
                    <div class="gameplay-box">
                        <div class="gameplay-content">
                            <p><?php print $heroe->caption;?></p>
                        </div>
                    </div>
                </div>
            </section>
            <div class="hero-body-wrapper">
                <section id="hero-details">
                    <div class="detail-row stats">
                        <h3>Dificultad del Heroe</h3>
                        <div class="difficulty-bar">
                            <div class="fill-difficulty-bar-<?php print $heroe->difficulty;?>"></div>
                            <ul class="num-contain">
                                <li>1</li>
                                <li>2</li>
                                <li>3</li>
                                <li>4</li>
                                <li>5</li>
                            <ul>
                        </div>                    
                        <h3>Hero Stats</h3>
                        <div class="stat-bar-contain">
                            <div class="stat-value">
                                <span class="title">Damage</span>
                                <span class="value"><?php print $heroe->damage;?>%</span>
                            </div>           
                            <div class="icon-attack-damage"></div>
                            <div class="stat-bar">
                                <div class="fill-attack-damage" style="width:<?php print $heroe->damage;?>%"><span class="fill"><span class="fill-end"></span></span></div>
                            </div>
                        </div>
                        <div class="stat-bar-contain">
                            <div class="stat-value">
                                <span class="title">Armor</span>
                                <span class="value"><?php print $heroe->armor;?>%</span>
                            </div>                        
                            <div class="icon-attack-range"></div>
                            <div class="stat-bar">
                                <div class="fill-attack-range" style="width:<?php print $heroe->armor;?>%"><span class="fill"><span class="fill-end"></span></span></div>
                            </div>
                        </div>
                        <div class="stat-bar-contain">
                            <div class="stat-value">
                                <span class="title">Agility</span>
                                <span class="value"><?php print $heroe->agility;?>%</span>
                            </div>                        
                            <div class="icon-health"></div>
                            <div class="stat-bar">
                                <div class="fill-health" style="width:<?php print $heroe->agility;?>%"><span class="fill"><span class="fill-end"></span></span></div>
                            </div>
                        </div>
                        <div class="stat-bar-contain">
                            <div class="stat-value">
                                <span class="title">Utility</span>
                                <span class="value"><?php print $heroe->utility;?>%</span>
                            </div>                        
                            <div class="icon-mana"></div>
                            <div class="stat-bar">
                                <div class="fill-mana" style="width:<?php print $heroe->utility;?>%"><span class="fill"><span class="fill-end"></span></span></div>
                            </div>
                        </div>
                        <div class="stat-bar-contain">
                            <div class="stat-value">
                                <span class="title">Speed</span>
                                <span class="value"><?php print $heroe->speed;?>%</span>
                            </div>                        
                            <div class="icon-armor"></div>
                            <div class="stat-bar">
                                <div class="fill-armor" style="width:<?php print $heroe->speed;?>%"><span class="fill"><span class="fill-end"></span></span></div>
                            </div>
                        </div>
                    </div>
                    <div class="detail-row abilities">
                        <h3>Habilidades</h3>
                        <ul class="ability-wrapper">
                            <li class="ability key-1 hero_<?php print $heroe->slug;?> selected" data-ability="q-key" data-key="q">
                                <div class="keyboard-key">F1</div>
                            </li>
                            <li class="ability key-2 hero_<?php print $heroe->slug;?>" data-ability="w-key" data-key="w">
                                <div class="keyboard-key">F2</div>
                            </li>
                            <li class="ability key-3 hero_<?php print $heroe->slug;?>" data-ability="e-key" data-key="e">
                                <div class="keyboard-key">F3</div>
                            </li>
                            <li class="ability key-4 hero_<?php print $heroe->slug;?>" data-ability="r-key" data-key="r">
                                <div class="keyboard-key">F4</div>
                            </li>
                            <li class="ability key-5 hero_<?php print $heroe->slug;?>" data-ability="s-key" data-key="s">
                                <div class="keyboard-key">F5</div>
                            </li>
                            <li class="ability key-6 hero_<?php print $heroe->slug;?>" data-ability="d-key" data-key="d">
                                <div class="keyboard-key">F6</div>
                            </li>
                            <li class="ability key-7 hero_<?php print $heroe->slug;?>" data-ability="f-key" data-key="f">
                                <div class="keyboard-key">F7</div>
                            </li>
                            <li class="ability key-8 hero_<?php print $heroe->slug;?>" data-ability="g-key" data-key="g">
                                <div class="keyboard-key">F8</div>
                            </li>
                        </ul>
                        <div class="ability-description" id="q-key">
                            <div class="description-contain">
                                <h5>Corpse Life Drain</h5>
                                <div class="mana-contain">
                                    <div class="mana-box"></div>
                                    <span class="text-box">35/50/65</span>
                                </div>
                                <p>Absorbe la energia de la vida residual de un cadáver para restaurar su propia HP.</p>
                            </div>
                        </div>
                        <div class="ability-description" id="w-key">
                            <div class="description-contain">
                                <h5>Prominence</h5>
                                <div class="mana-contain">
                                    <div class="mana-box"></div>
                                    <span class="text-box">25/30/50</span>
                                </div>
                                <p>Lanza una magia de fuego que inflige daño sobre el enemigo.</p>
                            </div>
                        </div>
                        <div class="ability-description" id="e-key">
                            <div class="description-contain">
                                <h5>Elemental Crash</h5>
                                <div class="mana-contain">
                                    <div class="mana-box"></div>
                                    <span class="text-box">40/60/70</span>
                                </div>
                                <p>Inflige un ataque de magia con propiedades elementales sobre el objetivo, que lo hace desplazarse hacia atrás reduciendo su velocidad de movimiento.</p>
                            </div>
                        </div>
                        <div class="ability-description" id="r-key">
                            <div class="description-contain">
                                <h5>Count of Fire</h5>
                                <div class="mana-contain">
                                    <div class="mana-box"></div>
                                    <span class="text-box">40/50/60</span>
                                </div>
                                <p>Utiliza el poder del fuego para quemar al enemigo e infligir daño por segundo.</p>
                            </div>
                        </div>
                        <div class="ability-description" id="s-key">
                            <div class="description-contain">
                                <h5>Elemental Destruction</h5>
                                <div class="mana-contain">
                                    <div class="mana-box"></div>
                                    <span class="text-box">40/50/60</span>
                                </div>
                                <p>Inflige un ataque de magia con propiedades elementales a un grupo de enemigos.</p>
                            </div>
                        </div>
                        <div class="ability-description" id="d-key">
                            <div class="description-contain">
                                <h5>Fire Vortex</h5>
                                <div class="mana-contain">
                                    <div class="mana-box"></div>
                                    <span class="text-box">35/50/65</span>
                                </div>
                                <p>Invoca un vórtice dimensional de fuego que inflige daño sobre el enemigo.</p>
                            </div>
                        </div>
                        <div class="ability-description" id="f-key">
                            <div class="description-contain">
                                <h5>Flame Armor (Ultimate)</h5>
                                <div class="mana-contain">
                                    <div class="mana-box"></div>
                                    <span class="text-box">50/65/75</span>
                                </div>
                                <p>Envuelve al usuario con una capa protectora de fuego y aumenta la chance de realizar críticos magicos.</p>
                            </div>
                        </div>
                        <div class="ability-description" id="g-key">
                            <div class="description-contain">
                                <h5>Meteor (Ultimate)</h5>
                                <div class="mana-contain">
                                    <div class="mana-box"></div>
                                    <span class="text-box">50/65/80</span>
                                </div>
                                <p>Invoca meteoros que infligen daño contínuo de fuego en un área seleccionada.</p>
                            </div>
                        </div>
                    </div>
                    <div class="detail-row passive">
                        <h3>Pasiva</h3>
                        <ul class="passive-wrapper">
                            <li class="passive p-1 selected hero_<?php print $heroe->slug;?>">
                            </li>
                        </ul>                       
                        <div class="passive-description" style="display:block">
                            <div class="description-contain">
                                <h5>Sorcerer pasive</h5>
                                <p>Disminuye el consumo de MP de todos las habilidades.</p>
                            </div>
                        </div>                        
                    </div>
                </section>
            </div>
        </div>
    </section>
</div>