<div class="col-md-12 col-xs-12 content text-shadow border-radius">
	<div class="col-md-12 col-xs-12 single">
    	<div class="no-pad light-gold main-title">Aprende a moverte</div>
      		<div class="no-pad light-gold creator">Posted by 
        		<span class="gold">Redline</span> | 28 de Diciembre del 2014
      		</div>
      		<div class="col-md-12 col-xs-12 text">
        		<div class="group-control">&nbsp;</div>
	  			<p align="center"><img src="/assets/009/img/pages/suchi10.png" width="689" height="140"><br>
  <br>
  Controlar tu campeón puede resultar una tarea bastante difícil aunque no lo parezca. Debes tener en cuenta principalmente, que cada clase es un mundo totalmente diferente. No todos los heroes cumplen el mismo roll, cada uno de ellos posee ventajas y desventajas. Proponte aprenderlas para desenvolverte de la mejor manera dentro del campo. <br>
  (Más información sobre los heroes y sus rolls? <a href="<?php print site_url('/heroes');?>">Click aquí</a>)<br>
  <br>
  A pesar de dichas diferencias, todos los heroes usan un mismo sistema de control tradicional.<br>
  <br>
  - Para mover tu personaje, haz click izquierdo sobre el terreno al que quieras moverte.<br>
  <img src="/assets/009/img/pages/suchi4.jpg" width="390" height="112"><br>
  <br>
  - Para usar tus habilidades, utiliza la barra de acciones armandola a tu gusto. (Por default F1-F12)<br>
  <img src="/assets/009/img/pages/suchi5.jpg" width="506" height="44"><br>
  <br>
  - Para girar la cámara mantén presionado el click derecho del mouse, y muévelo en la dirección que prefieras.<br>
  <br>
  - Para alejar o acercar la cámara, usa la ruedita del mouse.<br>
  <br>
  **<u>Los controles se pueden personalizar a través del menú de Opciones</u>**<br>
</p>			</div>
		
	  	</div>
	</div>
</div>