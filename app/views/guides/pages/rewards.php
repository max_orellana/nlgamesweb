<div class="col-md-12 col-xs-12 content text-shadow border-radius">
	<div class="col-md-12 col-xs-12 single">
    	<div class="col-md-12 col-xs-12 no-pad light-gold main-title">Sistema de recompensa</div>
      		<div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
        		<span class="gold">Redline</span> | 28 de Diciembre del 2014
      		</div>
      		<div class="col-md-12 col-xs-12 text">
        		<div class="group-control">&nbsp;</div>
	  <div class="image-container image-shadow"><img src="www.strife.com/images/news/suchi29.png" width="689" height="140"><br>
        <p>Cuando  hablamos sobre el &ldquo;Sistema de recompensa&rdquo; en Newline, nos referimos específicamente  a la entrega de NLP (Newline Points) que hace el servidor al termino de cada  partida.<br>
          <br>
¿<u>Qué significa  NLP</u>?<br>
<br>
NLP (Newline point), es la moneda de compra principal dentro del juego. Con  ella, podrás ir adquiriendo la gran mayoría de las cosas. Como por ejemplo:  nuevos héroes, dyes y accesorios. (Esta moneda es intradeable y no es válida  para la compra de ítems-skins)<br>
<br>
¿<u>Todas las partidas otorgan la misma cantidad de puntos</u>?<br>
<br>
No. Los puntos que el sistema entrega a los jugadores al final de cada partida,  variará según dos cosas: La primera, será dependiendo de si el jugador gano o perdió  su partida. En caso de haber alcanzado una victoria, este obtendrá un bonus  adicional de puntos. Caso contrario, la ganancia de puntos será inferior. El  segundo motivo por el cual los puntos de recompensa también varían, es la duración  de cada partida, cuanto más duren, más puntos irán ganando los jugadores. De esta manera, estaremos premiando constantemente al esfuerzo de todos los  jugadores, en relación tiempo/resultados.</p>
        <div align="center">
        <!--  		@page { margin: 2cm }  		P { margin-bottom: 0.21cm }  		A:link { so-language: zxx }  	--></div>
        <p align="center"><br>
  	<br>
</p></div>
		
	  	</div>
	</div>
</div>