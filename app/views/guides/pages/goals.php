<div class="col-md-12 col-xs-12 content text-shadow border-radius">
	<div class="col-md-12 col-xs-12 single">
    	<div class="col-md-12 col-xs-12 no-pad light-gold main-title">Objetivos importantes</div>
      		<div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
        		<span class="gold">Redline</span> | 28 de Diciembre del 2014
      		</div>
      		<div class="col-md-12 col-xs-12 text">
        		<div class="group-control">&nbsp;</div>
	  <p align="center">
      <img src="/assets/009/img/pages/suchi12.jpg" width="689" height="140"></p>
<p align="center">¿<u>Que son los objetivos</u>?<br>
  Los objetivos en Newline, son los pasos importantes que da  un equipo en cada partida. Podemos asegurarle, que cumplir los objetivos en  tiempo y forma aseguraran practicamente la llegada de su victoria.<br>
  <u><br>
  Primer objetivo – Los beneficios de la jungla</u><br>
  <br>
  En principio, obtener los beneficios de la jungla parece ser un objetivo  bastante simple de lograr. Pero si lo pensamos detalladamente, nos daremos  cuenta de que esto no es tan así. <br>
  Resulta evidente que un cazador se mantendrá colaborando constantemente en las  líneas en la medida de lo posible. Y si el equipo enemigo posee una buena comunicación,  o bien el cazador rival se da cuenta de que el gank contrario no fue efectivo,  es posible que inicie un contra-gank cumpliendo diferentes objetivos, ya sea robando  la jungla contraria o bien iniciando el derribo de algún raid. Por lo que será importante  en todo momento tener el control absoluto del mapa y de todas sus criaturas  para no perder ningún tipo de ventaja.<br>
  <u><br>
  Segundo objetivo – La destrucción de las torres</u><br>
  <br>
  Destruir las torres es un objetivo realmente importante. Ya que no solo hará  que los súbditos logren avanzar con mayor facilidad sobre los carriles, sino  que también los enemigos pasaran a tener menos defensa. Por último y no menos  importante, derribar una torre otorgara a todo el equipo un bonus extra de oro.<br>
  <br>
  <u>Tercer objetivo – El asesinato a los raids</u><br>
  <br>
  Uno de los objetivos más importantes es el control de ambos radis (Dragon &amp;  Spheal). Ya que ambos otorgan importantes beneficios y oro a todo el equipo.  Algo clave para marcar la diferencia en combates grupales e individuales. <br>
  (Mas información sobre los beneficios? <a href="<?php print site_url('/mobs');?>">Click aquí</a>)<br>
  <br>
  <u>Cuarto objetivo – La destrucción de los Nexus</u><br>
  <br>
  Como ya hemos mencionado en puntos anteriores, la destrucción de un nexus podría  ser en gran parte el principio de la victoria. Destruirlos logrará ejercer una presión  inmediata sobre los carriles. Ya que una fuerza sobrenatural, empezara a  generar poderosos súbditos de elite tras cada oleada. Criaturas mucho más  fuertes y resistentes que los súbditos normales. Sin embargo, debes recordar  que esta estructura se regenerará cada 5 minutos una vez destruida, por lo que deberás  de aprovechar ese tiempo al máximo. Intentando cumplir la mayor cantidad de  objetivos como te sea posible.</p>
<p align="center"><br>
</p></div>
		
	  	</div>
	</div>
</div>
