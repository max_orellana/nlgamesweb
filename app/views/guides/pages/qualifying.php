<div class="col-md-12 col-xs-12 content text-shadow border-radius">
	<div class="col-md-12 col-xs-12 single">
    	<div class="col-md-12 col-xs-12 no-pad light-gold main-title">Clasificatorias</div>
      		<div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
        		<span class="gold">Redline</span> | 28 de Diciembre del 2014
      		</div>
      		<div class="col-md-12 col-xs-12 text">
        		<div class="group-control">&nbsp;</div>
	  <div class="image-container image-shadow">
							<a href="news/article/101.html"><img src="www.strife.com/images/news/suchi36.jpg" width="833" /></a><br>
        <p align="center">¿<u>Que son las partidas clasificatorias</u>?<br>
          <br>
Las partidas clasificatorias están hechas para aquellos jugadores  experimentados que deseen jugar en un ambiente más competitivo. Y si bien mantienen  prácticamente el mismo estilo de juego que las partidas clásicas/normales de Newline,  exceptúan algunas diferencias. Las cuales explicaremos a continuación:<br>
<br>
-En las partidas clasificatorias las clases NO pueden repetirse. Es decir: Si  un equipo decide elegir a un bishop por ejemplo, el equipo rival ya no podrá elegirlo.<br>
-El orden de selección de clases es totalmente distinto al de las partidas  normales y mantiene un orden. Intentaremos explicarlo de la manera más entendible  posible:<br>
<br>
El jugador número uno del equipo “Angeles” hace la primera elección, seleccionando  a su héroe preferido. A continuación, el primer jugador del equipo rival será quien  siga eligiendo. Luego, el turno volverá a ser del equipo “Angeles” pero esta  vez para el jugador numero dos y así sucesivamente hasta que los diez jugadores  hayan seleccionado todas sus clases.<br>
<img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
¿<u>A partir de que nivel puedo jugarlas</u>?<br>
<br>
-Un jugador podrá empezar a jugar partidas clasificatorias (individuales), si  su cuenta es nivel 35 o mayor. <br>
-Un Jugador podrá empezar a jugar partidas clasificatorias (En equipo), si  posee un team/clan. Para crear un clan, el líder del mismo debe de tener su  cuenta nivel 40 o mayor.<br>
<img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
¿<u>Cómo funciona el sistema de clasificación</u>?<br>
<br>
El  sistema de clasificación en Newline es bastante simple. A medida que uno gana o  pierde partidas, va adquiriendo o perdiendo puntos de fama y con ellos el rango  sube o baja. (Más información en el siguiente punto). También cabe aclarar, que  cuanto mayor sea el rango, la dificultad obviamente irá aumentando. Y no solo será  por el nivel de los jugadores a los que debas enfrentarte, sino también por el  simple hecho de que los puntos que obtengas por una victoria irán decayendo,  mientras que los puntos que pierdas tras una derrota serán siempre los mismos,  de comienzo a fin, sin importar el rango en el que estés. De esta manera, solo  los mejores jugadores de Newline llegaran y lograran mantenerse en la cima!<br>
<img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
¿<u>Cuales son los distintos rangos existentes</u>?<br>
<br>
0 Puntos de fama - <strong>Vagabond</strong> – Recompensa: Vagabond Circlet<br>
100 Puntos de fama - <strong>Noble</strong> – Recompensa: Noble Circlet<br>
200 Puntos de fama - <strong>Vassal</strong> – Recompensa: Vassal Circlet<br>
300 Puntos de fama - <strong>Heir</strong> – Recompensa: Heir Circlet<br>
400 Puntos de fama - <strong>Knight</strong> – Recompensa: Knight Circlet<br>
500 Puntos de fama - <strong>Elder</strong> - Recompensa: Elder Circlet<br>
600 Puntos de fama - <strong>Baron</strong> - Recompensa: Baron Circlet<br>
700 Puntos de fama - <strong>Viscount</strong> – Recompensa: Viscount Circlet<br>
800 Puntos de fama - <strong>Count</strong> - Recompensa: Count Circlet<br>
900 Puntos de fama - <strong>Marquis</strong> – Recompensa: Marquis Circlet<br>
1000 Puntos de fama -<strong>Duke</strong> – Recompensa: Duke Circlet<br>
1100 Puntos de fama - <strong>Grand Duke</strong> – Recompensa: Gran Duke Circlet<br>
1150 Puntos de fama y 3 Victorias Heroicas* - <strong>Distinguished King</strong> – Recompensa: The King’s Crown</p>
        <p align="center">        
        <div align="center">
        <!--  		@page { margin: 2cm }  		P { margin-bottom: 0.21cm }  		A:link { so-language: zxx }  	-->
        <br>
*Información sobre las Victorias Heroicas? <a href="a-real-heroe.html">Click aquí </a>
</p>
        </div>
        <p align="center"><br>
  <br>
  <br>
  <br>
</p></div>
		
	  	</div>
	</div>
</div>