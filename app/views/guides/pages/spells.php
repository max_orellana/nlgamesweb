<div class="col-md-12 col-xs-12 content text-shadow border-radius">
	<div class="col-md-12 col-xs-12 single">
    	<div class="col-md-12 col-xs-12 no-pad light-gold main-title">Spells</div>
      		<div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
        		<span class="gold">Redline</span> | 28 de Diciembre del 2014
      		</div>
      		<div class="col-md-12 col-xs-12 text">
        		<div class="group-control">&nbsp;</div>

							
	  <p align="center"><img src="www.strife.com/images/news/suchi35.png" width="689" height="140"><br>
	    
	    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
	    <HTML>
	    <!--  		@page { margin: 2cm }  		P { margin-bottom: 0.21cm }  		A:link { so-language: zxx }  	--> Muy  bien, en este apartado como bien lo menciona el titulo, hablaremos de los  spells.<br>
    <br>
Los spells son ocho habilidades <u>totalmente externas</u> a las propias de  cada clase. En cada partida, todos los personajes indiferentemente de la raza  que sean, podrán escoger dos de estas mismas para utilizarlas a su favor, en el  momento que crean más adecuado.<br>
<br>
<img src="www.strife.com/images/news/sk1.png" width="300" height="65"><br>
El target enemigo se desangra lentamente por unos segundos (DPS) y absorbe al  mismo tiempo un 40% menos de vida. *Este último desbuff, se ve reflejado en todo tipo de regeneración de HP*<br>
<img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
<img src="www.strife.com/images/news/sk2.png" width="300" height="65"><br>
Nuestro personaje y nuestros aliados cercanos a un radio de &ldquo;300&ldquo;, se ven  afectados por una curación divina de vida.<br>
<img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
<img src="www.strife.com/images/news/sk3.png" width="300" height="65"><br>
Nuestro personaje entra en un estado mental divino. A tal punto que resistirá  por unos breves segundos, cualquier tipo de desbuff recibido.<br>
<img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
<img src="www.strife.com/images/news/sk6.png" alt="" width="300" height="65"><BR>
Nuestro  personaje recibe una resistencia divina. Aumentando su resistencia física y mágica  por unos breves segundos.<BR>
        <img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
        <img src="www.strife.com/images/news/sk4.png" width="300" height="65"><BR>
        Nuestro  personaje aumenta su velocidad de movimiento, en un 25% por unos breves  segundos.<br>
        <img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
<img src="www.strife.com/images/news/sk5.png" width="300" height="65"><BR>
        Nuestro  personaje se teletransporta en sentido recto a una breve distancia.<br>
        <img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><BR>
        <img src="www.strife.com/images/news/sk7.png" width="300" height="65"><BR>
        Infringe daño verdadero a todo tipo de criaturas. *No surge efecto sobre los personajes*<br>
        <img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
<img src="www.strife.com/images/news/sk8.png" width="300" height="65"><br>
        Nuestro  personaje se ve afectado por una curación divina de mana.<BR>
	  </p>
<p align="center"><br>
</p>	</div>
		
	  	</div>
	</div>
</div>