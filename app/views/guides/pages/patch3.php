<div class="col-md-12 col-xs-12 content text-shadow border-radius">
	<div class="col-md-12 col-xs-12 single">
    	<div class="col-md-12 col-xs-12 no-pad light-gold main-title">Parche V.0.0.3 (PA)</div>
      		<div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
        		<span class="gold">Redline</span> | 28 de Diciembre del 2014
      		</div>
      		<div class="col-md-12 col-xs-12 text">
        		<div class="group-control">&nbsp;</div>
	  <p>A continuación le traemos todas las novedades sobre el parche V.0.0.3 (P.Alpha). <br>
	    Planteos, implementaciones, optimizaciones, cambios y todo lo que necesitas saber sobre este mismo. <br>
	    Ante cualquier duda o inquietud, no dudes en comunicarte con nuestros representantes via foro.<br>
	    <br>
	    <img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
	  </p>
      <div align="left"><br>
        <span style="color: #ff6600;"><u>AVANCES</u>:</span><br>
<br>
        -IA de torres (O)<br>
        <br>
        -IA de minions. (O)<br>
        <br>
        -IA de 
        criaturas neutrales (O)<br>
        <br>
        -IA de Inhibidores (O)<br>
        <br>
        -IA de minions de elite (P/I)<br>
<br>
-Cimientos sobre el sistema de evento. (O)<br>
<br>
-Sistema de combates heroicos (P)<br>
<br>
-Sistema de asesinato/muerte. (O)<br>
<br>
-Sistema de dyes (I)<br>
<br>
-Sistema de oro para soportes (P/I)<br>
<br>
-Sistema de spells (I)
<br>
<br>
-Sistema de glifos (I)<br>
<br>
-Sistema de recompensa. (O)<br>
<br>
-Precios de items (P/I)<br>
<br>
-Drop de criaturas neutrales (P/I)<br>
<br>
-Drop de minions (P/I)<br>
<br>
-Balance de skills (O)<br>
<br>
-Balance de items (O)<br>
<br>
-Balance de mobs (O)<br>
<br>
-Stats base de todas las clases. (O)<br>
<br>
-Cliente: Sonidos general del mapa clásico &quot;Inferno&quot; (I)<br>
<br>
-Cliente: Mapa clásico &quot;Inferno&quot; (Lado oscuro) (I)<br>
<br>
-Cliente:  Mapa cásico &quot;Inferno&quot; (Río - Catacumba - Cueva) (I) <br>
<br>
-Cliente:  Texturas, efectos y sonidos de Spheal (I)<br>
<br>
-Cliente:  Texturas, efectos y sonidos del Dragon (I)<br>
<br>
-Cliente: Bloqueo y limitación de ciertas funciones del juego. (O)<br>
<br>
-Cliente: Avisos de acontecimientos durante la partida. (O)<br>
<br>
-Cliente: Textura de torres (Lado oscuro) (I)<br>
<br>
-Cliente: Textura del nexus (Lado oscuro) (I)<br>
<br>
-Cliente: Textura de inhibidores (Lado oscuro) (I)<br>
<br>
-Cliente: Efectos y sonidos de torres (Lado oscuro) (I)<br>
<br>
-Cliente: Efectos y sonidos del nexus
(Lado oscuro) (I)<br>
<br>
-Cliente: Efectos
y sonidos de inhibidores (Lado oscuro) (I)<br>
<br>
-Cliente: Textura de portales (I)
<br>
<br>
-Cliente: Protección de archivos (P/I)<br>
<br>
-Cliente: Updater en modo beta (I)<br>
<br>
-Mapa en miniatura. (O)<br>
      </div><br>
      <img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
      <br>

      <p>(P) = <u>Planteado</u> - (I) = <u>Implementado</u> - (RP) = <u>Replanteado</u> - (O) = <u>Optimizado</u> - (C) = <u>Cambiado</u><br>
        <br>
        <br>
      </p>
      <span style="color: #ff6600;"><u>AVISO</u>:</span> Los  avances que usted pudo observar, están generalizados. La mayoría de los items mencionados  anteriormente, poseen un nivel de complejidad bastante alto y abarcan en sí mismo,  a una gran cantidad de puntos. Sin embargo, no nos hemos tomado el tiempo  de detallar cada uno de ellos, por el simple motivo de que este parche forma  parte del servidor en <u>estado Alpha</u> y la cantidad de avances que trae consigo son  demasiadas como para explicarlas, además de que se ven en constante  desarrollo/cambio.<br></div>
		
	  	</div>
	</div>
</div>