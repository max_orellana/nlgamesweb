<div class="col-md-12 col-xs-12 content text-shadow border-radius">
	<div class="col-md-12 col-xs-12 single">
    	<div class="col-md-12 col-xs-12 no-pad light-gold main-title">Parche V.0.0.1 (PA)</div>
      		<div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
        		<span class="gold">Redline</span> | 28 de Diciembre del 2014
      		</div>
      		<div class="col-md-12 col-xs-12 text">
        		<div class="group-control">&nbsp;</div>
	  <p>A continuación le traemos todas las novedades sobre el parche V.0.0.1 (P.Alpha). <br>
	    Planteos, implementaciones, optimizaciones, cambios y todo lo que necesitas saber sobre este mismo. <br>
	    Ante cualquier duda o inquietud, no dudes en comunicarte con nuestros representantes via foro.<br>
	    <br>
	    <img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
	  </p>
      <div align="left"><br>
        <span style="color: #ff6600;"><u>AVANCES</u>:</span><br>
<br>
        -Estructura  general del modo de juego. (P)<br>
        <br>
-Estructura del &ldquo;Shop&rdquo; (Compra/Venta). (P)<br>
<br>
-Estructura del &ldquo;Enchanter&rdquo; (P)<br>
<br>
-Dimensiones y estructura del mapa clásico: &ldquo;INFERNO&rdquo;. (P)<br>
<br>
-IA de todos los NPC. (P)<br>
<br>
-Cimientos sobre el sistema de evento. (P/I)<br>
<br>
-Sistema de experiencia. (P)<br>
<br>
-Sistema de asesinato/muerte. (P)<br>
<br>
-Sistema de farmeo. (P)<br>
<br>
-Sistema de estadísticas. (P)<br>
<br>
-Sistema de selección de clases. (P/I)<br>
<br>
-Sistema de aprendizaje de skills. (P)<br>
<br>
-Sistema de re-conexión. (P)<br>
<br>
-Sistema de registro. (P/I)<br>
<br>
-Sistema de clasificación. (P)<br>
<br>
-Sistema de recompensa. (P)<br>
<br>
-Sistema de clanes. (P)<br>
<br>
-Sistema de castigo (Karma). (P)<br>
<br>
-Ciudad principal/inicial. (P/I)<br>
<br>
-Estadísticas generales de todas las razas. (P/I)<br>
<br>
-Stats base de todas las clases. (P/I)<br>
<br>
-Instancias de evento. (I)<br>
<br>
-Selección de skills propios a cada clase. (P/I)<br>
<br>
-Selección de minions. (P)<br>
<br>
-Selección de las criaturas neutrales. (I)<br>
<br>
-Selección de items retail. (P/I)<br>
<br>
-Selección de skins customs. (P/I)<br>
<br>
-Cliente: Nuevos Splashs. (I) <br>
<br>
-Cliente: Nueva &ldquo;Intro&rdquo;. (I)<br>
<br>
-Cliente: Nuevo &ldquo;Loading&rdquo;. (I)<br>
<br>
-Cliente: Bloqueo y limitación de ciertas funciones del juego. (P)<br>
<br>
-Cliente: Avisos de acontecimientos durante la partida. (P)<br>
<br>
-Deshabilitados varios comandos/acciones &ldquo;defaults&rdquo; del juego. (P/I)<br>
<br>
-Cambios en el uso de monturas. (P/I)<br>
<br>
-Mapa en miniatura. (P)<br>
<br>
-Nivel de cuentas. (P/I)<br>
<br>
-Limite de nivel en personajes. (P/I)<br>
<br>
-Bonus/Buffs de algunas criaturas neutrales. (P)<br>
      </div><br>
      <img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
      <br>

      <p>(P) = <u>Planteado</u> - (I) = <u>Implementado</u> - (RP) = <u>Replanteado</u> - (O) = <u>Optimizado</u> - (C) = <u>Cambiado</u><br>
        <br>
        <br>
      </p>
      <span style="color: #ff6600;"><u>AVISO</u>:</span> Los  avances que usted pudo observar, están generalizados. La mayoría de los items mencionados  anteriormente, poseen un nivel de complejidad bastante alto y abarcan en sí mismo,  a una gran cantidad de puntos. Sin embargo, no nos hemos tomado el tiempo  de detallar cada uno de ellos, por el simple motivo de que este parche forma  parte del servidor en <u>estado Alpha</u> y la cantidad de avances que trae consigo son  demasiadas como para explicarlas, además de que se ven en constante  desarrollo/cambio.<br></div>
		
	  	</div>
	</div>
</div>
