<div class="col-md-12 col-xs-12 content text-shadow border-radius">
	<div class="col-md-12 col-xs-12 single">
    	<div class="col-md-12 col-xs-12 no-pad light-gold main-title">Fases de juego</div>
      		<div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
        		<span class="gold">Redline</span> | 28 de Diciembre del 2014
      		</div>
      		<div class="col-md-12 col-xs-12 text">
        		<div class="group-control">&nbsp;</div>
        <p align="center">El objetivo principal ya se ha puesto en marcha, destruir la base enemiga al costo que sea. Quien lo logre hacer primero, se hará acreedor de la victoria. Las partidas de Newline suelen durar aproximadamente 40 minutos y serán tan entretenidas que no te conformaras con solo una. <br>
          <br>
          Las clases y líneas ya habrán sido elegidas y los usuarios teletransportados a una nueva dimensión de la famosa y nueva &ldquo;Arena&rdquo;. Notaras que este campo de batalla cuenta con un mapa en miniatura estático en la parte inferior derecha de la pantalla. Este mismo te será de gran utilidad y podemos asegurarte que si terminas de entender la enorme importancia de observarlo a cada momento, lograras marcar la diferencia sobre el resto. Al mirarlo, por ejemplo, sabrás bien donde se encuentra cada aliado y eso podría ser un factor importante para terminar incluso de dar vuelta una partida. <br>
          <br>
          También podrás ver apenas aparezcas en tu base a dos NPC. El primero será un &ldquo;<u>Shop</u>&rdquo; totalmente estático. Esto quiere decir que será un NPC inamovible, este mismo se mantendrá durante toda la partida, de comienzo a fin en el mismo sitio.<br>
          <br>
	                          <br>
	                          <img src="www.strife.com/images/news/suchi.jpg" width="805" height="168"><br>
	                          <br>
	                          En él, podrás hacer toda clase de compra que desees con el oro que vayas adquiriendo con el transcurso de la partida. Armaduras, armas, joyerías, monturas y consumibles serán una de las tantas cosas que este mismo tendrá para ofrecerte. Tu ya deberás decidir desde el comienzo del juego, con tu oro inicial, que crees que será lo más indicado comprar y en qué orden para enfrentarte de la mejor manera a tus rivales. <br>
	                          <u><a href="items.html">(Más información sobre ítems - Click aquí)</a></u><br>
	                          <br>
	                          <img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
	                          El segundo NPC que podrás observar también dentro de tu base, es conocido como &ldquo;Enchanter&rdquo; y este también se mantendrá estático durante toda la partida.<br>
Aunque a diferencia del shop, la función que cumplirá este NPC será solo una: Brindarle un bonus extra a tu arma a cambio de un solo ítem, el SA Crystal.<br>
<u><a href="guide10.html">(Información sobre que es el SA Crysta - Click aquí)</a></u><br>
          <br>
          <img src="www.strife.com/images/news/suchi2.jpg" width="805" height="168"><br>
          <img src="www.strife.com/images/news/suchibar.png" alt="" width="689" height="15"><br>
          Ahora bien, pasemos a centrarnos un poco mas en el tema principal. Cada partida de Newline será &ldquo;dividida imaginariamente&rdquo; en tres etapas. Las cuales explicaremos a continuación:<br>
          <br>
          1- <u><strong>Early game (Juego temprano)</strong></u><br>
          <br>
          Resumidamente, podríamos decir que el early game trata principalmente sobre la fase de carriles. Esto quiere decir, cuando todos los heroes se encuentran peleando en su carril por derribar la primer torre y al mismo tiempo en duelos directos contra sus contrapartes. Ahora bien, si tuviéramos que explicar de manera más detallada de que trata esta primera fase, sin duda que aclararíamos los siguientes puntos:<br>
          <br>
          -El early game se rompe cuando un enemigo comienza a tomar mas responsabilidades en el juego que su posición natural. <br>
          -No es necesario que caiga una torre para que el Early game termine.<br>
          -El juego temprano de Newline es la fase donde los jugadores necesitan permanecer en sus posiciones para garantizar su impacto en las etapas tardías del juego.<br>
          <br>
          Consejos sobre lo que debes hacer en esta primera fase:<br>
          A) Farmea lo mas que puedas.<br>
          B) Muere lo menos posible.<br>
          C) Gankea siempre y cuando tengas la posibilidad y la fe de que el gank sera efectivo.<br>
          <img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
          2- <u><strong>Mid game (Juego intermedio)</strong></u><br>
          <br>
          Resulta bastante interesante hablar sobre la fase central de juego en Newline. Se podría decir que esta comienza desde el momento en que los jugadores tienen suficiente poder para visitar otros carriles sin grandes consecuencias. Me explico:<br>
          <br>
          Si un heroe de bot deja su carril al nivel 3 para ir a Mid y no consigue nada, seria bastante negativo. Ya que su adversario pasaría a tener más oro y experiencia que el y eso a niveles bajos se recentiría muchísimo, incluso hasta podría definir el porvenir del carril. Sin embargo, si un heroe nivel 9 deja su carril para ir a mid, realmente no existe situación donde no consiga nada si juega correctamente.Ya que bien podría:<br>
          A) Ayudar a derribar la torre.<br>
          B) Subir más y ayudar en el carril superior<br>
          C) Aprovechar la superioridad númerica y tomar el dragon con su equipo.<br>
          <br>
          (<u>Todos estos puntos serian un indicador de que el juego medio a comenzado</u>)<br>
          <br>
          Consejos sobre lo que debes hacer en esta segunda fase:<br>
          A) No te quedes en tu carril si no es necesario, sobre todo si no estas recibiendo ningún tipo de presión sobre la línea.<br>
          B) No estes tan lejos de tus aliados o de los objetivos, nunca sabes con exactitud cuando será el momento oportuno para ir a cumplir uno de ellos.<br>
          C) Empuja los carriles, creara presión sobre el equipo contrario, y eso seguramente los termine obligando a desplazarse rápidamente sin si quiera poder pensarlo.<br>
          <img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
        3- <u><strong>Late game (Juego tardío)</strong></u></p>
        <p align="center"><br>
          El Late game en Newline suele suceder alrededor del minuto 40 y será la fase del juego en donde los ítems estarán completados y en donde ciertos campeones brillarán más que otros. En esta etapa, en donde cualquier cosa puede ocurrir, si alguien cometiese algún error, probablemente el costo del mismo termine siendo una derrota. Los heroes estarán en su nivel máximo, por lo que si son asesinados, el tiempo de respawn será más prolongado. Entonces su equipo, se verá obligado a defender su base con un personaje menos en juego y por un tiempo bastante más largo de lo que seria en otra fase del juego.<br>
          <br>
          Por dichos motivos, es que primordialmente aconsejamos para esta fase <u>NO MORIR</u>. No sin por lo menos, llevarse consigo mismo a un enemigo.<br>
          <br>
          Uno de los consejos también que podriamos brindarte para esta fase, es que asegures con tu equipo a "Spheal". Ya que el equipo que logra asesinarlo, marcará bastante la diferencía. Tienes que pensar que probablemente si la partida se demora mucho, o bien viene demasiado pareja un bonus como el que brinda esta criatura, podría terminar siendo la clave para la hora de armar una team-fight. <br>
          <br>
  <br>
        </p>
				</div>
		
	  	</div>
	</div>
</div>