<div class="col-md-12 col-xs-12 content text-shadow border-radius">
	<div class="col-md-12 col-xs-12 single">
    	<div class="col-md-12 col-xs-12 no-pad light-gold main-title">Karma (Sistema de castigo)</div>
      		<div class="col-md-12 col-xs-12 no-pad light-gold creator">Posted by 
        		<span class="gold">Redline</span> | 28 de Diciembre del 2014
      		</div>
      		<div class="col-md-12 col-xs-12 text">
        		<div class="group-control">&nbsp;</div>
							<img src="www.strife.com/images/news/suchi31.png" width="690" height="200">
	  <p align="center">
	    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">El  sistema de karma en Newline, es conocido como el sistema de castigo hacia los  personajes que no cumplen las reglas dentro del servidor. A continuación le  explicaremos detalladamente como es que funciona y porque motivos hemos decidido  implementarlo.<br>
        <br>
La función principal de este sistema, será mantener en raya a todos los usuarios  que afecten negativamente en menor o en mayor medida, la experiencia de juego  en otros usuarios. <br>
Este es sin duda, el motivo principal del porque hemos decidido implementarlo.<br>
<br>
Sabemos bien por experiencia propia, que resulta bastante tedioso tener que jugar  cualquier tipo de partida, con gente que solo entra para molestar a sus aliados  o enemigos. Ya sea agrediendo verbalmente a otros jugadores, quedándose AFK en  medio de partidas o hasta perdiendo apropósito en algunos casos. Y si bien  todos podemos tener un mal día, actuar de esta manera jamás solucionara nada.  Por eso es que mediante este sistema, hemos decidido castigar dependiendo la  gravedad del caso, a aquellos jugadores que no actúen en consecuencia a las  reglas. (Si no conoces las reglas, haz Click aquٕí)<br>
<img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
¿<u>Cómo aumenta un personaje su karma</u>?<br>
<br>
Los mismos usuarios serán quienes decidan al finalizar cada partida, a que  jugadores reportar y bajo que motivo. Cada uno de estos reportes que pueda  llegar a recibir un usuario, hará que su karma aumente. <br>
<img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
¿<u>Cómo sabe el equipo de Newline si los jugadores reportan adecuadamente</u>?<br>
<br>
Newline posee un grupo de GMS totalmente activos, que entraran diariamente al  juego para hacer un control general de la mayor cantidad de partidas posibles. Dándole  prioridad, a aquellas partidas en donde jueguen personajes con karma ALTO. De  esta manera, sabrán quienes son los personajes que se comportan de una forma  inadecuada. Y en caso de que observen a algunos usuarios reportar a otros bajo ningún  fundamento lógico, también actuaran en consecuencia, castigándolos de la manera  que crean más adecuada. (Quedara a criterio de caga GameMaster)<br>
<img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
¿<u>Cuáles son los castigos que se le otorgan a los personajes con karma</u>?<br>
<br>
Los castigos que se le otorgan a los personajes con karma, irán variando según  el tipo y la cantidad de reportes. Pongamos unos ejemplo: <br>
<br>
-Personaje &ldquo;X&rdquo; - Karma: 100 – Cantidad de reportes 20 – Motivo de los reportes:  &ldquo;Lenguaje ofensivo&rdquo; <br>
<strong>Sanción: BAN CHAT POR 24 HORAS</strong><br>
<br>
-Personaje &ldquo;X&rdquo; – Karma 500 – Cantidad de reportes 100 – Motivo de los reportes:  &ldquo;Lenguaje ofensivo&rdquo; <br>
<strong>Sanción: BAN GENERAL CUATRO DIAS</strong><br>
<br>
-Personaje &ldquo;W&rdquo; – Karma 100 – Cantidad de reportes 10 – Motivo de los reportes: &ldquo;Abandona  el juego&rdquo; <br>
<strong>Sanción: BAN GENERAL TRES DIAS</strong><br>
<br>
-Personaje &ldquo;W&rdquo; – Karma 500 – Cantidad de reportes 50 – Motivo de los reportes: &ldquo;Abandona  el juego&rdquo; <br>
<strong>Sanción: BAN GENERAL DOS SEMANAS</strong><br>
<br>
-Personaje &ldquo;Y&rdquo; – Karma 100 – Cantidad de reportes 5 – Motivo de los reportes: &ldquo;Perder  apropósito&rdquo; <br>
<strong>Sanción: BAN GENERAL CUATRO DIAS</strong><br>
<br>
-Personaje &ldquo;Y&rdquo; – Karma 800 – Cantidad de reportes 40 – Motivo de los reportes &ldquo;Perder  apropósito&rdquo; <br>
<strong>Sanción: BAN DEFINITIVO</strong><br>
<br>
Esta claro que estos son solo unos ejemplos, no siempre funciona de la misma  manera, como hemos mencionado anteriormente, la sanción dependerá siempre de la  gravedad del caso. Si tenemos un usuario que luego de haber sido castigado,  continua con la misma actitud en reiteradas ocasiones, el BAN seguramente  termine siendo definitivo sin importar el motivo de los reportes.<br>
<img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
¿<u>El karma baja en algún momento</u>?<br>
<br>
Si. El karma de los personajes baja automáticamente mientras los mismos se  mantengan por un tiempo prolongado sin recibir reportes.<br>
<img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
¿<u>Dónde figura el karma de mi personaje</u>?<br>
<br>
El karma de tu personaje puedes observarlo abriendo la ventana de &ldquo;Character  Status&rdquo; (ALT+T)<br>
<img src="www.strife.com/images/news/suchibar.png" width="689" height="15"><br>
¿<u>Existe manera de saber el karma de otros personajes</u>?<br>
<br>
Podríamos decir que sí, pero no de manera exacta. A medida que un personaje  aumenta su karma, el color de su Nick se ve afectado por un color rojizo,  cuanto más karma obtenga el personaje, más rojo será su Nick.<BR>
	  </p>
<p align="center"><br>
</p></div>
		
	  	</div>
	</div>
</div>