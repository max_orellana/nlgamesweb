<section id="heroes">
  <div class="contents">
    <div id="skills-main" class="skills-panel">
      <div class="table">
        <div class="table-row">
          <div class="table-cell text-center">
            <div class="skills-text-wrapper"> <img id="skills-img-header" class="img-header" src="/assets/009/img/heroes/header_section_skills.png" alt="New Skills" />
              <p>Un héroe jamás pierde la esperanza de poder cambiar la historia, una vez más...</p>
            </div>
            <div id="skills-nav" class="skills-sub-nav">
                <ul>
                    <li class="warrior-icon-item">
                        <a class="skill-button">
                            <p class="skill-button-text transition">Guerreros</p>
                            <div class="warrior-icon class-icon"><span></span></div>
                        </a>
                    </li>
                    <li class="rogue-icon-item">
                        <a class="skill-button">
                            <p class="skill-button-text transition">Asesinos</p>
                            <div class="rogue-icon class-icon"><span></span></div>
                        </a>
                    </li>
                    <li class="wizard-icon-item">
                        <a class="skill-button">
                            <p class="skill-button-text transition">Magos</p>
                            <div class="wizard-icon class-icon"><span></span></div>
                        </a>
                    </li>
                    <li class="summoner-icon-item">
                        <a class="skill-button">
                            <p class="skill-button-text transition">Invocadores</p>
                            <div class="summoner-icon class-icon"><span></span></div>
                        </a>
                    </li>
                    <li class="healer-icon-item">
                        <a class="skill-button">
                            <p class="skill-button-text transition">Soportes</p>
                            <div class="healer-icon class-icon"><span></span></div>
                        </a>
                    </li>
                    <li class="archer-icon-item">
                        <a class="skill-button">
                            <p class="skill-button-text transition">Arqueros</p>
                            <div class="archer-icon class-icon"><span></span></div>
                        </a>
                    </li>
                    <li class="knight-icon-item">
                        <a class="skill-button">
                            <p class="skill-button-text transition">Tanques</p>
                            <div class="knight-icon class-icon"><span></span></div>
                        </a>
                    </li>
                </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="skills-sub" class="skills skills-panel displaced">
      <div id="sub-class-menu" class="skills-sub-nav displaced"></div>
      <div id="skills-prev-next" class="no-select">
        <div class="table prev-next" style="width:75px; position: absolute; top: 0; left: 0;">
          <div class="table-row">
            <div class="table-cell text-center"> <span class="prev skill-prev-next prev-next-link"></span> </div>
          </div>
        </div>
        <div class="table prev-next" style="width:120px; position: absolute; top: 0; right: 0;">
          <div class="table-row">
            <div class="table-cell text-center"> <span class="next skill-prev-next prev-next-link"></span> </div>
          </div>
        </div>
      </div>
      <div class="skills-classes" style="color:#fff;">
        <div id="warrior-details" class="class-skill-details">
          <div class="table">
            <div class="table-row">
              <div class="table-cell text-center">
                <div class="main-content">
                    <div class="icon-container">
                        <div id="warrior-class-icon" class="class-icon large-class-icon warrior-icon"><span></span></div>
                    </div>
                  <div class="transparent-bg-overlay"></div>
                  <img id="warrior-img-header" class="img-header" src="/assets/009/img/heroes/header_skill_warrior.png" alt="Warrior" />
                  <p class="class-description-text">Si abrazas bien el dolor, la gloria vendrá después.</p>
                  <p class="class-video"><a id="video-warrior" class="fancybox btn-youtube transition" rel="group" href="https://www.youtube.com/watch?v=EHfNokKrzrg"></a></p>
                    <div class="classes">
                        <ul>
                            <li>
                                <a id="darkavenger" class="fancybox transition" rel="group" data-fancybox-href="/heroes/darkavenger">
                                    <div class="divPrincipal">
                                        <p class="transition">Dark Avenger</p>
                                        <div class="dark-avenger-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="destroyer" class="fancybox transition" rel="group" data-fancybox-href="/heroes/destroyer">
                                    <div class="divPrincipal">
                                        <p class="transition">Destroyer</p>
                                        <div class="destroyer-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="tyrant" class="fancybox transition" rel="group" data-fancybox-href="/heroes/tyrant">
                                    <div class="divPrincipal">
                                        <p class="transition">Tyrant</p>
                                        <div class="tyrant-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="warlord" class="fancybox transition" rel="group" data-fancybox-href="/heroes/warlord">
                                    <div class="divPrincipal">
                                        <p class="transition">Warlord</p>
                                        <div class="warlord-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Warrior -->
        <div id="rogue-details" class="class-skill-details">
          <div class="table">
            <div class="table-row">
              <div class="table-cell text-center">
                <div class="main-content">
                    <div class="icon-container">
                        <div id="rogue-class-icon" class="class-icon large-class-icon rogue-icon"><span></span></div>
                    </div>
                  <div class="transparent-bg-overlay"></div>
                  <img id="rogue-img-header" class="img-header" src="/assets/009/img/heroes/header_skill_rogue.png" alt="Rogue" />
                  <p class="class-description-text">Hay un placer en la locura, que solo los locos conocen.</p>
                  <p class="class-video"><a id="video-rogue" class="fancybox btn-youtube transition" rel="group" href="https://www.youtube.com/watch?v=EHfNokKrzrg"></a></p>
                    <div class="classes">
                        <ul>
                            <li>
                                <a id="abysswalker" class="fancybox transition" target="self" rel="group" data-fancybox-href="/heroes/abysswalker">
                                    <div class="divPrincipal">
                                        <p class="transition">Abyss Walker</p>
                                        <div class="abyss-walker-icon path-icon"><span></span></div>
                                    </div>
                                </a> 
                            </li>
                            <li>
                                <a id="gladiator" class="fancybox transition" rel="group" data-fancybox-href="/heroes/gladiator">
                                    <div class="divPrincipal">
                                        <p class="transition">Gladiator</p>
                                        <div class="gladiator-icon path-icon"><span></span></div>
                                    </div>
                                </a> 
                            </li>
                            <li>
                                <a id="plainswalker" class="fancybox transition" rel="group" data-fancybox-href="/heroes/plainswalker">
                                    <div class="divPrincipal">
                                        <p class="transition">Plains Walker</p>
                                        <div class="plains-walker-icon path-icon"><span></span></div>
                                    </div>
                                </a> 
                            </li>
                            <li>
                                <a id="treasurehunter" class="fancybox transition" rel="group" data-fancybox-href="/heroes/treasurehunter">
                                    <div class="divPrincipal">
                                        <p class="transition">Treasure Hunter</p>
                                        <div class="treasure-hunter-icon path-icon"><span></span></div>
                                    </div>
                                </a> 
                            </li>
                        </ul>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Rogue -->
        <div id="wizard-details" class="class-skill-details">
          <div class="table">
            <div class="table-row">
              <div class="table-cell text-center">
                <div class="main-content">
                    <div class="icon-container">
                        <div id="wizard-class-icon" class="class-icon large-class-icon wizard-icon"><span></span></div>
                    </div>
                  <div class="transparent-bg-overlay"></div>
                  <img id="wizard-img-header" class="img-header" src="/assets/009/img/heroes/header_skill_wizard.png" alt="Wizard" />
                  <p class="class-description-text">Quien no cree en la magia, cuando la encuentra ya es tarde.</p>
                  <p class="class-video"><a id="video-wizard" class="fancybox btn-youtube transition" rel="group" href="https://www.youtube.com/watch?v=EHfNokKrzrg"></a></p>
                    <div class="classes">
                        <ul>
                            <li>
                                <a id="necromancer" class="fancybox transition" rel="group" data-fancybox-href="/heroes/necromancer">
                                    <div class="divPrincipal">
                                        <p class="transition">Necromancer</p>
                                        <div class="necromancer-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="overlord" class="fancybox transition" rel="group" data-fancybox-href="/heroes/overlord">
                                    <div class="divPrincipal">
                                        <p class="transition">Overlord</p>
                                        <div class="overlord-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="spellhowler" class="fancybox transition" rel="group" data-fancybox-href="/heroes/spellhowler">
                                    <div class="divPrincipal">
                                        <p class="transition">Spellhowler</p>
                                        <div class="spellhowler-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="sorcerer" class="fancybox transition" rel="group" data-fancybox-href="/heroes/sorcerer">
                                    <div class="divPrincipal">
                                        <p class="transition">Sorcerer</p>
                                        <div class="sorcerer-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="spellsinger" class="fancybox transition" rel="group" data-fancybox-href="/heroes/spellsinger">
                                    <div class="divPrincipal">
                                        <p class="transition">Spellsinger</p>
                                        <div class="spellsinger-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="warcry" class="fancybox transition" rel="group" data-fancybox-href="/heroes/warcry">
                                    <div class="divPrincipal">
                                        <p class="transition">Warcry</p>
                                        <div class="warcry-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Wizard -->
        <div id="summoner-details" class="class-skill-details">
          <div class="table">
            <div class="table-row">
              <div class="table-cell text-center">
                <div class="main-content">
                    <div class="icon-container">
                        <div id="summoner-class-icon" class="class-icon large-class-icon summoner-icon"><span></span></div>
                    </div>
                  <div class="transparent-bg-overlay"></div>
                  <img id="summoner-img-header" class="img-header" src="/assets/009/img/heroes/header_skill_summoner.png" alt="Summoner" />
                  <p class="class-description-text">Lo bueno de las invocaciones, es que no tienen piedad.</p>
                  <p class="class-video"><a id="video-summoner" class="fancybox btn-youtube transition" rel="group" href="https://www.youtube.com/watch?v=EHfNokKrzrg"></a></p>
                    <div class="classes">
                        <ul>
                            <li>
                                <a id="elementalsummoner" class="fancybox transition" rel="group" data-fancybox-href="/heroes/elementalsummoner">
                                    <div class="divPrincipal">
                                        <p class="transition">Elemental Summoner</p>
                                        <div class="elemental-summoner-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="phantomsummoner" class="fancybox transition" rel="group" data-fancybox-href="/heroes/phantomsummoner">
                                    <div class="divPrincipal">
                                        <p class="transition">Phantom Summoner</p>
                                        <div class="phantom-summoner-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="warlock" class="fancybox transition" rel="group" data-fancybox-href="/heroes/warlock">
                                    <div class="divPrincipal">
                                        <p class="transition">Warlock</p>
                                        <div class="warlock-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Summoner -->
        <div id="healer-details" class="class-skill-details">
          <div class="table">
            <div class="table-row">
              <div class="table-cell text-center">
                <div class="main-content">
                    <div class="icon-container">
                        <div id="healer-class-icon" class="class-icon large-class-icon healer-icon"><span></span></div>
                    </div>
                  <div class="transparent-bg-overlay"></div>
                  <img id="healer-img-header" class="img-header" src="/assets/009/img/heroes/header_skill_healer.png" alt="Healer" />
                  <p class="class-description-text">Mira al cielo cuando te encuentres perdido, allí me encontraras.</p>
                  <p class="class-video"><a id="video-healer" class="fancybox btn-youtube transition" rel="group" href="https://www.youtube.com/watch?v=EHfNokKrzrg"></a></p>
                    <div class="classes">
                        <ul>
                            <li>
                                <a id="bishop" class="fancybox transition" rel="group" data-fancybox-href="/heroes/bishop">
                                    <div class="divPrincipal">
                                        <p class="transition">Bishop</p>
                                        <div class="bishop-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="bladedancer" class="fancybox transition" rel="group" data-fancybox-href="/heroes/bladedancer">
                                    <div class="divPrincipal">
                                        <p class="transition">Blade Dancer</p>
                                        <div class="blade-dancer-icon path-icon"><span></span></div>
                                    </div>
                                </a> 
                            </li>
                            <li>
                                <a id="elvenelder" class="fancybox transition" rel="group" data-fancybox-href="/heroes/elvenelder">
                                    <div class="divPrincipal">
                                        <p class="transition">Elven Elder</p>
                                        <div class="elven-elder-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="prophet" class="fancybox transition" rel="group" data-fancybox-href="/heroes/prophet">
                                    <div class="divPrincipal">
                                        <p class="transition">Prophet</p>
                                        <div class="prophet-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="shillienelder" class="fancybox transition" rel="group" data-fancybox-href="/heroes/shillienelder">
                                    <div class="divPrincipal">
                                        <p class="transition">Shillien Elder</p>
                                        <div class="shillien-elder-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="swordsinger" class="fancybox transition" rel="group" data-fancybox-href="/heroes/swordsinger">
                                    <div class="divPrincipal">
                                        <p class="transition">Sword Singer</p>
                                        <div class="sword-singer-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Healer -->
        <div id="archer-details" class="class-skill-details">
          <div class="table">
            <div class="table-row">
              <div class="table-cell text-center">
                <div class="main-content">
                    <div class="icon-container">
                        <div id="archer-class-icon" class="class-icon large-class-icon archer-icon"><span></span></div>
                    </div>
                  <div class="transparent-bg-overlay"></div>
                  <img id="archer-img-header" class="img-header" class="fancybox" rel="group" src="/assets/009/img/heroes/header_skill_archer.png" alt="Archer" />
                  <p class="class-description-text">El costo de tu vida, es de una sola flecha.</p>
                  <p class="class-video"><a id="video-archer" class="fancybox btn-youtube transition" rel="group" href="https://www.youtube.com/watch?v=EHfNokKrzrg"></a></p>
                    <div class="classes">
                        <ul>
                            <li>
                                <a id="bountyhunter" class="fancybox transition" rel="group" data-fancybox-href="/heroes/bountyhunter">
                                    <div class="divPrincipal">
                                        <p class="transition">Bounty Hunter</p>
                                        <div class="bounty-hunter-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="hawkeye" class="fancybox transition" rel="group" data-fancybox-href="/heroes/hawkeye">
                                    <div class="divPrincipal">
                                        <p class="transition">Hawkeye</p>
                                        <div class="hawkeye-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="phantomranger" class="fancybox transition" rel="group" data-fancybox-href="/heroes/phantomranger">
                                    <div class="divPrincipal">
                                        <p class="transition">Phantom Ranger</p>
                                        <div class="phantom-ranger-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="silverranger" class="fancybox transition" rel="group" data-fancybox-href="/heroes/silverranger">
                                    <div class="divPrincipal">
                                        <p class="transition">Silver Ranger</p>
                                        <div class="silver-ranger-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Archer -->
        <div id="knight-details" class="class-skill-details">
          <div class="table">
            <div class="table-row">
              <div class="table-cell text-center">
                <div class="main-content">
                    <div class="icon-container">
                        <div id="knight-class-icon" class="class-icon large-class-icon knight-icon"><span></span></div>
                    </div>
                  <div class="transparent-bg-overlay"></div>
                  <img id="knight-img-header" class="img-header" src="/assets/009/img/heroes/header_skill_knight.png" alt="Knight" />
                  <p class="class-description-text">Resistir y avanzar, retroceder es signo de dibilidad.</p>
                  <p class="class-video"><a id="video-knight" class="fancybox btn-youtube transition" rel="group" href="https://www.youtube.com/watch?v=EHfNokKrzrgi"></a></p>
                    <div class="classes">
                        <ul>
                            <li>
                                <a id="paladin" class="fancybox transition" rel="group" data-fancybox-href="/heroes/paladin">
                                    <div class="divPrincipal">
                                        <p class="transition">Paladin</p>
                                        <div class="paladin-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="shillienknight" class="fancybox transition" rel="group" data-fancybox-href="/heroes/shillienknight">
                                    <div class="divPrincipal">
                                        <p class="transition">Shillien Knight</p>
                                        <div class="shillien-knight-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="templeknight" class="fancybox transition" rel="group" data-fancybox-href="/heroes/templeknight">
                                    <div class="divPrincipal">
                                        <p class="transition">Temple Knight</p>
                                        <div class="temple-knight-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a id="warsmith" class="fancybox transition" rel="group" data-fancybox-href="/heroes/warsmith">
                                    <div class="divPrincipal">
                                        <p class="transition">Warsmith</p>
                                        <div class="warsmith-icon path-icon"><span></span></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Knight --> 
      </div>
    </div>
  </div>
</section>
<div id="sticky-footer-position"></div>
<div id="sticky-footer">
  <div class="sns-links">
    <ul>
      <li><a href="https://www.facebook.com/www.nlgames.net?fref=ts"><i class="fa fa-lg fa-facebook"></i></a></li>
      <!--li><a href="http://twitter.com/newlinegames"><i class="fa fa-lg fa-twitter"></i></a></li-->
      <li><a href="https://www.youtube.com/channel/UC-8CpJAwKTuJaP5scajDyFw"><i class="fa fa-lg fa-youtube"></i></a></li>
    </ul>
  </div>
</div>

<script>
    $(function(){
        $('.class-video a').click(function(){
            return false;
        });
    })
</script>