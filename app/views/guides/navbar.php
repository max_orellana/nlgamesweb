
    <div class="col-md-12 col-xs-12 no-pad text-center text-shadow menu">
        <nav class="col-md-12 col-xs-12 no-pad menu-inside">
            <a href="<?php print site_url('/guides');?>"><?php print locale('home');?></a>
            <a href="<?php print site_url('/#section-3');?>">Qué es Newline</a>
            <a id="Beginners" href="javascript:void(0)">Principiantes</a>
            <a id="Apprentices" href="javascript:void(0)">Aprendices</a>
            <!--a id="Leading" href="javascript:void(0)"><li>Destacados</li></a-->
        </nav>

        <nav class="col-md-12 col-xs-12 sub-menu">
            <div class="col-md-12 col-xs-12 no-pad beginners">
                <a href="<?php print site_url('/guides/heroes');?>">Heroes</a>
                <a href="<?php print site_url('/guides/inferno');?>">Arena</a>
                <a href="<?php print site_url('/guides/items');?>">Items</a>
                <a href="<?php print site_url('/guides/moving');?>">Aprende a moverte</a>
            </div>
            <div class="col-md-12 col-xs-12 no-pad apprentices">
                <a href="<?php print site_url('/guides/phases');?>">Fases de juego</a>
                <a href="<?php print site_url('/guides/mobs');?>">NPC's</a>
                <a href="<?php print site_url('/guides/spells');?>">Spells</a>
                <!--a href="<?php print site_url('/guides/tips');?>"><li>Consejos utiles</li></a-->
                <a href="<?php print site_url('/guides/goals');?>">Objetivos importantes</a>
            </div>
            <!--div class="col-md-12 col-xs-12 no-pad leading">
                <a href="<?php print site_url('/guides/qualifying');?>"><li>Partidas clasificatorias</li></a>
                <a href="<?php print site_url('/guides/rewards');?>"><li>Sistema de recompensa</li></a>
                <a href="<?php print site_url('/guides/karma');?>"><li>Sistema de castigo</li></a>
                <a href="<?php print site_url('/guides/clans-and-alliances');?>"><li>Clanes & Alianzas</li></a>
                <a href="<?php print site_url('/guides/a-real-heroe');?>"><li>Un verdadero Heroe</li></a>
            </div-->
        </nav>
    </div>