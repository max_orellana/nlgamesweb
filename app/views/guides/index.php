
<div class="col-md-12 col-xs-12 content border-radius">
  <div class="col-md-12 col-xs-12 single">
    <section class="" id="landing">
      <div class="feature__wrapper" id="feature">
        <ul class="feature__navigation">
        </ul>
        <div class="feature__sidebar">
            <a class="button button-blue button-large button--getting-started" href="#"> <div class="btn-icon-container"><i class="fa fa-lock"></i></div><strong>Instalar Newline</strong><br />GUÍA PASO A PASO</a>
            <ul class="sidebar__sections">
                <li class="sidebar__sections--item sidebar__sections--item--heroes"><a href="/guides/heroes">Heroes</a></li>
                <li class="sidebar__sections--item sidebar__sections--item--pets"><a href="/guides/inferno">Arena</a></li>
                <li class="sidebar__sections--item sidebar__sections--item--items"><a href="/guides/items">Items</a></li>
            </ul>
        </div>
        <div class="feature__item feature__item--4-12_carousel" data-slide="4-12_carousel">
          <div class="feature__item--text">
            <h1>La historia ha cambiado!</h1>
            <p>El mundo de Adén ya es historia...</p>
          </div>
        </div>
        <div class="feature__item feature__item--reveal_carousel" data-slide="reveal_carousel">
          <div class="feature__item--text">
            <h1>Nueva Meta!</h1>
            <p>Un nuevo moba llega para este 2016!</p>
          </div>
        </div>
        <div class="feature__item feature__item--friday_feed_carousel" data-slide="friday_feed_carousel">
          <div class="feature__item--text">
            <h1>Conviértete en el mejor!</h1>
            <p>Ahora podrás destacarte más que nunca!</p>
          </div>
        </div>
      </div>

      <div class="separator"></div>

      <section class="c-section">
          <header class="c-section_header">
              <h3 id="title" class="c-section_title u-text-gradient">MANTENTE ACTUALIZADO</h3>
          </header>
      </section>


      <div class="tertiary-features">

        <div class="tertiary-features__item tertiary-features__item--recent-patch-notes">
          <h3>ULTIMOS PARCHES</h3>
          <ul class="recent-patch-notes">
            <li class="recent-patch-notes__item"> <a href="<?php print config()->forum_url;?>/index.php?/topic/69-patch-v04-a/"> <span class="recent-patch-notes__item--title">Parche 0.4</span> <span class="recent-patch-notes__item--details">Bienvenidos al parche más grande hasta el día de la fecha. Informate de todo lo nuevo.</span></a></li>
            <li class="recent-patch-notes__item"> <a href="<?php print config()->forum_url;?>/index.php?/topic/70-patch-v03-pa/"> <span class="recent-patch-notes__item--title">Parche 0.3</span> <span class="recent-patch-notes__item--details">Sin descanso. Seguimos progresando... Gran avance en texturas, precios, ambs etc.</span></a></li>
            <li class="recent-patch-notes__item"> <a href="<?php print config()->forum_url;?>/index.php?/topic/71-patch-v02-pa/"> <span class="recent-patch-notes__item--title">Parche 0.2</span> <span class="recent-patch-notes__item--details">Continuamos avanzando! Items, buffs, inteligencias, registros y mucho más.</span></a></li>
          </ul>
        </div>
        <div class="tertiary-features__item tertiary-features__item--media media--strife-trailer">
            <h3>ULTIMOS POSTS</h3>
            <ul class="news">
          <?php foreach($forum_latest as $post):?>
              <li class="community__item community__item--sticky"> <a href="<?php print config()->forum_url;?>/index.php?/topic/<?php print $post->tid;?>-<?php print $post->title_seo;?>"> <span class="community__item--title"><?php echo words($post->title,5);?></span> <span class="community__item--description"><?php print locale('published_by');?> <?php print $post->last_poster_name;?> </span> </a> </li>
          <?php endforeach;?>
            </ul>
        </div>
        <div class="tertiary-features__item tertiary-features__item--community">
          <h3>ULTIMAS NOTICIAS</h3>
          <ul class="community">
          <?php foreach($latest as $post):?>
            <li class="community__item community__item--sticky"> <a href="/blog/<?php print $post->slug;?>"> <span class="community__item--title"><?php echo words($post->{'title_' . LOCALE},5);?></span> <span class="community__item--description"><?php print locale('published_by');?> <?php print $post->login;?></span> </a> </li>
          <?php endforeach;?>
          </ul>
        </div>
      </div>
      <div class="secondary-features">
        <section class="c-section">
            <header class="c-section_header">
                <h3 id="title" class="c-section_title u-text-gradient">EPISODIO I - LA CORONA</h3>
            </header>
        </section>
        <ul>
          <li class="secondary-features__item secondary-features__item--darkstone"> 
            <a href="/story#story-1-part-1"> 
              <img src="/assets/009/img/landing/cotd.png" alt="Capitulo 1" />
              <div class="text">
              <h2>Capítulo I - El principio del cambio</h2>
              <p>Un apocalipsis en donde nadie sobrevivió.<br>El mundo de Aden ha tocado su fin. La esperanza? Es difícil encontrarla si te encuentras en el mismísimo Limbo.</p></div></a> 
          </li>
          <li class="secondary-features__item secondary-features__item--patch"> <a href="/story#story-1-part-2"> <img src="/assets/009/img/landing/patch-notes.png" alt="Capitulo 2" />
            <div class="text">
            <h2>Capítulo II - La corona</h2>
            <p>Hay quienes creen en los milagros. Hoy más que nunca. En medio de todo el caos, un humano se ha encontrado con una corona divina. Nadie sabe porque...</p></div>
            </a> </li>
          <li class="secondary-features__item secondary-features__item--trailer"> <a href="/story#story-1-part-3"> <img src="/assets/009/img/landing/trailer.png" alt="Cinematic Trailer" />
            <div class="text">
            <h2>Capítulo III - Aclaman al nuevo Heroe</h2>
            <p>Casualidad? Un acto de los dioses? Nadie<br> puede asegurarlo... Pero muchos ya aclaman al nuevo Heroe, Drago pasó a ser la esperanza de la mayoría aquí en El Limbo.
            </p></div></a></li>
        </ul>
      </div>      
    </section>

  </div>
</div>