<div class="bg-items">
    <section class="content container" id="items">

        <header class="c-section_header section-user bottom-margin">
            <h2 class="c-section_title u-text-gradient-user">Items Newline</h2>
        </header>  


        <div class="filter-bar__wrapper">
            <div class="filter-bar">
                <ul class="filter-bar__items">
                    <li class="filter-bar__item filter-bar__item--all is-active" data-filter="*">
                        <span class="filter-bar__item--title">Todos</span>
                    </li>
                    <li class="filter-bar__item" data-filter="pattack">
                        <i class="fa fa-legal"></i>
                        <span class="filter-bar__item--title">Daño físico</span>
                    </li>
                    <li class="filter-bar__item" data-filter="mattack">
                        <i class="fa fa-magic"></i>
                        <span class="filter-bar__item--title">Daño mágico</span>
                    </li>
                    <li class="filter-bar__item" data-filter="pattackspeed">
                        <i class="fa fa-bolt"></i>
                        <span class="filter-bar__item--title">Velocidad de ataque físico</span>
                    </li>
                    <li class="filter-bar__item" data-filter="mattackspeed">
                        <i class="fa fa-leanpub"></i>
                        <span class="filter-bar__item--title">Velocidad de ataque mágico</span>
                    </li>
                    <li class="filter-bar__item" data-filter="pdefense">
                        <i class="fa fa-shield"></i>
                        <span class="filter-bar__item--title">Defensa física</span>
                    </li>
                    <li class="filter-bar__item" data-filter="mdefense">
                        <i class="fa fa-get-pocket"></i>
                        <span class="filter-bar__item--title">Defensa mágica</span>
                    </li>
                    <li class="filter-bar__item" data-filter="speed">
                        <i class="fa fa-dashboard"></i>
                        <span class="filter-bar__item--title">Velocidad</span>
                    </li>
                    <li class="filter-bar__item" data-filter="health">
                        <i class="fa fa-heart"></i>
                        <span class="filter-bar__item--title">Vida</span>
                    </li>
                    <li class="filter-bar__item" data-filter="mana">
                        <i class="fa fa-tint"></i>
                        <span class="filter-bar__item--title">Mana</span>
                    </li>
                    <li class="filter-bar__item" data-filter="cp">
                        <i class="fa fa-dot-circle-o"></i>
                        <span class="filter-bar__item--title">CP</span>
                    </li>
                    <li class="filter-bar__item" data-filter="cdreduction">
                        <i class="fa fa-refresh"></i>
                        <span class="filter-bar__item--title">CReduction</span>
                    </li>
                    <li class="filter-bar__item" data-filter="regen">
                        <i class="fa fa-heartbeat"></i>
                        <span class="filter-bar__item--title">Regeneración</span>
                    </li>
                    <li class="filter-bar__item" data-filter="lifesteal">
                        <i class="fa fa-fire"></i>
                        <span class="filter-bar__item--title">Life steal</span>
                    </li>
                    <li class="filter-bar__item" data-filter="consumables">
                        <i class="fa fa-apple"></i>
                        <span class="filter-bar__item--title">Consumables</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="item-group item-group--recipes">
            <h3></h3>


            <ul class="items">
                <li class="items__item items__item--item_healingpotion is-visible regen consumables"> <span class="items__item--icon"></span> <span class="items__item--name">   Healing Potion  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Healing Potion <span class="item-details__gold item-details__gold--total-cost">         50       </span>
                            </div>
                            <div class="item-details__header--description"> <i class="fa fa-heartbeat"></i> Restaura HP por segundo</div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_manapotion is-visible regen consumables"> <span class="items__item--icon"></span> <span class="items__item--name">   Mana Potion  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Mana Potion <span class="item-details__gold item-details__gold--total-cost">         50       </span>
                            </div>
                            <div class="item-details__header--description"> <i class="fa fa-heartbeat"></i> Restaura MP por segundo</div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_cppotion is-visible regen consumables"> <span class="items__item--icon"></span> <span class="items__item--name">   CP Potion  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> CP Potion <span class="item-details__gold item-details__gold--total-cost">         80       </span>
                            </div>
                            <div class="item-details__header--description"> <i class="fa fa-heartbeat"></i> Restaura CP</div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_oraclepotion is-visible consumables"> <span class="items__item--icon"></span> <span class="items__item--name">   Oracle Potion  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Oracle Potion <span class="item-details__gold item-details__gold--total-cost">         150       </span>
                            </div>	
                            <div class="item-details__header--description"> Revela a los enemigos invisibles cercanos </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_strider is-visible consumables"> <span class="items__item--icon"></span> <span class="items__item--name">   Strider  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Strider <span class="item-details__gold item-details__gold--total-cost">         600       </span>
                            </div>	
                            <div class="item-details__header--description"> Invocas un Strider que aumenta considerablemente su velocidad de movimiento </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_soulshot is-visible consumables"> <span class="items__item--icon"></span> <span class="items__item--name">   Soulshot  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Soulshot <span class="item-details__gold item-details__gold--total-cost">         0       </span>
                            </div>	
                            <div class="item-details__header--description"> <i class="fa fa-heartbeat"></i> Activas las Soulshots para adquirir mayor precisión al atacar, mejorando la fuerza de impacto y la velocidad de ataque. </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_blessedspiritshot is-visible consumables"> <span class="items__item--icon"></span> <span class="items__item--name">   Spiritshot  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Soulshot <span class="item-details__gold item-details__gold--total-cost">         0       </span>
                            </div>	
                            <div class="item-details__header--description"> <i class="fa fa-heartbeat"></i> Activas las Spiritshots para adquirir mayor precisión al atacar, mejorando la fuerza de impacto y la velocidad de casteo. </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_arrow is-visible consumables"> <span class="items__item--icon"></span> <span class="items__item--name">   Arrow  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Arrow <span class="item-details__gold item-details__gold--total-cost">         0       </span>
                            </div>	
                            <div class="item-details__header--description"> <i class="fa fa-heartbeat"></i> Adquiere las flechas para utilizar el arco</div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_bolt is-visible consumables"> <span class="items__item--icon"></span> <span class="items__item--name">   Bolt  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Bolt <span class="item-details__gold item-details__gold--total-cost">         0       </span>
                            </div>	
                            <div class="item-details__header--description"> <i class="fa fa-heartbeat"></i> Adquiere las flechas para utilizar la ballesta</div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_plated is-visible regen pattackspeed speed lifesteal"> <span class="items__item--icon"></span> <span class="items__item--name">   Plated  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Plated Leather Armor <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         424       </span>
                            </div>
                            <div class="item-details__header--description"> <i class="fa fa-heartbeat"></i> HP +1  <i class="fa fa-bolt"></i> +16  <i class="fa fa-dashboard"></i> +7  <i class="fa fa-fire"></i> +2% </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_karmian is-visible regen mattackspeed speed cdreduction"> <span class="items__item--icon"></span> <span class="items__item--name">   Karmian  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Karmian Tunic <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         261       </span>
                            </div>
                            <div class="item-details__header--description"> <i class="fa fa-heartbeat"></i> MP +1  <i class="fa fa-leanpub"></i> +30  <i class="fa fa-dashboard"></i> +5  <i class="fa fa-refresh"></i> +2.5% </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_compound is-visible regen speed pdefense"> <span class="items__item--icon"></span> <span class="items__item--name">   Compound  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Compound Armor <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         356       </span>
                            </div>
                            <div class="item-details__header--description"> <i class="fa fa-heartbeat"></i> HP +2  <i class="fa fa-dashboard"></i> +3  <i class="fa fa-shield"></i> +75 </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_nightmarelight is-visible mdefense regen pattackspeed speed cdreduction pdefense mana health cp lifesteal"> <span class="items__item--icon"></span> <span class="items__item--name">   Nightmare  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Leather Armor of Nightmare <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         7.020       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +433  
							<i class="fa fa-heartbeat"></i> MP +0.5  
							<i class="fa fa-heartbeat"></i> HP +2.5  
							<i class="fa fa-bolt"></i> +83.5  
							<i class="fa fa-dashboard"></i>  +21.5  
							<i class="fa fa-refresh"></i> +8%  
							<i class="fa fa-shield"></i> +300.5  
							<i class="fa fa-tint"></i> +832  
							<i class="fa fa-heart"></i> +983  
							<i class="fa fa-dot-circle-o"></i> +650  
							<i class="fa fa-fire"></i> +6%  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_tallumlight is-visible mdefense regen pattackspeed speed cdreduction pdefense mana health cp lifesteal"> <span class="items__item--icon"></span> <span class="items__item--name">   Tallum  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Tallum Leather Armor <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         6.602       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +380  
							<i class="fa fa-heartbeat"></i> MP +1.5  
							<i class="fa fa-heartbeat"></i> HP +2.5  
							<i class="fa fa-bolt"></i> +83.5  
							<i class="fa fa-dashboard"></i>  +21.5  
							<i class="fa fa-refresh"></i> +10%  
							<i class="fa fa-shield"></i> +346  
							<i class="fa fa-tint"></i> +566  
							<i class="fa fa-heart"></i> +850  
							<i class="fa fa-dot-circle-o"></i> +800  
							<i class="fa fa-fire"></i> +7.5%  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_nightmarerobe is-visible mdefense regen mattackspeed speed cdreduction pdefense mana health cp"> <span class="items__item--icon"></span> <span class="items__item--name">   Nightmare  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Robe of Nightmare <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         5.677       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +333  
							<i class="fa fa-heartbeat"></i> MP +2.5  
							<i class="fa fa-heartbeat"></i> HP +2.5  
							<i class="fa fa-leanpub"></i> +115  
							<i class="fa fa-dashboard"></i>  +14.5  
							<i class="fa fa-refresh"></i> +15%  
							<i class="fa fa-shield"></i> +362  
							<i class="fa fa-tint"></i> +1112  
							<i class="fa fa-heart"></i> +800  
							<i class="fa fa-dot-circle-o"></i> +766 </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_tallumrobe is-visible mdefense regen mattackspeed speed cdreduction pdefense mana health cp"> <span class="items__item--icon"></span> <span class="items__item--name">   Tallum  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Tallum Tunic <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         5.911       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +433  
							<i class="fa fa-heartbeat"></i> MP +3.5  
							<i class="fa fa-heartbeat"></i> HP +2  
							<i class="fa fa-leanpub"></i> +93  
							<i class="fa fa-dashboard"></i>  +14.5  
							<i class="fa fa-refresh"></i> +13%  
							<i class="fa fa-shield"></i> +316  
							<i class="fa fa-tint"></i> +782  
							<i class="fa fa-heart"></i> +965  
							<i class="fa fa-dot-circle-o"></i> +600  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_nightmareheavy is-visible mdefense regen pattackspeed speed cdreduction pdefense mana health cp"> <span class="items__item--icon"></span> <span class="items__item--name">   Nightmare  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Armor of Nightmare <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         6.707       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +450  
							<i class="fa fa-heartbeat"></i> HP +4.5   
							<i class="fa fa-bolt"></i> +26.5  
							<i class="fa fa-dashboard"></i>  +10  
							<i class="fa fa-refresh"></i> +6.5%  
							<i class="fa fa-shield"></i> +382  
							<i class="fa fa-tint"></i> +616  
							<i class="fa fa-heart"></i> +1435  
							<i class="fa fa-dot-circle-o"></i> +1632  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_tallumheavy is-visible mdefense regen pattackspeed speed cdreduction pdefense mana health cp"> <span class="items__item--icon"></span> <span class="items__item--name">   Tallum  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Tallum Plate Armor <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         7.178       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +379  
							<i class="fa fa-heartbeat"></i> HP +5.5  
							<i class="fa fa-bolt"></i> +26.5  
							<i class="fa fa-dashboard"></i>  +10  
							<i class="fa fa-refresh"></i> +6.5%  
							<i class="fa fa-shield"></i> +382  
							<i class="fa fa-tint"></i> +616  
							<i class="fa fa-heart"></i> +1435  
							<i class="fa fa-dot-circle-o"></i> +1632  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_vesperleather is-visible mdefense regen pattackspeed speed cdreduction pdefense mana health cp lifesteal"> <span class="items__item--icon"></span> <span class="items__item--name">   Vesper  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Vesper Leather Breastplate <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         10.189       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +585  
							<i class="fa fa-heartbeat"></i> MP +2  
							<i class="fa fa-heartbeat"></i> HP +3.7  
							<i class="fa fa-bolt"></i> +100  
							<i class="fa fa-dashboard"></i>  +32.5  
							<i class="fa fa-refresh"></i> +15%  
							<i class="fa fa-shield"></i> +520  
							<i class="fa fa-tint"></i> +1250  
							<i class="fa fa-heart"></i> +1475  
							<i class="fa fa-dot-circle-o"></i> +975  
							<i class="fa fa-fire"></i> +11.50%
							<i class="fa fa-heartbeat"></i> CP +2.5  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_vespernobleleather is-visible mdefense regen pattackspeed speed cdreduction pdefense mana health cp lifesteal"> <span class="items__item--icon"></span> <span class="items__item--name">   Vesper Noble  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Vesper Noble Leather Breastplate <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         10.532       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +650  
							<i class="fa fa-heartbeat"></i> MP +4  
							<i class="fa fa-heartbeat"></i> HP +5  
							<i class="fa fa-bolt"></i> +125  
							<i class="fa fa-dashboard"></i>  +32.5  
							<i class="fa fa-refresh"></i> +12%  
							<i class="fa fa-shield"></i> +450  
							<i class="fa fa-tint"></i> +850  
							<i class="fa fa-heart"></i> +1275  
							<i class="fa fa-dot-circle-o"></i> +1200  
							<i class="fa fa-fire"></i> +12%
							<i class="fa fa-heartbeat"></i> CP +3.5  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_vespertunic is-visible mdefense regen mattackspeed speed cdreduction pdefense mana health cp"> <span class="items__item--icon"></span> <span class="items__item--name">   Vesper  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Vesper Tunic <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         8.990       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +650  
							<i class="fa fa-heartbeat"></i> MP +4  
							<i class="fa fa-heartbeat"></i> HP +3  
							<i class="fa fa-leanpub"></i> +140  
							<i class="fa fa-dashboard"></i>  +22  
							<i class="fa fa-refresh"></i> +20%  
							<i class="fa fa-shield"></i> +475  
							<i class="fa fa-tint"></i> +1670  
							<i class="fa fa-heart"></i> +1200  
							<i class="fa fa-dot-circle-o"></i> +1150  
							<i class="fa fa-heartbeat"></i> CP +1  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_vespernobletunic is-visible mdefense regen mattackspeed speed cdreduction pdefense mana health cp"> <span class="items__item--icon"></span> <span class="items__item--name">   Vesper Noble  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Vesper Noble Tunic <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         8.317       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +500  
							<i class="fa fa-heartbeat"></i> MP +5.5  
							<i class="fa fa-heartbeat"></i> HP +2  
							<i class="fa fa-leanpub"></i> +174  
							<i class="fa fa-dashboard"></i>  +22  
							<i class="fa fa-refresh"></i> +22.50%  
							<i class="fa fa-shield"></i> +545  
							<i class="fa fa-tint"></i> +1175  
							<i class="fa fa-heart"></i> +1400  
							<i class="fa fa-dot-circle-o"></i> +700  
							<i class="fa fa-heartbeat"></i> CP +2  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_vesperbreastplate is-visible mdefense regen pattackspeed speed cdreduction pdefense mana health cp lifesteal"> <span class="items__item--icon"></span> <span class="items__item--name">   Vesper Breastplate  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Vesper Breastplate <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         11.442       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +675  
							<i class="fa fa-heartbeat"></i> MP +1  
							<i class="fa fa-heartbeat"></i> HP +8.5  
							<i class="fa fa-bolt"></i> +40  
							<i class="fa fa-dashboard"></i>  +15  
							<i class="fa fa-refresh"></i> +10%  
							<i class="fa fa-shield"></i> +800  
							<i class="fa fa-tint"></i> +925  
							<i class="fa fa-heart"></i> +2155  
							<i class="fa fa-dot-circle-o"></i> +2450  
							<i class="fa fa-fire"></i> +12%
							<i class="fa fa-heartbeat"></i> CP +6.5  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_vespernoblebreastplate is-visible mdefense regen pattackspeed speed cdreduction pdefense mana health cp lifesteal"> <span class="items__item--icon"></span> <span class="items__item--name">   Vesper Noble Breastplate  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Vesper Noble Breastplate <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         11.895       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +844  
							<i class="fa fa-heartbeat"></i> MP +2  
							<i class="fa fa-heartbeat"></i> HP +7  
							<i class="fa fa-bolt"></i> +70  
							<i class="fa fa-dashboard"></i>  +15  
							<i class="fa fa-refresh"></i> +6%  
							<i class="fa fa-shield"></i> +575  
							<i class="fa fa-tint"></i> +590  
							<i class="fa fa-heart"></i> +2570  
							<i class="fa fa-dot-circle-o"></i> +2035  
							<i class="fa fa-fire"></i> +10%
							<i class="fa fa-heartbeat"></i> CP +8  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_stormbringer is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Stormbringer  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Stormbringer <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         942       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +200  
							<i class="fa fa-bolt"></i> +10  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_dualstormbringer is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Dual Stormbringer  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Dual Stormbringer <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         1.152       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +248  
							<i class="fa fa-bolt"></i> +12  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_eminencebow is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Eminence  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Eminence Bow <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         1.174       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +280  
							<i class="fa fa-bolt"></i> +10  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_becdecorbin is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Bec de Corbin  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Bec de Corbin <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         753       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +160  
							<i class="fa fa-bolt"></i> +8  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_crystaldagger is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Crystal  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Crystal Dagger <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         788       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +110  
							<i class="fa fa-bolt"></i> +13  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_homunkulussword is-visible mattack mattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Homunkulus  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Homunkulus Sword <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         704       </span>
                            </div>
                            <div class="item-details__header--description">  
							<i class="fa fa-magic"></i> +80    
							<i class="fa fa-leanpub"></i> +45  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_paradiastaff is-visible mattack mattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Paradia  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Paradia Staff <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         894       </span>
                            </div>
                            <div class="item-details__header--description">  
							<i class="fa fa-magic"></i> +120    
							<i class="fa fa-leanpub"></i> +35  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_yaksamace is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Yaksa  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Yaksa Mace <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         740       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +205  
							<i class="fa fa-bolt"></i> +8  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_paagriohammer is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Pa'agrio  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Pa'agrio Hammer <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         922       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +280  
							<i class="fa fa-bolt"></i> +6  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_greatpata is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Great Pata  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Great Pata <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         608       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +110  
							<i class="fa fa-bolt"></i> +16  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_swordofdamascus is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Damascus  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Damascus Sword <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         2.750       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +700  
							<i class="fa fa-bolt"></i> +40  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_damascusdamascus is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Dual Damascus  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Dual Damascus <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         3.345       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +865  
							<i class="fa fa-bolt"></i> +46.5  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_carnagebow is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Carnage  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Carnage Bow <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         3.874       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +946  
							<i class="fa fa-bolt"></i> +63  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_tallumglaive is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Tallum Glaive  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Tallum Glaive <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         2.576       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +600  
							<i class="fa fa-bolt"></i> +46.5  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_soulseparator is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Soul Separator  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Soul Separator <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         2.602       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +432  
							<i class="fa fa-bolt"></i> +74  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_swordofmiracles is-visible mattack mattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Miracles  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Sword of Miracles <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         3.042       </span>
                            </div>
                            <div class="item-details__header--description">  
							<i class="fa fa-magic"></i> +173    
							<i class="fa fa-leanpub"></i> +363  </div>
                        </div>
                    </div>
                </li>    
				<li class="items__item items__item--item_branchofthemothertree is-visible mattack mattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Mother Tree  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Branch of the Mother Tree <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         3.754       </span>
                            </div>
                            <div class="item-details__header--description">  
							<i class="fa fa-magic"></i> +515    
							<i class="fa fa-leanpub"></i> +133  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_meteorshower is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Meteor Shower  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Meteor Shower <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         2.696       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +766  
							<i class="fa fa-bolt"></i> +26.5  </div>
                        </div>
                    </div>
                </li>				
				<li class="items__item items__item--item_doomcrusher is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Doom Crusher  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Doom Crusher <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         3.062       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +932  
							<i class="fa fa-bolt"></i> +20  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_bloodtornado is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Blood Tornado  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Blood Tornado <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         3.502       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +432  
							<i class="fa fa-bolt"></i> +125  </div>
                        </div>
                    </div>
                </li>		
				<li class="items__item items__item--item_dynastysword is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Dynasty Sword  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Dynasty Sword <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         4.124       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +1050  
							<i class="fa fa-bolt"></i> +60  </div>
                        </div>
                    </div>
                </li>		
				<li class="items__item items__item--item_dynastydualsword is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Dynasty Dualsword  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Dynasty Dualsword <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         5.030       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +1300  
							<i class="fa fa-bolt"></i> +70  </div>
                        </div>
                    </div>
                </li>	
				<li class="items__item items__item--item_dynastybow is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Dynasty Bow  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Dynasty Bow <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         5.842       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +1425  
							<i class="fa fa-bolt"></i> +95  </div>
                        </div>
                    </div>
                </li>	
				<li class="items__item items__item--item_dynastyhalberd is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Dynasty Halberd  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Dynasty Halberd <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         3.870       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +900  
							<i class="fa fa-bolt"></i> +70  </div>
                        </div>
                    </div>
                </li>			
				<li class="items__item items__item--item_dynastyknife is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Dynasty Knife  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Dynasty Knife <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         3.774       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +650  
							<i class="fa fa-bolt"></i> +105  </div>
                        </div>
                    </div>
                </li>	
				<li class="items__item items__item--item_dynastymace is-visible mattack mattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Dynasty Mace  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Dynasty Mace <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         4.570       </span>
                            </div>
                            <div class="item-details__header--description">  
							<i class="fa fa-magic"></i> +545    
							<i class="fa fa-leanpub"></i> +260  </div>
                        </div>
                    </div>
                </li>			
				<li class="items__item items__item--item_dynastystaff is-visible mattack mattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Dynasty Staff  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Dynasty Staff <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         4.600       </span>
                            </div>
                            <div class="item-details__header--description">  
							<i class="fa fa-magic"></i> +775    
							<i class="fa fa-leanpub"></i> +200  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_dynastycrusher is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Dynasty Crusher  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Dynasty Crusher <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         5.650       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +1400  
							<i class="fa fa-bolt"></i> +30  </div>
                        </div>
                    </div>
                </li>	
				<li class="items__item items__item--item_dynastycudgel is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Dynasty Cudgel  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Dynasty Cudgel <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         4.054       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +1150  
							<i class="fa fa-bolt"></i> +40  </div>
                        </div>
                    </div>
                </li>	
				<li class="items__item items__item--item_dynastybaghnakh is-visible pattack pattackspeed"> <span class="items__item--icon"></span> <span class="items__item--name">   Dynasty Bagh-Nakh  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Dynasty Bagh-Nakh <span class="s-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         4.054       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-legal"></i> +650  
							<i class="fa fa-bolt"></i> +185  </div>
                        </div>
                    </div>
                </li>	
				<li class="items__item items__item--item_chainshield is-visible pdefense cp regen"> <span class="items__item--icon"></span> <span class="items__item--name">   Chain  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Chain Shield <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         71       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-shield"></i> +70  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_compoundshield is-visible pdefense cp regen"> <span class="items__item--icon"></span> <span class="items__item--name">   Compound  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Compound Shield <span class="b-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         80       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-shield"></i> +45  </div>
                        </div>
                    </div>
                </li>	
				<li class="items__item items__item--item_shieldofnightmare is-visible pdefense cp regen"> <span class="items__item--icon"></span> <span class="items__item--name">   Nightmare  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Nightmare Shield <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         355       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-shield"></i> +249  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_gloriousshield is-visible pdefense cp regen"> <span class="items__item--icon"></span> <span class="items__item--name">   Glorious  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Glorious Shield <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         400       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-shield"></i> +284  </div>
                        </div>
                    </div>
                </li>		
				<li class="items__item items__item--item_vorpalshield is-visible pdefense cp regen"> <span class="items__item--icon"></span> <span class="items__item--name">   Vorpal  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Vorpal Shield <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         710       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-shield"></i> +427  </div>
                        </div>
                    </div>
                </li>	
				<li class="items__item items__item--item_vespershield is-visible pdefense cp regen"> <span class="items__item--icon"></span> <span class="items__item--name">   Vesper  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Vesper Shield <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         1.000       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-shield"></i> +375  </div>
                        </div>
                    </div>
                </li>	
				<li class="items__item items__item--item_seraphnecklace is-visible mdefense regen cdreduction mana"> <span class="items__item--icon"></span> <span class="items__item--name">   Seraph Necklace  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Seraph Necklace <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         698       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +90  
							<i class="fa fa-heartbeat"></i> MP +1  
							<i class="fa fa-heartbeat"></i> HP +1.25
							<i class="fa fa-refresh"></i> +2.5%  
							<i class="fa fa-tint"></i> +150  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_seraphearring is-visible mdefense regen cdreduction mana"> <span class="items__item--icon"></span> <span class="items__item--name">   Seraph Earring  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Seraph Earring <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         303       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +39  
							<i class="fa fa-heartbeat"></i> MP +0.5  
							<i class="fa fa-heartbeat"></i> HP +0.5
							<i class="fa fa-refresh"></i> +1.15%  
							<i class="fa fa-tint"></i> +60  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_immortalearring is-visible mdefense regen cdreduction mana"> <span class="items__item--icon"></span> <span class="items__item--name">   Immortal Earring  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Immortal Earring <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         303       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +39  
							<i class="fa fa-heartbeat"></i> MP +0.5  
							<i class="fa fa-heartbeat"></i> HP +0.5
							<i class="fa fa-refresh"></i> +1.15%  
							<i class="fa fa-tint"></i> +60  </div>
                        </div>
                    </div>
                </li>				
				<li class="items__item items__item--item_seraphring is-visible mdefense regen mana"> <span class="items__item--icon"></span> <span class="items__item--name">   Seraph Ring  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Seraph Ring <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         177       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +26  
							<i class="fa fa-heartbeat"></i> MP +0.25  
							<i class="fa fa-heartbeat"></i> HP +0.25
							<i class="fa fa-tint"></i> +24  </div>
                        </div>
                    </div>
                </li>			
				<li class="items__item items__item--item_immortalring is-visible mdefense regen mana"> <span class="items__item--icon"></span> <span class="items__item--name">   Immortal Ring  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Immortal Ring <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         177       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +26  
							<i class="fa fa-heartbeat"></i> MP +0.25  
							<i class="fa fa-heartbeat"></i> HP +0.25
							<i class="fa fa-tint"></i> +24  </div>
                        </div>
                    </div>
                </li>	
				<li class="items__item items__item--item_valakasnecklace is-visible mdefense regen speed cdreduction mana lifesteal"> <span class="items__item--icon"></span> <span class="items__item--name">   Valakas  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Valakas Necklace <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         1.506       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +184  
							<i class="fa fa-heartbeat"></i> MP +2  
							<i class="fa fa-heartbeat"></i> HP +2.5  
							<i class="fa fa-dashboard"></i> +4  
							<i class="fa fa-refresh"></i> +3.5%  
							<i class="fa fa-tint"></i> +300  
							<i class="fa fa-fire"></i> +2.5%  </div>
                        </div>
                    </div>
                </li>
				<li class="items__item items__item--item_antharasearring is-visible mdefense regen speed cdreduction mana lifesteal"> <span class="items__item--icon"></span> <span class="items__item--name">   Antharas  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Antharas Earring <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         922       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +80.5  
							<i class="fa fa-heartbeat"></i> MP +1  
							<i class="fa fa-heartbeat"></i> HP +1  
							<i class="fa fa-dashboard"></i> +2  
							<i class="fa fa-refresh"></i> +2%  
							<i class="fa fa-tint"></i> +125  
							<i class="fa fa-fire"></i> +1.25%  </div>
                        </div>
                    </div>
                </li>		
				<li class="items__item items__item--item_zakenearring is-visible mdefense regen speed cdreduction mana lifesteal"> <span class="items__item--icon"></span> <span class="items__item--name">   Zaken  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Zaken Earring <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         922       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +80.5  
							<i class="fa fa-heartbeat"></i> MP +1  
							<i class="fa fa-heartbeat"></i> HP +1  
							<i class="fa fa-dashboard"></i> +2  
							<i class="fa fa-refresh"></i> +2%  
							<i class="fa fa-tint"></i> +125  
							<i class="fa fa-fire"></i> +1.25%  </div>
                        </div>
                    </div>
                </li>	
				<li class="items__item items__item--item_baiumring is-visible mdefense regen speed cdreduction mana lifesteal"> <span class="items__item--icon"></span> <span class="items__item--name">   Baium  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Baium Ring <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         435       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +57.5  
							<i class="fa fa-heartbeat"></i> MP +0.5  
							<i class="fa fa-heartbeat"></i> HP +0.5  
							<i class="fa fa-dashboard"></i> +0.75  
							<i class="fa fa-refresh"></i> +1.25%  
							<i class="fa fa-tint"></i> +50  
							<i class="fa fa-fire"></i> +0.5%  </div>
                        </div>
                    </div>
                </li>	
				<li class="items__item items__item--item_queenantring is-visible mdefense regen speed cdreduction mana lifesteal"> <span class="items__item--icon"></span> <span class="items__item--name">   Queen Ant  </span>
                    <div class="items__item--details item-details">
                        <div class="item-details__header">
                            <div class="item-details__header--name"> Queen Ant's Ring <span class="a-grade"></span> <span class="item-details__gold item-details__gold--total-cost">         435       </span>
                            </div>
                            <div class="item-details__header--description"> 
							<i class="fa fa-get-pocket"></i> +57.5  
							<i class="fa fa-heartbeat"></i> MP +0.5  
							<i class="fa fa-heartbeat"></i> HP +0.5  
							<i class="fa fa-dashboard"></i> +0.75  
							<i class="fa fa-refresh"></i> +1.25%  
							<i class="fa fa-tint"></i> +50  
							<i class="fa fa-fire"></i> +0.5%  </div>
                        </div>
                    </div>
                </li>					
				</ul>
                <div class="footer-text">
                    <div class="filter-bar"><p>En Newline hemos repensado los distintos grados de items para ser adecuados a la nueva jugabilidad. Quedando como grados definitivos los siguientes: "B, A, & S". Los items grado "B" son conocidos también como "Items Iniciales", con los que los jugadores comienzan cada partida. Por otro lado tenemos los items grado "A" que suelen verse mayoritariamente en el juego intermedio, y por ultimo los items TOP grado "S", quienes brindan los stats más altos del juego. El jugador posee la posibilidad de upgradear los items dentro del shop, sin la necesidad de tener que venderlos. De esta forma, el usuario se asegura de no perder Adena en dicha transacción.</p></div>
                </div>

            </div>


    </section>
</div>