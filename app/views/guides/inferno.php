
<section id="zone" class="show">
  <div class="contents">
    <div id="zone-main" class="zones">
      <div class="table">
        <div class="table-row">
          <div class="table-cell text-center">
            <div id="zone-nav">
              <div class="h2"><span>Conoce el mapa clásico de Newline</span></div>
              <div id="link-atelia" class="zone-item zone-link transition"></div>
              <div id="zones-tab-container" class="zone-item">
                <table>
                  <tbody>
                    <tr>
                      <td><span class="zone-nav-lines"></span></td>
                    </tr>
                    <tr class="zone-nav-text-row">
                      <td><div id="zone-nav-text"> <img id="zones-img-header" class="img-header" src="/assets/009/img/heroes/header_section_zones.png" alt="Zones" />
                          <p>Te invitamos a que indages nuestro mapa exclusivo</p>
                        </div></td>
                    </tr>
                    <tr>
                      <td><span class="zone-nav-lines"></span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div id="link-garden" class="zone-item zone-link transition"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="transparent-bg-overlay"></div>
      <div id="zone-main-atelia-bg" class="zone-main-bg desaturate transition"> 
        <!-- <img class="desaturate transition" src="http://www.lineage2.com/ertheia////assets/009/img/heroes/bg_05_a.jpg" alt="Lineage II: Infinite Odyssey - Zones Menu"> --> 
      </div>
      <div id="zone-main-garden-bg" class="zone-main-bg desaturate transition"> 
        <!-- <img class="desaturate transition" src="http://www.lineage2.com/ertheia////assets/009/img/heroes/bg_05_a.jpg" alt="Lineage II: Infinite Odyssey - Zones Menu"> --> 
      </div>
    </div>
    <div id="zone-sub" class="zones">
      <div class="zone-atelia zone-sub-panel">
        <!--div class="expand-atelia expand-zone"><i class="fa fa-2x fa-expand"></i></div-->
        <div class="table">
          <div class="table-row">
            <div class="table-cell text-center"> 
              <!-- header_section_zone_atelia.png --> 
              <img id="zone-atelia-img-header" class="img-header" src="/assets/009/img/heroes/header_section_zone_atelia.png" alt="Zones - Atelia" />
              <div class="zone-atelia-text zone-text">
                <p>La serenidad se pierde cuando pisas este suelo,  un fuerte escalofrió invade todo tu cuerpo, del cual nunca terminas de acostumbrarte. Galaxia se encuentra latente, jamás muere, siempre se encuentra oculto detrás los cielos, hasta que llega el momento oportuno…</p>
                <div id="to-garden" class="zone-to no-select transition"></div>
              </div>
              <div id="atelia-nav" class="zones-nav no-select"> 
                <a class="transition" style="width: 121px; height: 138px; background: url(/assets/009/img/heroes/thumb_atelia_01.png) no-repeat 0 0;"></a> 
                <a class="transition" style="width: 121px; height: 138px; background: url(/assets/009/img/heroes/thumb_atelia_02.png) no-repeat 0 0;"></a> 
                <a class="transition" style="width: 121px; height: 138px; background: url(/assets/009/img/heroes/thumb_atelia_03.png) no-repeat 0 0;"></a> 
                <a class="transition" style="width: 121px; height: 138px; background: url(/assets/009/img/heroes/thumb_atelia_04.png) no-repeat 0 0;"></a> 
                <a class="transition" style="width: 121px; height: 138px; background: url(/assets/009/img/heroes/thumb_atelia_05.png) no-repeat 0 0;"></a> 
              </div>
            </div>
          </div>
        </div>
        <div class="transparent-bg-overlay"></div>
        <div class="gallery-container">
          <div class="table prev-next">
            <div class="table-row">
              <div class="table-cell text-center">
                <div class="close-atleia-gallery close-zone-gallery"><i class="fa fa-3x fa-times"></i></div>
                <div id="atelia-prev-next" class="no-select zone-prev-next"> <span class="prev prev-next-link"></span> <span class="next prev-next-link"></span> </div>
              </div>
            </div>
          </div>
          <div id="atelia-gallery" class="zone-gallery">
            <div class="gallery" style="background-image: url(/assets/009/img/heroes/zone_gallery/atelia_gallery_01.jpg);"></div>
            <div class="gallery" style="background-image: url(/assets/009/img/heroes/zone_gallery/atelia_gallery_02.jpg);"></div>
            <div class="gallery" style="background-image: url(/assets/009/img/heroes/zone_gallery/atelia_gallery_03.jpg);"></div>
            <div class="gallery" style="background-image: url(/assets/009/img/heroes/zone_gallery/atelia_gallery_04.jpg);"></div>
            <div class="gallery" style="background-image: url(/assets/009/img/heroes/zone_gallery/atelia_gallery_05.png);"></div>
          </div>
        </div>
      </div>
      <div class="zone-garden zone-sub-panel">
        <!--div class="expand-garden expand-zone"><i class="fa fa-2x fa-expand"></i></div-->
        <div class="table">
          <div class="table-row">
            <div class="table-cell text-center"> <img id="zone-garden-img-header" class="img-header" src="/assets/009/img/heroes/header_section_zone_garden.png" alt="Zones - Garden" />
              <div class="zone-garden-text zone-text">
                <p>Hay quienes creen que la mayoría de nuestras almas, han decidido seguir luchando simplemente por sentirse como en casa...<br1> Y vaya si cuando pisas esta tierra no lo sientes. Nunca dejará de asombrarnos la similitud de este suelo con los campos de Elven Village...</p>
                <div id="to-atelia" class="zone-to no-select transition"></div>
              </div>
              <div id="garden-nav" class="zones-nav no-select"> <a class="transition" style="width: 121px; height: 138px; background: url(/assets/009/img/heroes/thumb_garden_01.png) no-repeat 0 0;"></a> <a class="transition" style="width: 121px; height: 138px; background: url(/assets/009/img/heroes/thumb_garden_02.png) no-repeat 0 0;"></a> <a class="transition" style="width: 121px; height: 138px; background: url(/assets/009/img/heroes/thumb_garden_03.png) no-repeat 0 0;"></a> <a class="transition" style="width: 121px; height: 138px; background: url(/assets/009/img/heroes/thumb_garden_04.png) no-repeat 0 0;"></a> <a class="transition" style="width: 121px; height: 138px; background: url(/assets/009/img/heroes/thumb_garden_05.png) no-repeat 0 0;"></a> </div>
            </div>
          </div>
        </div>
        <div class="transparent-bg-overlay"></div>
        <div class="gallery-container">
          <div class="table prev-next">
            <div class="table-row">
              <div class="table-cell text-center">
                <div class="close-garden-gallery close-zone-gallery"><i class="fa fa-3x fa-times"></i></div>
                <div id="garden-prev-next" class="no-select zone-prev-next"> <span class="prev prev-next-link"></span> <span class="next prev-next-link"></span> </div>
              </div>
            </div>
          </div>
          <div id="garden-gallery" class="zone-gallery">
            <div class="gallery" style="background-image: url(/assets/009/img/heroes/zone_gallery/garden_gallery_01.jpg)" alt="Garden of Spirits 01"></div>
            <div class="gallery" style="background-image: url(/assets/009/img/heroes/zone_gallery/garden_gallery_02.jpg)" alt="Garden of Spirits 02"></div>
            <div class="gallery" style="background-image: url(/assets/009/img/heroes/zone_gallery/garden_gallery_03.jpg)" alt="Garden of Spirits 03"></div>
            <div class="gallery" style="background-image: url(/assets/009/img/heroes/zone_gallery/garden_gallery_04.jpg)" alt="Garden of Spirits 04"></div>
            <div class="gallery" style="background-image: url(/assets/009/img/heroes/zone_gallery/garden_gallery_05.png)" alt="Garden of Spirits 04"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div id="sticky-footer-position"></div>