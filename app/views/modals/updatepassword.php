<h2 class="modal-title"><?php print locale('update_password');?></h2>
<form action="<?php print site_url('/updatepassword');?>" method="post" class="form form-ajax extra-padding">

	<?php echo messages();?>
	<div class="text-center">
		<div class="col-md-12">
			<div class="text-left">
				<p><?php print locale('updatepassword_msg');?></p>
			</div>
		    <div class="form-group">
		      	<input type="password" name="password" class="form-control" placeholder="<?php print locale('password');?>" required />
		    </div>	    
		</div>
		<div class="col-md-12">
		    <div class="form-group">
		      	<input type="password" name="password1" class="form-control" placeholder="<?php print locale('new_password');?>" required />
		    </div>	    
		</div>
		<div class="col-md-12">
		    <div class="form-group">
		      	<input type="password" name="password2" class="form-control" placeholder="<?php print locale('confirm_password');?>" required />
		    </div>	    
		</div>

		<div class="col-md-12">
	        <div class="group-control">&nbsp;</div>
			<div class="form-group text-center btn-dark">
	            <button type="submit" class="btn"><span><?php print locale('update_password')?></span></button>
	        </div><div class="clearfix"></div>
		</div><div class="clearfix"></div>
	</div><div class="clearfix"></div>
</form>
