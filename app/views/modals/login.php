<h2 class="modal-title"><?php print locale('login');?></h2>
<form action="<?php print site_url('/login');?>" method="post" class="form form-ajax extra-padding">
	<input type="hidden" name="redirect" value="<?php print ! empty($_GET['redirect']) ? $_GET['redirect']: site_url('/account/index');?>">
	<?php echo messages();?>
	<div class="text-center">
		<div class="col-md-12">
			<div class="text-left">
				<p><?php print locale('login_msg');?></p>
			</div>
		    <div class="form-group">
		      	<input type="text" name="email" class="form-control" placeholder="<?php print locale('username');?>" required />
		    </div>
		</div>
		<div class="col-md-12">
		    <div class="form-group">
		      	<input type="password" name="password" class="form-control" placeholder="<?php print locale('password');?>" required />
		    </div>	    
		</div>
		<div class="col-md-12">
			<div class="pull-left">
		          <input name="rememberme" id="rememberme" value="forever" type="checkbox">
		          <label for="rememberme"><span class="text-muted"><?php print locale('rememberme');?></span></label>
	        </div>
	        <div class="pull-right">
	        	 <a href="/#register" role="button" data-toggle="modal"><i class="fa fa-key"></i> <?php print locale('register');?></a>&nbsp;&nbsp;
	        	<a href="/#lostpassword" role="button" data-toggle="modal"><i class="fa fa-warning"></i> <?php print locale('login_forgot_password');?></a>
        	</div>
       		<div class="group-control">&nbsp;</div>
       		<div class="group-control">&nbsp;</div>
	        <div class="group-control">&nbsp;</div>
			<div class="form-group text-center btn-dark">
	            <input type="submit" class="btn" value="<?php print locale('login')?>">
	        </div><div class="clearfix"></div>
		</div><div class="clearfix"></div>
	</div><div class="clearfix"></div>
</form>
