<h2 class="modal-title"><?php print locale('beta_suscribe_dash_button');?></h2>
<form action="<?php print site_url('/wannajoinbeta');?>" method="post" class="form form-ajax">
	<?php echo messages();?>
	<div class="text-left">
		<p><?php print locale('wannajoinbeta_msg');?></p>
	</div>
	<div class="col-md-6">
	    <div class="form-group">
		   	<label class="control-label" for="inputError2"><?php echo locale('moba-experience');?></label>
								<select name="experience" class="form-control" required>
									<option value="" selected><?php echo locale('select_one');?></option>
									<option value="1"><?php echo locale('none');?></option>
									<option value="2"><?php echo locale('less-than-year');?></option>
									<option value="3"><?php echo locale('more-than-year');?></option>
									<option value="4"><?php echo locale('more-than-two-year');?></option>
									<option value="5"><?php echo locale('more-than-three-year');?></option>
								</select>

	    </div>
	    <div class="form-group">
		   	<label class="control-label" for="inputError2"><?php echo locale('favorite-heroe');?></label>
								<select name="heroe" class="form-control" required>
									<option value="" selected><?php echo locale('select_one');?></option>
									<option value="1">Paladin</option>
									<option value="2">Dark Avenger</option>
									<option value="3">Warlord</option>
									<option value="4">Gladiator</option>
									<option value="5">Treasure Hunter</option>
									<option value="6">Hawkeye</option>
									<option value="7">Sorcerer</option>
									<option value="8">Necromancer</option>
									<option value="9">Warlock</option>
									<option value="10">Bishop</option>
									<option value="11">Prophet</option>
									<option value="12">Temple Knight</option>
									<option value="13">Sword Singer</option>
									<option value="14">Plains Walker</option>
									<option value="15">Silver Ranger</option>
									<option value="16">Spellsinger</option>
									<option value="17">Elemental Summoner</option>
									<option value="18">Elven Elder</option>
									<option value="19">Shillien Knight</option>
									<option value="20">Bladedancer</option>
									<option value="21">Abyss Walker</option>
									<option value="22">Phantom Ranger</option>
									<option value="23">Spellhowler</option>
									<option value="24">Phantom Summoner</option>
									<option value="25">Shillien Elder</option>
									<option value="26">Destroyer</option>
									<option value="27">Tryant</option>
									<option value="28">Overlord</option>
									<option value="29">Warcry</option>
									<option value="30">Bounty Hunter</option>
									<option value="31">Warsmith</option>
								</select>
	    </div>
	</div>
	<div class="col-md-6">
	    <div class="form-group">
		   	<label class="control-label" for="inputError2"><?php echo locale('favorite-role');?></label>
								<select name="role" class="form-control" required>
									<option value="" selected><?php echo locale('select_one');?></option>
									<option value="1"><?php echo locale('role-tank');?></option>
									<option value="2"><?php echo locale('role-magician');?></option>
									<option value="3"><?php echo locale('role-hunter');?></option>
									<option value="4"><?php echo locale('role-shooter');?></option>
								</select>
	    </div>	    
	    <div class="form-group">
		   	<label class="control-label" for="inputError2"><?php echo locale('week-play-hours');?></label>
								<select name="week_play_hours" class="form-control" required>
									<option value="" selected><?php echo locale('select_one');?></option>
									<option value="1"><?php echo locale('6-or-more');?></option>
									<option value="2"><?php echo locale('14-or-more');?></option>
									<option value="3"><?php echo locale('24-or-more');?></option>
									<option value="4"><?php echo locale('36-or-more');?></option>
									<option value="5"><?php echo locale('44-or-more');?></option>
									<option value="6"><?php echo locale('56-or-more');?></option>
								</select>
	    </div>
	</div>
	<div class="col-md-12">
		<div class="form-group text-center btn-dark">
            <button type="submit" class="btn"><span><?php print locale('beta_suscribe_dash_button')?></span></button>
        </div><div class="clearfix"></div>
	</div><div class="clearfix"></div>
</form>

