<h2 class="modal-title"><?php print locale('modal_alert_no_beta_title');?></h2>
	<div class="col-md-12">
		<p><?php print locale('modal_alert_no_beta_text');?></p>
		<div class="form-group text-right btn-dark">
            <a class="btn" href="<?php print site_url('/account/index');?>"><i class="ion-ios-bolt fa-x1"></i> <span><?php print locale('continue')?></span></a>
        </div><div class="clearfix"></div>
	</div><div class="clearfix"></div>
</form>