<h2 class="modal-title"><?php print locale($title);?></h2>
<p><?php print locale($message);?></p>
<div class="group-control">&nbsp;</div>
<div class="form-group text-center btn-dark">
	<a class="btn" href="<?php print (!empty($continueto) ? locale($continueto) : 'javascript:BootstrapDialog.closeAll();');?>"><span><?php print locale('understood')?></span></a>
</div>