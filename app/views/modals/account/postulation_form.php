<h2 class="modal-title"><?php print $data['title'];?></h2>
<form class="form" action="<?php print site_url('/account/postulations/send');?>" enctype="multipart/form-data" method="post">
 	<input type="hidden" name="postulation_id" id="postulation_id" value="<?php print $data['id'];?>">
 	<p class="text-center"><?php echo locale('postulation_form_msg');?></p>
	<div class="col-md-6">
	    <div class="form-group">
	      	<input type="text" name="fullname" class="form-control" placeholder="<?php print locale('postulations_fullname');?>" required />
	    </div>
	</div>
	<div class="col-md-6">
	    <div class="form-group">
		    <input type="number" min="15" name="age" class="form-control" placeholder="<?php print locale('postulations_age');?>" required />
	    </div>
	</div>
	<div class="col-md-6">
	    <div class="form-group">
	      	<input type="text" name="residence" class="form-control" placeholder="<?php print locale('postulations_residence');?>" required />
	    </div>
	</div>
	<div class="col-md-6">
	    <div class="form-group">
		    <input type="text" name="languages" class="form-control" placeholder="<?php print locale('postulations_languages');?>" required />
	    </div>
	</div>
	<div class="col-md-6">
	    <div class="form-group">
	      	<input type="text" name="availability" class="form-control" placeholder="<?php print locale('postulations_availability');?>" required />
	    </div>
	</div>
	<div class="col-md-6">
	    <div class="form-group">
	      	<input type="text" name="skype" class="form-control" placeholder="<?php print locale('postulations_skype');?>" />
	    </div>
	</div>
<?php if($data['id']==16):?>
	<div class="col-md-6">
	    <div class="form-group">
		    <input type="text" name="medium" class="form-control" placeholder="<?php print locale('postulations_medium');?>" required />
	    </div>
	</div>
	<div class="col-md-6">
	    <div class="form-group">
	    	<select class="form-control" name="medium_reach">
	    		<option value=""><?php print locale('postulations_medium_reach');?></option>
	    		<option value="very_low"><?php print locale('very_low');?></option>
	    		<option value="low"><?php print locale('low');?></option>
	    		<option value="standard"><?php print locale('standard');?></option>
	    		<option value="high"><?php print locale('high');?></option>
	    		<option value="very_high"><?php print locale('very_high');?></option>
	    	</select>
	    </div>
	</div>
<?php endif;?>
	<div class="col-md-12">
	    <div class="form-group">
		    <textarea name="experience" class="form-control" placeholder="<?php print locale('postulations_experience');?>" required /></textarea>
	    </div>
	    <div class="form-group">
		    <textarea name="message" class="form-control" placeholder="<?php print locale('postulations_message');?>" /></textarea>
	    </div>
        <div class="form-group">
        	<div class="pull-left">
            	<input type="file" name="file" id="file" class="inputfile" value="">
            	<label class="inputlabel" for="file"><i class="fa fa-paperclip"></i> <?php print locale('upload_file');?></label>
            </div>
			<div class="pull-right btn-dark">
	            <button type="submit" class="btn btn-lg btn-success"><span><?php print locale('postulations_apply_now')?></span></button>
	        </div><div class="clearfix"></div>
        </div>
	</div><div class="clearfix"></div>
</form>

<script>
	$(function(){
		$('#file').change(
			function(){ 
				$('.inputlabel').html($('#file').val());
			}
		);
	})
</script>