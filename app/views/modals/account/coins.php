<h2 class="modal-title"><?php print locale('donate_title');?></h2>
<form class="form" action="<?php print site_url('/account/checkout');?>" method="post">
	<?php echo messages();?>
	<div class="text-center modal-coins">
		<div class="col-md-12">
			<div class="modal-section expanding-box">
				<h1 class="expanding-handle"><?php print locale('promos');?></h1>
				<div class="notvisible">
				<?php foreach($data['packs'] as $slug => $message):?>
					<div class="option-<?php print $slug;?>"><p><?php print $message;?></p></div>
				<?php endforeach;?>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<p class="text-center"><?php print locale('donation_coins_msg');?></p>
			<div class="col-md-6 row">
			    <div class="form-group donation-field pull-right">
			      	<input type="number" min="12" max="5000" name="coins" id="coins" class="form-control text-center" value="<?php print !empty($_GET['coins']) ? ($_GET['coins']-$_SESSION['coins']) : 0;?>" required />
			    </div>
		    </div>
		    <div class="col-md-6 text-left">
		    	<h1 class="no-margin"><?php print str_replace('S','<i class="fa fa-usd"></i>',session('dc_currency'));?> <span class="converted"></span></h1>
		    	<!--h6><?php print locale('coins_balance');?> <?php print $_SESSION['coins'];?></h6>
		    	<h6><?php print locale('coins_balance_to');?> <?php print !empty($_GET['coins']) ? ($_GET['coins']-$_SESSION['coins']) : 0;?></h6-->
			</div><div class="clearfix"></div>
			<p class="text-center"><?php print locale('donation_payment_method_msg');?></p>
            <div class="form-group custom-radio text-center">
            	<!--div class="pull-left">
					<label title="Paypal">
					  <input checked="checked" value="paypal" id="optionsRadios1" name="payment_method" type="radio">
                      <img src="<?php print site_asset('/img/paypal-logo.jpg');?>" width="100" >
					</label>&nbsp;&nbsp;&nbsp;&nbsp;
				</div-->
            	<div class="">
					<label class="selected" title="PayU">
					  <input value="payu" id="optionsRadios2" name="payment_method" type="radio" checked="checked">
                      <img src="<?php print site_asset('/img/logo_payu.png');?>" width="100">
					</label>
			    </div>
            </div><div class="clearfix"></div>		    
			<div class="expanding-box">
				<input type="checkbox" name="terms" id="terms" required> <a href="javascript:void(0)" class="expanding-handle terms"><span><?php print locale('donation_legal_title');?></span></a>
				<div class="notvisible">
					<textarea class="form-control donate-legal" disabled><?php print locale('donation_legal_text');?></textarea>
				</div>
			</div>
			<div class="group-control">&nbsp;</div>
			<div class="form-group btn-dark">
	            <button type="submit" class="btn"><span><?php print locale('donate')?></span></button>
	        </div><div class="clearfix"></div>
		</div><div class="clearfix"></div>
	</div>
</form>

<script>

	var dc_conversion_value = <?php print session('dc_conversion_value');?>;

	$(function(){

		$('.terms').click(function(){
			$( ".donate-legal" ).val( $( ".donate-legal" ).val().split('<br />').join("\n") );
			$(this).find('input[type=checkbox]').prop('checked', !$(this).find('input[type=checkbox]').is(':checked'));
		});
		$('#coins').on('keyup click',function(){
			$('.converted').text(parseFloat($(this).val() * dc_conversion_value).toFixed(2));
		}).trigger('keyup');

	    $('.custom-radio input').click(function(){
	        $('.custom-radio label').removeClass('selected');
	        $(this).parent().addClass('selected');
	    });
	});
</script>