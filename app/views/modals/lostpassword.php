<h2 class="modal-title"><?php print locale('lostpassword');?></h2>
<form action="<?php print site_url('/recoverpassword');?>" method="post" class="form form-ajax extra-padding">
	<?php echo messages();?>
	<div class="text-center">
		<div class="col-md-12">
			<div class="text-left">
				<p><?php print locale('lostpassword_msg');?></p>
			</div>
		    <div class="form-group">
		      	<input type="text" name="email" class="form-control" placeholder="<?php print locale('username_or_email');?>" required />
		    </div>
		</div>
		<div class="col-md-12 input-code hide">
		    <div class="form-group">
		      	<input type="text" name="code" class="form-control" placeholder="<?php print locale('input_email_code');?>" />
		    </div>	    
		</div>
		<div class="col-md-12">
	        <div class="pull-right">
	        	<a href="/#login" role="button" data-toggle="modal"><?php print locale('login');?></a>
        	</div>
       		<div class="group-control">&nbsp;</div>
       		<div class="group-control">&nbsp;</div>
	        <div class="group-control">&nbsp;</div>
			<div class="form-group text-center btn-dark">
	            <button type="submit" class="btn"><span><?php print locale('send')?></span></button>
	        </div><div class="clearfix"></div>
		</div><div class="clearfix"></div>
	</div><div class="clearfix"></div>
</form>
