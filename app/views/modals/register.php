<h2 class="modal-title"><?php print locale('register');?></h2>
<form action="<?php print site_url('/register');?>" method="post" class="form form-ajax form-register" autocomplete="off">
	<?php echo messages();?>
	<div class="text-center">
		<div class="col-md-12">
			<div class="text-left">
				<p><?php print locale('register_msg');?></p>
			</div>
		    <div class="form-group">
		      	<input type="text" name="username" class="form-control" placeholder="<?php print locale('username');?>" required />
		      	<div class="input-message" id="messages-username"></div>
		    </div>
		</div>
	    <div class="form-group col-md-6">
		    <input type="password" name="password" class="form-control" placeholder="<?php print locale('password');?>" required />
		    <div id="messages"></div>
	    </div>
	    <div class="form-group col-md-6">
		    <input type="password" name="password2" class="form-control" placeholder="<?php print locale('repeat_password');?>" data-validation-match-match="passsword" data-validation-match-message="<?php print locale('passwords_must_match');?>" required />
	    </div>
		<div class="col-md-12">
		    <div class="form-group">
		      	<input type="text" name="character" class="form-control" placeholder="<?php print locale('character');?>" required />
		      	<div class="input-message" id="messages-character"></div>
		    </div>	    
		</div>
	    <div class="form-group col-md-6">
		    <input type="email" name="email" class="form-control" placeholder="<?php print locale('email');?>" data-validation-email-message="Ingresa un correo electrónico válido" required  />
		    <div class="input-message" id="messages-email"></div>
	    </div>
	    <div class="form-group col-md-6">
			<select name="gender" class="form-control" title="<?php print locale('gender');?>">
				<option value="1"><?php echo locale('female');?></option>
				<option value="0"><?php echo locale('male');?></option>
			</select>
	    </div>
		<div class="col-md-12">
			<div class="text-left">
		        <fieldset>
		          <input id="terms" name="terms" value="1" class="" type="checkbox" required>
		          <label class="" for="terms" id="termsLabel"><?php echo locale('agree');?> <!--a href="#tos" role="button" data-toggle="modal"--> <?php echo locale('tos_and_privacy');?> <!--/a--> </label>
		        </fieldset>	        
		        <fieldset>
		          <input id="wannajoinbeta" name="wannajoinbeta" value="1" class="" type="checkbox">
		          <label for="wannajoinbeta"><?php echo locale('wanna_join_beta');?> </label>
		        </fieldset>	    
	        </div>
	        <div class="group-control">&nbsp;</div>
       		<div id="recaptcha" class="recaptcha-center"></div>
       		<div class="group-control">&nbsp;</div>
			<div class="form-group btn-dark">
	            <button type="submit" class="btn-register btn"><span><?php print locale('register')?></span></button>
	        </div><div class="clearfix"></div>
		</div><div class="clearfix"></div>
	</div>
</form>

    <script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('recaptcha', {
          'sitekey' : '<?php print config()->gcaptchasitekey;?>',
          'theme' : 'dark'
        });
      };
    </script>

    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>

