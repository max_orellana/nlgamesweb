<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">
			<?php print session('login');?>
		</h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-4 col-md-6">
    <h3><?php print locale('account_info');?></h3>
    <table class="table">
        <tr>
            <td><strong><?php print locale('ip_current');?></strong></td>
            <td><?php print session('pcip');?></td>
        </tr>
        <tr>
            <td><strong><?php print locale('ip_last_entrance');?></strong></td>
            <td><?php print session('lastip');?></td>
        </tr>
        <tr>
            <td><strong><?php print locale('last_entrance');?></strong></td>
            <td><?php print session('lastserver');?></td>
        </tr>
    </table>
    </div>
</div>