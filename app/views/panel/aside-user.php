<aside class="user">
	<div class="nav">
		<nav class="menu">
			<?php if(session('group') == 'admin'):?>

				<div class="text-center">
					<span class="badge sidebar admin">
						<a href="javascript:void(0)" id="admin-menu-btn"><i class="fa fa-chevron-down"></i> <?php print session('group');?></a>
					</span>
				</div>

				<ul class="parent-menu <?php print session('group');?>" id="admin-menu">

				<li class="<?php print segments(2) == '' && segments(1) == 'admin' ? 'active':'';?>">
					<a href="<?php print site_url('/admin/dash');?>" title=""><i class="fa fa-desktop"></i><span><?php print locale('admin_panel');?></a>
				</li>
				<li class="<?php print segments(2) == 'posts' ? 'active':'';?>">
					<a href="<?php print site_url('/admin/posts');?>" title=""><i class="fa fa-anchor"></i><span><?php print locale('posts');?></a>
					<!--ul>
						<li><a href="<?php print site_url('/admin/posts/create');?>"><?php print locale('create');?></a></li>
					</ul-->
				</li>
				<!--li class="<?php print segments(2) == 'items' ? 'active':'';?>">
					<a href="<?php print site_url('/admin/items');?>" title=""><i class="fa fa-anchor"></i><span><?php print locale('items');?></a>
					<ul>
						<li><a href="<?php print site_url('/admin/items/create');?>"><?php print locale('create');?></a></li>
						<li><a href="<?php print site_url('/admin/items-types');?>"><?php print locale('types');?></a></li>
						<li><a href="<?php print site_url('/admin/items-grades');?>"><?php print locale('grades');?></a></li>
						<li><a href="<?php print site_url('/admin/items-classes');?>"><?php print locale('classes');?></a></li>
					</ul>
				</li-->
				<li class="<?php print segments(2) == 'postulations' ? 'active':'';?>">
					<a href="<?php print site_url('/admin/postulations');?>" title=""><i class="fa fa-users"></i><span><?php print locale('postulations');?></span></a>
					<ul>
						<li><a href="<?php print site_url('/admin/postulations-relation');?>"><?php print locale('list');?></a></li>
					</ul>
				</li>
				<!--li>
					<a href="<?php print site_url('/admin/tickets');?>" title=""><i class="fa fa-support"></i><span><?php print locale('tickets');?></span></a>
				</li-->
				<!--li class="<?php print segments(2) == 'coupons' ? 'active':'';?>">
					<a href="<?php print site_url('/admin/coupons');?>" title=""><i class="fa fa-credit-card"></i><span><?php print locale('access');?></span></a>
					<ul>
						<li><a href="<?php print site_url('/admin/coupons-types');?>"><?php print locale('types');?></a></li>
					</ul>
				</li-->
				<li class="<?php print segments(2) == 'sales' ? 'active':'';?>">
					<a href="<?php print site_url('/admin/orders');?>" title=""><i class="fa fa-shopping-cart"></i><span><?php print locale('sales');?></span></a>
					<ul>
						<li><a href="<?php print site_url('/admin/orders');?>"><?php print locale('orders');?></a></li>
						<li><a href="<?php print site_url('/admin/payments');?>"><?php print locale('payments');?></a></li>
						<li><a href="<?php print site_url('/admin/packs');?>"><?php print locale('packs');?></a></li>
						<li><a href="<?php print site_url('/admin/advertisers');?>"><?php print locale('advertisers');?></a></li>
					</ul>
				</li>
				<!--li>
					<a href="<?php print site_url('/admin/tasks');?>" title=""><i class="fa fa-tasks"></i><span><?php print locale('tasks');?></span></a>
					<ul>
						<li><a href="<?php print site_url('/admin/tasks/create');?>"><?php print locale('create');?></a></li>
					</ul>
				</li-->
				<li>
					<a href="<?php print site_url('/admin/accounts');?>" title=""><i class="fa fa-users"></i><span><?php print locale('accounts');?></span></a>
					<!--ul>
						<li><a href="<?php print site_url('/admin/users/online');?>"><?php print locale('online');?></a></li>
					</ul-->
				</li>				
				<!--li>
					<a href="<?php print site_url('/admin/tags');?>" title=""><i class="fa fa-tag"></i><span><?php print locale('tags');?></span></a>
				</li-->
				<li>
					<a href="<?php print site_url('/admin/heroes');?>" title=""><i class="fa fa-child"></i><span><?php print locale('heroes');?></span></a>
					<!--ul>
						<li><a href="<?php print site_url('/admin/heroes/create');?>"><?php print locale('create');?></a></li>
					</ul-->
				</li>				
				<li>
					<a href="<?php print site_url('/admin/words');?>" title=""><i class="fa fa-shield"></i><span><?php print locale('words');?></span></a>
					<ul>
						<li><a href="<?php print site_url('/admin/words/create');?>"><?php print locale('create');?></a></li>
					</ul>
				</li>
			</ul>

			<?php elseif(session('group') == 'advertiser'):?>
				<div class="text-center">
					<span class="badge sidebar advertiser">
						<a href="javascript:void(0)" id="admin-menu-btn"><i class="fa fa-chevron-down"></i> <?php print session('group');?></a>
					</span>
				</div>

				<ul class="parent-menu <?php print session('group');?>" id="admin-menu">

				<li class="<?php print segments(2) == '' && segments(1) == 'advertiser' ? 'active':'';?>">
					<a href="<?php print site_url('/advertiser/index');?>" title=""><i class="fa fa-desktop"></i><span><?php print locale('advertiser_panel');?></a>
				</li>
			</ul>
			<?php endif;?>				

			<ul class="parent-menu">
				<li class="<?php print segments(2) == 'index' ? 'active':'';?>">
					<a href="<?php print site_url('/account/index');?>" title=""><i class="fa fa-desktop"></i><span><?php print locale('dashboard');?></a>
				</li>
				<li class="high <?php print segments(2) == 'packages' ? 'active':'';?>">
					<a href="<?php print site_url('/joinbeta');?>" target="_blank"><i class="fa fa-gamepad"></i><span><?php print locale('join_beta');?></a>
				</li>
				<li class="<?php print segments(2) == 'profile' ? 'active':'';?>">
					<a href="<?php print site_url('/account/profile');?>" title=""><i class="fa fa-user"></i><span><?php print locale('my_account');?></a>
				</li>
				<!--li class="<?php print segments(2) == 'items' ? 'active':'';?>">
					<a href="<?php print site_url('/account/items');?>" title=""><i class="fa fa-wand"></i> <span><?php print locale('items');?></a>
				</li-->
				<li class="<?php print segments(2) == 'forum' ? 'active':'';?>">
					<a href="<?php print site_url('/account/forum');?>" title=""><i class="fa fa-<?php print empty(session('forum_name'))?'plug':'rss';?>"></i><span><?php print locale((empty(session('forum_name')) ? 'connect_forum' : 'forum'));?></a>
				</li>
				<li class="<?php print segments(2) == 'postulations' ? 'active':'';?>">
					<a href="<?php print site_url('/account/postulations');?>" title=""><i class="fa fa-users"></i><span><?php print locale('postulations');?></a>
				</li>
				<!--li class="<?php print segments(2) == 'tickets' ? 'active':'';?>">
					<a href="<?php print site_url('/account/tickets');?>" title=""><i class="fa fa-pricetags"></i><span><?php print locale('support');?></a>
				</li-->
			</ul>
		</nav>
	</div>
</aside>