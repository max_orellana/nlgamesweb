
<div class="pull-right">
    <?php if(!empty(session('advertise_token'))):?>
        <!-- advertise token : <?php print session('advertise_token');?> -->
    <?php endif;?>
	<div class="custom-dropdowns">
		<div class="notification-list dropdown hide">
			<a title=""><div class="warning-icon notransition"></div></a>
			<div class="notification drop-list"></div>
		</div><!-- Notification List -->
	</div>
<?php if(session('group') == 'admin'):?>
	<!--a title="" class="slide-panel-btn"><i class="ion-gear-b"></i></a-->
<?php endif;?>
<?php if(session('login')):?>            
    <div class="riot profile btn-group">
        <a title="" class="dropdown-toggle" data-toggle="dropdown">
            <?php if(!empty(session('forum_avatar'))):?>
                <img class="summoner-icon" src="<?php print avatar(session('login'),session('forum_avatar'));?>">
            <?php else:?>
                <img class="summoner-icon" src="<?php print site_asset('img/profile_default.png');?>">
            <?php endif;?>	
            <span class="summoner-name"><?php print session('login');?></span>
            <div id="account-dropdown-trigger"><span></span></div>
        </a>
        <ul class="riot-dropdown dropdown-menu">
            <li><a class="high" href="<?php print site_url('/joinbeta');?>" title=""><i class="fa fa-gamepad"></i> <?php print locale('join_beta');?></a></li>
            <li><a href="<?php print site_url('/account/index');?>" title=""><i class="fa fa-desktop"></i> <?php print locale('dashboard');?></a></li>
            <li><a href="<?php print site_url('/account/profile');?>" title=""><i class="fa fa-user"></i> <?php print locale('my_account');?></a></li>
            <li><a href="<?php print site_url('/account/inbox');?>" title=""><i class="fa fa-envelope"></i> <?php print locale('inbox');?><span class="inbox-unread"></span></a></li>
            <li><a href="<?php print site_url('/logout');?>" title=""><i class="fa fa-power-off"></i> <?php print locale('logout');?></a></li>
        </ul>
    </div>
</div>
<?php endif;?>
<div class="pull-right"> 
<?php if( ! session('login')):?>
    <div class="user-actions btn-custom">
        <a class="btn btn-danger" href="#register" role="button" data-toggle="modal" data-shown="modal_register_open">
            <span><?php print locale('register');?></span>
        </a>
        <a class="btn btn-info" href="#login" role="button" data-toggle="modal">
            <i class="ion-person"></i>
            <span><?php print locale('login');?></span>
        </a>
    </div>

<?php endif;?>

    <!--div class="lang-selector-box">
        <a href="#" class="lang-button"><?php print locale('language');?>: <?php print locale(LOCALE);?> &nbsp; <i class="ion-android-arrow-dropdown"></i></a> </span>
        <ul class="lang-selector hide">
            <?php foreach(config()->languages as $lang):?>
                <li><a href="?lang=<?php echo $lang;?>"> <?php print locale($lang);?> <img src="<?php print site_asset('fl/' . $lang . '.png');?>"> <?php echo $lang == LOCALE ? '<i class="ion-android-arrow-dropleft"></i>' : '<i class="ion-android-arrow-dropleft" style="visibility:hidden"></i>';?></a></li>
            <?php endforeach;?>
        </ul>
    </div-->
</div>