<div class="ps-wrapper panel-container content-height">
	<div class="bg-container bg-user darkfade">
	    <header class="c-section_header section-user bottom-margin">
	        <h2 class="c-section_title u-text-gradient-user"><?php print locale('inbox');?></h2>
	    </header>

		<div class="bd-box btn-custom ps-wrapper">
		<?php echo messages();?>			
		<?php if(session('group') == 'admin'):?>
			<a href="<?php print site_url('/account/inbox/compose');?>" class="btn btn-success"><span><?php print locale('compose');?></span></a>
			<a href="<?php print site_url('/account/inbox/sent');?>" class="btn btn-success"><span><?php print locale('sent');?></span></a>
			<div class="group-control">&nbsp;</div>
		<?php else:?>		
			<div class="bd-caption text-center">
				<p><?php print locale('inbox_msg');?></p>
			</div>
		<?php endif;?>

		<?php if(count($entries)):?>
			<div class="inbox-columns">
				<div class="col-md-4 fixed-height">
					<div class="list-container ps-wrapper">
						<ul class="list-inbox opacity-box">
						<?php foreach($entries as $entry):?>
							<li class="<?php print $entry->read_time ? 'success' : '';?>">	
								<form class="form-ajax pull-right" action="<?php print site_url('/account/inbox/delete/' . $entry->id);?>" method="post">
										<button type="button" class="btn-discrete deleteboxelement"><i class="ion-trash-a"></i></button>
								</form>							
								<a href="javascript:void(0)" class="viewinboxelement" data-id="<?php print $entry->id;?>">
									<div>
										<div class="pull-left text-left" style="width:40px;">
											<img class="summoner-icon" src="<?php print empty($entry->forum->forum_avatar) ? site_asset('img/profile_default.png')  : $entry->forum->forum_avatar;?>">
										</div>
										<div class="inbox-list-title pull-left">
											<?php print $entry->inbox->title;?>
										</div>
									</div>
									<div>
										<div class="pull-left">
											<strong><?php print session('group') == 'user' ? ( $entry->login == 'system' ? locale('system_title') : locale('golden_moderator') . ' ' . $entry->login) : locale('golden_moderator')  . ' ' . $entry->login;?></strong>
										</div>
										<div class="pull-right">
											<?php echo timespan($entry->sent_time);?>	
										</div>
									</div>
								</a>
							</li>
						<?php endforeach;?>
						</ul>
					</div>
				</div>
				<div class="col-md-8 fixed-height row">
					<div class="entry-container ps-wrapper">
						<ul class="entry-inbox opacity-box"></ul>
					</div>
				</div>
			<?php else:?>
				<div class="opacity-box fa-message container-fluid text-center"><i class="fa fa-envelope"></i><p><?php print locale('inbox_empty');?></p></div>
			<?php endif;?>
			</div>
		</div><div class="clearfix"></div>
	</div>
</div>

<script>

	$(function(){

		$('.deleteboxelement').click(function(){
			$this = $(this);
			BootstrapDialog.confirm({
				type:'type-warning',
				message: '<?php print locale('inbox_delete_confirm');?>', 
				callback: function(result){
		            if(result) {
		            	$this.parents('form').submit();
		            }
		        }
	        });
		});

		forms.accountinboxdelete = function(json,form){
			if(json.code == 'success'){
				setTimeout(function(){
					$(form).parents('li').slideUp(100,function(){$(this).remove();
						if( ! $('.viewinboxelement').length){
							$('.inbox-columns').html('<div class="opacity-box fa-message container-fluid text-center"><i class="fa fa-envelope"></i><p><?php print locale('inbox_empty');?></p></div>');
						} else {
							$('.viewinboxelement').first().click();	
						}
					});
				},500);
			}
		}

		$('.viewinboxelement').click(function(e){

			e.preventDefault();

			$('.list-inbox li').removeClass('active');
			$('body').addClass('loading');
			var $this = $(this);

			$('.entry-container').fadeTo('fast',0.1,function(){

				$('.entry-container').addClass('rainbow-loader');
				$this.parent().addClass('success active');

			    $.ajax({
			      type: 'get',
			      url: '/account/inbox/fetch/' +$this.data('id'),
			      success:function(json){
			      	$('.entry-inbox').html('');
			      	$('.entry-inbox').append('<li><h1>' + json.title + '</h1></li>');
			      	//$('.entry-inbox').append('<li><strong>' + json.sent + '</strong></li>');
			      	$('.entry-inbox').append('<li><p>' + json.content + '</p></li>');
			      	$('.entry-container').removeClass('rainbow-loader');

			      	$('.entry-container').fadeTo(100,1, function(){
			      		 $('.ps-wrapper').perfectScrollbar('update');
			      	});
			      	$('body').removeClass('loading');

			      }
			    });
			});
		});	
		
		setTimeout(function(){
			$('.viewinboxelement').first().click();			
		},100);
	});

</script>