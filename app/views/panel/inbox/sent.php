<div class="ps-wrapper panel-container">
	<div class="bg-container bg-dash darkfade">
	    <header class="c-section_header color2 bottom-margin">
	        <h2 class="c-section_title u-text-gradient-<?php print session('group');?>"><?php print locale('inbox');?></h2>
	    </header>

		<div class="bd-box btn-custom">
			<?php echo messages();?>
			<a href="<?php print site_url('/account/inbox/compose');?>" class="btn btn-success"><span> <?php print locale('compose');?></span></a>
			<a href="<?php print site_url('/account/inbox');?>" class="btn btn-success"><span> <?php print locale('inbox');?></span></a>
			<div class="group-control">&nbsp;</div>
			<?php if(count($entries)):?>
				<table class="table table-blue">
					<thead>
						<tr>
							<th><i class="ion-eye"></i></th>
							<th><i class="ion-clock"></i></th>
							<th><i class="ion-user"></i></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($entries as $entry):?>
						<tr class="<?php print $entry->read_time ? 'success' : '';?>">	
							<td><?php print $entry->recipient;?></td>						
							<td><a href="<?php print site_url('/inbox/' . $entry->id);?>"><?php print $entry->inbox->title;?></a></td>
							<td><?php echo timespan($entry->sent_time);?></td>
						</tr>
					<?php endforeach;?>
					</tbody>
				</table>
			</div>
			<?php $entries[0]->paginator();?>
			<?php endif;?>
		</div><div class="clearfix"></div>
	</div>
</div>