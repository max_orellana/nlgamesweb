<div class="ps-wrapper panel-container">
	<div class="panel">
		<div class="col-md-12">
			<form class="form" action="<?php print site_url('/account/inbox/send');?>" method="post" autocomplete="off">
				<div class="form-group" style="position:relative">
					<input type="text" class="form-control" name="recipients_input" placeholder="<?php print locale('recipients');?>" data-minlength="3" value="">
					<input type="hidden" name="recipients" id="recipients" value="<?php print count($recipients) ? implode(',',$recipients) : '';?>">
					<div class="recipients-drop drop-list"></div>
					<div class="recipients-list alert alert-success <?php print count($recipients) ? '' : 'hide';?>">
				<?php if( count($recipients)):?>
						<?php foreach($recipients as $recipient):?>
							<li><i class="ti-user"></i> <?php print $recipient;?>
						<?php endforeach;?>
				<?php endif;?>
					</div>
				</div>

				<div class="form-group">
					<input type="text" class="form-control" name="title" placeholder="<?php print locale('title');?>" value="" required>
				</div>

                <div class="progress summernote-progress hide">
                  <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                    <span class="sr-only"></span>
                  </div>
                </div>
                                        
				<div class="form-group">
					<textarea class="form-control editor" name="content" id="content" placeholder="<?php print locale('content');?>"></textarea>
				</div>

				<div class="form-group text-center btn-custom">
					<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('send');?></span> </button>
				</div>
			</form>
		</div><div class="clearfix"></div>
	</div>
</div>

<script type="text/javascript">

	var recipients = [];

	$(document).on('click','.remove-recipient',function(){
		recipients.splice(recipients.indexOf($(this).parent().find('span').text()), 1);
		$(this).parent().remove();
	});

	$(document).on('click','.recipient',function(){
		var recipient = $(this).attr('name');
		recipients.push(recipient);

		$('#recipients').val(recipients.join(','));

		$('.recipients-list')
			.append('<div class="pull-left right-margined"><i class="ti-user"></i> <a class="remove-recipient pull-right" href="javascript:void(0)"><i class="ion-close-circled text-danger"></i></a><span>' + recipient  + '</span>&nbsp;&nbsp;</div>&nbsp;&nbsp; ')
			.removeClass('hide')
			.show();

	    $('input[name="recipients_input"]').val('').focus();			
	});

    jQuery(document).ready(function(){

    	$('input[name="recipients_input"]').ajaxify({
		  url: '/account/inbox/recipients',
		  focus: false,
		  onexit: function(){
		  	$('.recipients-drop')
		    	.html('')
		    	.fadeOut(100);
		  },
		  success: function(json){
		  	var str='<ul>';
		  	$(json).each(function(i,row){
		  		if( recipients.indexOf(row.title) == -1){
		  			str+='<li class="recipient" name="' + row.title + '"><a href="javascript:void(0)"><span><img src="' + row.avatar + '"></span><i>' + row.title + '</i></a></li>';
		  		}
		  	});
		  	str+='</ul>';
		    $('.recipients-drop')
		    	.html(str)
		    	.fadeIn(100);
		  }
		});
    });
</script>   