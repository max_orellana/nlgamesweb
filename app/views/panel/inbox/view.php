<div class="ps-wrapper panel-container">
    <div class="bg-container bg-user darkfade">
        <div class="backdrop-user">
        <?php echo messages();?>
        <?php if($entry):?>
            <div class="alert alert-info">
                <h4><a href="<?php print site_url('/account/inbox');?>"><?php print locale('inbox');?></a></h4>
                <h4><i class="ti-user"></i> <?php print $entry->login;?> &nbsp;&nbsp; <i class="ti-time"></i> <?php print timespan($entry->sent_time);?></h4>
                <h4><?php print $entry->inbox->title;?></h4>
            </div>
            <div class="alert">
    	        <p><?php print $entry->inbox->content;?></p><div class="clearfix"></div>
    	    </div>
        <?php endif;?>
    	    <!--div class="alert alert-success">
    	        <h3><?php print locale('reply')?> </h3>
                <form class="form" action="<?php print site_url('/account/inbox/send');?>" method="post">
                	<input type="hidden" name="inbox_id" value="<?php print segments(3);?>">
                    <div class="form-group">
                        <textarea class="form-control" name="description" value="" placeholder="<?php print locale('tickets_description');?>" required></textarea>
                    </div>
                    <div class="extended hide">
                        <div class="alert message hide"></div>
                        <div class="form-group text-right">
                            <a href="<?php print site_url('/account/inbox');?>" class="btn btn-lg btn-primary back"><i class="ion-ios-bolt fa-x1"></i> &nbsp;&nbsp;<?php print locale('back')?></a>
                            <button type="submit" name="reply" class="btn btn-lg btn-success" value="sender"><i class="ion-ios-bolt fa-x1"></i> &nbsp;&nbsp;<?php print locale('reply')?></button>
                            <button type="submit" name="reply" class="btn btn-lg btn-success" value="all"><i class="ion-ios-bolt fa-x1"></i> &nbsp;&nbsp;<?php print locale('reply_all')?></button>
                        </div>
                    </div>
                </form>
    	    </div><div class="clearfix"></div-->    
    	</div><div class="clearfix"></div>
    </div>
</div>

<script type="text/javascript">

    jQuery(document).ready(function(){
        $('textarea[name="description"]').click(function(){
            if( $('.extended').is(':hidden')){
                $('.extended')
                    .removeClass('hide')
                    .hide()
                    .slideDown();
            }
        });

    });
</script>   