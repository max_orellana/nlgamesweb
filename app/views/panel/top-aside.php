	<div class="nav-profile">
	    <?php if(session('forum_avatar')):?>
	    	<span><img src="<?php print session('forum_avatar');?>" alt="" width="100" height="100"></span>
        <?php else:?>
	    	<span><img src="<?php print site_asset('img/profile_default.png');?>" alt="" width="100" height="100"></span>
        <?php endif;?>
		
		<i>Welcome</i>
		<h3><?php print session('login');?></h3>
		<h6 class="status online"><i></i> Online</h6>
		<ul>
			<li><a href="/mail/inbox" title="Inbox" data-toggle="tooltip" data-placement="bottom"><i class="ti-email"></i></a></li>
			<li><a href="/personal/profile" title="Profile" data-toggle="tooltip" data-placement="bottom"><i class="ti-user"></i></a></li>
		<?php if(session('group') == 'user'):?>
			<li><a href="/user/forum" onclick="javascript:toggleFullScreen()" title="Full Screen" data-toggle="tooltip" data-placement="bottom"><i class="ti-fullscreen"></i></a></li>
		<?php endif;?>
		</ul>
	</div><!-- Navigation Profile -->