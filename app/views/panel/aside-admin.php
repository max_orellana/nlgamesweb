<div class="slide-panel" id="panel-scroll">
	<div class="slide-panel-inner">
		<div class="nav">
			<nav class="menu">
				<ul class="parent-menu">
					<li class="<?php print segments(2) == '' ? 'active':'';?>">
						<a href="<?php print site_url('/admin');?>" title=""><i class="ti-desktop"></i><span><?php print locale('dashboard');?></a>
					</li>
					<li class="<?php print segments(2) == 'posts' ? 'active':'';?>">
						<a href="<?php print site_url('/admin/posts');?>" title=""><i class="ti-anchor"></i><span><?php print locale('posts');?></a>
						<ul>
							<li><a href="<?php print site_url('/admin/posts/create');?>"><?php print locale('create');?></a></li>
						</ul>
					</li>
					<li class="<?php print segments(2) == 'items' ? 'active':'';?>">
						<a href="<?php print site_url('/admin/items');?>" title=""><i class="ti-anchor"></i><span><?php print locale('items');?></a>
						<ul>
							<li><a href="<?php print site_url('/admin/items/create');?>"><?php print locale('create');?></a></li>
							<li><a href="<?php print site_url('/admin/items-types');?>"><?php print locale('types');?></a></li>
							<li><a href="<?php print site_url('/admin/items-grades');?>"><?php print locale('grades');?></a></li>
							<li><a href="<?php print site_url('/admin/items-classes');?>"><?php print locale('classes');?></a></li>
						</ul>
					</li>
					<li class="<?php print segments(2) == 'postulations' ? 'active':'';?>">
						<a href="<?php print site_url('/admin/postulations');?>" title=""><i class="ti-bolt"></i><span><?php print locale('postulations');?></span></a>
						<ul>
							<li><a href="<?php print site_url('/admin/postulations-relation');?>"><?php print locale('list');?></a></li>
						</ul>
					</li>
					<li>
						<a href="<?php print site_url('/account/inbox');?>" title=""><i class="ti-email"></i><span><?php print locale('inbox');?></a>
						<ul>
							<li><a href="<?php print site_url('/account/inbox/compose');?>"><?php print locale('create');?></a></li>
						</ul>
					</li>
					<li>
						<a href="<?php print site_url('/admin/tickets');?>" title=""><i class="ti-support"></i><span><?php print locale('tickets');?></span></a>
					</li>
					<li>
						<a href="<?php print site_url('/admin/coupons');?>" title=""><i class="ti-pin-alt"></i><span><?php print locale('coupons');?></span></a>
						<ul>
							<li><a href="<?php print site_url('/admin/coupons-types');?>"><?php print locale('types');?></a></li>
						</ul>
					</li>
					<li>
						<a href="<?php print site_url('/admin/sales');?>" title=""><i class="ti-pin-alt"></i><span><?php print locale('sales');?></span></a>
						<ul>
							<li><a href="<?php print site_url('/admin/orders');?>"><?php print locale('orders');?></a></li>
							<li><a href="<?php print site_url('/admin/paymens');?>"><?php print locale('payments');?></a></li>
						</ul>
					</li>
					<li>
						<a href="<?php print site_url('/admin/tags');?>" title=""><i class="ti-tag"></i><span><?php print locale('tags');?></span></a>
					</li>
					<li>
						<a href="<?php print site_url('/admin/words');?>" title=""><i class="ti-text"></i><span><?php print locale('words');?></span></a>
						<ul>
							<li><a href="<?php print site_url('/admin/words/create');?>"><?php print locale('create');?></a></li>
						</ul>
					</li>
				</ul>
			</nav>
		</div>

		<div class="nav">
			<nav class="menu">
				<ul class="parent-menu">
					<li class="high <?php print segments(2) == 'packages' ? 'active':'';?>">
						<a href="<?php print site_url('/account/packages');?>" title=""><i class="ion-key"></i><span><?php print locale('join_beta');?></a>
					</li>
					<li class="<?php print (segments(1) == 'user' AND ! segments(2)) ? 'active':'';?>">
						<a href="<?php print site_url('/account/dash');?>" title=""><i class="ion-home"></i><span><?php print locale('dashboard');?></a>
					</li>
					<li class="<?php print segments(2) == 'profile' ? 'active':'';?>">
						<a href="<?php print site_url('/account/profile');?>" title=""><i class="ion-android-person"></i><span><?php print locale('my_account');?></a>
					</li>
					<!--li class="<?php print segments(2) == 'items' ? 'active':'';?>">
						<a href="<?php print site_url('/account/items');?>" title=""><i class="ion-wand"></i> <span><?php print locale('items');?></a>
					</li-->
					<li class="<?php print segments(2) == 'forum' ? 'active':'';?>">
						<a href="<?php print site_url('/account/forum');?>" title=""><i class="ion-<?php print empty(session('forum_name'))?'pull-request':'network';?>"></i><span><?php print locale((empty(session('forum_name')) ? 'connect_forum' : 'forum'));?></a>
					</li>
					<li class="<?php print segments(2) == 'postulations' ? 'active':'';?>">
						<a href="<?php print site_url('/account/postulations');?>" title=""><i class="ion-ios-people"></i><span><?php print locale('postulations');?></a>
					</li>
					<!--li class="<?php print segments(2) == 'tickets' ? 'active':'';?>">
						<a href="<?php print site_url('/account/tickets');?>" title=""><i class="ion-pricetags"></i><span><?php print locale('support');?></a>
					</li-->
				</ul>
			</nav>
		</div>
			
	</div>
</div>