<div class="ps-wrapper panel-container content-height">
  <div class="bg-container bg-advertiser darkfade">
      <header class="c-section_header section-advertiser bottom-margin">
          <h2 class="c-section_title u-text-gradient-advertiser"><?php print locale('advertiser_goals_title');?></h2>
      </header>
       
      <div class="col-md-6">
      <h3><?php print locale('advertiser_current_level');?>: <?php print $level+1;?></h3>
        
        
        <?php if($rules[$level]['coins']):?>
        <small><?php print locale('advertiser_progress_coins_level');?></small>
        <div class="progress">
          <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?php print $bar_perc['coins'];?>%">
            <span class="sr-only"></span>
          </div>
        </div>
        <?php endif;?>

        <?php if($rules[$level]['registers']):?>
        <small><?php print locale('advertiser_progress_registers_level');?></small>
        <div class="progress">
          <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?php print $bar_perc['registers'];?>%">
            <span class="sr-only"></span>
          </div>
        </div>
        <?php endif;?>
        <?php foreach ($rules as $i => $rule):?>
          <?php if($i):?>
            <div>
              <h3 class="<?php print ($level == ($i-1)?'':'text-muted');?>"><?php print ($level == ($i-1)?'<i class="fa fa-chevron-right"></i>&nbsp;':'');?><?php print locale('advertiser_goals_' . $i . '_title');?></h3>
              <p class="<?php print ($level == ($i-1)?'':'text-muted');?>"><?php print locale('advertiser_goals_' . $i . '_text');?></p>
            </div>
          <?php endif;?>
        <?php endforeach;?>
      </div>
      <div class="col-md-6">
        <h4><?php print locale('advertisers_goals_title');?></h4>

      <?php if($rules[$level]['coins']):?>
        <h2><?php print locale('coins_goal');?> : <?php print $rules[$level]['coins'];?> </h2>
        <h2><?php print locale('coins_left');?> : <?php print($rules[$level]['coins'] - $coins);?></h2>
        <hr>
        <h2><?php print locale('money_current');?> : <?php print ceil($coins * $rules[$level]['perc']);?> </h2>
        <h2><?php print locale('money_goal');?> : <?php print ceil($rules[$level]['coins'] * $rules[$level]['perc']);?> </h2>

      <?php endif;?>

      <?php if($rules[$level]['registers']):?>
        <h2><?php print locale('goal');?> : <?php print $rules[$level]['registers'];?> </h2>
        <h2><?php print locale('left');?> :  <?php print locale('advertiser_registers');?>: <?php print($rules[$level]['registers'] - count($registers));?></h2>
        <hr>
        <h2><?php print locale('money');?> : <?php print ceil($rules[$level]['registers'] * $rules[$level]['perc']);?> </h2>
      <?php endif;?>

      </div>
  </div><div class="clearfix"></div>
</div>