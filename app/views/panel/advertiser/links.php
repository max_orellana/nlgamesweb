<div class="ps-wrapper panel-container content-height">
  <div class="bg-container bg-advertiser darkfade">
      <header class="c-section_header section-advertiser bottom-margin">
          <h2 class="c-section_title u-text-gradient-advertiser"><?php print locale('advertiser_link_generator_title');?></h2>
      </header>
       
      <div class="container">
        <div class="opacity-box">
          <h3 class="text-center"><?php print locale('my_advertiser_token');?> <span class="advertiser-token"><?php print session('token');?></span></h3>
          <div class="row">
            <div class="col-md-6">
              <h3><?php print locale('advertiser-links-fanpage');?></h3>
              <input type="text" class="form-control" value="<?php print site_url('/?at=' . session('token'));?>" readonly>
            </div>
            <div class="col-md-6">
              <h3><?php print locale('advertiser-links-news');?></h3>
              <input type="text" class="form-control" value="<?php print site_url('/news/?at=' . session('token'));?>" readonly>
            </div>
            <div class="col-md-6">
              <h3><?php print locale('advertiser-links-guide');?></h3>
              <input type="text" class="form-control" value="<?php print site_url('/guides/?at=' . session('token'));?>" readonly>
            </div>
            <div class="col-md-6">
              <h3><?php print locale('advertiser-links-heroes');?></h3>
              <input type="text" class="form-control" value="<?php print site_url('/guides/heroes/?at=' . session('token'));?>" readonly>
            </div>
            <div class="col-md-6">
              <h3><?php print locale('advertiser-links-inferno');?></h3>
              <input type="text" class="form-control" value="<?php print site_url('/guides/inferno?at=' . session('token'));?>" readonly>
            </div>
            <div class="col-md-6">
              <h3><?php print locale('advertiser-links-story');?></h3>
              <input type="text" class="form-control" value="<?php print site_url('/story/?at=' . session('token'));?>" readonly>
            </div><div class="clearfix"></div>
          </div>
          <div class="group-control">&nbsp;</div>
        </div>
      </div>
  </div><div class="clearfix"></div>
</div>