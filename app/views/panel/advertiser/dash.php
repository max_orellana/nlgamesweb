<div class="ps-wrapper panel-container content-height">
  <div class="bg-container bg-advertiser darkfade">
      <header class="c-section_header section-advertiser bottom-margin">
          <h2 class="c-section_title u-text-gradient-advertiser"><?php print locale('advertiser_panel_title');?></h2>
      </header>

      <div class="row-vertical">
          <div class="row-top">
              <div class="bd-box text-center">
                  <div class="bd-box-page-box1">
                      <div class="bd-box-hat-title"><?php print locale('advertiser_link_generator_title');?></div>
                      <div class="bd-box-q4"></div>
                      <div class="bd-box-hat-text"><?php print locale('advertiser_link_generator_text');?></div>
                      <div class="clearfix"></div>
                      <div class="special-button-container">
                        <a href="/advertiser/links" class="special-button"><span><?php print locale('advertiser_link_generator_button');?></span></a>
                      </div>
                  </div>

                  <div class="bd-box-page-box2">
                      <div class="bd-box-book-title"><?php print locale('advertiser_performance_title');?></div>
                      <div class="bd-box-objective"></div>
                      <div class="bd-box-book-text"><?php print locale('advertiser_performance_text');?></div>
                      <div class="special-button-container">
                          <a href="<?php print site_url('/advertiser/goals');?>" class="special-button"><span><?php print locale('advertiser_performance_button');?></span></a>
                      </div>
                  </div>
                  <div class="clear"></div>
              </div>
          </div>
         
          <div class="row-bottom">
            <div class="text-center">
                <h1 class="text-center bottom-spaced"><?php print locale('dash_advertiser_boxes_title');?></h1>
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                      <h3><?php print locale('dash_advertiser_boxes_box_1_title');?></h3>
                      <p><?php print locale('dash_advertiser_boxes_box_1_text');?> </p>
                      <a href="/advertiser/progress/day">
                      <h5><?php print $registers_day;?> <?php print locale('advertiser_registers');?></h5>
                      <h5><?php print $packs_day;?> <?php print locale('advertiser_packs');?></h5>
                      </a>
                    </div>
                    <a href="/advertiser/progress/day" class="small-box-footer">ARS <?php print $coins_day;?></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                      <h3><?php print locale('dash_advertiser_boxes_box_2_title');?></h3>
                      <p><?php print locale('dash_advertiser_boxes_box_2_text');?></p>
                      <a href="/advertiser/progress/week">
                      <h5><?php print $registers_week;?> <?php print locale('advertiser_registers');?></h5>
                      <h5><?php print $packs_week;?> <?php print locale('advertiser_packs');?></h5>
                      </a>
                    </div>
                    <div class="icon">
                      <i class="fa fa-advertiser"></i>
                    </div>
                    <a href="/advertiser/progress/week" class="small-box-footer">ARS <?php print $coins_week;?></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                      <h3><?php print locale('dash_advertiser_boxes_box_3_title');?></h3>
                      <p><?php print locale('dash_advertiser_boxes_box_3_text');?> </p>
                      <a href="/advertiser/progress/month">
                      <h5><?php print $registers_month;?> <?php print locale('advertiser_registers');?></h5>
                      <h5><?php print $packs_month;?> <?php print locale('advertiser_packs');?></h5>
                      </a>
                    </div>
                    <a href="/advertiser/progress/month" class="small-box-footer">ARS <?php print $coins_month;?></a>
                  </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-green">
                    <div class="inner">
                      <h3><?php print locale('dash_advertiser_boxes_box_4_title');?></h3>
                      <p><?php print locale('dash_advertiser_boxes_box_4_text');?> </p>
                      <a href="/advertiser/progress">
                      <h5><?php print $registers_total;?> <?php print locale('advertiser_registers');?></h5>
                      <h5><?php print $packs_total;?> <?php print locale('advertiser_packs');?></h5>
                      </a>
                    </div>
                    <div class="icon">
                      <i class="fa fa-line-chart"></i>
                    </div>
                    <a href="/advertiser/progress" class="small-box-footer">ARS <?php print $coins_total;?></a>
                  </div>
                </div><div class="clearfix"></div>
            </div><div class="clearfix"></div>
          </div><div class="clearfix"></div>
      </div><div class="clearfix"></div>
  </div><div class="clearfix"></div>
</div>