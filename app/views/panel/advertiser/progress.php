<div class="ps-wrapper panel-container">
	<div class="bg-container bg-advertiser darkfade">
	    <header class="c-section_header section-advertiser bottom-margin">
	        <h2 class="c-section_title u-text-gradient-advertiser"><?php print locale('advertiser-progress');?> <?php print locale(segments(3));?></h2>
	    </header>
		<div class="bd-box btn-custom">
			<?php echo messages();?>
			
			<a class="btn btn-<?php print (segments(3) == 'day' ? 'success' : 'info');?>" href="/advertiser/progress/day"><?php print locale('today');?></a>
			<a class="btn btn-<?php print (segments(3) == 'week' ? 'success' : 'info');?>" href="/advertiser/progress/week"><?php print locale('week');?></a>
			<a class="btn btn-<?php print (segments(3) == 'month' ? 'success' : 'info');?>" href="/advertiser/progress/month"><?php print locale('month');?></a>
			<a class="btn btn-<?php print (segments(3) == '' ? 'success' : 'info');?>" href="/advertiser/progress"><?php print locale('total');?></a>			

			<div class="group-control">&nbsp;</div>
			<?php if(count($entries)):?>
				<table class="table table-advertiser">
					<thead>
						<tr>
							<th width="70"><i class="ion-images"></i></th>
							<th><?php print locale('user');?></th>
							<th><?php print locale('coins');?></th>
							<th><?php print locale('date');?></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($entries as $entry):?>
						<tr class="<?php print $entry['css'];?>">
							<td>
						    <?php if(!empty($entry['avatar'])):?>
						    	<img src="<?php print $entry['avatar'];?>" alt="" width="50" height="50">
					        <?php else:?>
						    	<img src="<?php print site_asset('img/profile_default.png');?>" alt="" width="50" height="50">
					        <?php endif;?>	
							</td>
							<td><?php print $entry['login'];?></td>
							<td><?php print $entry['value'];?></td>
							<td><?php print $entry['datetime'];?></td>
						</tr>
					<?php endforeach;?>
					</tbody>
				</table>
				<?php endif;?>
			</div><div class="clearfix"></div>
		</div>
	</div>
</div>