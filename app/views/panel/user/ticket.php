<div class="panel">
    <div class="col-md-12">
    <?php echo messages();?>
    <?php if($rows):?>
        <?php foreach($rows as $ticket):?>
            <div class="alert <?php print $ticket->group_id == 2 ? '' : 'alert-success text-right';?>">
                <h4><?php print timespan($ticket->created);?></h4>
                <p><?php print $ticket->content;?></p><div class="clearfix"></div>
            </div>
        <?php endforeach;?>
    <?php else:?>
        <div class="alert alert-success">
            <i class="ion-checkmark-circled"></i> <?php print locale('tickets_empty');?>
            <a class="btn btn-success"><?php print locale('tickets_new');?></a>
        </div>
    <?php endif;?>
    <?php if($thread->status_id == 1):?>
        <div class="alert alert-success">
            <h1><?php print locale('reply')?> </h1>
            <form class="form" action="<?php print site_url('/account/tickets/send');?>" enctype="multipart/form-data" method="post">
                <input type="hidden" name="ticket_id" value="<?php print $rows[0]->id;?>">
                <div class="form-group">
                    <textarea class="form-control" name="description" value="" placeholder="<?php print locale('tickets_description');?>" required></textarea>
                </div>
                <div class="form-group">
                    <input type="file" name="file" value="" placeholder="<?php print locale('tickets_attachment');?>">
                </div>
                <div class="alert message hide"></div>
                <div class="form-group text-right">
                    <a href="<?php print site_url('/account/tickets');?>" class="btn btn-lg btn-primary back"><i class="ion-backspace fa-x1"></i> &nbsp;&nbsp;<?php print locale('back')?></a>
                    <button type="submit" class="btn btn-lg btn-success"><i class="ion-ios-bolt fa-x1"></i> &nbsp;&nbsp;<?php print locale('send')?></button>
                </form>
            </form>
        </div><div class="clearfix"></div>    
    </div><div class="clearfix"></div>
    <?php endif;?>
</div>
