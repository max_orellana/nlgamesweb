<div class="bg-container">
    <div class="bg-content-wide">

        <div class="widget white">
            <div class="database-migration">
                <h3><?php print locale('get_coins');?></h3>
                <p><?php print locale('coins_balance');?> <?php print isset($row) ? $row->coins:0;?></p>
                <div class="progress w-tooltip">
                    <div style="width: 33%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="33" role="progressbar" class="green progress-bar">
                    <span><i><?php print locale('select_coins');?> </i> 1/3</span>
                   </div>
                </div>
            </div>
        </div>

        <div class="widget color red">
            <div class="database-migration">
                <h3><i class="fa fa-hand-o-right"></i> Escoge la cantidad de monedas que deseas adquirir</h3>
                <hr/>
                <p>Nuestros métodos de pago son 100% seguros.</p>

                <form class="form" action="<?php print site_url('/account/payment');?>">
                    <input type="hidden" name="source" value="coins" />
                    <div class="group-control">&nbsp;</div>
                    <div class="form-group">
                    	<select name="coins" class="form-control">
                    		<?php foreach(\Controller\User\PanelController::$coins as $option):?>
                    			<option value="<?php print $option;?>"><?php print $option;?> <?php print locale('coins');?></option>
                    		<?php endforeach;?>
                    	</select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="alert message hide"></div>
                    <div class="form-group text-right btn-custom">
                        <button type="submit" class="btn btn-success"><i class="ion-ios-bolt fa-x1"></i> <span><?php print locale('continue');?></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
