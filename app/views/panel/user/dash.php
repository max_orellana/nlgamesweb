<div class="ps-wrapper panel-container content-height">
  <div class="bg-container bg-user darkfade">
      <header class="c-section_header section-user bottom-margin">
          <h2 class="c-section_title u-text-gradient-user"><?php print locale('beta_makesure');?></h2>
      </header>
       
      <div class="row-vertical">
          <div class="row-top">

              <div class="bd-box text-center">
                  <div class="bd-box-page-box1">
                      <div class="bd-box-hat-title"><?php print locale('beta_suscribe_dash_title');?></div>
                      <div class="bd-box-hat"></div>
                      <div class="bd-box-hat-text"><?php print locale('beta_suscribe_dash_text');?></div>
                      
                        <?php if(!empty($pack)):?>
                          <p class="text-success"><?php print locale('wannajoinbeta_already_pack');?></p>
                        <?php elseif(!empty($account->heroe)):?>
                          <p class="text-success"><?php print locale('wannajoinbeta_already_suscribed');?></p>
                        <?php else:?>
                          <div class="special-button-container">
                            <a href="#wannajoinbeta" data-toggle="modal" class="special-button"><span><?php print locale('beta_suscribe_dash_button');?></span></a>
                            </div>
                        <?php endif;?>
                     
                  </div>

                  <div class="bd-box-page-box2">
                      <div class="bd-box-book-title"><?php print locale('beta_makesure_dash_title');?></div>
                      <div class="bd-box-book"></div>
                      <div class="bd-box-book-text"><?php print locale('beta_makesure_dash_text');?></div>
                      <div class="special-button-container">
                          <a href="<?php print site_url('/joinbeta');?>" class="special-button" target="_blank"><span><?php print locale('beta_makesure_dash_button');?></span></a>
                      </div>
                  </div>
                  
                  <div class="clear"></div>
              </div>
          </div>
         
          <div class="row-bottom">
            <div class="col-pad-lg text-center">
                <h1 class="text-center bottom-spaced"><?php print locale('dash_boxes_title');?></h1>
                <div class="col-lg-4 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                      <h3><?php print locale('dash_boxes_box_1_title');?></h3>
                      <p><?php print locale('dash_boxes_box_1_text');?></p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-users"></i>
                    </div>
                    <a href="<?php print site_url('/account/postulations');?>" class="small-box-footer"><?php print locale('more_info');?></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-4 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                      <h3><?php print locale('dash_boxes_box_2_title');?></h3>
                      <p><?php print locale('dash_boxes_box_2_text');?> </p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-user"></i>
                    </div>
                    <a href="<?php print site_url('/account/profile');?>" class="small-box-footer"><?php print locale('more_info');?></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-4 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                      <h3><?php print locale('dash_boxes_box_3_title');?></h3>
                      <p><?php print locale('dash_boxes_box_3_text');?> </p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-plug"></i>
                    </div>
                    <a href="<?php print site_url('/account/forum');?>" class="small-box-footer"><?php print locale('more_info');?></a>
                  </div>
                </div><div class="clearfix"></div>
            </div><div class="clearfix"></div>
          </div><div class="clearfix"></div>
      </div><div class="clearfix"></div>
  </div><div class="clearfix"></div>
</div>