<div class="ps-wrapper panel-container content-height">
    <div class="bg-container bg-profile darkfade">

        <header class="c-section_header section-<?php print session('group');?> bottom-margin">
            <h2 class="c-section_title u-text-gradient-<?php print session('group');?>">MI CUENTA</h2>
        </header>

        <div class="row-vertical">
            <div class="row-top">
                <div class="col-sm-4">
                    <div class="small-profile">
                        <div class="col1 text-center">
                            <h5 class="copper text-center text-uppercase"><?php print session('login');?></h5>
                            <div class="avatar-frame">
                            <?php if(!empty(session('forum_avatar'))):?>
                                <img src="<?php print session('forum_avatar');?>">
                            <?php else:?>   
                                <img src="<?php print site_asset('img/profile_default.png');?>">
                            <?php endif;?>  
                            </div>
                        </div>
                        <div class="col2 text-center">
                            <h5 class="copper text-center">STATS</h5>
                            <div class="btn-exp">
                                <div class="xp-gauge">Lvl 1 - 0%</div> 
                                <!--div class="xp-val"><span class="copper"></span></div-->
                            </div>
                            <div class="btn-blue" style="padding-top:2px;"><img src="<?php print site_asset('/img/icon-rank.png');?>" /> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span class="copper"> VAGABOND</span> &nbsp; &nbsp; </div>
                            <div class="btn-blue" style="padding-top:7px;"><img src="<?php print site_asset('/img/icon-blades.png');?>" /> &nbsp; <span class="copper">0</span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img src="<?php print site_asset('/img/icon-coin.png');?>" /> &nbsp; <span class="copper"><?php print session('coins');?></span></div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">&nbsp;</div>

                <div class="col-sm-4">
                    <h5 class="copper text-center">ESTADO DE CUENTA</h5>
                    
                    <?php if(!empty($package)):?>
                        <h3 class="account-statustext-success text-center text-uppercase"><?php print locale('account_active_waiting_beta');?></h3>
                    <?php elseif(!empty($game) AND $game->accessLevel > -1):?>
                        <h3 class="account-status text-success text-center text-uppercase"><?php print locale('account_active');?></h3>
                    <?php else:?>
                        <h3 class="account-status text-danger text-center text-uppercase"><?php print locale('account_inactive');?></h3>
                    <?php endif;?>
                    <h5 class="copper text-center">PRIVACIDAD</h5>
                    <div class="text-center">
                        <div class="btn-blue"><a href="#updatepassword" role="button" data-toggle="modal">CAMBIAR CONTRASE&Ntilde;A</a></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="btn-blue">ACTIVAR BETA-KEY</div>
                    </div>
                </div>
            </div>

            <div class="row-bottom">
                <div class="col-sm-4">
                    <h4 class="copper text-center">ESTADISTICAS</h4>
                    <div class="opacity-box statistics-frame">
                        <div class="row">
                            <h5 class="text-center copper">PARTIDAS TOTALES</h5>
                            <div class="row">
                                <div class="col-sm-4">
                                    <p class="copper text-center">JUGADAS</p>
                                    <p class="copper text-center"><img src="<?php print site_asset('/img/icon-played.png');?>" /> 0</p>
                                </div>
                                <div class="col-sm-4">
                                    <p class="copper text-center">GANADAS</p>
                                    <p class="copper text-center"><img src="<?php print site_asset('/img/icon-won.png');?>" /> 0</p>
                                </div>
                                <div class="col-sm-4">
                                    <p class="copper text-center">PERDIDAS</p>
                                    <p class="copper text-center"><img src="<?php print site_asset('/img/icon-lost.png');?>" /> 0</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <h5 class="text-center copper">REPUTACION</h5>
                                    <p class="copper text-center"><img src="<?php print site_asset('/img/icon-rep.png');?>" /> 0</p>
                                </div>
                                <div class="col-sm-6">
                                    <h5 class="text-center copper">KARMA</h5>
                                    <p class="copper text-center"><img src="<?php print site_asset('/img/icon-karma.png');?>" /> 0</p>
                                </div>
                            </div>
                            <h5 class="text-center copper">TROFEOS PREMIUM</h5>
                            <div class="row">
                                <div class="col-sm-4 text-center"><img src="<?php print site_asset('/img/copper-cup.png');?>" alt="copa cobre"/><p class="copper text-center">0</p></div>
                                <div class="col-sm-4 text-center"><img src="<?php print site_asset('/img/silver-cup.png');?>" alt="copa plata" /><p class="copper text-center">0</p></div>
                                <div class="col-sm-4 text-center"><img src="<?php print site_asset('/img/gold-cup.png');?>" alt="copa oro"/><p class="copper text-center">0</p></div>
                            </div>
                            <h5 class="text-center copper">TROFEOS ONLINE</h5>
                            <div class="row">
                                <div class="col-sm-4 text-center"><img src="<?php print site_asset('/img/copper-cup.png');?>" alt="copa cobre"/><p class="copper text-center">0</p></div>
                                <div class="col-sm-4 text-center"><img src="<?php print site_asset('/img/silver-cup.png');?>" alt="copa plata"/><p class="copper text-center">0</p></div>
                                <div class="col-sm-4 text-center"><img src="<?php print site_asset('/img/gold-cup.png');?>" alt="copa oro"/><p class="copper text-center">0</p></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 row-container">
                    <h4 class="text-center copper"></h4>
                    <div class="popular-classes"><h4 class="text-center copper">HEROES MAS JUGADOS</h4>
                        <div class="class-circle grey"><div class="lg-icon icon-none"></div><h5 class="copper">NONE</h5></div>
                        <div class="class-circle grey"><div class="lg-icon icon-none"></div><h5 class="copper">NONE</h5></div>
                        <div class="class-circle grey"><div class="lg-icon icon-none"></div><h5 class="copper">NONE</h5></div>
                    </div>
                </div>

                <div class="col-sm-4 no-padding match-history-box">
                    <h4 class="copper text-center">HISTORIAL DE PARTIDAS</h4>
                    <div class="opacity-box fa-message container-fluid text-center">
                        <div>
                            <i class="fa fa-hand-rock-o"></i><p><?php print locale('account_no_matches');?></p>
                        </div>
                    </div>
                    <!--div class="statistics-frame">
                        <table class="table-game-history">
                            <tbody>
                                <tr class="won">
                                    <td><div class="sm-icon icon-knight-sm"></div></td>
                                    <td>Victoria</td>
                                    <td><div class="sm-icon icon-kills"></div></td>
                                    <td>999</td>
                                    <td><div class="sm-icon icon-deaths"></div></td>
                                    <td>999</td>
                                    <td><div class="sm-icon icon-paw"></div></td>
                                    <td>999</td>
                                    <td>18-04-16</td>
                                </tr>
                                <tr class="won">
                                    <td><div class="sm-icon icon-knight-sm"></div></td>
                                    <td>Victoria</td>
                                    <td><div class="sm-icon icon-kills"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-deaths"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-paw"></div></td>
                                    <td>37</td>
                                    <td>18-04-16</td>
                                </tr>
                                <tr class="lost">
                                    <td><div class="sm-icon icon-knight"></div></td>
                                    <td>Derrota</td>
                                    <td><div class="sm-icon icon-kills"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-deaths"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-paw"></div></td>
                                    <td>37</td>
                                    <td>18-04-16</td>
                                </tr>
                                <tr class="lost">
                                    <td><div class="sm-icon icon-knight"></div></td>
                                    <td>Derrota</td>
                                    <td><div class="sm-icon icon-kills"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-deaths"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-paw"></div></td>
                                    <td>37</td>
                                    <td>18-04-16</td>
                                </tr>
                                <tr class="won">
                                    <td><div class="sm-icon icon-knight"></div></td>
                                    <td>Victoria</td>
                                    <td><div class="sm-icon icon-kills"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-deaths"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-paw"></div></td>
                                    <td>37</td>
                                    <td>18-04-16</td>
                                </tr>
                                <tr class="won">
                                    <td><div class="sm-icon icon-knight"></div></td>
                                    <td>Victoria</td>
                                    <td><div class="sm-icon icon-kills"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-deaths"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-paw"></div></td>
                                    <td>37</td>
                                    <td>18-04-16</td>
                                </tr>
                                <tr class="lost">
                                    <td><div class="sm-icon icon-knight"></div></td>
                                    <td>Derrota</td>
                                    <td><div class="sm-icon icon-kills"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-deaths"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-paw"></div></td>
                                    <td>37</td>
                                    <td>18-04-16</td>
                                </tr>
                                <tr class="won">
                                    <td><div class="sm-icon icon-knight"></div></td>
                                    <td>Victoria</td>
                                    <td><div class="sm-icon icon-kills"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-deaths"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-paw"></div></td>
                                    <td>37</td>
                                    <td>18-04-16</td>
                                </tr>
                                <tr class="lost">
                                    <td><div class="sm-icon icon-knight"></div></td>
                                    <td>Derrota</td>
                                    <td><div class="sm-icon icon-kills"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-deaths"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-paw"></div></td>
                                    <td>37</td>
                                    <td>18-04-16</td>
                                </tr>
                                <tr class="won">
                                    <td><div class="sm-icon icon-knight"></div></td>
                                    <td>Victoria</td>
                                    <td><div class="sm-icon icon-kills"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-deaths"></div></td>
                                    <td>8</td>
                                    <td><div class="sm-icon icon-paw"></div></td>
                                    <td>37</td>
                                    <td>18-04-16</td>
                                </tr>
                            </tbody>
                        </table>
                    </div-->
                </div>
            </div>
        </div>
    </div>
</div>