<div class="bg-container">
    <div class="bg-content-wide">

        <div class="widget white">
            <div class="database-migration">
                <h3><?php print locale('get_coins');?></h3>
                <p><?php print locale('coins_balance');?> <?php print isset($row) ? $row->coins:0;?></p>
                <div class="progress w-tooltip">
                    <div style="width: 100%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" role="progressbar" class="green progress-bar">
                    <span><i><?php print locale('order_confirm');?> </i> 3/3</span>
                   </div>
                </div>
            </div>
        </div>

        <div class="widget color red">
            <div class="database-migration">    
                <h3><i class="fa fa-credit-card"></i> <?php print locale('order_confirm');?></h3>
                <hr/>
                <p><?php print locale('order_confirm_text');?></p>

                <table class="table">
                    <tr>
                        <th><?php print locale('operation');?></th>
                        <td><?php print locale('coins');?></td>
                    </tr>
                    <tr>
                        <th><?php print locale('reference');?></th>
                        <td><?php print $order->reference;?></td>
                    </tr>
                    <tr>
                        <th><?php print locale('payment_method');?></th>
                        <td><?php print $order->payment_method;?></td>
                    </tr>
                    <tr>
                        <th><?php print locale('coins');?></th>
                        <td><?php print $order->coins;?></td>
                    </tr>
                    <tr>
                        <th><strong><?php print locale('amount');?></strong></th>
                        <td><?php print $order->currency;?> <?php print $order->amount;?></td>
                    </tr>
                </table>
                <form class="form" action="<?php print site_url('/user/order');?>" method="post">
                    <input type="hidden" name="id" value="<?php print $order->id;?>" />
                    <div class="clearfix"></div>
                    <div class="alert message hide"></div>
                    <div class="form-group text-right btn-custom">
                        <button type="submit" class="btn btn-success"><i class="ion-ios-bolt fa-x1"></i> <span><?php print locale('confirm');?></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>