<div class="bg-container">
    <div class="bg-content-wide">

    
        <div class="widget white">
            <div class="database-migration">
                <h3><?php print locale('get_coins');?></h3>
                <p><?php print locale('coins_balance');?> <?php print isset($row) ? $row->coins:0;?></p>
                <div class="progress w-tooltip">
                    <div style="width: 66%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="66" role="progressbar" class="green progress-bar">
                    <span><i><?php print locale('select_payment_method');?> </i> 2/3</span>
                   </div>
                </div>
            </div>
        </div>

        <div class="widget color red">
            <div class="database-migration">
                <h3><i class="fa fa-credit-card"></i> Escoge el método de pago</h3>
                <hr/>
                <p>Nuestros métodos de pago son 100% seguros.</p>

                <form class="form" action="<?php print site_url('/user/resume');?>" method="post">
                    <input type="hidden" name="coins" value="<?php print $order->coins;?>" />
                    <input type="hidden" name="order_id" value="<?php print $order->id;?>" />
                    <input type="hidden" name="source" value="<?php print $order->source;?>" />
                    <input type="hidden" name="amount" value="<?php print $order->amount;?>" />
                    <div class="form-group custom-radio text-center">
                    	<div class="pull-left">
        					<label class="selected">
        					  <input checked="checked" value="paypal" id="optionsRadios1" name="payment_method" type="radio">
                              <img src="<?php print site_asset('/img/paypal-logo.jpg');?>" width="100" >
        					</label>&nbsp;&nbsp;&nbsp;&nbsp;
        				</div>
                    	<div class="pull-left">
        					<label>
        					  <input value="payu" id="optionsRadios2" name="payment_method" type="radio">
                              <img src="<?php print site_asset('/img/PayU-logo.jpg');?>" width="100">
        					</label>
        			    </div>
                    </div><div class="clearfix"></div>
                    <div class="alert message hide"></div>
                    <div class="form-group text-right btn-custom">
                        <button type="submit" class="btn btn-success"><i class="ion-ios-bolt fa-x1"></i> <span><?php print locale('continue');?></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $('.custom-radio input').click(function(){
        console.log("sss");
        $('.custom-radio label').removeClass('selected');
        $(this).parent().addClass('selected');
    });
</script>