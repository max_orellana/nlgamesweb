<link rel="stylesheet" href="<?php print site_url('/assets/009/css/postulations.css');?>">

<div class="ps-wrapper panel-container content-height">
    <div class="admin-content darkfade">

        <?php echo messages();?>

        <div id="presentation">
            <section class="c-section">
                <header class="c-section_header section-<?php print session('group');?>">
                    <h2 class="c-section_title u-text-gradient-<?php print session('group');?>"><?php print locale('postulations_title');?></h2>
                </header>    
            </section>

            <div class="career-content">
                <div class="section roles">
                    <div class="section-text center">
                        <h3 class="section-header">
                            Elige tu destino
                        </h3>
                    </div><div class="clearfix"></div>
                    <ul class="career-buttons">
                        <?php foreach($groups as $group):?>
                            <li class="career-button <?php echo $group->slug;?>">
                                <a href="javascript:void(0)" class="postulation-group" data-slug="<?php echo $group->slug;?>">
                                    <span class="role-icon"></span>
                                    <span class="role-label"><?php echo locale('postulations_' . $group->slug);?></span>
                                </a>
                            </li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="postulation-list text-center">
            <table>
                <tr>
                    <td width="25%">&nbsp;</td>
                    <td width="50%" style="vertical-align: middle;position: relative;">
                        <div class="text-center postulation-desc">
                            <h1><?php echo locale('postulations_intro_title');?></h1>
                            <p><?php echo locale('postulations_intro_text');?></p>
                        </div>
                    </td>
                    <td width="25%">&nbsp;</td>
                </tr>
            </table>
        </div>

    <?php foreach($groups as $group):?>
        <div class="postulation-list hide <?php echo $group->slug;?>">
            <table>
                <tr>
                    <td width="25%" style="vertical-align: top;padding-top: 5px">
                        <ul class="career-buttons">
                            <li class="career-button <?php echo $group->slug;?>">
                                <span class="role-icon"></span>
                            </li>
                        </ul>                
                        <ul class="dots margin-vcenter">
                    <?php foreach($group->postulations() as $postulation):?>
                            <li><a href="javascript:void(0)" class="postulation-item" data-id="<?php echo $postulation->id;?>" data-slug="<?php echo $postulation->slug;?>"><?php echo locale('postulations_' . $postulation->slug);?></a></li>
                    <?php endforeach;?>
                        </ul>
                    </td>
                    <td width="75%">

                    <?php foreach($group->postulations() as $postulation):?>

                        <div class="text-center hide postulation-desc <?php echo $postulation->slug;?>">
                            <div class="col-md-8">
                                    <h1><?php echo locale('postulations_' . $postulation->slug);?></h1>
                                <p><?php echo locale('postulations_' . $postulation->slug . '_text');?></p>
                            </div>
                            <div class="col-md-4 btn-custom" style="position: absolute;top: 70px;right:0">
                                <h4 class="blue-glow" style="font-size:20px"><?php echo locale('postulations_vacancy_count');?></h4>
                                <h3 class="blue-glow"><?php echo $applicants[$postulation->id];?> <?php echo locale('out_of');?> <?php echo $postulation->vacancies;?></h3>
                            <?php if($mys[$postulation->id]):?>
                                <p><u><?php print locale('postulations_already_sent');?></u></p>
                            <?php else:?>
                                <div class="special-button-container">
                                    <a class="special-button" href="#account.postulation_form" data-json='{"id":"<?php print $postulation->id;?>","title":"<?php echo locale('postulations_' . $postulation->slug);?>"}' role="button" data-toggle="modal">
                                    <span><?php print locale('apply_now');?></span>
                                    </a>
                                </div>
                            <?php endif;?>
                            </div>
                        </div>
                    <?php endforeach;?>
                    </td>
                </tr>
            </table>
        </div>
    <?php endforeach;?>
    </div>
</div>

<script type="text/javascript">

    jQuery(document).ready(function(){

        $(".postulation-group").click( function(){

            $(".postulation-group").removeClass('active');
            $(".postulation-item").removeClass('active');
            $(this).addClass("active");

            $('.postulation-list').hide();
            $('.' + $(this).data('slug'))
                .removeClass('hide')
                .hide()
                .fadeIn('fast');

            $('.' + $(this).data('slug') + ' .postulation-item').removeClass('active').eq(0).click();
        });

        $(".postulation-item").click( function(){
            $(".postulation-item").removeClass('active');
            $(this).addClass("active");
            $('.postulation-desc').hide();
            $('.' + $(this).data('slug'))
                .removeClass('hide')
                .hide()
                .fadeIn('fast');
        });

    });
</script>  
