<div class="panel">
    <div class="col-md-12 tickets btn-custom">
    <?php echo messages();?>
    <?php if($rows):?>
        <table class="table">
            <thead>
                <tr>
                    <th><i class="ion-pound"></i></th>
                    <th><?php print locale('title');?></th>
                    <th><?php print locale('tag');?></th>
                    <th><?php print locale('replies');?></th>
                    <th><i class="ion-clock"></i></th>
                    <th><i class="ion-gear-a"></i></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($rows as $ticket):?>
                <tr>
                    <td><?php print $ticket->code;?></td>
                    <td><?php print $ticket->title;?></td>                    
                    <td><?php print locale($ticket->tag->tag);?></td>
                    <td><?php print timespan($ticket->created);?></td>
                    <td>
                        <a href="<?php print site_url('/account/tickets/' . $ticket->code);?>" class="btn btn-success" title="Ver"><i class="ion-eye"></i></a>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    <?php else:?>
        <div class="alert alert-success">
            <i class="ion-checkmark-circled"></i> <?php print locale('tickets_empty');?>
        </div>
        <button type="button" class="btn btn-success btn-create"><i class="ion-ios-bolt fa-x1"></i> <span><?php print locale('tickets_new')?></span></button>
        <div class="group-control">&nbsp;</div>
    <?php endif;?>
    </div><div class="clearfix"></div>
    <div class="col-md-12 ticket_form hide">
        <div class="alert alert-success">
            <h3><?php print locale('tickets_new')?> </h3>
            <div class="col-md-6">
                <div class="row">
                    <form class="form" action="<?php print site_url('/account/tickets/send');?>" enctype="multipart/form-data" method="post">
                        <div class="form-group">
                            <input class="form-control" name="title" value="" placeholder="<?php print locale('title');?>" required>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="tag_id" required>
                            <?php foreach($tags as $tag):?>
                                <option value="<?php print $tag->id;?>"><?php print locale($tag->tag);?></option>
                            <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="description" value="" placeholder="<?php print locale('description');?>" required></textarea>
                        </div>
                        <div class="form-group">
                            <input type="file" name="file" value="" placeholder="<?php print locale('tickets_attachment');?>">
                        </div>
                        <div class="alert message hide"></div>
                        <div class="form-group text-right btn-custom">
                            <button type="submit" class="btn btn-success"><i class="ion-ios-bolt fa-x1"></i> <span><?php print locale('tickets_new')?></span></button>
                        </div>
                    </form>
                </div>
            </div><div class="clearfix"></div>
        </div>
    </div><div class="clearfix"></div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){

        $(".back").click( function(){
            $('.ticket_form').fadeOut('fast',function(){
                $('.tickets').fadeIn('fast');
            });
        });

        $(".btn-create").click( function(){
            $('.tickets').fadeOut('fast',function(){
                $('.ticket_form')
                    .removeClass('hide')
                    .hide()
                    .fadeIn('fast');
            });
        });
    });
</script>        