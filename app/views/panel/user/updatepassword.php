<div class="bg-container">
    <div class="bg-content-wide">
        <div class="widget color red">
            <div class="database-migration">
                <h3><i class="fa fa-hand-o-right"></i> <?php print locale('update_password');?></h3>
                <hr/>
                <form class="form form-ajax" action="<?php print site_url('/updatepasswordcode');?>">
                    <input type="hidden" name="skiprequestpassword" value="<?php print $skiprequestpassword;?>"
                    <div class="group-control">&nbsp;</div>
                <?php if(!$skiprequestpassword):?>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="<?php print locale('password');?>">
                    </div>
                <?php endif;?>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password1" placeholder="<?php print locale('new_password');?>">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password2" placeholder="<?php print locale('confirm_password');?>">
                    </div>

                    <div class="clearfix"></div>
                    <div class="alert message hide"></div>
                    <div class="form-group text-right btn-custom">
                        <button type="submit" class="btn btn-success"><i class="ion-ios-bolt fa-x1"></i> <span><?php print locale('update_password');?></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
