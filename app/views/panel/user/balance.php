<div class="bg-container">
    <div class="bg-content-wide">

        <div class="widget color red">
            <h2 class="widget-title"><?php print locale('my_balance');?></h2>
            <div class="panel-function">
                <a title="" class="hide-btn"><i class="ti-arrow-circle-down"></i></a> 
            </div>
            <div class="widget-content">
                <p><?php print locale('my_balance_text');?></p>
                <table class="table translucid">
                    <tr>
                        <th><?php print locale('username');?></th>
                        <td><?php print session('login');?></td>
                    </tr>
                    <tr>
                        <th><strong><?php print locale('coins_balance');?></strong></th>
                        <td><?php print session('coins');?></td>
                    </tr>
                </table>
                <?php if( ! empty($orders_pending)):?>
                <p><?php print locale('you_have_orders_pending');?></p>
                <table class="table translucid btn-custom">
                    <tr>
                        <th><?php print locale('date');?></th>
                        <td><?php print locale('coins');?></td>
                        <td><?php print locale('amount');?></td>
                        <td></td>
                    </tr>
                    <?php foreach($orders_pending as $order):?>
                    <tr>
                        <th><?php print timespan($order->updated);?></th>
                        <td><?php print $order->coins;?></td>
                        <td>ARS<?php print monetize_coins($order->coins);?></td>
                        <td>
                            <a class="btn" href="<?php print site_url('/account/payment?ref=' . $order->reference);?>"><?php print locale('resume');?></a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </table>
                <?php endif;?>
            </div>
        </div>

        <div class="group-control">&nbsp;</div>

        <div class="form-group text-right btn-custom">
            <a href="<?php print site_url('/account/coins');?>" class="btn btn-success"><i class="ion-ios-bolt fa-x1"></i> <span><?php print locale('get_coins');?></span></a>
        </div>

    </div>
</div>