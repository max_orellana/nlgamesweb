<div class="ps-wrapper panel-container content-height">
    <div class="bg-container bg-forum darkfade">

        <header class="c-section_header section-<?php print session('group');?> bottom-margin">
            <h2 class="c-section_title u-text-gradient-<?php print session('group');?>"><?php print locale('forum_connection');?></h2>
        </header>  

        <div class="bg-content">

            <div class="opacity-box custom-pad">
            <?php if( strlen($row->forum_name)):?>
                <div class="text-center"> 
                    <h3 class="forum-title"><?php print session('forum_name');?></h3>
                    <p><?php print $row->forum_title;?></p>
                    <div class="avatar-frame flagless">
                <?php if(!empty(session('forum_avatar'))):?>
                        <img src="<?php print session('forum_avatar');?>" alt="" >
                <?php else:?>
                        <img src="<?php print site_asset('img/profile_default.png');?>">
                <?php endif;?>                      
                    </div>
                </div>
                <div class="group-control">&nbsp;</div>
                <div class="text-center"> 
                    <p><strong><?php print locale('forum_members_day_posts');?></strong>: <?php print (int) $row->forum_members_day_posts;?> <?php print locale('forum_members_day_posts_value');?></p>
                    <p><strong><?php print locale('last_acivity');?></strong>: <?php print timespan($row->forum_last_activity);?></p>
                    <p><strong><?php print locale('joined');?></strong>: <?php print timespan($row->forum_joined);?></p>
                    <p><strong><?php print locale('timezone');?></strong>: <?php print $row->forum_timezone;?></p>
                </div>
                <div class="group-control">&nbsp;</div>
                <form class="form form-ajax" action="<?php print site_url('/account/forum/disconnect');?>" method="post">
                    <?php echo messages();?>
                    <div class="form-group text-center">
                        <div class="special-button-container">
                            <button type="submit" class="special-button"><span><?php print locale('disconnect');?></span></button>
                        </div>
                    </div>
                </form>

                <?php else:?>

                <h3><?php print locale('forum_unconnected_title');?></h3>
                <p><?php print locale('forum_unconnected_msg');?></p>
                <div class="group-control">&nbsp;</div>
                <form class="form form-ajax" action="<?php print site_url('/account/forum/connect');?>" method="post">
                    <?php echo messages();?>
                    <div class="form-group">
                        <input type="text" class="form-control" name="login" value="" placeholder="<?php print locale('username');?>" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" value="" placeholder="<?php print locale('password');?>" required>
                    </div>
                    <div class="form-group text-center">
                        <div class="special-button-container">
                            <button type="submit" class="special-button"><span><?php print locale('connect');?></span></button>
                        </div>
                    </div>
                </form>

            <?php endif;?>
            </div>
        </div>
    </div>
</div>