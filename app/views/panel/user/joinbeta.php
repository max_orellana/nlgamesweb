<div class="bg-container bg-profile darkfade">
    <header class="c-section_header secxtion-<?php print session('group');?> bottom-margin">
        <h2 class="c-section_title u-text-gradient-<?php print session('group');?>"><?php print locale('wannajoinbeta');?></h2>
    </header>
    <div class="bd-box btn-custom">
        <?php echo messages();?>
    </div>    
    <script>
        $(function(){
            modal_click('wannajoinbeta');
        });
    </script>
</div>