<div class="ps-wrapper panel-container">
	<div class="bg-container bg-<?php print session('group');?> darkfade">
	    <header class="c-section_header section-<?php print session('group');?> bottom-margin">
	        <h2 class="c-section_title u-text-gradient-<?php print session('group');?>"><?php print locale('orders');?></h2>
	    </header>

		<div class="bd-box btn-custom">
			<?php echo messages();?>
			<a href="<?php print site_url('/admin/orders/create');?>" class="btn btn-success"><i class="ion-compose"></i> <span><?php print locale('add');?></span></a>

			<div class="group-control">&nbsp;</div>

		<?php if(count($entries)):?>
			<?php echo search('id,title');?>
			<table class="table table-<?php print session('group');?>">
				<thead>
					<tr>
						<th><?php print locale('id');?></th>
						<th><?php print locale('login');?></th>
						<th><?php print locale('amount');?></th>
						<th><?php print locale('status');?></th>
						<th><?php print locale('date');?></th>
						<th><i class="ion-gear-a"></i></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($entries as $entry):?>
					<tr>
						<td><?php print $entry->id;?></td>
						<td><?php print $entry->login;?></td>
						<td><?php print intval($entry->amount);?></td>
						<td><?php print $entry->status;?></td>
						<td><?php print timespan($entry->created);?></td>
						<td>
							<!--form action="<?php print site_url('/admin/orders/delete/' . $entry->id);?>" method="post" class="btn-custom">
							<button type="submit" class="btn btn-danger" onclick="if(!confirm('Your are about to delete this post, are you sure?')) return false;"><i class="ion-close-circled"></i></button>
							</form-->
							<a href="<?php print site_url('/admin/orders/show/' . $entry->id);?>" class="btn btn-success"><i class="fa fa-pdf-o"></i></a>
						</td>
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
			<?php $entries[0]->paginator();?>
			<?php endif;?>
		</div><div class="clearfix"></div>
	</div>
</div>