<div class="ps-wrapper panel-container container-height">
	<div class="panel">
		<div class="col-md-12">
			<h3>&nbsp;<i class="ion-edit"></i> <?php echo $entry->title;?></h3>
			<input type="hidden" name="id" value="<?php echo $entry->id;?>">
			<form class="form form-ajax" id="post" action="<?php print site_url('/admin/heroes/update/' . $entry->id);?>">
				<div class="form-group">
					<input type="text" class="form-control" name="title" placeholder="<?php print locale('title');?>" value="<?php echo $entry->title;?>" required>
				</div>

				<div class="form-group">
					<input type="text" class="form-control" name="slug" placeholder="<?php print locale('slug');?>" value="<?php echo $entry->slug;?>" required>
				</div>

				<div class="form-group">
					<textarea class="form-control" name="caption" placeholder="<?php print locale('caption');?>"><?php echo $entry->caption;?></textarea>
				</div>

				<div class="form-group">
					<input class="form-control" type="number" name="damage" placeholder="<?php print locale('damage');?>" value="<?php echo $entry->damage;?>">
				</div>

				<div class="form-group">
					<input class="form-control" type="number" name="armor" placeholder="<?php print locale('armor');?>" value="<?php echo $entry->armor;?>">
				</div>

				<div class="form-group">
					<input class="form-control" type="number" name="agility" placeholder="<?php print locale('agility');?>" value="<?php echo $entry->agility;?>">
				</div>

				<div class="form-group">
					<input class="form-control" type="number" name="utility" placeholder="<?php print locale('utility');?>" value="<?php echo $entry->utility;?>">
				</div>

				<div class="form-group">
					<input class="form-control" type="number" name="speed" placeholder="<?php print locale('speed');?>" value="<?php echo $entry->speed;?>">
				</div>

				<div class="form-group">
					<input class="form-control" type="number" name="difficulty" placeholder="<?php print locale('difficulty');?>" value="<?php echo $entry->difficulty;?>">
				</div>

				<div class="form-group text-center btn-custom">
					<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
				</div>
			</form>
		</div><div class="clearfix"></div>
	</div>
</div>