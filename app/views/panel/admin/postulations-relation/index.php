<div class="ps-wrapper panel-container">
	<div class="bg-container bg-<?php print session('group');?> darkfade">
	    <header class="c-section_header section-<?php print session('group');?> bottom-margin">
	        <h2 class="c-section_title u-text-gradient-<?php print session('group');?>"><?php print locale('user-postulations');?></h2>
	    </header>

		<div class="bd-box btn-custom">
			<?php echo messages();?>

			<?php if(count($entries)):?>
				<?php echo search('id,login');?>
				<table class="table table-<?php print session('group');?>">
					<thead>
						<tr>
							<th><?php print locale('user');?></th>
							<th><?php print locale('postulation');?></th>
							<th><?php print locale('status');?></th>						
							<th><?php print locale('attachment');?></th>
							<th><?php print locale('modified');?></th>
							<th><i class="ion-gear-a"></i></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($entries as $entry):?>
						<tr>	
							<td><?php echo $entry->login;?></td>
							<td><?php echo strlen($entry->postulation->slug) ? locale('postulations_' . $entry->postulation->slug) : '?';?></td>
							<td><?php print locale('postulations-status-' . $entry->status);?></td>
							<td><?php print ( is_file( SP . 'public/upload/postulations/' . $entry->file) ? '<i class="ion-paperclip"></i>':'<i class="ion-close"></i>');?></td>
							<td>
								<i class="ion-clock"> <?php echo timespan($entry->updated);?></i>
							</td>
							<td>
								<form action="<?php print site_url('/admin/postulations-relation/delete/' . $entry->id);?>" method="post">
									<a href="<?php print site_url('/admin/postulations-relation/' . $entry->id);?>" class="btn btn-success"><i class="ion-compose"></i></a>
									<button type="submit" class="btn btn-danger" onclick="if(!confirm('Your are about to delete this post, are you sure?')) return false;"><i class="ion-close-circled"></i></button>
								</form>
							</td>
						</tr>
					<?php endforeach;?>
					</tbody>
				</table>
			</div>
			<?php $entries[0]->paginator();?>
			<?php endif;?>
		</div><div class="clearfix"></div>
	</div>
</div>