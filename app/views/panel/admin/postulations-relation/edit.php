<div class="ps-wrapper panel-container">
	<div class="panel">
		<div class="col-md-9">
			<h3>&nbsp;<i class="ion-edit"></i> <?php echo $entry->login;?></h3>
			<input type="hidden" name="id" value="<?php echo $entry->id;?>">
			<form class="form form-ajax" id="post" action="<?php print site_url('/admin/postulations-relation/update/' . $entry->id);?>">
				<div class="radio">
					<label>
				    	<input type="radio" id="status1" name="status" value="1"<?php echo ($entry->status == 1 OR ! $entry->status) ? ' checked' : '';?>><?php print locale('postulations-status-1');?>
				    </label>&nbsp;&nbsp;
					<label>
				    	<input type="radio" id="status2" name="status" value="2"<?php echo $entry->status == 2 ? ' checked' : '';?>><?php print locale('postulations-status-2');?>
				    </label>&nbsp;&nbsp;
					<label>
				    	<input type="radio" id="status3" name="status" value="3"<?php echo $entry->status == 3 ? ' checked' : '';?>><?php print locale('postulations-status-3');?>
				    </label>&nbsp;&nbsp;
					<label>
				    	<input type="radio" id="status4" name="status" value="4"<?php echo $entry->status == 4 ? ' checked' : '';?>><?php print locale('postulations-status-4');?>
				    </label>&nbsp;&nbsp;
					<label>
				    	<input type="radio" id="status5" name="status" value="5"<?php echo $entry->status == 5 ? ' checked' : '';?>><?php print locale('postulations-status-5');?>
				    </label>
				</div>
				<div class="clearfix"></div>

		        <div class="form-group">
		              <div class='input-group date datetimepicker'>
		                  <input type='text' class="form-control" name="interview_schedule" data-date-format="YYYY-MM-DD HH:mm:ss" value="<?php print ($entry->interview_schedule == '0000-00-00 00:00:00' ? '' : $entry->interview_schedule);?>" />
		                  <span class="input-group-addon"><span class="fa fa-calendar"></span>
		                  </span>
		              </div>
		          </div>

				<div class="clearfix"></div>
				<div class="control-group">&nbsp;</div>
				<div class="form-group">
				   	<label class="control-label" for="inputError2"><?php echo locale('postulations_fullname');?></label>
					<input type="text" class="form-control disabled" name="fullname" placeholder="" value="<?php echo $entry->fullname;?>" disabled>
				</div>

				<div class="form-group">
			   		<label class="control-label" for="inputError2"><?php echo locale('postulations_age');?></label>			
					<input type="text" class="form-control disabled" name="age" placeholder="" value="<?php echo $entry->age;?>" disabled>
				</div>

				<div class="form-group">
			   		<label class="control-label" for="inputError2"><?php echo locale('postulations_residence');?></label>
					<input type="text" class="form-control disabled" name="residence" placeholder="" value="<?php echo $entry->residence;?>" disabled>
				</div>

				<div class="form-group">
				   	<label class="control-label" for="inputError2"><?php echo locale('postulations_languages');?></label>
					<input type="text" class="form-control disabled" name="languages" placeholder="" value="<?php echo $entry->languages;?>" disabled>
				</div>

				<div class="form-group">
				   	<label class="control-label" for="inputError2"><?php echo locale('postulations_availability');?></label>
					<input type="text" class="form-control disabled" name="availability" placeholder="" value="<?php echo $entry->availability;?>" disabled>
				</div>


				<div class="form-group">
				   	<label class="control-label" for="inputError2"><?php echo locale('skype');?></label>
					<textarea class="form-control disabled" name="skype" placeholder="" disabled><?php echo $entry->skype;?></textarea>
				</div>

				<hr>
				<div class="form-group">
				   	<label class="control-label" for="inputError2"><?php echo locale('postulations_medium');?></label>
					<textarea class="form-control disabled" name="medium" placeholder="" disabled><?php echo $entry->experience;?></textarea>
				</div>

				<div class="form-group">
				   	<label class="control-label" for="inputError2"><?php echo locale('postulations_medium_reach');?></label>
					<textarea class="form-control disabled" name="medium_reach" placeholder="" disabled><?php echo $entry->medium_reach;?></textarea>
				</div>

				<hr>

				<div class="form-group">
				   	<label class="control-label" for="inputError2"><?php echo locale('postulations_experience');?></label>
					<textarea class="form-control disabled" name="experience" placeholder="" disabled><?php echo $entry->experience;?></textarea>
				</div>

				<div class="form-group">
				   	<label class="control-label" for="inputError2"><?php echo locale('postulations_message');?></label>			
					<textarea class="form-control disabled" name="message" placeholder="" disabled><?php echo $entry->message;?></textarea>
				</div>
				<div class="form-group">
			<?php if( strlen($entry->file) AND is_file( SP . 'public/upload/postulations/' . $entry->file)):?>
					<a href="<?php print site_url('/upload/postulations/' . $entry->file);?>" target="_blank"><?php print $entry->file;?></a></span>
				<?php else:?>
					<span><?php print locale('no_attachments');?></span>
			<?php endif;?>
				</div>
				<div class="form-group text-center btn-custom">
					<a href="<?php print site_url('/admin/postulations-relation');?>" class="btn btn-primary"> <i class="ion-backspace"></i> <span><?php print locale('back');?></span> </a>
					<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
				</div><div class="clearfix"></div>
			</form>
		</div><div class="clearfix"></div>
	</div>
</div>