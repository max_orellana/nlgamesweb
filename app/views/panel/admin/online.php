<div class="ps-wrapper panel-container container-height">
	<div class="panel">
		<div class="col-md-12">
		<?php if($prep):?>
		<div class="alert alert-success"><?php print locale('you_are_the_only_one');?></div>
		<?php endif;?>
		<?php if($entries):?>
			<table class="table">
				<thead>
					<tr>
						<th width="66"><i class="ion-eye"></i></th>
						<th><i class="ion-images"></i></th>
						<th><i class="ion-images"></i></th>
						<th><i class="ion-cog"></i></th>					
					</tr>
				</thead>
				<tbody>
				<?php foreach($entries as $entry):?>
					<tr>
						<td>
					    <?php if(!empty($entry->account->forum_avatar)):?>
					    	<img src="<?php print $entry->account->forum_avatar;?>" alt="" width="50" height="50">
				        <?php else:?>
					    	<img src="<?php print site_asset('img/profile_default.png');?>" alt="" width="50" height="50">
				        <?php endif;?>	
						</td>
						<td><?php print $entry->login;?></td>
						<td><?php print (!empty($entry->account->last_login)?timespan($entry->account->last_login):'?');?></td>
						<td><a class="btn" href="<?php print site_url('/account/inbox/compose?r=' . $entry->login);?>" title="<?php print locale('send_message');?>"><i class="fa fa-envelope"></i></a></td>
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
		<?php endif;?>
		</div><div class="clearfix"></div>
	</div>
</div>