<div class="panel">
	<div class="col-md-12">
		<form class="form form-ajax" id="post" action="<?php print site_url('/admin/coupons/update/0');?>">

			<div class="form-group" style="position:relative">
				<label class="label-control"><?php echo locale('recipients');?></label>
				<input type="text" class="form-control" name="recipients_input" placeholder="<?php print locale('recipients');?>" value="" required>
				<div class="recipients-drop drop-list"></div>
				<div class="recipients-list alert alert-success hide"></div>
			</div>

            <div class="form-group">
				<label class="label-control"><?php echo locale('coupon_type');?></label>
                <select class="form-control" name="type_id" required>
                <?php foreach($types as $type):?>
                    <option value="<?php print $type->id;?>"><?php print locale($type->slug);?></option>
                <?php endforeach;?>
                </select>
            </div>

			<div class="form-group">
				<label class="label-control"><?php echo locale('quantity');?></label>
				<input type="number" class="form-control" name="count" placeholder="<?php print locale('count');?>" value="1" title="<?php echo locale('quantity');?>" required>
			</div>

			<div class="form-group">
				<textarea class="form-control" name="caption" placeholder="<?php print locale('caption');?>"></textarea>
			</div>

			<div class="form-group text-center btn-custom">
				<a href="<?php print site_url('/admin/coupons');?>" class="btn btn-primary"> <i class="ion-backspace"></i> <span><?php print locale('back');?></span> </a>
				<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
			</div>
		</form>
	</div><div class="clearfix"></div>
</div>

<script type="text/javascript">

	$(document).on('click','.recipient',function(){
		$('.form').append('<input type="hidden" name="recipients[]" value="' + $(this).data('text') + '">');
		$('.recipients-list')
			.append('<i class="ti-user"></i> ' + $(this).data('text')  + ' &nbsp;&nbsp;&nbsp;')
			.removeClass('hide')
			.show();
	});

    jQuery(document).ready(function(){

		$('input[name="recipients_input"]').ajaxify({
		  url: '/account/inbox/recipients',
		  focus: false,
		  success: function(json){
		  	var str='<ul>';
		  	$(json).each(function(i,row){
		  		str+='<li><a class="recipient" data-text="' + row.title + '" href="javascript:void(0)"><span><img src="' + row.avatar + '"></span><i>' + row.title + '</i></a></li>';
		  	});
		  	str+='</ul>';
		    $('.recipients-drop')
		    	.html(str)
		    	.show();
		  }
		});
    });
</script>  
