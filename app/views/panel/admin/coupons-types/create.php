<div class="panel">
	<div class="col-md-12">
		<form class="form form-ajax" id="post" action="<?php print site_url('/admin/coupons-types/update/0');?>">
			<div class="form-group">
				<input type="text" class="form-control" name="slug" placeholder="<?php print locale('slug');?>" value="" required>
			</div>

			<div class="form-group">
				<label class="label-control"><?php echo locale('coupon_type');?></label>
				<textarea class="form-control" name="caption" placeholder="<?php print locale('caption');?>"></textarea>
			</div>

			<div class="form-group text-center btn-custom">
				<a href="<?php print site_url('/admin/coupons-types');?>" class="btn btn-primary"> <i class="ion-backspace"></i> <span><?php print locale('back');?></span> </a>
				<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
			</div>
		</form>
	</div><div class="clearfix"></div>
</div>
