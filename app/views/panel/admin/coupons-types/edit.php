<div class="panel">
	<div class="col-md-12">
		<h3>&nbsp;<i class="ion-edit"></i> <?php print $entry->slug;?></h3>
		<input type="hidden" name="id" value="<?php echo $entry->id;?>">
		<form class="form form-ajax" id="post" action="<?php print site_url('/admin/coupons-types/update/' . $entry->id);?>">
			<div class="form-group">
				<input type="text" class="form-control" name="slug" placeholder="<?php print locale('slug');?>" value="<?php print $entry->slug;?>" required>
			</div>

			<div class="form-group">
				<textarea class="form-control" name="caption" placeholder="<?php print locale('caption');?>"><?php print $entry->caption;?></textarea>
			</div>

			<div class="form-group text-center btn-custom">
				<a href="<?php print site_url('/admin/coupons-types');?>" class="btn btn-primary"> <i class="ion-backspace"></i> <span><?php print locale('back');?></span> </a>
				<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
			</div>
		</form>
	</div><div class="clearfix"></div>
</div>
