<div class="panel">
	<div class="col-md-12 btn-custom">
	<?php echo messages();?>
	<?php if(count($entries)):?>
		<?php echo search('id,word_key,word_es,word_en');?>
		<a href="<?php print site_url('/admin/coupons-types/create');?>" class="btn btn-success"><i class="ion-compose"></i> <span><?php print locale('add');?></span></a>
		<table class="table">
			<thead>
				<tr>
					<th><i class="ion-eye"></i></th>
					<th><i class="ion-images"></i></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($entries as $entry):?>
				<tr>
					<td><?php print $entry->slug;?></td>
					<td>
						<form action="<?php print site_url('/admin/coupons-types/delete/' . $entry->id);?>;?>" method="post">
						<button type="submit" class="btn btn-danger" onclick="if(!confirm('Your are about to delete this post, are you sure?')) return false;" title="Delete"><i class="ion-close-circled"></i></button>
						<a href="<?php print site_url('/admin/coupons-types/' . $entry->id);?>" class="btn btn-success" title="Edit"><i class="ion-edit"></i></a>
						</form>
					</td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
		<?php $entries[0]->paginator();?>
		<?php else:?>
			<a href="<?php print site_url('/admin/coupons-types/create');?>" class="btn btn-success"><i class="ion-battery-empty"></i> <span>coupons-types badge is empty! You may create one.</span></a>
		<?php endif;?>
	</div><div class="clearfix"></div>
</div>