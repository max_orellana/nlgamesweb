<div class="ps-wrapper panel-container container-height">
    <div class="bg-container bg-admin darkfade">
        <header class="c-section_header section-admin bottom-margin">
            <h2 class="c-section_title u-text-gradient-<?php print session('group');?>"><?php print locale('dashboard');?></h2>
        </header>
         
        <div class="bd-box text-center">
            <form class="form form-ajax" action="<?php print site_url('/admin/alert');?>" method="post">
                <input type="hidden" name="code" value="info">
                <input class="btn btn-info" type="submit" value="info">
            </form><hr>
            <form class="form form-ajax" action="<?php print site_url('/admin/alert');?>" method="post">
                <input type="hidden" name="code" value="warning">
                <input class="btn btn-warning" type="submit" value="warning">
            </form><hr>
            <form class="form form-ajax" action="<?php print site_url('/admin/alert');?>" method="post">
                <input type="hidden" name="code" value="success">
                <input class="btn btn-success" type="submit" value="success">
            </form><hr>
            <form class="form form-ajax" action="<?php print site_url('/admin/alert');?>" method="post">
                <input type="hidden" name="code" value="danger">
                <input class="btn btn-danger" type="submit" value="danger">
            </form>
            
            <div class="clear"></div>
        </div>
    </div>
</div>