<div class="ps-wrapper panel-container">
	<div class="bg-container bg-<?php print session('group');?> darkfade">
	    <header class="c-section_header section-<?php print session('group');?> bottom-margin">
	        <h2 class="c-section_title u-text-gradient-<?php print session('group');?>"><?php print locale('postulations');?></h2>
	    </header>

		<div class="bd-box btn-custom">
			<?php echo messages();?>

			<a href="<?php print site_url('/admin/postulations/create');?>" class="btn btn-success"><i class="ion-compose"></i> <span><?php print locale('add');?></span></a>
			<div class="group-control">&nbsp;</div>
			<?php if(count($entries)):?>
				<?php echo search('id,slug');?>
				<table class="table table-<?php print session('group');?>">
					<thead>
						<tr>
							<th><?php print locale('title');?></th>
							<th><?php print locale('group');?></th>
							<th><?php print locale('availability');?></th>						
							<th><?php print locale('modified');?></th>
							<th><i class="ion-gear-a"></i></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($entries as $entry):?>
						<tr>	
							<td><?php print locale('postulations_' . $entry->slug);?></td>
							<td><?php print locale('postulations_' . $entry->group->slug);?></td>
							<td><?php print $applicants[$entry->id];?> / <?php print $entry->vacancies;?></td>
							<td>
								<!--i class="ion-calendar"> <?php echo timespan($entry->created);?></i-->
								<i class="ion-clock"> <?php echo timespan($entry->updated);?></i>
							</td>
							<td>
								<form action="<?php print site_url('/admin/postulations/delete/' . $entry->id);?>" method="post">
								<a href="<?php print site_url('/admin/postulations/' . $entry->id);?>" class="btn btn-success"><i class="ion-compose"></i></a>
								<button type="submit" class="btn btn-danger" onclick="if(!confirm('Your are about to delete this post, are you sure?')) return false;"><i class="ion-close-circled"></i></button>
								</form>
							</td>
						</tr>
					<?php endforeach;?>
					</tbody>
				</table>
			</div>
			<?php $entries[0]->paginator();?>
			<?php endif;?>
		</div><div class="clearfix"></div>
	</div>
</div>