<div class="ps-wrapper panel-container">
	<div class="panel">
		<div class="col-md-9">
			<h3>&nbsp;<i class="ion-edit"></i> <?php print locale('postulations_' . $entry->slug);?></h3>
			<input type="hidden" name="id" value="<?php echo $entry->id;?>">
			<form class="form form-ajax" id="post" action="<?php print site_url('/admin/postulations/update/' . $entry->id);?>">

				<div class="form-group">
					<label class="control-label"><?php echo locale('slug');?></label>
					<input type="text" class="form-control" name="slug" placeholder="" value="<?php echo $entry->slug;?>">
				</div>

			<?php foreach(config()->languages as $language):?>
				<div class="form-group">
					<label class="control-label"><?php echo locale('title');?> <?php echo locale($language);?></label>
					<input type="text" class="form-control" name="title_<?php echo $language;?>" placeholder="<?php echo locale($language);?>" value="<?php print locale('postulations_' . $entry->slug,$language);?>">
				</div>
			<?php endforeach;?>			

	            <div class="form-group">
					<label class="control-label"><?php echo locale('postulations_group');?></label>            
	                <select class="form-control" name="group_id" required>
	                <?php foreach($groups as $group):?>
	                    <option value="<?php print $group->id;?>"<?php print $group->id == $entry->group_id ? ' selected' : '' ;?>><?php print locale($group->slug);?></option>
	                <?php endforeach;?>
	                </select>
	            </div>

				<div class="form-group">
					<label class="control-label"><?php echo locale('postulations_vacancies');?></label>
					<input type="number" class="form-control" name="vacancies" placeholder="" value="<?php echo $entry->vacancies;?>">
				</div>

				<div class="form-group">
					<label class="control-label"><?php echo locale('postulations_base');?></label>
					<input type="number" class="form-control" name="base" placeholder="" value="<?php echo $entry->base;?>">
				</div>

			<?php foreach(config()->languages as $language):?>
				<div class="form-group">
					<label class="control-label"><?php echo locale($language);?></label>
					<textarea class="form-control" name="description_<?php echo $language;?>" placeholder="<?php echo locale($language);?>"><?php print locale('postulations_' . $entry->slug . '_text',$language);?></textarea>
				</div>
			<?php endforeach;?>
				<div class="form-group text-center btn-custom">
					<a href="<?php print site_url('/admin/postulations');?>" class="btn btn-primary"> <i class="ion-backspace"></i> <span><?php print locale('back');?></span> </a>
					<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
				</div><div class="clearfix"></div>
			</form>

		</div><div class="clearfix"></div>
	</div>
</div>