<div class="panel">
	<div class="col-md-12">
		<h3>&nbsp;<i class="ion-compose"></i> <?php print locale('create');?> </h3>

		<form class="form form-ajax" action="<?php print site_url('/admin/items/update/0');?>" enctype="multipart/form-data" method="post">
			<div class="form-group">
				<label class="control-label"><?php echo locale('title');?></label>            
				<input type="text" class="form-control" name="title" placeholder="<?php print locale('title');?>" value="" required>
			</div>

            <div class="form-group">
				<label class="control-label"><?php echo locale('type');?></label>            
                <select class="form-control" name="type_id" required>
                <?php foreach($types as $type):?>
                    <option value="<?php print $type->id;?>"><?php print locale($type->slug);?></option>
                <?php endforeach;?>
                </select>
            </div>

            <div class="form-group">
				<label class="control-label"><?php echo locale('grade');?></label>            
                <select class="form-control" name="grade_id" required>
                <?php foreach($grades as $grade):?>
                    <option value="<?php print $grade->id;?>"><?php print locale($grade->slug);?></option>
                <?php endforeach;?>
                </select>
            </div>

            <div class="form-group">
				<label class="control-label"><?php echo locale('class');?></label>            
                <select class="form-control" name="class_id" required>
                <?php foreach($classes as $class):?>
                    <option value="<?php print $class->id;?>"><?php print locale($class->slug);?></option>
                <?php endforeach;?>
                </select>
            </div>

			<div class="form-group">
				<label class="control-label"><?php echo locale('price');?></label>            
				<input type="number" class="form-control" name="price" placeholder="<?php print locale('price');?>" value="" required>
			</div>

			<div class="form-group">
				<label class="control-label"><?php echo locale('game_id');?></label>            
				<input type="number" class="form-control" name="game_id" placeholder="<?php print locale('game_id');?>" value="" required>
			</div>

			<div class="form-group">
				<label class="control-label"><?php echo locale('original_skin');?></label>            
				<input type="text" class="form-control" name="original_skin" placeholder="<?php print locale('game_id');?>" value="" required>
			</div>

	        <div class="form-group">
				<label class="control-label"><?php echo locale('image');?></label>            	        
	            <input type="file" name="image" value="">
	        </div>

	        <div class="form-group">
				<label class="control-label"><?php echo locale('video');?></label>            	        
	            <input type="file" name="video" value="">
	        </div>

			<div class="form-group text-center btn-custom">
				<a href="<?php print site_url('/admin/items');?>" class="btn btn-primary"> <i class="ion-backspace"></i> <span><?php print locale('back');?></span> </a>
				<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
			</div>
		</form>
	</div><div class="clearfix"></div>
</div>
