<div class="panel">
	<div class="col-md-12 btn-custom">
	<?php echo messages();?>
	<a href="<?php print site_url('/admin/items/create');?>" class="btn btn-success"><i class="ion-compose"></i> <span><?php print locale('create');?></span></a>	
	<hr>
	<?php if(count($entries)):?>
		<?php echo search('id,title');?>
		<table class="table">
			<thead>
				<tr>
					<th><i class="ion-eye"></i></th>
					<th><i class="ion-clock"></i> <?php print locale('type');?></th>
					<th><i class="ion-clock"></i> <?php print locale('grade');?></th>
					<th><i class="ion-clock"></i> <?php print locale('class');?></th>
					<th><i class="ion-gear-a"></i></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($entries as $entry):?>
				<tr>
					<td><?php print $entry->title;?></td>
					<td><?php print locale($entry->type->slug);?></td>
					<td><?php print locale($entry->grade->slug);?></td>
					<td><?php print locale($entry->class->slug);?></td>
					<td>
						<form action="<?php print site_url('/admin/items/delete/' . $entry->id);?>" method="post">
							<button type="submit" class="btn btn-danger" onclick="if(!confirm('Your are about to delete this post, are you sure?')) return false;" title="Delete"><i class="ion-close-circled"></i></button>
							<a href="<?php print site_url('/admin/items/' . $entry->id);?>" class="btn btn-success" title="Edit"><i class="ion-edit"></i></a>
						</form>
					</td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
		<?php $entries[0]->paginator();?>
		<?php endif;?>
	</div><div class="clearfix"></div>
</div>