<div class="panel">
	<div class="col-md-12">
		<h3>&nbsp;<i class="ion-edit"></i> <?php print $entry->title;?></h3>
		<input type="hidden" name="id" value="<?php echo $entry->id;?>">
		<form class="form form-ajax" action="<?php print site_url('/admin/items/update/' . $entry->id);?>" enctype="multipart/form-data" method="post">
			<div class="form-group">
				<label class="control-label"><?php echo locale('title');?></label>            
				<input type="text" class="form-control" name="title" placeholder="<?php print locale('slug');?>" value="<?php print $entry->title;?>" required>
			</div>

            <div class="form-group">
				<label class="control-label"><?php echo locale('type');?></label>            
                <select class="form-control" name="type_id" required>
                <?php foreach($types as $type):?>
                    <option value="<?php print $type->id;?>"<?php print $type->id == $entry->type_id ? ' selected' : '' ;?>><?php print locale($type->slug);?></option>
                <?php endforeach;?>
                </select>
            </div>

            <div class="form-group">
				<label class="control-label"><?php echo locale('grade');?></label>            
                <select class="form-control" name="grade_id" required>
                <?php foreach($grades as $grade):?>
                    <option value="<?php print $grade->id;?>"<?php print $grade->id == $entry->grade_id ? ' selected' : '' ;?>><?php print locale($grade->slug);?></option>
                <?php endforeach;?>
                </select>
            </div>

            <div class="form-group">
				<label class="control-label"><?php echo locale('class');?></label>            
                <select class="form-control" name="class_id" required>
                <?php foreach($classes as $class):?>
                    <option value="<?php print $class->id;?>"<?php print $class->id == $entry->class_id ? ' selected' : '' ;?>><?php print locale($class->slug);?></option>
                <?php endforeach;?>
                </select>
            </div>

			<div class="form-group">
				<label class="control-label"><?php echo locale('price');?></label>            
				<input type="number" class="form-control" name="price" placeholder="<?php print locale('price');?>" value="<?php print $entry->price;?>" required>
			</div>

			<div class="form-group">
				<label class="control-label"><?php echo locale('game_id');?></label>            
				<input type="number" class="form-control" name="game_id" placeholder="<?php print locale('game_id');?>" value="<?php print $entry->game_id;?>" required>
			</div>

			<div class="form-group">
				<label class="control-label"><?php echo locale('original_skin');?></label>            
				<input type="text" class="form-control" name="original_skin" placeholder="<?php print locale('game_id');?>" value="<?php print $entry->original_skin;?>" required>
			</div>

	        <div class="form-group">
				<label class="control-label"><?php echo locale('image');?></label>            
	            <input type="file" name="image" value="<?php print $entry->image;?>">
				<?php if(strlen($entry->image)):?>
				  	<img src="<?php print '/upload/items/' . $entry->image;?>"  width="100" height="100">
				<?php endif;?>
	        </div>

	        <div class="form-group">
				<label class="control-label"><?php echo locale('video');?></label>            
	            <input type="file" name="video" value="<?php print $entry->video;?>">
				<?php if(strlen($entry->video)):?>
					<video autoplay loop width="100" height="100">
					  <source src="<?php print '/upload/items/' . $entry->video;?>" type="video/webm">Your browser does not support the video tag.
					</video>
				<?php endif;?>
	        </div>
        
			<div class="form-group text-center btn-custom">
				<a href="<?php print site_url('/admin/items');?>" class="btn btn-primary"> <i class="ion-backspace"></i> <span><?php print locale('back');?></span> </a>
				<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
			</div>
		</form>
	</div><div class="clearfix"></div>
</div>
