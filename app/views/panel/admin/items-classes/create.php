<div class="panel">
	<div class="col-md-12">
		<h3>&nbsp;<i class="ion-compose"></i> <?php print locale('create');?> </h3>

		<form class="form form-ajax" action="<?php print site_url('/admin/items-classes/update/0');?>">
			<div class="form-group">
				<label class="control-label"><?php echo locale('slug');?></label>            
				<input type="text" class="form-control" name="slug" placeholder="<?php print locale('slug');?>" value="" required>
			</div>

			<hr>
			
			<div class="form-group">
				<label class="control-label"><?php echo locale('es');?></label>            
				<input type="text" class="form-control" name="title_es" placeholder="<?php print locale('es');?>" value="" required>
			</div>

			<div class="form-group">
				<label class="control-label"><?php echo locale('en');?></label>            
				<input type="text" class="form-control" name="title_en" placeholder="<?php print locale('en');?>" value="" required>
			</div>

			<div class="form-group text-center btn-custom">
				<a href="<?php print site_url('/admin/items-classes');?>" class="btn btn-primary"> <i class="ion-backspace"></i> <span><?php print locale('back');?></span> </a>
				<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
			</div>
		</form>
	</div><div class="clearfix"></div>
</div>
