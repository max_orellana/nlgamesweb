<div class="ps-wrapper panel-container container-height">
	<div class="panel">
		<div class="col-md-12">
			<h3>&nbsp;<i class="ion-edit"></i> <?php echo $entry->title;?></h3>
			<input type="hidden" name="id" value="<?php echo $entry->id;?>">
			<form class="form form-ajax" id="post" action="<?php print site_url('/admin/tasks/update/' . $entry->id);?>">
				<input type="hidden" name="controller" value="redline">
				<input type="hidden" name="developer" value="developer">

				<div class="form-group">
					<label for="type1"><?php print locale('task-bug');?></label>
					<input type="radio" id="type1" class="form-radio" name="type" value="bug"<?php print ($entry->type == 'bug' ? ' checked':'');?>>
					<label for="type2"><?php print locale('task-issue');?></label>
					<input type="radio" id="type2" class="form-radio" name="type" value="issue"<?php print ($entry->type == 'issue' ? ' checked':'');?>>
				</div>
				<div class="form-group">
					<label for="urgent"><?php print locale('urgent');?></label>
					<input type="checkbox" id="urgent" name="urgent" class="form-radio" name="urgent" value="1">
				</div>

				<div class="form-group">
					<input type="text" class="form-control" name="title" placeholder="<?php print locale('title');?>" value="<?php echo $entry->title;?>" required>
				</div>

				<div class="form-group">
					<textarea class="form-control" name="caption" placeholder="<?php print locale('caption');?>"><?php echo $entry->caption;?></textarea>
				</div>

		        <div class="form-group">
		              <div class='input-group date datetimepicker'>
		                  <input type='text' class="form-control" name="due" data-date-format="YYYY-MM-DD hh:mm:ss" value="<?php print (empty($entry->due) ? date('Y-m-d H:i:s') : $entry->due);?>" />
		                  <span class="input-group-addon"><span class="fa fa-calendar"></span>
		                  </span>
		              </div>
		          </div>

				<div class="form-group text-center btn-custom">
					<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
				</div>
			</form>
		</div><div class="clearfix"></div>
	</div>
</div>