<div class="ps-wrapper panel-container">
	<div class="bg-container bg-<?php print session('group');?> darkfade">
	    <header class="c-section_header section-<?php print session('group');?> bottom-margin">
	        <h2 class="c-section_title u-text-gradient-<?php print session('group');?>"><?php print locale('advertisers');?>: <?php print $entry->login;?></h2>
	    </header>	

		<div class="bd-box btn-custom">
			<?php echo messages();?>
			<a href="<?php print site_url('/admin/advertisers/create');?>" class="btn btn-success"><i class="ion-compose"></i> <span><?php print locale('add');?></span></a>

			<div class="group-control">&nbsp;</div>

		<?php if(count($orders)):?>
			<?php echo search('id,title');?>
			<table class="table table-<?php print session('group');?>">
				<thead>
					<tr>
						<th><?php print locale('id');?></th>
						<th><?php print locale('amount');?></th>
						<th><?php print locale('date');?></th>
						<th><i class="ion-gear-a"></i></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($orders as $entry):?>
					<tr>
						<td><?php print $entry->id;?></td>
						<td><?php print $entry->amount;?></td>
						<td><?php print timespan($entry->created);?></td>
						<td>
							<!--form action="<?php print site_url('/admin/advertisers/delete/' . $entry->id);?>" method="post" class="btn-custom">
							<button type="submit" class="btn btn-danger" onclick="if(!confirm('Your are about to delete this post, are you sure?')) return false;"><i class="ion-close-circled"></i></button>
							</form-->
							<a href="<?php print site_url('/admin/advertisers/' . $entry->id);?>" class="btn btn-success"><i class="fa fa-file-pdf-o"></i></a>
						</td>
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
			<?php $orders[0]->paginator();?>
			<?php endif;?>
		</div><div class="clearfix"></div>
	</div>
</div>