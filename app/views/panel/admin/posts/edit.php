<div class="ps-wrapper panel-container">
	<div class="panel font-spiegel">
		<div class="col-md-12">
			<div class="col-md-9">
				<h3>&nbsp;<i class="ion-edit"></i> <?php echo $entry->title_es;?></h3>
				<input type="hidden" name="id" value="<?php echo $entry->id;?>">
				<input type="hidden" name="type" value="post">
				<form class="form form-ajax" action="<?php print site_url('/admin/posts/update/' . $entry->id);?>">
					<div class="form-group">
					    <div class="input-group">
					      <span class="input-group-btn">
					        <button class="btn btn-sm disabled" type="button"><?php echo site_url('blog');?>/</button>
					      </span>
					      <input type="text" name="slug" class="form-control input-sm" placeholder="slug" value="<?php echo $entry->slug;?>" />
					    </div>
					</div>

					<h3><?php print locale('es');?></h3>
					<div class="form-group">
						<input type="text" class="form-control slugify" data-target="slug" name="title_es" placeholder="Title" value="<?php echo $entry->title_es;?>" required>
					</div>

					<div class="form-group">
						<textarea class="form-control" name="caption_es" placeholder="Caption"><?php echo $entry->caption_es;?></textarea>
					</div>

				    <div class="progress summernote-progress hide">
				      <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
				        <span class="sr-only"></span>
				      </div>
				    </div>

					<div class="form-group">
						<textarea class="form-control editor" id="content_es" name="content_es" placeholder="Contenido"><?php echo $entry->content_es;?></textarea>
					</div><div class="clearfix"></div>

					<hr>
					<h3><?php print locale('en');?></h3>

					<div class="form-group">
						<input type="text" class="form-control slugify" data-target="slug" name="title_en" placeholder="Title" value="<?php echo $entry->title_en;?>" required>
					</div>

					<div class="form-group">
						<textarea class="form-control" name="caption_en" placeholder="Caption"><?php echo $entry->caption_en;?></textarea>
					</div>

					<div class="form-group">
						<textarea class="form-control editor" name="content_en" id="content_en" placeholder="Contenido"><?php echo $entry->content_en;?></textarea>
					</div><div class="clearfix"></div>

					<hr>

					<div class="form-group">
						<input type="text" class="form-control" name="external_link" placeholder="http://external-link.com" value="<?php echo $entry->external_link;?>">
					</div>

				<div class="form-group text-center btn-custom">
					<a href="<?php print site_url('/admin/posts');?>" class="btn btn-primary"> <i class="ion-backspace"></i> <span><?php print locale('back');?></span> </a>
					<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
				</div>
				<div class="clearfix"></div>

			</div>
			<div class="col-md-3">
				<div class="group-control">&nbsp;</div>
				<h4><i class="ion-eye"></i> Privacidad </h4>
				<div class="radio">
					<label>
				    	<input type="radio" id="privacy_id1" name="privacy_id" value="1"<?php echo $entry->privacy_id == 1 ? ' checked' : '';?>>
				    	<i class="ion-earth"></i> Publicado 
				    </label>
				</div>
				<div class="radio">
					<label>
				    	<input type="radio" id="privacy_id2" name="privacy_id" value="2"<?php echo ($entry->privacy_id == 2 OR is_null($entry->privacy_id)) ? ' checked' : '';?>>
				    	<i class="ion-locked"></i> Borrador 
				    </label>
				</div>
				<div class="clearfix"></div>
				<div class="font-group">
					<input type="text" class="form-control" name="button_text" placeholder="Texto botón" value="" />
				</div>
				</form>
				<div class="clearfix"></div>			
				<div class="text-left tags">
					<h4> <i class="ion-pricetags"></i> Tags </h4>
				    <div class="input-group">
				      <input type="text" id="tags-add" class="form-control" placeholder="Sports, Health, Latest News, ..." />
				      <span class="input-group-btn">
				        <button type="button" class="btn btn-default btn-primary btn-tags-add"><i class="ion-pricetag"></i></button>
				      </span>
				    </div><hr>
					<div class="tags-included"></div>
					<div class="tags-excluded"></div>
				</div><hr><div class="clearfix"></div>
				<h4> <i class="ion-images"></i> Gallery </h4>
				<form 
					class="dropzone" 
					data-target="post" 
					data-message="Drop images here" 
					data-domain="posts" 
					data-url="/admin/files/resize" 
					data-index="/files/post/<?php echo $entry->id;?>" 
					data-id="<?php echo $entry->id;?>" data-max="10">
				</form>	

			</div><div class="clearfix"></div>
		</div><div class="clearfix"></div>
	</div>
</div>

<script>
	$(function(){
		if($('.tags-included').length){
			tags();	
		}

	});
	initSample();
	
</script>