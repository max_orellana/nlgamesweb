<div class="ps-wrapper panel-container">
	<div class="bg-container bg-<?php print session('group');?> darkfade">
	    <header class="c-section_header section-<?php print session('group');?> bottom-margin">
	        <h2 class="c-section_title u-text-gradient-<?php print session('group');?>"><?php print locale('posts');?></h2>
	    </header>
		<div class="bd-box btn-custom">
			<?php echo messages();?>
			<a href="<?php print site_url('/admin/posts/create');?>" class="btn btn-success"><i class="ion-compose"></i> <span><?php print locale('create');?></span></a>	
			<div class="group-control">&nbsp;</div>
		        <?php echo search('id,slug,title_' . LOCALE . ',caption_' . LOCALE . ',content_' . LOCALE);?>   
			<div class="alert alert-danger posts-tags">
				<span><a  class="<?php print isset($_GET['tag']) ? '' : 'active';?>" href="<?php print site_url('/admin/posts');?>"><?php print locale('posts_all');?></a> <span class="badge"><?php print $total;?></span></span>&nbsp;&nbsp;&nbsp;
			<?php foreach($tags['entries'] as $tag):?>
				<span><a class="<?php print isset($_GET['tag']) ? ($_GET['tag'] == $tag->id ? 'active' : '') : '';?>" href="?tag=<?php print $tag->id;?>"><?php print $tag->tag;?></a> <span class="badge"><?php print isset($tags['count'][$tag->id])?$tags['count'][$tag->id]:'0';?></span>&nbsp;&nbsp;&nbsp;
			<?php endforeach;?>
			</div>
			<?php if(count($entries)):?>
				<table class="table table-<?php print session('group');?>">
					<thead>
						<tr>
							<th><i class="ion-eye"></i></th>
							<th><i class="ion-images"></i></th>
							<th><?php print locale('title');?></th>
							<th><?php print locale('author');?></th>
							<th><?php print locale('tags');?></th>
							<th><?php print locale('modified');?></th>
							<th><i class="ion-gear-a"></i></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($entries as $post):?>
						<tr>
							<td><span class="label label-<?php echo is_numeric($post->privacy_id) ? \Model\Post::$statuscolors[($post->privacy_id-1)][0] : '?';?> label-badge"><i class="<?php echo is_numeric($post->privacy_id) ? \Model\Post::$statuscolors[($post->privacy_id-1)][1] : '?';?>"></i></span></td>
							<td>
								<a href="<?php print site_url('/admin/posts/' . $post->id);?>">
								<?php if(count($post->files())):?>
									<img src="<?php print site_url('/upload/posts/50x50/' . $post->files()[0]->name);?>" class="summoner-icon">
								<?php else:?>
									<img src="<?php print site_asset('img/dummy-50x50.jpg');?>" class="summoner-icon">
								<?php endif;?>
								</a>
							</td>
							<td><?php echo $post->title_es;?></td>
							<td><?php echo $post->login;?></td>
							<td>
								<?php foreach($post->tags() as $tag):?>
									<?php echo $tag->tag;?>
								<?php endforeach;?>
							</td>
							<td><i class="ion-clock"> <?php echo timespan($post->updated);?></td>
							<td>
								<form action="<?php print site_url('/admin/posts/delete/' . $post->id);?>" method="post" class="btn-custom">
									<button type="submit" class="btn-discrete text-danger" onclick="if(!confirm('Your are about to delete this post, are you sure?')) return false;"><i class="ion-trash-a"></i></button>
									<a href="<?php print site_url('/blog/' . $post->slug);?>" class="btn-discrete text-warning" target="_blank" title="See"><i class="ion-eye"></i></a>
									<a href="<?php print site_url('/admin/posts/' . $post->id);?>" class="btn-discrete text-success" title="Edit"><i class="ion-edit"></i></a>
								</form>
							</td>
						</tr>
					<?php endforeach;?>
					</tbody>
				</table>
				<?php $entries[0]->paginator();?>
				<?php else:?>
					<a href="<?php print site_url('/admin/posts/create');?>" class="btn btn-lg btn-success"><i class="ion-battery-empty"></i> &nbsp; Posts badge is empty! You may create one.</a>
				<?php endif;?>
			</div><div class="clearfix"></div>
		</div>
	</div>
</div>