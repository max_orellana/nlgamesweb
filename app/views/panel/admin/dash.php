<div class="ps-wrapper panel-container container-height">
    <div class="bg-container bg-admin darkfade">
        <header class="c-section_header section-admin bottom-margin">
            <h2 class="c-section_title u-text-gradient-<?php print session('group');?>"><?php print locale('dashboard');?></h2>
        </header>
         
         <div class="bd-box text-center">
            <div class="bd-box-page-box1">
                <div class="bd-box-hat-title"><?php print locale('users');?></div>
                <a href="<?php print site_url('/admin/online');?>">
                    <div class="online-list"></div>
                    <h3><span id="online"></span>/<span id="usercount"></span></h3>
                </a>
                <div class="bd-box-hat-text"><?php print locale('users_connected_and_users');?></div>
            </div>

            <div class="bd-box-page-box2">
                <div class="bd-box-book-title"><?php print locale('posts');?></div>
                <a href="<?php print site_url('/admin/posts');?>">
                    <h3 id="postcount">-</h3>
                </a>

                <div class="bd-box-book-text"><?php print locale('total_posts_dash_text');?></div>
            </div>
            
        <div class="clear"></div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function dash(){
        $.ajax({
            type: 'get',
            url: '/admin/dash/realtime',
            cache: false,
            success: function(json){
                $('.online-list').html('');
                $(json.online).each(function(i,row){
                    $('.online-list').append('<span class="badge">' + row.login + ' <small>' + row.since + '</small></span>&nbsp;&nbsp;');
                });
                $('#online').text($(json.online).length+1);
                $('#postcount').text(json.postcount);
                $('#usercount').text(json.usercount);
            }
        });
    }

    jQuery(document).ready(function(){
        setInterval(function(){
            dash();
        },30000);dash();
    });
</script>        