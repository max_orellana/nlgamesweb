<div class="panel">
    <div class="col-md-12">
    <?php echo messages();?>
    <?php if($rows):?>
        <?php foreach($rows as $ticket):?>
        <div class="alert <?php print $ticket->group_id == 2 ? 'alert-success':'';?>">
            <span class="pull-right"><?php print timespan($ticket->created);?></span>
            <h4><i class="fa fa-user text-success"></i> &nbsp; <?php print $ticket->user;?></h4>
            <p><?php print $ticket->content;?></p><div class="clearfix"></div>
            <?php if( is_file( SP . 'public/upload/tickets/' . $ticket->file)):?>
                <img src="<?php print site_url('/upload/tickets/' . $ticket->file);?>">
            <?php endif;?>
        </div>
        <?php endforeach;?>
    <?php endif;?>
	    <div class="alert alert-success">
	        <h3><?php print locale('reply')?> </h3>
            <form class="form form-ajax" action="<?php print site_url('/admin/tickets/send');?>" method="post">
            	<input type="hidden" name="ticket_id" value="<?php print segments(3);?>">
                <div class="form-group">
                    <select class="form-control" name="tag" required>
                    <?php foreach($tags as $tag):?>
                        <option value="<?php print $tag->id;?>"><?php print locale($tag->tag);?></option>
                    <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="status_id" required>
                        <option value="0"<?php print $ticket->status_id == 0 ? ' selected':'';?>><?php print locale('closed');?></option>
                        <option value="1"<?php print $ticket->status_id == 1 ? ' selected':'';?>><?php print locale('open');?></option>
                    </select>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="description" value="" placeholder="<?php print locale('tickets_description');?>"></textarea>
                </div>
                <div class="alert message hide"></div>
                <div class="form-group text-right btn-custom">
                    <a href="<?php print site_url('/admin/tickets');?>" class="btn btn-lg btn-primary back"><i class="ion-ios-bolt fa-x1"></i> <span><?php print locale('back')?></span></a>
                    <button type="submit" class="btn btn-success"><i class="ion-ios-bolt fa-x1"></i> <span><?php print locale('reply')?></span></button>
                </div>
            </form>
	    </div><div class="clearfix"></div>    
	</div><div class="clearfix"></div>
</div>

