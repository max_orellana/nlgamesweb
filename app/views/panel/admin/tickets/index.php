<div class="panel">
	<div class="col-md-12">
	<?php echo messages();?>
	<?php if(count($entries)):?>
		<table class="table">
			<thead>
				<tr>
					<th><i class="ion-eye"></i></th>
					<th><i class="ion-images"></i></th>
					<th><?php print locale('type');?></th>
					<th><?php print locale('type');?></th>
					<th><?php print locale('threads');?></th>
					<th><i class="ion-clock"></i></th>
					<th><i class="ion-clock"></i></th>
					<th><i class="ion-gear-a"></i></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($entries as $entry):?>
				<tr class="<?php print ! $entry->status_id ? 'success':'warning';?>">
					<td><?php print $entry->id;?></td>
					<td><?php print $entry->user;?></td>
					<td><?php print $entry->tag ? locale($entry->tag->tag) : '?';?></td>
					<td><?php print $entry->tag ? count($entry->tag) : '?';?></td>
					<td><?php print timespan($entry->created);?></td>
					<td><?php print ( is_file( SP . 'public/upload/tickets/' . $entry->file) ? '<i class="ion-paperclip"></i>':'');?></td>
              		<td><a href="javascript:void(0)" class="toggle">
              			<i 
	              			class="fa fa-md <?php print ! $entry->status_id ? 'fa-check-circle' : 'fa-exclamation-circle';?>" 
	              			data-id="<?php print $entry->id;?>" 
	              			data-field="status_id" data-model="Ticket" 
	              			data-value="<?php print $entry->status_id;?>" 
	              			data-icons="fa-check-circle,fa-exclamation-circle" 
	              			data-row-status="success,warning">
              			</i></a></td>
					<td class="btn-custom">
						<a href="<?php print site_url('/admin/tickets/' . $entry->id);?>" class="btn btn-success"><i class="ion-compose"></i> </a>
					</td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table><div class="clearfix"></div>
		<?php $entries[0]->paginator();?>
		<?php else:?>
			<a href="<?php print site_url('/admin/posts/create');?>" class="btn btn-success"><i class="ion-battery-empty"></i> &nbsp; Posts badge is empty! You may create one.</a>
		<?php endif;?>
	</div><div class="clearfix"></div>
</div>