<div class="panel">
	<div class="col-md-12">
		<h3>&nbsp;<i class="ion-edit"></i> <?php print $entry->slug;?></h3>
		<input type="hidden" name="id" value="<?php echo $entry->id;?>">
		<form class="form form-ajax" action="<?php print site_url('/admin/items-types/update/' . $entry->id);?>">
			<div class="form-group">
				<label class="control-label"><?php echo locale('slug');?></label>            
				<input type="text" class="form-control" name="slug" placeholder="<?php print locale('slug');?>" value="<?php print $entry->slug;?>" required>
			</div>

			<hr>
			
			<div class="form-group">
				<label class="control-label"><?php echo locale('es');?></label>            
				<input type="text" class="form-control" name="title_es" placeholder="<?php print locale('es');?>" value="<?php print locale($entry->slug,'es');?>" required>
			</div>

			<div class="form-group">
				<label class="control-label"><?php echo locale('en');?></label>            
				<input type="text" class="form-control" name="title_en" placeholder="<?php print locale('en');?>" value="<?php print locale($entry->slug,'en');?>" required>
			</div>

			<div class="form-group text-center btn-custom">
				<a href="<?php print site_url('/admin/items-types');?>" class="btn btn-primary"> <i class="ion-backspace"></i> <span><?php print locale('back');?></span> </a>
				<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
			</div>
		</form>
	</div><div class="clearfix"></div>
</div>
