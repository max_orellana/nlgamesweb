<div class="ps-wrapper panel-container container-height">
	<div class="panel">
		<div class="col-md-12">
			<h3>&nbsp;<i class="ion-edit"></i> </h3>
			<form class="form form-ajax" id="post" action="<?php print site_url('/admin/heroes/update/0');?>">
				<div class="form-group">
					<input type="text" class="form-control" name="title" placeholder="<?php print locale('title');?>" value="" required>
				</div>

				<div class="form-group">
					<input type="text" class="form-control" name="slug" placeholder="<?php print locale('slug');?>" value="" required>
				</div>

				<div class="form-group">
					<textarea class="form-control" name="caption" placeholder="<?php print locale('caption');?>"></textarea>
				</div>

				<div class="form-group">
					<input class="form-control" type="number" name="damage" placeholder="<?php print locale('damage');?>" value="">
				</div>

				<div class="form-group">
					<input class="form-control" type="number" name="armor" placeholder="<?php print locale('armor');?>" value="">
				</div>

				<div class="form-group">
					<input class="form-control" type="number" name="agility" placeholder="<?php print locale('agility');?>" value="">
				</div>

				<div class="form-group">
					<input class="form-control" type="number" name="utility" placeholder="<?php print locale('utility');?>" value="">
				</div>

				<div class="form-group">
					<input class="form-control" type="number" name="speed" placeholder="<?php print locale('speed');?>" value="">
				</div>

				<div class="form-group">
					<input class="form-control" type="number" name="difficulty" placeholder="<?php print locale('difficulty');?>" value="">
				</div>

				<div class="form-group text-center btn-custom">
					<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
				</div>
			</form>
		</div><div class="clearfix"></div>
	</div>
</div>