<div class="ps-wrapper panel-container container-height">
	<div class="bg-container bg-<?php print session('group');?> darkfade">
	    <header class="c-section_header section-<?php print session('group');?> bottom-margin">
	        <h2 class="c-section_title u-text-gradient-<?php print session('group');?>"><?php print locale('words');?></h2>
	    </header>

		<div class="bd-box btn-custom">
			<?php echo messages();?>

			<a href="<?php print site_url('/admin/words/create');?>" class="btn btn-success"><i class="ion-compose"></i> <span><?php print locale('add');?></span></a>
			<div class="group-control">&nbsp;</div>

		<?php if(count($entries)):?>
			<?php echo search('id,word_key,word_es,word_en');?>
			<table class="table table-<?php print session('group');?>">
				<thead>
					<tr>
						<th><?php print locale('key');?></th>
						<th><?php print locale('value');?></th>
						<th><i class="ion-gear-a"></i></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($entries as $entry):?>
					<tr>
						<td><?php print $entry->word_key;?></td>
						<td><?php print words($entry->{'word_' . LOCALE},5);?></td>
						<td>
							<form action="<?php print site_url('/admin/words/delete/' . $entry->id);?>" method="post" class="btn-custom">
							<button type="submit" class="btn btn-danger" onclick="if(!confirm('Your are about to delete this post, are you sure?')) return false;"><i class="ion-close-circled"></i></button>
							<a href="<?php print site_url('/admin/words/' . $entry->id);?>" class="btn btn-success"><i class="ion-edit"></i></a>
							</form>
						</td>
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
			<?php $entries[0]->paginator();?>
			<?php else:?>
				<a href="/admin/words/create" class="btn btn-success"><i class="ion-battery-empty"></i> &nbsp; words badge is empty! You may create one.</a>
			<?php endif;?>
		</div><div class="clearfix"></div>
	</div>
</div>