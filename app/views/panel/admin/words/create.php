<div class="ps-wrapper panel-container container-height">
	<div class="panel">
		<div class="col-md-12">
			<form class="form form-ajax" id="post" action="<?php print site_url('/admin/words/update/0');?>">
				<div class="form-group">
					<input type="text" class="form-control" name="word_key" placeholder="<?php print locale('word_key');?>" value="" required>
				</div>

				<div class="form-group">
					<textarea class="form-control" name="word_es" placeholder="<?php print locale('es');?>"></textarea>
				</div>

				<div class="form-group">
					<textarea class="form-control" name="word_en" placeholder="<?php print locale('en');?>"></textarea>
				</div>

				<div class="form-group text-center btn-custom">
					<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
				</div>
			</form>
		</div><div class="clearfix"></div>
	</div>
</div>