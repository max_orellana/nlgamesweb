<div class="ps-wrapper panel-container container-height">
	<div class="panel">
		<div class="col-md-12">
			<h3>&nbsp;<i class="ion-edit"></i> <?php echo $entry->word_key;?></h3>
			<input type="hidden" name="id" value="<?php echo $entry->id;?>">
			<form class="form form-ajax" id="post" action="<?php print site_url('/admin/words/update/' . $entry->id);?>">
				<div class="form-group">
					<input type="text" class="form-control" name="word_key" placeholder="" value="<?php echo $entry->word_key;?>" required>
				</div>

				<div class="form-group">
					<textarea class="form-control" name="word_es" placeholder="<?php print locale('es');?>"><?php echo $entry->word_es;?></textarea>
				</div>

				<div class="form-group">
					<textarea class="form-control" name="word_en" placeholder="<?php print locale('en');?>"><?php echo $entry->word_en;?></textarea>
				</div>

				<div class="form-group text-center btn-custom">
					<button type="submit" class="btn btn-success"> <i class="ion-checkmark-round"></i> <span><?php print locale('save');?></span> </button>
				</div>
			</form>
		</div><div class="clearfix"></div>
	</div>
</div>