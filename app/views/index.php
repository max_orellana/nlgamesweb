<!--div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script-->

<div class="col-md-12 col-xs-12 no-pad slider-type border-radius">
    <ul class="col-md-3 col-xs-12 aside border-radius no-pad">
        <?php foreach($slides as $post):?>
        <a href="#">
            <li class="col-md-12 col-xs-12 border-radius item">
                <div class="col-md-3 col-xs-3 border-radius pic" style="background-image:url(<?php echo count($post->files()) ? site_url('/upload/posts/50x50/' . $post->files()[0]->name) : '';?>"></div>
                <div class="col-md-12 col-xs-12 no-pad border-radius wrap-shadow"></div>
                <div class="col-md-9 col-xs-9 no-pad text">
                    <div class="col-md-12 col-xs-12 title gold"><?php echo $post->{'title_' . LOCALE};?></div>
                    <div class="col-md-12 col-xs-12 date"><?php echo timespan($post->updated);?></div>
                </div>
            </li>
        </a>
        <?php endforeach;?>
    </ul>
    <ul class="col-md-9 col-xs-12 main border-radius no-pad">
    <?php if($slides):?>
        <?php foreach($slides as $post):?>
        <a href="<?php print strlen($post->external_link) ? $post->external_link : site_url('blog/' . $post->slug);?>"<?php print strlen($post->external_link) ? ' target="_blank"':'';?>>            
            <li class="col-md-12 col-xs-12 no-pad pic" style="background-image:url(<?php echo count($post->files()) ? site_url('/upload/posts/800x300/' . $post->files()[0]->name) : '';?>)">
               <div class="col-md-12 col-xs-12 dark-gold title"><?php echo $post->{'caption_' . LOCALE};?></div>
            </li>
        </a>            
        <?php endforeach;?>
    <?php endif;?>
    </ul>
</div>

<!--div class="col-md-12 col-xs-12 content border-radius"-->

<div class="col-md-12 col-xs-12 content border-radius">
    <div class="col-md-12 col-xs-12 no-pad main">
    <?php if($feature):?>
	    <ul class="col-md-12 col-xs-12 no-pad latest-news">
        <?php foreach($feature as $post):?>
            <li class="col-md-12 col-xs-12">
                <div class="col-md-12 col-xs-12 no-pad title">
                <?php if(count($post->files())):?>
                	<div class="col-md-3 col-xs-3 border-radius no-pad pic-thumb" style="background-image: url(<?php echo count($post->files()) ? site_url('/upload/posts/50x50/' . $post->files()[0]->name) : '';?>)" alt="<?php echo $post->{'title_' . LOCALE};?>)"></div>
                <?php endif;?>
                    <div class="col-md-9 col-xs-9 light-gold main-title"><?php echo $post->{'title_' . LOCALE};?></div>
                    <div class="col-md-9 col-xs-9 light-gold creator"><?php print locale('published_by');?> <span class="gold"><?php echo $post->login;?></span></div>
                    <div class="col-6 col-xs-6 read-more border-radius right btn-expand"><span class="fa fa-plus"></span></div>
                </div>
            <?php if(strlen(strip_tags(trim($post->{'content_' . LOCALE}))) OR strlen(trim($post->external_link))):?>
                <a href="<?php print strlen($post->external_link) ? $post->external_link : site_url('blog/' . $post->slug);?>"<?php print strlen($post->external_link) ? ' target="_blank"':'';?>>
            <?php endif;?>
                    <div class="col-md-12 col-xs-12 no-pad back-thumb">
                    <?php if(count($post->files())):?>
                        <div class="col-md-12 col-xs-12 no-pad border-radius pic" style="background-image:url(<?php echo site_url('/upload/posts/1090x200/' . $post->files()[0]->name);?>)"></div>
                    <?php endif;?>
                        <div class="col-md-12 col-xs-12 no-pad text"><p><?php echo $post->{'caption_' . LOCALE};?></p></div>
                    <?php if(strlen(strip_tags(trim($post->{'content_' . LOCALE}))) OR strlen($post->external_link)):?>
                        <div class="col-6 col-xs-6 read-more border-radius right"><?php print locale($post->button_text?:'read_more');?></div>
                    <?php endif;?>
                    </div>
            <?php if(strlen(strip_tags(trim($post->{'content_' . LOCALE}))) OR strlen(trim($post->external_link))):?>                    
                </a>
            <?php endif;?>
            </li>
        <?php endforeach;?>
        </ul>
    <?php endif;?>        
    </div>
    <!--div class="col-md-4 col-xs-12 no-pad side-bar">
        <div class="col-md-12 col-xs-12 no-pad text-shadow facebook">
            <div class="col-md-12 col-xs-12 light-gold main-title">Facebook</div>
            <div class="col-md-12 col-xs-12 iframe"><div class="fb-page" data-href="https://www.facebook.com/www.nlgames.net" data-width="600" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/www.nlgames.net"><a href="https://www.facebook.com/www.nlgames.net">Newline</a></blockquote></div></div></div>
        </div>
        <div class="col-md-12 col-xs-12 no-pad text-shadow community">
            <div class="col-md-12 col-xs-12 light-gold main-title"><?php print locale('community_posts');?></div>
            <ul class="col-md-12 col-xs-12 no-pad posts">

          <?php foreach($forum_latest as $post):?>
                <a class="col-md-12 col-xs-12" href="<?php print config()->forum_url;?>/index.php?/topic/<?php print $post->tid;?>-<?php print $post->title_seo;?>" target="_blank">
                    <li>
                        <div class="col-md-12 col-xs-12 no-pad wrap-shadow"></div>
                        <div class="col-md-12 col-xs-12 no-pad text">
                            <div class="col-md-12 col-xs-12 no-pad gold title"><?php echo words($post->title,5);?></div>
                            <div class="col-md-12 col-xs-12 no-pad date"><?php print timespan($post->last_post);?> - <?php print locale('published_by');?><span class="author"><?php print $post->last_poster_name;?></span></div>
                        </div>
                        <div class="right border-radius fi-comments"><i class="fa fa-comments-o"></i></div>
                    </li></a>
          <?php endforeach;?>
            </ul>
        </div>    
    </div-->
</div>

<?php if( ! empty($_SESSION['redirect'])):?>
    <script>
        $(function(){
            $('[href="#login"]').click(); 
        });
    </script>
<?php endif;?>