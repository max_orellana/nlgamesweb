<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width">
        <title>Bienvenido a nlgames </title>
        
    <style type="text/css">
		#outlook a{
			padding:0;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
			line-height:100%;
		}
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:100%;
			-ms-text-size-adjust:100%;
		}
		table,td{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		body{
			margin:0;
			padding:0;
			background:#EEEEEE;
		}
		img{
			border:0;
			height:auto;
			line-height:100%;
			outline:none;
			text-decoration:none;
			max-width:100%;
		}
		table{
			border-collapse:collapse !important;
		}
		body,#bodyTable{
			height:100% !important;
			margin:0;
			padding:0;
			width:100% !important;
		}
		#bodyTable{
			font-family:Helvetica Neue, Helvetica, Arial, sans-serif;
			min-height:100%;
			background:#eeeeee;
			color:#444444;
		}
		img{
			border:0!important;
		}
		a{
			text-decoration:none;
		}
		h1{
			font-family:Helvetica;
			font-size:36px;
			font-style:normal;
			font-weight:bold;
			text-align:center;
			margin:20px auto;
		}
		h2{
			font-size:32px;
			font-style:normal;
			font-weight:bold;
			margin:30px auto 0;
			color:#FFFFFF!important;
		}
		h3{
			font-size:20px;
			font-weight:bold;
			margin:25px 0;
			letter-spacing:normal;
			text-align:left;
		}
		a h3{
			color:#444444!important;
			text-decoration:none!important;
		}
		.titleLink{
			text-decoration:none!important;
		}
		.preheaderContent{
			color:#808080;
			font-size:10px;
			line-height:125%;
		}
		.preheaderContent a:link,.preheaderContent a:visited,.preheaderContent a .yshortcuts{
			color:#606060;
			font-weight:normal;
			text-decoration:underline;
		}
		#emailHeader,#tacoTip{
			color:#FFFFFF;
		}
		#emailHeader{
			background-color:#ffffff;
		}
		p{
			font-size:20px;
			line-height:24px;
			margin:10px auto;
		}
		p.article{
			text-align:left;
			margin:0px 0px 20px 0;
			color:#4d4d4d;
		}
		.trello-chat p{
			text-align:left;
			padding:40px 10px 0 20px;
		}
		.article-img{
			margin:0px 30px 40px 0px;
			width:86px;
		}
		.middle-article{
			padding:0 40px;
		}
		.first-article{
			padding:40px 40px 0;
		}
		.last-article{
			padding:0 40px 0;
		}
		.first-article>img,.middle-article>img,.last-article>img{
			margin:0px 30px 40px 0px;
		}
		#header img{
			width:600px;
			max-width:100%!important;
		}
		#button{
			display:inline-block;
			margin:10px auto;
			background:#ffffff;
			border-radius:4px;
			font-weight:bold;
			font-size:18px;
			padding:15px 20px;
			cursor:pointer;
			color:#0079bf;
			margin-bottom:50px;
		}
		#socialIconWrap img{
			line-height:35px!important;
			padding:0px 5px;
		}
		.footerContent div{
			color:#707070;
			font-family:Arial;
			font-size:12px;
			line-height:125%;
			text-align:center;
			max-width:100%!important;
		}
		.footerContent div a:link,.footerContent div a:visited{
			color:#336699;
			font-weight:normal;
			text-decoration:underline;
		}
		.footerContent img{
			display:inline;
		}
		#socialLinks img{
			margin:10px 2px;
		}
		#utility{
			border-top:1px solid #dddddd;
		}
		#utility div{
			text-align:center;
		}
		#monkeyRewards img{
			max-width:160px;
		}
		#emailFooter{
			max-width:100%!important;
		}
		#footerTwitter a,#footerFacebook a{
			text-decoration:none!important;
			color:#ffffff!important;
			font-size:14px;
		}
		#emailButton{
			border-radius:6px;
			background:#70B500!important;
			margin:0px auto 60px;
			box-shadow:0px 4px 0px #578C00;
		}
		#socialLinks a{
			width:40px;
		}
		#socialLinks #blogLink{
			width:80px!important;
		}
		#logoWrap{
			background:url('<?php echo site_asset('/emails/img/d7a697bc-1cb3-4334-a1c4-2da3f78473a6.jpg');?>') center center / 600px 50px no-repeat!important;
		}
		.sectionWrap{
			text-align:center;
		}
	@media screen and (max-width: 599px){
		td>img{
			display:block!important;
			margin:0 auto!important;
		}

}	@media screen and (max-width: 599px){
		#emailContainer,#emailFooter,#emailHeader,.sectionWrap,#tacoTip{
			max-width:100%!important;
			width:600px !important;
			margin:0!important;
			border-radius:0!important;
			box-shadow:none!important;
		}

}	@media screen and (max-width: 599px){
		td>img{
			display:block!important;
			margin:0 auto!important;
		}

}	@media screen and (max-width: 599px){
		p.article{
			display:block!important;
			margin:0 20px!important;
		}

}	@media screen and (max-width: 599px){
		.preheaderContent{
			text-align:center!important;
		}

}	@media screen and (max-width: 599px){
		#content p{
			font-size:18px;
			line-height:24px;
		}

}	@media screen and (max-width: 599px){
		h2{
			font-size:28px;
		}

}	@media screen and (max-width: 599px){
		h1{
			font-size:36px;
		}

}</style></head>
    <body style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;background: #EEEEEE;height: 100% !important;width: 100% !important;">
        
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0;font-family: Helvetica Neue, Helvetica, Arial, sans-serif;min-height: 100%;background: #eeeeee;color: #444444;border-collapse: collapse !important;height: 100% !important;width: 100% !important;">
            <tr>
                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                    <table align="center" border="0" cellspacing="0" width="600" id="emailContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                        <!--tr>
                            <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader" style="margin: 15px 0 10px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                    <tr>
                                        <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                            <table align="left" border="0" cellspacing="0" width="50%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                <tr>
                                                    <td valign="top" class="preheaderContent" align="left" style="text-align: left!important;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #808080;font-size: 10px;line-height: 125%;">
                                                        Trello helps you start fresh.
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="left" border="0" cellspacing="0" width="50%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                <tr>
                                                    <td valign="top" align="right" class="preheaderContent" style="text-align: right!important;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #808080;font-size: 10px;line-height: 125%;">
                                                        <a href="http://us9.campaign-archive1.com/?u=5cb712992a906406c5eae28a7&id=cf1417d2ef&e=8655269812" target="_blank" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;text-decoration: underline;color: #606060;font-weight: normal;">View this email in your browser</a>.
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr-->
						<tr>
                             <td id="logoWrap" align="bottom" style="text-align: center!important;background-image: url('<?php echo site_asset('/emails/img/d7a697bc-1cb3-4334-a1c4-2da3f78473a6.jpg');?>');background-repeat: no-repeat;background-size: 600px 50px;background-position: center center;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background: url(<?php echo site_asset('/emails/img/d7a697bc-1cb3-4334-a1c4-2da3f78473a6.jpg');?>) center center / 600px 50px no-repeat!important;">
                               <p></p>
                             </td>
                        </tr>

                        <!-- ////////////////////////////
                        /////// BEGIN CONTENT /////// 
                        /////////////////////////// -->
                        
          
                        <tr>
                            <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                <table class="sectionWrap" border="0" cellpadding="0" cellspacing="0" width="600" style="background: #FFFFFF;overflow: hidden;border-radius: 4px;box-shadow: 0px 3px 0px #DDDDDD;max-width: 600px!important;min-width: 600px!important;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: center;border-collapse: collapse !important;" id="content">
                                    <tr>
                                        <td id="header" valign="top" bgcolor="#EB5A46;" style="padding: 20px;max-width: 100%!important;background: #EB5A46;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                        
                                            
                                            <h1 style="font-family: Helvetica;font-size: 36px;font-style: normal;font-weight: bold;text-align: center;margin: 20px auto;"><font color="white">Bienvenido <?php echo $username;?> </font></h1>
                                            <p style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-size: 20px;line-height: 24px;margin: 10px auto;"><font color="white">
                                            Tu Blog <?php echo $title;?> ha sido creado  </font> </p>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                    	<td bgcolor="#FFFFFF" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                        	<table cellpadding="20" width="600" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                            	<col width="30%"></col>
                                                <col width="69%"></col>
                                            	<tr>
                                                	<td valign="top" align="left" style="width: 30%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                    <a href="" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;text-decoration: none;">
                                                    <img src="<?php echo site_asset('/emails/img/aca18c1d-c876-4506-afe5-3e87e2ead583.png');?>" width="180" style="max-width: 100%;-ms-interpolation-mode: bicubic;height: auto;line-height: 100%;outline: none;text-decoration: none;border: 0!important;"></a>
                                                    </td>
                                                    <td align="left" style="width: 69%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                    <p style="text-align: left!important;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-size: 20px;line-height: 24px;margin: 10px auto;"><font color="#444;">
                                                    <a href="" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;text-decoration: none;"><b><font color="#0079bf">Visualiza tu Blog</font></b></a>
                                                    <br><br>
                                                   Texto de prueba.
                                                    </font>
                                                    </p>
                                                    <table class="button main" border="0" cellpadding="10" cellspacing="0" style="display: inline-block;margin: 10px auto 0;border-radius: 6px;font-weight: bold;font-size: 16px;text-align: center;cursor: pointer;color: #FFFFFF;background: #61BD4F;box-shadow: 0px 4px 0px #519839;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="left">
                                                          <tr>
                                                              <td align="left" valign="middle" class="emailButtonContent" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                  <a href="<?php echo site_url('/camp');?>" target="_blank" style="text-decoration: none;padding: 10px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;"><font color="white">Acceder mi Blog &rarr;</font></a>


                                                              </td>
                                                          </tr>
                                                      </table>
                                                    </td>
                                                </tr>   	                                      						</table>
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td bgcolor="#FFFFFF" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                        	<table cellpadding="20" width="600" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                            	<col width="30%"></col>
                                                <col width="69%"></col>
                                            	<tr>
                                                	<td valign="top" align="left" style="width: 30%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                    <a href="http://blogs.devmeta.net/login" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;text-decoration: none;">
                                                    <img src="<?php echo site_asset('/emails/img/095b6d9c-759f-4aea-9d83-92fdf3361ae6.png');?>" width="180" style="max-width: 100%;-ms-interpolation-mode: bicubic;height: auto;line-height: 100%;outline: none;text-decoration: none;border: 0!important;"></a>
                                                    </td>
                                                    <td align="left" style="width: 69%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                    <p style="text-align: left!important;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-size: 20px;line-height: 24px;margin: 10px auto;"><font color="#444;">
                                                    <a href="http://blogs.devmeta.net/login" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;text-decoration: none;"><b><font color="#0079bf">Accede a tu Blog</font></b></a>
                                                    <br><br>
                                                    Ingresa al panel de control para agregar o editar tus publicaciones.
                                                    </font>
                                                    </p>
                                                    <table class="button main" border="0" cellpadding="10" cellspacing="0" style="display: inline-block;margin: 10px auto 0;border-radius: 6px;font-weight: bold;font-size: 16px;text-align: center;cursor: pointer;color: #FFFFFF;background: #61BD4F;box-shadow: 0px 4px 0px #519839;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="left">
                                                          <tr>
                                                              <td align="left" valign="middle" class="emailButtonContent" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                  <a href="http://blogs.devmeta.net/login" target="_blank" style="text-decoration: none;padding: 10px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;"><font color="white">Accede al Panel &rarr;</font></a>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                    </td>
                                                </tr>   	                                      									</table>
                                                <table border="0" cellpadding="10" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                              <tr>
                                                  <td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">&nbsp;</td>
                                              </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                        	<table cellpadding="5" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                            <tr><td style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;"></td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!-- ////////////////////////////
                        //////////// FOOTER //////////// 
                        /////////////////////////// -->


                        <tr>
                            <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                <table border="0" align="center" cellpadding="10" cellspacing="0" width="600" id="emailFooter" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;max-width: 100%!important;">
                                        <tr>
                                            <td valign="top" class="footerContent" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                            
                                                <!-- // Begin Module: Standard Footer \\ -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                    
                                                    
                                                    <tr>
                                                        <td valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                            <br>
                                                            <div style="color: #707070;font-family: Arial;font-size: 12px;line-height: 125%;text-align: center;max-width: 100%!important;">
                                                                
                                                                <em>Copyright &copy; 2015 nlgames</em>
                                                                
                                                            </div>
                                                            <br>
                                                        </td>
                                                      
                                                    </tr>

                                                </table>
                                                <!-- // End Module: Standard Footer \\ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Footer \\ -->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
</html>