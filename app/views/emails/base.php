<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width">
        <title>Newline Games</title>
        
    <style type="text/css">
		#outlook a{
			padding:0;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
			line-height:100%;
		}
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:100%;
			-ms-text-size-adjust:100%;
		}
		table,td{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		body{
			margin:0;
			padding:0;
			background:#EEEEEE;
		}
		img{
			border:0;
			height:auto;
			line-height:100%;
			outline:none;
			text-decoration:none;
			max-width:100%;
		}
		table{
			border-collapse:collapse !important;
		}
		body,#bodyTable{
			height:100% !important;
			margin:0;
			padding:0;
			width:100% !important;
		}
		#bodyTable{
			font-family:Helvetica Neue, Helvetica, Arial, sans-serif;
			min-height:100%;
			background:#eeeeee;
			color:#444444;
		}
		#emailContainer {

		}
		img{
			border:0!important;
		}
		a{
			text-decoration:none;
		}
		h1{
			font-family:Helvetica;
			font-size:36px;
			font-style:normal;
			font-weight:bold;
			text-align:center;
			margin:20px auto;
		}
		h2{
			font-size:32px;
			font-style:normal;
			font-weight:bold;
			margin:30px auto 0;
			color:#FFFFFF!important;
		}
		h3{
			font-size:20px;
			font-weight:bold;
			margin:25px 0;
			letter-spacing:normal;
			text-align:left;
		}
		a h3{
			color:#444444!important;
			text-decoration:none!important;
		}
		.titleLink{
			text-decoration:none!important;
		}
		.preheaderContent{
			color:#808080;
			font-size:10px;
			line-height:125%;
		}
		.preheaderContent a:link,.preheaderContent a:visited,.preheaderContent a .yshortcuts{
			color:#606060;
			font-weight:normal;
			text-decoration:underline;
		}
		#emailHeader,#tacoTip{
			color:#FFFFFF;
		}
		#emailHeader{
			background-color:#ffffff;
		}
		p{
			font-size:20px;
			line-height:24px;
			margin:10px auto;
		}
		p.article{
			text-align:left;
			margin:0px 0px 20px 0;
			color:#4d4d4d;
		}
		.trello-chat p{
			text-align:left;
			padding:40px 10px 0 20px;
		}
		.article-img{
			margin:0px 30px 40px 0px;
			width:86px;
		}
		.middle-article{
			padding:0 40px;
		}
		.first-article{
			padding:40px 40px 0;
		}
		.last-article{
			padding:0 40px 0;
		}
		.first-article>img,.middle-article>img,.last-article>img{
			margin:0px 30px 40px 0px;
		}
		#header img{
			max-width:100%!important;
		}
		#button{
			display:inline-block;
			margin:10px auto;
			background:#ffffff;
			border-radius:4px;
			font-weight:bold;
			font-size:18px;
			padding:15px 20px;
			cursor:pointer;
			color:#0079bf;
			margin-bottom:50px;
		}
		#socialIconWrap img{
			line-height:35px!important;
			padding:0px 5px;
		}
		.footerContent div{
			color:#707070;
			font-family:Arial;
			font-size:12px;
			line-height:125%;
			text-align:center;
			max-width:100%!important;
		}
		.footerContent div a:link,.footerContent div a:visited{
			color:#336699;
			font-weight:normal;
			text-decoration:underline;
		}
		.footerContent img{
			display:inline;
		}
		#socialLinks img{
			margin:10px 2px;
		}
		#utility{
			border-top:1px solid #dddddd;
		}
		#utility div{
			text-align:center;
		}
		#monkeyRewards img{
			max-width:160px;
		}
		#emailFooter{
			max-width:100%!important;
		}
		#footerTwitter a,#footerFacebook a{
			text-decoration:none!important;
			color:#ffffff!important;
			font-size:14px;
		}
		#emailButton{
			border-radius:6px;
			background:#70B500!important;
			margin:0px auto 60px;
			box-shadow:0px 4px 0px #578C00;
		}
		#socialLinks a{
			width:40px;
		}
		#socialLinks #blogLink{
			width:80px!important;
		}
		#logoWrap{

		}
		.sectionWrap{
			text-align:center;
		}
	@media screen and (max-width: 599px){
		td>img{
			display:block!important;
			margin:0 auto!important;
		}

}	@media screen and (max-width: 599px){
		#emailContainer,#emailFooter,#emailHeader,.sectionWrap,#tacoTip{
			max-width:100%!important;
			width:600px !important;
			margin:0!important;
			border-radius:0!important;
			box-shadow:none!important;
		}

}	@media screen and (max-width: 599px){
		td>img{
			display:block!important;
			margin:0 auto!important;
		}

}	@media screen and (max-width: 599px){
		p.article{
			display:block!important;
			margin:0 20px!important;
		}

}	@media screen and (max-width: 599px){
		.preheaderContent{
			text-align:center!important;
		}

}	@media screen and (max-width: 599px){
		#content p{
			font-size:18px;
			line-height:24px;
		}

}	@media screen and (max-width: 599px){
		h2{
			font-size:28px;
		}

}	@media screen and (max-width: 599px){
		h1{
			font-size:36px;
		}

}</style></head>
    <body style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;background: #EEEEEE;height: 100% !important;width: 100% !important;">
        
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0;font-family: Helvetica Neue, Helvetica, Arial, sans-serif;min-height: 100%;background: #eeeeee;color: #444444;border-collapse: collapse !important;height: 100% !important;width: 100% !important;">
            <tr>
                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                    <table align="center" border="0" cellspacing="0" width="600" id="emailContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
						<tr>
                             <td id="logoWrap" align="bottom" style="text-align: center!important;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                             <a href="http://nlgames.net">
                               <img src="<?php echo site_asset('/email/header.png');?>" style="position: relative; top: 75px;">
                               </a>
                             </td>
                        </tr>

                        <!-- ////////////////////////////
                        /////// BEGIN CONTENT /////// 
                        /////////////////////////// -->
                        
          
                        <tr>
                            <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                <table class="sectionWrap" border="0" cellpadding="0" cellspacing="0" width="600" style="background: #FFFFFF;overflow: hidden;max-width: 600px!important;min-width: 600px!important;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: center;border-collapse: collapse !important;" id="content">
                                    <tr>
                                        <td id="header" valign="top" bgcolor="#051c2d;" style="padding: 40px 20px 10px;max-width: 100%!important;background: #051c2d;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                        <?php if(!empty($title)):?>
                                            <h1 style="font-family: Helvetica;font-size: 36px;font-style: normal;font-weight: bold;text-align: center;margin: 20px auto;"><font color="white"><?php echo $title;?> </font></h1>
                                           <?php endif;?>
                                            <p style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-size: 20px;line-height: 24px;margin: 10px auto;"><font color="white">
                                             </font> </p>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                    	<td bgcolor="#FFFFFF" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                        	<table cellpadding="20" width="600" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                <col width="100%"></col>
                                            	<tr>
                                                    <td align="center" style="width: 69%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                    <p style="text-align: left!important;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-size: 20px;line-height: 24px;margin: 10px auto;padding-bottom: 80px;"><font color="#444;">
                                                   	<?php if(!empty($message)):?>
                                                   	<?php print $message;?>
                                               		<?php endif;?>
                                                    </font>
                                                    </p>
                                                    <?php if(!empty($links)):?>
                                                    	<?php foreach($links as $link):?>
                                                    <table class="button main" border="0" cellpadding="10" cellspacing="0" style="display: inline-block;margin: 10px auto 0;border-radius: 6px;font-weight: bold;font-size: 16px;text-align: center;cursor: pointer;color: #FFFFFF;background: #61BD4F;box-shadow: 0px 4px 0px #519839;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;" align="left">
                                                          <tr>
                                                              <td align="center" valign="middle" class="emailButtonContent" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                              
                                                                  <a href="<?php echo site_url($link->url);?>" target="_blank" style="text-decoration: none;padding: 10px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;"><font color="white"><?php print locale($link->slug);?> &rarr;</font></a>
                                                              </td>
                                                          </tr>
                                                    </table>
                                                		<?php endforeach;?>
                                                    <?php endif;?>                                                      
                                                    </td>
                                                </tr>   
                                                <tr>
													<td id="header" valign="top" bgcolor="#051c2d;" style="position:relative;padding: 20px;max-width: 100%!important;background: #051c2d;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
			                                            <h6 style="font-family: Helvetica;font-size: 18px;font-style: normal;font-weight: bold;text-align: center;margin: 20px auto;"><font color="white">ATTE . NEWLINE TEAM</font></h6>
			                                            <img src="<?php print site_asset('/email/her.png');?>" width="275" height="174" style="position: absolute; right: -10px; bottom:0;">
			                                        </td>                                                
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!-- ////////////////////////////
                        //////////// FOOTER //////////// 
                        /////////////////////////// -->


                        <tr>
                            <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                <table border="0" align="center" cellpadding="10" cellspacing="0" width="600" id="emailFooter" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;max-width: 100%!important;padding:0;">
                                        <tr>
                                            <td valign="top" class="footerContent" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;padding:0;">
                                            
                                                <!-- // Begin Module: Standard Footer \\ -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;padding:0;">
                                                    <tr>
                                                        <td valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;padding:0;">
                                                            <br>
                                                            <a href="https://facebook.com/www.nlgames.net">
                                                            <div style="color: #707070;font-family: Arial;font-size: 12px;line-height: 125%;text-align: center;max-width: 100%!important;padding:0;">
                                                                <div style="text-align: left!important;padding-left:80px;background-image: url('<?php echo site_asset('/email/facebook.png');?>');background-repeat: no-repeat;background-size: 65px 65px;background-position: center left;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background: url(<?php echo site_asset('/email/facebook.png');?>) center left / 65px 65px no-repeat!important;height: 60px;"></div>
                                                                <em>Copyright &copy; 2016 Newline Games</em>
                                                            </div></a>
                                                            <br>
                                                        </td>
                                                      
                                                    </tr>

                                                </table>
                                                <!-- // End Module: Standard Footer \\ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Footer \\ -->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
</html>