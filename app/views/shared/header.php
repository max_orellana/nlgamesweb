<header id="hosted">
    <div class="row">
        <div class="pull-left">
            <a href="<?php print site_url('/');?>" class="pennant"></a>
        </div>
        <div class="pull-left">
            <div class="main-actions">
                <a href="<?php print site_url('/guides');?>" class="<?php print segments(1) == 'guides' ? 'active' : '';?>"><?php print locale('guide');?></a>
                <a href="<?php print site_url('/news');?>" class="<?php print segments(1) == 'news' ? 'active' : '';?>"><?php print locale('news');?></a>
                <a href="<?php print config()->forum_url;?>"><?php print locale('community');?></a>
            <?php if( ! empty(session('login'))):?>
                <a href="<?php print site_url('/account/index');?>" class="<?php print segments(1) == session('group') ? 'active' : '';?>"><?php print locale('user_panel');?></a>
            <?php endif;?>
            </div>
        </div>
        <?php include SP . 'app/views/panel/live.php';?>
    </div>
</header>
