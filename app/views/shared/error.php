<div class="bg-container">
    <div class="bg-content-wide">

        <div class="widget white">
            <div class="database-migration">
                <h3><?php print locale('error');?></h3>
                <p><?php print locale('error');?></p>
            <?php if( ! empty($error)):?>
            	<p><?php print $error;?></p>
            <?php endif;?>
            </div>
        </div>
    </div>
</div>