<div class="networks">
	<div class="facebook">
		<a href="https://www.facebook.com/www.nlgames.net" target="blank" title="Facebook">
			<img src="<?php print site_url('/assets/009/img/sidebar-fb.png');?>">
		</a>
	</div>
	<div class="youtube">
		<a href="https://www.youtube.com/channel/UC-8CpJAwKTuJaP5scajDyFw" target="blank" title="Youtube">
			<img src="<?php print site_url('/assets/009/img/sidebar-yt.png');?>">
		</a>
	</div>
</div>